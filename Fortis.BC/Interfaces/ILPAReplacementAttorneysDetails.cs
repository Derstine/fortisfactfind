﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAReplacementAttorneysDetails : IRepository<lpa_replacement_attorneys_detail> { }
}