﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTMonetaryGifts : IRepository<fapt_monetary_gifts> { }
}