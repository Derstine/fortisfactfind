﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAParagraphs : IRepository<lpa_paragraphs> { }
}