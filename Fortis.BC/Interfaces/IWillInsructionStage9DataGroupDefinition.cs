﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage9DataGroupDefinition: IRepository<will_instruction_stage9_data_group_definition> { }
}