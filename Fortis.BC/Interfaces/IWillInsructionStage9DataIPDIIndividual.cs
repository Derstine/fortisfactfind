﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage9DataIPDIIndividual: IRepository<will_instruction_stage9_data_IPDI_individual> { }
}