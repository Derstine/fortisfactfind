﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage9DataDiscretionaryTrust: IRepository<will_instruction_stage9_data_discretionary_trust> { }
}