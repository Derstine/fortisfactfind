﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateControl : IRepository<probate_control> { }
}