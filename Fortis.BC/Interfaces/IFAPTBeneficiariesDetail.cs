﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTBeneficiariesDetail : IRepository<fapt_beneficiaries_detail> { }
}