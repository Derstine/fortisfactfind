﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IClientAppointment : IRepository<client_appointment> { }
}