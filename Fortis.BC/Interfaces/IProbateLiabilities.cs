﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateLiabilities: IRepository<probate_liabilities> { }
}