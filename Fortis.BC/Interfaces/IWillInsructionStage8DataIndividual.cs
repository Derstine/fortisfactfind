﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage8DataIndividual: IRepository<will_instruction_stage8_data_individual> { }
}