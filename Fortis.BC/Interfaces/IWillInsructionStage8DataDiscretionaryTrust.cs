﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage8DataDiscretionaryTrust: IRepository<will_instruction_stage8_data_discretionary_trust> { }
}