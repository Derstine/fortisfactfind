﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateBeneficiaries : IRepository<probate_beneficiaries> { }
}