﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fortis.BusinessComponent
{
    public interface IRepository<T> where T : class
    {
        Task<int> InsertAsync(T t);
        Task<int> UpdateAsync(T t);
        Task<int> DeleteAsync(int id);
        Task<T> GetByIdAsync(int id);
        Task<List<T>> GetAllAsync();
    }
}