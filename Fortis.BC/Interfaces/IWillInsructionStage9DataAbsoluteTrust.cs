﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage9DataAbsoluteTrust: IRepository<will_instruction_stage9_data_absolute_trust> { }
}