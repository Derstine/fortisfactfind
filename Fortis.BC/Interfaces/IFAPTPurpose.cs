﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTPurpose : IRepository<fapt_purpose> { }
}