﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTMonetaryGiftsDetail : IRepository<fapt_monetary_gifts_detail> { }
}