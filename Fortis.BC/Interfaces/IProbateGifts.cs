﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateGifts: IRepository<probate_gifts> { }
}