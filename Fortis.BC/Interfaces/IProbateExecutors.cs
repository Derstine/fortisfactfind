﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateExecutors: IRepository<probate_executors> { }
}