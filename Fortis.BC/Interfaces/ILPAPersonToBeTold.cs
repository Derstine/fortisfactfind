﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAPersonToBeTold : IRepository<lpa_persons_to_be_told> { }
}