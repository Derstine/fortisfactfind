﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPARegister : IRepository<lpa_register> { }
}