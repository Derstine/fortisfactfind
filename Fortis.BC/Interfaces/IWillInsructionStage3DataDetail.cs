﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage3DataDetail: IRepository<will_instruction_stage3_data_detail> { }
}