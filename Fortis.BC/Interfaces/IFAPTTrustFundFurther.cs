﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTTrustFundFurther : IRepository<fapt_trust_fund_further> { }
}