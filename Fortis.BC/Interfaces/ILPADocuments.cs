﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPADocuments : IRepository<lpa_documents> { }
}