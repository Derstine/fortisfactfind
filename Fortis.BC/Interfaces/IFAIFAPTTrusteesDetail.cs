﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IFAPTTrusteesDetail : IRepository<fapt_trustees_detail> { }
}