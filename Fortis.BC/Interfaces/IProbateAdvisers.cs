﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateAdvisers : IRepository<probate_advisers> { }
}