﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAReplacementAttorneys : IRepository<lpa_replacement_attorneys> { }
}