﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAAttorneysDetail : IRepository<lpa_attorneys_detail> { }
}