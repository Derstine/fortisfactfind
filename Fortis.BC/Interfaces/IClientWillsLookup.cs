﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IClientWillsLookup : IRepository<client_wills_lookup> { }
}