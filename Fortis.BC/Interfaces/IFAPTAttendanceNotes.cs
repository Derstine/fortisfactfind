﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTAttendanceNotes : IRepository<fapt_attendance_notes> { }
}