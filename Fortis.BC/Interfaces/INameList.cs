﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface INameList : IRepository<name_list> { }
}