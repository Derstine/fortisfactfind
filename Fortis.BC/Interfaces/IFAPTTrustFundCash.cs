﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTTrustFundCash : IRepository<fapt_trust_fund_cash> { }
}