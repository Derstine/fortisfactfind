﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbatePlan: IRepository<probate_plan> { }
}