﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAPreferences : IRepository<lpa_preferences> { }
}