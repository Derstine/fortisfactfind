﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTTrustFundProperty : IRepository<fapt_trust_fund_property> { }
}