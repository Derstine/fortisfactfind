﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAAttorneys : IRepository<lpa_attorneys> { }
}