﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPACertificateProviderDetails : IRepository<lpa_certificate_provider_details> { }
}