﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateAssets : IRepository<probate_assets> { }
}