﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ITdAdvanceDirective: IRepository<td_advance_directive> { }
}