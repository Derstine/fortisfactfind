﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTBeneficiaries : IRepository<fapt_beneficiaries> { }
}