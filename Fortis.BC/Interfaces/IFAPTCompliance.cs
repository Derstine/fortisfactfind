﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface  IFAPTCompliance : IRepository<fapt_compliance> { }
}