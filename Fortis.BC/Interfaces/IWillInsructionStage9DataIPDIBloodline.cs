﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IWillInsructionStage9DataIPDIBloodline: IRepository<will_instruction_stage9_data_IPDI_bloodline> { }
}