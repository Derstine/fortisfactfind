﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPAttendanceNotes : IRepository<lpa_attendance_notes> { }
}