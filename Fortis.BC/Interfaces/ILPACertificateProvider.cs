﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface ILPACertificateProvider : IRepository<lpa_certificate_provider> { }
}