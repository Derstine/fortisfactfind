﻿using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public interface IProbateRelative: IRepository<probate_relative> { }
}