﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Fortis.ModelDtoc;
using Microsoft.VisualBasic;

namespace Fortis.BusinessComponent
{
    public class CurentCasesComponent
{
    public ICurrentCases CurrentCases { get; }

    public CurentCasesComponent(ICurrentCases currentCases)
    {
        this.CurrentCases = currentCases;
    }

    public async Task<List<CurrentCasesDto>> GetCurrentCases()
    {
        var cases = await this.CurrentCases.GetCurrentCases();
        foreach (var currentCasesDto in cases)
        {
            if (currentCasesDto.Person2ForeNames.Trim().Length > 0)
                currentCasesDto.ClientName = currentCasesDto.Person1ForeNames + " " + currentCasesDto.Person1Surname + "&" + currentCasesDto.Person2ForeNames + " " + currentCasesDto.Person2Surname;
            else
                currentCasesDto.ClientName = currentCasesDto.Person1ForeNames + " " + currentCasesDto.Person1Surname;

            if (currentCasesDto.AppointmentStart != "")
                currentCasesDto.AppointmentStart = Strings.Mid(currentCasesDto.AppointmentStart, 1, 16);

            if (currentCasesDto.Status == "offline")
            {
                currentCasesDto.Edit = "Edit";
                currentCasesDto.Delete = "Delete";
                currentCasesDto.Archive = "Archive";
                currentCasesDto.Synchronize = "mark as ready";
            }
            else
            {
                currentCasesDto.Edit = "";
                currentCasesDto.Delete = "";
                currentCasesDto.Archive = "";
                currentCasesDto.Synchronize = "mark NOT ready";
            }
        }
        return cases;
    }
}

public interface ICurrentCases
{
    Task<List<CurrentCasesDto>> GetCurrentCases();
}

public interface ITest
{
    IEnumerable<string> GetString();
}

public class TestComponent
{
    public ITest CurrentCases { get; }
    public TestComponent(ITest currentCases)
    {
        this.CurrentCases = currentCases;
    }
    public IEnumerable<string> Testing()
    {
        return CurrentCases.GetString();
    }
}
}


