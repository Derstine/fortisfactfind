﻿using System;
using System.Net.NetworkInformation;

namespace Fortis.BusinessComponent
{
    public static class Internet
    {
        public static bool IsAvailable()
        {
            try
            {
                var myPing = new Ping();
                var host = "google.com";
                var buffer = new byte[32];
                var timeout = 1000;
                var pingOptions = new PingOptions();
                var reply = myPing.Send(host, timeout, buffer, pingOptions);
                return reply != null && reply.Status == IPStatus.Success;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}