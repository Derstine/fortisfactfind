﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Fortis.Modelc;

namespace Fortis.BusinessComponent
{
    public class UserActivityComponent
    {
        private readonly IUserActivity _userActivity;

        public UserActivityComponent(IUserActivity userActivity)
        {
            _userActivity = userActivity;
        }


        public async Task<int> InsertNewActivity(string message, int clientId=0)
        {
            var activity = new user_activity {clientID = clientId, activity = message};
            return await _userActivity.InsertAsync(activity);
        }
    }
}