﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fortis.ModelDtoc
{
    public class CurrentCasesDto
    {
        public int Id { get; set; }
        public string Appointment { get; set; }
        public string Person1ForeNames { get; set; }
        public string Person1Surname { get; set; }
        public string Person2ForeNames { get; set; }
        public string Person2Surname { get; set; }
        public string Person1Address { get; set; }
        public string Person1Postcode { get; set; }
        public string AppointmentStart { get; set; }
        public string Status { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string Edit { get; set; }
        public string Delete { get; set; }
        public string Archive { get; set; }
        public string Synchronize { get; set; }
    }
}
