﻿Public Class CurrentCasesDto
    Public Property Id As Integer
    Public Property Appointment As String

    Public Property Person1ForeNames() As String
    Public Property Person1Surname() As String   
    
    Public Property Person2ForeNames() As String
    Public Property Person2Surname() As String

    Public Property Person1Address() As String
    Public Property Person1Postcode() As String

    Public Property  AppointmentStart() As String

    Public Property  Status() As String

    Public Property ClientName As String
    Public Property Address As String
    Public Property Postcode As String
    Public Property Edit As String
    Public  Property Delete As String
    Public  Property Archive As String
    Public  Property Synchronize As String
End Class


