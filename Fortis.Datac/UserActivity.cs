﻿using System.Collections.Generic;
using System.Data.OleDb;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using Fortis.BusinessComponent;
using Fortis.Modelc;
using Fortis.ModelDtoc;

namespace Fortis.Datac
{
    public class UserActivity:IUserActivity
    {
        public async Task<int> InsertNewActivity(user_activity userActivity)
        {
            using (var con = new OleDbConnection(new Connection().BuildConnectionString))
            {
                string sql = @"INSERT INTO user_activity
	                                                (
		                                                clientID, 
                                                        activityTimestamp, 
                                                        activity, 
                                                        activityDetail
	                                                )
	                                                VALUES
	                                                (		
                                                        ?clientID?,
		                                                Now(),
		                                                ?activity?,
		                                                ?activityDetail?
	                                                )";
                return (await con.ExecuteAsync(sql, userActivity));
            }
        }

        public async Task<int> InsertAsync(user_activity t)
        {
            using (var con = new OleDbConnection(new Connection().BuildConnectionString))
            {
                string sql = @"INSERT INTO user_activity
	                                                (
		                                                clientID, 
                                                        activityTimestamp, 
                                                        activity, 
                                                        activityDetail
	                                                )
	                                                VALUES
	                                                (		
                                                        ?clientID?,
		                                                Now(),
		                                                ?activity?,
		                                                ?activityDetail?
	                                                )";
                return (await con.ExecuteAsync(sql, t));
            }
        }

        public Task<int> UpdateAsync(user_activity t)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> DeleteAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<user_activity> GetByIdAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<user_activity>> GetAllAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}