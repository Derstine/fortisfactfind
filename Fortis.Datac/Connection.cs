﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fortis.Datac
{
    public class Connection
    {
        public string BuildConnectionString
        {
            get
            {
                OleDbConnectionStringBuilder builder = new OleDbConnectionStringBuilder()
                {
                    ConnectionString = string.Concat("Data Source=", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "fortis.accdb"))
                };
                builder.Add("Provider", "Microsoft.ACE.OLEDB.12.0");
                return builder.ToString();
            }
        }

        public Connection()
        {
        }
    }
}
