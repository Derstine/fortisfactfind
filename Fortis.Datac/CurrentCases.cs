﻿using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Fortis.BusinessComponent;
using Fortis.ModelDtoc;

namespace Fortis.Datac
{
    public class CurrentCases : ICurrentCases
    {
        public async Task<List<CurrentCasesDto>> GetCurrentCases()
        {
            string sql = @"SELECT
                            will_instruction_stage2_data.willClientSequence AS Appointment,
                            person1Forenames,
                            person1Surname,
                            person1Address,
                            person1Postcode,
                            person2Forenames,
                            person2Surname,
                            client_appointment.appointmentStart,
                            status
                    FROM    ((will_instruction_stage2_data
                        INNER JOIN
                            will_instruction_stage0_data
                                ON will_instruction_stage2_data.willClientSequence = will_instruction_stage0_data.sequence)
                        LEFT JOIN
                            client_appointment
                                ON will_instruction_stage2_data.willClientSequence = client_appointment.sequence)
                    WHERE
                            will_instruction_stage0_data.status = 'offline'
                            OR will_instruction_stage0_data.status = 'synchronise'
                    ORDER BY
                            will_instruction_stage2_data.person1Surname,
                            will_instruction_stage2_data.person1Forenames ASC;";
            using (var con=new OleDbConnection(new Connection().BuildConnectionString))
            {
                return (await con.QueryAsync<CurrentCasesDto>(sql)).ToList();
            }
        }
    }
}