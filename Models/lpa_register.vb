Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class lpa_register
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(100)>
    Public Property registerType As String

    <StringLength(100)>
    Public Property registerNames As String

    <StringLength(15)>
    Public Property registerPhone As String

    Public Property registerAddress As String

    <StringLength(100)>
    Public Property registerEmail As String

    <StringLength(100)>
    Public Property corresType As String

    <StringLength(255)>
    Public Property corresNames As String

    <StringLength(255)>
    Public Property corresPhone As String

    Public Property corresAddress As String

    <StringLength(255)>
    Public Property corresEmail As String

    <StringLength(255)>
    Public Property repeatFlag As String

    <StringLength(255)>
    Public Property byPost As String

    <StringLength(255)>
    Public Property byEmail As String

    <StringLength(255)>
    Public Property byPhone As String

    <StringLength(255)>
    Public Property Welsh As String

    <StringLength(255)>
    Public Property registerTitle As String

    <StringLength(255)>
    Public Property registerSurname As String

    <StringLength(255)>
    Public Property registerPostcode As String

    <StringLength(255)>
    Public Property corresTitle As String

    <StringLength(255)>
    Public Property corresSurname As String

    <StringLength(255)>
    Public Property corresPostcode As String

    <StringLength(255)>
    Public Property witnessTitle As String

    <StringLength(255)>
    Public Property witnessNames As String

    <StringLength(255)>
    Public Property witnessSurname As String

    Public Property witnessAddress As String

    <StringLength(255)>
    Public Property witnessPostcode As String

    <StringLength(255)>
    Public Property witnessPhone As String

    <StringLength(255)>
    Public Property witnessEmail As String
End Class
