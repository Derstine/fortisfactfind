Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class lpa_paragraphs
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    <StringLength(255)>
    Public Property lpaType As String

    Public Property paragraph As String

    <StringLength(255)>
    Public Property paragraphType As String
End Class
