Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class lpa_persons_to_be_told
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(100)>
    Public Property personType As String

    <StringLength(100)>
    Public Property personNames As String

    <StringLength(15)>
    Public Property personPhone As String

    <StringLength(200)>
    Public Property personRelationshipClient1 As String

    <StringLength(200)>
    Public Property personRelationshipClient2 As String

    Public Property personAddress As String

    <StringLength(100)>
    Public Property personEmail As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property personTitle As String

    <StringLength(255)>
    Public Property personSurname As String

    <StringLength(255)>
    Public Property personPostcode As String
End Class
