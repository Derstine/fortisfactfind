Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class client_products
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(10)>
    Public Property whichClient As String

    <StringLength(1)>
    Public Property q1 As String

    <StringLength(1)>
    Public Property q2 As String

    <StringLength(1)>
    Public Property q3 As String

    <StringLength(1)>
    Public Property q4 As String

    <StringLength(1)>
    Public Property q5 As String

    <StringLength(1)>
    Public Property q6 As String

    <StringLength(1)>
    Public Property q7 As String

    <StringLength(1)>
    Public Property q8 As String

    <StringLength(1)>
    Public Property q9 As String

    <StringLength(1)>
    Public Property q10 As String

    <StringLength(1)>
    Public Property q11 As String

    <StringLength(1)>
    Public Property q12 As String

    <StringLength(1)>
    Public Property q13 As String

    <StringLength(1)>
    Public Property q14 As String

    <StringLength(1)>
    Public Property q15 As String
End Class
