Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage10_data
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property notes As String

    <StringLength(200)>
    Public Property recording As String

    Public Property q1 As String

    Public Property q2 As String

    Public Property q3 As String

    Public Property q4 As String

    Public Property q5 As String

    Public Property q6 As String

    Public Property q7 As String

    Public Property q8 As String

    Public Property q9 As String

    Public Property q10 As String

    Public Property q11 As String

    Public Property q12 As String

    Public Property q13 As String

    Public Property q14 As String

    Public Property q15 As String

    Public Property q16 As String

    Public Property q17 As String

    Public Property q18 As String

    Public Property q19 As String

    Public Property q20 As String

    Public Property q1tick As Integer?

    Public Property q2tick As Integer?

    Public Property q3tick As Integer?

    Public Property q4tick As Integer?

    Public Property q5tick As Integer?

    Public Property q6tick As Integer?

    Public Property q7tick As Integer?

    Public Property q8tick As Integer?

    Public Property q9tick As Integer?

    Public Property q10tick As Integer?

    Public Property q11tick As Integer?

    Public Property q12tick As Integer?

    Public Property q13tick As Integer?

    Public Property q14tick As Integer?

    Public Property q15tick As Integer?

    Public Property q16tick As Integer?

    Public Property q17tick As Integer?

    Public Property q18tick As Integer?

    Public Property q19tick As Integer?

    Public Property q20tick As Integer?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String
End Class
