Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class lpa_documents
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property client1PFA As String

    <StringLength(255)>
    Public Property client2PFA As String

    <StringLength(255)>
    Public Property mirrorPFA As String

    <StringLength(255)>
    Public Property client1HW As String

    <StringLength(255)>
    Public Property client2HW As String

    <StringLength(255)>
    Public Property mirrorHW As String

    <StringLength(100)>
    Public Property status As String

    Public Property statusDate As Date?

    <StringLength(255)>
    Public Property client1B As String

    <StringLength(255)>
    Public Property client2B As String

    <StringLength(255)>
    Public Property mirrorB As String
End Class
