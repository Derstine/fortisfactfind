Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class fapt_trust_fund_property
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property address As String

    Public Property worth As Double?

    <StringLength(50)>
    Public Property registeredFlag As String

    Public Property owner As String

    <StringLength(1)>
    Public Property mortgageFlag As String

    Public Property mortgageValue As Double?

    <StringLength(20)>
    Public Property leaseOrFreehold As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property assetCategory As String

    <StringLength(255)>
    Public Property description As String

    <StringLength(1)>
    Public Property includeInTrust As String
End Class
