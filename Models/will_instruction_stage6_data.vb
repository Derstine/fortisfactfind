Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage6_data
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(20)>
    Public Property funeralClient1Type As String

    Public Property funeralClient1Wishes As String

    <StringLength(20)>
    Public Property funeralClient2Type As String

    Public Property funeralClient2Wishes As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property funeralClient1Plan As String

    <StringLength(255)>
    Public Property funeralClient2Plan As String
End Class
