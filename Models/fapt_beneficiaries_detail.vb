Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class fapt_beneficiaries_detail
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property trustSequence As Integer?

    <StringLength(100)>
    Public Property beneficiaryType As String

    <StringLength(100)>
    Public Property beneficiaryName As String

    <StringLength(10)>
    Public Property lapseOrIssue As String

    Public Property percentage As Single?

    Public Property atWhatAge As Integer?

    Public Property atWhatAgeIssue As Integer?

    <StringLength(1)>
    Public Property primaryBeneficiaryFlag As String

    <StringLength(255)>
    Public Property relationshipClient1 As String

    <StringLength(255)>
    Public Property relationshipClient2 As String

    Public Property beneficiaryDOB As Date?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property beneficiaryTitle As String

    <StringLength(255)>
    Public Property beneficiaryForenames As String

    <StringLength(255)>
    Public Property beneficiarySurname As String
End Class
