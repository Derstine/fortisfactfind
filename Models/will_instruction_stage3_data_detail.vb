Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage3_data_detail
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(100)>
    Public Property executorType As String

    <StringLength(100)>
    Public Property executorNames As String

    <StringLength(15)>
    Public Property executorPhone As String

    <StringLength(200)>
    Public Property executorRelationshipClient1 As String

    <StringLength(200)>
    Public Property executorRelationshipClient2 As String

    Public Property executorAddress As String

    <StringLength(100)>
    Public Property executorEmail As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property executorTitle As String

    <StringLength(255)>
    Public Property executorSurname As String

    <StringLength(255)>
    Public Property executorPostcode As String

    <StringLength(20)>
    Public Property trusteeFlag As String

    <StringLength(20)>
    Public Property executorDOB As String
End Class
