Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class probate_assets
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property whichClient As String

    Public Property details As String

    <StringLength(255)>
    Public Property worth As String

    <StringLength(255)>
    Public Property assetType As String
End Class
