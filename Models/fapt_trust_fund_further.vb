Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class fapt_trust_fund_further
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property description As String

    Public Property worth As Double?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property assetCategory As String

    <StringLength(1)>
    Public Property includeInTrust As String
End Class
