Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class client_storage
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property sourceClientID As Integer?

    <StringLength(255)>
    Public Property storageFlag As String

    <StringLength(255)>
    Public Property storageType As String

    <StringLength(30)>
    Public Property storageAmount As String

    <StringLength(255)>
    Public Property storageSortCode As String

    <StringLength(255)>
    Public Property storageAccountNumber As String

    <StringLength(255)>
    Public Property storagePaymentMethod As String
End Class
