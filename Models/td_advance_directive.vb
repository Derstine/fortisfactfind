Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class td_advance_directive
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property valueStatement As String

    <StringLength(1)>
    Public Property option1 As String

    <StringLength(1)>
    Public Property option2 As String

    <StringLength(1)>
    Public Property option3 As String

    <StringLength(1)>
    Public Property option4 As String

    <StringLength(1)>
    Public Property option5 As String

    <StringLength(1)>
    Public Property option6 As String

    <StringLength(1)>
    Public Property option7 As String

    <StringLength(1)>
    Public Property option8 As String

    <StringLength(1)>
    Public Property option9 As String

    <StringLength(1)>
    Public Property option10 As String

    <StringLength(1)>
    Public Property option11 As String

    <StringLength(1)>
    Public Property option12 As String

    <StringLength(1)>
    Public Property option13 As String

    <StringLength(1)>
    Public Property option14 As String

    <StringLength(1)>
    Public Property option15 As String

    <StringLength(1)>
    Public Property option16 As String

    <StringLength(1)>
    Public Property option17 As String

    <StringLength(1)>
    Public Property option18 As String

    <StringLength(1)>
    Public Property option19 As String

    <StringLength(1)>
    Public Property option20 As String

    Public Property additionalText1 As String

    Public Property additionalText2 As String

    Public Property additionalText3 As String

    Public Property additionalText4 As String

    Public Property additionalText5 As String

    <StringLength(50)>
    Public Property whichClient As String
End Class
