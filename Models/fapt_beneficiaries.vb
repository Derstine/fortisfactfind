Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class fapt_beneficiaries
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property charityBackstop As String

    Public Property charityDetails As String

    <StringLength(255)>
    Public Property whichClient As String

    Public Property letterOfWishes As String
End Class
