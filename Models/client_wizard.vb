Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class client_wizard
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property manualOverride As String

    <StringLength(255)>
    Public Property willReviewFlag As String

    <StringLength(255)>
    Public Property willPPTFlag As String

    <StringLength(255)>
    Public Property willBasicFlag As String

    <StringLength(255)>
    Public Property lpaHWFlag As String

    <StringLength(255)>
    Public Property lpaPFAFlag As String

    <StringLength(255)>
    Public Property faptFlag As String

    <StringLength(255)>
    Public Property probateFlag As String

    <StringLength(255)>
    Public Property otherFlag As String

    <StringLength(255)>
    Public Property otherPrice As String

    <StringLength(255)>
    Public Property otherDescription As String

    <StringLength(255)>
    Public Property manHWOPG As String

    <StringLength(255)>
    Public Property manPFAOPG As String

    <StringLength(5)>
    Public Property followUpFlag As String

    Public Property followUpAppointmentDate As Date?

    Public Property followUpNotes As String

    <StringLength(5)>
    Public Property rec_willReviewFlag As String

    <StringLength(5)>
    Public Property rec_willPPTFlag As String

    <StringLength(5)>
    Public Property rec_willBasicFlag As String

    <StringLength(5)>
    Public Property rec_lpaHWFlag As String

    <StringLength(5)>
    Public Property rec_lpaPFAFlag As String

    <StringLength(5)>
    Public Property rec_faptFlag As String

    <StringLength(5)>
    Public Property rec_probateFlag As String

    <StringLength(5)>
    Public Property rec_storageFlag As String

    <StringLength(5)>
    Public Property storageFlag As String
End Class
