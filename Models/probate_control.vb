Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class probate_control
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property whichClient As String

    Public Property deathAddress As String

    <StringLength(255)>
    Public Property deathPostcode As String

    <StringLength(255)>
    Public Property nursingHomeFlag As String

    Public Property deathDate As Date?

    <StringLength(255)>
    Public Property willFlag As String

    Public Property willDate As Date?

    Public Property attendanceNotes As String

    <StringLength(255)>
    Public Property assetValue As String

    <StringLength(255)>
    Public Property liabilitiesValue As String

    <StringLength(255)>
    Public Property netAssetValue As String

    <StringLength(255)>
    Public Property probateFee As String

    <StringLength(255)>
    Public Property probateCost As String

    <StringLength(255)>
    Public Property giftsGivenFlag As String
End Class
