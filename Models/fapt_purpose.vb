Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class fapt_purpose
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property q1 As Integer?

    Public Property q2 As Integer?

    Public Property q3 As Integer?

    Public Property q4 As Integer?

    Public Property q5 As Integer?

    Public Property q6 As Integer?

    Public Property q7 As Integer?

    Public Property q8 As Integer?

    Public Property q9 As Integer?

    Public Property q10 As Integer?

    Public Property q11 As Integer?

    Public Property q12 As Integer?

    Public Property q13 As Integer?

    Public Property q14 As Integer?

    Public Property q15 As Integer?

    Public Property q16 As Integer?

    Public Property q17 As Integer?

    Public Property q18 As Integer?

    Public Property q19 As Integer?

    Public Property q20 As Integer?
End Class
