Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage12_data
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property whichClient As String

    Public Property mainResidence As Decimal?

    Public Property otherProperties As Decimal?

    Public Property overseasProperties As Decimal?

    Public Property bankAccounts As Decimal?

    Public Property investments As Decimal?

    Public Property lifePolicies As Decimal?

    Public Property pension As Decimal?

    <StringLength(255)>
    Public Property businessAssets As String

    <StringLength(1)>
    Public Property businessType As String

    Public Property businessValue As Decimal?

    Public Property partnershipAgreement As Decimal?

    Public Property shares As Decimal?

    <StringLength(1)>
    Public Property otherPartnersFlag As String

    <StringLength(200)>
    Public Property natureOfBusiness As String

    Public Property totalBusinessAssets As Decimal?

    Public Property broughtForward As Decimal?

    Public Property mortgages As Decimal?

    Public Property loans As Decimal?

    Public Property liabilities As Decimal?

    Public Property additionalInformation As String

    <StringLength(255)>
    Public Property whichDocument As String
End Class
