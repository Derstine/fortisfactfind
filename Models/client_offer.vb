Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class client_offer
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(10)>
    Public Property whichClient As String

    Public Property willsPrice As Integer?

    <StringLength(1)>
    Public Property willsFlag As String

    Public Property HWLPAPrice As Integer?

    <StringLength(1)>
    Public Property HWLPAFlag As String

    Public Property PFLPAPrice As Integer?

    <StringLength(1)>
    Public Property PFLPAFlag As String

    Public Property TrustsPrice As Integer?

    <StringLength(1)>
    Public Property TrustsFlag As String

    Public Property FuneralPlanPrice As Integer?

    <StringLength(1)>
    Public Property FuneralPlanFlag As String

    Public Property ProbatePrice As Integer?

    <StringLength(1)>
    Public Property ProbateFlag As String
End Class
