Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage7_data
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(10)>
    Public Property giftFrom As String

    <StringLength(1)>
    Public Property giftSharedFlag As String

    <StringLength(40)>
    Public Property typeOfGift As String

    Public Property itemDetails As String

    <StringLength(1)>
    Public Property taxFree As String

    <StringLength(1)>
    Public Property soleJointGiftFlag As String

    <StringLength(1)>
    Public Property firstSecondDeathFlag As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String
End Class
