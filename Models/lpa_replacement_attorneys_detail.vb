Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class lpa_replacement_attorneys_detail
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(100)>
    Public Property attorneyType As String

    <StringLength(100)>
    Public Property attorneyNames As String

    <StringLength(15)>
    Public Property attorneyPhone As String

    <StringLength(200)>
    Public Property attorneyRelationshipClient1 As String

    <StringLength(200)>
    Public Property attorneyRelationshipClient2 As String

    Public Property attorneyAddress As String

    <StringLength(100)>
    Public Property attorneyEmail As String

    <StringLength(255)>
    Public Property howToAct As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property occupation As String

    Public Property attorneyDOB As Date?

    <StringLength(255)>
    Public Property attorneyTitle As String

    <StringLength(255)>
    Public Property attorneySurname As String

    <StringLength(255)>
    Public Property attorneyPostcode As String
End Class
