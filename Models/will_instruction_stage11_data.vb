Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage11_data
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property insertionPoint As Integer?

    Public Property libraryClauseSequence As Integer?

    Public Property bespokeText As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String
End Class
