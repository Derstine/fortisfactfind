Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class electronicFile
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property title As String

    <StringLength(255)>
    Public Property filename As String

    <StringLength(255)>
    Public Property associateWith As String
End Class
