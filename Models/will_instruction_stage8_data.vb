Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage8_data
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(100)>
    Public Property trustType As String

    <StringLength(1)>
    Public Property naConfirmationFlag As String

    <StringLength(20)>
    Public Property FLITtype As String

    <StringLength(100)>
    Public Property FLITname As String

    <StringLength(30)>
    Public Property r2rFlag As String

    Public Property r2rYears As Integer?

    Public Property r2rTime As String

    Public Property r2rLocation As String

    <StringLength(100)>
    Public Property r2rWhoTo As String

    <StringLength(200)>
    Public Property r2rNamed As String

    <StringLength(50)>
    Public Property r2rBenefits As String

    <StringLength(15)>
    Public Property r2rMortgage As String

    <StringLength(15)>
    Public Property r2rFurniture As String

    <StringLength(50)>
    Public Property r2rBeneficialShare As String

    <StringLength(15)>
    Public Property r2rCeaseRemarry As String

    <StringLength(20)>
    Public Property dtEstateType As String

    Public Property dtEstate As String

    Public Property dtWishes As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(60)>
    Public Property backstopCharityName As String

    <StringLength(20)>
    Public Property backstopCharityNumber As String
End Class
