Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage0_data
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property consultantSequence As Integer?

    Public Property consultantCompany As Integer?

    <StringLength(100)>
    Public Property status As String

    Public Property statusDate As Date?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property parentFlag As String

    Public Property linkedSequence As Integer?

    Public Property serverSequence As Integer?

    <StringLength(255)>
    Public Property misc1 As String

    <StringLength(255)>
    Public Property misc2 As String

    <StringLength(255)>
    Public Property misc3 As String

    <StringLength(255)>
    Public Property misc4 As String

    <StringLength(255)>
    Public Property misc5 As String

    <StringLength(255)>
    Public Property misc6 As String

    <StringLength(255)>
    Public Property misc7 As String

    <StringLength(255)>
    Public Property misc8 As String

    <StringLength(255)>
    Public Property misc9 As String

    <StringLength(255)>
    Public Property misc10 As String

    <StringLength(20)>
    Public Property TDProduct As String
End Class
