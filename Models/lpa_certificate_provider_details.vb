Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class lpa_certificate_provider_details
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(100)>
    Public Property certificateType As String

    <StringLength(100)>
    Public Property certificateNames As String

    <StringLength(15)>
    Public Property certificatePhone As String

    <StringLength(200)>
    Public Property certificateRelationshipClient1 As String

    <StringLength(200)>
    Public Property certificateRelationshipClient2 As String

    Public Property certificateAddress As String

    <StringLength(100)>
    Public Property certificateEmail As String

    <StringLength(255)>
    Public Property howToAct As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property occupation As String

    <StringLength(255)>
    Public Property certificateTitle As String

    <StringLength(255)>
    Public Property certificateSurname As String

    <StringLength(255)>
    Public Property certificatePostcode As String
End Class
