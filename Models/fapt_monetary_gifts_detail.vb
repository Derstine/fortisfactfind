Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class fapt_monetary_gifts_detail
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property giftSequence As Integer?

    <StringLength(20)>
    Public Property beneficiaryType As String

    <StringLength(200)>
    Public Property beneficiaryName As String

    <StringLength(20)>
    Public Property giftSharedType As String

    Public Property giftSharedPercentage As Single?

    <StringLength(200)>
    Public Property charityName As String

    <StringLength(20)>
    Public Property charityRegNumber As String

    Public Property atWhatAge As Integer?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property relationToClient1 As String

    <StringLength(255)>
    Public Property relationToClient2 As String

    <StringLength(255)>
    Public Property beneficiaryTitle As String

    <StringLength(255)>
    Public Property beneficiaryForenames As String

    <StringLength(255)>
    Public Property beneficiarySurname As String
End Class
