Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class clause_library
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property client As Integer?

    <StringLength(255)>
    Public Property companySequence As String

    Public Property addedDate As Date?

    <StringLength(200)>
    Public Property title As String

    <StringLength(200)>
    Public Property type As String

    Public Property clause As String
End Class
