Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class name_list
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property forenames As String

    <StringLength(255)>
    Public Property surname As String

    <StringLength(255)>
    Public Property fullname As String

    Public Property address As String

    <StringLength(255)>
    Public Property phone As String

    <StringLength(255)>
    Public Property email As String

    <StringLength(255)>
    Public Property occupation As String

    <StringLength(255)>
    Public Property postcode As String

    Public Property dob As Date?

    <StringLength(255)>
    Public Property title As String

    <StringLength(255)>
    Public Property relationshipClient1 As String

    <StringLength(255)>
    Public Property relationshipClient2 As String
End Class
