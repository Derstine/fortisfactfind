Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class client_returns
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property investment As Integer?

    Public Property deposit As Integer?

    Public Property months As Integer?
End Class
