Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

<Table("userdata")>
Partial Public Class userdata
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    <StringLength(255)>
    Public Property title As String

    <StringLength(255)>
    Public Property forename As String

    <StringLength(255)>
    Public Property surname As String

    Public Property address As String

    <StringLength(255)>
    Public Property postcode As String

    <StringLength(255)>
    Public Property company As String

    <StringLength(255)>
    Public Property phone As String

    <StringLength(255)>
    Public Property mobile As String

    <StringLength(255)>
    Public Property email As String

    Public Property userDOB As Date?

    <StringLength(255)>
    Public Property lpaTitle As String

    <StringLength(255)>
    Public Property lpaForename As String

    <StringLength(255)>
    Public Property lpaSurname As String

    Public Property lpaAddress As String

    <StringLength(255)>
    Public Property lpaPostcode As String

    <StringLength(255)>
    Public Property misc1 As String

    <StringLength(255)>
    Public Property misc2 As String

    <StringLength(255)>
    Public Property misc3 As String

    <StringLength(255)>
    Public Property misc4 As String

    <StringLength(255)>
    Public Property misc5 As String

    <StringLength(255)>
    Public Property misc6 As String

    <StringLength(255)>
    Public Property misc7 As String

    <StringLength(255)>
    Public Property misc8 As String

    <StringLength(255)>
    Public Property misc9 As String

    <StringLength(255)>
    Public Property misc10 As String

    <StringLength(255)>
    Public Property username As String

    <StringLength(255)>
    Public Property apikey As String

    <StringLength(255)>
    Public Property witnessTitle As String

    <StringLength(255)>
    Public Property witnessForename As String

    <StringLength(255)>
    Public Property witnessSurname As String

    Public Property witnessAddress As String

    <StringLength(255)>
    Public Property witnessPostcode As String

    <StringLength(255)>
    Public Property witnessEmail As String

    <StringLength(255)>
    Public Property witnessPhone As String

    <StringLength(255)>
    Public Property witnessOccupation As String

    Public Property witnessDOB As Date?

    <StringLength(1)>
    Public Property faptReadyFlag As String

    <StringLength(1)>
    Public Property sendDatabaseFlag As String

    Public Property HWcouldText As String

    Public Property HWshouldText As String

    Public Property PFcouldText As String

    Public Property PFshouldText As String

    <StringLength(1)>
    Public Property internationalWillFlag As String
End Class
