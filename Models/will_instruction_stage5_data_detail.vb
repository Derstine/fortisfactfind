Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage5_data_detail
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(20)>
    Public Property guardianTitle As String

    <StringLength(50)>
    Public Property guardianSurname As String

    <StringLength(100)>
    Public Property guardianNames As String

    <StringLength(20)>
    Public Property guardianType As String

    Public Property guardianAddress As String

    <StringLength(20)>
    Public Property guardianPhone As String

    <StringLength(100)>
    Public Property guardianEmail As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property guardianPostcode As String

    <StringLength(50)>
    Public Property relationToClient1 As String

    <StringLength(50)>
    Public Property relationToClient2 As String

    <StringLength(20)>
    Public Property guardianDOB As String
End Class
