Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage4_data_detail
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(50)>
    Public Property childSurname As String

    <StringLength(100)>
    Public Property childForenames As String

    <StringLength(50)>
    Public Property relationToClient1 As String

    <StringLength(50)>
    Public Property relationToClient2 As String

    Public Property address As String

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String

    <StringLength(255)>
    Public Property childTitle As String

    <StringLength(255)>
    Public Property childPostcode As String

    <StringLength(20)>
    Public Property childDOB As String

    <StringLength(255)>
    Public Property childEmail As String

    <StringLength(255)>
    Public Property childPhone As String
End Class
