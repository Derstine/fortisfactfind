Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class probate_utilities
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property utilityType As String

    <StringLength(255)>
    Public Property provider As String

    Public Property providerAddress As String

    <StringLength(255)>
    Public Property providerPostcode As String

    <StringLength(255)>
    Public Property providerPhone As String

    <StringLength(255)>
    Public Property providerEmail As String

    <StringLength(255)>
    Public Property accountNumber As String

    <StringLength(255)>
    Public Property meterReading As String
End Class
