Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class client_appointment
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property sourceClientID As Integer?

    <StringLength(255)>
    Public Property appointmentID As String

    Public Property appointmentStart As Date?

    Public Property appointmentEnd As Date?

    <StringLength(255)>
    Public Property appointmentTitle As String

    <StringLength(255)>
    Public Property campaignID As String
End Class
