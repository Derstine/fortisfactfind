Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class user_activity
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property ID As Integer

    Public Property clientID As Integer?

    Public Property activityTimestamp As Date?

    <StringLength(255)>
    Public Property activity As String

    Public Property activityDetail As String
End Class
