Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Data.Entity.Spatial

Partial Public Class will_instruction_stage8_data_charity
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property sequence As Integer

    Public Property willClientSequence As Integer?

    Public Property trustSequence As Integer?

    <StringLength(100)>
    Public Property beneficiaryType As String

    <StringLength(100)>
    Public Property beneficiaryName As String

    <StringLength(30)>
    Public Property charityRegNumber As String

    <StringLength(10)>
    Public Property lapseOrIssue As String

    Public Property percentage As Single?

    <StringLength(255)>
    Public Property whichClient As String

    <StringLength(255)>
    Public Property whichDocument As String
End Class
