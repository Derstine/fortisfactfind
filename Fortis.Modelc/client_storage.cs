namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class client_storage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? sourceClientID { get; set; }

        [StringLength(255)]
        public string storageFlag { get; set; }

        [StringLength(255)]
        public string storageType { get; set; }

        [StringLength(30)]
        public string storageAmount { get; set; }

        [StringLength(255)]
        public string storageSortCode { get; set; }

        [StringLength(255)]
        public string storageAccountNumber { get; set; }

        [StringLength(255)]
        public string storagePaymentMethod { get; set; }
    }
}
