namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class clause_library
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? client { get; set; }

        [StringLength(255)]
        public string companySequence { get; set; }

        public DateTime? addedDate { get; set; }

        [StringLength(200)]
        public string title { get; set; }

        [StringLength(200)]
        public string type { get; set; }

        public string clause { get; set; }
    }
}
