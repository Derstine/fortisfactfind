namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_trust_fund_cash
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public string description { get; set; }

        public double? worth { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string assetCategory { get; set; }

        [StringLength(255)]
        public string currentCharges { get; set; }

        [StringLength(1)]
        public string includeInTrust { get; set; }
    }
}
