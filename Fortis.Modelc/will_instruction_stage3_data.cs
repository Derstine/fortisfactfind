namespace Fortis.Modelc
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage3_data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(100)]
        public string executorType { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }
    }
}
