namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class client_appointment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? sourceClientID { get; set; }

        [StringLength(255)]
        public string appointmentID { get; set; }

        public DateTime? appointmentStart { get; set; }

        public DateTime? appointmentEnd { get; set; }

        [StringLength(255)]
        public string appointmentTitle { get; set; }

        [StringLength(255)]
        public string campaignID { get; set; }
    }
}
