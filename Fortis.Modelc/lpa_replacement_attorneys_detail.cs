namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lpa_replacement_attorneys_detail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(100)]
        public string attorneyType { get; set; }

        [StringLength(100)]
        public string attorneyNames { get; set; }

        [StringLength(15)]
        public string attorneyPhone { get; set; }

        [StringLength(200)]
        public string attorneyRelationshipClient1 { get; set; }

        [StringLength(200)]
        public string attorneyRelationshipClient2 { get; set; }

        public string attorneyAddress { get; set; }

        [StringLength(100)]
        public string attorneyEmail { get; set; }

        [StringLength(255)]
        public string howToAct { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string occupation { get; set; }

        public DateTime? attorneyDOB { get; set; }

        [StringLength(255)]
        public string attorneyTitle { get; set; }

        [StringLength(255)]
        public string attorneySurname { get; set; }

        [StringLength(255)]
        public string attorneyPostcode { get; set; }
    }
}
