namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_monetary_gifts_detail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? giftSequence { get; set; }

        [StringLength(20)]
        public string beneficiaryType { get; set; }

        [StringLength(200)]
        public string beneficiaryName { get; set; }

        [StringLength(20)]
        public string giftSharedType { get; set; }

        public float? giftSharedPercentage { get; set; }

        [StringLength(200)]
        public string charityName { get; set; }

        [StringLength(20)]
        public string charityRegNumber { get; set; }

        public int? atWhatAge { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string relationToClient1 { get; set; }

        [StringLength(255)]
        public string relationToClient2 { get; set; }

        [StringLength(255)]
        public string beneficiaryTitle { get; set; }

        [StringLength(255)]
        public string beneficiaryForenames { get; set; }

        [StringLength(255)]
        public string beneficiarySurname { get; set; }
    }
}
