namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_beneficiaries
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string charityBackstop { get; set; }

        public string charityDetails { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        public string letterOfWishes { get; set; }
    }
}
