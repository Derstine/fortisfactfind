namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_purpose
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? q1 { get; set; }

        public int? q2 { get; set; }

        public int? q3 { get; set; }

        public int? q4 { get; set; }

        public int? q5 { get; set; }

        public int? q6 { get; set; }

        public int? q7 { get; set; }

        public int? q8 { get; set; }

        public int? q9 { get; set; }

        public int? q10 { get; set; }

        public int? q11 { get; set; }

        public int? q12 { get; set; }

        public int? q13 { get; set; }

        public int? q14 { get; set; }

        public int? q15 { get; set; }

        public int? q16 { get; set; }

        public int? q17 { get; set; }

        public int? q18 { get; set; }

        public int? q19 { get; set; }

        public int? q20 { get; set; }
    }
}
