namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class client_assets
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(10)]
        public string whichClient { get; set; }

        public int? grossIncome { get; set; }

        public int? emergencyAmount { get; set; }

        [StringLength(255)]
        public string feelings { get; set; }
    }
}
