namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class probate_utilities
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string utilityType { get; set; }

        [StringLength(255)]
        public string provider { get; set; }

        public string providerAddress { get; set; }

        [StringLength(255)]
        public string providerPostcode { get; set; }

        [StringLength(255)]
        public string providerPhone { get; set; }

        [StringLength(255)]
        public string providerEmail { get; set; }

        [StringLength(255)]
        public string accountNumber { get; set; }

        [StringLength(255)]
        public string meterReading { get; set; }
    }
}
