namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lpa_register
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(100)]
        public string registerType { get; set; }

        [StringLength(100)]
        public string registerNames { get; set; }

        [StringLength(15)]
        public string registerPhone { get; set; }

        public string registerAddress { get; set; }

        [StringLength(100)]
        public string registerEmail { get; set; }

        [StringLength(100)]
        public string corresType { get; set; }

        [StringLength(255)]
        public string corresNames { get; set; }

        [StringLength(255)]
        public string corresPhone { get; set; }

        public string corresAddress { get; set; }

        [StringLength(255)]
        public string corresEmail { get; set; }

        [StringLength(255)]
        public string repeatFlag { get; set; }

        [StringLength(255)]
        public string byPost { get; set; }

        [StringLength(255)]
        public string byEmail { get; set; }

        [StringLength(255)]
        public string byPhone { get; set; }

        [StringLength(255)]
        public string Welsh { get; set; }

        [StringLength(255)]
        public string registerTitle { get; set; }

        [StringLength(255)]
        public string registerSurname { get; set; }

        [StringLength(255)]
        public string registerPostcode { get; set; }

        [StringLength(255)]
        public string corresTitle { get; set; }

        [StringLength(255)]
        public string corresSurname { get; set; }

        [StringLength(255)]
        public string corresPostcode { get; set; }

        [StringLength(255)]
        public string witnessTitle { get; set; }

        [StringLength(255)]
        public string witnessNames { get; set; }

        [StringLength(255)]
        public string witnessSurname { get; set; }

        public string witnessAddress { get; set; }

        [StringLength(255)]
        public string witnessPostcode { get; set; }

        [StringLength(255)]
        public string witnessPhone { get; set; }

        [StringLength(255)]
        public string witnessEmail { get; set; }
    }
}
