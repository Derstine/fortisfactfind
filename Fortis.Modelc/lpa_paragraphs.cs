namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lpa_paragraphs
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        [StringLength(255)]
        public string lpaType { get; set; }

        public string paragraph { get; set; }

        [StringLength(255)]
        public string paragraphType { get; set; }
    }
}
