namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_compliance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public string notes { get; set; }

        [StringLength(200)]
        public string recording { get; set; }

        public string q1 { get; set; }

        public string q2 { get; set; }

        public string q3 { get; set; }

        public string q4 { get; set; }

        public string q5 { get; set; }

        public string q6 { get; set; }

        public string q7 { get; set; }

        public string q8 { get; set; }

        public string q9 { get; set; }

        public string q10 { get; set; }

        public string q11 { get; set; }

        public string q12 { get; set; }

        public string q13 { get; set; }

        public string q14 { get; set; }

        public string q15 { get; set; }

        public string q16 { get; set; }

        public string q17 { get; set; }

        public string q18 { get; set; }

        public string q19 { get; set; }

        public string q20 { get; set; }

        public int? q1tick { get; set; }

        public int? q2tick { get; set; }

        public int? q3tick { get; set; }

        public int? q4tick { get; set; }

        public int? q5tick { get; set; }

        public int? q6tick { get; set; }

        public int? q7tick { get; set; }

        public int? q8tick { get; set; }

        public int? q9tick { get; set; }

        public int? q10tick { get; set; }

        public int? q11tick { get; set; }

        public int? q12tick { get; set; }

        public int? q13tick { get; set; }

        public int? q14tick { get; set; }

        public int? q15tick { get; set; }

        public int? q16tick { get; set; }

        public int? q17tick { get; set; }

        public int? q18tick { get; set; }

        public int? q19tick { get; set; }

        public int? q20tick { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }
    }
}
