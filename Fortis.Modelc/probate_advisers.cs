namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class probate_advisers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string forenames { get; set; }

        [StringLength(255)]
        public string surname { get; set; }

        public string address { get; set; }

        [StringLength(255)]
        public string postcode { get; set; }

        [StringLength(255)]
        public string phone { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [StringLength(255)]
        public string relationship { get; set; }

        [StringLength(255)]
        public string adviserType { get; set; }
    }
}
