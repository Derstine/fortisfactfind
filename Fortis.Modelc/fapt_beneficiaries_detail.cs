namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_beneficiaries_detail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? trustSequence { get; set; }

        [StringLength(100)]
        public string beneficiaryType { get; set; }

        [StringLength(100)]
        public string beneficiaryName { get; set; }

        [StringLength(10)]
        public string lapseOrIssue { get; set; }

        public float? percentage { get; set; }

        public int? atWhatAge { get; set; }

        public int? atWhatAgeIssue { get; set; }

        [StringLength(1)]
        public string primaryBeneficiaryFlag { get; set; }

        [StringLength(255)]
        public string relationshipClient1 { get; set; }

        [StringLength(255)]
        public string relationshipClient2 { get; set; }

        public DateTime? beneficiaryDOB { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string beneficiaryTitle { get; set; }

        [StringLength(255)]
        public string beneficiaryForenames { get; set; }

        [StringLength(255)]
        public string beneficiarySurname { get; set; }
    }
}
