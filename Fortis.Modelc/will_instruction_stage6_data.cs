namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage6_data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(20)]
        public string funeralClient1Type { get; set; }

        public string funeralClient1Wishes { get; set; }

        [StringLength(20)]
        public string funeralClient2Type { get; set; }

        public string funeralClient2Wishes { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string funeralClient1Plan { get; set; }

        [StringLength(255)]
        public string funeralClient2Plan { get; set; }
    }
}
