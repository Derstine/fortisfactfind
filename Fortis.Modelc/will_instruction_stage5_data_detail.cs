namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage5_data_detail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(20)]
        public string guardianTitle { get; set; }

        [StringLength(50)]
        public string guardianSurname { get; set; }

        [StringLength(100)]
        public string guardianNames { get; set; }

        [StringLength(20)]
        public string guardianType { get; set; }

        public string guardianAddress { get; set; }

        [StringLength(20)]
        public string guardianPhone { get; set; }

        [StringLength(100)]
        public string guardianEmail { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string guardianPostcode { get; set; }

        [StringLength(50)]
        public string relationToClient1 { get; set; }

        [StringLength(50)]
        public string relationToClient2 { get; set; }

        [StringLength(20)]
        public string guardianDOB { get; set; }
    }
}
