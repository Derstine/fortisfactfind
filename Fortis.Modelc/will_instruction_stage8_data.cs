namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage8_data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(100)]
        public string trustType { get; set; }

        [StringLength(1)]
        public string naConfirmationFlag { get; set; }

        [StringLength(20)]
        public string FLITtype { get; set; }

        [StringLength(100)]
        public string FLITname { get; set; }

        [StringLength(30)]
        public string r2rFlag { get; set; }

        public int? r2rYears { get; set; }

        public string r2rTime { get; set; }

        public string r2rLocation { get; set; }

        [StringLength(100)]
        public string r2rWhoTo { get; set; }

        [StringLength(200)]
        public string r2rNamed { get; set; }

        [StringLength(50)]
        public string r2rBenefits { get; set; }

        [StringLength(15)]
        public string r2rMortgage { get; set; }

        [StringLength(15)]
        public string r2rFurniture { get; set; }

        [StringLength(50)]
        public string r2rBeneficialShare { get; set; }

        [StringLength(15)]
        public string r2rCeaseRemarry { get; set; }

        [StringLength(20)]
        public string dtEstateType { get; set; }

        public string dtEstate { get; set; }

        public string dtWishes { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(60)]
        public string backstopCharityName { get; set; }

        [StringLength(20)]
        public string backstopCharityNumber { get; set; }
    }
}
