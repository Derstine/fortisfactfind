namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage4_data_detail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(50)]
        public string childSurname { get; set; }

        [StringLength(100)]
        public string childForenames { get; set; }

        [StringLength(50)]
        public string relationToClient1 { get; set; }

        [StringLength(50)]
        public string relationToClient2 { get; set; }

        public string address { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string childTitle { get; set; }

        [StringLength(255)]
        public string childPostcode { get; set; }

        [StringLength(20)]
        public string childDOB { get; set; }

        [StringLength(255)]
        public string childEmail { get; set; }

        [StringLength(255)]
        public string childPhone { get; set; }
    }
}
