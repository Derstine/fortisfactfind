namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lpa_certificate_provider_details
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(100)]
        public string certificateType { get; set; }

        [StringLength(100)]
        public string certificateNames { get; set; }

        [StringLength(15)]
        public string certificatePhone { get; set; }

        [StringLength(200)]
        public string certificateRelationshipClient1 { get; set; }

        [StringLength(200)]
        public string certificateRelationshipClient2 { get; set; }

        public string certificateAddress { get; set; }

        [StringLength(100)]
        public string certificateEmail { get; set; }

        [StringLength(255)]
        public string howToAct { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string occupation { get; set; }

        [StringLength(255)]
        public string certificateTitle { get; set; }

        [StringLength(255)]
        public string certificateSurname { get; set; }

        [StringLength(255)]
        public string certificatePostcode { get; set; }
    }
}
