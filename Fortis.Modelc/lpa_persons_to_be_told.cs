namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lpa_persons_to_be_told
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(100)]
        public string personType { get; set; }

        [StringLength(100)]
        public string personNames { get; set; }

        [StringLength(15)]
        public string personPhone { get; set; }

        [StringLength(200)]
        public string personRelationshipClient1 { get; set; }

        [StringLength(200)]
        public string personRelationshipClient2 { get; set; }

        public string personAddress { get; set; }

        [StringLength(100)]
        public string personEmail { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string personTitle { get; set; }

        [StringLength(255)]
        public string personSurname { get; set; }

        [StringLength(255)]
        public string personPostcode { get; set; }
    }
}
