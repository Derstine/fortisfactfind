namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage3_data_detail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(100)]
        public string executorType { get; set; }

        [StringLength(100)]
        public string executorNames { get; set; }

        [StringLength(15)]
        public string executorPhone { get; set; }

        [StringLength(200)]
        public string executorRelationshipClient1 { get; set; }

        [StringLength(200)]
        public string executorRelationshipClient2 { get; set; }

        public string executorAddress { get; set; }

        [StringLength(100)]
        public string executorEmail { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string executorTitle { get; set; }

        [StringLength(255)]
        public string executorSurname { get; set; }

        [StringLength(255)]
        public string executorPostcode { get; set; }

        [StringLength(20)]
        public string trusteeFlag { get; set; }

        [StringLength(20)]
        public string executorDOB { get; set; }
    }
}
