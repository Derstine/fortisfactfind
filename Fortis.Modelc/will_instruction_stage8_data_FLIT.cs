namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage8_data_FLIT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? trustSequence { get; set; }

        [StringLength(100)]
        public string beneficiaryType { get; set; }

        [StringLength(20)]
        public string firstOrDefault { get; set; }

        [StringLength(200)]
        public string beneficiaryName { get; set; }

        [StringLength(10)]
        public string lapseOrIssue { get; set; }

        public int? atWhatAge { get; set; }

        public int? atWhatAgeIssue { get; set; }

        public int? percentage { get; set; }

        [StringLength(1)]
        public string primaryBeneficiaryFlag { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string relationToClient1 { get; set; }

        [StringLength(255)]
        public string relationToClient2 { get; set; }

        [StringLength(255)]
        public string beneficiaryTitle { get; set; }

        [StringLength(255)]
        public string beneficiaryForenames { get; set; }

        [StringLength(255)]
        public string beneficiarySurname { get; set; }

        public string beneficiaryAddress { get; set; }

        [StringLength(255)]
        public string beneficiaryPostcode { get; set; }

        [StringLength(255)]
        public string beneficiaryEmail { get; set; }

        [StringLength(255)]
        public string beneficiaryPhone { get; set; }
    }
}
