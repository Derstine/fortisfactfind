namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class electronicFiles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string filename { get; set; }

        [StringLength(255)]
        public string associateWith { get; set; }
    }
}
