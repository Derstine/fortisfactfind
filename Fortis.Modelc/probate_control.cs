using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fortis.Modelc
{
    public class probate_control
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)] public string whichClient { get; set; }

        public string deathAddress { get; set; }

        [StringLength(255)] public string deathPostcode { get; set; }

        [StringLength(255)] public string nursingHomeFlag { get; set; }

        public DateTime? deathDate { get; set; }

        [StringLength(255)] public string willFlag { get; set; }

        public DateTime? willDate { get; set; }

        public string attendanceNotes { get; set; }

        [StringLength(255)] public string assetValue { get; set; }

        [StringLength(255)] public string liabilitiesValue { get; set; }

        [StringLength(255)] public string netAssetValue { get; set; }

        [StringLength(255)] public string probateFee { get; set; }

        [StringLength(255)] public string probateCost { get; set; }

        [StringLength(255)] public string giftsGivenFlag { get; set; }
    }
}