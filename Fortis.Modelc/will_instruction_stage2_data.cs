using System;

namespace Fortis.Modelc
{
    public class will_instruction_stage2_data
    {
        public int? willClientSequence { get; set; }

        public string clientType { get; set; }

        public string clientView { get; set; }

        public DateTime? instructionTimestamp { get; set; }

        public string clientName { get; set; }

        public string storageFlag { get; set; }

        public string postOrDeliver { get; set; }

        public string introducer { get; set; }

        public string whiteLabelSequence { get; set; }

        public string consultant { get; set; }

        public string licenceeSequence { get; set; }

        public string instructionTakersNotes { get; set; }

        public string person1Title { get; set; }

        public string person1Surname { get; set; }

        public string person1Forenames { get; set; }

        public string person1AKA { get; set; }

        public string person1Address { get; set; }

        public string person1MaritalStatus { get; set; }

        public DateTime? person1DOB { get; set; }

        public string person1HomeNumber { get; set; }

        public string person1WorkNumber { get; set; }

        public string person1MobileNumber { get; set; }

        public string person1Email { get; set; }

        public string person2Title { get; set; }

        public string person2Surname { get; set; }

        public string person2Forenames { get; set; }

        public string person2AKA { get; set; }

        public string person2Address { get; set; }

        public string person2MaritalStatus { get; set; }

        public DateTime? person2DOB { get; set; }

        public string person2HomeNumber { get; set; }

        public string person2WorkNumber { get; set; }

        public string person2MobileNumber { get; set; }

        public string person2Email { get; set; }

        public string specialNeedsFlag { get; set; }

        public string specialNeedsDetails { get; set; }

        public string stepChildrenFlag { get; set; }

        public string stepChildrenDetails { get; set; }

        public string stepGrandChildrenFlag { get; set; }

        public string stepGrandChildrenDetails { get; set; }

        public string willInAnotherCountryFlag { get; set; }

        public string willInAnotherCountryDetail { get; set; }

        public string futureSpouseFlag { get; set; }

        public string futureSpouseName { get; set; }

        public string spouseSoleExecutorFlag { get; set; }

        public string childrenToBeExecutors { get; set; }

        public string spouseActWithChildren { get; set; }

        public string spouseActWithOthers { get; set; }

        public string nameSoleExecutorFlag { get; set; }

        public string nameSoleExecutor { get; set; }

        public string namedExecutorWorkWithOthers { get; set; }

        public string willWritingStatus { get; set; }

        public string whoHasWill { get; set; }

        public string willCheckedBy { get; set; }

        public double? introducerAmount { get; set; }

        public double? introducerPercentage { get; set; }

        public string whoHasLPA { get; set; }

        public string lpaCheckedBy { get; set; }

        public string passedToBB { get; set; }

        public double? caseValue { get; set; }

        public string expatFlag { get; set; }

        public string expatDetail { get; set; }

        public string worldwideAssetsFlag { get; set; }

        public string person1Occupation { get; set; }

        public string person2Occupation { get; set; }

        public string person1NINumber { get; set; }

        public string person2NINumber { get; set; }

        public string person1Postcode { get; set; }

        public string person2Postcode { get; set; }

        public string person1Nationality { get; set; }

        public string person2Nationality { get; set; }

        public string person1Residence { get; set; }

        public string person2Residence { get; set; }

        public string person1Domicile { get; set; }

        public string person2Domicile { get; set; }

        public string clientReferenceNumber { get; set; }

        public string miscNotes { get; set; }

    }
}