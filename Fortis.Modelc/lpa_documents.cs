namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class lpa_documents
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string client1PFA { get; set; }

        [StringLength(255)]
        public string client2PFA { get; set; }

        [StringLength(255)]
        public string mirrorPFA { get; set; }

        [StringLength(255)]
        public string client1HW { get; set; }

        [StringLength(255)]
        public string client2HW { get; set; }

        [StringLength(255)]
        public string mirrorHW { get; set; }

        [StringLength(100)]
        public string status { get; set; }

        public DateTime? statusDate { get; set; }

        [StringLength(255)]
        public string client1B { get; set; }

        [StringLength(255)]
        public string client2B { get; set; }

        [StringLength(255)]
        public string mirrorB { get; set; }
    }
}
