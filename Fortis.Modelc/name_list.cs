namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class name_list
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string forenames { get; set; }

        [StringLength(255)]
        public string surname { get; set; }

        [StringLength(255)]
        public string fullname { get; set; }

        public string address { get; set; }

        [StringLength(255)]
        public string phone { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [StringLength(255)]
        public string occupation { get; set; }

        [StringLength(255)]
        public string postcode { get; set; }

        public DateTime? dob { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string relationshipClient1 { get; set; }

        [StringLength(255)]
        public string relationshipClient2 { get; set; }
    }
}
