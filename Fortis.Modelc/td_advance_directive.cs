namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class td_advance_directive
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public string valueStatement { get; set; }

        [StringLength(1)]
        public string option1 { get; set; }

        [StringLength(1)]
        public string option2 { get; set; }

        [StringLength(1)]
        public string option3 { get; set; }

        [StringLength(1)]
        public string option4 { get; set; }

        [StringLength(1)]
        public string option5 { get; set; }

        [StringLength(1)]
        public string option6 { get; set; }

        [StringLength(1)]
        public string option7 { get; set; }

        [StringLength(1)]
        public string option8 { get; set; }

        [StringLength(1)]
        public string option9 { get; set; }

        [StringLength(1)]
        public string option10 { get; set; }

        [StringLength(1)]
        public string option11 { get; set; }

        [StringLength(1)]
        public string option12 { get; set; }

        [StringLength(1)]
        public string option13 { get; set; }

        [StringLength(1)]
        public string option14 { get; set; }

        [StringLength(1)]
        public string option15 { get; set; }

        [StringLength(1)]
        public string option16 { get; set; }

        [StringLength(1)]
        public string option17 { get; set; }

        [StringLength(1)]
        public string option18 { get; set; }

        [StringLength(1)]
        public string option19 { get; set; }

        [StringLength(1)]
        public string option20 { get; set; }

        public string additionalText1 { get; set; }

        public string additionalText2 { get; set; }

        public string additionalText3 { get; set; }

        public string additionalText4 { get; set; }

        public string additionalText5 { get; set; }

        [StringLength(50)]
        public string whichClient { get; set; }
    }
}
