namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class client_products
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(10)]
        public string whichClient { get; set; }

        [StringLength(1)]
        public string q1 { get; set; }

        [StringLength(1)]
        public string q2 { get; set; }

        [StringLength(1)]
        public string q3 { get; set; }

        [StringLength(1)]
        public string q4 { get; set; }

        [StringLength(1)]
        public string q5 { get; set; }

        [StringLength(1)]
        public string q6 { get; set; }

        [StringLength(1)]
        public string q7 { get; set; }

        [StringLength(1)]
        public string q8 { get; set; }

        [StringLength(1)]
        public string q9 { get; set; }

        [StringLength(1)]
        public string q10 { get; set; }

        [StringLength(1)]
        public string q11 { get; set; }

        [StringLength(1)]
        public string q12 { get; set; }

        [StringLength(1)]
        public string q13 { get; set; }

        [StringLength(1)]
        public string q14 { get; set; }

        [StringLength(1)]
        public string q15 { get; set; }
    }
}
