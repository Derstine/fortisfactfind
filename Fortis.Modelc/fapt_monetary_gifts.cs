namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_monetary_gifts
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(10)]
        public string giftFrom { get; set; }

        [StringLength(1)]
        public string giftSharedFlag { get; set; }

        [StringLength(40)]
        public string typeOfGift { get; set; }

        public string itemDetails { get; set; }

        [StringLength(1)]
        public string taxFree { get; set; }

        [StringLength(1)]
        public string soleJointGiftFlag { get; set; }

        [StringLength(1)]
        public string firstSecondDeathFlag { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }
    }
}
