namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class client_wizard
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string manualOverride { get; set; }

        [StringLength(255)]
        public string willReviewFlag { get; set; }

        [StringLength(255)]
        public string willPPTFlag { get; set; }

        [StringLength(255)]
        public string willBasicFlag { get; set; }

        [StringLength(255)]
        public string lpaHWFlag { get; set; }

        [StringLength(255)]
        public string lpaPFAFlag { get; set; }

        [StringLength(255)]
        public string faptFlag { get; set; }

        [StringLength(255)]
        public string probateFlag { get; set; }

        [StringLength(255)]
        public string otherFlag { get; set; }

        [StringLength(255)]
        public string otherPrice { get; set; }

        [StringLength(255)]
        public string otherDescription { get; set; }

        [StringLength(255)]
        public string manHWOPG { get; set; }

        [StringLength(255)]
        public string manPFAOPG { get; set; }

        [StringLength(5)]
        public string followUpFlag { get; set; }

        public DateTime? followUpAppointmentDate { get; set; }

        public string followUpNotes { get; set; }

        [StringLength(5)]
        public string rec_willReviewFlag { get; set; }

        [StringLength(5)]
        public string rec_willPPTFlag { get; set; }

        [StringLength(5)]
        public string rec_willBasicFlag { get; set; }

        [StringLength(5)]
        public string rec_lpaHWFlag { get; set; }

        [StringLength(5)]
        public string rec_lpaPFAFlag { get; set; }

        [StringLength(5)]
        public string rec_faptFlag { get; set; }

        [StringLength(5)]
        public string rec_probateFlag { get; set; }

        [StringLength(5)]
        public string rec_storageFlag { get; set; }

        [StringLength(5)]
        public string storageFlag { get; set; }
    }
}
