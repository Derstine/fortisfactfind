namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage0_data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? consultantSequence { get; set; }

        public int? consultantCompany { get; set; }

        [StringLength(100)]
        public string status { get; set; }

        public DateTime? statusDate { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(255)]
        public string parentFlag { get; set; }

        public int? linkedSequence { get; set; }

        public int? serverSequence { get; set; }

        [StringLength(255)]
        public string misc1 { get; set; }

        [StringLength(255)]
        public string misc2 { get; set; }

        [StringLength(255)]
        public string misc3 { get; set; }

        [StringLength(255)]
        public string misc4 { get; set; }

        [StringLength(255)]
        public string misc5 { get; set; }

        [StringLength(255)]
        public string misc6 { get; set; }

        [StringLength(255)]
        public string misc7 { get; set; }

        [StringLength(255)]
        public string misc8 { get; set; }

        [StringLength(255)]
        public string misc9 { get; set; }

        [StringLength(255)]
        public string misc10 { get; set; }

        [StringLength(20)]
        public string TDProduct { get; set; }
    }
}
