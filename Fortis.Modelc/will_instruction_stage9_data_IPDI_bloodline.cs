namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage9_data_IPDI_bloodline
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? residueSequence { get; set; }

        [StringLength(100)]
        public string beneficiaryType { get; set; }

        public int? atWhatAge { get; set; }

        public int? atWhatAgeIssue { get; set; }

        [StringLength(10)]
        public string lapseOrIssue { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }
    }
}
