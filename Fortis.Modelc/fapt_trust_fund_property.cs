namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fapt_trust_fund_property
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public string address { get; set; }

        public double? worth { get; set; }

        [StringLength(50)]
        public string registeredFlag { get; set; }

        public string owner { get; set; }

        [StringLength(1)]
        public string mortgageFlag { get; set; }

        public double? mortgageValue { get; set; }

        [StringLength(20)]
        public string leaseOrFreehold { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string assetCategory { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        [StringLength(1)]
        public string includeInTrust { get; set; }
    }
}
