namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class client_offer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(10)]
        public string whichClient { get; set; }

        public int? willsPrice { get; set; }

        [StringLength(1)]
        public string willsFlag { get; set; }

        public int? HWLPAPrice { get; set; }

        [StringLength(1)]
        public string HWLPAFlag { get; set; }

        public int? PFLPAPrice { get; set; }

        [StringLength(1)]
        public string PFLPAFlag { get; set; }

        public int? TrustsPrice { get; set; }

        [StringLength(1)]
        public string TrustsFlag { get; set; }

        public int? FuneralPlanPrice { get; set; }

        [StringLength(1)]
        public string FuneralPlanFlag { get; set; }

        public int? ProbatePrice { get; set; }

        [StringLength(1)]
        public string ProbateFlag { get; set; }
    }
}
