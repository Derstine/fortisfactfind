namespace Fortis.Modelc
{
    using System;

    public  class user_activity
    {
        public int ID { get; set; }
        public int clientID { get; set; }
        public DateTime activityTimestamp { get; set; }
        public string activity { get; set; }
        public string activityDetail { get; set; }
    }
}
