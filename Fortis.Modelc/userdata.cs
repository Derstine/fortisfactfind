namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("userdata")]
    public partial class userdata
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        [StringLength(255)]
        public string title { get; set; }

        [StringLength(255)]
        public string forename { get; set; }

        [StringLength(255)]
        public string surname { get; set; }

        public string address { get; set; }

        [StringLength(255)]
        public string postcode { get; set; }

        [StringLength(255)]
        public string company { get; set; }

        [StringLength(255)]
        public string phone { get; set; }

        [StringLength(255)]
        public string mobile { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        public DateTime? userDOB { get; set; }

        [StringLength(255)]
        public string lpaTitle { get; set; }

        [StringLength(255)]
        public string lpaForename { get; set; }

        [StringLength(255)]
        public string lpaSurname { get; set; }

        public string lpaAddress { get; set; }

        [StringLength(255)]
        public string lpaPostcode { get; set; }

        [StringLength(255)]
        public string misc1 { get; set; }

        [StringLength(255)]
        public string misc2 { get; set; }

        [StringLength(255)]
        public string misc3 { get; set; }

        [StringLength(255)]
        public string misc4 { get; set; }

        [StringLength(255)]
        public string misc5 { get; set; }

        [StringLength(255)]
        public string misc6 { get; set; }

        [StringLength(255)]
        public string misc7 { get; set; }

        [StringLength(255)]
        public string misc8 { get; set; }

        [StringLength(255)]
        public string misc9 { get; set; }

        [StringLength(255)]
        public string misc10 { get; set; }

        [StringLength(255)]
        public string username { get; set; }

        [StringLength(255)]
        public string apikey { get; set; }

        [StringLength(255)]
        public string witnessTitle { get; set; }

        [StringLength(255)]
        public string witnessForename { get; set; }

        [StringLength(255)]
        public string witnessSurname { get; set; }

        public string witnessAddress { get; set; }

        [StringLength(255)]
        public string witnessPostcode { get; set; }

        [StringLength(255)]
        public string witnessEmail { get; set; }

        [StringLength(255)]
        public string witnessPhone { get; set; }

        [StringLength(255)]
        public string witnessOccupation { get; set; }

        public DateTime? witnessDOB { get; set; }

        [StringLength(1)]
        public string faptReadyFlag { get; set; }

        [StringLength(1)]
        public string sendDatabaseFlag { get; set; }

        public string HWcouldText { get; set; }

        public string HWshouldText { get; set; }

        public string PFcouldText { get; set; }

        public string PFshouldText { get; set; }

        [StringLength(1)]
        public string internationalWillFlag { get; set; }
    }
}
