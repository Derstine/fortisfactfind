namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage9_data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        public int? residueLevel { get; set; }

        [StringLength(100)]
        public string residueType { get; set; }

        public string bespokeText { get; set; }

        [StringLength(20)]
        public string dtEstateType { get; set; }

        public string dtEstate { get; set; }

        public string dtWishes { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }

        [StringLength(100)]
        public string r2rWhoTo { get; set; }

        [StringLength(200)]
        public string r2rNamed { get; set; }

        [StringLength(50)]
        public string r2rBenefits { get; set; }

        [StringLength(50)]
        public string r2rBeneficialShare { get; set; }

        [StringLength(255)]
        public string r2Occupy { get; set; }

        [StringLength(60)]
        public string backstopCharityName { get; set; }

        [StringLength(20)]
        public string backstopCharityNumber { get; set; }
    }
}
