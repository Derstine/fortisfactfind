namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class probate_assets
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        public string details { get; set; }

        [StringLength(255)]
        public string worth { get; set; }

        [StringLength(255)]
        public string assetType { get; set; }
    }
}
