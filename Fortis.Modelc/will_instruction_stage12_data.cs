namespace Fortis.Modelc
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class will_instruction_stage12_data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sequence { get; set; }

        public int? willClientSequence { get; set; }

        [StringLength(255)]
        public string whichClient { get; set; }

        public decimal? mainResidence { get; set; }

        public decimal? otherProperties { get; set; }

        public decimal? overseasProperties { get; set; }

        public decimal? bankAccounts { get; set; }

        public decimal? investments { get; set; }

        public decimal? lifePolicies { get; set; }

        public decimal? pension { get; set; }

        [StringLength(255)]
        public string businessAssets { get; set; }

        [StringLength(1)]
        public string businessType { get; set; }

        public decimal? businessValue { get; set; }

        public decimal? partnershipAgreement { get; set; }

        public decimal? shares { get; set; }

        [StringLength(1)]
        public string otherPartnersFlag { get; set; }

        [StringLength(200)]
        public string natureOfBusiness { get; set; }

        public decimal? totalBusinessAssets { get; set; }

        public decimal? broughtForward { get; set; }

        public decimal? mortgages { get; set; }

        public decimal? loans { get; set; }

        public decimal? liabilities { get; set; }

        public string additionalInformation { get; set; }

        [StringLength(255)]
        public string whichDocument { get; set; }
    }
}
