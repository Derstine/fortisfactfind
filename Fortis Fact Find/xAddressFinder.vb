﻿Public Class xAddressFinder

    Public Property FoundAddress As String
    Public Property FoundPostcode As String

    Private Sub xAddressFinder_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)
        If searchPostcode = "" Then
            Me.txtPostcode.Text = ""
        Else
            Me.txtPostcode.Text = searchPostcode
        End If

        Me.ComboBuilding.Properties.Items.Clear()
    End Sub

    Private Sub ButtonFindAddress_Click(sender As Object, e As EventArgs) Handles ButtonFindAddress.Click
          Me.txtHidden.Text = ""
        'need to see if there is an internet connection
        If My.Computer.Network.Ping("8.8.8.8") Then
            If Me.txtPostcode.Text = "" Then
                MsgBox("You must enter a postcode")
            Else
                'just in case someone adds lots of postcodes, then we need to clear the list
                Try
                    Me.ComboBuilding.Properties .Items.Clear()
                    ComboBuilding.Properties.Items.Add("Select...")

                    Dim address_string As String = Nothing
                    Dim address_string2 As String = Nothing
                    Dim address_string3 As String = Nothing
                    Dim address_string4 As String = Nothing
                    Dim address_town As String = Nothing
                    Dim address_county As String = Nothing
                    Dim address_premisedata As String = Nothing
                    Dim lookup As New PostcodeLookup.Lookup()
                    Dim address As New PostcodeLookup.Address()
                    'replace "accountid", "password", "postcode"
                    address = lookup.getAddress("3828", "dtqkjt48", Me.txtPostcode.Text)
                    'Put address properties into variables
                    address_string = address.Address1
                    address_string2 = address.Address2
                    address_string3 = address.Address3
                    address_string4 = address.Address4
                    address_town = address.Town
                    address_county = address.County

                    'format postcode correctly
                    Me.txtPostcode.Text = address.Postcode

                    'populate hidden text box field
                    If address_string <> "" Then Me.txtHidden.Text = Me.txtHidden.Text + address_string + vbCrLf
                    If address_string2 <> "" Then Me.txtHidden.Text = Me.txtHidden.Text + address_string2 + vbCrLf
                    If address_string3 <> "" Then Me.txtHidden.Text = Me.txtHidden.Text + address_string3 + vbCrLf
                    If address_string4 <> "" Then Me.txtHidden.Text = Me.txtHidden.Text + address_string4 + vbCrLf
                    If address_town <> "" Then Me.txtHidden.Text = Me.txtHidden.Text + address_town + vbCrLf
                    If address_county <> "" Then Me.txtHidden.Text = Me.txtHidden.Text + address_county + vbCrLf

                    'Put Premise into string variable (requires splitting)
                    address_premisedata = address.PremiseData

                    Dim value As String = address.PremiseData
                    Dim delimiter As Char = ";"
                    Dim substrings() As String = value.Split(delimiter)
                    For Each substring In substrings
                        substring = substring.Replace("|", "")
                        ComboBuilding.Properties .Items.Add(substring)
                    Next

                    ComboBuilding.SelectedIndex = 0
                    Me.ComboBuilding.Enabled = True
                    Me.btnSave.Enabled = True
                    Me.Refresh()
                Catch ex As Exception
                    MsgBox("Cannot Find Postcode")
                End Try
            End If
        Else
            MsgBox("Cannot Find Internet Connection")
        End If
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        FoundAddress = ComboBuilding.SelectedItem.ToString + " " + txtHidden.Text
        FoundAddress = FoundAddress.ToUpper
        FoundAddress = FoundAddress.Replace("&", "AND")
        FoundAddress = FoundAddress.Replace("'", "")
        FoundPostcode = Me.txtPostcode.Text.ToUpper
        searchPostcode = "" ' need to reset this field just in case another form calls this page
        Me.Close()
    End Sub
End Class