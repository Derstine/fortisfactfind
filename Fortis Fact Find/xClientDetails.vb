﻿Imports DevExpress.XtraEditors
Imports Fortis.BusinessComponent

Public Class xClientDetails
    Sub New ()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub xClientDetails_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

'        If internetFlag = "Y" Then Me.Button3.Enabled = True Else Me.Button3.Enabled = False
'        If internetFlag = "Y" Then Me.Button4.Enabled = True Else Me.Button4.Enabled = False

        spy("client details screen loaded")
    End Sub


    Function setDefaults()

        Me.CheckEditSingle.Checked = True



        'need to clear some data as it looks like resetForm doesn't work in my test
'        TextEditForenames1.Text = ""
'        TextEditSurname1.Text = ""
'        MemoEditAddress.Text = ""
'        person1Postcode.Text = ""
'        person1HomeNumber.Text = ""
'        person1MobileNumber.Text = ""
'        person1Email.Text = ""
'        person1DOB.Value = Now
'
'        person2Forenames.Text = ""
'        person2Surname.Text = ""
'        person2Address.Text = ""
'        person2Postcode.Text = ""
'        person2HomeNumber.Text = ""
'        person2MobileNumber.Text = ""
'        person2Email.Text = ""
'        person2DOB.Value = Now
        Return True
    End Function

     Function populateScreen()

        If clientID <> "" And clientID <> "0" Then
            'we have some data to popultate
            Dim result As String
            Dim results As Array
            Dim sql As String

            'mirror or single
            sql = "select typeOfWill from will_instruction_stage1_data where willClientSequence=" + clientID
            result = runSQLwithID(sql)
            If result = "single" Then CheckEditSingle.Checked = True Else CheckEditDual.Checked = True

            'personal details
            sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
            results = runSQLwithArray(sql)
         
'            person1Title.SelectedItem = results(12)
'            person1Surname.Text = results(13)
'            person1Forenames.Text = results(14)
'            person1Address.Text = results(16)
'            person1Postcode.Text = results(67)
'            person1MaritalStatus.SelectedItem = results(17)
'            person1DOB.Value = results(18)
'            person1HomeNumber.Text = results(19)
'            person1MobileNumber.Text = results(21)
'            person1Email.Text = results(22)
'            person2Title.SelectedItem = results(23)
'            person2Surname.Text = results(24)
'            person2Forenames.Text = results(25)
'            person2Address.Text = results(27)
'            person2Postcode.Text = results(68)
'            person2MaritalStatus.SelectedItem = results(28)
            ' MsgBox(results(29))
            If results(29) = "" Then
'                person2DOB.Value = Now()
            Else
'                person2DOB.Value = results(29)
            End If

'            person2HomeNumber.Text = results(30)
'            person2MobileNumber.Text = results(32)
'            person2Email.Text = results(33)
'            clientReferenceNumber.Text = results(75)

            'as it's an existing client, we need to change the button text
'            Button1.Text = "Update"
        Else
            'its a new case, not an upate
'            Button1.Text = "Save"
        End If
            Return True
    End Function


    Private Sub SimpleButtonLookup2_Click(sender As Object, e As EventArgs) Handles SimpleButtonLookup2.Click, SimpleButtonLookup1.Click
      Dim addressFinder As New AddressFinder
        addressFinder.Show()
    End Sub

    Private Sub CheckEditMs1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditMs1.CheckedChanged, CheckEditMrs1.CheckedChanged, CheckEditMr1.CheckedChanged, CheckEditMiss1.CheckedChanged
        
        If (DirectCast(sender, CheckEdit).Checked) Then
            For Each control As Object In Me.LayoutControl1.Controls
                Dim checkEdit1 = TryCast(control, CheckEdit)
                Dim checkEdit As CheckEdit = checkEdit1
                If (checkEdit1 IsNot Nothing) Then
                    If (checkEdit.Text <> DirectCast(sender, CheckEdit).Text) Then
                        checkEdit.Checked = False
                    Else 
                        Continue For
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub CheckEditMr2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditMs2.CheckedChanged, CheckEditMrs2.CheckedChanged, CheckEditMr2.CheckedChanged, CheckEditMiss2.CheckedChanged
        If (DirectCast(sender, CheckEdit).Checked) Then
            For Each control As Object In Me.LayoutControl2.Controls
                Dim checkEdit1 = TryCast(control, CheckEdit)
                Dim checkEdit As CheckEdit = checkEdit1
                If (checkEdit1 IsNot Nothing) Then
                    If (checkEdit.Text <> DirectCast(sender, CheckEdit).Text) Then
                        checkEdit.Checked = False
                    Else 
                        Continue For
                    End If
                End If
            Next
        End If
    End Sub
End Class

Public Class AddressFinder

    Public Sub Show()
        If Internet.IsAvailable() Then
            Dim lookup as New xAddressFinder
            lookup.ShowDialog()
        Else 
            XtraMessageBox.Show("You are not connected to the internet, so postcode lookup is not available", "Lookup")
        End If

    End Sub

End Class