﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xFATPAttendanceQ4
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xFATPAttendanceQ4))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Arial", 12!)
        Me.LabelControl1.Appearance.Options.UseFont = true
        Me.LabelControl1.Location = New System.Drawing.Point(33, 21)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(606, 36)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Do you have any disability or physical condition which would require assistance f"& _ 
    "rom any "&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"person while signing the Lasting Power of Attorney?"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(263, 72)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "YES"
        Me.CheckEdit1.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit1.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit1.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit1.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit1.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit1.Size = New System.Drawing.Size(109, 36)
        Me.CheckEdit1.TabIndex = 1
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(378, 72)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Caption = "NO"
        Me.CheckEdit2.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit2.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit2.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit2.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit2.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit2.Size = New System.Drawing.Size(121, 36)
        Me.CheckEdit2.TabIndex = 1
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(33, 143)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(666, 63)
        Me.MemoEdit1.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Arial", 12!)
        Me.LabelControl2.Appearance.Options.UseFont = true
        Me.LabelControl2.Location = New System.Drawing.Point(33, 119)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(164, 18)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "If ""YES"", provide details"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton1.Location = New System.Drawing.Point(33, 222)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(429, 42)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Cancel"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton2.Location = New System.Drawing.Point(468, 222)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(231, 42)
        Me.SimpleButton2.TabIndex = 5
        Me.SimpleButton2.Text = "Cancel"
        '
        'xFATPAttendanceQ4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(759, 291)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.MemoEdit1)
        Me.Controls.Add(Me.CheckEdit2)
        Me.Controls.Add(Me.CheckEdit1)
        Me.Controls.Add(Me.LabelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "xFATPAttendanceQ4"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FAPT Attendance Notes - Q4"
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
End Class
