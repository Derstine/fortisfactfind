﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class zzz_factfind_wizard_version1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.RadioButton45 = New System.Windows.Forms.RadioButton()
        Me.RadioButton46 = New System.Windows.Forms.RadioButton()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.RadioButton21 = New System.Windows.Forms.RadioButton()
        Me.RadioButton22 = New System.Windows.Forms.RadioButton()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.RadioButton23 = New System.Windows.Forms.RadioButton()
        Me.RadioButton24 = New System.Windows.Forms.RadioButton()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.RadioButton25 = New System.Windows.Forms.RadioButton()
        Me.RadioButton26 = New System.Windows.Forms.RadioButton()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.RadioButton27 = New System.Windows.Forms.RadioButton()
        Me.RadioButton28 = New System.Windows.Forms.RadioButton()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.RadioButton29 = New System.Windows.Forms.RadioButton()
        Me.RadioButton30 = New System.Windows.Forms.RadioButton()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.RadioButton31 = New System.Windows.Forms.RadioButton()
        Me.RadioButton32 = New System.Windows.Forms.RadioButton()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.RadioButton33 = New System.Windows.Forms.RadioButton()
        Me.RadioButton34 = New System.Windows.Forms.RadioButton()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.RadioButton35 = New System.Windows.Forms.RadioButton()
        Me.RadioButton36 = New System.Windows.Forms.RadioButton()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.RadioButton37 = New System.Windows.Forms.RadioButton()
        Me.RadioButton38 = New System.Windows.Forms.RadioButton()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.RadioButton39 = New System.Windows.Forms.RadioButton()
        Me.RadioButton40 = New System.Windows.Forms.RadioButton()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.RadioButton43 = New System.Windows.Forms.RadioButton()
        Me.RadioButton44 = New System.Windows.Forms.RadioButton()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.RadioButton41 = New System.Windows.Forms.RadioButton()
        Me.RadioButton42 = New System.Windows.Forms.RadioButton()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.RadioButton15 = New System.Windows.Forms.RadioButton()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.RadioButton19 = New System.Windows.Forms.RadioButton()
        Me.RadioButton20 = New System.Windows.Forms.RadioButton()
        Me.RadioButton16 = New System.Windows.Forms.RadioButton()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.RadioButton13 = New System.Windows.Forms.RadioButton()
        Me.RadioButton14 = New System.Windows.Forms.RadioButton()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.RadioButton17 = New System.Windows.Forms.RadioButton()
        Me.RadioButton18 = New System.Windows.Forms.RadioButton()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.RadioButton11 = New System.Windows.Forms.RadioButton()
        Me.RadioButton12 = New System.Windows.Forms.RadioButton()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.TextBox74 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn2 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBox32 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.TextBox27 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TextBox31 = New System.Windows.Forms.TextBox()
        Me.TextBox28 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.TextBox30 = New System.Windows.Forms.TextBox()
        Me.TextBox29 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.CheckBox10 = New System.Windows.Forms.CheckBox()
        Me.CheckBox11 = New System.Windows.Forms.CheckBox()
        Me.CheckBox12 = New System.Windows.Forms.CheckBox()
        Me.CheckBox13 = New System.Windows.Forms.CheckBox()
        Me.CheckBox14 = New System.Windows.Forms.CheckBox()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.TextBox21 = New System.Windows.Forms.TextBox()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.TextBox23 = New System.Windows.Forms.TextBox()
        Me.TextBox24 = New System.Windows.Forms.TextBox()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.TextBox26 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.TextBox76 = New System.Windows.Forms.TextBox()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.TextBox75 = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.TextBox42 = New System.Windows.Forms.TextBox()
        Me.TextBox41 = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.TextBox40 = New System.Windows.Forms.TextBox()
        Me.TextBox39 = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.TextBox38 = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.TextBox37 = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.TextBox36 = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.TextBox35 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TextBox34 = New System.Windows.Forms.TextBox()
        Me.TextBox33 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.TextBox49 = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TextBox46 = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.TextBox47 = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.TextBox48 = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.TextBox45 = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.TextBox44 = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.TextBox43 = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.TextBox59 = New System.Windows.Forms.TextBox()
        Me.TextBox60 = New System.Windows.Forms.TextBox()
        Me.TextBox61 = New System.Windows.Forms.TextBox()
        Me.TextBox62 = New System.Windows.Forms.TextBox()
        Me.TextBox63 = New System.Windows.Forms.TextBox()
        Me.TextBox64 = New System.Windows.Forms.TextBox()
        Me.TextBox65 = New System.Windows.Forms.TextBox()
        Me.TextBox66 = New System.Windows.Forms.TextBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.TextBox67 = New System.Windows.Forms.TextBox()
        Me.TextBox68 = New System.Windows.Forms.TextBox()
        Me.TextBox69 = New System.Windows.Forms.TextBox()
        Me.TextBox70 = New System.Windows.Forms.TextBox()
        Me.TextBox71 = New System.Windows.Forms.TextBox()
        Me.TextBox72 = New System.Windows.Forms.TextBox()
        Me.TextBox73 = New System.Windows.Forms.TextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.TextBox58 = New System.Windows.Forms.TextBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.TextBox57 = New System.Windows.Forms.TextBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.TextBox56 = New System.Windows.Forms.TextBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.TextBox55 = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.TextBox54 = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.TextBox53 = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.TextBox52 = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.TextBox51 = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.TextBox50 = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel23.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Panel16.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.black
        Me.Button2.Location = New System.Drawing.Point(1120, 602)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(102, 47)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Close"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 66)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.Padding = New System.Drawing.Point(10, 10)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1209, 530)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Location = New System.Drawing.Point(4, 39)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Products && Services"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel23)
        Me.GroupBox2.Controls.Add(Me.Panel11)
        Me.GroupBox2.Controls.Add(Me.Panel12)
        Me.GroupBox2.Controls.Add(Me.Panel13)
        Me.GroupBox2.Controls.Add(Me.Panel14)
        Me.GroupBox2.Controls.Add(Me.Panel15)
        Me.GroupBox2.Controls.Add(Me.Panel16)
        Me.GroupBox2.Controls.Add(Me.Panel17)
        Me.GroupBox2.Controls.Add(Me.Panel18)
        Me.GroupBox2.Controls.Add(Me.Panel19)
        Me.GroupBox2.Controls.Add(Me.Panel20)
        Me.GroupBox2.Location = New System.Drawing.Point(754, 38)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(150, 390)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Client 2"
        '
        'Panel23
        '
        Me.Panel23.Controls.Add(Me.RadioButton45)
        Me.Panel23.Controls.Add(Me.RadioButton46)
        Me.Panel23.Location = New System.Drawing.Point(21, 352)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(108, 22)
        Me.Panel23.TabIndex = 6
        '
        'RadioButton45
        '
        Me.RadioButton45.AutoSize = True
        Me.RadioButton45.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton45.Name = "RadioButton45"
        Me.RadioButton45.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton45.TabIndex = 1
        Me.RadioButton45.TabStop = True
        Me.RadioButton45.Text = "No"
        Me.RadioButton45.UseVisualStyleBackColor = True
        '
        'RadioButton46
        '
        Me.RadioButton46.AutoSize = True
        Me.RadioButton46.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton46.Name = "RadioButton46"
        Me.RadioButton46.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton46.TabIndex = 0
        Me.RadioButton46.TabStop = True
        Me.RadioButton46.Text = "Yes"
        Me.RadioButton46.UseVisualStyleBackColor = True
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.RadioButton21)
        Me.Panel11.Controls.Add(Me.RadioButton22)
        Me.Panel11.Location = New System.Drawing.Point(21, 319)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(108, 22)
        Me.Panel11.TabIndex = 5
        '
        'RadioButton21
        '
        Me.RadioButton21.AutoSize = True
        Me.RadioButton21.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton21.Name = "RadioButton21"
        Me.RadioButton21.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton21.TabIndex = 1
        Me.RadioButton21.TabStop = True
        Me.RadioButton21.Text = "No"
        Me.RadioButton21.UseVisualStyleBackColor = True
        '
        'RadioButton22
        '
        Me.RadioButton22.AutoSize = True
        Me.RadioButton22.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton22.Name = "RadioButton22"
        Me.RadioButton22.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton22.TabIndex = 0
        Me.RadioButton22.TabStop = True
        Me.RadioButton22.Text = "Yes"
        Me.RadioButton22.UseVisualStyleBackColor = True
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.RadioButton23)
        Me.Panel12.Controls.Add(Me.RadioButton24)
        Me.Panel12.Location = New System.Drawing.Point(21, 286)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(108, 22)
        Me.Panel12.TabIndex = 4
        '
        'RadioButton23
        '
        Me.RadioButton23.AutoSize = True
        Me.RadioButton23.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton23.Name = "RadioButton23"
        Me.RadioButton23.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton23.TabIndex = 1
        Me.RadioButton23.TabStop = True
        Me.RadioButton23.Text = "No"
        Me.RadioButton23.UseVisualStyleBackColor = True
        '
        'RadioButton24
        '
        Me.RadioButton24.AutoSize = True
        Me.RadioButton24.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton24.Name = "RadioButton24"
        Me.RadioButton24.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton24.TabIndex = 0
        Me.RadioButton24.TabStop = True
        Me.RadioButton24.Text = "Yes"
        Me.RadioButton24.UseVisualStyleBackColor = True
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.RadioButton25)
        Me.Panel13.Controls.Add(Me.RadioButton26)
        Me.Panel13.Location = New System.Drawing.Point(21, 253)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(108, 22)
        Me.Panel13.TabIndex = 3
        '
        'RadioButton25
        '
        Me.RadioButton25.AutoSize = True
        Me.RadioButton25.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton25.Name = "RadioButton25"
        Me.RadioButton25.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton25.TabIndex = 1
        Me.RadioButton25.TabStop = True
        Me.RadioButton25.Text = "No"
        Me.RadioButton25.UseVisualStyleBackColor = True
        '
        'RadioButton26
        '
        Me.RadioButton26.AutoSize = True
        Me.RadioButton26.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton26.Name = "RadioButton26"
        Me.RadioButton26.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton26.TabIndex = 0
        Me.RadioButton26.TabStop = True
        Me.RadioButton26.Text = "Yes"
        Me.RadioButton26.UseVisualStyleBackColor = True
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.RadioButton27)
        Me.Panel14.Controls.Add(Me.RadioButton28)
        Me.Panel14.Location = New System.Drawing.Point(21, 220)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(108, 22)
        Me.Panel14.TabIndex = 2
        '
        'RadioButton27
        '
        Me.RadioButton27.AutoSize = True
        Me.RadioButton27.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton27.Name = "RadioButton27"
        Me.RadioButton27.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton27.TabIndex = 1
        Me.RadioButton27.TabStop = True
        Me.RadioButton27.Text = "No"
        Me.RadioButton27.UseVisualStyleBackColor = True
        '
        'RadioButton28
        '
        Me.RadioButton28.AutoSize = True
        Me.RadioButton28.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton28.Name = "RadioButton28"
        Me.RadioButton28.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton28.TabIndex = 0
        Me.RadioButton28.TabStop = True
        Me.RadioButton28.Text = "Yes"
        Me.RadioButton28.UseVisualStyleBackColor = True
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.RadioButton29)
        Me.Panel15.Controls.Add(Me.RadioButton30)
        Me.Panel15.Location = New System.Drawing.Point(21, 187)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(108, 22)
        Me.Panel15.TabIndex = 2
        '
        'RadioButton29
        '
        Me.RadioButton29.AutoSize = True
        Me.RadioButton29.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton29.Name = "RadioButton29"
        Me.RadioButton29.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton29.TabIndex = 1
        Me.RadioButton29.TabStop = True
        Me.RadioButton29.Text = "No"
        Me.RadioButton29.UseVisualStyleBackColor = True
        '
        'RadioButton30
        '
        Me.RadioButton30.AutoSize = True
        Me.RadioButton30.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton30.Name = "RadioButton30"
        Me.RadioButton30.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton30.TabIndex = 0
        Me.RadioButton30.TabStop = True
        Me.RadioButton30.Text = "Yes"
        Me.RadioButton30.UseVisualStyleBackColor = True
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.RadioButton31)
        Me.Panel16.Controls.Add(Me.RadioButton32)
        Me.Panel16.Location = New System.Drawing.Point(21, 154)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(108, 22)
        Me.Panel16.TabIndex = 2
        '
        'RadioButton31
        '
        Me.RadioButton31.AutoSize = True
        Me.RadioButton31.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton31.Name = "RadioButton31"
        Me.RadioButton31.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton31.TabIndex = 1
        Me.RadioButton31.TabStop = True
        Me.RadioButton31.Text = "No"
        Me.RadioButton31.UseVisualStyleBackColor = True
        '
        'RadioButton32
        '
        Me.RadioButton32.AutoSize = True
        Me.RadioButton32.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton32.Name = "RadioButton32"
        Me.RadioButton32.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton32.TabIndex = 0
        Me.RadioButton32.TabStop = True
        Me.RadioButton32.Text = "Yes"
        Me.RadioButton32.UseVisualStyleBackColor = True
        '
        'Panel17
        '
        Me.Panel17.Controls.Add(Me.RadioButton33)
        Me.Panel17.Controls.Add(Me.RadioButton34)
        Me.Panel17.Location = New System.Drawing.Point(21, 121)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(108, 22)
        Me.Panel17.TabIndex = 2
        '
        'RadioButton33
        '
        Me.RadioButton33.AutoSize = True
        Me.RadioButton33.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton33.Name = "RadioButton33"
        Me.RadioButton33.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton33.TabIndex = 1
        Me.RadioButton33.TabStop = True
        Me.RadioButton33.Text = "No"
        Me.RadioButton33.UseVisualStyleBackColor = True
        '
        'RadioButton34
        '
        Me.RadioButton34.AutoSize = True
        Me.RadioButton34.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton34.Name = "RadioButton34"
        Me.RadioButton34.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton34.TabIndex = 0
        Me.RadioButton34.TabStop = True
        Me.RadioButton34.Text = "Yes"
        Me.RadioButton34.UseVisualStyleBackColor = True
        '
        'Panel18
        '
        Me.Panel18.Controls.Add(Me.RadioButton35)
        Me.Panel18.Controls.Add(Me.RadioButton36)
        Me.Panel18.Location = New System.Drawing.Point(21, 88)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(108, 22)
        Me.Panel18.TabIndex = 2
        '
        'RadioButton35
        '
        Me.RadioButton35.AutoSize = True
        Me.RadioButton35.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton35.Name = "RadioButton35"
        Me.RadioButton35.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton35.TabIndex = 1
        Me.RadioButton35.TabStop = True
        Me.RadioButton35.Text = "No"
        Me.RadioButton35.UseVisualStyleBackColor = True
        '
        'RadioButton36
        '
        Me.RadioButton36.AutoSize = True
        Me.RadioButton36.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton36.Name = "RadioButton36"
        Me.RadioButton36.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton36.TabIndex = 0
        Me.RadioButton36.TabStop = True
        Me.RadioButton36.Text = "Yes"
        Me.RadioButton36.UseVisualStyleBackColor = True
        '
        'Panel19
        '
        Me.Panel19.Controls.Add(Me.RadioButton37)
        Me.Panel19.Controls.Add(Me.RadioButton38)
        Me.Panel19.Location = New System.Drawing.Point(21, 55)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(108, 22)
        Me.Panel19.TabIndex = 1
        '
        'RadioButton37
        '
        Me.RadioButton37.AutoSize = True
        Me.RadioButton37.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton37.Name = "RadioButton37"
        Me.RadioButton37.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton37.TabIndex = 1
        Me.RadioButton37.TabStop = True
        Me.RadioButton37.Text = "No"
        Me.RadioButton37.UseVisualStyleBackColor = True
        '
        'RadioButton38
        '
        Me.RadioButton38.AutoSize = True
        Me.RadioButton38.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton38.Name = "RadioButton38"
        Me.RadioButton38.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton38.TabIndex = 0
        Me.RadioButton38.TabStop = True
        Me.RadioButton38.Text = "Yes"
        Me.RadioButton38.UseVisualStyleBackColor = True
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.RadioButton39)
        Me.Panel20.Controls.Add(Me.RadioButton40)
        Me.Panel20.Location = New System.Drawing.Point(21, 22)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(108, 22)
        Me.Panel20.TabIndex = 0
        '
        'RadioButton39
        '
        Me.RadioButton39.AutoSize = True
        Me.RadioButton39.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton39.Name = "RadioButton39"
        Me.RadioButton39.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton39.TabIndex = 1
        Me.RadioButton39.TabStop = True
        Me.RadioButton39.Text = "No"
        Me.RadioButton39.UseVisualStyleBackColor = True
        '
        'RadioButton40
        '
        Me.RadioButton40.AutoSize = True
        Me.RadioButton40.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton40.Name = "RadioButton40"
        Me.RadioButton40.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton40.TabIndex = 0
        Me.RadioButton40.TabStop = True
        Me.RadioButton40.Text = "Yes"
        Me.RadioButton40.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(112, 394)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(480, 16)
        Me.Label19.TabIndex = 16
        Me.Label19.Text = "Do you want to protect your assets for your beneficiaries and future generations?" &
    ""
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(135, 361)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(457, 16)
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "Have you set up Trusts to protect your assets from sideways disinheritance?"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(59, 328)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(533, 16)
        Me.Label17.TabIndex = 14
        Me.Label17.Text = "Do you want to take away the burden on loved ones and save thousands by paying no" &
    "w?"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(351, 295)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(241, 16)
        Me.Label16.TabIndex = 13
        Me.Label16.Text = "Have you pre-paid your future probate?"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(222, 262)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(370, 16)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Do you want today's prices or to pay double in 12 years time?"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(364, 229)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(228, 16)
        Me.Label14.TabIndex = 11
        Me.Label14.Text = "Do you have a pre-paid funeral plan?"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(144, 196)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(448, 16)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "Do you want LPA's so your loved ones are able to act if you lose capacity?"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(143, 163)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(449, 16)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Have you organised all Lasting Powers of Attorney for Property && Finance?"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(156, 130)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(436, 16)
        Me.Label11.TabIndex = 8
        Me.Label11.Text = "Have you organised all Lasting Powers of Attorney for Health && Welfare?"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(345, 97)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(247, 16)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Do you want a professionally written will?"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(559, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Have you got well written, satisfactory Will, nominating a professional Trustee a" &
    "s an Executor?"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel22)
        Me.GroupBox1.Controls.Add(Me.Panel21)
        Me.GroupBox1.Controls.Add(Me.Panel8)
        Me.GroupBox1.Controls.Add(Me.Panel7)
        Me.GroupBox1.Controls.Add(Me.Panel9)
        Me.GroupBox1.Controls.Add(Me.Panel6)
        Me.GroupBox1.Controls.Add(Me.Panel5)
        Me.GroupBox1.Controls.Add(Me.Panel4)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Location = New System.Drawing.Point(598, 38)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(150, 390)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Client 1"
        '
        'Panel22
        '
        Me.Panel22.Controls.Add(Me.RadioButton43)
        Me.Panel22.Controls.Add(Me.RadioButton44)
        Me.Panel22.Location = New System.Drawing.Point(21, 352)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(108, 22)
        Me.Panel22.TabIndex = 7
        '
        'RadioButton43
        '
        Me.RadioButton43.AutoSize = True
        Me.RadioButton43.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton43.Name = "RadioButton43"
        Me.RadioButton43.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton43.TabIndex = 1
        Me.RadioButton43.TabStop = True
        Me.RadioButton43.Text = "No"
        Me.RadioButton43.UseVisualStyleBackColor = True
        '
        'RadioButton44
        '
        Me.RadioButton44.AutoSize = True
        Me.RadioButton44.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton44.Name = "RadioButton44"
        Me.RadioButton44.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton44.TabIndex = 0
        Me.RadioButton44.TabStop = True
        Me.RadioButton44.Text = "Yes"
        Me.RadioButton44.UseVisualStyleBackColor = True
        '
        'Panel21
        '
        Me.Panel21.Controls.Add(Me.RadioButton41)
        Me.Panel21.Controls.Add(Me.RadioButton42)
        Me.Panel21.Location = New System.Drawing.Point(21, 319)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(108, 22)
        Me.Panel21.TabIndex = 6
        '
        'RadioButton41
        '
        Me.RadioButton41.AutoSize = True
        Me.RadioButton41.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton41.Name = "RadioButton41"
        Me.RadioButton41.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton41.TabIndex = 1
        Me.RadioButton41.TabStop = True
        Me.RadioButton41.Text = "No"
        Me.RadioButton41.UseVisualStyleBackColor = True
        '
        'RadioButton42
        '
        Me.RadioButton42.AutoSize = True
        Me.RadioButton42.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton42.Name = "RadioButton42"
        Me.RadioButton42.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton42.TabIndex = 0
        Me.RadioButton42.TabStop = True
        Me.RadioButton42.Text = "Yes"
        Me.RadioButton42.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.RadioButton15)
        Me.Panel8.Controls.Add(Me.Panel10)
        Me.Panel8.Controls.Add(Me.RadioButton16)
        Me.Panel8.Location = New System.Drawing.Point(21, 286)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(108, 22)
        Me.Panel8.TabIndex = 4
        '
        'RadioButton15
        '
        Me.RadioButton15.AutoSize = True
        Me.RadioButton15.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton15.Name = "RadioButton15"
        Me.RadioButton15.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton15.TabIndex = 1
        Me.RadioButton15.TabStop = True
        Me.RadioButton15.Text = "No"
        Me.RadioButton15.UseVisualStyleBackColor = True
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.RadioButton19)
        Me.Panel10.Controls.Add(Me.RadioButton20)
        Me.Panel10.Location = New System.Drawing.Point(3, 26)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(108, 20)
        Me.Panel10.TabIndex = 5
        '
        'RadioButton19
        '
        Me.RadioButton19.AutoSize = True
        Me.RadioButton19.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton19.Name = "RadioButton19"
        Me.RadioButton19.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton19.TabIndex = 1
        Me.RadioButton19.TabStop = True
        Me.RadioButton19.Text = "No"
        Me.RadioButton19.UseVisualStyleBackColor = True
        '
        'RadioButton20
        '
        Me.RadioButton20.AutoSize = True
        Me.RadioButton20.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton20.Name = "RadioButton20"
        Me.RadioButton20.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton20.TabIndex = 0
        Me.RadioButton20.TabStop = True
        Me.RadioButton20.Text = "Yes"
        Me.RadioButton20.UseVisualStyleBackColor = True
        '
        'RadioButton16
        '
        Me.RadioButton16.AutoSize = True
        Me.RadioButton16.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton16.Name = "RadioButton16"
        Me.RadioButton16.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton16.TabIndex = 0
        Me.RadioButton16.TabStop = True
        Me.RadioButton16.Text = "Yes"
        Me.RadioButton16.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.RadioButton13)
        Me.Panel7.Controls.Add(Me.RadioButton14)
        Me.Panel7.Location = New System.Drawing.Point(21, 253)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(108, 22)
        Me.Panel7.TabIndex = 3
        '
        'RadioButton13
        '
        Me.RadioButton13.AutoSize = True
        Me.RadioButton13.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton13.Name = "RadioButton13"
        Me.RadioButton13.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton13.TabIndex = 1
        Me.RadioButton13.TabStop = True
        Me.RadioButton13.Text = "No"
        Me.RadioButton13.UseVisualStyleBackColor = True
        '
        'RadioButton14
        '
        Me.RadioButton14.AutoSize = True
        Me.RadioButton14.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton14.Name = "RadioButton14"
        Me.RadioButton14.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton14.TabIndex = 0
        Me.RadioButton14.TabStop = True
        Me.RadioButton14.Text = "Yes"
        Me.RadioButton14.UseVisualStyleBackColor = True
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.RadioButton17)
        Me.Panel9.Controls.Add(Me.RadioButton18)
        Me.Panel9.Location = New System.Drawing.Point(21, 220)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(108, 22)
        Me.Panel9.TabIndex = 2
        '
        'RadioButton17
        '
        Me.RadioButton17.AutoSize = True
        Me.RadioButton17.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton17.Name = "RadioButton17"
        Me.RadioButton17.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton17.TabIndex = 1
        Me.RadioButton17.TabStop = True
        Me.RadioButton17.Text = "No"
        Me.RadioButton17.UseVisualStyleBackColor = True
        '
        'RadioButton18
        '
        Me.RadioButton18.AutoSize = True
        Me.RadioButton18.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton18.Name = "RadioButton18"
        Me.RadioButton18.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton18.TabIndex = 0
        Me.RadioButton18.TabStop = True
        Me.RadioButton18.Text = "Yes"
        Me.RadioButton18.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.RadioButton11)
        Me.Panel6.Controls.Add(Me.RadioButton12)
        Me.Panel6.Location = New System.Drawing.Point(21, 187)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(108, 22)
        Me.Panel6.TabIndex = 2
        '
        'RadioButton11
        '
        Me.RadioButton11.AutoSize = True
        Me.RadioButton11.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton11.TabIndex = 1
        Me.RadioButton11.TabStop = True
        Me.RadioButton11.Text = "No"
        Me.RadioButton11.UseVisualStyleBackColor = True
        '
        'RadioButton12
        '
        Me.RadioButton12.AutoSize = True
        Me.RadioButton12.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton12.TabIndex = 0
        Me.RadioButton12.TabStop = True
        Me.RadioButton12.Text = "Yes"
        Me.RadioButton12.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.RadioButton9)
        Me.Panel5.Controls.Add(Me.RadioButton10)
        Me.Panel5.Location = New System.Drawing.Point(21, 154)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(108, 22)
        Me.Panel5.TabIndex = 2
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = True
        Me.RadioButton9.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton9.TabIndex = 1
        Me.RadioButton9.TabStop = True
        Me.RadioButton9.Text = "No"
        Me.RadioButton9.UseVisualStyleBackColor = True
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton10.TabIndex = 0
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "Yes"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.RadioButton7)
        Me.Panel4.Controls.Add(Me.RadioButton8)
        Me.Panel4.Location = New System.Drawing.Point(21, 121)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(108, 22)
        Me.Panel4.TabIndex = 2
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton7.TabIndex = 1
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "No"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton8.TabIndex = 0
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Yes"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.RadioButton5)
        Me.Panel3.Controls.Add(Me.RadioButton6)
        Me.Panel3.Location = New System.Drawing.Point(21, 88)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(108, 22)
        Me.Panel3.TabIndex = 2
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton5.TabIndex = 1
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "No"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton6.TabIndex = 0
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "Yes"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.RadioButton3)
        Me.Panel2.Controls.Add(Me.RadioButton4)
        Me.Panel2.Location = New System.Drawing.Point(21, 55)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(108, 22)
        Me.Panel2.TabIndex = 1
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton3.TabIndex = 1
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "No"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton4.TabIndex = 0
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Yes"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButton2)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Location = New System.Drawing.Point(21, 22)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(108, 22)
        Me.Panel1.TabIndex = 0
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(59, 3)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(44, 20)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "No"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(50, 20)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Yes"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(151, 20)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Products && Services"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.Label76)
        Me.TabPage2.Controls.Add(Me.TextBox74)
        Me.TabPage2.Controls.Add(Me.TextBox9)
        Me.TabPage2.Controls.Add(Me.TextBox10)
        Me.TabPage2.Controls.Add(Me.TextBox11)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.TextBox7)
        Me.TabPage2.Controls.Add(Me.TextBox8)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.TextBox4)
        Me.TabPage2.Controls.Add(Me.TextBox5)
        Me.TabPage2.Controls.Add(Me.TextBox6)
        Me.TabPage2.Controls.Add(Me.TextBox3)
        Me.TabPage2.Controls.Add(Me.TextBox2)
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.DataGridView2)
        Me.TabPage2.Controls.Add(Me.Button3)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Location = New System.Drawing.Point(4, 39)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Asset Information"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(710, 357)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(15, 16)
        Me.Label76.TabIndex = 59
        Me.Label76.Text = "="
        '
        'TextBox74
        '
        Me.TextBox74.Enabled = False
        Me.TextBox74.Location = New System.Drawing.Point(731, 354)
        Me.TextBox74.Name = "TextBox74"
        Me.TextBox74.Size = New System.Drawing.Size(96, 22)
        Me.TextBox74.TabIndex = 58
        '
        'TextBox9
        '
        Me.TextBox9.Enabled = False
        Me.TextBox9.Location = New System.Drawing.Point(608, 354)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(96, 22)
        Me.TextBox9.TabIndex = 57
        '
        'TextBox10
        '
        Me.TextBox10.Enabled = False
        Me.TextBox10.Location = New System.Drawing.Point(509, 354)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(96, 22)
        Me.TextBox10.TabIndex = 56
        '
        'TextBox11
        '
        Me.TextBox11.Enabled = False
        Me.TextBox11.Location = New System.Drawing.Point(410, 354)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(96, 22)
        Me.TextBox11.TabIndex = 55
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(321, 357)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(83, 16)
        Me.Label23.TabIndex = 54
        Me.Label23.Text = "Total Assets"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(509, 435)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(96, 22)
        Me.TextBox7.TabIndex = 6
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(410, 435)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(96, 22)
        Me.TextBox8.TabIndex = 5
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(95, 438)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(309, 16)
        Me.Label22.TabIndex = 51
        Me.Label22.Text = "How do you feel about your current levels of return?"
        '
        'TextBox4
        '
        Me.TextBox4.Enabled = False
        Me.TextBox4.Location = New System.Drawing.Point(731, 381)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(96, 22)
        Me.TextBox4.TabIndex = 50
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(509, 381)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(96, 22)
        Me.TextBox5.TabIndex = 2
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(410, 381)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(96, 22)
        Me.TextBox6.TabIndex = 1
        '
        'TextBox3
        '
        Me.TextBox3.Enabled = False
        Me.TextBox3.Location = New System.Drawing.Point(731, 408)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(96, 22)
        Me.TextBox3.TabIndex = 47
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(509, 408)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(96, 22)
        Me.TextBox2.TabIndex = 4
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(410, 408)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(96, 22)
        Me.TextBox1.TabIndex = 3
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(55, 411)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(349, 16)
        Me.Label21.TabIndex = 44
        Me.Label21.Text = "How much emergency cash should you keep in the bank?"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(160, 384)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(244, 16)
        Me.Label20.TabIndex = 43
        Me.Label20.Text = "Total gross income including everything"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.DataGridViewLinkColumn2})
        Me.DataGridView2.Location = New System.Drawing.Point(16, 72)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView2.Size = New System.Drawing.Size(1114, 272)
        Me.DataGridView2.TabIndex = 42
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Asset Type"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Detail"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Client 1"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Client 2"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'Column1
        '
        Me.Column1.HeaderText = "Joint Total"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "In Trust?"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Income"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Returns"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'DataGridViewLinkColumn2
        '
        Me.DataGridViewLinkColumn2.HeaderText = "Action"
        Me.DataGridViewLinkColumn2.Name = "DataGridViewLinkColumn2"
        Me.DataGridViewLinkColumn2.ReadOnly = True
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.black
        Me.Button3.Location = New System.Drawing.Point(1005, 30)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(125, 36)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "Add An Asset"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(135, 20)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Asset Information"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage3.Controls.Add(Me.GroupBox5)
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.TextBox19)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.Label31)
        Me.TabPage3.Controls.Add(Me.Label30)
        Me.TabPage3.Controls.Add(Me.Label29)
        Me.TabPage3.Controls.Add(Me.Label28)
        Me.TabPage3.Controls.Add(Me.Label27)
        Me.TabPage3.Controls.Add(Me.Label26)
        Me.TabPage3.Controls.Add(Me.Label25)
        Me.TabPage3.Controls.Add(Me.Label24)
        Me.TabPage3.Controls.Add(Me.DataGridView1)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Location = New System.Drawing.Point(4, 39)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "  Client Offer"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBox32)
        Me.GroupBox5.Controls.Add(Me.Button1)
        Me.GroupBox5.Controls.Add(Me.Label37)
        Me.GroupBox5.Controls.Add(Me.TextBox27)
        Me.GroupBox5.Controls.Add(Me.Label36)
        Me.GroupBox5.Controls.Add(Me.Label32)
        Me.GroupBox5.Controls.Add(Me.TextBox31)
        Me.GroupBox5.Controls.Add(Me.TextBox28)
        Me.GroupBox5.Controls.Add(Me.Label35)
        Me.GroupBox5.Controls.Add(Me.Label33)
        Me.GroupBox5.Controls.Add(Me.TextBox30)
        Me.GroupBox5.Controls.Add(Me.TextBox29)
        Me.GroupBox5.Controls.Add(Me.Label34)
        Me.GroupBox5.Location = New System.Drawing.Point(374, 49)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(234, 260)
        Me.GroupBox5.TabIndex = 23
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Returns Calculator"
        '
        'TextBox32
        '
        Me.TextBox32.Location = New System.Drawing.Point(130, 25)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.Size = New System.Drawing.Size(90, 22)
        Me.TextBox32.TabIndex = 7
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(130, 162)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(90, 27)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "calculate"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(52, 28)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(72, 16)
        Me.Label37.TabIndex = 17
        Me.Label37.Text = "Investment"
        '
        'TextBox27
        '
        Me.TextBox27.Enabled = False
        Me.TextBox27.Location = New System.Drawing.Point(130, 223)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New System.Drawing.Size(90, 22)
        Me.TextBox27.TabIndex = 12
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(69, 55)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(55, 16)
        Me.Label36.TabIndex = 18
        Me.Label36.Text = "Deposit"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(77, 226)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(47, 16)
        Me.Label32.TabIndex = 22
        Me.Label32.Text = "Return"
        '
        'TextBox31
        '
        Me.TextBox31.Location = New System.Drawing.Point(130, 52)
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.Size = New System.Drawing.Size(90, 22)
        Me.TextBox31.TabIndex = 8
        '
        'TextBox28
        '
        Me.TextBox28.Enabled = False
        Me.TextBox28.Location = New System.Drawing.Point(130, 133)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Size = New System.Drawing.Size(90, 22)
        Me.TextBox28.TabIndex = 11
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(86, 82)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(38, 16)
        Me.Label35.TabIndex = 19
        Me.Label35.Text = "Loan"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(7, 136)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(117, 16)
        Me.Label33.TabIndex = 21
        Me.Label33.Text = "Repayment @ 8%"
        '
        'TextBox30
        '
        Me.TextBox30.Enabled = False
        Me.TextBox30.Location = New System.Drawing.Point(130, 79)
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.Size = New System.Drawing.Size(90, 22)
        Me.TextBox30.TabIndex = 9
        '
        'TextBox29
        '
        Me.TextBox29.Location = New System.Drawing.Point(130, 106)
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.Size = New System.Drawing.Size(90, 22)
        Me.TextBox29.TabIndex = 10
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(73, 109)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(51, 16)
        Me.Label34.TabIndex = 20
        Me.Label34.Text = "Months"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.CheckBox9)
        Me.GroupBox4.Controls.Add(Me.CheckBox10)
        Me.GroupBox4.Controls.Add(Me.CheckBox11)
        Me.GroupBox4.Controls.Add(Me.CheckBox12)
        Me.GroupBox4.Controls.Add(Me.CheckBox13)
        Me.GroupBox4.Controls.Add(Me.CheckBox14)
        Me.GroupBox4.Controls.Add(Me.TextBox20)
        Me.GroupBox4.Controls.Add(Me.TextBox21)
        Me.GroupBox4.Controls.Add(Me.TextBox22)
        Me.GroupBox4.Controls.Add(Me.TextBox23)
        Me.GroupBox4.Controls.Add(Me.TextBox24)
        Me.GroupBox4.Controls.Add(Me.TextBox25)
        Me.GroupBox4.Controls.Add(Me.TextBox26)
        Me.GroupBox4.Location = New System.Drawing.Point(233, 49)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(104, 217)
        Me.GroupBox4.TabIndex = 16
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Client 2"
        '
        'CheckBox9
        '
        Me.CheckBox9.AutoSize = True
        Me.CheckBox9.Location = New System.Drawing.Point(82, 165)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox9.TabIndex = 19
        Me.CheckBox9.UseVisualStyleBackColor = True
        '
        'CheckBox10
        '
        Me.CheckBox10.AutoSize = True
        Me.CheckBox10.Location = New System.Drawing.Point(82, 138)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox10.TabIndex = 18
        Me.CheckBox10.UseVisualStyleBackColor = True
        '
        'CheckBox11
        '
        Me.CheckBox11.AutoSize = True
        Me.CheckBox11.Location = New System.Drawing.Point(82, 111)
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox11.TabIndex = 17
        Me.CheckBox11.UseVisualStyleBackColor = True
        '
        'CheckBox12
        '
        Me.CheckBox12.AutoSize = True
        Me.CheckBox12.Location = New System.Drawing.Point(82, 84)
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox12.TabIndex = 16
        Me.CheckBox12.UseVisualStyleBackColor = True
        '
        'CheckBox13
        '
        Me.CheckBox13.AutoSize = True
        Me.CheckBox13.Location = New System.Drawing.Point(82, 60)
        Me.CheckBox13.Name = "CheckBox13"
        Me.CheckBox13.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox13.TabIndex = 15
        Me.CheckBox13.UseVisualStyleBackColor = True
        '
        'CheckBox14
        '
        Me.CheckBox14.AutoSize = True
        Me.CheckBox14.Location = New System.Drawing.Point(82, 30)
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox14.TabIndex = 14
        Me.CheckBox14.UseVisualStyleBackColor = True
        '
        'TextBox20
        '
        Me.TextBox20.Enabled = False
        Me.TextBox20.Location = New System.Drawing.Point(6, 187)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(70, 22)
        Me.TextBox20.TabIndex = 6
        '
        'TextBox21
        '
        Me.TextBox21.Location = New System.Drawing.Point(6, 160)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Size = New System.Drawing.Size(70, 22)
        Me.TextBox21.TabIndex = 5
        '
        'TextBox22
        '
        Me.TextBox22.Location = New System.Drawing.Point(6, 133)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(70, 22)
        Me.TextBox22.TabIndex = 4
        '
        'TextBox23
        '
        Me.TextBox23.Location = New System.Drawing.Point(6, 106)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New System.Drawing.Size(70, 22)
        Me.TextBox23.TabIndex = 3
        '
        'TextBox24
        '
        Me.TextBox24.Location = New System.Drawing.Point(6, 79)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New System.Drawing.Size(70, 22)
        Me.TextBox24.TabIndex = 2
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(6, 52)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(70, 22)
        Me.TextBox25.TabIndex = 1
        '
        'TextBox26
        '
        Me.TextBox26.Location = New System.Drawing.Point(6, 25)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New System.Drawing.Size(70, 22)
        Me.TextBox26.TabIndex = 0
        '
        'TextBox19
        '
        Me.TextBox19.Enabled = False
        Me.TextBox19.Location = New System.Drawing.Point(115, 272)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(90, 22)
        Me.TextBox19.TabIndex = 7
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CheckBox6)
        Me.GroupBox3.Controls.Add(Me.CheckBox5)
        Me.GroupBox3.Controls.Add(Me.CheckBox4)
        Me.GroupBox3.Controls.Add(Me.CheckBox3)
        Me.GroupBox3.Controls.Add(Me.CheckBox2)
        Me.GroupBox3.Controls.Add(Me.CheckBox1)
        Me.GroupBox3.Controls.Add(Me.TextBox18)
        Me.GroupBox3.Controls.Add(Me.TextBox17)
        Me.GroupBox3.Controls.Add(Me.TextBox16)
        Me.GroupBox3.Controls.Add(Me.TextBox15)
        Me.GroupBox3.Controls.Add(Me.TextBox14)
        Me.GroupBox3.Controls.Add(Me.TextBox13)
        Me.GroupBox3.Controls.Add(Me.TextBox12)
        Me.GroupBox3.Location = New System.Drawing.Point(109, 49)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(104, 217)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Client 1"
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Location = New System.Drawing.Point(84, 165)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox6.TabIndex = 12
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(84, 138)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox5.TabIndex = 11
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Location = New System.Drawing.Point(84, 111)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox4.TabIndex = 10
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(84, 84)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox3.TabIndex = 9
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(84, 60)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox2.TabIndex = 8
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(84, 30)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 7
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'TextBox18
        '
        Me.TextBox18.Enabled = False
        Me.TextBox18.Location = New System.Drawing.Point(6, 187)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(70, 22)
        Me.TextBox18.TabIndex = 6
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(6, 160)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(70, 22)
        Me.TextBox17.TabIndex = 5
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(6, 133)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(70, 22)
        Me.TextBox16.TabIndex = 4
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(6, 106)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(70, 22)
        Me.TextBox15.TabIndex = 3
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(6, 79)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(70, 22)
        Me.TextBox14.TabIndex = 2
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(6, 52)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(70, 22)
        Me.TextBox13.TabIndex = 1
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(6, 25)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(70, 22)
        Me.TextBox12.TabIndex = 0
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(24, 275)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(79, 16)
        Me.Label31.TabIndex = 14
        Me.Label31.Text = "Grand Total"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(37, 239)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(66, 16)
        Me.Label30.TabIndex = 13
        Me.Label30.Text = "Sub Total"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 212)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(86, 16)
        Me.Label29.TabIndex = 12
        Me.Label29.Text = "Probate Plan"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(20, 185)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(83, 16)
        Me.Label28.TabIndex = 11
        Me.Label28.Text = "Funeral Plan"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(58, 158)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(45, 16)
        Me.Label27.TabIndex = 10
        Me.Label27.Text = "Trusts"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(41, 131)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(62, 16)
        Me.Label26.TabIndex = 9
        Me.Label26.Text = "P&&F LPA"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(35, 104)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(68, 16)
        Me.Label25.TabIndex = 8
        Me.Label25.Text = "H&&W LPA"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(66, 77)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(37, 16)
        Me.Label24.TabIndex = 7
        Me.Label24.Text = "Wills"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11})
        Me.DataGridView1.Location = New System.Drawing.Point(648, 49)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.Size = New System.Drawing.Size(532, 424)
        Me.DataGridView1.TabIndex = 3
        '
        'Column6
        '
        Me.Column6.HeaderText = "Month"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 70
        '
        'Column7
        '
        Me.Column7.HeaderText = "Invested"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 80
        '
        'Column8
        '
        Me.Column8.HeaderText = "Interest @ 6%"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 80
        '
        'Column9
        '
        Me.Column9.HeaderText = "AMC @1.2%"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 80
        '
        'Column10
        '
        Me.Column10.HeaderText = "Loan"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 80
        '
        'Column11
        '
        Me.Column11.HeaderText = "Value"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 80
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 20)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Client Offer"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage4.Controls.Add(Me.Label78)
        Me.TabPage4.Controls.Add(Me.TextBox76)
        Me.TabPage4.Controls.Add(Me.Label77)
        Me.TabPage4.Controls.Add(Me.TextBox75)
        Me.TabPage4.Controls.Add(Me.Label49)
        Me.TabPage4.Controls.Add(Me.Label48)
        Me.TabPage4.Controls.Add(Me.TextBox42)
        Me.TabPage4.Controls.Add(Me.TextBox41)
        Me.TabPage4.Controls.Add(Me.Label47)
        Me.TabPage4.Controls.Add(Me.TextBox40)
        Me.TabPage4.Controls.Add(Me.TextBox39)
        Me.TabPage4.Controls.Add(Me.Label46)
        Me.TabPage4.Controls.Add(Me.TextBox38)
        Me.TabPage4.Controls.Add(Me.Label44)
        Me.TabPage4.Controls.Add(Me.TextBox37)
        Me.TabPage4.Controls.Add(Me.Label45)
        Me.TabPage4.Controls.Add(Me.Button4)
        Me.TabPage4.Controls.Add(Me.TextBox36)
        Me.TabPage4.Controls.Add(Me.Label43)
        Me.TabPage4.Controls.Add(Me.Label42)
        Me.TabPage4.Controls.Add(Me.TextBox35)
        Me.TabPage4.Controls.Add(Me.Label41)
        Me.TabPage4.Controls.Add(Me.Label40)
        Me.TabPage4.Controls.Add(Me.TextBox34)
        Me.TabPage4.Controls.Add(Me.TextBox33)
        Me.TabPage4.Controls.Add(Me.Label39)
        Me.TabPage4.Controls.Add(Me.Label38)
        Me.TabPage4.Controls.Add(Me.Label3)
        Me.TabPage4.Location = New System.Drawing.Point(4, 39)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Probate Forecast"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(831, 32)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(77, 16)
        Me.Label78.TabIndex = 34
        Me.Label78.Text = "expectancy"
        Me.Label78.Visible = False
        '
        'TextBox76
        '
        Me.TextBox76.Location = New System.Drawing.Point(825, 61)
        Me.TextBox76.Name = "TextBox76"
        Me.TextBox76.Size = New System.Drawing.Size(39, 22)
        Me.TextBox76.TabIndex = 33
        Me.TextBox76.Visible = False
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(755, 32)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(59, 16)
        Me.Label77.TabIndex = 32
        Me.Label77.Text = "average"
        Me.Label77.Visible = False
        '
        'TextBox75
        '
        Me.TextBox75.Location = New System.Drawing.Point(749, 61)
        Me.TextBox75.Name = "TextBox75"
        Me.TextBox75.Size = New System.Drawing.Size(39, 22)
        Me.TextBox75.TabIndex = 31
        Me.TextBox75.Visible = False
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(697, 32)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(39, 16)
        Me.Label49.TabIndex = 30
        Me.Label49.Text = "age2"
        Me.Label49.Visible = False
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(643, 32)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(39, 16)
        Me.Label48.TabIndex = 29
        Me.Label48.Text = "age1"
        Me.Label48.Visible = False
        '
        'TextBox42
        '
        Me.TextBox42.Location = New System.Drawing.Point(691, 61)
        Me.TextBox42.Name = "TextBox42"
        Me.TextBox42.Size = New System.Drawing.Size(39, 22)
        Me.TextBox42.TabIndex = 28
        Me.TextBox42.Visible = False
        '
        'TextBox41
        '
        Me.TextBox41.Location = New System.Drawing.Point(646, 61)
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.Size = New System.Drawing.Size(39, 22)
        Me.TextBox41.TabIndex = 27
        Me.TextBox41.Visible = False
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(27, 284)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(101, 16)
        Me.Label47.TabIndex = 26
        Me.Label47.Text = "Probate Saving"
        '
        'TextBox40
        '
        Me.TextBox40.BackColor = System.Drawing.Color.LightGreen
        Me.TextBox40.Location = New System.Drawing.Point(330, 281)
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.Size = New System.Drawing.Size(125, 22)
        Me.TextBox40.TabIndex = 25
        '
        'TextBox39
        '
        Me.TextBox39.Location = New System.Drawing.Point(330, 243)
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.Size = New System.Drawing.Size(125, 22)
        Me.TextBox39.TabIndex = 24
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(27, 246)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(65, 16)
        Me.Label46.TabIndex = 23
        Me.Label46.Text = "PPP Cost"
        '
        'TextBox38
        '
        Me.TextBox38.Location = New System.Drawing.Point(330, 210)
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.Size = New System.Drawing.Size(125, 22)
        Me.TextBox38.TabIndex = 22
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(289, 213)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(20, 16)
        Me.Label44.TabIndex = 21
        Me.Label44.Text = "%"
        '
        'TextBox37
        '
        Me.TextBox37.Location = New System.Drawing.Point(243, 210)
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.Size = New System.Drawing.Size(39, 22)
        Me.TextBox37.TabIndex = 20
        Me.TextBox37.Text = "1.5"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(27, 213)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(210, 16)
        Me.Label45.TabIndex = 19
        Me.Label45.Text = "Estimated probate cost (incl. VAT)"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(330, 147)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 18
        Me.Button4.Text = "Button4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'TextBox36
        '
        Me.TextBox36.Location = New System.Drawing.Point(157, 147)
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.Size = New System.Drawing.Size(125, 22)
        Me.TextBox36.TabIndex = 17
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(27, 150)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(124, 16)
        Me.Label43.TabIndex = 16
        Me.Label43.Text = "Future Estate Value"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(190, 122)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(42, 16)
        Me.Label42.TabIndex = 15
        Me.Label42.Text = "years"
        '
        'TextBox35
        '
        Me.TextBox35.Location = New System.Drawing.Point(157, 119)
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.Size = New System.Drawing.Size(27, 22)
        Me.TextBox35.TabIndex = 14
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(49, 122)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(102, 16)
        Me.Label41.TabIndex = 13
        Me.Label41.Text = "Life Expectancy"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(197, 92)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(20, 16)
        Me.Label40.TabIndex = 12
        Me.Label40.Text = "%"
        '
        'TextBox34
        '
        Me.TextBox34.Location = New System.Drawing.Point(157, 89)
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.Size = New System.Drawing.Size(39, 22)
        Me.TextBox34.TabIndex = 11
        '
        'TextBox33
        '
        Me.TextBox33.Location = New System.Drawing.Point(157, 61)
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.Size = New System.Drawing.Size(125, 22)
        Me.TextBox33.TabIndex = 10
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(98, 92)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(53, 16)
        Me.Label39.TabIndex = 9
        Me.Label39.Text = "Inflation"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(33, 64)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(118, 16)
        Me.Label38.TabIndex = 8
        Me.Label38.Text = "Total Estate Value"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Probate Forecast"
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage5.Controls.Add(Me.Label58)
        Me.TabPage5.Controls.Add(Me.TextBox49)
        Me.TabPage5.Controls.Add(Me.Button5)
        Me.TabPage5.Controls.Add(Me.TextBox46)
        Me.TabPage5.Controls.Add(Me.Label53)
        Me.TabPage5.Controls.Add(Me.Label54)
        Me.TabPage5.Controls.Add(Me.TextBox47)
        Me.TabPage5.Controls.Add(Me.Label55)
        Me.TabPage5.Controls.Add(Me.Label56)
        Me.TabPage5.Controls.Add(Me.TextBox48)
        Me.TabPage5.Controls.Add(Me.Label57)
        Me.TabPage5.Controls.Add(Me.TextBox45)
        Me.TabPage5.Controls.Add(Me.Label52)
        Me.TabPage5.Controls.Add(Me.TextBox44)
        Me.TabPage5.Controls.Add(Me.Label51)
        Me.TabPage5.Controls.Add(Me.TextBox43)
        Me.TabPage5.Controls.Add(Me.Label50)
        Me.TabPage5.Controls.Add(Me.Label2)
        Me.TabPage5.Location = New System.Drawing.Point(4, 39)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Funeral Forecast"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(101, 256)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(50, 16)
        Me.Label58.TabIndex = 29
        Me.Label58.Text = "Saving"
        '
        'TextBox49
        '
        Me.TextBox49.BackColor = System.Drawing.Color.LightGreen
        Me.TextBox49.Location = New System.Drawing.Point(157, 253)
        Me.TextBox49.Name = "TextBox49"
        Me.TextBox49.Size = New System.Drawing.Size(75, 22)
        Me.TextBox49.TabIndex = 28
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(280, 213)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 27
        Me.Button5.Text = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'TextBox46
        '
        Me.TextBox46.Location = New System.Drawing.Point(157, 214)
        Me.TextBox46.Name = "TextBox46"
        Me.TextBox46.Size = New System.Drawing.Size(75, 22)
        Me.TextBox46.TabIndex = 26
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(76, 217)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(75, 16)
        Me.Label53.TabIndex = 25
        Me.Label53.Text = "Future Cost"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(190, 189)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(42, 16)
        Me.Label54.TabIndex = 24
        Me.Label54.Text = "years"
        '
        'TextBox47
        '
        Me.TextBox47.Location = New System.Drawing.Point(157, 186)
        Me.TextBox47.Name = "TextBox47"
        Me.TextBox47.Size = New System.Drawing.Size(27, 22)
        Me.TextBox47.TabIndex = 23
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(49, 189)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(102, 16)
        Me.Label55.TabIndex = 22
        Me.Label55.Text = "Life Expectancy"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(197, 159)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(20, 16)
        Me.Label56.TabIndex = 21
        Me.Label56.Text = "%"
        '
        'TextBox48
        '
        Me.TextBox48.Location = New System.Drawing.Point(157, 156)
        Me.TextBox48.Name = "TextBox48"
        Me.TextBox48.Size = New System.Drawing.Size(39, 22)
        Me.TextBox48.TabIndex = 20
        Me.TextBox48.Text = "6.0"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(98, 159)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(53, 16)
        Me.Label57.TabIndex = 19
        Me.Label57.Text = "Inflation"
        '
        'TextBox45
        '
        Me.TextBox45.Location = New System.Drawing.Point(157, 117)
        Me.TextBox45.Name = "TextBox45"
        Me.TextBox45.Size = New System.Drawing.Size(71, 22)
        Me.TextBox45.TabIndex = 16
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(112, 120)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(39, 16)
        Me.Label52.TabIndex = 15
        Me.Label52.Text = "Total"
        '
        'TextBox44
        '
        Me.TextBox44.Location = New System.Drawing.Point(157, 89)
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.Size = New System.Drawing.Size(71, 22)
        Me.TextBox44.TabIndex = 14
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(35, 92)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(116, 16)
        Me.Label51.TabIndex = 13
        Me.Label51.Text = "Client 2 Allowance"
        '
        'TextBox43
        '
        Me.TextBox43.Location = New System.Drawing.Point(157, 61)
        Me.TextBox43.Name = "TextBox43"
        Me.TextBox43.Size = New System.Drawing.Size(71, 22)
        Me.TextBox43.TabIndex = 12
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(35, 64)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(116, 16)
        Me.Label50.TabIndex = 11
        Me.Label50.Text = "Client 1 Allowance"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Funeral Plans"
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage6.Controls.Add(Me.GroupBox6)
        Me.TabPage6.Controls.Add(Me.TextBox58)
        Me.TabPage6.Controls.Add(Me.Label67)
        Me.TabPage6.Controls.Add(Me.TextBox57)
        Me.TabPage6.Controls.Add(Me.Label66)
        Me.TabPage6.Controls.Add(Me.TextBox56)
        Me.TabPage6.Controls.Add(Me.Label65)
        Me.TabPage6.Controls.Add(Me.TextBox55)
        Me.TabPage6.Controls.Add(Me.Label64)
        Me.TabPage6.Controls.Add(Me.TextBox54)
        Me.TabPage6.Controls.Add(Me.Label63)
        Me.TabPage6.Controls.Add(Me.TextBox53)
        Me.TabPage6.Controls.Add(Me.Label62)
        Me.TabPage6.Controls.Add(Me.TextBox52)
        Me.TabPage6.Controls.Add(Me.Label61)
        Me.TabPage6.Controls.Add(Me.TextBox51)
        Me.TabPage6.Controls.Add(Me.Label60)
        Me.TabPage6.Controls.Add(Me.TextBox50)
        Me.TabPage6.Controls.Add(Me.Label59)
        Me.TabPage6.Controls.Add(Me.Label8)
        Me.TabPage6.Location = New System.Drawing.Point(4, 39)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Saving Summary"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.GroupBox7)
        Me.GroupBox6.Controls.Add(Me.TextBox66)
        Me.GroupBox6.Controls.Add(Me.GroupBox8)
        Me.GroupBox6.Controls.Add(Me.Label68)
        Me.GroupBox6.Controls.Add(Me.Label69)
        Me.GroupBox6.Controls.Add(Me.Label70)
        Me.GroupBox6.Controls.Add(Me.Label71)
        Me.GroupBox6.Controls.Add(Me.Label72)
        Me.GroupBox6.Controls.Add(Me.Label73)
        Me.GroupBox6.Controls.Add(Me.Label74)
        Me.GroupBox6.Controls.Add(Me.Label75)
        Me.GroupBox6.Location = New System.Drawing.Point(730, 57)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(329, 303)
        Me.GroupBox6.TabIndex = 31
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "This includes the trust paying for"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.TextBox59)
        Me.GroupBox7.Controls.Add(Me.TextBox60)
        Me.GroupBox7.Controls.Add(Me.TextBox61)
        Me.GroupBox7.Controls.Add(Me.TextBox62)
        Me.GroupBox7.Controls.Add(Me.TextBox63)
        Me.GroupBox7.Controls.Add(Me.TextBox64)
        Me.GroupBox7.Controls.Add(Me.TextBox65)
        Me.GroupBox7.Enabled = False
        Me.GroupBox7.Location = New System.Drawing.Point(218, 41)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(85, 217)
        Me.GroupBox7.TabIndex = 27
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Client 2"
        '
        'TextBox59
        '
        Me.TextBox59.Location = New System.Drawing.Point(6, 187)
        Me.TextBox59.Name = "TextBox59"
        Me.TextBox59.Size = New System.Drawing.Size(70, 22)
        Me.TextBox59.TabIndex = 6
        '
        'TextBox60
        '
        Me.TextBox60.Location = New System.Drawing.Point(6, 160)
        Me.TextBox60.Name = "TextBox60"
        Me.TextBox60.Size = New System.Drawing.Size(70, 22)
        Me.TextBox60.TabIndex = 5
        '
        'TextBox61
        '
        Me.TextBox61.Location = New System.Drawing.Point(6, 133)
        Me.TextBox61.Name = "TextBox61"
        Me.TextBox61.Size = New System.Drawing.Size(70, 22)
        Me.TextBox61.TabIndex = 4
        '
        'TextBox62
        '
        Me.TextBox62.Location = New System.Drawing.Point(6, 106)
        Me.TextBox62.Name = "TextBox62"
        Me.TextBox62.Size = New System.Drawing.Size(70, 22)
        Me.TextBox62.TabIndex = 3
        '
        'TextBox63
        '
        Me.TextBox63.Location = New System.Drawing.Point(6, 79)
        Me.TextBox63.Name = "TextBox63"
        Me.TextBox63.Size = New System.Drawing.Size(70, 22)
        Me.TextBox63.TabIndex = 2
        '
        'TextBox64
        '
        Me.TextBox64.Location = New System.Drawing.Point(6, 52)
        Me.TextBox64.Name = "TextBox64"
        Me.TextBox64.Size = New System.Drawing.Size(70, 22)
        Me.TextBox64.TabIndex = 1
        '
        'TextBox65
        '
        Me.TextBox65.Location = New System.Drawing.Point(6, 25)
        Me.TextBox65.Name = "TextBox65"
        Me.TextBox65.Size = New System.Drawing.Size(70, 22)
        Me.TextBox65.TabIndex = 0
        '
        'TextBox66
        '
        Me.TextBox66.Enabled = False
        Me.TextBox66.Location = New System.Drawing.Point(119, 264)
        Me.TextBox66.Name = "TextBox66"
        Me.TextBox66.Size = New System.Drawing.Size(90, 22)
        Me.TextBox66.TabIndex = 17
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.TextBox67)
        Me.GroupBox8.Controls.Add(Me.TextBox68)
        Me.GroupBox8.Controls.Add(Me.TextBox69)
        Me.GroupBox8.Controls.Add(Me.TextBox70)
        Me.GroupBox8.Controls.Add(Me.TextBox71)
        Me.GroupBox8.Controls.Add(Me.TextBox72)
        Me.GroupBox8.Controls.Add(Me.TextBox73)
        Me.GroupBox8.Enabled = False
        Me.GroupBox8.Location = New System.Drawing.Point(113, 41)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(84, 217)
        Me.GroupBox8.TabIndex = 26
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Client 1"
        '
        'TextBox67
        '
        Me.TextBox67.Location = New System.Drawing.Point(6, 187)
        Me.TextBox67.Name = "TextBox67"
        Me.TextBox67.Size = New System.Drawing.Size(70, 22)
        Me.TextBox67.TabIndex = 6
        '
        'TextBox68
        '
        Me.TextBox68.Location = New System.Drawing.Point(6, 160)
        Me.TextBox68.Name = "TextBox68"
        Me.TextBox68.Size = New System.Drawing.Size(70, 22)
        Me.TextBox68.TabIndex = 5
        '
        'TextBox69
        '
        Me.TextBox69.Location = New System.Drawing.Point(6, 133)
        Me.TextBox69.Name = "TextBox69"
        Me.TextBox69.Size = New System.Drawing.Size(70, 22)
        Me.TextBox69.TabIndex = 4
        '
        'TextBox70
        '
        Me.TextBox70.Location = New System.Drawing.Point(6, 106)
        Me.TextBox70.Name = "TextBox70"
        Me.TextBox70.Size = New System.Drawing.Size(70, 22)
        Me.TextBox70.TabIndex = 3
        '
        'TextBox71
        '
        Me.TextBox71.Location = New System.Drawing.Point(6, 79)
        Me.TextBox71.Name = "TextBox71"
        Me.TextBox71.Size = New System.Drawing.Size(70, 22)
        Me.TextBox71.TabIndex = 2
        '
        'TextBox72
        '
        Me.TextBox72.Location = New System.Drawing.Point(6, 52)
        Me.TextBox72.Name = "TextBox72"
        Me.TextBox72.Size = New System.Drawing.Size(70, 22)
        Me.TextBox72.TabIndex = 1
        '
        'TextBox73
        '
        Me.TextBox73.Location = New System.Drawing.Point(6, 25)
        Me.TextBox73.Name = "TextBox73"
        Me.TextBox73.Size = New System.Drawing.Size(70, 22)
        Me.TextBox73.TabIndex = 0
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Enabled = False
        Me.Label68.Location = New System.Drawing.Point(28, 267)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(79, 16)
        Me.Label68.TabIndex = 25
        Me.Label68.Text = "Grand Total"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Enabled = False
        Me.Label69.Location = New System.Drawing.Point(41, 231)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(66, 16)
        Me.Label69.TabIndex = 24
        Me.Label69.Text = "Sub Total"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Enabled = False
        Me.Label70.Location = New System.Drawing.Point(21, 204)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(86, 16)
        Me.Label70.TabIndex = 23
        Me.Label70.Text = "Probate Plan"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Enabled = False
        Me.Label71.Location = New System.Drawing.Point(24, 177)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(83, 16)
        Me.Label71.TabIndex = 22
        Me.Label71.Text = "Funeral Plan"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Enabled = False
        Me.Label72.Location = New System.Drawing.Point(62, 150)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(45, 16)
        Me.Label72.TabIndex = 21
        Me.Label72.Text = "Trusts"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Enabled = False
        Me.Label73.Location = New System.Drawing.Point(45, 123)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(62, 16)
        Me.Label73.TabIndex = 20
        Me.Label73.Text = "P&&F LPA"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Enabled = False
        Me.Label74.Location = New System.Drawing.Point(39, 96)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(68, 16)
        Me.Label74.TabIndex = 19
        Me.Label74.Text = "H&&W LPA"
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Enabled = False
        Me.Label75.Location = New System.Drawing.Point(70, 69)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(37, 16)
        Me.Label75.TabIndex = 18
        Me.Label75.Text = "Wills"
        '
        'TextBox58
        '
        Me.TextBox58.BackColor = System.Drawing.Color.LightGreen
        Me.TextBox58.Enabled = False
        Me.TextBox58.Location = New System.Drawing.Point(552, 324)
        Me.TextBox58.Name = "TextBox58"
        Me.TextBox58.Size = New System.Drawing.Size(108, 22)
        Me.TextBox58.TabIndex = 30
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(494, 327)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(57, 16)
        Me.Label67.TabIndex = 29
        Me.Label67.Text = "TOTAL"
        '
        'TextBox57
        '
        Me.TextBox57.Enabled = False
        Me.TextBox57.Location = New System.Drawing.Point(552, 296)
        Me.TextBox57.Name = "TextBox57"
        Me.TextBox57.Size = New System.Drawing.Size(108, 22)
        Me.TextBox57.TabIndex = 28
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(272, 299)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(274, 16)
        Me.Label66.TabIndex = 27
        Me.Label66.Text = "Amount of IHT shetlered now in BPR/QNUPS"
        '
        'TextBox56
        '
        Me.TextBox56.Enabled = False
        Me.TextBox56.Location = New System.Drawing.Point(552, 268)
        Me.TextBox56.Name = "TextBox56"
        Me.TextBox56.Size = New System.Drawing.Size(108, 22)
        Me.TextBox56.TabIndex = 26
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(474, 271)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(72, 16)
        Me.Label65.TabIndex = 25
        Me.Label65.Text = "Total Trust"
        '
        'TextBox55
        '
        Me.TextBox55.Enabled = False
        Me.TextBox55.Location = New System.Drawing.Point(552, 222)
        Me.TextBox55.Name = "TextBox55"
        Me.TextBox55.Size = New System.Drawing.Size(108, 22)
        Me.TextBox55.TabIndex = 24
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(480, 225)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(66, 16)
        Me.Label64.TabIndex = 23
        Me.Label64.Text = "(property)"
        '
        'TextBox54
        '
        Me.TextBox54.Enabled = False
        Me.TextBox54.Location = New System.Drawing.Point(552, 189)
        Me.TextBox54.Name = "TextBox54"
        Me.TextBox54.Size = New System.Drawing.Size(108, 22)
        Me.TextBox54.TabIndex = 22
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(74, 192)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(472, 16)
        Me.Label63.TabIndex = 21
        Me.Label63.Text = "By shielding your assets in trust, your beneficiaries could be better off by (cas" &
    "h)"
        '
        'TextBox53
        '
        Me.TextBox53.Enabled = False
        Me.TextBox53.Location = New System.Drawing.Point(552, 156)
        Me.TextBox53.Name = "TextBox53"
        Me.TextBox53.Size = New System.Drawing.Size(108, 22)
        Me.TextBox53.TabIndex = 20
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(120, 159)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(426, 16)
        Me.Label62.TabIndex = 19
        Me.Label62.Text = "By protecting your cash in trust, your beneficiaries could be better off by"
        '
        'TextBox52
        '
        Me.TextBox52.Enabled = False
        Me.TextBox52.Location = New System.Drawing.Point(552, 123)
        Me.TextBox52.Name = "TextBox52"
        Me.TextBox52.Size = New System.Drawing.Size(108, 22)
        Me.TextBox52.TabIndex = 18
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(309, 126)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(237, 16)
        Me.Label61.TabIndex = 17
        Me.Label61.Text = "Reducing government probate fees by"
        '
        'TextBox51
        '
        Me.TextBox51.Enabled = False
        Me.TextBox51.Location = New System.Drawing.Point(552, 90)
        Me.TextBox51.Name = "TextBox51"
        Me.TextBox51.Size = New System.Drawing.Size(108, 22)
        Me.TextBox51.TabIndex = 16
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(115, 93)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(431, 16)
        Me.Label60.TabIndex = 15
        Me.Label60.Text = "By paying for your probate now, your beneficiaries could be better off by"
        '
        'TextBox50
        '
        Me.TextBox50.Enabled = False
        Me.TextBox50.Location = New System.Drawing.Point(552, 57)
        Me.TextBox50.Name = "TextBox50"
        Me.TextBox50.Size = New System.Drawing.Size(108, 22)
        Me.TextBox50.TabIndex = 14
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(27, 60)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(519, 16)
        Me.Label59.TabIndex = 13
        Me.Label59.Text = "By paying for your funerals now, your beneficiaries could be better off by approx" &
    "imately"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(269, 20)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Presentation && Summary Of Benefits"
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage7.Controls.Add(Me.Label9)
        Me.TabPage7.Location = New System.Drawing.Point(4, 39)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "IHT Calculator Solutions"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(12, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(110, 20)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "IHT Calculator"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.color.black
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(213, 25)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Estate Plan Wizard"
        '
        'factfind_wizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
 Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button2)
        Me.MaximizeBox = False
        Me.Name = "factfind_wizard"
        Me.Text = "Client Instruction - Estate Plan Wizard"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button2 As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Panel11 As Panel
    Friend WithEvents RadioButton21 As RadioButton
    Friend WithEvents RadioButton22 As RadioButton
    Friend WithEvents Panel12 As Panel
    Friend WithEvents RadioButton23 As RadioButton
    Friend WithEvents RadioButton24 As RadioButton
    Friend WithEvents Panel13 As Panel
    Friend WithEvents RadioButton25 As RadioButton
    Friend WithEvents RadioButton26 As RadioButton
    Friend WithEvents Panel14 As Panel
    Friend WithEvents RadioButton27 As RadioButton
    Friend WithEvents RadioButton28 As RadioButton
    Friend WithEvents Panel15 As Panel
    Friend WithEvents RadioButton29 As RadioButton
    Friend WithEvents RadioButton30 As RadioButton
    Friend WithEvents Panel16 As Panel
    Friend WithEvents RadioButton31 As RadioButton
    Friend WithEvents RadioButton32 As RadioButton
    Friend WithEvents Panel17 As Panel
    Friend WithEvents RadioButton33 As RadioButton
    Friend WithEvents RadioButton34 As RadioButton
    Friend WithEvents Panel18 As Panel
    Friend WithEvents RadioButton35 As RadioButton
    Friend WithEvents RadioButton36 As RadioButton
    Friend WithEvents Panel19 As Panel
    Friend WithEvents RadioButton37 As RadioButton
    Friend WithEvents RadioButton38 As RadioButton
    Friend WithEvents Panel20 As Panel
    Friend WithEvents RadioButton39 As RadioButton
    Friend WithEvents RadioButton40 As RadioButton
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Panel10 As Panel
    Friend WithEvents RadioButton19 As RadioButton
    Friend WithEvents RadioButton20 As RadioButton
    Friend WithEvents Panel8 As Panel
    Friend WithEvents RadioButton15 As RadioButton
    Friend WithEvents RadioButton16 As RadioButton
    Friend WithEvents Panel7 As Panel
    Friend WithEvents RadioButton13 As RadioButton
    Friend WithEvents RadioButton14 As RadioButton
    Friend WithEvents Panel9 As Panel
    Friend WithEvents RadioButton17 As RadioButton
    Friend WithEvents RadioButton18 As RadioButton
    Friend WithEvents Panel6 As Panel
    Friend WithEvents RadioButton11 As RadioButton
    Friend WithEvents RadioButton12 As RadioButton
    Friend WithEvents Panel5 As Panel
    Friend WithEvents RadioButton9 As RadioButton
    Friend WithEvents RadioButton10 As RadioButton
    Friend WithEvents Panel4 As Panel
    Friend WithEvents RadioButton7 As RadioButton
    Friend WithEvents RadioButton8 As RadioButton
    Friend WithEvents Panel3 As Panel
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents RadioButton6 As RadioButton
    Friend WithEvents Panel2 As Panel
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Panel21 As Panel
    Friend WithEvents RadioButton41 As RadioButton
    Friend WithEvents RadioButton42 As RadioButton
    Friend WithEvents Panel22 As Panel
    Friend WithEvents RadioButton43 As RadioButton
    Friend WithEvents RadioButton44 As RadioButton
    Friend WithEvents Panel23 As Panel
    Friend WithEvents RadioButton45 As RadioButton
    Friend WithEvents RadioButton46 As RadioButton
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents TextBox10 As TextBox
    Friend WithEvents TextBox11 As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn2 As DataGridViewLinkColumn
    Friend WithEvents Button3 As Button
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents TextBox27 As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents TextBox28 As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents TextBox29 As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents TextBox30 As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents TextBox31 As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents TextBox32 As TextBox
    Friend WithEvents Label37 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents TextBox20 As TextBox
    Friend WithEvents TextBox21 As TextBox
    Friend WithEvents TextBox22 As TextBox
    Friend WithEvents TextBox23 As TextBox
    Friend WithEvents TextBox24 As TextBox
    Friend WithEvents TextBox25 As TextBox
    Friend WithEvents TextBox26 As TextBox
    Friend WithEvents TextBox19 As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents TextBox18 As TextBox
    Friend WithEvents TextBox17 As TextBox
    Friend WithEvents TextBox16 As TextBox
    Friend WithEvents TextBox15 As TextBox
    Friend WithEvents TextBox14 As TextBox
    Friend WithEvents TextBox13 As TextBox
    Friend WithEvents TextBox12 As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DataGridViewTextBoxColumn
    Friend WithEvents Button4 As Button
    Friend WithEvents TextBox36 As TextBox
    Friend WithEvents Label43 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents TextBox35 As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents TextBox34 As TextBox
    Friend WithEvents TextBox33 As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents TextBox38 As TextBox
    Friend WithEvents Label44 As Label
    Friend WithEvents TextBox37 As TextBox
    Friend WithEvents Label45 As Label
    Friend WithEvents TextBox42 As TextBox
    Friend WithEvents TextBox41 As TextBox
    Friend WithEvents Label47 As Label
    Friend WithEvents TextBox40 As TextBox
    Friend WithEvents TextBox39 As TextBox
    Friend WithEvents Label46 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents TextBox49 As TextBox
    Friend WithEvents Button5 As Button
    Friend WithEvents TextBox46 As TextBox
    Friend WithEvents Label53 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents TextBox47 As TextBox
    Friend WithEvents Label55 As Label
    Friend WithEvents Label56 As Label
    Friend WithEvents TextBox48 As TextBox
    Friend WithEvents Label57 As Label
    Friend WithEvents TextBox45 As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents TextBox44 As TextBox
    Friend WithEvents Label51 As Label
    Friend WithEvents TextBox43 As TextBox
    Friend WithEvents Label50 As Label
    Friend WithEvents CheckBox9 As CheckBox
    Friend WithEvents CheckBox10 As CheckBox
    Friend WithEvents CheckBox11 As CheckBox
    Friend WithEvents CheckBox12 As CheckBox
    Friend WithEvents CheckBox13 As CheckBox
    Friend WithEvents CheckBox14 As CheckBox
    Friend WithEvents CheckBox6 As CheckBox
    Friend WithEvents CheckBox5 As CheckBox
    Friend WithEvents CheckBox4 As CheckBox
    Friend WithEvents CheckBox3 As CheckBox
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents TextBox50 As TextBox
    Friend WithEvents Label59 As Label
    Friend WithEvents TextBox58 As TextBox
    Friend WithEvents Label67 As Label
    Friend WithEvents TextBox57 As TextBox
    Friend WithEvents Label66 As Label
    Friend WithEvents TextBox56 As TextBox
    Friend WithEvents Label65 As Label
    Friend WithEvents TextBox55 As TextBox
    Friend WithEvents Label64 As Label
    Friend WithEvents TextBox54 As TextBox
    Friend WithEvents Label63 As Label
    Friend WithEvents TextBox53 As TextBox
    Friend WithEvents Label62 As Label
    Friend WithEvents TextBox52 As TextBox
    Friend WithEvents Label61 As Label
    Friend WithEvents TextBox51 As TextBox
    Friend WithEvents Label60 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents TextBox59 As TextBox
    Friend WithEvents TextBox60 As TextBox
    Friend WithEvents TextBox61 As TextBox
    Friend WithEvents TextBox62 As TextBox
    Friend WithEvents TextBox63 As TextBox
    Friend WithEvents TextBox64 As TextBox
    Friend WithEvents TextBox65 As TextBox
    Friend WithEvents TextBox66 As TextBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents TextBox67 As TextBox
    Friend WithEvents TextBox68 As TextBox
    Friend WithEvents TextBox69 As TextBox
    Friend WithEvents TextBox70 As TextBox
    Friend WithEvents TextBox71 As TextBox
    Friend WithEvents TextBox72 As TextBox
    Friend WithEvents TextBox73 As TextBox
    Friend WithEvents Label68 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents Label70 As Label
    Friend WithEvents Label71 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents Label73 As Label
    Friend WithEvents Label74 As Label
    Friend WithEvents Label75 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents TextBox74 As TextBox
    Friend WithEvents Label78 As Label
    Friend WithEvents TextBox76 As TextBox
    Friend WithEvents Label77 As Label
    Friend WithEvents TextBox75 As TextBox
End Class
