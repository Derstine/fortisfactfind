﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xWillInstruction
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xWillInstruction))
        Dim CustomHeaderButtonImageOptions1 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions2 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.NavigationPane1 = New DevExpress.XtraBars.Navigation.NavigationPane()
        Me.NavigationPageClientDetails = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationFrame1 = New DevExpress.XtraBars.Navigation.NavigationFrame()
        Me.NavigationPageAddNewClient = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditMs2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMiss2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMrs2 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup2 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckEdit9 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem2 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleSeparator2 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditMs = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMiss = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMr = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMrs = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit5 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem1 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleSeparator1 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditDual = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditSingle = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.NavigationPage4 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPageRecommendationTool = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.TabPane1 = New DevExpress.XtraBars.Navigation.TabPane()
        Me.TabNavigationPage1 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit12 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit11 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit13 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit14 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit15 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit16 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit17 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit18 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit19 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit20 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit21 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit22 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit23 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit24 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit25 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit26 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit27 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit28 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem44 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem45 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem46 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem47 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TabNavigationPage2 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.ComboBoxEdit3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumnAssetType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnDetail = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnClient1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnClient2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnJointTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnAction = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TabNavigationPage3 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit29 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit30 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit15 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit16 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit17 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit18 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem48 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem49 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem50 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem51 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem52 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem53 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem54 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TabNavigationPage4 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.CheckEdit41 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit42 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit40 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit39 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit39 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit42 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit47 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit46 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit45 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit44 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit43 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit41 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit38 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit37 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit36 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit35 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit31 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit20 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit32 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit21 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit33 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit25 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit26 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit27 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit28 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit29 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit30 = New DevExpress.XtraEditors.TextEdit()
        Me.ButtonEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit34 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit35 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit36 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit37 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit38 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit31 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit32 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem55 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem56 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem57 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem58 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem59 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem60 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem67 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem68 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem69 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem70 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem71 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem72 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem73 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem74 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem75 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem76 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem77 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem79 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem78 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem80 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit34 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit33 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit40 = New DevExpress.XtraEditors.CheckEdit()
        Me.TabNavigationPage5 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.LayoutControl6 = New DevExpress.XtraLayout.LayoutControl()
        Me.TrackBarControl2 = New DevExpress.XtraEditors.TrackBarControl()
        Me.TrackBarControl1 = New DevExpress.XtraEditors.TrackBarControl()
        Me.TextEdit22 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit23 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit24 = New DevExpress.XtraEditors.TextEdit()
        Me.ButtonEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem61 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem62 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem63 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem65 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem3 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.SimpleLabelItem4 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem64 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.SimpleLabelItem5 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem66 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.NavigationPageWillInstruction = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit46 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit44 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit45 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit43 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl8 = New DevExpress.XtraLayout.LayoutControl()
        Me.MemoEdit4 = New DevExpress.XtraEditors.MemoEdit()
        Me.DateEdit4 = New DevExpress.XtraEditors.DateEdit()
        Me.TextEdit50 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit51 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem85 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem86 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem87 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem88 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl7 = New DevExpress.XtraLayout.LayoutControl()
        Me.MemoEdit3 = New DevExpress.XtraEditors.MemoEdit()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.TextEdit48 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit49 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem81 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem82 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem83 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem84 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumnName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnDOB = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnRelationShipToClient1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnRelationshipToClient2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnActionInWillInstruction = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControl9 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit47 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit48 = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton7 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem89 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem90 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem91 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LayoutControl10 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit49 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit50 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit51 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit52 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit53 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit54 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit55 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit56 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit57 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit58 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit59 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit60 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit61 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit62 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit63 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit64 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit65 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit66 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit67 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit68 = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton8 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlItem110 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem92 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem93 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem94 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem95 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem96 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem97 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem98 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem11 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem99 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem100 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem12 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem101 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem102 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem103 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem104 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem105 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem13 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem106 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem107 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem108 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem109 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem111 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem14 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem112 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton9 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControl12 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit72 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit73 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit74 = New DevExpress.XtraEditors.CheckEdit()
        Me.MemoEdit6 = New DevExpress.XtraEditors.MemoEdit()
        Me.CheckEdit77 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit78 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem116 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem15 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem117 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem118 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem7 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.EmptySpaceItem19 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem119 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem123 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem124 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControl11 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit69 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit70 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit71 = New DevExpress.XtraEditors.CheckEdit()
        Me.MemoEdit5 = New DevExpress.XtraEditors.MemoEdit()
        Me.CheckEdit75 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit76 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem113 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem114 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem115 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem6 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.EmptySpaceItem17 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem121 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem120 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem122 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton10 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControl13 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit79 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit80 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup12 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem125 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem126 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage7 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage12 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabPage11 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl6 = New DevExpress.XtraGrid.GridControl()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit52 = New DevExpress.XtraEditors.TextEdit()
        Me.SeparatorControl1 = New DevExpress.XtraEditors.SeparatorControl()
        Me.CheckEdit84 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit53 = New DevExpress.XtraEditors.TextEdit()
        Me.SeparatorControl2 = New DevExpress.XtraEditors.SeparatorControl()
        Me.CheckEdit85 = New DevExpress.XtraEditors.CheckEdit()
        Me.SeparatorControl3 = New DevExpress.XtraEditors.SeparatorControl()
        Me.CheckEdit86 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit87 = New DevExpress.XtraEditors.CheckEdit()
        Me.SeparatorControl4 = New DevExpress.XtraEditors.SeparatorControl()
        Me.CheckEdit88 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit89 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.SeparatorControl5 = New DevExpress.XtraEditors.SeparatorControl()
        Me.CheckEdit90 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit91 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.SeparatorControl6 = New DevExpress.XtraEditors.SeparatorControl()
        Me.CheckEdit92 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit93 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit94 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit95 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit96 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit97 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControl14 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit81 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit82 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit83 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup13 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem127 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem128 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem129 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage8 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabPage9 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabPage10 = New DevExpress.XtraTab.XtraTabPage()
        Me.NavigationPageLPAInstruction = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.XtraTabControl3 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage13 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl16 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit56 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit6 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit8 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit57 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit100 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit101 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup15 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem136 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem137 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem138 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem21 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem139 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem22 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem140 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem141 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem23 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl15 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit54 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit5 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit7 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit55 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit98 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit99 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup14 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem130 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem131 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem132 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem133 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem16 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem18 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem134 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem135 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem20 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage14 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton11 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit9 = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit108 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit109 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit110 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit111 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl7 = New DevExpress.XtraGrid.GridControl()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckEdit102 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit103 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit104 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit105 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit106 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit107 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage15 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton12 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl8 = New DevExpress.XtraGrid.GridControl()
        Me.GridView8 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckEdit112 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit113 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage16 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl17 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit58 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit59 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit60 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup3 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit10 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit61 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit62 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit7 = New DevExpress.XtraEditors.DateEdit()
        Me.CheckEdit121 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup16 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem142 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem143 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem144 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem145 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem146 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem147 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem148 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem149 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem24 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem25 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem26 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem150 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem8 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit114 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit115 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit116 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit117 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit118 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit119 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit120 = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabPage17 = New DevExpress.XtraTab.XtraTabPage()
        Me.MemoEdit11 = New DevExpress.XtraEditors.MemoEdit()
        Me.XtraTabPage18 = New DevExpress.XtraTab.XtraTabPage()
        Me.MemoEdit12 = New DevExpress.XtraEditors.MemoEdit()
        Me.XtraTabPage19 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton14 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl9 = New DevExpress.XtraGrid.GridControl()
        Me.GridView9 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage20 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl18 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit122 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit123 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit124 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit125 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit126 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit127 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit128 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit129 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit130 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit63 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup4 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit13 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit64 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit65 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit8 = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup17 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem151 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem152 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem153 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem154 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem155 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem27 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem156 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem157 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem158 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem159 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem160 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem161 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem28 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem162 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem163 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem164 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem29 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem30 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem31 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem165 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem32 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage21 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.MemoEdit14 = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl10 = New DevExpress.XtraGrid.GridControl()
        Me.GridView10 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn31 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.NavigationPageFAPTInstruction = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.XtraTabControl4 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage22 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl20 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit68 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit10 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit16 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit69 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup19 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem36 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem37 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem170 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem171 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem172 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem173 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem38 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl19 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit66 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit9 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit15 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit67 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup18 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem33 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem34 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem166 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem167 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem168 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem169 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem35 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage23 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton17 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton16 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControl22 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridControl12 = New DevExpress.XtraGrid.GridControl()
        Me.GridView12 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn37 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn38 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn39 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn40 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckEdit136 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit137 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit138 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit139 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit140 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup21 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem180 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem181 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem182 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem183 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem184 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem185 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.NavigationPageTrusteeInformation = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.LayoutControl21 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridControl11 = New DevExpress.XtraGrid.GridControl()
        Me.GridView11 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn34 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn35 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn36 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckEdit131 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit132 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit133 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit134 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit135 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup20 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem174 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem175 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem176 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem177 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem178 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem179 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage24 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButtonLookup5 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit70 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit71 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit72 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit141 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit142 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton18 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton19 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl13 = New DevExpress.XtraGrid.GridControl()
        Me.GridView13 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn41 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn42 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn43 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn44 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn45 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage25 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl5 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage29 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl14 = New DevExpress.XtraGrid.GridControl()
        Me.GridView14 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn46 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn47 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn48 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn49 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn50 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn51 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton21 = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage30 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl15 = New DevExpress.XtraGrid.GridControl()
        Me.GridView15 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn52 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn53 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn54 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn55 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn56 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton22 = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage31 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl16 = New DevExpress.XtraGrid.GridControl()
        Me.GridView16 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn57 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn58 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn59 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn60 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn61 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton23 = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage26 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl23 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit143 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit144 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit145 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit146 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit147 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit148 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit149 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit150 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit151 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit152 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup22 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem39 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.SimpleLabelItem9 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem186 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem187 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem188 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem189 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem190 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem191 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem192 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem193 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem194 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem195 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage27 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl17 = New DevExpress.XtraGrid.GridControl()
        Me.GridView17 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn62 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn63 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn64 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage28 = New DevExpress.XtraTab.XtraTabPage()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit17 = New DevExpress.XtraEditors.MemoEdit()
        Me.GridControl18 = New DevExpress.XtraGrid.GridControl()
        Me.GridView18 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn65 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn66 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn67 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.NavigationPageDigitalSignatures = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPageProbatePlan = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.XtraTabControl6 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage32 = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEdit153 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit154 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl61 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControl24 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit73 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit11 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit18 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit74 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup23 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup24 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem196 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem197 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem198 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem199 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem40 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControl25 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit75 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit12 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit19 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit77 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup25 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup26 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem200 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem201 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem202 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem203 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem41 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage33 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl26 = New DevExpress.XtraLayout.LayoutControl()
        Me.MemoEdit20 = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup27 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem204 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.NavigationPageProbateAssessment = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.XtraTabControl7 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage42 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl26 = New DevExpress.XtraGrid.GridControl()
        Me.GridView26 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn101 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn102 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn103 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn104 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn105 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton33 = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckEdit160 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit159 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl62 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage34 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl27 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit78 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit76 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton24 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton25 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit21 = New DevExpress.XtraEditors.MemoEdit()
        Me.CheckEdit155 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit156 = New DevExpress.XtraEditors.CheckEdit()
        Me.DateEdit13 = New DevExpress.XtraEditors.DateEdit()
        Me.CheckEdit157 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit158 = New DevExpress.XtraEditors.CheckEdit()
        Me.DateEdit14 = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup28 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem205 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem42 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem206 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem207 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem208 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem209 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem210 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem211 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem212 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem213 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem214 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem215 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage35 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton26 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl19 = New DevExpress.XtraGrid.GridControl()
        Me.GridView19 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn68 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn69 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn70 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn71 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn72 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage36 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton27 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl20 = New DevExpress.XtraGrid.GridControl()
        Me.GridView20 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn73 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn74 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn75 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn76 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn77 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage37 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton28 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl21 = New DevExpress.XtraGrid.GridControl()
        Me.GridView21 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn78 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn79 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn80 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn81 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn82 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage38 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton29 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl22 = New DevExpress.XtraGrid.GridControl()
        Me.GridView22 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn83 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn84 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn86 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn87 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage39 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton30 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl23 = New DevExpress.XtraGrid.GridControl()
        Me.GridView23 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn85 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn88 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn89 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn90 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage40 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton31 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl24 = New DevExpress.XtraGrid.GridControl()
        Me.GridView24 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn91 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn92 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn93 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn94 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn95 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage41 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl25 = New DevExpress.XtraGrid.GridControl()
        Me.GridView25 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn96 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn97 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn98 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn99 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn100 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton32 = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage43 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl28 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit79 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit80 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit81 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit82 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit83 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup29 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem216 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem217 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem218 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem219 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem220 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem43 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.SimpleLabelItem10 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.XtraTabPage44 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl29 = New DevExpress.XtraLayout.LayoutControl()
        Me.MemoEdit22 = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup30 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem221 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LabelControl63 = New DevExpress.XtraEditors.LabelControl()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage2 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPane1.SuspendLayout
        Me.NavigationPageClientDetails.SuspendLayout
        CType(Me.NavigationFrame1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationFrame1.SuspendLayout
        Me.NavigationPageAddNewClient.SuspendLayout
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl2.SuspendLayout
        CType(Me.TextEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit10.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit13.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMs2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMiss2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMrs2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit14.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleSeparator2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl1.SuspendLayout
        CType(Me.TextEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMs.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMiss.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMr.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMrs.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleSeparator1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditDual.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditSingle.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageRecommendationTool.SuspendLayout
        CType(Me.TabPane1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabPane1.SuspendLayout
        Me.TabNavigationPage1.SuspendLayout
        CType(Me.LayoutControl3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl3.SuspendLayout
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit13.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit14.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit15.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit16.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit17.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit18.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit19.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit20.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit21.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit22.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit23.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit24.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit25.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit26.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit27.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit28.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem30,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem31,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem32,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem33,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem34,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem35,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem36,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem37,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem38,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem39,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem40,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem41,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem42,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem43,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem44,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem45,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem46,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem47,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabNavigationPage2.SuspendLayout
        CType(Me.ComboBoxEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabNavigationPage3.SuspendLayout
        CType(Me.LayoutControl4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl4.SuspendLayout
        CType(Me.CheckEdit29.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit30.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit15.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit16.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit17.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit18.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem48,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem49,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem50,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem51,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem52,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem53,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem54,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabNavigationPage4.SuspendLayout
        CType(Me.CheckEdit41.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit42.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit40.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit39.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit39.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit42.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit47.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit46.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit45.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit44.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit43.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit41.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit38.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit37.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit36.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit35.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl5,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl5.SuspendLayout
        CType(Me.TextEdit19.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit31.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit20.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit32.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit21.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit33.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit25.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit26.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit27.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit28.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit29.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit30.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ButtonEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit34.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit35.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit36.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit37.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit38.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit31.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit32.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem55,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem56,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem57,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem58,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem59,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem60,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem67,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem68,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem69,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem70,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem71,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem72,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem73,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem74,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem75,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem76,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem77,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem79,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem78,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem80,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit34.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit33.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit40.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabNavigationPage5.SuspendLayout
        CType(Me.LayoutControl6,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl6.SuspendLayout
        CType(Me.TrackBarControl2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TrackBarControl2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TrackBarControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TrackBarControl1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit22.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit23.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit24.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ButtonEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem61,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem62,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem63,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem65,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem64,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem66,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageWillInstruction.SuspendLayout
        CType(Me.XtraTabControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl1.SuspendLayout
        Me.XtraTabPage1.SuspendLayout
        CType(Me.ComboBoxEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit46.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit44.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit45.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit43.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GroupControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl2.SuspendLayout
        CType(Me.LayoutControl8,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl8.SuspendLayout
        CType(Me.MemoEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit4.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit50.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit51.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem85,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem86,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem87,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem88,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GroupControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl1.SuspendLayout
        CType(Me.LayoutControl7,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl7.SuspendLayout
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit3.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit48.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit49.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem81,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem82,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem83,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem84,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage2.SuspendLayout
        CType(Me.GridControl2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl9,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl9.SuspendLayout
        CType(Me.CheckEdit47.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit48.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem89,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem90,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem91,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage3.SuspendLayout
        CType(Me.GridControl3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl10,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl10.SuspendLayout
        CType(Me.CheckEdit49.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit50.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit51.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit52.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit53.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit54.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit55.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit56.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit57.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit58.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit59.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit60.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit61.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit62.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit63.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit64.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit65.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit66.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit67.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit68.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem110,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem92,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem93,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem94,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem95,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem96,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem97,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem98,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem99,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem100,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem101,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem102,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem103,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem104,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem105,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem106,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem107,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem108,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem109,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem111,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem112,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage4.SuspendLayout
        CType(Me.GridControl4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage5.SuspendLayout
        CType(Me.LayoutControl12,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl12.SuspendLayout
        CType(Me.CheckEdit72.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit73.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit74.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit77.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit78.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem116,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem117,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem118,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem119,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem123,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem124,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl11,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl11.SuspendLayout
        CType(Me.CheckEdit69.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit70.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit71.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit75.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit76.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem113,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem114,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem115,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem121,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem120,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem122,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage6.SuspendLayout
        CType(Me.GridControl5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl13,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl13.SuspendLayout
        CType(Me.CheckEdit79.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit80.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem125,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem126,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage7.SuspendLayout
        CType(Me.XtraTabControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl2.SuspendLayout
        Me.XtraTabPage11.SuspendLayout
        CType(Me.GridControl6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit52.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SeparatorControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit84.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit53.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SeparatorControl2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit85.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SeparatorControl3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit86.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit87.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SeparatorControl4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit88.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit89.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SeparatorControl5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit90.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit91.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SeparatorControl6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit92.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit93.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit94.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit95.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit96.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit97.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl14,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl14.SuspendLayout
        CType(Me.CheckEdit81.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit82.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit83.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem127,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem128,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem129,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageLPAInstruction.SuspendLayout
        CType(Me.XtraTabControl3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl3.SuspendLayout
        Me.XtraTabPage13.SuspendLayout
        CType(Me.GroupControl4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl4.SuspendLayout
        CType(Me.LayoutControl16,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl16.SuspendLayout
        CType(Me.TextEdit56.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit6.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit57.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit100.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit101.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem136,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem137,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem138,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem139,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem140,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem141,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GroupControl3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl3.SuspendLayout
        CType(Me.LayoutControl15,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl15.SuspendLayout
        CType(Me.TextEdit54.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit5.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit55.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit98.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit99.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem130,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem131,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem132,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem133,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem16,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem134,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem135,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem20,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage14.SuspendLayout
        CType(Me.MemoEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit108.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit109.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit110.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit111.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit102.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit103.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit104.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit105.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit106.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit107.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage15.SuspendLayout
        CType(Me.GridControl8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit112.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit113.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage16.SuspendLayout
        CType(Me.LayoutControl17,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl17.SuspendLayout
        CType(Me.TextEdit58.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit59.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit60.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit10.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit61.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit62.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit7.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit121.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup16,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem142,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem143,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem144,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem145,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem146,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem147,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem148,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem149,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem25,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem150,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit114.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit115.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit116.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit117.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit118.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit119.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit120.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage17.SuspendLayout
        CType(Me.MemoEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage18.SuspendLayout
        CType(Me.MemoEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage19.SuspendLayout
        CType(Me.GridControl9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView9,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage20.SuspendLayout
        CType(Me.LayoutControl18,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl18.SuspendLayout
        CType(Me.CheckEdit122.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit123.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit124.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit125.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit126.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit127.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit128.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit129.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit130.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit63.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit13.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit64.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit65.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit8.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem151,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem152,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem153,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem154,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem155,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem27,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem156,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem157,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem158,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem159,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem160,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem161,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem28,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem162,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem163,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem164,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem29,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem30,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem31,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem165,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem32,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage21.SuspendLayout
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelControl1.SuspendLayout
        CType(Me.MemoEdit14.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView10,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageFAPTInstruction.SuspendLayout
        CType(Me.XtraTabControl4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl4.SuspendLayout
        Me.XtraTabPage22.SuspendLayout
        CType(Me.GroupControl6,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl6.SuspendLayout
        CType(Me.LayoutControl20,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl20.SuspendLayout
        CType(Me.TextEdit68.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit10.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit10.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit16.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit69.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem36,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem37,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem170,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem171,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem172,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem173,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem38,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GroupControl5,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl5.SuspendLayout
        CType(Me.LayoutControl19,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl19.SuspendLayout
        CType(Me.TextEdit66.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit9.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit15.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit67.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem33,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem34,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem166,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem167,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem168,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem169,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem35,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage23.SuspendLayout
        CType(Me.LayoutControl22,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl22.SuspendLayout
        CType(Me.GridControl12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit136.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit137.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit138.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit139.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit140.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem180,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem181,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem182,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem183,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem184,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem185,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageTrusteeInformation.SuspendLayout
        CType(Me.LayoutControl21,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl21.SuspendLayout
        CType(Me.GridControl11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit131.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit132.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit133.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit134.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit135.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup20,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem174,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem175,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem176,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem177,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem178,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem179,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage24.SuspendLayout
        CType(Me.TextEdit70.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit71.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit72.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit141.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit142.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView13,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage25.SuspendLayout
        CType(Me.XtraTabControl5,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl5.SuspendLayout
        Me.XtraTabPage29.SuspendLayout
        CType(Me.GridControl14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView14,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage30.SuspendLayout
        CType(Me.GridControl15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView15,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage31.SuspendLayout
        CType(Me.GridControl16,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView16,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage26.SuspendLayout
        CType(Me.LayoutControl23,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl23.SuspendLayout
        CType(Me.CheckEdit143.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit144.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit145.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit146.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit147.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit148.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit149.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit150.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit151.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit152.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem39,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem186,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem187,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem188,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem189,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem190,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem191,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem192,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem193,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem194,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem195,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage27.SuspendLayout
        CType(Me.GridControl17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView17,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage28.SuspendLayout
        CType(Me.PanelControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelControl2.SuspendLayout
        CType(Me.MemoEdit17.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView18,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageProbatePlan.SuspendLayout
        CType(Me.XtraTabControl6,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl6.SuspendLayout
        Me.XtraTabPage32.SuspendLayout
        CType(Me.CheckEdit153.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit154.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl24,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl24.SuspendLayout
        CType(Me.TextEdit73.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit11.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit18.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit74.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem196,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem197,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem198,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem199,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem40,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl25,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl25.SuspendLayout
        CType(Me.TextEdit75.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit12.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit19.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit77.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup25,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem200,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem201,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem202,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem203,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem41,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage33.SuspendLayout
        CType(Me.LayoutControl26,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl26.SuspendLayout
        CType(Me.MemoEdit20.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup27,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem204,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageProbateAssessment.SuspendLayout
        CType(Me.XtraTabControl7,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl7.SuspendLayout
        Me.XtraTabPage42.SuspendLayout
        CType(Me.GridControl26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit160.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit159.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage34.SuspendLayout
        CType(Me.LayoutControl27,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl27.SuspendLayout
        CType(Me.TextEdit78.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit76.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit21.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit155.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit156.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit13.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit13.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit157.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit158.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit14.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit14.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup28,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem205,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem42,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem206,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem207,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem208,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem209,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem210,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem211,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem212,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem213,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem214,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem215,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage35.SuspendLayout
        CType(Me.GridControl19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView19,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage36.SuspendLayout
        CType(Me.GridControl20,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView20,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage37.SuspendLayout
        CType(Me.GridControl21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView21,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage38.SuspendLayout
        CType(Me.GridControl22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView22,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage39.SuspendLayout
        CType(Me.GridControl23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView23,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage40.SuspendLayout
        CType(Me.GridControl24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView24,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage41.SuspendLayout
        CType(Me.GridControl25,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView25,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage43.SuspendLayout
        CType(Me.LayoutControl28,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl28.SuspendLayout
        CType(Me.TextEdit79.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit80.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit81.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit82.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit83.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup29,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem216,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem217,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem218,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem219,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem220,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem43,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem10,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage44.SuspendLayout
        CType(Me.LayoutControl29,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl29.SuspendLayout
        CType(Me.MemoEdit22.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup30,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem221,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DockManager1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'NavigationPane1
        '
        Me.NavigationPane1.Controls.Add(Me.NavigationPageClientDetails)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageRecommendationTool)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageWillInstruction)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageLPAInstruction)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageFAPTInstruction)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageDigitalSignatures)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageProbatePlan)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageProbateAssessment)
        Me.NavigationPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPane1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText
        Me.NavigationPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPageClientDetails, Me.NavigationPageRecommendationTool, Me.NavigationPageWillInstruction, Me.NavigationPageLPAInstruction, Me.NavigationPageFAPTInstruction, Me.NavigationPageProbatePlan, Me.NavigationPageDigitalSignatures, Me.NavigationPageProbateAssessment})
        Me.NavigationPane1.RegularSize = New System.Drawing.Size(1240, 662)
        Me.NavigationPane1.SelectedPage = Me.NavigationPageClientDetails
        Me.NavigationPane1.Size = New System.Drawing.Size(1240, 662)
        Me.NavigationPane1.TabIndex = 0
        Me.NavigationPane1.Text = "Client Details"
        '
        'NavigationPageClientDetails
        '
        Me.NavigationPageClientDetails.Caption = "   Client Details"
        Me.NavigationPageClientDetails.Controls.Add(Me.NavigationFrame1)
        Me.NavigationPageClientDetails.ImageOptions.Image = CType(resources.GetObject("NavigationPageClientDetails.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageClientDetails.Name = "NavigationPageClientDetails"
        Me.NavigationPageClientDetails.Size = New System.Drawing.Size(1026, 602)
        '
        'NavigationFrame1
        '
        Me.NavigationFrame1.Controls.Add(Me.NavigationPageAddNewClient)
        Me.NavigationFrame1.Controls.Add(Me.NavigationPage4)
        Me.NavigationFrame1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationFrame1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationFrame1.Name = "NavigationFrame1"
        Me.NavigationFrame1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPageAddNewClient, Me.NavigationPage4})
        Me.NavigationFrame1.SelectedPage = Me.NavigationPageAddNewClient
        Me.NavigationFrame1.Size = New System.Drawing.Size(1026, 602)
        Me.NavigationFrame1.TabIndex = 0
        Me.NavigationFrame1.Text = "NavigationFrame1"
        '
        'NavigationPageAddNewClient
        '
        Me.NavigationPageAddNewClient.Controls.Add(Me.SimpleButton5)
        Me.NavigationPageAddNewClient.Controls.Add(Me.SimpleButton4)
        Me.NavigationPageAddNewClient.Controls.Add(Me.LayoutControl2)
        Me.NavigationPageAddNewClient.Controls.Add(Me.LayoutControl1)
        Me.NavigationPageAddNewClient.Controls.Add(Me.LabelControl1)
        Me.NavigationPageAddNewClient.Controls.Add(Me.CheckEditDual)
        Me.NavigationPageAddNewClient.Controls.Add(Me.CheckEditSingle)
        Me.NavigationPageAddNewClient.Controls.Add(Me.LabelControl2)
        Me.NavigationPageAddNewClient.Name = "NavigationPageAddNewClient"
        Me.NavigationPageAddNewClient.Size = New System.Drawing.Size(1026, 602)
        '
        'SimpleButton5
        '
        Me.SimpleButton5.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton5.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton5.Location = New System.Drawing.Point(904, 8)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(119, 38)
        Me.SimpleButton5.TabIndex = 18
        Me.SimpleButton5.Text = "Clear"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton4.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton4.Location = New System.Drawing.Point(779, 8)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(119, 38)
        Me.SimpleButton4.TabIndex = 17
        Me.SimpleButton4.Text = "Save"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.LayoutControl2.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl2.Controls.Add(Me.TextEdit9)
        Me.LayoutControl2.Controls.Add(Me.TextEdit10)
        Me.LayoutControl2.Controls.Add(Me.TextEdit11)
        Me.LayoutControl2.Controls.Add(Me.MemoEdit2)
        Me.LayoutControl2.Controls.Add(Me.DateEdit2)
        Me.LayoutControl2.Controls.Add(Me.TextEdit12)
        Me.LayoutControl2.Controls.Add(Me.TextEdit13)
        Me.LayoutControl2.Controls.Add(Me.CheckEditMs2)
        Me.LayoutControl2.Controls.Add(Me.CheckEditMiss2)
        Me.LayoutControl2.Controls.Add(Me.CheckEditMrs2)
        Me.LayoutControl2.Controls.Add(Me.TextEdit14)
        Me.LayoutControl2.Controls.Add(Me.SimpleButtonLookup2)
        Me.LayoutControl2.Controls.Add(Me.ComboBoxEdit1)
        Me.LayoutControl2.Controls.Add(Me.CheckEdit9)
        Me.LayoutControl2.Location = New System.Drawing.Point(473, 51)
        Me.LayoutControl2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(542, 546)
        Me.LayoutControl2.TabIndex = 16
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.ImageOptions.Image = CType(resources.GetObject("SimpleButton3.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton3.Location = New System.Drawing.Point(406, 210)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(150, 42)
        Me.SimpleButton3.StyleController = Me.LayoutControl2
        Me.SimpleButton3.TabIndex = 18
        Me.SimpleButton3.Text = "Copy Client 1"
        '
        'TextEdit9
        '
        Me.TextEdit9.Location = New System.Drawing.Point(93, 483)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit9.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit9.Size = New System.Drawing.Size(464, 44)
        Me.TextEdit9.StyleController = Me.LayoutControl2
        Me.TextEdit9.TabIndex = 17
        '
        'TextEdit10
        '
        Me.TextEdit10.Location = New System.Drawing.Point(93, 435)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit10.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit10.Size = New System.Drawing.Size(463, 44)
        Me.TextEdit10.StyleController = Me.LayoutControl2
        Me.TextEdit10.TabIndex = 16
        '
        'TextEdit11
        '
        Me.TextEdit11.Location = New System.Drawing.Point(93, 387)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit11.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit11.Size = New System.Drawing.Size(463, 44)
        Me.TextEdit11.StyleController = Me.LayoutControl2
        Me.TextEdit11.TabIndex = 15
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(93, 306)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Size = New System.Drawing.Size(463, 77)
        Me.MemoEdit2.StyleController = Me.LayoutControl2
        Me.MemoEdit2.TabIndex = 13
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(93, 162)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.ContextImageOptions.Image = CType(resources.GetObject("DateEdit2.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.DateEdit2.Size = New System.Drawing.Size(463, 44)
        Me.DateEdit2.StyleController = Me.LayoutControl2
        Me.DateEdit2.TabIndex = 10
        '
        'TextEdit12
        '
        Me.TextEdit12.Location = New System.Drawing.Point(93, 114)
        Me.TextEdit12.Name = "TextEdit12"
        Me.TextEdit12.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit12.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit12.Size = New System.Drawing.Size(463, 44)
        Me.TextEdit12.StyleController = Me.LayoutControl2
        Me.TextEdit12.TabIndex = 9
        '
        'TextEdit13
        '
        Me.TextEdit13.Location = New System.Drawing.Point(93, 66)
        Me.TextEdit13.Name = "TextEdit13"
        Me.TextEdit13.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit13.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit13.Size = New System.Drawing.Size(463, 44)
        Me.TextEdit13.StyleController = Me.LayoutControl2
        Me.TextEdit13.TabIndex = 8
        '
        'CheckEditMs2
        '
        Me.CheckEditMs2.Location = New System.Drawing.Point(449, 18)
        Me.CheckEditMs2.Name = "CheckEditMs2"
        Me.CheckEditMs2.Properties.Caption = "Ms."
        Me.CheckEditMs2.Size = New System.Drawing.Size(107, 44)
        Me.CheckEditMs2.StyleController = Me.LayoutControl2
        Me.CheckEditMs2.TabIndex = 7
        '
        'CheckEditMiss2
        '
        Me.CheckEditMiss2.Location = New System.Drawing.Point(338, 18)
        Me.CheckEditMiss2.Name = "CheckEditMiss2"
        Me.CheckEditMiss2.Properties.Caption = "Miss"
        Me.CheckEditMiss2.Size = New System.Drawing.Size(107, 44)
        Me.CheckEditMiss2.StyleController = Me.LayoutControl2
        Me.CheckEditMiss2.TabIndex = 6
        '
        'CheckEditMrs2
        '
        Me.CheckEditMrs2.Location = New System.Drawing.Point(227, 18)
        Me.CheckEditMrs2.Name = "CheckEditMrs2"
        Me.CheckEditMrs2.Properties.Caption = "Mrs."
        Me.CheckEditMrs2.Size = New System.Drawing.Size(107, 44)
        Me.CheckEditMrs2.StyleController = Me.LayoutControl2
        Me.CheckEditMrs2.TabIndex = 4
        '
        'TextEdit14
        '
        Me.TextEdit14.Location = New System.Drawing.Point(93, 210)
        Me.TextEdit14.Name = "TextEdit14"
        Me.TextEdit14.Properties.AutoHeight = false
        Me.TextEdit14.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit14.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit14.Properties.Name = "TextEdit4"
        Me.TextEdit14.Size = New System.Drawing.Size(183, 44)
        Me.TextEdit14.StyleController = Me.LayoutControl2
        Me.TextEdit14.TabIndex = 11
        '
        'SimpleButtonLookup2
        '
        Me.SimpleButtonLookup2.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup2.Location = New System.Drawing.Point(280, 210)
        Me.SimpleButtonLookup2.Name = "SimpleButtonLookup2"
        Me.SimpleButtonLookup2.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButtonLookup2.StyleController = Me.LayoutControl2
        Me.SimpleButtonLookup2.TabIndex = 12
        Me.SimpleButtonLookup2.Text = "Lookup"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(93, 258)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.ContextImageOptions.Image = CType(resources.GetObject("ComboBoxEdit1.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"Married", "Single", "Partnered", "Widowed", "Divorced"})
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(463, 44)
        Me.ComboBoxEdit1.StyleController = Me.LayoutControl2
        Me.ComboBoxEdit1.TabIndex = 14
        '
        'CheckEdit9
        '
        Me.CheckEdit9.Location = New System.Drawing.Point(157, 18)
        Me.CheckEdit9.Name = "CheckEdit9"
        Me.CheckEdit9.Properties.Caption = "Mr."
        Me.CheckEdit9.Size = New System.Drawing.Size(66, 44)
        Me.CheckEdit9.StyleController = Me.LayoutControl2
        Me.CheckEdit9.TabIndex = 5
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[False]
        Me.LayoutControlGroup1.GroupBordersVisible = false
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.SimpleLabelItem2, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem27, Me.LayoutControlItem28, Me.SimpleSeparator2, Me.LayoutControlItem29})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(559, 529)
        Me.LayoutControlGroup1.TextVisible = false
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.CheckEditMrs2
        Me.LayoutControlItem15.Location = New System.Drawing.Point(225, 16)
        Me.LayoutControlItem15.Name = "LayoutControlItem1"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(111, 48)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = false
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.CheckEdit9
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 16)
        Me.LayoutControlItem16.Name = "LayoutControlItem2"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(225, 48)
        Me.LayoutControlItem16.Text = "Title"
        Me.LayoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(150, 16)
        Me.LayoutControlItem16.TextToControlDistance = 5
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.CheckEditMiss2
        Me.LayoutControlItem17.Location = New System.Drawing.Point(336, 16)
        Me.LayoutControlItem17.Name = "LayoutControlItem3"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(111, 48)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = false
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.CheckEditMs2
        Me.LayoutControlItem18.Location = New System.Drawing.Point(447, 16)
        Me.LayoutControlItem18.Name = "LayoutControlItem4"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(111, 48)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = false
        '
        'SimpleLabelItem2
        '
        Me.SimpleLabelItem2.AllowHotTrack = false
        Me.SimpleLabelItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.SimpleLabelItem2.AppearanceItemCaption.Options.UseFont = true
        Me.SimpleLabelItem2.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem2.MinSize = New System.Drawing.Size(45, 16)
        Me.SimpleLabelItem2.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem2.Size = New System.Drawing.Size(558, 16)
        Me.SimpleLabelItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.SimpleLabelItem2.Text = "Client 2"
        Me.SimpleLabelItem2.TextSize = New System.Drawing.Size(48, 16)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.TextEdit13
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 64)
        Me.LayoutControlItem19.Name = "LayoutControlItem5"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(558, 48)
        Me.LayoutControlItem19.Text = "Forename(s)"
        Me.LayoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem19.TextToControlDistance = 5
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TextEdit12
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 112)
        Me.LayoutControlItem20.Name = "LayoutControlItem6"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(558, 48)
        Me.LayoutControlItem20.Text = "Surname"
        Me.LayoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem20.TextToControlDistance = 5
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.DateEdit2
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 160)
        Me.LayoutControlItem21.Name = "LayoutControlItem7"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(558, 48)
        Me.LayoutControlItem21.Text = "DOB"
        Me.LayoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem21.TextToControlDistance = 5
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.TextEdit14
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 208)
        Me.LayoutControlItem22.Name = "LayoutControlItem8"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(278, 48)
        Me.LayoutControlItem22.Text = "Postcode"
        Me.LayoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(86, 13)
        Me.LayoutControlItem22.TextToControlDistance = 5
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.SimpleButtonLookup2
        Me.LayoutControlItem23.Location = New System.Drawing.Point(278, 208)
        Me.LayoutControlItem23.Name = "LayoutControlItem9"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(126, 48)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = false
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem24.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem24.Control = Me.MemoEdit2
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 304)
        Me.LayoutControlItem24.Name = "LayoutControlItem10"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(558, 81)
        Me.LayoutControlItem24.Text = "Address"
        Me.LayoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem24.TextToControlDistance = 5
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.ComboBoxEdit1
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 256)
        Me.LayoutControlItem25.Name = "LayoutControlItem11"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(558, 48)
        Me.LayoutControlItem25.Text = "Marital Status"
        Me.LayoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem25.TextToControlDistance = 5
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.TextEdit11
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 385)
        Me.LayoutControlItem26.Name = "LayoutControlItem12"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(558, 48)
        Me.LayoutControlItem26.Text = "Mobile Number"
        Me.LayoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem26.TextToControlDistance = 5
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.TextEdit10
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 433)
        Me.LayoutControlItem27.Name = "LayoutControlItem13"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(558, 48)
        Me.LayoutControlItem27.Text = "Home Number"
        Me.LayoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem27.TextToControlDistance = 5
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.TextEdit9
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 481)
        Me.LayoutControlItem28.Name = "LayoutControlItem14"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(559, 48)
        Me.LayoutControlItem28.Text = "Email"
        Me.LayoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem28.TextToControlDistance = 5
        '
        'SimpleSeparator2
        '
        Me.SimpleSeparator2.AllowHotTrack = false
        Me.SimpleSeparator2.Location = New System.Drawing.Point(558, 0)
        Me.SimpleSeparator2.Name = "SimpleSeparator1"
        Me.SimpleSeparator2.Size = New System.Drawing.Size(1, 481)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.SimpleButton3
        Me.LayoutControlItem29.Location = New System.Drawing.Point(404, 208)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(154, 48)
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = false
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.LayoutControl1.Controls.Add(Me.TextEdit8)
        Me.LayoutControl1.Controls.Add(Me.TextEdit7)
        Me.LayoutControl1.Controls.Add(Me.TextEdit6)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit1)
        Me.LayoutControl1.Controls.Add(Me.DateEdit1)
        Me.LayoutControl1.Controls.Add(Me.TextEdit3)
        Me.LayoutControl1.Controls.Add(Me.TextEdit2)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMs)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMiss)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMr)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMrs)
        Me.LayoutControl1.Controls.Add(Me.TextEdit4)
        Me.LayoutControl1.Controls.Add(Me.SimpleButtonLookup1)
        Me.LayoutControl1.Controls.Add(Me.TextEdit5)
        Me.LayoutControl1.Location = New System.Drawing.Point(3, 51)
        Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(448, 549)
        Me.LayoutControl1.TabIndex = 12
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'TextEdit8
        '
        Me.TextEdit8.Location = New System.Drawing.Point(93, 503)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit8.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit8.Size = New System.Drawing.Size(353, 44)
        Me.TextEdit8.StyleController = Me.LayoutControl1
        Me.TextEdit8.TabIndex = 17
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(93, 455)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit7.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit7.Size = New System.Drawing.Size(352, 44)
        Me.TextEdit7.StyleController = Me.LayoutControl1
        Me.TextEdit7.TabIndex = 16
        '
        'TextEdit6
        '
        Me.TextEdit6.Location = New System.Drawing.Point(93, 407)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit6.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit6.Size = New System.Drawing.Size(352, 44)
        Me.TextEdit6.StyleController = Me.LayoutControl1
        Me.TextEdit6.TabIndex = 15
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(93, 307)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(352, 96)
        Me.MemoEdit1.StyleController = Me.LayoutControl1
        Me.MemoEdit1.TabIndex = 13
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(93, 163)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.ContextImageOptions.Image = CType(resources.GetObject("DateEdit1.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.DateEdit1.Size = New System.Drawing.Size(352, 44)
        Me.DateEdit1.StyleController = Me.LayoutControl1
        Me.DateEdit1.TabIndex = 10
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(93, 115)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit3.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit3.Size = New System.Drawing.Size(352, 44)
        Me.TextEdit3.StyleController = Me.LayoutControl1
        Me.TextEdit3.TabIndex = 9
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(93, 67)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit2.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit2.Size = New System.Drawing.Size(352, 44)
        Me.TextEdit2.StyleController = Me.LayoutControl1
        Me.TextEdit2.TabIndex = 8
        '
        'CheckEditMs
        '
        Me.CheckEditMs.Location = New System.Drawing.Point(338, 19)
        Me.CheckEditMs.Name = "CheckEditMs"
        Me.CheckEditMs.Properties.Caption = "Ms."
        Me.CheckEditMs.Size = New System.Drawing.Size(107, 44)
        Me.CheckEditMs.StyleController = Me.LayoutControl1
        Me.CheckEditMs.TabIndex = 7
        '
        'CheckEditMiss
        '
        Me.CheckEditMiss.Location = New System.Drawing.Point(259, 19)
        Me.CheckEditMiss.Name = "CheckEditMiss"
        Me.CheckEditMiss.Properties.Caption = "Miss"
        Me.CheckEditMiss.Size = New System.Drawing.Size(75, 44)
        Me.CheckEditMiss.StyleController = Me.LayoutControl1
        Me.CheckEditMiss.TabIndex = 6
        '
        'CheckEditMr
        '
        Me.CheckEditMr.Location = New System.Drawing.Point(110, 19)
        Me.CheckEditMr.Name = "CheckEditMr"
        Me.CheckEditMr.Properties.Caption = "Mr."
        Me.CheckEditMr.Size = New System.Drawing.Size(68, 44)
        Me.CheckEditMr.StyleController = Me.LayoutControl1
        Me.CheckEditMr.TabIndex = 5
        '
        'CheckEditMrs
        '
        Me.CheckEditMrs.Location = New System.Drawing.Point(182, 19)
        Me.CheckEditMrs.Name = "CheckEditMrs"
        Me.CheckEditMrs.Properties.Caption = "Mrs."
        Me.CheckEditMrs.Size = New System.Drawing.Size(73, 44)
        Me.CheckEditMrs.StyleController = Me.LayoutControl1
        Me.CheckEditMrs.TabIndex = 4
        '
        'TextEdit4
        '
        Me.TextEdit4.Location = New System.Drawing.Point(93, 211)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.AutoHeight = false
        Me.TextEdit4.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit4.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit4.Properties.Name = "TextEdit4"
        Me.TextEdit4.Size = New System.Drawing.Size(215, 44)
        Me.TextEdit4.StyleController = Me.LayoutControl1
        Me.TextEdit4.TabIndex = 11
        '
        'SimpleButtonLookup1
        '
        Me.SimpleButtonLookup1.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup1.Location = New System.Drawing.Point(312, 211)
        Me.SimpleButtonLookup1.Name = "SimpleButtonLookup1"
        Me.SimpleButtonLookup1.Size = New System.Drawing.Size(133, 42)
        Me.SimpleButtonLookup1.StyleController = Me.LayoutControl1
        Me.SimpleButtonLookup1.TabIndex = 12
        Me.SimpleButtonLookup1.Text = "Lookup"
        '
        'TextEdit5
        '
        Me.TextEdit5.Location = New System.Drawing.Point(93, 259)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TextEdit5.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit5.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit5.Properties.Items.AddRange(New Object() {"Married", "Single", "Partnered", "Widowed", "Divorced"})
        Me.TextEdit5.Size = New System.Drawing.Size(352, 44)
        Me.TextEdit5.StyleController = Me.LayoutControl1
        Me.TextEdit5.TabIndex = 14
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[False]
        Me.Root.GroupBordersVisible = false
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.SimpleLabelItem1, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.SimpleSeparator1})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(448, 549)
        Me.Root.TextVisible = false
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.CheckEditMrs
        Me.LayoutControlItem1.Location = New System.Drawing.Point(180, 17)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(77, 48)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = false
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.CheckEditMr
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 17)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(180, 48)
        Me.LayoutControlItem2.Text = "Title"
        Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(103, 16)
        Me.LayoutControlItem2.TextToControlDistance = 5
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.CheckEditMiss
        Me.LayoutControlItem3.Location = New System.Drawing.Point(257, 17)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 48)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = false
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.CheckEditMs
        Me.LayoutControlItem4.Location = New System.Drawing.Point(336, 17)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(111, 48)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = false
        '
        'SimpleLabelItem1
        '
        Me.SimpleLabelItem1.AllowHotTrack = false
        Me.SimpleLabelItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.SimpleLabelItem1.AppearanceItemCaption.Options.UseFont = true
        Me.SimpleLabelItem1.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem1.MinSize = New System.Drawing.Size(45, 16)
        Me.SimpleLabelItem1.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem1.Size = New System.Drawing.Size(447, 17)
        Me.SimpleLabelItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.SimpleLabelItem1.Text = "Client 1"
        Me.SimpleLabelItem1.TextSize = New System.Drawing.Size(48, 16)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.TextEdit2
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 65)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(447, 48)
        Me.LayoutControlItem5.Text = "Forename(s)"
        Me.LayoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem5.TextToControlDistance = 5
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.TextEdit3
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 113)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(447, 48)
        Me.LayoutControlItem6.Text = "Surname"
        Me.LayoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem6.TextToControlDistance = 5
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.DateEdit1
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 161)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(447, 48)
        Me.LayoutControlItem7.Text = "DOB"
        Me.LayoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem7.TextToControlDistance = 5
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.TextEdit4
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 209)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(310, 48)
        Me.LayoutControlItem8.Text = "Postcode"
        Me.LayoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(86, 13)
        Me.LayoutControlItem8.TextToControlDistance = 5
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.SimpleButtonLookup1
        Me.LayoutControlItem9.Location = New System.Drawing.Point(310, 209)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(137, 48)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = false
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem10.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem10.Control = Me.MemoEdit1
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 305)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(447, 100)
        Me.LayoutControlItem10.Text = "Address"
        Me.LayoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem10.TextToControlDistance = 5
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.TextEdit5
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 257)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(447, 48)
        Me.LayoutControlItem11.Text = "Marital Status"
        Me.LayoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem11.TextToControlDistance = 5
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.TextEdit6
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 405)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(447, 48)
        Me.LayoutControlItem12.Text = "Mobile Number"
        Me.LayoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem12.TextToControlDistance = 5
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.TextEdit7
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 453)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(447, 48)
        Me.LayoutControlItem13.Text = "Home Number"
        Me.LayoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem13.TextToControlDistance = 5
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.TextEdit8
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 501)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(448, 48)
        Me.LayoutControlItem14.Text = "Email"
        Me.LayoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem14.TextToControlDistance = 5
        '
        'SimpleSeparator1
        '
        Me.SimpleSeparator1.AllowHotTrack = false
        Me.SimpleSeparator1.Location = New System.Drawing.Point(447, 0)
        Me.SimpleSeparator1.Name = "SimpleSeparator1"
        Me.SimpleSeparator1.Size = New System.Drawing.Size(1, 501)
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(370, 18)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(178, 16)
        Me.LabelControl1.TabIndex = 11
        Me.LabelControl1.Text = "Reference Number: XXXXXXXXX"
        '
        'CheckEditDual
        '
        Me.CheckEditDual.Location = New System.Drawing.Point(206, 4)
        Me.CheckEditDual.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.CheckEditDual.Name = "CheckEditDual"
        Me.CheckEditDual.Properties.Caption = "Dual"
        Me.CheckEditDual.Size = New System.Drawing.Size(121, 44)
        Me.CheckEditDual.TabIndex = 10
        '
        'CheckEditSingle
        '
        Me.CheckEditSingle.Location = New System.Drawing.Point(110, 4)
        Me.CheckEditSingle.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.CheckEditSingle.Name = "CheckEditSingle"
        Me.CheckEditSingle.Properties.Caption = "Single"
        Me.CheckEditSingle.Size = New System.Drawing.Size(90, 44)
        Me.CheckEditSingle.TabIndex = 9
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(3, 18)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(79, 16)
        Me.LabelControl2.TabIndex = 8
        Me.LabelControl2.Text = "Type of Client"
        '
        'NavigationPage4
        '
        Me.NavigationPage4.Caption = "NavigationPage4"
        Me.NavigationPage4.Name = "NavigationPage4"
        Me.NavigationPage4.Size = New System.Drawing.Size(1026, 602)
        '
        'NavigationPageRecommendationTool
        '
        Me.NavigationPageRecommendationTool.Caption = "   Recommendation Tool"
        Me.NavigationPageRecommendationTool.Controls.Add(Me.TabPane1)
        Me.NavigationPageRecommendationTool.ImageOptions.Image = CType(resources.GetObject("NavigationPageRecommendationTool.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageRecommendationTool.Name = "NavigationPageRecommendationTool"
        Me.NavigationPageRecommendationTool.Size = New System.Drawing.Size(1026, 602)
        '
        'TabPane1
        '
        Me.TabPane1.Controls.Add(Me.TabNavigationPage1)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage2)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage3)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage4)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage5)
        Me.TabPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabPane1.Location = New System.Drawing.Point(0, 0)
        Me.TabPane1.Name = "TabPane1"
        Me.TabPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.TabNavigationPage1, Me.TabNavigationPage2, Me.TabNavigationPage3, Me.TabNavigationPage4, Me.TabNavigationPage5})
        Me.TabPane1.RegularSize = New System.Drawing.Size(1026, 602)
        Me.TabPane1.SelectedPage = Me.TabNavigationPage1
        Me.TabPane1.Size = New System.Drawing.Size(1026, 602)
        Me.TabPane1.TabIndex = 0
        Me.TabPane1.Text = "TabPane1"
        '
        'TabNavigationPage1
        '
        Me.TabNavigationPage1.Caption = "Establishing Facts"
        Me.TabNavigationPage1.Controls.Add(Me.LayoutControl3)
        Me.TabNavigationPage1.Name = "TabNavigationPage1"
        Me.TabNavigationPage1.Size = New System.Drawing.Size(1026, 572)
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.CheckEdit12)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit11)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit13)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit14)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit15)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit16)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit17)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit18)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit19)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit20)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit21)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit22)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit23)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit24)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit25)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit26)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit27)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit28)
        Me.LayoutControl3.Location = New System.Drawing.Point(10, 28)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup2
        Me.LayoutControl3.Size = New System.Drawing.Size(1025, 532)
        Me.LayoutControl3.TabIndex = 3
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'CheckEdit12
        '
        Me.CheckEdit12.Location = New System.Drawing.Point(808, 12)
        Me.CheckEdit12.Name = "CheckEdit12"
        Me.CheckEdit12.Properties.Caption = "NO"
        Me.CheckEdit12.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit12.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit12.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit12.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit12.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit12.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit12.StyleController = Me.LayoutControl3
        Me.CheckEdit12.TabIndex = 2
        '
        'CheckEdit11
        '
        Me.CheckEdit11.Location = New System.Drawing.Point(617, 12)
        Me.CheckEdit11.Name = "CheckEdit11"
        Me.CheckEdit11.Properties.Caption = "YES"
        Me.CheckEdit11.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit11.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit11.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit11.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit11.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit11.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit11.StyleController = Me.LayoutControl3
        Me.CheckEdit11.TabIndex = 1
        '
        'CheckEdit13
        '
        Me.CheckEdit13.Location = New System.Drawing.Point(617, 52)
        Me.CheckEdit13.Name = "CheckEdit13"
        Me.CheckEdit13.Properties.Caption = "YES"
        Me.CheckEdit13.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit13.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit13.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit13.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit13.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit13.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit13.StyleController = Me.LayoutControl3
        Me.CheckEdit13.TabIndex = 4
        '
        'CheckEdit14
        '
        Me.CheckEdit14.Location = New System.Drawing.Point(808, 52)
        Me.CheckEdit14.Name = "CheckEdit14"
        Me.CheckEdit14.Properties.Caption = "NO"
        Me.CheckEdit14.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit14.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit14.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit14.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit14.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit14.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit14.StyleController = Me.LayoutControl3
        Me.CheckEdit14.TabIndex = 5
        '
        'CheckEdit15
        '
        Me.CheckEdit15.Location = New System.Drawing.Point(617, 92)
        Me.CheckEdit15.Name = "CheckEdit15"
        Me.CheckEdit15.Properties.Caption = "YES"
        Me.CheckEdit15.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit15.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit15.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit15.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit15.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit15.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit15.StyleController = Me.LayoutControl3
        Me.CheckEdit15.TabIndex = 6
        '
        'CheckEdit16
        '
        Me.CheckEdit16.Location = New System.Drawing.Point(808, 92)
        Me.CheckEdit16.Name = "CheckEdit16"
        Me.CheckEdit16.Properties.Caption = "NO"
        Me.CheckEdit16.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit16.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit16.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit16.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit16.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit16.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit16.StyleController = Me.LayoutControl3
        Me.CheckEdit16.TabIndex = 7
        '
        'CheckEdit17
        '
        Me.CheckEdit17.Location = New System.Drawing.Point(617, 132)
        Me.CheckEdit17.Name = "CheckEdit17"
        Me.CheckEdit17.Properties.Caption = "YES"
        Me.CheckEdit17.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit17.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit17.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit17.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit17.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit17.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit17.StyleController = Me.LayoutControl3
        Me.CheckEdit17.TabIndex = 8
        '
        'CheckEdit18
        '
        Me.CheckEdit18.Location = New System.Drawing.Point(808, 132)
        Me.CheckEdit18.Name = "CheckEdit18"
        Me.CheckEdit18.Properties.Caption = "NO"
        Me.CheckEdit18.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit18.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit18.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit18.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit18.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit18.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit18.StyleController = Me.LayoutControl3
        Me.CheckEdit18.TabIndex = 9
        '
        'CheckEdit19
        '
        Me.CheckEdit19.Location = New System.Drawing.Point(617, 172)
        Me.CheckEdit19.Name = "CheckEdit19"
        Me.CheckEdit19.Properties.Caption = "YES"
        Me.CheckEdit19.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit19.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit19.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit19.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit19.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit19.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit19.StyleController = Me.LayoutControl3
        Me.CheckEdit19.TabIndex = 10
        '
        'CheckEdit20
        '
        Me.CheckEdit20.Location = New System.Drawing.Point(808, 172)
        Me.CheckEdit20.Name = "CheckEdit20"
        Me.CheckEdit20.Properties.Caption = "NO"
        Me.CheckEdit20.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit20.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit20.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit20.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit20.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit20.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit20.StyleController = Me.LayoutControl3
        Me.CheckEdit20.TabIndex = 11
        '
        'CheckEdit21
        '
        Me.CheckEdit21.Location = New System.Drawing.Point(617, 212)
        Me.CheckEdit21.Name = "CheckEdit21"
        Me.CheckEdit21.Properties.Caption = "YES"
        Me.CheckEdit21.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit21.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit21.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit21.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit21.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit21.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit21.StyleController = Me.LayoutControl3
        Me.CheckEdit21.TabIndex = 12
        '
        'CheckEdit22
        '
        Me.CheckEdit22.Location = New System.Drawing.Point(808, 212)
        Me.CheckEdit22.Name = "CheckEdit22"
        Me.CheckEdit22.Properties.Caption = "NO"
        Me.CheckEdit22.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit22.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit22.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit22.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit22.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit22.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit22.StyleController = Me.LayoutControl3
        Me.CheckEdit22.TabIndex = 13
        '
        'CheckEdit23
        '
        Me.CheckEdit23.Location = New System.Drawing.Point(617, 252)
        Me.CheckEdit23.Name = "CheckEdit23"
        Me.CheckEdit23.Properties.Caption = "YES"
        Me.CheckEdit23.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit23.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit23.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit23.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit23.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit23.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit23.StyleController = Me.LayoutControl3
        Me.CheckEdit23.TabIndex = 14
        '
        'CheckEdit24
        '
        Me.CheckEdit24.Location = New System.Drawing.Point(617, 292)
        Me.CheckEdit24.Name = "CheckEdit24"
        Me.CheckEdit24.Properties.Caption = "YES"
        Me.CheckEdit24.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit24.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit24.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit24.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit24.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit24.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit24.StyleController = Me.LayoutControl3
        Me.CheckEdit24.TabIndex = 15
        '
        'CheckEdit25
        '
        Me.CheckEdit25.Location = New System.Drawing.Point(617, 332)
        Me.CheckEdit25.Name = "CheckEdit25"
        Me.CheckEdit25.Properties.Caption = "YES"
        Me.CheckEdit25.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit25.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit25.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit25.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit25.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit25.Size = New System.Drawing.Size(187, 36)
        Me.CheckEdit25.StyleController = Me.LayoutControl3
        Me.CheckEdit25.TabIndex = 16
        '
        'CheckEdit26
        '
        Me.CheckEdit26.Location = New System.Drawing.Point(808, 252)
        Me.CheckEdit26.Name = "CheckEdit26"
        Me.CheckEdit26.Properties.Caption = "NO"
        Me.CheckEdit26.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit26.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit26.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit26.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit26.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit26.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit26.StyleController = Me.LayoutControl3
        Me.CheckEdit26.TabIndex = 17
        '
        'CheckEdit27
        '
        Me.CheckEdit27.Location = New System.Drawing.Point(808, 292)
        Me.CheckEdit27.Name = "CheckEdit27"
        Me.CheckEdit27.Properties.Caption = "NO"
        Me.CheckEdit27.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit27.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit27.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit27.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit27.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit27.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit27.StyleController = Me.LayoutControl3
        Me.CheckEdit27.TabIndex = 18
        '
        'CheckEdit28
        '
        Me.CheckEdit28.Location = New System.Drawing.Point(808, 332)
        Me.CheckEdit28.Name = "CheckEdit28"
        Me.CheckEdit28.Properties.Caption = "NO"
        Me.CheckEdit28.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit28.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit28.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit28.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit28.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit28.Size = New System.Drawing.Size(205, 36)
        Me.CheckEdit28.StyleController = Me.LayoutControl3
        Me.CheckEdit28.TabIndex = 19
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = false
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem30, Me.LayoutControlItem31, Me.LayoutControlItem32, Me.LayoutControlItem33, Me.LayoutControlItem34, Me.LayoutControlItem35, Me.LayoutControlItem36, Me.LayoutControlItem37, Me.LayoutControlItem38, Me.LayoutControlItem39, Me.LayoutControlItem40, Me.LayoutControlItem41, Me.LayoutControlItem42, Me.LayoutControlItem43, Me.LayoutControlItem44, Me.LayoutControlItem45, Me.LayoutControlItem46, Me.LayoutControlItem47})
        Me.LayoutControlGroup2.Name = "Root"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1025, 532)
        Me.LayoutControlGroup2.TextVisible = false
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.CheckEdit11
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem30.Text = "Do you own a property?"
        Me.LayoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem30.TextToControlDistance = 5
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.CheckEdit12
        Me.LayoutControlItem31.Location = New System.Drawing.Point(796, 0)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = false
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.CheckEdit13
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem32.Text = "Is it Mortgage Free?"
        Me.LayoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem32.TextToControlDistance = 5
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.CheckEdit14
        Me.LayoutControlItem33.Location = New System.Drawing.Point(796, 40)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem33.TextVisible = false
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.CheckEdit15
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 80)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem34.Text = "Are you interested in protecting your property"
        Me.LayoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem34.TextToControlDistance = 5
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.CheckEdit16
        Me.LayoutControlItem35.Location = New System.Drawing.Point(796, 80)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem35.TextVisible = false
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.CheckEdit17
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem36.Text = "Any other financial assets you want to protect?"
        Me.LayoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem36.TextToControlDistance = 5
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.CheckEdit18
        Me.LayoutControlItem37.Location = New System.Drawing.Point(796, 120)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextVisible = false
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.CheckEdit19
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 160)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem38.Text = "Have you appointed anyone to look after your financial affairs are you get older?"& _ 
    ""
        Me.LayoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(600, 16)
        Me.LayoutControlItem38.TextToControlDistance = 5
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.CheckEdit20
        Me.LayoutControlItem39.Location = New System.Drawing.Point(796, 160)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = false
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.Control = Me.CheckEdit21
        Me.LayoutControlItem40.Location = New System.Drawing.Point(0, 200)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem40.Text = "Have you appointed anyone to look after your health affairs are you get older?"
        Me.LayoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem40.TextToControlDistance = 5
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.Control = Me.CheckEdit22
        Me.LayoutControlItem41.Location = New System.Drawing.Point(796, 200)
        Me.LayoutControlItem41.Name = "LayoutControlItem41"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem41.TextVisible = false
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.Control = Me.CheckEdit23
        Me.LayoutControlItem42.Location = New System.Drawing.Point(0, 240)
        Me.LayoutControlItem42.Name = "LayoutControlItem42"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem42.Text = "Are any beneficiaries registered disabled?"
        Me.LayoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem42.TextToControlDistance = 5
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.Control = Me.CheckEdit24
        Me.LayoutControlItem43.Location = New System.Drawing.Point(0, 280)
        Me.LayoutControlItem43.Name = "LayoutControlItem43"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(796, 40)
        Me.LayoutControlItem43.Text = "Have you pre-paid for your probate?"
        Me.LayoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem43.TextToControlDistance = 5
        '
        'LayoutControlItem44
        '
        Me.LayoutControlItem44.Control = Me.CheckEdit25
        Me.LayoutControlItem44.Location = New System.Drawing.Point(0, 320)
        Me.LayoutControlItem44.Name = "LayoutControlItem44"
        Me.LayoutControlItem44.Size = New System.Drawing.Size(796, 192)
        Me.LayoutControlItem44.Text = "Have your arranged and pre-paid your funeral?"
        Me.LayoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem44.TextSize = New System.Drawing.Size(600, 20)
        Me.LayoutControlItem44.TextToControlDistance = 5
        '
        'LayoutControlItem45
        '
        Me.LayoutControlItem45.Control = Me.CheckEdit26
        Me.LayoutControlItem45.Location = New System.Drawing.Point(796, 240)
        Me.LayoutControlItem45.Name = "LayoutControlItem45"
        Me.LayoutControlItem45.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem45.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem45.TextVisible = false
        '
        'LayoutControlItem46
        '
        Me.LayoutControlItem46.Control = Me.CheckEdit27
        Me.LayoutControlItem46.Location = New System.Drawing.Point(796, 280)
        Me.LayoutControlItem46.Name = "LayoutControlItem46"
        Me.LayoutControlItem46.Size = New System.Drawing.Size(209, 40)
        Me.LayoutControlItem46.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem46.TextVisible = false
        '
        'LayoutControlItem47
        '
        Me.LayoutControlItem47.Control = Me.CheckEdit28
        Me.LayoutControlItem47.Location = New System.Drawing.Point(796, 320)
        Me.LayoutControlItem47.Name = "LayoutControlItem47"
        Me.LayoutControlItem47.Size = New System.Drawing.Size(209, 192)
        Me.LayoutControlItem47.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem47.TextVisible = false
        '
        'TabNavigationPage2
        '
        Me.TabNavigationPage2.Caption = "Asset Information"
        Me.TabNavigationPage2.Controls.Add(Me.ComboBoxEdit3)
        Me.TabNavigationPage2.Controls.Add(Me.ComboBoxEdit2)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl17)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl14)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl16)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl11)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl13)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl12)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl10)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl9)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl15)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl8)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl7)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl6)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl5)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl4)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl3)
        Me.TabNavigationPage2.Controls.Add(Me.SimpleButton6)
        Me.TabNavigationPage2.Controls.Add(Me.GridControl1)
        Me.TabNavigationPage2.Name = "TabNavigationPage2"
        Me.TabNavigationPage2.Size = New System.Drawing.Size(1034, 562)
        '
        'ComboBoxEdit3
        '
        Me.ComboBoxEdit3.Location = New System.Drawing.Point(682, 492)
        Me.ComboBoxEdit3.Name = "ComboBoxEdit3"
        Me.ComboBoxEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit3.Size = New System.Drawing.Size(121, 38)
        Me.ComboBoxEdit3.TabIndex = 10
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(472, 492)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(125, 38)
        Me.ComboBoxEdit2.TabIndex = 9
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl17.Appearance.Options.UseFont = true
        Me.LabelControl17.Location = New System.Drawing.Point(947, 461)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl17.TabIndex = 8
        Me.LabelControl17.Text = "Total Assets"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl14.Appearance.Options.UseFont = true
        Me.LabelControl14.Location = New System.Drawing.Point(682, 461)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl14.TabIndex = 8
        Me.LabelControl14.Text = "Total Assets"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl16.Appearance.Options.UseFont = true
        Me.LabelControl16.Location = New System.Drawing.Point(947, 431)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl16.TabIndex = 8
        Me.LabelControl16.Text = "Total Assets"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl11.Appearance.Options.UseFont = true
        Me.LabelControl11.Location = New System.Drawing.Point(682, 431)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl11.TabIndex = 8
        Me.LabelControl11.Text = "Total Assets"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl13.Appearance.Options.UseFont = true
        Me.LabelControl13.Location = New System.Drawing.Point(472, 461)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl13.TabIndex = 7
        Me.LabelControl13.Text = "Total Assets"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl12.Appearance.Options.UseFont = true
        Me.LabelControl12.Location = New System.Drawing.Point(472, 431)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl12.TabIndex = 7
        Me.LabelControl12.Text = "Total Assets"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl10.Appearance.Options.UseFont = true
        Me.LabelControl10.Location = New System.Drawing.Point(903, 403)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(8, 16)
        Me.LabelControl10.TabIndex = 6
        Me.LabelControl10.Text = "="
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Appearance.Options.UseFont = true
        Me.LabelControl9.Location = New System.Drawing.Point(947, 403)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl9.TabIndex = 5
        Me.LabelControl9.Text = "Total Assets"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl15.Appearance.Options.UseFont = true
        Me.LabelControl15.Location = New System.Drawing.Point(803, 403)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl15.TabIndex = 4
        Me.LabelControl15.Text = "Total Assets"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl8.Appearance.Options.UseFont = true
        Me.LabelControl8.Location = New System.Drawing.Point(682, 403)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl8.TabIndex = 4
        Me.LabelControl8.Text = "Total Assets"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl7.Appearance.Options.UseFont = true
        Me.LabelControl7.Location = New System.Drawing.Point(472, 403)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl7.TabIndex = 3
        Me.LabelControl7.Text = "Total Assets"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl6.Appearance.Options.UseFont = true
        Me.LabelControl6.Location = New System.Drawing.Point(68, 503)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(331, 16)
        Me.LabelControl6.TabIndex = 2
        Me.LabelControl6.Text = "How do you feel about your current levels of return?"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl5.Appearance.Options.UseFont = true
        Me.LabelControl5.Location = New System.Drawing.Point(33, 461)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(366, 16)
        Me.LabelControl5.TabIndex = 2
        Me.LabelControl5.Text = "How much emergency cash should you keep in the bank?"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl4.Appearance.Options.UseFont = true
        Me.LabelControl4.Location = New System.Drawing.Point(277, 431)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(122, 16)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Total Gross Income"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl3.Appearance.Options.UseFont = true
        Me.LabelControl3.Location = New System.Drawing.Point(324, 403)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Total Assets"
        '
        'SimpleButton6
        '
        Me.SimpleButton6.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton6.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton6.Location = New System.Drawing.Point(12, 7)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(192, 43)
        Me.SimpleButton6.TabIndex = 1
        Me.SimpleButton6.Text = "Add New Asset"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(12, 52)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1010, 309)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumnAssetType, Me.GridColumnDetail, Me.GridColumnClient1, Me.GridColumnClient2, Me.GridColumnJointTotal, Me.GridColumnAction})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = false
        Me.GridView1.OptionsView.ShowIndicator = false
        Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumnAssetType
        '
        Me.GridColumnAssetType.Caption = "Asset Type"
        Me.GridColumnAssetType.Name = "GridColumnAssetType"
        Me.GridColumnAssetType.Visible = true
        Me.GridColumnAssetType.VisibleIndex = 0
        '
        'GridColumnDetail
        '
        Me.GridColumnDetail.Caption = "Detail"
        Me.GridColumnDetail.Name = "GridColumnDetail"
        Me.GridColumnDetail.Visible = true
        Me.GridColumnDetail.VisibleIndex = 1
        '
        'GridColumnClient1
        '
        Me.GridColumnClient1.Caption = "Client"
        Me.GridColumnClient1.Name = "GridColumnClient1"
        Me.GridColumnClient1.Visible = true
        Me.GridColumnClient1.VisibleIndex = 2
        '
        'GridColumnClient2
        '
        Me.GridColumnClient2.Caption = "Client 2"
        Me.GridColumnClient2.Name = "GridColumnClient2"
        Me.GridColumnClient2.Visible = true
        Me.GridColumnClient2.VisibleIndex = 3
        '
        'GridColumnJointTotal
        '
        Me.GridColumnJointTotal.Caption = "Join Total"
        Me.GridColumnJointTotal.Name = "GridColumnJointTotal"
        Me.GridColumnJointTotal.Visible = true
        Me.GridColumnJointTotal.VisibleIndex = 4
        '
        'GridColumnAction
        '
        Me.GridColumnAction.Caption = "Action"
        Me.GridColumnAction.Name = "GridColumnAction"
        Me.GridColumnAction.Visible = true
        Me.GridColumnAction.VisibleIndex = 5
        '
        'TabNavigationPage3
        '
        Me.TabNavigationPage3.Caption = "Storage Options"
        Me.TabNavigationPage3.Controls.Add(Me.LayoutControl4)
        Me.TabNavigationPage3.Name = "TabNavigationPage3"
        Me.TabNavigationPage3.Size = New System.Drawing.Size(1034, 562)
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.CheckEdit29)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit30)
        Me.LayoutControl4.Controls.Add(Me.TextEdit1)
        Me.LayoutControl4.Controls.Add(Me.TextEdit15)
        Me.LayoutControl4.Controls.Add(Me.TextEdit16)
        Me.LayoutControl4.Controls.Add(Me.TextEdit17)
        Me.LayoutControl4.Controls.Add(Me.TextEdit18)
        Me.LayoutControl4.Location = New System.Drawing.Point(116, 18)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup3
        Me.LayoutControl4.Size = New System.Drawing.Size(789, 521)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'CheckEdit29
        '
        Me.CheckEdit29.Location = New System.Drawing.Point(317, 12)
        Me.CheckEdit29.Name = "CheckEdit29"
        Me.CheckEdit29.Properties.Caption = "YES"
        Me.CheckEdit29.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit29.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit29.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit29.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit29.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit29.Size = New System.Drawing.Size(135, 36)
        Me.CheckEdit29.StyleController = Me.LayoutControl4
        Me.CheckEdit29.TabIndex = 4
        '
        'CheckEdit30
        '
        Me.CheckEdit30.Location = New System.Drawing.Point(456, 12)
        Me.CheckEdit30.Name = "CheckEdit30"
        Me.CheckEdit30.Properties.Caption = "NO"
        Me.CheckEdit30.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit30.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit30.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit30.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit30.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit30.Size = New System.Drawing.Size(321, 36)
        Me.CheckEdit30.StyleController = Me.LayoutControl4
        Me.CheckEdit30.TabIndex = 5
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(317, 57)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit1.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit1.Size = New System.Drawing.Size(460, 52)
        Me.TextEdit1.StyleController = Me.LayoutControl4
        Me.TextEdit1.TabIndex = 6
        '
        'TextEdit15
        '
        Me.TextEdit15.Location = New System.Drawing.Point(317, 118)
        Me.TextEdit15.Name = "TextEdit15"
        Me.TextEdit15.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit15.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit15.Size = New System.Drawing.Size(460, 52)
        Me.TextEdit15.StyleController = Me.LayoutControl4
        Me.TextEdit15.TabIndex = 7
        '
        'TextEdit16
        '
        Me.TextEdit16.Location = New System.Drawing.Point(317, 179)
        Me.TextEdit16.Name = "TextEdit16"
        Me.TextEdit16.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit16.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit16.Size = New System.Drawing.Size(460, 52)
        Me.TextEdit16.StyleController = Me.LayoutControl4
        Me.TextEdit16.TabIndex = 8
        '
        'TextEdit17
        '
        Me.TextEdit17.Location = New System.Drawing.Point(317, 240)
        Me.TextEdit17.Name = "TextEdit17"
        Me.TextEdit17.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit17.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit17.Size = New System.Drawing.Size(460, 52)
        Me.TextEdit17.StyleController = Me.LayoutControl4
        Me.TextEdit17.TabIndex = 9
        '
        'TextEdit18
        '
        Me.TextEdit18.Location = New System.Drawing.Point(317, 301)
        Me.TextEdit18.Name = "TextEdit18"
        Me.TextEdit18.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit18.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEdit18.Size = New System.Drawing.Size(460, 52)
        Me.TextEdit18.StyleController = Me.LayoutControl4
        Me.TextEdit18.TabIndex = 10
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = false
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem48, Me.EmptySpaceItem1, Me.LayoutControlItem49, Me.LayoutControlItem50, Me.LayoutControlItem51, Me.LayoutControlItem52, Me.LayoutControlItem53, Me.LayoutControlItem54})
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(789, 521)
        Me.LayoutControlGroup3.TextVisible = false
        '
        'LayoutControlItem48
        '
        Me.LayoutControlItem48.Control = Me.CheckEdit29
        Me.LayoutControlItem48.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem48.Name = "LayoutControlItem48"
        Me.LayoutControlItem48.Size = New System.Drawing.Size(444, 40)
        Me.LayoutControlItem48.Text = "Existing Storage?"
        Me.LayoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem48.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem48.TextToControlDistance = 5
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = false
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 345)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(769, 156)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem49
        '
        Me.LayoutControlItem49.Control = Me.CheckEdit30
        Me.LayoutControlItem49.Location = New System.Drawing.Point(444, 0)
        Me.LayoutControlItem49.Name = "LayoutControlItem49"
        Me.LayoutControlItem49.Size = New System.Drawing.Size(325, 40)
        Me.LayoutControlItem49.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem49.TextVisible = false
        '
        'LayoutControlItem50
        '
        Me.LayoutControlItem50.Control = Me.TextEdit1
        Me.LayoutControlItem50.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControlItem50.Name = "LayoutControlItem50"
        Me.LayoutControlItem50.Size = New System.Drawing.Size(769, 61)
        Me.LayoutControlItem50.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 5, 0)
        Me.LayoutControlItem50.Text = "Storage Product"
        Me.LayoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem50.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem50.TextToControlDistance = 5
        '
        'LayoutControlItem51
        '
        Me.LayoutControlItem51.Control = Me.TextEdit15
        Me.LayoutControlItem51.Location = New System.Drawing.Point(0, 101)
        Me.LayoutControlItem51.Name = "LayoutControlItem51"
        Me.LayoutControlItem51.Size = New System.Drawing.Size(769, 61)
        Me.LayoutControlItem51.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 5, 0)
        Me.LayoutControlItem51.Text = "Annual Amount"
        Me.LayoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem51.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem51.TextToControlDistance = 5
        '
        'LayoutControlItem52
        '
        Me.LayoutControlItem52.Control = Me.TextEdit16
        Me.LayoutControlItem52.Location = New System.Drawing.Point(0, 162)
        Me.LayoutControlItem52.Name = "LayoutControlItem52"
        Me.LayoutControlItem52.Size = New System.Drawing.Size(769, 61)
        Me.LayoutControlItem52.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 5, 0)
        Me.LayoutControlItem52.Text = "Sort Code"
        Me.LayoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem52.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem52.TextToControlDistance = 5
        '
        'LayoutControlItem53
        '
        Me.LayoutControlItem53.Control = Me.TextEdit17
        Me.LayoutControlItem53.Location = New System.Drawing.Point(0, 223)
        Me.LayoutControlItem53.Name = "LayoutControlItem53"
        Me.LayoutControlItem53.Size = New System.Drawing.Size(769, 61)
        Me.LayoutControlItem53.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 5, 0)
        Me.LayoutControlItem53.Text = "Account Number"
        Me.LayoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem53.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem53.TextToControlDistance = 5
        '
        'LayoutControlItem54
        '
        Me.LayoutControlItem54.Control = Me.TextEdit18
        Me.LayoutControlItem54.Location = New System.Drawing.Point(0, 284)
        Me.LayoutControlItem54.Name = "LayoutControlItem54"
        Me.LayoutControlItem54.Size = New System.Drawing.Size(769, 61)
        Me.LayoutControlItem54.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 5, 0)
        Me.LayoutControlItem54.Text = "Payment Method"
        Me.LayoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem54.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem54.TextToControlDistance = 5
        '
        'TabNavigationPage4
        '
        Me.TabNavigationPage4.Caption = "Client Recommendations"
        Me.TabNavigationPage4.Controls.Add(Me.CheckEdit41)
        Me.TabNavigationPage4.Controls.Add(Me.CheckEdit42)
        Me.TabNavigationPage4.Controls.Add(Me.CheckEdit40)
        Me.TabNavigationPage4.Controls.Add(Me.CheckEdit39)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl26)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl32)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl30)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl28)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl35)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl34)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl33)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl31)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl29)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl27)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl25)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl24)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl23)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl22)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit39)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit42)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit47)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit46)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit45)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit44)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit43)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit41)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit38)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit37)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit36)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit35)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl21)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl20)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl19)
        Me.TabNavigationPage4.Controls.Add(Me.LayoutControl5)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl18)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit34)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit33)
        Me.TabNavigationPage4.Controls.Add(Me.TextEdit40)
        Me.TabNavigationPage4.Name = "TabNavigationPage4"
        Me.TabNavigationPage4.Size = New System.Drawing.Size(1034, 562)
        '
        'CheckEdit41
        '
        Me.CheckEdit41.EditValue = "sdfgsdfg"
        Me.CheckEdit41.Location = New System.Drawing.Point(915, 195)
        Me.CheckEdit41.Name = "CheckEdit41"
        Me.CheckEdit41.Properties.AutoHeight = false
        Me.CheckEdit41.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.[Default]
        Me.CheckEdit41.Properties.Caption = "OPG Fee"
        Me.CheckEdit41.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit41.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit41.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit41.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit41.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit41.Size = New System.Drawing.Size(106, 39)
        Me.CheckEdit41.TabIndex = 9
        '
        'CheckEdit42
        '
        Me.CheckEdit42.EditValue = "sdfgsdfg"
        Me.CheckEdit42.Location = New System.Drawing.Point(395, 6)
        Me.CheckEdit42.Name = "CheckEdit42"
        Me.CheckEdit42.Properties.AutoHeight = false
        Me.CheckEdit42.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.[Default]
        Me.CheckEdit42.Properties.Caption = "Manual Override Required"
        Me.CheckEdit42.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit42.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit42.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit42.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit42.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit42.Size = New System.Drawing.Size(223, 39)
        Me.CheckEdit42.TabIndex = 9
        '
        'CheckEdit40
        '
        Me.CheckEdit40.EditValue = "sdfgsdfg"
        Me.CheckEdit40.Location = New System.Drawing.Point(915, 160)
        Me.CheckEdit40.Name = "CheckEdit40"
        Me.CheckEdit40.Properties.AutoHeight = false
        Me.CheckEdit40.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.[Default]
        Me.CheckEdit40.Properties.Caption = "OPG Fee"
        Me.CheckEdit40.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit40.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit40.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit40.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit40.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit40.Size = New System.Drawing.Size(106, 39)
        Me.CheckEdit40.TabIndex = 9
        '
        'CheckEdit39
        '
        Me.CheckEdit39.Location = New System.Drawing.Point(818, 450)
        Me.CheckEdit39.Name = "CheckEdit39"
        Me.CheckEdit39.Properties.AutoHeight = false
        Me.CheckEdit39.Properties.Caption = ""
        Me.CheckEdit39.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit39.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit39.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit39.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit39.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit39.Size = New System.Drawing.Size(75, 40)
        Me.CheckEdit39.TabIndex = 8
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(580, 276)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(75, 16)
        Me.LabelControl26.TabIndex = 7
        Me.LabelControl26.Text = "Probate Plan"
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(827, 424)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(47, 16)
        Me.LabelControl32.TabIndex = 7
        Me.LabelControl32.Text = "inc VAT"
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(827, 389)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(47, 16)
        Me.LabelControl30.TabIndex = 7
        Me.LabelControl30.Text = "inc VAT"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(904, 354)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl28.TabIndex = 7
        Me.LabelControl28.Text = "Price"
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl35.Appearance.Options.UseFont = true
        Me.LabelControl35.Location = New System.Drawing.Point(827, 521)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(104, 16)
        Me.LabelControl35.TabIndex = 7
        Me.LabelControl35.Text = "Confirm Amount"
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl34.Appearance.Options.UseFont = true
        Me.LabelControl34.Location = New System.Drawing.Point(508, 521)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(160, 16)
        Me.LabelControl34.TabIndex = 7
        Me.LabelControl34.Text = "How is the client paying?"
        '
        'LabelControl33
        '
        Me.LabelControl33.Location = New System.Drawing.Point(547, 459)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(108, 16)
        Me.LabelControl33.TabIndex = 7
        Me.LabelControl33.Text = "Document Storage"
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(570, 424)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(85, 16)
        Me.LabelControl31.TabIndex = 7
        Me.LabelControl31.Text = "Package Price"
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(588, 389)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(67, 16)
        Me.LabelControl29.TabIndex = 7
        Me.LabelControl29.Text = "Grand Total"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(590, 354)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(65, 16)
        Me.LabelControl27.TabIndex = 7
        Me.LabelControl27.Text = "Description"
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(482, 241)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(173, 16)
        Me.LabelControl25.TabIndex = 7
        Me.LabelControl25.Text = "Family Asset Protection Trust"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(599, 206)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl24.TabIndex = 7
        Me.LabelControl24.Text = "P&&F LPA"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(594, 171)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(61, 16)
        Me.LabelControl23.TabIndex = 7
        Me.LabelControl23.Text = "H&&W LPA"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(596, 136)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(59, 16)
        Me.LabelControl22.TabIndex = 7
        Me.LabelControl22.Text = "Basic Will"
        '
        'TextEdit39
        '
        Me.TextEdit39.EditValue = ""
        Me.TextEdit39.Location = New System.Drawing.Point(671, 270)
        Me.TextEdit39.Name = "TextEdit39"
        Me.TextEdit39.Properties.AutoHeight = false
        Me.TextEdit39.Size = New System.Drawing.Size(238, 29)
        Me.TextEdit39.TabIndex = 6
        '
        'TextEdit42
        '
        Me.TextEdit42.EditValue = ""
        Me.TextEdit42.Location = New System.Drawing.Point(940, 348)
        Me.TextEdit42.Name = "TextEdit42"
        Me.TextEdit42.Properties.AutoHeight = false
        Me.TextEdit42.Size = New System.Drawing.Size(88, 29)
        Me.TextEdit42.TabIndex = 6
        '
        'TextEdit47
        '
        Me.TextEdit47.EditValue = ""
        Me.TextEdit47.Location = New System.Drawing.Point(937, 515)
        Me.TextEdit47.Name = "TextEdit47"
        Me.TextEdit47.Properties.AutoHeight = false
        Me.TextEdit47.Size = New System.Drawing.Size(88, 29)
        Me.TextEdit47.TabIndex = 6
        '
        'TextEdit46
        '
        Me.TextEdit46.EditValue = ""
        Me.TextEdit46.Location = New System.Drawing.Point(671, 515)
        Me.TextEdit46.Name = "TextEdit46"
        Me.TextEdit46.Properties.AutoHeight = false
        Me.TextEdit46.Size = New System.Drawing.Size(132, 29)
        Me.TextEdit46.TabIndex = 6
        '
        'TextEdit45
        '
        Me.TextEdit45.EditValue = ""
        Me.TextEdit45.Location = New System.Drawing.Point(671, 453)
        Me.TextEdit45.Name = "TextEdit45"
        Me.TextEdit45.Properties.AutoHeight = false
        Me.TextEdit45.Size = New System.Drawing.Size(132, 29)
        Me.TextEdit45.TabIndex = 6
        '
        'TextEdit44
        '
        Me.TextEdit44.EditValue = ""
        Me.TextEdit44.Location = New System.Drawing.Point(671, 418)
        Me.TextEdit44.Name = "TextEdit44"
        Me.TextEdit44.Properties.AutoHeight = false
        Me.TextEdit44.Size = New System.Drawing.Size(132, 29)
        Me.TextEdit44.TabIndex = 6
        '
        'TextEdit43
        '
        Me.TextEdit43.EditValue = ""
        Me.TextEdit43.Location = New System.Drawing.Point(671, 383)
        Me.TextEdit43.Name = "TextEdit43"
        Me.TextEdit43.Properties.AutoHeight = false
        Me.TextEdit43.Size = New System.Drawing.Size(132, 29)
        Me.TextEdit43.TabIndex = 6
        '
        'TextEdit41
        '
        Me.TextEdit41.EditValue = ""
        Me.TextEdit41.Location = New System.Drawing.Point(671, 348)
        Me.TextEdit41.Name = "TextEdit41"
        Me.TextEdit41.Properties.AutoHeight = false
        Me.TextEdit41.Size = New System.Drawing.Size(132, 29)
        Me.TextEdit41.TabIndex = 6
        '
        'TextEdit38
        '
        Me.TextEdit38.EditValue = ""
        Me.TextEdit38.Location = New System.Drawing.Point(671, 235)
        Me.TextEdit38.Name = "TextEdit38"
        Me.TextEdit38.Properties.AutoHeight = false
        Me.TextEdit38.Size = New System.Drawing.Size(238, 29)
        Me.TextEdit38.TabIndex = 6
        '
        'TextEdit37
        '
        Me.TextEdit37.EditValue = ""
        Me.TextEdit37.Location = New System.Drawing.Point(671, 200)
        Me.TextEdit37.Name = "TextEdit37"
        Me.TextEdit37.Properties.AutoHeight = false
        Me.TextEdit37.Size = New System.Drawing.Size(238, 29)
        Me.TextEdit37.TabIndex = 6
        '
        'TextEdit36
        '
        Me.TextEdit36.EditValue = ""
        Me.TextEdit36.Location = New System.Drawing.Point(671, 165)
        Me.TextEdit36.Name = "TextEdit36"
        Me.TextEdit36.Properties.AutoHeight = false
        Me.TextEdit36.Size = New System.Drawing.Size(238, 29)
        Me.TextEdit36.TabIndex = 6
        '
        'TextEdit35
        '
        Me.TextEdit35.EditValue = ""
        Me.TextEdit35.Location = New System.Drawing.Point(671, 130)
        Me.TextEdit35.Name = "TextEdit35"
        Me.TextEdit35.Properties.AutoHeight = false
        Me.TextEdit35.Size = New System.Drawing.Size(238, 29)
        Me.TextEdit35.TabIndex = 6
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(573, 101)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(82, 16)
        Me.LabelControl21.TabIndex = 5
        Me.LabelControl21.Text = "Will with Trust"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(589, 66)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(66, 16)
        Me.LabelControl20.TabIndex = 5
        Me.LabelControl20.Text = "Will Review"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl19.Appearance.Options.UseFont = true
        Me.LabelControl19.Location = New System.Drawing.Point(671, 17)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(168, 16)
        Me.LabelControl19.TabIndex = 2
        Me.LabelControl19.Text = "Manually Choose Products"
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.TextEdit19)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit31)
        Me.LayoutControl5.Controls.Add(Me.TextEdit20)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit32)
        Me.LayoutControl5.Controls.Add(Me.TextEdit21)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit33)
        Me.LayoutControl5.Controls.Add(Me.TextEdit25)
        Me.LayoutControl5.Controls.Add(Me.TextEdit26)
        Me.LayoutControl5.Controls.Add(Me.TextEdit27)
        Me.LayoutControl5.Controls.Add(Me.TextEdit28)
        Me.LayoutControl5.Controls.Add(Me.TextEdit29)
        Me.LayoutControl5.Controls.Add(Me.TextEdit30)
        Me.LayoutControl5.Controls.Add(Me.ButtonEdit2)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit34)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit35)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit36)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit37)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit38)
        Me.LayoutControl5.Controls.Add(Me.TextEdit31)
        Me.LayoutControl5.Controls.Add(Me.TextEdit32)
        Me.LayoutControl5.Location = New System.Drawing.Point(4, 44)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup4
        Me.LayoutControl5.Size = New System.Drawing.Size(470, 488)
        Me.LayoutControl5.TabIndex = 1
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'TextEdit19
        '
        Me.TextEdit19.Location = New System.Drawing.Point(188, 12)
        Me.TextEdit19.Name = "TextEdit19"
        Me.TextEdit19.Properties.AutoHeight = false
        Me.TextEdit19.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit19.StyleController = Me.LayoutControl5
        Me.TextEdit19.TabIndex = 4
        '
        'CheckEdit31
        '
        Me.CheckEdit31.Location = New System.Drawing.Point(391, 12)
        Me.CheckEdit31.Name = "CheckEdit31"
        Me.CheckEdit31.Properties.Caption = ""
        Me.CheckEdit31.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit31.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit31.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit31.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit31.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit31.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit31.StyleController = Me.LayoutControl5
        Me.CheckEdit31.TabIndex = 5
        '
        'TextEdit20
        '
        Me.TextEdit20.Location = New System.Drawing.Point(188, 54)
        Me.TextEdit20.Name = "TextEdit20"
        Me.TextEdit20.Properties.AutoHeight = false
        Me.TextEdit20.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit20.StyleController = Me.LayoutControl5
        Me.TextEdit20.TabIndex = 6
        '
        'CheckEdit32
        '
        Me.CheckEdit32.Location = New System.Drawing.Point(391, 54)
        Me.CheckEdit32.Name = "CheckEdit32"
        Me.CheckEdit32.Properties.Caption = ""
        Me.CheckEdit32.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit32.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit32.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit32.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit32.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit32.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit32.StyleController = Me.LayoutControl5
        Me.CheckEdit32.TabIndex = 7
        '
        'TextEdit21
        '
        Me.TextEdit21.Location = New System.Drawing.Point(188, 96)
        Me.TextEdit21.Name = "TextEdit21"
        Me.TextEdit21.Properties.AutoHeight = false
        Me.TextEdit21.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit21.StyleController = Me.LayoutControl5
        Me.TextEdit21.TabIndex = 8
        '
        'CheckEdit33
        '
        Me.CheckEdit33.Location = New System.Drawing.Point(391, 96)
        Me.CheckEdit33.Name = "CheckEdit33"
        Me.CheckEdit33.Properties.Caption = ""
        Me.CheckEdit33.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit33.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit33.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit33.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit33.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit33.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit33.StyleController = Me.LayoutControl5
        Me.CheckEdit33.TabIndex = 9
        '
        'TextEdit25
        '
        Me.TextEdit25.Location = New System.Drawing.Point(188, 138)
        Me.TextEdit25.Name = "TextEdit25"
        Me.TextEdit25.Properties.AutoHeight = false
        Me.TextEdit25.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit25.StyleController = Me.LayoutControl5
        Me.TextEdit25.TabIndex = 10
        '
        'TextEdit26
        '
        Me.TextEdit26.Location = New System.Drawing.Point(188, 180)
        Me.TextEdit26.Name = "TextEdit26"
        Me.TextEdit26.Properties.AutoHeight = false
        Me.TextEdit26.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit26.StyleController = Me.LayoutControl5
        Me.TextEdit26.TabIndex = 11
        '
        'TextEdit27
        '
        Me.TextEdit27.Location = New System.Drawing.Point(188, 222)
        Me.TextEdit27.Name = "TextEdit27"
        Me.TextEdit27.Properties.AutoHeight = false
        Me.TextEdit27.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit27.StyleController = Me.LayoutControl5
        Me.TextEdit27.TabIndex = 12
        '
        'TextEdit28
        '
        Me.TextEdit28.Location = New System.Drawing.Point(188, 264)
        Me.TextEdit28.Name = "TextEdit28"
        Me.TextEdit28.Properties.AutoHeight = false
        Me.TextEdit28.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit28.StyleController = Me.LayoutControl5
        Me.TextEdit28.TabIndex = 13
        '
        'TextEdit29
        '
        Me.TextEdit29.Location = New System.Drawing.Point(188, 348)
        Me.TextEdit29.Name = "TextEdit29"
        Me.TextEdit29.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit29.StyleController = Me.LayoutControl5
        Me.TextEdit29.TabIndex = 15
        '
        'TextEdit30
        '
        Me.TextEdit30.Location = New System.Drawing.Point(188, 390)
        Me.TextEdit30.Name = "TextEdit30"
        Me.TextEdit30.Size = New System.Drawing.Size(199, 38)
        Me.TextEdit30.StyleController = Me.LayoutControl5
        Me.TextEdit30.TabIndex = 16
        '
        'ButtonEdit2
        '
        Me.ButtonEdit2.Location = New System.Drawing.Point(188, 306)
        Me.ButtonEdit2.Name = "ButtonEdit2"
        Me.ButtonEdit2.Size = New System.Drawing.Size(199, 38)
        Me.ButtonEdit2.StyleController = Me.LayoutControl5
        Me.ButtonEdit2.TabIndex = 14
        '
        'CheckEdit34
        '
        Me.CheckEdit34.Location = New System.Drawing.Point(391, 138)
        Me.CheckEdit34.Name = "CheckEdit34"
        Me.CheckEdit34.Properties.Caption = ""
        Me.CheckEdit34.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit34.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit34.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit34.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit34.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit34.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit34.StyleController = Me.LayoutControl5
        Me.CheckEdit34.TabIndex = 17
        '
        'CheckEdit35
        '
        Me.CheckEdit35.Location = New System.Drawing.Point(391, 180)
        Me.CheckEdit35.Name = "CheckEdit35"
        Me.CheckEdit35.Properties.Caption = ""
        Me.CheckEdit35.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit35.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit35.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit35.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit35.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit35.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit35.StyleController = Me.LayoutControl5
        Me.CheckEdit35.TabIndex = 18
        '
        'CheckEdit36
        '
        Me.CheckEdit36.Location = New System.Drawing.Point(391, 222)
        Me.CheckEdit36.Name = "CheckEdit36"
        Me.CheckEdit36.Properties.Caption = ""
        Me.CheckEdit36.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit36.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit36.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit36.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit36.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit36.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit36.StyleController = Me.LayoutControl5
        Me.CheckEdit36.TabIndex = 19
        '
        'CheckEdit37
        '
        Me.CheckEdit37.Location = New System.Drawing.Point(391, 264)
        Me.CheckEdit37.Name = "CheckEdit37"
        Me.CheckEdit37.Properties.Caption = ""
        Me.CheckEdit37.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit37.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit37.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit37.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit37.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit37.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit37.StyleController = Me.LayoutControl5
        Me.CheckEdit37.TabIndex = 20
        '
        'CheckEdit38
        '
        Me.CheckEdit38.Location = New System.Drawing.Point(391, 390)
        Me.CheckEdit38.Name = "CheckEdit38"
        Me.CheckEdit38.Properties.Caption = ""
        Me.CheckEdit38.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit38.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit38.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit38.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit38.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit38.Size = New System.Drawing.Size(67, 36)
        Me.CheckEdit38.StyleController = Me.LayoutControl5
        Me.CheckEdit38.TabIndex = 21
        '
        'TextEdit31
        '
        Me.TextEdit31.EditValue = "inc VAT"
        Me.TextEdit31.Enabled = false
        Me.TextEdit31.Location = New System.Drawing.Point(391, 306)
        Me.TextEdit31.Name = "TextEdit31"
        Me.TextEdit31.Properties.Appearance.BackColor = System.Drawing.Color.DimGray
        Me.TextEdit31.Properties.Appearance.ForeColor = System.Drawing.Color.White
        Me.TextEdit31.Properties.Appearance.Options.UseBackColor = true
        Me.TextEdit31.Properties.Appearance.Options.UseForeColor = true
        Me.TextEdit31.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TextEdit31.Size = New System.Drawing.Size(67, 36)
        Me.TextEdit31.StyleController = Me.LayoutControl5
        Me.TextEdit31.TabIndex = 22
        '
        'TextEdit32
        '
        Me.TextEdit32.EditValue = "inc VAT"
        Me.TextEdit32.Location = New System.Drawing.Point(391, 348)
        Me.TextEdit32.Name = "TextEdit32"
        Me.TextEdit32.Properties.Appearance.BackColor = System.Drawing.Color.DimGray
        Me.TextEdit32.Properties.Appearance.ForeColor = System.Drawing.Color.White
        Me.TextEdit32.Properties.Appearance.Options.UseBackColor = true
        Me.TextEdit32.Properties.Appearance.Options.UseForeColor = true
        Me.TextEdit32.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TextEdit32.Size = New System.Drawing.Size(67, 36)
        Me.TextEdit32.StyleController = Me.LayoutControl5
        Me.TextEdit32.TabIndex = 23
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = false
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem55, Me.LayoutControlItem56, Me.LayoutControlItem57, Me.LayoutControlItem58, Me.LayoutControlItem59, Me.LayoutControlItem60, Me.LayoutControlItem67, Me.LayoutControlItem68, Me.LayoutControlItem69, Me.LayoutControlItem70, Me.LayoutControlItem71, Me.LayoutControlItem72, Me.LayoutControlItem73, Me.LayoutControlItem74, Me.LayoutControlItem75, Me.LayoutControlItem76, Me.LayoutControlItem77, Me.LayoutControlItem79, Me.LayoutControlItem78, Me.LayoutControlItem80})
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(470, 488)
        Me.LayoutControlGroup4.TextVisible = false
        '
        'LayoutControlItem55
        '
        Me.LayoutControlItem55.Control = Me.TextEdit19
        Me.LayoutControlItem55.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem55.Name = "LayoutControlItem55"
        Me.LayoutControlItem55.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem55.Text = "Will Review"
        Me.LayoutControlItem55.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem56
        '
        Me.LayoutControlItem56.Control = Me.CheckEdit31
        Me.LayoutControlItem56.Location = New System.Drawing.Point(379, 0)
        Me.LayoutControlItem56.Name = "LayoutControlItem56"
        Me.LayoutControlItem56.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem56.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem56.TextVisible = false
        '
        'LayoutControlItem57
        '
        Me.LayoutControlItem57.Control = Me.TextEdit20
        Me.LayoutControlItem57.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem57.Name = "LayoutControlItem57"
        Me.LayoutControlItem57.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem57.Text = "Will with Trust"
        Me.LayoutControlItem57.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem58
        '
        Me.LayoutControlItem58.Control = Me.CheckEdit32
        Me.LayoutControlItem58.Location = New System.Drawing.Point(379, 42)
        Me.LayoutControlItem58.Name = "LayoutControlItem58"
        Me.LayoutControlItem58.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem58.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem58.TextVisible = false
        '
        'LayoutControlItem59
        '
        Me.LayoutControlItem59.Control = Me.TextEdit21
        Me.LayoutControlItem59.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem59.Name = "LayoutControlItem59"
        Me.LayoutControlItem59.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem59.Text = "Basic Will"
        Me.LayoutControlItem59.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem60
        '
        Me.LayoutControlItem60.Control = Me.CheckEdit33
        Me.LayoutControlItem60.Location = New System.Drawing.Point(379, 84)
        Me.LayoutControlItem60.Name = "LayoutControlItem60"
        Me.LayoutControlItem60.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem60.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem60.TextVisible = false
        '
        'LayoutControlItem67
        '
        Me.LayoutControlItem67.Control = Me.TextEdit25
        Me.LayoutControlItem67.Location = New System.Drawing.Point(0, 126)
        Me.LayoutControlItem67.Name = "LayoutControlItem67"
        Me.LayoutControlItem67.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem67.Text = "H&&W LPA inc OPG Fee"
        Me.LayoutControlItem67.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem68
        '
        Me.LayoutControlItem68.Control = Me.TextEdit26
        Me.LayoutControlItem68.Location = New System.Drawing.Point(0, 168)
        Me.LayoutControlItem68.Name = "LayoutControlItem68"
        Me.LayoutControlItem68.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem68.Text = "P&&F LPA inc OPG Fee"
        Me.LayoutControlItem68.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem69
        '
        Me.LayoutControlItem69.Control = Me.TextEdit27
        Me.LayoutControlItem69.Location = New System.Drawing.Point(0, 210)
        Me.LayoutControlItem69.Name = "LayoutControlItem69"
        Me.LayoutControlItem69.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem69.Text = "Family Asset Protection Trust"
        Me.LayoutControlItem69.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem70
        '
        Me.LayoutControlItem70.Control = Me.TextEdit28
        Me.LayoutControlItem70.Location = New System.Drawing.Point(0, 252)
        Me.LayoutControlItem70.Name = "LayoutControlItem70"
        Me.LayoutControlItem70.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem70.Text = "Probate Plan"
        Me.LayoutControlItem70.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem71
        '
        Me.LayoutControlItem71.Control = Me.ButtonEdit2
        Me.LayoutControlItem71.Location = New System.Drawing.Point(0, 294)
        Me.LayoutControlItem71.MinSize = New System.Drawing.Size(231, 42)
        Me.LayoutControlItem71.Name = "LayoutControlItem71"
        Me.LayoutControlItem71.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem71.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem71.Text = "Grand Total"
        Me.LayoutControlItem71.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem72
        '
        Me.LayoutControlItem72.Control = Me.TextEdit29
        Me.LayoutControlItem72.Location = New System.Drawing.Point(0, 336)
        Me.LayoutControlItem72.Name = "LayoutControlItem72"
        Me.LayoutControlItem72.Size = New System.Drawing.Size(379, 42)
        Me.LayoutControlItem72.Text = "Package Price"
        Me.LayoutControlItem72.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem73
        '
        Me.LayoutControlItem73.Control = Me.TextEdit30
        Me.LayoutControlItem73.Location = New System.Drawing.Point(0, 378)
        Me.LayoutControlItem73.Name = "LayoutControlItem73"
        Me.LayoutControlItem73.Size = New System.Drawing.Size(379, 90)
        Me.LayoutControlItem73.Text = "Document Storage"
        Me.LayoutControlItem73.TextSize = New System.Drawing.Size(173, 16)
        '
        'LayoutControlItem74
        '
        Me.LayoutControlItem74.Control = Me.CheckEdit34
        Me.LayoutControlItem74.Location = New System.Drawing.Point(379, 126)
        Me.LayoutControlItem74.Name = "LayoutControlItem74"
        Me.LayoutControlItem74.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem74.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem74.TextVisible = false
        '
        'LayoutControlItem75
        '
        Me.LayoutControlItem75.Control = Me.CheckEdit35
        Me.LayoutControlItem75.Location = New System.Drawing.Point(379, 168)
        Me.LayoutControlItem75.Name = "LayoutControlItem75"
        Me.LayoutControlItem75.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem75.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem75.TextVisible = false
        '
        'LayoutControlItem76
        '
        Me.LayoutControlItem76.Control = Me.CheckEdit36
        Me.LayoutControlItem76.Location = New System.Drawing.Point(379, 210)
        Me.LayoutControlItem76.Name = "LayoutControlItem76"
        Me.LayoutControlItem76.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem76.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem76.TextVisible = false
        '
        'LayoutControlItem77
        '
        Me.LayoutControlItem77.Control = Me.CheckEdit37
        Me.LayoutControlItem77.Location = New System.Drawing.Point(379, 252)
        Me.LayoutControlItem77.Name = "LayoutControlItem77"
        Me.LayoutControlItem77.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem77.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem77.TextVisible = false
        '
        'LayoutControlItem79
        '
        Me.LayoutControlItem79.Control = Me.TextEdit31
        Me.LayoutControlItem79.Location = New System.Drawing.Point(379, 294)
        Me.LayoutControlItem79.Name = "LayoutControlItem79"
        Me.LayoutControlItem79.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem79.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem79.TextVisible = false
        '
        'LayoutControlItem78
        '
        Me.LayoutControlItem78.Control = Me.CheckEdit38
        Me.LayoutControlItem78.Location = New System.Drawing.Point(379, 378)
        Me.LayoutControlItem78.Name = "LayoutControlItem78"
        Me.LayoutControlItem78.Size = New System.Drawing.Size(71, 90)
        Me.LayoutControlItem78.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem78.TextVisible = false
        '
        'LayoutControlItem80
        '
        Me.LayoutControlItem80.Control = Me.TextEdit32
        Me.LayoutControlItem80.Location = New System.Drawing.Point(379, 336)
        Me.LayoutControlItem80.Name = "LayoutControlItem80"
        Me.LayoutControlItem80.Size = New System.Drawing.Size(71, 42)
        Me.LayoutControlItem80.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem80.TextVisible = false
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl18.Appearance.Options.UseFont = true
        Me.LabelControl18.Location = New System.Drawing.Point(15, 17)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(155, 16)
        Me.LabelControl18.TabIndex = 0
        Me.LabelControl18.Text = "Recommended Products"
        '
        'TextEdit34
        '
        Me.TextEdit34.EditValue = ""
        Me.TextEdit34.Location = New System.Drawing.Point(671, 95)
        Me.TextEdit34.Name = "TextEdit34"
        Me.TextEdit34.Properties.AutoHeight = false
        Me.TextEdit34.Size = New System.Drawing.Size(238, 29)
        Me.TextEdit34.TabIndex = 4
        '
        'TextEdit33
        '
        Me.TextEdit33.EditValue = ""
        Me.TextEdit33.Location = New System.Drawing.Point(671, 60)
        Me.TextEdit33.Name = "TextEdit33"
        Me.TextEdit33.Properties.AutoHeight = false
        Me.TextEdit33.Size = New System.Drawing.Size(238, 29)
        Me.TextEdit33.TabIndex = 4
        '
        'TextEdit40
        '
        Me.TextEdit40.EditValue = "sdfgsdfg"
        Me.TextEdit40.Location = New System.Drawing.Point(667, 296)
        Me.TextEdit40.Name = "TextEdit40"
        Me.TextEdit40.Properties.AutoHeight = false
        Me.TextEdit40.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.[Default]
        Me.TextEdit40.Properties.Caption = "Other"
        Me.TextEdit40.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.TextEdit40.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("TextEdit40.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.TextEdit40.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("TextEdit40.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.TextEdit40.Size = New System.Drawing.Size(238, 39)
        Me.TextEdit40.TabIndex = 6
        '
        'TabNavigationPage5
        '
        Me.TabNavigationPage5.Caption = "Probate Forecast"
        Me.TabNavigationPage5.Controls.Add(Me.LayoutControl6)
        Me.TabNavigationPage5.Name = "TabNavigationPage5"
        Me.TabNavigationPage5.Size = New System.Drawing.Size(1034, 562)
        '
        'LayoutControl6
        '
        Me.LayoutControl6.Controls.Add(Me.TrackBarControl2)
        Me.LayoutControl6.Controls.Add(Me.TrackBarControl1)
        Me.LayoutControl6.Controls.Add(Me.TextEdit22)
        Me.LayoutControl6.Controls.Add(Me.TextEdit23)
        Me.LayoutControl6.Controls.Add(Me.TextEdit24)
        Me.LayoutControl6.Controls.Add(Me.ButtonEdit1)
        Me.LayoutControl6.Location = New System.Drawing.Point(106, 65)
        Me.LayoutControl6.Name = "LayoutControl6"
        Me.LayoutControl6.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1287, 411, 920, 400)
        Me.LayoutControl6.Root = Me.LayoutControlGroup5
        Me.LayoutControl6.Size = New System.Drawing.Size(838, 443)
        Me.LayoutControl6.TabIndex = 0
        Me.LayoutControl6.Text = "LayoutControl6"
        '
        'TrackBarControl2
        '
        Me.TrackBarControl2.EditValue = Nothing
        Me.TrackBarControl2.Location = New System.Drawing.Point(217, 187)
        Me.TrackBarControl2.Name = "TrackBarControl2"
        Me.TrackBarControl2.Properties.LabelAppearance.Options.UseTextOptions = true
        Me.TrackBarControl2.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.TrackBarControl2.Size = New System.Drawing.Size(498, 45)
        Me.TrackBarControl2.StyleController = Me.LayoutControl6
        Me.TrackBarControl2.TabIndex = 10
        '
        'TrackBarControl1
        '
        Me.TrackBarControl1.EditValue = Nothing
        Me.TrackBarControl1.Location = New System.Drawing.Point(217, 54)
        Me.TrackBarControl1.Name = "TrackBarControl1"
        Me.TrackBarControl1.Properties.LabelAppearance.Options.UseTextOptions = true
        Me.TrackBarControl1.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.TrackBarControl1.Size = New System.Drawing.Size(498, 45)
        Me.TrackBarControl1.StyleController = Me.LayoutControl6
        Me.TrackBarControl1.TabIndex = 5
        '
        'TextEdit22
        '
        Me.TextEdit22.Location = New System.Drawing.Point(217, 12)
        Me.TextEdit22.Name = "TextEdit22"
        Me.TextEdit22.Size = New System.Drawing.Size(322, 38)
        Me.TextEdit22.StyleController = Me.LayoutControl6
        Me.TextEdit22.TabIndex = 4
        '
        'TextEdit23
        '
        Me.TextEdit23.Location = New System.Drawing.Point(217, 103)
        Me.TextEdit23.Name = "TextEdit23"
        Me.TextEdit23.Size = New System.Drawing.Size(322, 38)
        Me.TextEdit23.StyleController = Me.LayoutControl6
        Me.TextEdit23.TabIndex = 8
        '
        'TextEdit24
        '
        Me.TextEdit24.Location = New System.Drawing.Point(217, 145)
        Me.TextEdit24.Name = "TextEdit24"
        Me.TextEdit24.Size = New System.Drawing.Size(322, 38)
        Me.TextEdit24.StyleController = Me.LayoutControl6
        Me.TextEdit24.TabIndex = 9
        '
        'ButtonEdit1
        '
        Me.ButtonEdit1.Location = New System.Drawing.Point(217, 236)
        Me.ButtonEdit1.Name = "ButtonEdit1"
        Me.ButtonEdit1.Size = New System.Drawing.Size(322, 38)
        Me.ButtonEdit1.StyleController = Me.LayoutControl6
        Me.ButtonEdit1.TabIndex = 11
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup5.GroupBordersVisible = false
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem61, Me.EmptySpaceItem4, Me.LayoutControlItem62, Me.LayoutControlItem63, Me.EmptySpaceItem5, Me.LayoutControlItem65, Me.SimpleLabelItem3, Me.SimpleLabelItem4, Me.LayoutControlItem64, Me.EmptySpaceItem3, Me.SimpleLabelItem5, Me.LayoutControlItem66, Me.EmptySpaceItem6})
        Me.LayoutControlGroup5.Name = "Root"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(838, 443)
        Me.LayoutControlGroup5.TextVisible = false
        '
        'LayoutControlItem61
        '
        Me.LayoutControlItem61.Control = Me.TextEdit22
        Me.LayoutControlItem61.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem61.Name = "LayoutControlItem61"
        Me.LayoutControlItem61.Size = New System.Drawing.Size(531, 42)
        Me.LayoutControlItem61.Text = "Current State Value"
        Me.LayoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem61.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem61.TextToControlDistance = 5
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = false
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(531, 0)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(287, 42)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem62
        '
        Me.LayoutControlItem62.Control = Me.TrackBarControl1
        Me.LayoutControlItem62.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem62.Name = "LayoutControlItem62"
        Me.LayoutControlItem62.Size = New System.Drawing.Size(707, 49)
        Me.LayoutControlItem62.Text = "Probate Fee (%)"
        Me.LayoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem62.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem62.TextToControlDistance = 5
        '
        'LayoutControlItem63
        '
        Me.LayoutControlItem63.Control = Me.TextEdit23
        Me.LayoutControlItem63.Location = New System.Drawing.Point(0, 91)
        Me.LayoutControlItem63.Name = "LayoutControlItem63"
        Me.LayoutControlItem63.Size = New System.Drawing.Size(531, 42)
        Me.LayoutControlItem63.Text = "Probate Cost"
        Me.LayoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem63.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem63.TextToControlDistance = 5
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = false
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(531, 91)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(287, 42)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem65
        '
        Me.LayoutControlItem65.Control = Me.TextEdit24
        Me.LayoutControlItem65.Location = New System.Drawing.Point(0, 133)
        Me.LayoutControlItem65.Name = "LayoutControlItem65"
        Me.LayoutControlItem65.Size = New System.Drawing.Size(531, 42)
        Me.LayoutControlItem65.Text = "Inflation"
        Me.LayoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem65.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem65.TextToControlDistance = 5
        '
        'SimpleLabelItem3
        '
        Me.SimpleLabelItem3.AllowHotTrack = false
        Me.SimpleLabelItem3.Location = New System.Drawing.Point(531, 133)
        Me.SimpleLabelItem3.Name = "SimpleLabelItem3"
        Me.SimpleLabelItem3.Size = New System.Drawing.Size(287, 42)
        Me.SimpleLabelItem3.Text = "%"
        Me.SimpleLabelItem3.TextSize = New System.Drawing.Size(30, 16)
        '
        'SimpleLabelItem4
        '
        Me.SimpleLabelItem4.AllowHotTrack = false
        Me.SimpleLabelItem4.Location = New System.Drawing.Point(707, 42)
        Me.SimpleLabelItem4.Name = "SimpleLabelItem4"
        Me.SimpleLabelItem4.Size = New System.Drawing.Size(111, 49)
        Me.SimpleLabelItem4.Text = "1.5%"
        Me.SimpleLabelItem4.TextSize = New System.Drawing.Size(30, 16)
        '
        'LayoutControlItem64
        '
        Me.LayoutControlItem64.Control = Me.TrackBarControl2
        Me.LayoutControlItem64.Location = New System.Drawing.Point(0, 175)
        Me.LayoutControlItem64.Name = "LayoutControlItem64"
        Me.LayoutControlItem64.Size = New System.Drawing.Size(707, 49)
        Me.LayoutControlItem64.Text = "Years Unitl Death"
        Me.LayoutControlItem64.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem64.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem64.TextToControlDistance = 5
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = false
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 266)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(818, 157)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'SimpleLabelItem5
        '
        Me.SimpleLabelItem5.AllowHotTrack = false
        Me.SimpleLabelItem5.Location = New System.Drawing.Point(707, 175)
        Me.SimpleLabelItem5.Name = "SimpleLabelItem5"
        Me.SimpleLabelItem5.Size = New System.Drawing.Size(111, 49)
        Me.SimpleLabelItem5.Text = "1"
        Me.SimpleLabelItem5.TextSize = New System.Drawing.Size(30, 16)
        '
        'LayoutControlItem66
        '
        Me.LayoutControlItem66.Control = Me.ButtonEdit1
        Me.LayoutControlItem66.Location = New System.Drawing.Point(0, 224)
        Me.LayoutControlItem66.Name = "LayoutControlItem66"
        Me.LayoutControlItem66.Size = New System.Drawing.Size(531, 42)
        Me.LayoutControlItem66.Text = "Future Probable Cost"
        Me.LayoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem66.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem66.TextToControlDistance = 5
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = false
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(531, 224)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(287, 42)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'NavigationPageWillInstruction
        '
        Me.NavigationPageWillInstruction.Caption = "   Will Instruction"
        Me.NavigationPageWillInstruction.Controls.Add(Me.XtraTabControl1)
        Me.NavigationPageWillInstruction.ImageOptions.Image = CType(resources.GetObject("NavigationPageWillInstruction.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageWillInstruction.Name = "NavigationPageWillInstruction"
        Me.NavigationPageWillInstruction.Size = New System.Drawing.Size(1026, 602)
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(1026, 602)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5, Me.XtraTabPage6, Me.XtraTabPage7, Me.XtraTabPage8, Me.XtraTabPage9, Me.XtraTabPage10})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LabelControl38)
        Me.XtraTabPage1.Controls.Add(Me.ComboBoxEdit4)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl37)
        Me.XtraTabPage1.Controls.Add(Me.CheckEdit46)
        Me.XtraTabPage1.Controls.Add(Me.CheckEdit44)
        Me.XtraTabPage1.Controls.Add(Me.CheckEdit45)
        Me.XtraTabPage1.Controls.Add(Me.CheckEdit43)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl36)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage1.Text = "Client Information"
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl38.Appearance.Options.UseFont = true
        Me.LabelControl38.Location = New System.Drawing.Point(30, 417)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(664, 16)
        Me.LabelControl38.TabIndex = 7
        Me.LabelControl38.Text = "If you are single, do you want your written will to include the partner named her"& _ 
    "e as your future spouse?"
        '
        'ComboBoxEdit4
        '
        Me.ComboBoxEdit4.Location = New System.Drawing.Point(764, 327)
        Me.ComboBoxEdit4.Name = "ComboBoxEdit4"
        Me.ComboBoxEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit4.Properties.NullValuePrompt = "Select Country"
        Me.ComboBoxEdit4.Size = New System.Drawing.Size(208, 38)
        Me.ComboBoxEdit4.TabIndex = 6
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl37.Appearance.Options.UseFont = true
        Me.LabelControl37.Location = New System.Drawing.Point(591, 338)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(135, 16)
        Me.LabelControl37.TabIndex = 5
        Me.LabelControl37.Text = "If YES, what country?"
        '
        'CheckEdit46
        '
        Me.CheckEdit46.Location = New System.Drawing.Point(180, 448)
        Me.CheckEdit46.Name = "CheckEdit46"
        Me.CheckEdit46.Properties.Caption = "NO"
        Me.CheckEdit46.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit46.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit46.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit46.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit46.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit46.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit46.TabIndex = 4
        '
        'CheckEdit44
        '
        Me.CheckEdit44.Location = New System.Drawing.Point(452, 328)
        Me.CheckEdit44.Name = "CheckEdit44"
        Me.CheckEdit44.Properties.Caption = "NO"
        Me.CheckEdit44.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit44.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit44.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit44.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit44.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit44.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit44.TabIndex = 4
        '
        'CheckEdit45
        '
        Me.CheckEdit45.Location = New System.Drawing.Point(30, 448)
        Me.CheckEdit45.Name = "CheckEdit45"
        Me.CheckEdit45.Properties.Caption = "YES"
        Me.CheckEdit45.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit45.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit45.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit45.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit45.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit45.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit45.TabIndex = 4
        '
        'CheckEdit43
        '
        Me.CheckEdit43.Location = New System.Drawing.Point(302, 328)
        Me.CheckEdit43.Name = "CheckEdit43"
        Me.CheckEdit43.Properties.Caption = "YES"
        Me.CheckEdit43.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit43.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit43.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit43.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit43.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit43.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit43.TabIndex = 4
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl36.Appearance.Options.UseFont = true
        Me.LabelControl36.Location = New System.Drawing.Point(30, 338)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(246, 16)
        Me.LabelControl36.TabIndex = 3
        Me.LabelControl36.Text = "Do you have a will in another country?"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.LayoutControl8)
        Me.GroupControl2.Location = New System.Drawing.Point(534, 30)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(460, 276)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Client 2"
        '
        'LayoutControl8
        '
        Me.LayoutControl8.Controls.Add(Me.MemoEdit4)
        Me.LayoutControl8.Controls.Add(Me.DateEdit4)
        Me.LayoutControl8.Controls.Add(Me.TextEdit50)
        Me.LayoutControl8.Controls.Add(Me.TextEdit51)
        Me.LayoutControl8.Location = New System.Drawing.Point(5, 24)
        Me.LayoutControl8.Name = "LayoutControl8"
        Me.LayoutControl8.Root = Me.LayoutControlGroup7
        Me.LayoutControl8.Size = New System.Drawing.Size(445, 241)
        Me.LayoutControl8.TabIndex = 1
        Me.LayoutControl8.Text = "LayoutControl7"
        '
        'MemoEdit4
        '
        Me.MemoEdit4.Location = New System.Drawing.Point(87, 96)
        Me.MemoEdit4.Name = "MemoEdit4"
        Me.MemoEdit4.Size = New System.Drawing.Size(346, 91)
        Me.MemoEdit4.StyleController = Me.LayoutControl8
        Me.MemoEdit4.TabIndex = 6
        '
        'DateEdit4
        '
        Me.DateEdit4.EditValue = Nothing
        Me.DateEdit4.Location = New System.Drawing.Point(87, 54)
        Me.DateEdit4.Name = "DateEdit4"
        Me.DateEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Size = New System.Drawing.Size(346, 38)
        Me.DateEdit4.StyleController = Me.LayoutControl8
        Me.DateEdit4.TabIndex = 5
        '
        'TextEdit50
        '
        Me.TextEdit50.Location = New System.Drawing.Point(87, 12)
        Me.TextEdit50.Name = "TextEdit50"
        Me.TextEdit50.Size = New System.Drawing.Size(346, 38)
        Me.TextEdit50.StyleController = Me.LayoutControl8
        Me.TextEdit50.TabIndex = 4
        '
        'TextEdit51
        '
        Me.TextEdit51.Location = New System.Drawing.Point(90, 191)
        Me.TextEdit51.Name = "TextEdit51"
        Me.TextEdit51.Size = New System.Drawing.Size(169, 38)
        Me.TextEdit51.StyleController = Me.LayoutControl8
        Me.TextEdit51.TabIndex = 7
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup7.GroupBordersVisible = false
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem85, Me.LayoutControlItem86, Me.LayoutControlItem87, Me.LayoutControlItem88, Me.EmptySpaceItem2, Me.EmptySpaceItem9})
        Me.LayoutControlGroup7.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(445, 241)
        Me.LayoutControlGroup7.TextVisible = false
        '
        'LayoutControlItem85
        '
        Me.LayoutControlItem85.Control = Me.TextEdit50
        Me.LayoutControlItem85.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem85.Name = "LayoutControlItem81"
        Me.LayoutControlItem85.Size = New System.Drawing.Size(425, 42)
        Me.LayoutControlItem85.Text = "Name"
        Me.LayoutControlItem85.TextSize = New System.Drawing.Size(72, 16)
        '
        'LayoutControlItem86
        '
        Me.LayoutControlItem86.Control = Me.DateEdit4
        Me.LayoutControlItem86.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem86.Name = "LayoutControlItem82"
        Me.LayoutControlItem86.Size = New System.Drawing.Size(425, 42)
        Me.LayoutControlItem86.Text = "Date of Birth"
        Me.LayoutControlItem86.TextSize = New System.Drawing.Size(72, 16)
        '
        'LayoutControlItem87
        '
        Me.LayoutControlItem87.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem87.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem87.Control = Me.MemoEdit4
        Me.LayoutControlItem87.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem87.Name = "LayoutControlItem83"
        Me.LayoutControlItem87.Size = New System.Drawing.Size(425, 95)
        Me.LayoutControlItem87.Text = "Address"
        Me.LayoutControlItem87.TextSize = New System.Drawing.Size(72, 16)
        '
        'LayoutControlItem88
        '
        Me.LayoutControlItem88.Control = Me.TextEdit51
        Me.LayoutControlItem88.Location = New System.Drawing.Point(78, 179)
        Me.LayoutControlItem88.Name = "LayoutControlItem84"
        Me.LayoutControlItem88.Size = New System.Drawing.Size(173, 42)
        Me.LayoutControlItem88.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem88.TextVisible = false
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = false
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 179)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(78, 42)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem9
        '
        Me.EmptySpaceItem9.AllowHotTrack = false
        Me.EmptySpaceItem9.Location = New System.Drawing.Point(251, 179)
        Me.EmptySpaceItem9.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem9.Size = New System.Drawing.Size(174, 42)
        Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LayoutControl7)
        Me.GroupControl1.Location = New System.Drawing.Point(30, 30)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(460, 276)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Client 1"
        '
        'LayoutControl7
        '
        Me.LayoutControl7.Controls.Add(Me.MemoEdit3)
        Me.LayoutControl7.Controls.Add(Me.DateEdit3)
        Me.LayoutControl7.Controls.Add(Me.TextEdit48)
        Me.LayoutControl7.Controls.Add(Me.TextEdit49)
        Me.LayoutControl7.Location = New System.Drawing.Point(5, 24)
        Me.LayoutControl7.Name = "LayoutControl7"
        Me.LayoutControl7.Root = Me.LayoutControlGroup6
        Me.LayoutControl7.Size = New System.Drawing.Size(445, 241)
        Me.LayoutControl7.TabIndex = 1
        Me.LayoutControl7.Text = "LayoutControl7"
        '
        'MemoEdit3
        '
        Me.MemoEdit3.Location = New System.Drawing.Point(87, 96)
        Me.MemoEdit3.Name = "MemoEdit3"
        Me.MemoEdit3.Size = New System.Drawing.Size(346, 91)
        Me.MemoEdit3.StyleController = Me.LayoutControl7
        Me.MemoEdit3.TabIndex = 6
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = Nothing
        Me.DateEdit3.Location = New System.Drawing.Point(87, 54)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Size = New System.Drawing.Size(346, 38)
        Me.DateEdit3.StyleController = Me.LayoutControl7
        Me.DateEdit3.TabIndex = 5
        '
        'TextEdit48
        '
        Me.TextEdit48.Location = New System.Drawing.Point(87, 12)
        Me.TextEdit48.Name = "TextEdit48"
        Me.TextEdit48.Size = New System.Drawing.Size(346, 38)
        Me.TextEdit48.StyleController = Me.LayoutControl7
        Me.TextEdit48.TabIndex = 4
        '
        'TextEdit49
        '
        Me.TextEdit49.Location = New System.Drawing.Point(90, 191)
        Me.TextEdit49.Name = "TextEdit49"
        Me.TextEdit49.Size = New System.Drawing.Size(169, 38)
        Me.TextEdit49.StyleController = Me.LayoutControl7
        Me.TextEdit49.TabIndex = 7
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup6.GroupBordersVisible = false
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem81, Me.LayoutControlItem82, Me.LayoutControlItem83, Me.LayoutControlItem84, Me.EmptySpaceItem7, Me.EmptySpaceItem8})
        Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(445, 241)
        Me.LayoutControlGroup6.TextVisible = false
        '
        'LayoutControlItem81
        '
        Me.LayoutControlItem81.Control = Me.TextEdit48
        Me.LayoutControlItem81.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem81.Name = "LayoutControlItem81"
        Me.LayoutControlItem81.Size = New System.Drawing.Size(425, 42)
        Me.LayoutControlItem81.Text = "Name"
        Me.LayoutControlItem81.TextSize = New System.Drawing.Size(72, 16)
        '
        'LayoutControlItem82
        '
        Me.LayoutControlItem82.Control = Me.DateEdit3
        Me.LayoutControlItem82.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem82.Name = "LayoutControlItem82"
        Me.LayoutControlItem82.Size = New System.Drawing.Size(425, 42)
        Me.LayoutControlItem82.Text = "Date of Birth"
        Me.LayoutControlItem82.TextSize = New System.Drawing.Size(72, 16)
        '
        'LayoutControlItem83
        '
        Me.LayoutControlItem83.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem83.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem83.Control = Me.MemoEdit3
        Me.LayoutControlItem83.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem83.Name = "LayoutControlItem83"
        Me.LayoutControlItem83.Size = New System.Drawing.Size(425, 95)
        Me.LayoutControlItem83.Text = "Address"
        Me.LayoutControlItem83.TextSize = New System.Drawing.Size(72, 16)
        '
        'LayoutControlItem84
        '
        Me.LayoutControlItem84.Control = Me.TextEdit49
        Me.LayoutControlItem84.Location = New System.Drawing.Point(78, 179)
        Me.LayoutControlItem84.Name = "LayoutControlItem84"
        Me.LayoutControlItem84.Size = New System.Drawing.Size(173, 42)
        Me.LayoutControlItem84.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem84.TextVisible = false
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = false
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(0, 179)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(78, 42)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = false
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(251, 179)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(174, 42)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.GridControl2)
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl9)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage2.Text = "Children"
        '
        'GridControl2
        '
        Me.GridControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GridControl2.Location = New System.Drawing.Point(31, 94)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(969, 452)
        Me.GridControl2.TabIndex = 1
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumnName, Me.GridColumnDOB, Me.GridColumnRelationShipToClient1, Me.GridColumnRelationshipToClient2, Me.GridColumnActionInWillInstruction})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowIndicator = false
        Me.GridView2.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumnName
        '
        Me.GridColumnName.Caption = "Name"
        Me.GridColumnName.Name = "GridColumnName"
        Me.GridColumnName.Visible = true
        Me.GridColumnName.VisibleIndex = 0
        '
        'GridColumnDOB
        '
        Me.GridColumnDOB.Caption = "Date of Birth"
        Me.GridColumnDOB.Name = "GridColumnDOB"
        Me.GridColumnDOB.Visible = true
        Me.GridColumnDOB.VisibleIndex = 1
        '
        'GridColumnRelationShipToClient1
        '
        Me.GridColumnRelationShipToClient1.Caption = "Relationship to Client 1"
        Me.GridColumnRelationShipToClient1.Name = "GridColumnRelationShipToClient1"
        Me.GridColumnRelationShipToClient1.Visible = true
        Me.GridColumnRelationShipToClient1.VisibleIndex = 2
        '
        'GridColumnRelationshipToClient2
        '
        Me.GridColumnRelationshipToClient2.Caption = "Relationship to Client 2"
        Me.GridColumnRelationshipToClient2.Name = "GridColumnRelationshipToClient2"
        Me.GridColumnRelationshipToClient2.Visible = true
        Me.GridColumnRelationshipToClient2.VisibleIndex = 3
        '
        'GridColumnActionInWillInstruction
        '
        Me.GridColumnActionInWillInstruction.Caption = "Action"
        Me.GridColumnActionInWillInstruction.Name = "GridColumnActionInWillInstruction"
        Me.GridColumnActionInWillInstruction.Visible = true
        Me.GridColumnActionInWillInstruction.VisibleIndex = 4
        '
        'LayoutControl9
        '
        Me.LayoutControl9.Controls.Add(Me.CheckEdit47)
        Me.LayoutControl9.Controls.Add(Me.CheckEdit48)
        Me.LayoutControl9.Controls.Add(Me.SimpleButton7)
        Me.LayoutControl9.Location = New System.Drawing.Point(19, 20)
        Me.LayoutControl9.Name = "LayoutControl9"
        Me.LayoutControl9.Root = Me.LayoutControlGroup8
        Me.LayoutControl9.Size = New System.Drawing.Size(989, 67)
        Me.LayoutControl9.TabIndex = 0
        Me.LayoutControl9.Text = "LayoutControl9"
        '
        'CheckEdit47
        '
        Me.CheckEdit47.Location = New System.Drawing.Point(217, 12)
        Me.CheckEdit47.Name = "CheckEdit47"
        Me.CheckEdit47.Properties.Caption = "YES"
        Me.CheckEdit47.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit47.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit47.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit47.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit47.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit47.Size = New System.Drawing.Size(99, 36)
        Me.CheckEdit47.StyleController = Me.LayoutControl9
        Me.CheckEdit47.TabIndex = 4
        '
        'CheckEdit48
        '
        Me.CheckEdit48.Location = New System.Drawing.Point(320, 12)
        Me.CheckEdit48.Name = "CheckEdit48"
        Me.CheckEdit48.Properties.Caption = "NO"
        Me.CheckEdit48.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit48.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit48.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit48.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit48.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit48.Size = New System.Drawing.Size(500, 36)
        Me.CheckEdit48.StyleController = Me.LayoutControl9
        Me.CheckEdit48.TabIndex = 5
        '
        'SimpleButton7
        '
        Me.SimpleButton7.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton7.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton7.Location = New System.Drawing.Point(824, 12)
        Me.SimpleButton7.Name = "SimpleButton7"
        Me.SimpleButton7.Size = New System.Drawing.Size(153, 42)
        Me.SimpleButton7.StyleController = Me.LayoutControl9
        Me.SimpleButton7.TabIndex = 6
        Me.SimpleButton7.Text = "Add a Child"
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup8.GroupBordersVisible = false
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem89, Me.LayoutControlItem90, Me.LayoutControlItem91})
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(989, 67)
        Me.LayoutControlGroup8.TextVisible = false
        '
        'LayoutControlItem89
        '
        Me.LayoutControlItem89.Control = Me.CheckEdit47
        Me.LayoutControlItem89.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem89.Name = "LayoutControlItem89"
        Me.LayoutControlItem89.Size = New System.Drawing.Size(308, 47)
        Me.LayoutControlItem89.Text = "Do you have a children?"
        Me.LayoutControlItem89.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem89.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem89.TextToControlDistance = 5
        '
        'LayoutControlItem90
        '
        Me.LayoutControlItem90.Control = Me.CheckEdit48
        Me.LayoutControlItem90.Location = New System.Drawing.Point(308, 0)
        Me.LayoutControlItem90.Name = "LayoutControlItem90"
        Me.LayoutControlItem90.Size = New System.Drawing.Size(504, 47)
        Me.LayoutControlItem90.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem90.TextVisible = false
        '
        'LayoutControlItem91
        '
        Me.LayoutControlItem91.Control = Me.SimpleButton7
        Me.LayoutControlItem91.Location = New System.Drawing.Point(812, 0)
        Me.LayoutControlItem91.Name = "LayoutControlItem91"
        Me.LayoutControlItem91.Size = New System.Drawing.Size(157, 47)
        Me.LayoutControlItem91.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem91.TextVisible = false
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.GridControl3)
        Me.XtraTabPage3.Controls.Add(Me.LayoutControl10)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage3.Text = "Executors"
        '
        'GridControl3
        '
        Me.GridControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GridControl3.Location = New System.Drawing.Point(22, 235)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(988, 317)
        Me.GridControl3.TabIndex = 1
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        '
        'LayoutControl10
        '
        Me.LayoutControl10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit49)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit50)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit51)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit52)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit53)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit54)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit55)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit56)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit57)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit58)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit59)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit60)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit61)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit62)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit63)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit64)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit65)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit66)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit67)
        Me.LayoutControl10.Controls.Add(Me.CheckEdit68)
        Me.LayoutControl10.Controls.Add(Me.SimpleButton8)
        Me.LayoutControl10.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem110})
        Me.LayoutControl10.Location = New System.Drawing.Point(6, 2)
        Me.LayoutControl10.Name = "LayoutControl10"
        Me.LayoutControl10.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1284, 206, 650, 882)
        Me.LayoutControl10.Root = Me.LayoutControlGroup9
        Me.LayoutControl10.Size = New System.Drawing.Size(1016, 226)
        Me.LayoutControl10.TabIndex = 0
        Me.LayoutControl10.Text = "LayoutControl10"
        '
        'CheckEdit49
        '
        Me.CheckEdit49.Location = New System.Drawing.Point(335, 12)
        Me.CheckEdit49.Name = "CheckEdit49"
        Me.CheckEdit49.Properties.Caption = "YES"
        Me.CheckEdit49.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit49.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit49.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit49.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit49.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit49.Size = New System.Drawing.Size(71, 36)
        Me.CheckEdit49.StyleController = Me.LayoutControl10
        Me.CheckEdit49.TabIndex = 4
        '
        'CheckEdit50
        '
        Me.CheckEdit50.Location = New System.Drawing.Point(410, 12)
        Me.CheckEdit50.Name = "CheckEdit50"
        Me.CheckEdit50.Properties.Caption = "NO"
        Me.CheckEdit50.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit50.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit50.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit50.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit50.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit50.Size = New System.Drawing.Size(594, 36)
        Me.CheckEdit50.StyleController = Me.LayoutControl10
        Me.CheckEdit50.TabIndex = 5
        '
        'CheckEdit51
        '
        Me.CheckEdit51.Location = New System.Drawing.Point(335, 52)
        Me.CheckEdit51.Name = "CheckEdit51"
        Me.CheckEdit51.Properties.Caption = "YES"
        Me.CheckEdit51.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit51.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit51.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit51.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit51.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit51.Size = New System.Drawing.Size(71, 36)
        Me.CheckEdit51.StyleController = Me.LayoutControl10
        Me.CheckEdit51.TabIndex = 6
        '
        'CheckEdit52
        '
        Me.CheckEdit52.Location = New System.Drawing.Point(410, 52)
        Me.CheckEdit52.Name = "CheckEdit52"
        Me.CheckEdit52.Properties.Caption = "NO"
        Me.CheckEdit52.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit52.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit52.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit52.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit52.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit52.Size = New System.Drawing.Size(66, 36)
        Me.CheckEdit52.StyleController = Me.LayoutControl10
        Me.CheckEdit52.TabIndex = 7
        '
        'CheckEdit53
        '
        Me.CheckEdit53.Location = New System.Drawing.Point(675, 52)
        Me.CheckEdit53.Name = "CheckEdit53"
        Me.CheckEdit53.Properties.Caption = "SOLE"
        Me.CheckEdit53.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit53.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit53.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit53.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit53.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit53.Size = New System.Drawing.Size(81, 36)
        Me.CheckEdit53.StyleController = Me.LayoutControl10
        Me.CheckEdit53.TabIndex = 8
        '
        'CheckEdit54
        '
        Me.CheckEdit54.Location = New System.Drawing.Point(760, 52)
        Me.CheckEdit54.Name = "CheckEdit54"
        Me.CheckEdit54.Properties.Caption = "JOINT"
        Me.CheckEdit54.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit54.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit54.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit54.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit54.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit54.Size = New System.Drawing.Size(83, 36)
        Me.CheckEdit54.StyleController = Me.LayoutControl10
        Me.CheckEdit54.TabIndex = 9
        '
        'CheckEdit55
        '
        Me.CheckEdit55.Location = New System.Drawing.Point(847, 52)
        Me.CheckEdit55.Name = "CheckEdit55"
        Me.CheckEdit55.Properties.Caption = "SUBSTITUTE"
        Me.CheckEdit55.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit55.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit55.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit55.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit55.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit55.Size = New System.Drawing.Size(157, 36)
        Me.CheckEdit55.StyleController = Me.LayoutControl10
        Me.CheckEdit55.TabIndex = 10
        '
        'CheckEdit56
        '
        Me.CheckEdit56.Location = New System.Drawing.Point(335, 92)
        Me.CheckEdit56.Name = "CheckEdit56"
        Me.CheckEdit56.Properties.Caption = "YES"
        Me.CheckEdit56.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit56.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit56.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit56.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit56.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit56.Size = New System.Drawing.Size(71, 36)
        Me.CheckEdit56.StyleController = Me.LayoutControl10
        Me.CheckEdit56.TabIndex = 11
        '
        'CheckEdit57
        '
        Me.CheckEdit57.Location = New System.Drawing.Point(410, 92)
        Me.CheckEdit57.Name = "CheckEdit57"
        Me.CheckEdit57.Properties.Caption = "NO"
        Me.CheckEdit57.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit57.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit57.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit57.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit57.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit57.Size = New System.Drawing.Size(66, 36)
        Me.CheckEdit57.StyleController = Me.LayoutControl10
        Me.CheckEdit57.TabIndex = 12
        '
        'CheckEdit58
        '
        Me.CheckEdit58.Location = New System.Drawing.Point(675, 92)
        Me.CheckEdit58.Name = "CheckEdit58"
        Me.CheckEdit58.Properties.Caption = "SOLE"
        Me.CheckEdit58.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit58.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit58.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit58.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit58.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit58.Size = New System.Drawing.Size(81, 36)
        Me.CheckEdit58.StyleController = Me.LayoutControl10
        Me.CheckEdit58.TabIndex = 13
        '
        'CheckEdit59
        '
        Me.CheckEdit59.Location = New System.Drawing.Point(760, 92)
        Me.CheckEdit59.Name = "CheckEdit59"
        Me.CheckEdit59.Properties.Caption = "JOINT"
        Me.CheckEdit59.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit59.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit59.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit59.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit59.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit59.Size = New System.Drawing.Size(83, 36)
        Me.CheckEdit59.StyleController = Me.LayoutControl10
        Me.CheckEdit59.TabIndex = 14
        '
        'CheckEdit60
        '
        Me.CheckEdit60.Location = New System.Drawing.Point(847, 92)
        Me.CheckEdit60.Name = "CheckEdit60"
        Me.CheckEdit60.Properties.Caption = "SUBSTITUTE"
        Me.CheckEdit60.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit60.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit60.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit60.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit60.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit60.Size = New System.Drawing.Size(157, 36)
        Me.CheckEdit60.StyleController = Me.LayoutControl10
        Me.CheckEdit60.TabIndex = 15
        '
        'CheckEdit61
        '
        Me.CheckEdit61.Location = New System.Drawing.Point(335, 132)
        Me.CheckEdit61.Name = "CheckEdit61"
        Me.CheckEdit61.Properties.Caption = "YES"
        Me.CheckEdit61.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit61.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit61.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit61.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit61.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit61.Size = New System.Drawing.Size(71, 36)
        Me.CheckEdit61.StyleController = Me.LayoutControl10
        Me.CheckEdit61.TabIndex = 16
        '
        'CheckEdit62
        '
        Me.CheckEdit62.Location = New System.Drawing.Point(410, 132)
        Me.CheckEdit62.Name = "CheckEdit62"
        Me.CheckEdit62.Properties.Caption = "NO"
        Me.CheckEdit62.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit62.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit62.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit62.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit62.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit62.Size = New System.Drawing.Size(66, 36)
        Me.CheckEdit62.StyleController = Me.LayoutControl10
        Me.CheckEdit62.TabIndex = 17
        '
        'CheckEdit63
        '
        Me.CheckEdit63.Location = New System.Drawing.Point(675, 132)
        Me.CheckEdit63.Name = "CheckEdit63"
        Me.CheckEdit63.Properties.Caption = "SOLE"
        Me.CheckEdit63.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit63.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit63.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit63.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit63.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit63.Size = New System.Drawing.Size(81, 36)
        Me.CheckEdit63.StyleController = Me.LayoutControl10
        Me.CheckEdit63.TabIndex = 18
        '
        'CheckEdit64
        '
        Me.CheckEdit64.Location = New System.Drawing.Point(760, 132)
        Me.CheckEdit64.Name = "CheckEdit64"
        Me.CheckEdit64.Properties.Caption = "JOINT"
        Me.CheckEdit64.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit64.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit64.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit64.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit64.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit64.Size = New System.Drawing.Size(83, 36)
        Me.CheckEdit64.StyleController = Me.LayoutControl10
        Me.CheckEdit64.TabIndex = 19
        '
        'CheckEdit65
        '
        Me.CheckEdit65.Location = New System.Drawing.Point(847, 132)
        Me.CheckEdit65.Name = "CheckEdit65"
        Me.CheckEdit65.Properties.Caption = "SUBSTITUTE"
        Me.CheckEdit65.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit65.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit65.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit65.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit65.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit65.Size = New System.Drawing.Size(157, 36)
        Me.CheckEdit65.StyleController = Me.LayoutControl10
        Me.CheckEdit65.TabIndex = 20
        '
        'CheckEdit66
        '
        Me.CheckEdit66.Location = New System.Drawing.Point(335, 172)
        Me.CheckEdit66.Name = "CheckEdit66"
        Me.CheckEdit66.Properties.Caption = "YES"
        Me.CheckEdit66.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit66.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit66.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit66.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit66.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit66.Size = New System.Drawing.Size(71, 36)
        Me.CheckEdit66.StyleController = Me.LayoutControl10
        Me.CheckEdit66.TabIndex = 21
        '
        'CheckEdit67
        '
        Me.CheckEdit67.Location = New System.Drawing.Point(838, 204)
        Me.CheckEdit67.Name = "CheckEdit67"
        Me.CheckEdit67.Properties.Caption = "CheckEdit67"
        Me.CheckEdit67.Size = New System.Drawing.Size(174, 44)
        Me.CheckEdit67.StyleController = Me.LayoutControl10
        Me.CheckEdit67.TabIndex = 22
        '
        'CheckEdit68
        '
        Me.CheckEdit68.Location = New System.Drawing.Point(410, 172)
        Me.CheckEdit68.Name = "CheckEdit68"
        Me.CheckEdit68.Properties.Caption = "NO"
        Me.CheckEdit68.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit68.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit68.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit68.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit68.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit68.Size = New System.Drawing.Size(66, 36)
        Me.CheckEdit68.StyleController = Me.LayoutControl10
        Me.CheckEdit68.TabIndex = 23
        '
        'SimpleButton8
        '
        Me.SimpleButton8.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton8.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton8.Location = New System.Drawing.Point(760, 172)
        Me.SimpleButton8.Name = "SimpleButton8"
        Me.SimpleButton8.Size = New System.Drawing.Size(244, 42)
        Me.SimpleButton8.StyleController = Me.LayoutControl10
        Me.SimpleButton8.TabIndex = 24
        Me.SimpleButton8.Text = "Add an Executor"
        '
        'LayoutControlItem110
        '
        Me.LayoutControlItem110.Control = Me.CheckEdit67
        Me.LayoutControlItem110.Location = New System.Drawing.Point(502, 192)
        Me.LayoutControlItem110.Name = "LayoutControlItem110"
        Me.LayoutControlItem110.Size = New System.Drawing.Size(502, 48)
        Me.LayoutControlItem110.TextSize = New System.Drawing.Size(50, 20)
        '
        'LayoutControlGroup9
        '
        Me.LayoutControlGroup9.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup9.GroupBordersVisible = false
        Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem92, Me.LayoutControlItem93, Me.LayoutControlItem94, Me.LayoutControlItem95, Me.LayoutControlItem96, Me.LayoutControlItem97, Me.LayoutControlItem98, Me.EmptySpaceItem11, Me.LayoutControlItem99, Me.LayoutControlItem100, Me.EmptySpaceItem12, Me.LayoutControlItem101, Me.LayoutControlItem102, Me.LayoutControlItem103, Me.LayoutControlItem104, Me.LayoutControlItem105, Me.EmptySpaceItem13, Me.LayoutControlItem106, Me.LayoutControlItem107, Me.LayoutControlItem108, Me.LayoutControlItem109, Me.LayoutControlItem111, Me.EmptySpaceItem14, Me.LayoutControlItem112})
        Me.LayoutControlGroup9.Name = "Root"
        Me.LayoutControlGroup9.Size = New System.Drawing.Size(1016, 226)
        Me.LayoutControlGroup9.TextVisible = false
        '
        'LayoutControlItem92
        '
        Me.LayoutControlItem92.Control = Me.CheckEdit49
        Me.LayoutControlItem92.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem92.Name = "LayoutControlItem92"
        Me.LayoutControlItem92.Size = New System.Drawing.Size(398, 40)
        Me.LayoutControlItem92.Text = "Do you require a Probate Plan?"
        Me.LayoutControlItem92.TextSize = New System.Drawing.Size(320, 16)
        '
        'LayoutControlItem93
        '
        Me.LayoutControlItem93.Control = Me.CheckEdit50
        Me.LayoutControlItem93.Location = New System.Drawing.Point(398, 0)
        Me.LayoutControlItem93.Name = "LayoutControlItem93"
        Me.LayoutControlItem93.Size = New System.Drawing.Size(598, 40)
        Me.LayoutControlItem93.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem93.TextVisible = false
        '
        'LayoutControlItem94
        '
        Me.LayoutControlItem94.Control = Me.CheckEdit51
        Me.LayoutControlItem94.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControlItem94.Name = "LayoutControlItem94"
        Me.LayoutControlItem94.Size = New System.Drawing.Size(398, 40)
        Me.LayoutControlItem94.Text = "Do you want our company to be your executor?"
        Me.LayoutControlItem94.TextSize = New System.Drawing.Size(320, 16)
        '
        'LayoutControlItem95
        '
        Me.LayoutControlItem95.Control = Me.CheckEdit52
        Me.LayoutControlItem95.Location = New System.Drawing.Point(398, 40)
        Me.LayoutControlItem95.Name = "LayoutControlItem95"
        Me.LayoutControlItem95.Size = New System.Drawing.Size(70, 40)
        Me.LayoutControlItem95.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem95.TextVisible = false
        '
        'LayoutControlItem96
        '
        Me.LayoutControlItem96.Control = Me.CheckEdit53
        Me.LayoutControlItem96.Location = New System.Drawing.Point(558, 40)
        Me.LayoutControlItem96.Name = "LayoutControlItem96"
        Me.LayoutControlItem96.Size = New System.Drawing.Size(190, 40)
        Me.LayoutControlItem96.Text = "Executor Type"
        Me.LayoutControlItem96.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem96.TextSize = New System.Drawing.Size(100, 16)
        Me.LayoutControlItem96.TextToControlDistance = 5
        '
        'LayoutControlItem97
        '
        Me.LayoutControlItem97.Control = Me.CheckEdit54
        Me.LayoutControlItem97.Location = New System.Drawing.Point(748, 40)
        Me.LayoutControlItem97.Name = "LayoutControlItem97"
        Me.LayoutControlItem97.Size = New System.Drawing.Size(87, 40)
        Me.LayoutControlItem97.Text = "JOINT"
        Me.LayoutControlItem97.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem97.TextVisible = false
        '
        'LayoutControlItem98
        '
        Me.LayoutControlItem98.Control = Me.CheckEdit55
        Me.LayoutControlItem98.Location = New System.Drawing.Point(835, 40)
        Me.LayoutControlItem98.Name = "LayoutControlItem98"
        Me.LayoutControlItem98.Size = New System.Drawing.Size(161, 40)
        Me.LayoutControlItem98.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem98.TextVisible = false
        '
        'EmptySpaceItem11
        '
        Me.EmptySpaceItem11.AllowHotTrack = false
        Me.EmptySpaceItem11.Location = New System.Drawing.Point(468, 40)
        Me.EmptySpaceItem11.Name = "EmptySpaceItem11"
        Me.EmptySpaceItem11.Size = New System.Drawing.Size(90, 40)
        Me.EmptySpaceItem11.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem99
        '
        Me.LayoutControlItem99.Control = Me.CheckEdit56
        Me.LayoutControlItem99.Location = New System.Drawing.Point(0, 80)
        Me.LayoutControlItem99.Name = "LayoutControlItem99"
        Me.LayoutControlItem99.Size = New System.Drawing.Size(398, 40)
        Me.LayoutControlItem99.Text = "Do you wish your spouse to be your executor?"
        Me.LayoutControlItem99.TextSize = New System.Drawing.Size(320, 16)
        '
        'LayoutControlItem100
        '
        Me.LayoutControlItem100.Control = Me.CheckEdit57
        Me.LayoutControlItem100.Location = New System.Drawing.Point(398, 80)
        Me.LayoutControlItem100.Name = "LayoutControlItem100"
        Me.LayoutControlItem100.Size = New System.Drawing.Size(70, 40)
        Me.LayoutControlItem100.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem100.TextVisible = false
        '
        'EmptySpaceItem12
        '
        Me.EmptySpaceItem12.AllowHotTrack = false
        Me.EmptySpaceItem12.Location = New System.Drawing.Point(468, 80)
        Me.EmptySpaceItem12.Name = "EmptySpaceItem12"
        Me.EmptySpaceItem12.Size = New System.Drawing.Size(90, 40)
        Me.EmptySpaceItem12.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem101
        '
        Me.LayoutControlItem101.Control = Me.CheckEdit58
        Me.LayoutControlItem101.Location = New System.Drawing.Point(558, 80)
        Me.LayoutControlItem101.Name = "LayoutControlItem101"
        Me.LayoutControlItem101.Size = New System.Drawing.Size(190, 40)
        Me.LayoutControlItem101.Text = "Executor Type"
        Me.LayoutControlItem101.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem101.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem101.TextToControlDistance = 5
        '
        'LayoutControlItem102
        '
        Me.LayoutControlItem102.Control = Me.CheckEdit59
        Me.LayoutControlItem102.Location = New System.Drawing.Point(748, 80)
        Me.LayoutControlItem102.Name = "LayoutControlItem102"
        Me.LayoutControlItem102.Size = New System.Drawing.Size(87, 40)
        Me.LayoutControlItem102.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem102.TextVisible = false
        '
        'LayoutControlItem103
        '
        Me.LayoutControlItem103.Control = Me.CheckEdit60
        Me.LayoutControlItem103.Location = New System.Drawing.Point(835, 80)
        Me.LayoutControlItem103.Name = "LayoutControlItem103"
        Me.LayoutControlItem103.Size = New System.Drawing.Size(161, 40)
        Me.LayoutControlItem103.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem103.TextVisible = false
        '
        'LayoutControlItem104
        '
        Me.LayoutControlItem104.Control = Me.CheckEdit61
        Me.LayoutControlItem104.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem104.Name = "LayoutControlItem104"
        Me.LayoutControlItem104.Size = New System.Drawing.Size(398, 40)
        Me.LayoutControlItem104.Text = "Do you wish ALL of your children to be your executors?"
        Me.LayoutControlItem104.TextSize = New System.Drawing.Size(320, 16)
        '
        'LayoutControlItem105
        '
        Me.LayoutControlItem105.Control = Me.CheckEdit62
        Me.LayoutControlItem105.Location = New System.Drawing.Point(398, 120)
        Me.LayoutControlItem105.Name = "LayoutControlItem105"
        Me.LayoutControlItem105.Size = New System.Drawing.Size(70, 40)
        Me.LayoutControlItem105.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem105.TextVisible = false
        '
        'EmptySpaceItem13
        '
        Me.EmptySpaceItem13.AllowHotTrack = false
        Me.EmptySpaceItem13.Location = New System.Drawing.Point(468, 120)
        Me.EmptySpaceItem13.Name = "EmptySpaceItem13"
        Me.EmptySpaceItem13.Size = New System.Drawing.Size(90, 40)
        Me.EmptySpaceItem13.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem106
        '
        Me.LayoutControlItem106.Control = Me.CheckEdit63
        Me.LayoutControlItem106.Location = New System.Drawing.Point(558, 120)
        Me.LayoutControlItem106.Name = "LayoutControlItem106"
        Me.LayoutControlItem106.Size = New System.Drawing.Size(190, 40)
        Me.LayoutControlItem106.Text = "Executory Type"
        Me.LayoutControlItem106.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem106.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem106.TextToControlDistance = 5
        '
        'LayoutControlItem107
        '
        Me.LayoutControlItem107.Control = Me.CheckEdit64
        Me.LayoutControlItem107.Location = New System.Drawing.Point(748, 120)
        Me.LayoutControlItem107.Name = "LayoutControlItem107"
        Me.LayoutControlItem107.Size = New System.Drawing.Size(87, 40)
        Me.LayoutControlItem107.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem107.TextVisible = false
        '
        'LayoutControlItem108
        '
        Me.LayoutControlItem108.Control = Me.CheckEdit65
        Me.LayoutControlItem108.Location = New System.Drawing.Point(835, 120)
        Me.LayoutControlItem108.Name = "LayoutControlItem108"
        Me.LayoutControlItem108.Size = New System.Drawing.Size(161, 40)
        Me.LayoutControlItem108.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem108.TextVisible = false
        '
        'LayoutControlItem109
        '
        Me.LayoutControlItem109.Control = Me.CheckEdit66
        Me.LayoutControlItem109.Location = New System.Drawing.Point(0, 160)
        Me.LayoutControlItem109.Name = "LayoutControlItem109"
        Me.LayoutControlItem109.Size = New System.Drawing.Size(398, 46)
        Me.LayoutControlItem109.Text = "Do you want to add any other executors?"
        Me.LayoutControlItem109.TextSize = New System.Drawing.Size(320, 16)
        '
        'LayoutControlItem111
        '
        Me.LayoutControlItem111.Control = Me.CheckEdit68
        Me.LayoutControlItem111.Location = New System.Drawing.Point(398, 160)
        Me.LayoutControlItem111.Name = "LayoutControlItem111"
        Me.LayoutControlItem111.Size = New System.Drawing.Size(70, 46)
        Me.LayoutControlItem111.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem111.TextVisible = false
        '
        'EmptySpaceItem14
        '
        Me.EmptySpaceItem14.AllowHotTrack = false
        Me.EmptySpaceItem14.Location = New System.Drawing.Point(468, 160)
        Me.EmptySpaceItem14.Name = "EmptySpaceItem14"
        Me.EmptySpaceItem14.Size = New System.Drawing.Size(280, 46)
        Me.EmptySpaceItem14.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem112
        '
        Me.LayoutControlItem112.Control = Me.SimpleButton8
        Me.LayoutControlItem112.Location = New System.Drawing.Point(748, 160)
        Me.LayoutControlItem112.Name = "LayoutControlItem112"
        Me.LayoutControlItem112.Size = New System.Drawing.Size(248, 46)
        Me.LayoutControlItem112.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem112.TextVisible = false
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.SimpleButton9)
        Me.XtraTabPage4.Controls.Add(Me.GridControl4)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage4.Text = "Guardians"
        '
        'SimpleButton9
        '
        Me.SimpleButton9.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton9.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton9.Location = New System.Drawing.Point(19, 11)
        Me.SimpleButton9.Name = "SimpleButton9"
        Me.SimpleButton9.Size = New System.Drawing.Size(192, 42)
        Me.SimpleButton9.TabIndex = 1
        Me.SimpleButton9.Text = "Add a Guardian"
        '
        'GridControl4
        '
        Me.GridControl4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GridControl4.Location = New System.Drawing.Point(19, 59)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(984, 496)
        Me.GridControl4.TabIndex = 0
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5})
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Name"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = true
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Guardian Type"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = true
        Me.GridColumn2.VisibleIndex = 1
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Relationship to Client 1"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = true
        Me.GridColumn3.VisibleIndex = 2
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Relationship to Client 2"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = true
        Me.GridColumn4.VisibleIndex = 3
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Action"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = true
        Me.GridColumn5.VisibleIndex = 4
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.LabelControl40)
        Me.XtraTabPage5.Controls.Add(Me.LabelControl39)
        Me.XtraTabPage5.Controls.Add(Me.LayoutControl12)
        Me.XtraTabPage5.Controls.Add(Me.LayoutControl11)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage5.Text = "Funeral Arrangements"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl40.Appearance.Options.UseFont = true
        Me.LabelControl40.Location = New System.Drawing.Point(539, 31)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(48, 16)
        Me.LabelControl40.TabIndex = 1
        Me.LabelControl40.Text = "Client 2"
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl39.Appearance.Options.UseFont = true
        Me.LabelControl39.Location = New System.Drawing.Point(34, 31)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(48, 16)
        Me.LabelControl39.TabIndex = 1
        Me.LabelControl39.Text = "Client 1"
        '
        'LayoutControl12
        '
        Me.LayoutControl12.Controls.Add(Me.CheckEdit72)
        Me.LayoutControl12.Controls.Add(Me.CheckEdit73)
        Me.LayoutControl12.Controls.Add(Me.CheckEdit74)
        Me.LayoutControl12.Controls.Add(Me.MemoEdit6)
        Me.LayoutControl12.Controls.Add(Me.CheckEdit77)
        Me.LayoutControl12.Controls.Add(Me.CheckEdit78)
        Me.LayoutControl12.Location = New System.Drawing.Point(527, 48)
        Me.LayoutControl12.Name = "LayoutControl12"
        Me.LayoutControl12.Root = Me.LayoutControlGroup11
        Me.LayoutControl12.Size = New System.Drawing.Size(484, 387)
        Me.LayoutControl12.TabIndex = 0
        Me.LayoutControl12.Text = "LayoutControl11"
        '
        'CheckEdit72
        '
        Me.CheckEdit72.Location = New System.Drawing.Point(12, 32)
        Me.CheckEdit72.Name = "CheckEdit72"
        Me.CheckEdit72.Properties.Caption = "CREMATION"
        Me.CheckEdit72.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit72.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit72.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit72.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit72.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit72.Size = New System.Drawing.Size(183, 36)
        Me.CheckEdit72.StyleController = Me.LayoutControl12
        Me.CheckEdit72.TabIndex = 4
        '
        'CheckEdit73
        '
        Me.CheckEdit73.Location = New System.Drawing.Point(199, 32)
        Me.CheckEdit73.Name = "CheckEdit73"
        Me.CheckEdit73.Properties.Caption = "BURIAL"
        Me.CheckEdit73.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit73.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit73.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit73.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit73.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit73.Size = New System.Drawing.Size(125, 36)
        Me.CheckEdit73.StyleController = Me.LayoutControl12
        Me.CheckEdit73.TabIndex = 5
        '
        'CheckEdit74
        '
        Me.CheckEdit74.Location = New System.Drawing.Point(328, 32)
        Me.CheckEdit74.Name = "CheckEdit74"
        Me.CheckEdit74.Properties.Caption = "NOT REQUIRED"
        Me.CheckEdit74.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit74.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit74.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit74.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit74.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit74.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit74.StyleController = Me.LayoutControl12
        Me.CheckEdit74.TabIndex = 6
        '
        'MemoEdit6
        '
        Me.MemoEdit6.Location = New System.Drawing.Point(106, 152)
        Me.MemoEdit6.Name = "MemoEdit6"
        Me.MemoEdit6.Size = New System.Drawing.Size(366, 125)
        Me.MemoEdit6.StyleController = Me.LayoutControl12
        Me.MemoEdit6.TabIndex = 7
        '
        'CheckEdit77
        '
        Me.CheckEdit77.Location = New System.Drawing.Point(197, 112)
        Me.CheckEdit77.Name = "CheckEdit77"
        Me.CheckEdit77.Properties.Caption = "YES"
        Me.CheckEdit77.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit77.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit77.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit77.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit77.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit77.Size = New System.Drawing.Size(127, 36)
        Me.CheckEdit77.StyleController = Me.LayoutControl12
        Me.CheckEdit77.TabIndex = 8
        '
        'CheckEdit78
        '
        Me.CheckEdit78.Location = New System.Drawing.Point(328, 112)
        Me.CheckEdit78.Name = "CheckEdit78"
        Me.CheckEdit78.Properties.Caption = "NO"
        Me.CheckEdit78.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit78.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit78.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit78.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit78.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit78.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit78.StyleController = Me.LayoutControl12
        Me.CheckEdit78.TabIndex = 9
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup11.GroupBordersVisible = false
        Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem116, Me.EmptySpaceItem15, Me.LayoutControlItem117, Me.LayoutControlItem118, Me.SimpleLabelItem7, Me.EmptySpaceItem19, Me.LayoutControlItem119, Me.LayoutControlItem123, Me.LayoutControlItem124})
        Me.LayoutControlGroup11.Name = "LayoutControlGroup10"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(484, 387)
        Me.LayoutControlGroup11.TextVisible = false
        '
        'LayoutControlItem116
        '
        Me.LayoutControlItem116.Control = Me.CheckEdit72
        Me.LayoutControlItem116.Location = New System.Drawing.Point(0, 20)
        Me.LayoutControlItem116.Name = "LayoutControlItem113"
        Me.LayoutControlItem116.Size = New System.Drawing.Size(187, 40)
        Me.LayoutControlItem116.Text = "Funeral Type"
        Me.LayoutControlItem116.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem116.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem116.TextToControlDistance = 0
        Me.LayoutControlItem116.TextVisible = false
        '
        'EmptySpaceItem15
        '
        Me.EmptySpaceItem15.AllowHotTrack = false
        Me.EmptySpaceItem15.Location = New System.Drawing.Point(0, 269)
        Me.EmptySpaceItem15.Name = "EmptySpaceItem10"
        Me.EmptySpaceItem15.Size = New System.Drawing.Size(464, 98)
        Me.EmptySpaceItem15.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem117
        '
        Me.LayoutControlItem117.Control = Me.CheckEdit73
        Me.LayoutControlItem117.Location = New System.Drawing.Point(187, 20)
        Me.LayoutControlItem117.Name = "LayoutControlItem114"
        Me.LayoutControlItem117.Size = New System.Drawing.Size(129, 40)
        Me.LayoutControlItem117.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem117.TextVisible = false
        '
        'LayoutControlItem118
        '
        Me.LayoutControlItem118.Control = Me.CheckEdit74
        Me.LayoutControlItem118.Location = New System.Drawing.Point(316, 20)
        Me.LayoutControlItem118.Name = "LayoutControlItem115"
        Me.LayoutControlItem118.Size = New System.Drawing.Size(148, 40)
        Me.LayoutControlItem118.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem118.TextVisible = false
        '
        'SimpleLabelItem7
        '
        Me.SimpleLabelItem7.AllowHotTrack = false
        Me.SimpleLabelItem7.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem7.Name = "SimpleLabelItem6"
        Me.SimpleLabelItem7.Size = New System.Drawing.Size(464, 20)
        Me.SimpleLabelItem7.Text = "Funeral Type"
        Me.SimpleLabelItem7.TextSize = New System.Drawing.Size(91, 16)
        '
        'EmptySpaceItem19
        '
        Me.EmptySpaceItem19.AllowHotTrack = false
        Me.EmptySpaceItem19.Location = New System.Drawing.Point(0, 60)
        Me.EmptySpaceItem19.Name = "EmptySpaceItem17"
        Me.EmptySpaceItem19.Size = New System.Drawing.Size(464, 40)
        Me.EmptySpaceItem19.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem119
        '
        Me.LayoutControlItem119.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem119.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem119.Control = Me.MemoEdit6
        Me.LayoutControlItem119.Location = New System.Drawing.Point(0, 140)
        Me.LayoutControlItem119.Name = "LayoutControlItem119"
        Me.LayoutControlItem119.Size = New System.Drawing.Size(464, 129)
        Me.LayoutControlItem119.Text = "Special Wishes"
        Me.LayoutControlItem119.TextSize = New System.Drawing.Size(91, 16)
        '
        'LayoutControlItem123
        '
        Me.LayoutControlItem123.Control = Me.CheckEdit77
        Me.LayoutControlItem123.Location = New System.Drawing.Point(0, 100)
        Me.LayoutControlItem123.Name = "LayoutControlItem123"
        Me.LayoutControlItem123.Size = New System.Drawing.Size(316, 40)
        Me.LayoutControlItem123.Text = "Do you have funeral plan?"
        Me.LayoutControlItem123.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem123.TextSize = New System.Drawing.Size(180, 20)
        Me.LayoutControlItem123.TextToControlDistance = 5
        '
        'LayoutControlItem124
        '
        Me.LayoutControlItem124.Control = Me.CheckEdit78
        Me.LayoutControlItem124.Location = New System.Drawing.Point(316, 100)
        Me.LayoutControlItem124.Name = "LayoutControlItem124"
        Me.LayoutControlItem124.Size = New System.Drawing.Size(148, 40)
        Me.LayoutControlItem124.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem124.TextVisible = false
        '
        'LayoutControl11
        '
        Me.LayoutControl11.Controls.Add(Me.CheckEdit69)
        Me.LayoutControl11.Controls.Add(Me.CheckEdit70)
        Me.LayoutControl11.Controls.Add(Me.CheckEdit71)
        Me.LayoutControl11.Controls.Add(Me.MemoEdit5)
        Me.LayoutControl11.Controls.Add(Me.CheckEdit75)
        Me.LayoutControl11.Controls.Add(Me.CheckEdit76)
        Me.LayoutControl11.Location = New System.Drawing.Point(22, 48)
        Me.LayoutControl11.Name = "LayoutControl11"
        Me.LayoutControl11.Root = Me.LayoutControlGroup10
        Me.LayoutControl11.Size = New System.Drawing.Size(484, 387)
        Me.LayoutControl11.TabIndex = 0
        Me.LayoutControl11.Text = "LayoutControl11"
        '
        'CheckEdit69
        '
        Me.CheckEdit69.Location = New System.Drawing.Point(12, 32)
        Me.CheckEdit69.Name = "CheckEdit69"
        Me.CheckEdit69.Properties.Caption = "CREMATION"
        Me.CheckEdit69.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit69.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit69.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit69.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit69.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit69.Size = New System.Drawing.Size(158, 36)
        Me.CheckEdit69.StyleController = Me.LayoutControl11
        Me.CheckEdit69.TabIndex = 4
        '
        'CheckEdit70
        '
        Me.CheckEdit70.Location = New System.Drawing.Point(174, 32)
        Me.CheckEdit70.Name = "CheckEdit70"
        Me.CheckEdit70.Properties.Caption = "BURIAL"
        Me.CheckEdit70.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit70.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit70.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit70.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit70.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit70.Size = New System.Drawing.Size(150, 36)
        Me.CheckEdit70.StyleController = Me.LayoutControl11
        Me.CheckEdit70.TabIndex = 5
        '
        'CheckEdit71
        '
        Me.CheckEdit71.Location = New System.Drawing.Point(328, 32)
        Me.CheckEdit71.Name = "CheckEdit71"
        Me.CheckEdit71.Properties.Caption = "NOT REQUIRED"
        Me.CheckEdit71.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit71.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit71.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit71.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit71.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit71.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit71.StyleController = Me.LayoutControl11
        Me.CheckEdit71.TabIndex = 6
        '
        'MemoEdit5
        '
        Me.MemoEdit5.Location = New System.Drawing.Point(117, 153)
        Me.MemoEdit5.Name = "MemoEdit5"
        Me.MemoEdit5.Size = New System.Drawing.Size(355, 124)
        Me.MemoEdit5.StyleController = Me.LayoutControl11
        Me.MemoEdit5.TabIndex = 7
        '
        'CheckEdit75
        '
        Me.CheckEdit75.Location = New System.Drawing.Point(172, 113)
        Me.CheckEdit75.Name = "CheckEdit75"
        Me.CheckEdit75.Properties.Caption = "YES"
        Me.CheckEdit75.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit75.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit75.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit75.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit75.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit75.Size = New System.Drawing.Size(152, 36)
        Me.CheckEdit75.StyleController = Me.LayoutControl11
        Me.CheckEdit75.TabIndex = 8
        '
        'CheckEdit76
        '
        Me.CheckEdit76.Location = New System.Drawing.Point(328, 113)
        Me.CheckEdit76.Name = "CheckEdit76"
        Me.CheckEdit76.Properties.Caption = "NO"
        Me.CheckEdit76.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit76.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit76.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit76.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit76.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit76.Size = New System.Drawing.Size(144, 36)
        Me.CheckEdit76.StyleController = Me.LayoutControl11
        Me.CheckEdit76.TabIndex = 9
        '
        'LayoutControlGroup10
        '
        Me.LayoutControlGroup10.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup10.GroupBordersVisible = false
        Me.LayoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem113, Me.EmptySpaceItem10, Me.LayoutControlItem114, Me.LayoutControlItem115, Me.SimpleLabelItem6, Me.EmptySpaceItem17, Me.LayoutControlItem121, Me.LayoutControlItem120, Me.LayoutControlItem122})
        Me.LayoutControlGroup10.Name = "LayoutControlGroup10"
        Me.LayoutControlGroup10.Size = New System.Drawing.Size(484, 387)
        Me.LayoutControlGroup10.TextVisible = false
        '
        'LayoutControlItem113
        '
        Me.LayoutControlItem113.Control = Me.CheckEdit69
        Me.LayoutControlItem113.Location = New System.Drawing.Point(0, 20)
        Me.LayoutControlItem113.Name = "LayoutControlItem113"
        Me.LayoutControlItem113.Size = New System.Drawing.Size(162, 40)
        Me.LayoutControlItem113.Text = "Funeral Type"
        Me.LayoutControlItem113.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem113.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem113.TextToControlDistance = 0
        Me.LayoutControlItem113.TextVisible = false
        '
        'EmptySpaceItem10
        '
        Me.EmptySpaceItem10.AllowHotTrack = false
        Me.EmptySpaceItem10.Location = New System.Drawing.Point(0, 269)
        Me.EmptySpaceItem10.Name = "EmptySpaceItem10"
        Me.EmptySpaceItem10.Size = New System.Drawing.Size(464, 98)
        Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem114
        '
        Me.LayoutControlItem114.Control = Me.CheckEdit70
        Me.LayoutControlItem114.Location = New System.Drawing.Point(162, 20)
        Me.LayoutControlItem114.Name = "LayoutControlItem114"
        Me.LayoutControlItem114.Size = New System.Drawing.Size(154, 40)
        Me.LayoutControlItem114.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem114.TextVisible = false
        '
        'LayoutControlItem115
        '
        Me.LayoutControlItem115.Control = Me.CheckEdit71
        Me.LayoutControlItem115.Location = New System.Drawing.Point(316, 20)
        Me.LayoutControlItem115.Name = "LayoutControlItem115"
        Me.LayoutControlItem115.Size = New System.Drawing.Size(148, 40)
        Me.LayoutControlItem115.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem115.TextVisible = false
        '
        'SimpleLabelItem6
        '
        Me.SimpleLabelItem6.AllowHotTrack = false
        Me.SimpleLabelItem6.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem6.Name = "SimpleLabelItem6"
        Me.SimpleLabelItem6.Size = New System.Drawing.Size(464, 20)
        Me.SimpleLabelItem6.Text = "Funeral Type"
        Me.SimpleLabelItem6.TextSize = New System.Drawing.Size(75, 16)
        '
        'EmptySpaceItem17
        '
        Me.EmptySpaceItem17.AllowHotTrack = false
        Me.EmptySpaceItem17.Location = New System.Drawing.Point(0, 60)
        Me.EmptySpaceItem17.Name = "EmptySpaceItem17"
        Me.EmptySpaceItem17.Size = New System.Drawing.Size(464, 41)
        Me.EmptySpaceItem17.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem121
        '
        Me.LayoutControlItem121.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem121.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem121.Control = Me.MemoEdit5
        Me.LayoutControlItem121.Location = New System.Drawing.Point(0, 141)
        Me.LayoutControlItem121.Name = "LayoutControlItem121"
        Me.LayoutControlItem121.Size = New System.Drawing.Size(464, 128)
        Me.LayoutControlItem121.Text = "Special Wishes"
        Me.LayoutControlItem121.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem121.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem121.TextToControlDistance = 5
        '
        'LayoutControlItem120
        '
        Me.LayoutControlItem120.Control = Me.CheckEdit75
        Me.LayoutControlItem120.Location = New System.Drawing.Point(0, 101)
        Me.LayoutControlItem120.Name = "LayoutControlItem120"
        Me.LayoutControlItem120.Size = New System.Drawing.Size(316, 40)
        Me.LayoutControlItem120.Text = "Do you have funeral plan?"
        Me.LayoutControlItem120.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem120.TextSize = New System.Drawing.Size(155, 16)
        Me.LayoutControlItem120.TextToControlDistance = 5
        '
        'LayoutControlItem122
        '
        Me.LayoutControlItem122.Control = Me.CheckEdit76
        Me.LayoutControlItem122.Location = New System.Drawing.Point(316, 101)
        Me.LayoutControlItem122.Name = "LayoutControlItem122"
        Me.LayoutControlItem122.Size = New System.Drawing.Size(148, 40)
        Me.LayoutControlItem122.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem122.TextVisible = false
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.SimpleButton10)
        Me.XtraTabPage6.Controls.Add(Me.GridControl5)
        Me.XtraTabPage6.Controls.Add(Me.LayoutControl13)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage6.Text = "Legacies"
        '
        'SimpleButton10
        '
        Me.SimpleButton10.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton10.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton10.Location = New System.Drawing.Point(20, 10)
        Me.SimpleButton10.Name = "SimpleButton10"
        Me.SimpleButton10.Size = New System.Drawing.Size(192, 42)
        Me.SimpleButton10.TabIndex = 2
        Me.SimpleButton10.Text = "Add a Legacy"
        '
        'GridControl5
        '
        Me.GridControl5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GridControl5.Location = New System.Drawing.Point(20, 58)
        Me.GridControl5.MainView = Me.GridView5
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(990, 494)
        Me.GridControl5.TabIndex = 1
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11})
        Me.GridView5.GridControl = Me.GridControl5
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Recipient"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = true
        Me.GridColumn6.VisibleIndex = 0
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Gift From"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = true
        Me.GridColumn7.VisibleIndex = 1
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Item"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = true
        Me.GridColumn8.VisibleIndex = 2
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Relationship to Client 1"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = true
        Me.GridColumn9.VisibleIndex = 3
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Relationship to Client 2"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = true
        Me.GridColumn10.VisibleIndex = 4
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Action"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = true
        Me.GridColumn11.VisibleIndex = 5
        '
        'LayoutControl13
        '
        Me.LayoutControl13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.LayoutControl13.Controls.Add(Me.CheckEdit79)
        Me.LayoutControl13.Controls.Add(Me.CheckEdit80)
        Me.LayoutControl13.Location = New System.Drawing.Point(389, 1)
        Me.LayoutControl13.Name = "LayoutControl13"
        Me.LayoutControl13.Root = Me.LayoutControlGroup12
        Me.LayoutControl13.Size = New System.Drawing.Size(621, 61)
        Me.LayoutControl13.TabIndex = 0
        Me.LayoutControl13.Text = "LayoutControl13"
        '
        'CheckEdit79
        '
        Me.CheckEdit79.Location = New System.Drawing.Point(367, 12)
        Me.CheckEdit79.Name = "CheckEdit79"
        Me.CheckEdit79.Properties.Caption = "YES"
        Me.CheckEdit79.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit79.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit79.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit79.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit79.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit79.Size = New System.Drawing.Size(66, 36)
        Me.CheckEdit79.StyleController = Me.LayoutControl13
        Me.CheckEdit79.TabIndex = 4
        '
        'CheckEdit80
        '
        Me.CheckEdit80.Location = New System.Drawing.Point(437, 12)
        Me.CheckEdit80.Name = "CheckEdit80"
        Me.CheckEdit80.Properties.Caption = "NO"
        Me.CheckEdit80.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit80.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit80.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit80.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit80.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit80.Size = New System.Drawing.Size(172, 36)
        Me.CheckEdit80.StyleController = Me.LayoutControl13
        Me.CheckEdit80.TabIndex = 5
        '
        'LayoutControlGroup12
        '
        Me.LayoutControlGroup12.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup12.GroupBordersVisible = false
        Me.LayoutControlGroup12.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem125, Me.LayoutControlItem126})
        Me.LayoutControlGroup12.Name = "LayoutControlGroup12"
        Me.LayoutControlGroup12.Size = New System.Drawing.Size(621, 61)
        Me.LayoutControlGroup12.TextVisible = false
        '
        'LayoutControlItem125
        '
        Me.LayoutControlItem125.Control = Me.CheckEdit79
        Me.LayoutControlItem125.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem125.Name = "LayoutControlItem125"
        Me.LayoutControlItem125.Size = New System.Drawing.Size(425, 41)
        Me.LayoutControlItem125.Text = "Do you wish to include memorandum wishes"
        Me.LayoutControlItem125.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem125.TextSize = New System.Drawing.Size(350, 20)
        Me.LayoutControlItem125.TextToControlDistance = 5
        '
        'LayoutControlItem126
        '
        Me.LayoutControlItem126.Control = Me.CheckEdit80
        Me.LayoutControlItem126.Location = New System.Drawing.Point(425, 0)
        Me.LayoutControlItem126.Name = "LayoutControlItem126"
        Me.LayoutControlItem126.Size = New System.Drawing.Size(176, 41)
        Me.LayoutControlItem126.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem126.TextVisible = false
        '
        'XtraTabPage7
        '
        Me.XtraTabPage7.AutoScroll = true
        Me.XtraTabPage7.Controls.Add(Me.XtraTabControl2)
        Me.XtraTabPage7.Controls.Add(Me.LabelControl41)
        Me.XtraTabPage7.Controls.Add(Me.LayoutControl14)
        Me.XtraTabPage7.Name = "XtraTabPage7"
        Me.XtraTabPage7.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage7.Text = "Trusts"
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl2.Location = New System.Drawing.Point(30, 111)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.SelectedTabPage = Me.XtraTabPage12
        Me.XtraTabControl2.Size = New System.Drawing.Size(982, 431)
        Me.XtraTabControl2.TabIndex = 3
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage11, Me.XtraTabPage12})
        '
        'XtraTabPage12
        '
        Me.XtraTabPage12.Name = "XtraTabPage12"
        Me.XtraTabPage12.Size = New System.Drawing.Size(980, 395)
        Me.XtraTabPage12.Text = "NRB Details"
        '
        'XtraTabPage11
        '
        Me.XtraTabPage11.AutoScroll = true
        Me.XtraTabPage11.Controls.Add(Me.GridControl6)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl42)
        Me.XtraTabPage11.Controls.Add(Me.TextEdit52)
        Me.XtraTabPage11.Controls.Add(Me.SeparatorControl1)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit84)
        Me.XtraTabPage11.Controls.Add(Me.TextEdit53)
        Me.XtraTabPage11.Controls.Add(Me.SeparatorControl2)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit85)
        Me.XtraTabPage11.Controls.Add(Me.SeparatorControl3)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit86)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit87)
        Me.XtraTabPage11.Controls.Add(Me.SeparatorControl4)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit88)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit89)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl43)
        Me.XtraTabPage11.Controls.Add(Me.SeparatorControl5)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit90)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit91)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl44)
        Me.XtraTabPage11.Controls.Add(Me.SeparatorControl6)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit92)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit93)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl45)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl46)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit94)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit95)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl47)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl48)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit96)
        Me.XtraTabPage11.Controls.Add(Me.CheckEdit97)
        Me.XtraTabPage11.Name = "XtraTabPage11"
        Me.XtraTabPage11.Size = New System.Drawing.Size(985, 395)
        Me.XtraTabPage11.Text = "PPT Details"
        '
        'GridControl6
        '
        Me.GridControl6.Location = New System.Drawing.Point(24, 368)
        Me.GridControl6.MainView = Me.GridView6
        Me.GridControl6.Name = "GridControl6"
        Me.GridControl6.Size = New System.Drawing.Size(932, 206)
        Me.GridControl6.TabIndex = 9
        Me.GridControl6.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView6})
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.GridColumn15, Me.GridColumn16, Me.GridColumn17})
        Me.GridView6.GridControl = Me.GridControl6
        Me.GridView6.Name = "GridView6"
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Name"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = true
        Me.GridColumn12.VisibleIndex = 0
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Lapse/Issue"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = true
        Me.GridColumn13.VisibleIndex = 1
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Percentage"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = true
        Me.GridColumn14.VisibleIndex = 2
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Relationship to Client 1"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = true
        Me.GridColumn15.VisibleIndex = 3
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Relationship to Client 2"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = true
        Me.GridColumn16.VisibleIndex = 4
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "Action"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = true
        Me.GridColumn17.VisibleIndex = 5
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(24, 28)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(93, 16)
        Me.LabelControl42.TabIndex = 0
        Me.LabelControl42.Text = "Residency Type"
        '
        'TextEdit52
        '
        Me.TextEdit52.Location = New System.Drawing.Point(598, 17)
        Me.TextEdit52.Name = "TextEdit52"
        Me.TextEdit52.Size = New System.Drawing.Size(358, 38)
        Me.TextEdit52.TabIndex = 2
        '
        'SeparatorControl1
        '
        Me.SeparatorControl1.Location = New System.Drawing.Point(24, 324)
        Me.SeparatorControl1.Name = "SeparatorControl1"
        Me.SeparatorControl1.Size = New System.Drawing.Size(949, 20)
        Me.SeparatorControl1.TabIndex = 8
        '
        'CheckEdit84
        '
        Me.CheckEdit84.Location = New System.Drawing.Point(827, 190)
        Me.CheckEdit84.Name = "CheckEdit84"
        Me.CheckEdit84.Properties.Caption = "NO"
        Me.CheckEdit84.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit84.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit84.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit84.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit84.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit84.Size = New System.Drawing.Size(129, 36)
        Me.CheckEdit84.TabIndex = 1
        '
        'TextEdit53
        '
        Me.TextEdit53.Location = New System.Drawing.Point(598, 78)
        Me.TextEdit53.Name = "TextEdit53"
        Me.TextEdit53.Size = New System.Drawing.Size(358, 38)
        Me.TextEdit53.TabIndex = 2
        '
        'SeparatorControl2
        '
        Me.SeparatorControl2.Location = New System.Drawing.Point(24, 276)
        Me.SeparatorControl2.Name = "SeparatorControl2"
        Me.SeparatorControl2.Size = New System.Drawing.Size(949, 20)
        Me.SeparatorControl2.TabIndex = 8
        '
        'CheckEdit85
        '
        Me.CheckEdit85.Location = New System.Drawing.Point(827, 142)
        Me.CheckEdit85.Name = "CheckEdit85"
        Me.CheckEdit85.Properties.Caption = "NO"
        Me.CheckEdit85.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit85.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit85.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit85.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit85.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit85.Size = New System.Drawing.Size(129, 36)
        Me.CheckEdit85.TabIndex = 1
        '
        'SeparatorControl3
        '
        Me.SeparatorControl3.Location = New System.Drawing.Point(24, 178)
        Me.SeparatorControl3.Name = "SeparatorControl3"
        Me.SeparatorControl3.Size = New System.Drawing.Size(949, 20)
        Me.SeparatorControl3.TabIndex = 3
        '
        'CheckEdit86
        '
        Me.CheckEdit86.Location = New System.Drawing.Point(518, 288)
        Me.CheckEdit86.Name = "CheckEdit86"
        Me.CheckEdit86.Properties.Caption = "NO"
        Me.CheckEdit86.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit86.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit86.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit86.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit86.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit86.Size = New System.Drawing.Size(99, 36)
        Me.CheckEdit86.TabIndex = 6
        '
        'CheckEdit87
        '
        Me.CheckEdit87.Location = New System.Drawing.Point(334, 72)
        Me.CheckEdit87.Name = "CheckEdit87"
        Me.CheckEdit87.Properties.Caption = "Right to Reside - who?"
        Me.CheckEdit87.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit87.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit87.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit87.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit87.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit87.Size = New System.Drawing.Size(231, 36)
        Me.CheckEdit87.TabIndex = 1
        '
        'SeparatorControl4
        '
        Me.SeparatorControl4.Location = New System.Drawing.Point(24, 226)
        Me.SeparatorControl4.Name = "SeparatorControl4"
        Me.SeparatorControl4.Size = New System.Drawing.Size(949, 20)
        Me.SeparatorControl4.TabIndex = 3
        '
        'CheckEdit88
        '
        Me.CheckEdit88.Location = New System.Drawing.Point(827, 288)
        Me.CheckEdit88.Name = "CheckEdit88"
        Me.CheckEdit88.Properties.Caption = "NO"
        Me.CheckEdit88.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit88.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit88.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit88.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit88.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit88.Size = New System.Drawing.Size(99, 36)
        Me.CheckEdit88.TabIndex = 6
        '
        'CheckEdit89
        '
        Me.CheckEdit89.Location = New System.Drawing.Point(194, 14)
        Me.CheckEdit89.Name = "CheckEdit89"
        Me.CheckEdit89.Properties.Caption = "Life Interest"
        Me.CheckEdit89.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit89.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit89.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit89.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit89.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit89.Size = New System.Drawing.Size(122, 36)
        Me.CheckEdit89.TabIndex = 1
        '
        'LabelControl43
        '
        Me.LabelControl43.Location = New System.Drawing.Point(24, 85)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(79, 16)
        Me.LabelControl43.TabIndex = 0
        Me.LabelControl43.Text = "Residency To"
        '
        'SeparatorControl5
        '
        Me.SeparatorControl5.Location = New System.Drawing.Point(20, 122)
        Me.SeparatorControl5.Name = "SeparatorControl5"
        Me.SeparatorControl5.Size = New System.Drawing.Size(949, 20)
        Me.SeparatorControl5.TabIndex = 3
        '
        'CheckEdit90
        '
        Me.CheckEdit90.Location = New System.Drawing.Point(827, 240)
        Me.CheckEdit90.Name = "CheckEdit90"
        Me.CheckEdit90.Properties.Caption = "NO"
        Me.CheckEdit90.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit90.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit90.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit90.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit90.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit90.Size = New System.Drawing.Size(129, 36)
        Me.CheckEdit90.TabIndex = 6
        '
        'CheckEdit91
        '
        Me.CheckEdit91.Location = New System.Drawing.Point(598, 190)
        Me.CheckEdit91.Name = "CheckEdit91"
        Me.CheckEdit91.Properties.Caption = "YES"
        Me.CheckEdit91.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit91.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit91.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit91.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit91.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit91.Size = New System.Drawing.Size(122, 36)
        Me.CheckEdit91.TabIndex = 1
        '
        'LabelControl44
        '
        Me.LabelControl44.Location = New System.Drawing.Point(640, 302)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(85, 16)
        Me.LabelControl44.TabIndex = 0
        Me.LabelControl44.Text = "Then to issue?"
        '
        'SeparatorControl6
        '
        Me.SeparatorControl6.Location = New System.Drawing.Point(24, 58)
        Me.SeparatorControl6.Name = "SeparatorControl6"
        Me.SeparatorControl6.Size = New System.Drawing.Size(949, 20)
        Me.SeparatorControl6.TabIndex = 4
        '
        'CheckEdit92
        '
        Me.CheckEdit92.Location = New System.Drawing.Point(422, 288)
        Me.CheckEdit92.Name = "CheckEdit92"
        Me.CheckEdit92.Properties.Caption = "YES"
        Me.CheckEdit92.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit92.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit92.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit92.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit92.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit92.Size = New System.Drawing.Size(90, 36)
        Me.CheckEdit92.TabIndex = 7
        '
        'CheckEdit93
        '
        Me.CheckEdit93.Location = New System.Drawing.Point(598, 142)
        Me.CheckEdit93.Name = "CheckEdit93"
        Me.CheckEdit93.Properties.Caption = "YES"
        Me.CheckEdit93.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit93.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit93.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit93.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit93.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit93.Size = New System.Drawing.Size(122, 36)
        Me.CheckEdit93.TabIndex = 1
        '
        'LabelControl45
        '
        Me.LabelControl45.Location = New System.Drawing.Point(24, 156)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(292, 16)
        Me.LabelControl45.TabIndex = 0
        Me.LabelControl45.Text = "Occupation to cease on co-habitation/re-marriage?"
        '
        'LabelControl46
        '
        Me.LabelControl46.Location = New System.Drawing.Point(24, 254)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(236, 16)
        Me.LabelControl46.TabIndex = 5
        Me.LabelControl46.Text = "Mortgage to be discharged from residue?"
        '
        'CheckEdit94
        '
        Me.CheckEdit94.Location = New System.Drawing.Point(731, 288)
        Me.CheckEdit94.Name = "CheckEdit94"
        Me.CheckEdit94.Properties.Caption = "YES"
        Me.CheckEdit94.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit94.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit94.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit94.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit94.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit94.Size = New System.Drawing.Size(90, 36)
        Me.CheckEdit94.TabIndex = 7
        '
        'CheckEdit95
        '
        Me.CheckEdit95.Location = New System.Drawing.Point(194, 72)
        Me.CheckEdit95.Name = "CheckEdit95"
        Me.CheckEdit95.Properties.Caption = "Spouse"
        Me.CheckEdit95.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit95.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit95.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit95.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit95.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit95.Size = New System.Drawing.Size(122, 36)
        Me.CheckEdit95.TabIndex = 1
        '
        'LabelControl47
        '
        Me.LabelControl47.Location = New System.Drawing.Point(24, 204)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(236, 16)
        Me.LabelControl47.TabIndex = 0
        Me.LabelControl47.Text = "Mortgage to be discharged from residue?"
        '
        'LabelControl48
        '
        Me.LabelControl48.Location = New System.Drawing.Point(24, 302)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(335, 16)
        Me.LabelControl48.TabIndex = 5
        Me.LabelControl48.Text = "Beneficial interest passing EQUALLY to bloodline children"
        '
        'CheckEdit96
        '
        Me.CheckEdit96.Location = New System.Drawing.Point(598, 240)
        Me.CheckEdit96.Name = "CheckEdit96"
        Me.CheckEdit96.Properties.Caption = "YES"
        Me.CheckEdit96.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit96.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit96.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit96.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit96.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit96.Size = New System.Drawing.Size(122, 36)
        Me.CheckEdit96.TabIndex = 7
        '
        'CheckEdit97
        '
        Me.CheckEdit97.Location = New System.Drawing.Point(334, 14)
        Me.CheckEdit97.Name = "CheckEdit97"
        Me.CheckEdit97.Properties.Caption = "Right to Reside - for how long?"
        Me.CheckEdit97.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom
        Me.CheckEdit97.Properties.ImageOptions.ImageChecked = CType(resources.GetObject("CheckEdit97.Properties.ImageOptions.ImageChecked"),System.Drawing.Image)
        Me.CheckEdit97.Properties.ImageOptions.ImageUnchecked = CType(resources.GetObject("CheckEdit97.Properties.ImageOptions.ImageUnchecked"),System.Drawing.Image)
        Me.CheckEdit97.Size = New System.Drawing.Size(231, 36)
        Me.CheckEdit97.TabIndex = 1
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl41.Appearance.Options.UseFont = true
        Me.LabelControl41.Location = New System.Drawing.Point(13, 12)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(138, 16)
        Me.LabelControl41.TabIndex = 1
        Me.LabelControl41.Text = "Trusts Before Residue"
        '
        'LayoutControl14
        '
        Me.LayoutControl14.Controls.Add(Me.CheckEdit81)
        Me.LayoutControl14.Controls.Add(Me.CheckEdit82)
        Me.LayoutControl14.Controls.Add(Me.CheckEdit83)
        Me.LayoutControl14.Location = New System.Drawing.Point(13, 34)
        Me.LayoutControl14.Name = "LayoutControl14"
        Me.LayoutControl14.Root = Me.LayoutControlGroup13
        Me.LayoutControl14.Size = New System.Drawing.Size(1012, 71)
        Me.LayoutControl14.TabIndex = 2
        Me.LayoutControl14.Text = "LayoutControl14"
        '
        'CheckEdit81
        '
        Me.CheckEdit81.Location = New System.Drawing.Point(257, 12)
        Me.CheckEdit81.Name = "CheckEdit81"
        Me.CheckEdit81.Properties.Caption = "NONE"
        Me.CheckEdit81.Size = New System.Drawing.Size(217, 44)
        Me.CheckEdit81.StyleController = Me.LayoutControl14
        Me.CheckEdit81.TabIndex = 4
        '
        'CheckEdit82
        '
        Me.CheckEdit82.Location = New System.Drawing.Point(478, 12)
        Me.CheckEdit82.Name = "CheckEdit82"
        Me.CheckEdit82.Properties.Caption = "PROTECTIVE PROPERTY TRUST"
        Me.CheckEdit82.Size = New System.Drawing.Size(278, 44)
        Me.CheckEdit82.StyleController = Me.LayoutControl14
        Me.CheckEdit82.TabIndex = 5
        '
        'CheckEdit83
        '
        Me.CheckEdit83.Location = New System.Drawing.Point(760, 12)
        Me.CheckEdit83.Name = "CheckEdit83"
        Me.CheckEdit83.Properties.Caption = "NIL RATE BAND"
        Me.CheckEdit83.Size = New System.Drawing.Size(240, 44)
        Me.CheckEdit83.StyleController = Me.LayoutControl14
        Me.CheckEdit83.TabIndex = 6
        '
        'LayoutControlGroup13
        '
        Me.LayoutControlGroup13.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup13.GroupBordersVisible = false
        Me.LayoutControlGroup13.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem127, Me.LayoutControlItem128, Me.LayoutControlItem129})
        Me.LayoutControlGroup13.Name = "LayoutControlGroup13"
        Me.LayoutControlGroup13.Size = New System.Drawing.Size(1012, 71)
        Me.LayoutControlGroup13.TextVisible = false
        '
        'LayoutControlItem127
        '
        Me.LayoutControlItem127.Control = Me.CheckEdit81
        Me.LayoutControlItem127.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem127.Name = "LayoutControlItem127"
        Me.LayoutControlItem127.Size = New System.Drawing.Size(466, 51)
        Me.LayoutControlItem127.Text = "Type of trust required"
        Me.LayoutControlItem127.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem127.TextSize = New System.Drawing.Size(240, 20)
        Me.LayoutControlItem127.TextToControlDistance = 5
        '
        'LayoutControlItem128
        '
        Me.LayoutControlItem128.Control = Me.CheckEdit82
        Me.LayoutControlItem128.Location = New System.Drawing.Point(466, 0)
        Me.LayoutControlItem128.Name = "LayoutControlItem128"
        Me.LayoutControlItem128.Size = New System.Drawing.Size(282, 51)
        Me.LayoutControlItem128.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem128.TextVisible = false
        '
        'LayoutControlItem129
        '
        Me.LayoutControlItem129.Control = Me.CheckEdit83
        Me.LayoutControlItem129.Location = New System.Drawing.Point(748, 0)
        Me.LayoutControlItem129.Name = "LayoutControlItem129"
        Me.LayoutControlItem129.Size = New System.Drawing.Size(244, 51)
        Me.LayoutControlItem129.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem129.TextVisible = false
        '
        'XtraTabPage8
        '
        Me.XtraTabPage8.Name = "XtraTabPage8"
        Me.XtraTabPage8.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage8.Text = "Residuary Estate"
        '
        'XtraTabPage9
        '
        Me.XtraTabPage9.Name = "XtraTabPage9"
        Me.XtraTabPage9.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage9.Text = "Potential Claims"
        '
        'XtraTabPage10
        '
        Me.XtraTabPage10.Name = "XtraTabPage10"
        Me.XtraTabPage10.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage10.Text = "Attendance Notes"
        '
        'NavigationPageLPAInstruction
        '
        Me.NavigationPageLPAInstruction.Caption = "   LPA Instruction"
        Me.NavigationPageLPAInstruction.Controls.Add(Me.XtraTabControl3)
        Me.NavigationPageLPAInstruction.ImageOptions.Image = CType(resources.GetObject("NavigationPageLPAInstruction.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageLPAInstruction.Name = "NavigationPageLPAInstruction"
        Me.NavigationPageLPAInstruction.Size = New System.Drawing.Size(1026, 602)
        '
        'XtraTabControl3
        '
        Me.XtraTabControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl3.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl3.Name = "XtraTabControl3"
        Me.XtraTabControl3.SelectedTabPage = Me.XtraTabPage13
        Me.XtraTabControl3.Size = New System.Drawing.Size(1026, 602)
        Me.XtraTabControl3.TabIndex = 0
        Me.XtraTabControl3.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage13, Me.XtraTabPage14, Me.XtraTabPage15, Me.XtraTabPage16, Me.XtraTabPage17, Me.XtraTabPage18, Me.XtraTabPage19, Me.XtraTabPage20, Me.XtraTabPage21})
        '
        'XtraTabPage13
        '
        Me.XtraTabPage13.Controls.Add(Me.GroupControl4)
        Me.XtraTabPage13.Controls.Add(Me.GroupControl3)
        Me.XtraTabPage13.Name = "XtraTabPage13"
        Me.XtraTabPage13.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage13.Text = "Donor Information"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.LayoutControl16)
        Me.GroupControl4.Location = New System.Drawing.Point(569, 28)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(415, 504)
        Me.GroupControl4.TabIndex = 2
        Me.GroupControl4.Text = "Donor 2"
        '
        'LayoutControl16
        '
        Me.LayoutControl16.Controls.Add(Me.TextEdit56)
        Me.LayoutControl16.Controls.Add(Me.DateEdit6)
        Me.LayoutControl16.Controls.Add(Me.MemoEdit8)
        Me.LayoutControl16.Controls.Add(Me.TextEdit57)
        Me.LayoutControl16.Controls.Add(Me.CheckEdit100)
        Me.LayoutControl16.Controls.Add(Me.CheckEdit101)
        Me.LayoutControl16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl16.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl16.Name = "LayoutControl16"
        Me.LayoutControl16.Root = Me.LayoutControlGroup15
        Me.LayoutControl16.Size = New System.Drawing.Size(411, 481)
        Me.LayoutControl16.TabIndex = 0
        Me.LayoutControl16.Text = "LayoutControl16"
        '
        'TextEdit56
        '
        Me.TextEdit56.Location = New System.Drawing.Point(117, 12)
        Me.TextEdit56.Name = "TextEdit56"
        Me.TextEdit56.Size = New System.Drawing.Size(282, 38)
        Me.TextEdit56.StyleController = Me.LayoutControl16
        Me.TextEdit56.TabIndex = 4
        '
        'DateEdit6
        '
        Me.DateEdit6.EditValue = Nothing
        Me.DateEdit6.Location = New System.Drawing.Point(117, 54)
        Me.DateEdit6.Name = "DateEdit6"
        Me.DateEdit6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit6.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit6.Size = New System.Drawing.Size(282, 38)
        Me.DateEdit6.StyleController = Me.LayoutControl16
        Me.DateEdit6.TabIndex = 5
        '
        'MemoEdit8
        '
        Me.MemoEdit8.Location = New System.Drawing.Point(117, 96)
        Me.MemoEdit8.Name = "MemoEdit8"
        Me.MemoEdit8.Size = New System.Drawing.Size(282, 184)
        Me.MemoEdit8.StyleController = Me.LayoutControl16
        Me.MemoEdit8.TabIndex = 6
        '
        'TextEdit57
        '
        Me.TextEdit57.Location = New System.Drawing.Point(119, 284)
        Me.TextEdit57.Name = "TextEdit57"
        Me.TextEdit57.Size = New System.Drawing.Size(280, 38)
        Me.TextEdit57.StyleController = Me.LayoutControl16
        Me.TextEdit57.TabIndex = 7
        '
        'CheckEdit100
        '
        Me.CheckEdit100.Location = New System.Drawing.Point(117, 326)
        Me.CheckEdit100.Name = "CheckEdit100"
        Me.CheckEdit100.Properties.Caption = "Property && Finance"
        Me.CheckEdit100.Size = New System.Drawing.Size(282, 44)
        Me.CheckEdit100.StyleController = Me.LayoutControl16
        Me.CheckEdit100.TabIndex = 8
        '
        'CheckEdit101
        '
        Me.CheckEdit101.Location = New System.Drawing.Point(119, 374)
        Me.CheckEdit101.Name = "CheckEdit101"
        Me.CheckEdit101.Properties.Caption = "Health Welfare"
        Me.CheckEdit101.Size = New System.Drawing.Size(280, 44)
        Me.CheckEdit101.StyleController = Me.LayoutControl16
        Me.CheckEdit101.TabIndex = 9
        '
        'LayoutControlGroup15
        '
        Me.LayoutControlGroup15.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup15.GroupBordersVisible = false
        Me.LayoutControlGroup15.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem136, Me.LayoutControlItem137, Me.LayoutControlItem138, Me.EmptySpaceItem21, Me.LayoutControlItem139, Me.EmptySpaceItem22, Me.LayoutControlItem140, Me.LayoutControlItem141, Me.EmptySpaceItem23})
        Me.LayoutControlGroup15.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup15.Size = New System.Drawing.Size(411, 481)
        Me.LayoutControlGroup15.TextVisible = false
        '
        'LayoutControlItem136
        '
        Me.LayoutControlItem136.Control = Me.TextEdit56
        Me.LayoutControlItem136.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem136.Name = "LayoutControlItem7"
        Me.LayoutControlItem136.Size = New System.Drawing.Size(391, 42)
        Me.LayoutControlItem136.Text = "Name"
        Me.LayoutControlItem136.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem136.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem136.TextToControlDistance = 5
        '
        'LayoutControlItem137
        '
        Me.LayoutControlItem137.Control = Me.DateEdit6
        Me.LayoutControlItem137.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem137.Name = "LayoutControlItem8"
        Me.LayoutControlItem137.Size = New System.Drawing.Size(391, 42)
        Me.LayoutControlItem137.Text = "Date of Birth"
        Me.LayoutControlItem137.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem137.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem137.TextToControlDistance = 5
        '
        'LayoutControlItem138
        '
        Me.LayoutControlItem138.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem138.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem138.Control = Me.MemoEdit8
        Me.LayoutControlItem138.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem138.Name = "LayoutControlItem9"
        Me.LayoutControlItem138.Size = New System.Drawing.Size(391, 188)
        Me.LayoutControlItem138.Text = "Address"
        Me.LayoutControlItem138.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem138.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem138.TextToControlDistance = 5
        '
        'EmptySpaceItem21
        '
        Me.EmptySpaceItem21.AllowHotTrack = false
        Me.EmptySpaceItem21.Location = New System.Drawing.Point(0, 410)
        Me.EmptySpaceItem21.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem21.Size = New System.Drawing.Size(391, 51)
        Me.EmptySpaceItem21.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem139
        '
        Me.LayoutControlItem139.Control = Me.TextEdit57
        Me.LayoutControlItem139.Location = New System.Drawing.Point(107, 272)
        Me.LayoutControlItem139.Name = "LayoutControlItem10"
        Me.LayoutControlItem139.Size = New System.Drawing.Size(284, 42)
        Me.LayoutControlItem139.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem139.TextVisible = false
        '
        'EmptySpaceItem22
        '
        Me.EmptySpaceItem22.AllowHotTrack = false
        Me.EmptySpaceItem22.Location = New System.Drawing.Point(0, 272)
        Me.EmptySpaceItem22.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem22.Size = New System.Drawing.Size(107, 42)
        Me.EmptySpaceItem22.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem140
        '
        Me.LayoutControlItem140.Control = Me.CheckEdit100
        Me.LayoutControlItem140.Location = New System.Drawing.Point(0, 314)
        Me.LayoutControlItem140.Name = "LayoutControlItem11"
        Me.LayoutControlItem140.Size = New System.Drawing.Size(391, 48)
        Me.LayoutControlItem140.Text = "LPAs Required"
        Me.LayoutControlItem140.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem140.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem140.TextToControlDistance = 5
        '
        'LayoutControlItem141
        '
        Me.LayoutControlItem141.Control = Me.CheckEdit101
        Me.LayoutControlItem141.Location = New System.Drawing.Point(107, 362)
        Me.LayoutControlItem141.Name = "LayoutControlItem12"
        Me.LayoutControlItem141.Size = New System.Drawing.Size(284, 48)
        Me.LayoutControlItem141.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem141.TextVisible = false
        '
        'EmptySpaceItem23
        '
        Me.EmptySpaceItem23.AllowHotTrack = false
        Me.EmptySpaceItem23.Location = New System.Drawing.Point(0, 362)
        Me.EmptySpaceItem23.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem23.Size = New System.Drawing.Size(107, 48)
        Me.EmptySpaceItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.EmptySpaceItem23.TextSize = New System.Drawing.Size(0, 0)
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.LayoutControl15)
        Me.GroupControl3.Location = New System.Drawing.Point(36, 28)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(450, 504)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "Donor 1"
        '
        'LayoutControl15
        '
        Me.LayoutControl15.Controls.Add(Me.TextEdit54)
        Me.LayoutControl15.Controls.Add(Me.DateEdit5)
        Me.LayoutControl15.Controls.Add(Me.MemoEdit7)
        Me.LayoutControl15.Controls.Add(Me.TextEdit55)
        Me.LayoutControl15.Controls.Add(Me.CheckEdit98)
        Me.LayoutControl15.Controls.Add(Me.CheckEdit99)
        Me.LayoutControl15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl15.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl15.Name = "LayoutControl15"
        Me.LayoutControl15.Root = Me.LayoutControlGroup14
        Me.LayoutControl15.Size = New System.Drawing.Size(446, 481)
        Me.LayoutControl15.TabIndex = 0
        Me.LayoutControl15.Text = "LayoutControl15"
        '
        'TextEdit54
        '
        Me.TextEdit54.Location = New System.Drawing.Point(117, 12)
        Me.TextEdit54.Name = "TextEdit54"
        Me.TextEdit54.Size = New System.Drawing.Size(317, 38)
        Me.TextEdit54.StyleController = Me.LayoutControl15
        Me.TextEdit54.TabIndex = 4
        '
        'DateEdit5
        '
        Me.DateEdit5.EditValue = Nothing
        Me.DateEdit5.Location = New System.Drawing.Point(117, 54)
        Me.DateEdit5.Name = "DateEdit5"
        Me.DateEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit5.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit5.Size = New System.Drawing.Size(317, 38)
        Me.DateEdit5.StyleController = Me.LayoutControl15
        Me.DateEdit5.TabIndex = 5
        '
        'MemoEdit7
        '
        Me.MemoEdit7.Location = New System.Drawing.Point(117, 96)
        Me.MemoEdit7.Name = "MemoEdit7"
        Me.MemoEdit7.Size = New System.Drawing.Size(317, 166)
        Me.MemoEdit7.StyleController = Me.LayoutControl15
        Me.MemoEdit7.TabIndex = 6
        '
        'TextEdit55
        '
        Me.TextEdit55.Location = New System.Drawing.Point(119, 266)
        Me.TextEdit55.Name = "TextEdit55"
        Me.TextEdit55.Size = New System.Drawing.Size(315, 38)
        Me.TextEdit55.StyleController = Me.LayoutControl15
        Me.TextEdit55.TabIndex = 8
        '
        'CheckEdit98
        '
        Me.CheckEdit98.Location = New System.Drawing.Point(117, 308)
        Me.CheckEdit98.Name = "CheckEdit98"
        Me.CheckEdit98.Properties.Caption = "Property && Finance"
        Me.CheckEdit98.Size = New System.Drawing.Size(317, 44)
        Me.CheckEdit98.StyleController = Me.LayoutControl15
        Me.CheckEdit98.TabIndex = 9
        '
        'CheckEdit99
        '
        Me.CheckEdit99.Location = New System.Drawing.Point(119, 356)
        Me.CheckEdit99.Name = "CheckEdit99"
        Me.CheckEdit99.Properties.Caption = "Health && Welfare"
        Me.CheckEdit99.Size = New System.Drawing.Size(315, 44)
        Me.CheckEdit99.StyleController = Me.LayoutControl15
        Me.CheckEdit99.TabIndex = 10
        '
        'LayoutControlGroup14
        '
        Me.LayoutControlGroup14.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup14.GroupBordersVisible = false
        Me.LayoutControlGroup14.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem130, Me.LayoutControlItem131, Me.LayoutControlItem132, Me.LayoutControlItem133, Me.EmptySpaceItem16, Me.EmptySpaceItem18, Me.LayoutControlItem134, Me.LayoutControlItem135, Me.EmptySpaceItem20})
        Me.LayoutControlGroup14.Name = "Root"
        Me.LayoutControlGroup14.Size = New System.Drawing.Size(446, 481)
        Me.LayoutControlGroup14.TextVisible = false
        '
        'LayoutControlItem130
        '
        Me.LayoutControlItem130.Control = Me.TextEdit54
        Me.LayoutControlItem130.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem130.Name = "LayoutControlItem1"
        Me.LayoutControlItem130.Size = New System.Drawing.Size(426, 42)
        Me.LayoutControlItem130.Text = "Name"
        Me.LayoutControlItem130.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem130.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem130.TextToControlDistance = 5
        '
        'LayoutControlItem131
        '
        Me.LayoutControlItem131.Control = Me.DateEdit5
        Me.LayoutControlItem131.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem131.Name = "LayoutControlItem2"
        Me.LayoutControlItem131.Size = New System.Drawing.Size(426, 42)
        Me.LayoutControlItem131.Text = "Date of Birth"
        Me.LayoutControlItem131.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem131.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem131.TextToControlDistance = 5
        '
        'LayoutControlItem132
        '
        Me.LayoutControlItem132.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem132.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem132.Control = Me.MemoEdit7
        Me.LayoutControlItem132.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem132.Name = "LayoutControlItem3"
        Me.LayoutControlItem132.Size = New System.Drawing.Size(426, 170)
        Me.LayoutControlItem132.Text = "Address"
        Me.LayoutControlItem132.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem132.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem132.TextToControlDistance = 5
        '
        'LayoutControlItem133
        '
        Me.LayoutControlItem133.Control = Me.TextEdit55
        Me.LayoutControlItem133.Location = New System.Drawing.Point(107, 254)
        Me.LayoutControlItem133.Name = "LayoutControlItem5"
        Me.LayoutControlItem133.Size = New System.Drawing.Size(319, 42)
        Me.LayoutControlItem133.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem133.TextVisible = false
        '
        'EmptySpaceItem16
        '
        Me.EmptySpaceItem16.AllowHotTrack = false
        Me.EmptySpaceItem16.Location = New System.Drawing.Point(0, 392)
        Me.EmptySpaceItem16.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem16.Size = New System.Drawing.Size(426, 69)
        Me.EmptySpaceItem16.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem18
        '
        Me.EmptySpaceItem18.AllowHotTrack = false
        Me.EmptySpaceItem18.Location = New System.Drawing.Point(0, 254)
        Me.EmptySpaceItem18.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem18.Size = New System.Drawing.Size(107, 42)
        Me.EmptySpaceItem18.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem134
        '
        Me.LayoutControlItem134.Control = Me.CheckEdit98
        Me.LayoutControlItem134.Location = New System.Drawing.Point(0, 296)
        Me.LayoutControlItem134.Name = "LayoutControlItem4"
        Me.LayoutControlItem134.Size = New System.Drawing.Size(426, 48)
        Me.LayoutControlItem134.Text = "LPAs Required"
        Me.LayoutControlItem134.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem134.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem134.TextToControlDistance = 5
        '
        'LayoutControlItem135
        '
        Me.LayoutControlItem135.Control = Me.CheckEdit99
        Me.LayoutControlItem135.Location = New System.Drawing.Point(107, 344)
        Me.LayoutControlItem135.Name = "LayoutControlItem6"
        Me.LayoutControlItem135.Size = New System.Drawing.Size(319, 48)
        Me.LayoutControlItem135.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem135.TextVisible = false
        '
        'EmptySpaceItem20
        '
        Me.EmptySpaceItem20.AllowHotTrack = false
        Me.EmptySpaceItem20.Location = New System.Drawing.Point(0, 344)
        Me.EmptySpaceItem20.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem20.Size = New System.Drawing.Size(107, 48)
        Me.EmptySpaceItem20.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage14
        '
        Me.XtraTabPage14.Controls.Add(Me.SimpleButton11)
        Me.XtraTabPage14.Controls.Add(Me.MemoEdit9)
        Me.XtraTabPage14.Controls.Add(Me.LabelControl53)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit108)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit109)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit110)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit111)
        Me.XtraTabPage14.Controls.Add(Me.LabelControl52)
        Me.XtraTabPage14.Controls.Add(Me.GridControl7)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit102)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit103)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit104)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit105)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit106)
        Me.XtraTabPage14.Controls.Add(Me.CheckEdit107)
        Me.XtraTabPage14.Controls.Add(Me.LabelControl49)
        Me.XtraTabPage14.Controls.Add(Me.LabelControl50)
        Me.XtraTabPage14.Controls.Add(Me.LabelControl51)
        Me.XtraTabPage14.Name = "XtraTabPage14"
        Me.XtraTabPage14.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage14.Text = "Attoryneys"
        '
        'SimpleButton11
        '
        Me.SimpleButton11.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton11.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton11.Location = New System.Drawing.Point(832, 122)
        Me.SimpleButton11.Name = "SimpleButton11"
        Me.SimpleButton11.Size = New System.Drawing.Size(158, 45)
        Me.SimpleButton11.TabIndex = 19
        Me.SimpleButton11.Text = "Add Attorney"
        '
        'MemoEdit9
        '
        Me.MemoEdit9.Location = New System.Drawing.Point(258, 497)
        Me.MemoEdit9.Name = "MemoEdit9"
        Me.MemoEdit9.Size = New System.Drawing.Size(732, 45)
        Me.MemoEdit9.TabIndex = 18
        '
        'LabelControl53
        '
        Me.LabelControl53.Location = New System.Drawing.Point(40, 498)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(118, 16)
        Me.LabelControl53.TabIndex = 17
        Me.LabelControl53.Text = "Details of how to act"
        '
        'CheckEdit108
        '
        Me.CheckEdit108.Location = New System.Drawing.Point(499, 422)
        Me.CheckEdit108.Name = "CheckEdit108"
        Me.CheckEdit108.Properties.Caption = "JOINTLY FOR SOME, JOINTLY &&  SEVERALLY FOR OTHERS"
        Me.CheckEdit108.Size = New System.Drawing.Size(332, 44)
        Me.CheckEdit108.TabIndex = 13
        '
        'CheckEdit109
        '
        Me.CheckEdit109.Location = New System.Drawing.Point(258, 422)
        Me.CheckEdit109.Name = "CheckEdit109"
        Me.CheckEdit109.Properties.Caption = "JOINTLY && SEVERALLY"
        Me.CheckEdit109.Size = New System.Drawing.Size(195, 44)
        Me.CheckEdit109.TabIndex = 14
        '
        'CheckEdit110
        '
        Me.CheckEdit110.Location = New System.Drawing.Point(499, 372)
        Me.CheckEdit110.Name = "CheckEdit110"
        Me.CheckEdit110.Properties.Caption = "JOINTLY"
        Me.CheckEdit110.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit110.TabIndex = 15
        '
        'CheckEdit111
        '
        Me.CheckEdit111.Location = New System.Drawing.Point(258, 372)
        Me.CheckEdit111.Name = "CheckEdit111"
        Me.CheckEdit111.Properties.Caption = "SOLELY"
        Me.CheckEdit111.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit111.TabIndex = 16
        '
        'LabelControl52
        '
        Me.LabelControl52.Location = New System.Drawing.Point(40, 386)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(193, 16)
        Me.LabelControl52.TabIndex = 12
        Me.LabelControl52.Text = "Is anyone else to be an attorney?"
        '
        'GridControl7
        '
        Me.GridControl7.Location = New System.Drawing.Point(35, 173)
        Me.GridControl7.MainView = Me.GridView7
        Me.GridControl7.Name = "GridControl7"
        Me.GridControl7.Size = New System.Drawing.Size(955, 192)
        Me.GridControl7.TabIndex = 11
        Me.GridControl7.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView7})
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn18, Me.GridColumn19, Me.GridColumn20, Me.GridColumn21})
        Me.GridView7.GridControl = Me.GridControl7
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsView.ShowGroupPanel = false
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "Attorney Name"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = true
        Me.GridColumn18.VisibleIndex = 0
        Me.GridColumn18.Width = 278
        '
        'GridColumn19
        '
        Me.GridColumn19.Caption = "Relationship to Client 1"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = true
        Me.GridColumn19.VisibleIndex = 1
        Me.GridColumn19.Width = 278
        '
        'GridColumn20
        '
        Me.GridColumn20.Caption = "Relationship to Client 2"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = true
        Me.GridColumn20.VisibleIndex = 2
        Me.GridColumn20.Width = 278
        '
        'GridColumn21
        '
        Me.GridColumn21.Caption = "Action"
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Visible = true
        Me.GridColumn21.VisibleIndex = 3
        Me.GridColumn21.Width = 100
        '
        'CheckEdit102
        '
        Me.CheckEdit102.Location = New System.Drawing.Point(447, 108)
        Me.CheckEdit102.Name = "CheckEdit102"
        Me.CheckEdit102.Properties.Caption = "NO"
        Me.CheckEdit102.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit102.TabIndex = 5
        '
        'CheckEdit103
        '
        Me.CheckEdit103.Location = New System.Drawing.Point(447, 58)
        Me.CheckEdit103.Name = "CheckEdit103"
        Me.CheckEdit103.Properties.Caption = "NO"
        Me.CheckEdit103.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit103.TabIndex = 6
        '
        'CheckEdit104
        '
        Me.CheckEdit104.Location = New System.Drawing.Point(447, 8)
        Me.CheckEdit104.Name = "CheckEdit104"
        Me.CheckEdit104.Properties.Caption = "NO"
        Me.CheckEdit104.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit104.TabIndex = 7
        '
        'CheckEdit105
        '
        Me.CheckEdit105.Location = New System.Drawing.Point(326, 108)
        Me.CheckEdit105.Name = "CheckEdit105"
        Me.CheckEdit105.Properties.Caption = "YES"
        Me.CheckEdit105.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit105.TabIndex = 8
        '
        'CheckEdit106
        '
        Me.CheckEdit106.Location = New System.Drawing.Point(326, 58)
        Me.CheckEdit106.Name = "CheckEdit106"
        Me.CheckEdit106.Properties.Caption = "YES"
        Me.CheckEdit106.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit106.TabIndex = 9
        '
        'CheckEdit107
        '
        Me.CheckEdit107.Location = New System.Drawing.Point(326, 8)
        Me.CheckEdit107.Name = "CheckEdit107"
        Me.CheckEdit107.Properties.Caption = "YES"
        Me.CheckEdit107.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit107.TabIndex = 10
        '
        'LabelControl49
        '
        Me.LabelControl49.Location = New System.Drawing.Point(23, 122)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(193, 16)
        Me.LabelControl49.TabIndex = 2
        Me.LabelControl49.Text = "Is anyone else to be an attorney?"
        '
        'LabelControl50
        '
        Me.LabelControl50.Location = New System.Drawing.Point(23, 72)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(184, 16)
        Me.LabelControl50.TabIndex = 3
        Me.LabelControl50.Text = "Are all children to be attorneys?"
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(23, 22)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(198, 16)
        Me.LabelControl51.TabIndex = 4
        Me.LabelControl51.Text = "Is the spouse/partner an attorney?"
        '
        'XtraTabPage15
        '
        Me.XtraTabPage15.Controls.Add(Me.SimpleButton12)
        Me.XtraTabPage15.Controls.Add(Me.GridControl8)
        Me.XtraTabPage15.Controls.Add(Me.CheckEdit112)
        Me.XtraTabPage15.Controls.Add(Me.CheckEdit113)
        Me.XtraTabPage15.Controls.Add(Me.LabelControl54)
        Me.XtraTabPage15.Name = "XtraTabPage15"
        Me.XtraTabPage15.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage15.Text = "Replacement Attorneys"
        '
        'SimpleButton12
        '
        Me.SimpleButton12.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton12.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton12.Location = New System.Drawing.Point(755, 17)
        Me.SimpleButton12.Name = "SimpleButton12"
        Me.SimpleButton12.Size = New System.Drawing.Size(241, 45)
        Me.SimpleButton12.TabIndex = 20
        Me.SimpleButton12.Text = "Add Replacement Attorney"
        '
        'GridControl8
        '
        Me.GridControl8.Location = New System.Drawing.Point(29, 68)
        Me.GridControl8.MainView = Me.GridView8
        Me.GridControl8.Name = "GridControl8"
        Me.GridControl8.Size = New System.Drawing.Size(967, 471)
        Me.GridControl8.TabIndex = 5
        Me.GridControl8.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView8})
        '
        'GridView8
        '
        Me.GridView8.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn22, Me.GridColumn23, Me.GridColumn24, Me.GridColumn25})
        Me.GridView8.GridControl = Me.GridControl8
        Me.GridView8.Name = "GridView8"
        Me.GridView8.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn22
        '
        Me.GridColumn22.Caption = "Replacement Attorney Name"
        Me.GridColumn22.Name = "GridColumn22"
        Me.GridColumn22.Visible = true
        Me.GridColumn22.VisibleIndex = 0
        Me.GridColumn22.Width = 279
        '
        'GridColumn23
        '
        Me.GridColumn23.Caption = "Relationship to Client 1"
        Me.GridColumn23.Name = "GridColumn23"
        Me.GridColumn23.Visible = true
        Me.GridColumn23.VisibleIndex = 1
        Me.GridColumn23.Width = 279
        '
        'GridColumn24
        '
        Me.GridColumn24.Caption = "Relationship To Client 2"
        Me.GridColumn24.Name = "GridColumn24"
        Me.GridColumn24.Visible = true
        Me.GridColumn24.VisibleIndex = 2
        Me.GridColumn24.Width = 279
        '
        'GridColumn25
        '
        Me.GridColumn25.Caption = "Action"
        Me.GridColumn25.Name = "GridColumn25"
        Me.GridColumn25.OptionsColumn.FixedWidth = true
        Me.GridColumn25.Visible = true
        Me.GridColumn25.VisibleIndex = 3
        Me.GridColumn25.Width = 100
        '
        'CheckEdit112
        '
        Me.CheckEdit112.Location = New System.Drawing.Point(414, 18)
        Me.CheckEdit112.Name = "CheckEdit112"
        Me.CheckEdit112.Properties.Caption = "NO"
        Me.CheckEdit112.Size = New System.Drawing.Size(90, 44)
        Me.CheckEdit112.TabIndex = 4
        '
        'CheckEdit113
        '
        Me.CheckEdit113.Location = New System.Drawing.Point(318, 18)
        Me.CheckEdit113.Name = "CheckEdit113"
        Me.CheckEdit113.Properties.Caption = "YES"
        Me.CheckEdit113.Size = New System.Drawing.Size(90, 44)
        Me.CheckEdit113.TabIndex = 3
        '
        'LabelControl54
        '
        Me.LabelControl54.Location = New System.Drawing.Point(40, 32)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(224, 16)
        Me.LabelControl54.TabIndex = 2
        Me.LabelControl54.Text = "Do you require replacement attorneys?"
        '
        'XtraTabPage16
        '
        Me.XtraTabPage16.Controls.Add(Me.LayoutControl17)
        Me.XtraTabPage16.Controls.Add(Me.LabelControl55)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit114)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit115)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit116)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit117)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit118)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit119)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit120)
        Me.XtraTabPage16.Name = "XtraTabPage16"
        Me.XtraTabPage16.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage16.Text = "Certificate Provider"
        '
        'LayoutControl17
        '
        Me.LayoutControl17.Controls.Add(Me.TextEdit58)
        Me.LayoutControl17.Controls.Add(Me.TextEdit59)
        Me.LayoutControl17.Controls.Add(Me.TextEdit60)
        Me.LayoutControl17.Controls.Add(Me.SimpleButtonLookup3)
        Me.LayoutControl17.Controls.Add(Me.MemoEdit10)
        Me.LayoutControl17.Controls.Add(Me.TextEdit61)
        Me.LayoutControl17.Controls.Add(Me.TextEdit62)
        Me.LayoutControl17.Controls.Add(Me.DateEdit7)
        Me.LayoutControl17.Controls.Add(Me.CheckEdit121)
        Me.LayoutControl17.Location = New System.Drawing.Point(130, 114)
        Me.LayoutControl17.Name = "LayoutControl17"
        Me.LayoutControl17.Root = Me.LayoutControlGroup16
        Me.LayoutControl17.Size = New System.Drawing.Size(824, 431)
        Me.LayoutControl17.TabIndex = 11
        Me.LayoutControl17.Text = "LayoutControl17"
        '
        'TextEdit58
        '
        Me.TextEdit58.Location = New System.Drawing.Point(217, 12)
        Me.TextEdit58.Name = "TextEdit58"
        Me.TextEdit58.Size = New System.Drawing.Size(595, 38)
        Me.TextEdit58.StyleController = Me.LayoutControl17
        Me.TextEdit58.TabIndex = 4
        '
        'TextEdit59
        '
        Me.TextEdit59.Location = New System.Drawing.Point(217, 54)
        Me.TextEdit59.Name = "TextEdit59"
        Me.TextEdit59.Size = New System.Drawing.Size(595, 38)
        Me.TextEdit59.StyleController = Me.LayoutControl17
        Me.TextEdit59.TabIndex = 5
        '
        'TextEdit60
        '
        Me.TextEdit60.Location = New System.Drawing.Point(217, 96)
        Me.TextEdit60.Name = "TextEdit60"
        Me.TextEdit60.Properties.AutoHeight = false
        Me.TextEdit60.Size = New System.Drawing.Size(469, 42)
        Me.TextEdit60.StyleController = Me.LayoutControl17
        Me.TextEdit60.TabIndex = 6
        '
        'SimpleButtonLookup3
        '
        Me.SimpleButtonLookup3.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton13.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup3.Location = New System.Drawing.Point(690, 96)
        Me.SimpleButtonLookup3.Name = "SimpleButtonLookup3"
        Me.SimpleButtonLookup3.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButtonLookup3.StyleController = Me.LayoutControl17
        Me.SimpleButtonLookup3.TabIndex = 7
        Me.SimpleButtonLookup3.Text = "Lookup"
        '
        'MemoEdit10
        '
        Me.MemoEdit10.Location = New System.Drawing.Point(217, 142)
        Me.MemoEdit10.Name = "MemoEdit10"
        Me.MemoEdit10.Size = New System.Drawing.Size(595, 125)
        Me.MemoEdit10.StyleController = Me.LayoutControl17
        Me.MemoEdit10.TabIndex = 8
        '
        'TextEdit61
        '
        Me.TextEdit61.Location = New System.Drawing.Point(217, 271)
        Me.TextEdit61.Name = "TextEdit61"
        Me.TextEdit61.Size = New System.Drawing.Size(231, 38)
        Me.TextEdit61.StyleController = Me.LayoutControl17
        Me.TextEdit61.TabIndex = 9
        '
        'TextEdit62
        '
        Me.TextEdit62.Location = New System.Drawing.Point(217, 313)
        Me.TextEdit62.Name = "TextEdit62"
        Me.TextEdit62.Size = New System.Drawing.Size(231, 38)
        Me.TextEdit62.StyleController = Me.LayoutControl17
        Me.TextEdit62.TabIndex = 10
        '
        'DateEdit7
        '
        Me.DateEdit7.EditValue = Nothing
        Me.DateEdit7.Location = New System.Drawing.Point(462, 290)
        Me.DateEdit7.Name = "DateEdit7"
        Me.DateEdit7.Properties.AutoHeight = false
        Me.DateEdit7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit7.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit7.Size = New System.Drawing.Size(350, 43)
        Me.DateEdit7.StyleController = Me.LayoutControl17
        Me.DateEdit7.TabIndex = 11
        '
        'CheckEdit121
        '
        Me.CheckEdit121.Location = New System.Drawing.Point(220, 355)
        Me.CheckEdit121.Name = "CheckEdit121"
        Me.CheckEdit121.Properties.Caption = "I confirm that the chosen person is allowed to be a certificate provide"
        Me.CheckEdit121.Size = New System.Drawing.Size(592, 44)
        Me.CheckEdit121.StyleController = Me.LayoutControl17
        Me.CheckEdit121.TabIndex = 12
        '
        'LayoutControlGroup16
        '
        Me.LayoutControlGroup16.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup16.GroupBordersVisible = false
        Me.LayoutControlGroup16.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem142, Me.LayoutControlItem143, Me.LayoutControlItem144, Me.LayoutControlItem145, Me.LayoutControlItem146, Me.LayoutControlItem147, Me.LayoutControlItem148, Me.LayoutControlItem149, Me.EmptySpaceItem24, Me.EmptySpaceItem25, Me.EmptySpaceItem26, Me.LayoutControlItem150, Me.SimpleLabelItem8})
        Me.LayoutControlGroup16.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup16.Size = New System.Drawing.Size(824, 431)
        Me.LayoutControlGroup16.TextVisible = false
        '
        'LayoutControlItem142
        '
        Me.LayoutControlItem142.Control = Me.TextEdit58
        Me.LayoutControlItem142.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem142.Name = "LayoutControlItem13"
        Me.LayoutControlItem142.Size = New System.Drawing.Size(804, 42)
        Me.LayoutControlItem142.Text = "First Name(s)"
        Me.LayoutControlItem142.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem142.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem142.TextToControlDistance = 5
        '
        'LayoutControlItem143
        '
        Me.LayoutControlItem143.Control = Me.TextEdit59
        Me.LayoutControlItem143.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem143.Name = "LayoutControlItem14"
        Me.LayoutControlItem143.Size = New System.Drawing.Size(804, 42)
        Me.LayoutControlItem143.Text = "Surname"
        Me.LayoutControlItem143.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem143.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem143.TextToControlDistance = 5
        '
        'LayoutControlItem144
        '
        Me.LayoutControlItem144.Control = Me.TextEdit60
        Me.LayoutControlItem144.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem144.Name = "LayoutControlItem15"
        Me.LayoutControlItem144.Size = New System.Drawing.Size(678, 46)
        Me.LayoutControlItem144.Text = "Postcode"
        Me.LayoutControlItem144.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem144.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem144.TextToControlDistance = 5
        '
        'LayoutControlItem145
        '
        Me.LayoutControlItem145.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem145.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem145.Control = Me.MemoEdit10
        Me.LayoutControlItem145.Location = New System.Drawing.Point(0, 130)
        Me.LayoutControlItem145.Name = "LayoutControlItem17"
        Me.LayoutControlItem145.Size = New System.Drawing.Size(804, 129)
        Me.LayoutControlItem145.Text = "Address"
        Me.LayoutControlItem145.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem145.TextSize = New System.Drawing.Size(200, 16)
        Me.LayoutControlItem145.TextToControlDistance = 5
        '
        'LayoutControlItem146
        '
        Me.LayoutControlItem146.Control = Me.SimpleButtonLookup3
        Me.LayoutControlItem146.Location = New System.Drawing.Point(678, 84)
        Me.LayoutControlItem146.Name = "LayoutControlItem16"
        Me.LayoutControlItem146.Size = New System.Drawing.Size(126, 46)
        Me.LayoutControlItem146.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem146.TextVisible = false
        '
        'LayoutControlItem147
        '
        Me.LayoutControlItem147.Control = Me.TextEdit61
        Me.LayoutControlItem147.Location = New System.Drawing.Point(0, 259)
        Me.LayoutControlItem147.Name = "LayoutControlItem18"
        Me.LayoutControlItem147.Size = New System.Drawing.Size(440, 42)
        Me.LayoutControlItem147.Text = "Relationship To Client 1"
        Me.LayoutControlItem147.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem147.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem147.TextToControlDistance = 5
        '
        'LayoutControlItem148
        '
        Me.LayoutControlItem148.Control = Me.TextEdit62
        Me.LayoutControlItem148.Location = New System.Drawing.Point(0, 301)
        Me.LayoutControlItem148.Name = "LayoutControlItem19"
        Me.LayoutControlItem148.Size = New System.Drawing.Size(440, 42)
        Me.LayoutControlItem148.Text = "Relationship to Client 2"
        Me.LayoutControlItem148.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem148.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem148.TextToControlDistance = 5
        '
        'LayoutControlItem149
        '
        Me.LayoutControlItem149.Control = Me.DateEdit7
        Me.LayoutControlItem149.Location = New System.Drawing.Point(450, 259)
        Me.LayoutControlItem149.Name = "LayoutControlItem20"
        Me.LayoutControlItem149.Size = New System.Drawing.Size(354, 66)
        Me.LayoutControlItem149.Text = "Date of Birth"
        Me.LayoutControlItem149.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem149.TextSize = New System.Drawing.Size(350, 16)
        '
        'EmptySpaceItem24
        '
        Me.EmptySpaceItem24.AllowHotTrack = false
        Me.EmptySpaceItem24.Location = New System.Drawing.Point(440, 259)
        Me.EmptySpaceItem24.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem24.Size = New System.Drawing.Size(10, 84)
        Me.EmptySpaceItem24.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem25
        '
        Me.EmptySpaceItem25.AllowHotTrack = false
        Me.EmptySpaceItem25.Location = New System.Drawing.Point(450, 325)
        Me.EmptySpaceItem25.Name = "EmptySpaceItem9"
        Me.EmptySpaceItem25.Size = New System.Drawing.Size(354, 18)
        Me.EmptySpaceItem25.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem26
        '
        Me.EmptySpaceItem26.AllowHotTrack = false
        Me.EmptySpaceItem26.Location = New System.Drawing.Point(0, 343)
        Me.EmptySpaceItem26.Name = "EmptySpaceItem10"
        Me.EmptySpaceItem26.Size = New System.Drawing.Size(208, 68)
        Me.EmptySpaceItem26.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem150
        '
        Me.LayoutControlItem150.Control = Me.CheckEdit121
        Me.LayoutControlItem150.Location = New System.Drawing.Point(208, 343)
        Me.LayoutControlItem150.Name = "LayoutControlItem21"
        Me.LayoutControlItem150.Size = New System.Drawing.Size(596, 48)
        Me.LayoutControlItem150.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem150.TextVisible = false
        '
        'SimpleLabelItem8
        '
        Me.SimpleLabelItem8.AllowHotTrack = false
        Me.SimpleLabelItem8.Location = New System.Drawing.Point(208, 391)
        Me.SimpleLabelItem8.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem8.Size = New System.Drawing.Size(596, 20)
        Me.SimpleLabelItem8.Text = "(If in doubt, please check the list of those that are prohibited)"
        Me.SimpleLabelItem8.TextSize = New System.Drawing.Size(350, 16)
        '
        'LabelControl55
        '
        Me.LabelControl55.Location = New System.Drawing.Point(60, 78)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(24, 16)
        Me.LabelControl55.TabIndex = 10
        Me.LabelControl55.Text = "Title"
        '
        'CheckEdit114
        '
        Me.CheckEdit114.Location = New System.Drawing.Point(365, 14)
        Me.CheckEdit114.Name = "CheckEdit114"
        Me.CheckEdit114.Properties.Caption = "OTHER (MUST HAVE KNOWN THEM FOR ATLEAST 2 YEARS)"
        Me.CheckEdit114.Size = New System.Drawing.Size(462, 44)
        Me.CheckEdit114.TabIndex = 3
        '
        'CheckEdit115
        '
        Me.CheckEdit115.Location = New System.Drawing.Point(600, 64)
        Me.CheckEdit115.Name = "CheckEdit115"
        Me.CheckEdit115.Properties.Caption = "DOCTOR"
        Me.CheckEdit115.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit115.TabIndex = 4
        '
        'CheckEdit116
        '
        Me.CheckEdit116.Location = New System.Drawing.Point(491, 64)
        Me.CheckEdit116.Name = "CheckEdit116"
        Me.CheckEdit116.Properties.Caption = "MS"
        Me.CheckEdit116.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit116.TabIndex = 5
        '
        'CheckEdit117
        '
        Me.CheckEdit117.Location = New System.Drawing.Point(256, 64)
        Me.CheckEdit117.Name = "CheckEdit117"
        Me.CheckEdit117.Properties.Caption = "MRS"
        Me.CheckEdit117.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit117.TabIndex = 6
        '
        'CheckEdit118
        '
        Me.CheckEdit118.Location = New System.Drawing.Point(365, 64)
        Me.CheckEdit118.Name = "CheckEdit118"
        Me.CheckEdit118.Properties.Caption = "MISS"
        Me.CheckEdit118.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit118.TabIndex = 7
        '
        'CheckEdit119
        '
        Me.CheckEdit119.Location = New System.Drawing.Point(130, 64)
        Me.CheckEdit119.Name = "CheckEdit119"
        Me.CheckEdit119.Properties.Caption = "MR"
        Me.CheckEdit119.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit119.TabIndex = 8
        '
        'CheckEdit120
        '
        Me.CheckEdit120.Location = New System.Drawing.Point(130, 14)
        Me.CheckEdit120.Name = "CheckEdit120"
        Me.CheckEdit120.Properties.Caption = "PROFESSIONAL"
        Me.CheckEdit120.Size = New System.Drawing.Size(205, 44)
        Me.CheckEdit120.TabIndex = 9
        '
        'XtraTabPage17
        '
        Me.XtraTabPage17.Controls.Add(Me.MemoEdit11)
        Me.XtraTabPage17.Name = "XtraTabPage17"
        Me.XtraTabPage17.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage17.Text = "Instructions"
        '
        'MemoEdit11
        '
        Me.MemoEdit11.Location = New System.Drawing.Point(33, 22)
        Me.MemoEdit11.Name = "MemoEdit11"
        Me.MemoEdit11.Properties.NullText = "Add your intructions here"
        Me.MemoEdit11.Size = New System.Drawing.Size(959, 520)
        Me.MemoEdit11.TabIndex = 1
        '
        'XtraTabPage18
        '
        Me.XtraTabPage18.Controls.Add(Me.MemoEdit12)
        Me.XtraTabPage18.Name = "XtraTabPage18"
        Me.XtraTabPage18.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage18.Text = "Preferences"
        '
        'MemoEdit12
        '
        Me.MemoEdit12.EditValue = "Add your desired preferences"
        Me.MemoEdit12.Location = New System.Drawing.Point(41, 22)
        Me.MemoEdit12.Name = "MemoEdit12"
        Me.MemoEdit12.Properties.NullText = "Add your intructions here"
        Me.MemoEdit12.Size = New System.Drawing.Size(943, 520)
        Me.MemoEdit12.TabIndex = 2
        '
        'XtraTabPage19
        '
        Me.XtraTabPage19.Controls.Add(Me.SimpleButton14)
        Me.XtraTabPage19.Controls.Add(Me.GridControl9)
        Me.XtraTabPage19.Name = "XtraTabPage19"
        Me.XtraTabPage19.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage19.Text = "Persons to be Told"
        '
        'SimpleButton14
        '
        Me.SimpleButton14.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton14.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton14.Location = New System.Drawing.Point(26, 18)
        Me.SimpleButton14.Name = "SimpleButton14"
        Me.SimpleButton14.Size = New System.Drawing.Size(223, 45)
        Me.SimpleButton14.TabIndex = 20
        Me.SimpleButton14.Text = "Add Person to be Told"
        '
        'GridControl9
        '
        Me.GridControl9.Location = New System.Drawing.Point(26, 69)
        Me.GridControl9.MainView = Me.GridView9
        Me.GridControl9.Name = "GridControl9"
        Me.GridControl9.Size = New System.Drawing.Size(972, 459)
        Me.GridControl9.TabIndex = 1
        Me.GridControl9.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView9})
        '
        'GridView9
        '
        Me.GridView9.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn26, Me.GridColumn27, Me.GridColumn28, Me.GridColumn29})
        Me.GridView9.GridControl = Me.GridControl9
        Me.GridView9.Name = "GridView9"
        Me.GridView9.OptionsView.ShowGroupPanel = false
        Me.GridView9.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn26
        '
        Me.GridColumn26.Caption = "Name"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = true
        Me.GridColumn26.VisibleIndex = 0
        Me.GridColumn26.Width = 280
        '
        'GridColumn27
        '
        Me.GridColumn27.Caption = "Relationship to Client 1"
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.Visible = true
        Me.GridColumn27.VisibleIndex = 1
        Me.GridColumn27.Width = 280
        '
        'GridColumn28
        '
        Me.GridColumn28.Caption = "Relationship to Client 2"
        Me.GridColumn28.Name = "GridColumn28"
        Me.GridColumn28.Visible = true
        Me.GridColumn28.VisibleIndex = 2
        Me.GridColumn28.Width = 280
        '
        'GridColumn29
        '
        Me.GridColumn29.Caption = "Action"
        Me.GridColumn29.Name = "GridColumn29"
        Me.GridColumn29.Visible = true
        Me.GridColumn29.VisibleIndex = 3
        Me.GridColumn29.Width = 100
        '
        'XtraTabPage20
        '
        Me.XtraTabPage20.Controls.Add(Me.LayoutControl18)
        Me.XtraTabPage20.Name = "XtraTabPage20"
        Me.XtraTabPage20.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage20.Text = "Page Registration"
        '
        'LayoutControl18
        '
        Me.LayoutControl18.Controls.Add(Me.CheckEdit122)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit123)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit124)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit125)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit126)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit127)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit128)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit129)
        Me.LayoutControl18.Controls.Add(Me.CheckEdit130)
        Me.LayoutControl18.Controls.Add(Me.TextEdit63)
        Me.LayoutControl18.Controls.Add(Me.SimpleButtonLookup4)
        Me.LayoutControl18.Controls.Add(Me.MemoEdit13)
        Me.LayoutControl18.Controls.Add(Me.TextEdit64)
        Me.LayoutControl18.Controls.Add(Me.TextEdit65)
        Me.LayoutControl18.Controls.Add(Me.DateEdit8)
        Me.LayoutControl18.Location = New System.Drawing.Point(27, 1)
        Me.LayoutControl18.Name = "LayoutControl18"
        Me.LayoutControl18.Root = Me.LayoutControlGroup17
        Me.LayoutControl18.Size = New System.Drawing.Size(971, 563)
        Me.LayoutControl18.TabIndex = 1
        Me.LayoutControl18.Text = "LayoutControl18"
        '
        'CheckEdit122
        '
        Me.CheckEdit122.Location = New System.Drawing.Point(317, 12)
        Me.CheckEdit122.Name = "CheckEdit122"
        Me.CheckEdit122.Properties.Caption = "DONOR"
        Me.CheckEdit122.Size = New System.Drawing.Size(94, 44)
        Me.CheckEdit122.StyleController = Me.LayoutControl18
        Me.CheckEdit122.TabIndex = 4
        '
        'CheckEdit123
        '
        Me.CheckEdit123.Location = New System.Drawing.Point(317, 60)
        Me.CheckEdit123.Name = "CheckEdit123"
        Me.CheckEdit123.Properties.Caption = "DONOR"
        Me.CheckEdit123.Size = New System.Drawing.Size(94, 44)
        Me.CheckEdit123.StyleController = Me.LayoutControl18
        Me.CheckEdit123.TabIndex = 5
        '
        'CheckEdit124
        '
        Me.CheckEdit124.Location = New System.Drawing.Point(415, 12)
        Me.CheckEdit124.Name = "CheckEdit124"
        Me.CheckEdit124.Properties.Caption = "ATTORNEY"
        Me.CheckEdit124.Size = New System.Drawing.Size(544, 44)
        Me.CheckEdit124.StyleController = Me.LayoutControl18
        Me.CheckEdit124.TabIndex = 6
        '
        'CheckEdit125
        '
        Me.CheckEdit125.Location = New System.Drawing.Point(415, 60)
        Me.CheckEdit125.Name = "CheckEdit125"
        Me.CheckEdit125.Properties.Caption = "ATTORNEY"
        Me.CheckEdit125.Size = New System.Drawing.Size(116, 44)
        Me.CheckEdit125.StyleController = Me.LayoutControl18
        Me.CheckEdit125.TabIndex = 7
        '
        'CheckEdit126
        '
        Me.CheckEdit126.Location = New System.Drawing.Point(535, 60)
        Me.CheckEdit126.Name = "CheckEdit126"
        Me.CheckEdit126.Properties.Caption = "PROFESSIONAL (Enter details below)"
        Me.CheckEdit126.Size = New System.Drawing.Size(424, 44)
        Me.CheckEdit126.StyleController = Me.LayoutControl18
        Me.CheckEdit126.TabIndex = 8
        '
        'CheckEdit127
        '
        Me.CheckEdit127.Location = New System.Drawing.Point(317, 108)
        Me.CheckEdit127.Name = "CheckEdit127"
        Me.CheckEdit127.Properties.Caption = "MR"
        Me.CheckEdit127.Size = New System.Drawing.Size(94, 44)
        Me.CheckEdit127.StyleController = Me.LayoutControl18
        Me.CheckEdit127.TabIndex = 9
        '
        'CheckEdit128
        '
        Me.CheckEdit128.Location = New System.Drawing.Point(415, 108)
        Me.CheckEdit128.Name = "CheckEdit128"
        Me.CheckEdit128.Properties.Caption = "MRS"
        Me.CheckEdit128.Size = New System.Drawing.Size(116, 44)
        Me.CheckEdit128.StyleController = Me.LayoutControl18
        Me.CheckEdit128.TabIndex = 10
        '
        'CheckEdit129
        '
        Me.CheckEdit129.Location = New System.Drawing.Point(535, 108)
        Me.CheckEdit129.Name = "CheckEdit129"
        Me.CheckEdit129.Properties.Caption = "MISS"
        Me.CheckEdit129.Size = New System.Drawing.Size(79, 44)
        Me.CheckEdit129.StyleController = Me.LayoutControl18
        Me.CheckEdit129.TabIndex = 11
        '
        'CheckEdit130
        '
        Me.CheckEdit130.Location = New System.Drawing.Point(618, 108)
        Me.CheckEdit130.Name = "CheckEdit130"
        Me.CheckEdit130.Properties.Caption = "MS"
        Me.CheckEdit130.Size = New System.Drawing.Size(341, 44)
        Me.CheckEdit130.StyleController = Me.LayoutControl18
        Me.CheckEdit130.TabIndex = 13
        '
        'TextEdit63
        '
        Me.TextEdit63.Location = New System.Drawing.Point(317, 282)
        Me.TextEdit63.Name = "TextEdit63"
        Me.TextEdit63.Properties.AutoHeight = false
        Me.TextEdit63.Size = New System.Drawing.Size(375, 42)
        Me.TextEdit63.StyleController = Me.LayoutControl18
        Me.TextEdit63.TabIndex = 14
        '
        'SimpleButtonLookup4
        '
        Me.SimpleButtonLookup4.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton15.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup4.Location = New System.Drawing.Point(696, 282)
        Me.SimpleButtonLookup4.Name = "SimpleButtonLookup4"
        Me.SimpleButtonLookup4.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButtonLookup4.StyleController = Me.LayoutControl18
        Me.SimpleButtonLookup4.TabIndex = 15
        Me.SimpleButtonLookup4.Text = "Lookup"
        '
        'MemoEdit13
        '
        Me.MemoEdit13.Location = New System.Drawing.Point(317, 328)
        Me.MemoEdit13.Name = "MemoEdit13"
        Me.MemoEdit13.Size = New System.Drawing.Size(501, 127)
        Me.MemoEdit13.StyleController = Me.LayoutControl18
        Me.MemoEdit13.TabIndex = 16
        '
        'TextEdit64
        '
        Me.TextEdit64.Location = New System.Drawing.Point(317, 156)
        Me.TextEdit64.Name = "TextEdit64"
        Me.TextEdit64.Size = New System.Drawing.Size(375, 38)
        Me.TextEdit64.StyleController = Me.LayoutControl18
        Me.TextEdit64.TabIndex = 17
        '
        'TextEdit65
        '
        Me.TextEdit65.Location = New System.Drawing.Point(317, 198)
        Me.TextEdit65.Name = "TextEdit65"
        Me.TextEdit65.Size = New System.Drawing.Size(375, 38)
        Me.TextEdit65.StyleController = Me.LayoutControl18
        Me.TextEdit65.TabIndex = 18
        '
        'DateEdit8
        '
        Me.DateEdit8.EditValue = Nothing
        Me.DateEdit8.Location = New System.Drawing.Point(317, 240)
        Me.DateEdit8.Name = "DateEdit8"
        Me.DateEdit8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit8.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit8.Size = New System.Drawing.Size(375, 38)
        Me.DateEdit8.StyleController = Me.LayoutControl18
        Me.DateEdit8.TabIndex = 19
        '
        'LayoutControlGroup17
        '
        Me.LayoutControlGroup17.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup17.GroupBordersVisible = false
        Me.LayoutControlGroup17.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem151, Me.LayoutControlItem152, Me.LayoutControlItem153, Me.LayoutControlItem154, Me.LayoutControlItem155, Me.EmptySpaceItem27, Me.LayoutControlItem156, Me.LayoutControlItem157, Me.LayoutControlItem158, Me.LayoutControlItem159, Me.LayoutControlItem160, Me.LayoutControlItem161, Me.EmptySpaceItem28, Me.LayoutControlItem162, Me.LayoutControlItem163, Me.LayoutControlItem164, Me.EmptySpaceItem29, Me.EmptySpaceItem30, Me.EmptySpaceItem31, Me.LayoutControlItem165, Me.EmptySpaceItem32})
        Me.LayoutControlGroup17.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup17.Size = New System.Drawing.Size(971, 563)
        Me.LayoutControlGroup17.TextVisible = false
        '
        'LayoutControlItem151
        '
        Me.LayoutControlItem151.Control = Me.CheckEdit122
        Me.LayoutControlItem151.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem151.Name = "LayoutControlItem22"
        Me.LayoutControlItem151.Size = New System.Drawing.Size(403, 48)
        Me.LayoutControlItem151.Text = "Who is going to register LPA?"
        Me.LayoutControlItem151.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem151.TextSize = New System.Drawing.Size(300, 16)
        Me.LayoutControlItem151.TextToControlDistance = 5
        '
        'LayoutControlItem152
        '
        Me.LayoutControlItem152.Control = Me.CheckEdit123
        Me.LayoutControlItem152.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem152.Name = "LayoutControlItem23"
        Me.LayoutControlItem152.Size = New System.Drawing.Size(403, 48)
        Me.LayoutControlItem152.Text = "Who is the correspondent"
        Me.LayoutControlItem152.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem152.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem152.TextToControlDistance = 5
        '
        'LayoutControlItem153
        '
        Me.LayoutControlItem153.Control = Me.CheckEdit124
        Me.LayoutControlItem153.Location = New System.Drawing.Point(403, 0)
        Me.LayoutControlItem153.Name = "LayoutControlItem24"
        Me.LayoutControlItem153.Size = New System.Drawing.Size(548, 48)
        Me.LayoutControlItem153.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem153.TextVisible = false
        '
        'LayoutControlItem154
        '
        Me.LayoutControlItem154.Control = Me.CheckEdit125
        Me.LayoutControlItem154.Location = New System.Drawing.Point(403, 48)
        Me.LayoutControlItem154.Name = "LayoutControlItem25"
        Me.LayoutControlItem154.Size = New System.Drawing.Size(120, 48)
        Me.LayoutControlItem154.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem154.TextVisible = false
        '
        'LayoutControlItem155
        '
        Me.LayoutControlItem155.Control = Me.CheckEdit126
        Me.LayoutControlItem155.Location = New System.Drawing.Point(523, 48)
        Me.LayoutControlItem155.Name = "LayoutControlItem26"
        Me.LayoutControlItem155.Size = New System.Drawing.Size(428, 48)
        Me.LayoutControlItem155.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem155.TextVisible = false
        '
        'EmptySpaceItem27
        '
        Me.EmptySpaceItem27.AllowHotTrack = false
        Me.EmptySpaceItem27.Location = New System.Drawing.Point(0, 447)
        Me.EmptySpaceItem27.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem27.Size = New System.Drawing.Size(951, 96)
        Me.EmptySpaceItem27.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem156
        '
        Me.LayoutControlItem156.Control = Me.CheckEdit127
        Me.LayoutControlItem156.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem156.Name = "LayoutControlItem27"
        Me.LayoutControlItem156.Size = New System.Drawing.Size(403, 48)
        Me.LayoutControlItem156.Text = "Title"
        Me.LayoutControlItem156.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem156.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem156.TextToControlDistance = 5
        '
        'LayoutControlItem157
        '
        Me.LayoutControlItem157.Control = Me.CheckEdit128
        Me.LayoutControlItem157.Location = New System.Drawing.Point(403, 96)
        Me.LayoutControlItem157.Name = "LayoutControlItem28"
        Me.LayoutControlItem157.Size = New System.Drawing.Size(120, 48)
        Me.LayoutControlItem157.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem157.TextVisible = false
        '
        'LayoutControlItem158
        '
        Me.LayoutControlItem158.Control = Me.CheckEdit129
        Me.LayoutControlItem158.Location = New System.Drawing.Point(523, 96)
        Me.LayoutControlItem158.Name = "LayoutControlItem29"
        Me.LayoutControlItem158.Size = New System.Drawing.Size(83, 48)
        Me.LayoutControlItem158.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem158.TextVisible = false
        '
        'LayoutControlItem159
        '
        Me.LayoutControlItem159.Control = Me.CheckEdit130
        Me.LayoutControlItem159.Location = New System.Drawing.Point(606, 96)
        Me.LayoutControlItem159.Name = "LayoutControlItem31"
        Me.LayoutControlItem159.Size = New System.Drawing.Size(345, 48)
        Me.LayoutControlItem159.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem159.TextVisible = false
        '
        'LayoutControlItem160
        '
        Me.LayoutControlItem160.Control = Me.TextEdit63
        Me.LayoutControlItem160.Location = New System.Drawing.Point(0, 270)
        Me.LayoutControlItem160.Name = "LayoutControlItem32"
        Me.LayoutControlItem160.Size = New System.Drawing.Size(684, 46)
        Me.LayoutControlItem160.Text = "Postcode"
        Me.LayoutControlItem160.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem160.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem160.TextToControlDistance = 5
        '
        'LayoutControlItem161
        '
        Me.LayoutControlItem161.Control = Me.SimpleButtonLookup4
        Me.LayoutControlItem161.Location = New System.Drawing.Point(684, 270)
        Me.LayoutControlItem161.Name = "LayoutControlItem30"
        Me.LayoutControlItem161.Size = New System.Drawing.Size(126, 46)
        Me.LayoutControlItem161.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem161.TextVisible = false
        '
        'EmptySpaceItem28
        '
        Me.EmptySpaceItem28.AllowHotTrack = false
        Me.EmptySpaceItem28.Location = New System.Drawing.Point(810, 270)
        Me.EmptySpaceItem28.Name = "EmptySpaceItem11"
        Me.EmptySpaceItem28.Size = New System.Drawing.Size(141, 46)
        Me.EmptySpaceItem28.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem162
        '
        Me.LayoutControlItem162.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem162.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem162.Control = Me.MemoEdit13
        Me.LayoutControlItem162.Location = New System.Drawing.Point(0, 316)
        Me.LayoutControlItem162.Name = "LayoutControlItem33"
        Me.LayoutControlItem162.Size = New System.Drawing.Size(810, 131)
        Me.LayoutControlItem162.Text = "Address"
        Me.LayoutControlItem162.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem162.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem162.TextToControlDistance = 5
        '
        'LayoutControlItem163
        '
        Me.LayoutControlItem163.Control = Me.TextEdit64
        Me.LayoutControlItem163.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem163.Name = "LayoutControlItem34"
        Me.LayoutControlItem163.Size = New System.Drawing.Size(684, 42)
        Me.LayoutControlItem163.Text = "Firstname(s)"
        Me.LayoutControlItem163.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem163.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem163.TextToControlDistance = 5
        '
        'LayoutControlItem164
        '
        Me.LayoutControlItem164.Control = Me.TextEdit65
        Me.LayoutControlItem164.Location = New System.Drawing.Point(0, 186)
        Me.LayoutControlItem164.Name = "LayoutControlItem35"
        Me.LayoutControlItem164.Size = New System.Drawing.Size(684, 42)
        Me.LayoutControlItem164.Text = "Surname"
        Me.LayoutControlItem164.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem164.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem164.TextToControlDistance = 5
        '
        'EmptySpaceItem29
        '
        Me.EmptySpaceItem29.AllowHotTrack = false
        Me.EmptySpaceItem29.Location = New System.Drawing.Point(684, 144)
        Me.EmptySpaceItem29.Name = "EmptySpaceItem12"
        Me.EmptySpaceItem29.Size = New System.Drawing.Size(267, 42)
        Me.EmptySpaceItem29.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem30
        '
        Me.EmptySpaceItem30.AllowHotTrack = false
        Me.EmptySpaceItem30.Location = New System.Drawing.Point(684, 186)
        Me.EmptySpaceItem30.Name = "EmptySpaceItem13"
        Me.EmptySpaceItem30.Size = New System.Drawing.Size(267, 42)
        Me.EmptySpaceItem30.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem31
        '
        Me.EmptySpaceItem31.AllowHotTrack = false
        Me.EmptySpaceItem31.Location = New System.Drawing.Point(810, 316)
        Me.EmptySpaceItem31.Name = "EmptySpaceItem14"
        Me.EmptySpaceItem31.Size = New System.Drawing.Size(141, 131)
        Me.EmptySpaceItem31.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem165
        '
        Me.LayoutControlItem165.Control = Me.DateEdit8
        Me.LayoutControlItem165.Location = New System.Drawing.Point(0, 228)
        Me.LayoutControlItem165.Name = "LayoutControlItem36"
        Me.LayoutControlItem165.Size = New System.Drawing.Size(684, 42)
        Me.LayoutControlItem165.Text = "Date of Birth"
        Me.LayoutControlItem165.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem165.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem165.TextToControlDistance = 5
        '
        'EmptySpaceItem32
        '
        Me.EmptySpaceItem32.AllowHotTrack = false
        Me.EmptySpaceItem32.Location = New System.Drawing.Point(684, 228)
        Me.EmptySpaceItem32.Name = "EmptySpaceItem15"
        Me.EmptySpaceItem32.Size = New System.Drawing.Size(267, 42)
        Me.EmptySpaceItem32.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage21
        '
        Me.XtraTabPage21.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage21.Controls.Add(Me.GridControl10)
        Me.XtraTabPage21.Name = "XtraTabPage21"
        Me.XtraTabPage21.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage21.Text = "Attendance Notes"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.MemoEdit14)
        Me.PanelControl1.Controls.Add(Me.LabelControl56)
        Me.PanelControl1.Location = New System.Drawing.Point(663, 26)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(335, 506)
        Me.PanelControl1.TabIndex = 2
        '
        'MemoEdit14
        '
        Me.MemoEdit14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit14.Location = New System.Drawing.Point(2, 39)
        Me.MemoEdit14.Name = "MemoEdit14"
        Me.MemoEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.MemoEdit14.Size = New System.Drawing.Size(331, 465)
        Me.MemoEdit14.TabIndex = 1
        '
        'LabelControl56
        '
        Me.LabelControl56.Appearance.BackColor = System.Drawing.Color.DimGray
        Me.LabelControl56.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl56.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl56.Appearance.Options.UseBackColor = true
        Me.LabelControl56.Appearance.Options.UseFont = true
        Me.LabelControl56.Appearance.Options.UseForeColor = true
        Me.LabelControl56.Appearance.Options.UseTextOptions = true
        Me.LabelControl56.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl56.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl56.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl56.Location = New System.Drawing.Point(2, 2)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(331, 37)
        Me.LabelControl56.TabIndex = 0
        Me.LabelControl56.Text = "Supplementary Information"
        '
        'GridControl10
        '
        Me.GridControl10.Location = New System.Drawing.Point(30, 26)
        Me.GridControl10.MainView = Me.GridView10
        Me.GridControl10.Name = "GridControl10"
        Me.GridControl10.Size = New System.Drawing.Size(608, 506)
        Me.GridControl10.TabIndex = 1
        Me.GridControl10.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView10})
        '
        'GridView10
        '
        Me.GridView10.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn30, Me.GridColumn31, Me.GridColumn32})
        Me.GridView10.GridControl = Me.GridControl10
        Me.GridView10.Name = "GridView10"
        Me.GridView10.OptionsView.ShowGroupPanel = false
        Me.GridView10.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView10.OptionsView.ShowViewCaption = true
        Me.GridView10.ViewCaption = "Attendance Notes"
        '
        'GridColumn30
        '
        Me.GridColumn30.Name = "GridColumn30"
        Me.GridColumn30.Visible = true
        Me.GridColumn30.VisibleIndex = 0
        '
        'GridColumn31
        '
        Me.GridColumn31.Name = "GridColumn31"
        Me.GridColumn31.Visible = true
        Me.GridColumn31.VisibleIndex = 1
        '
        'GridColumn32
        '
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.Visible = true
        Me.GridColumn32.VisibleIndex = 2
        '
        'NavigationPageFAPTInstruction
        '
        Me.NavigationPageFAPTInstruction.Caption = "   FAPT Instruction"
        Me.NavigationPageFAPTInstruction.Controls.Add(Me.XtraTabControl4)
        Me.NavigationPageFAPTInstruction.ImageOptions.Image = CType(resources.GetObject("NavigationPageFAPTInstruction.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageFAPTInstruction.Name = "NavigationPageFAPTInstruction"
        Me.NavigationPageFAPTInstruction.Size = New System.Drawing.Size(1026, 602)
        '
        'XtraTabControl4
        '
        Me.XtraTabControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl4.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl4.Name = "XtraTabControl4"
        Me.XtraTabControl4.SelectedTabPage = Me.XtraTabPage22
        Me.XtraTabControl4.Size = New System.Drawing.Size(1026, 602)
        Me.XtraTabControl4.TabIndex = 0
        Me.XtraTabControl4.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage22, Me.XtraTabPage23, Me.XtraTabPage24, Me.XtraTabPage25, Me.XtraTabPage26, Me.XtraTabPage27, Me.XtraTabPage28})
        '
        'XtraTabPage22
        '
        Me.XtraTabPage22.Controls.Add(Me.GroupControl6)
        Me.XtraTabPage22.Controls.Add(Me.GroupControl5)
        Me.XtraTabPage22.Name = "XtraTabPage22"
        Me.XtraTabPage22.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage22.Text = "Settlor Details"
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.LayoutControl20)
        Me.GroupControl6.Location = New System.Drawing.Point(519, 27)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(468, 502)
        Me.GroupControl6.TabIndex = 6
        Me.GroupControl6.Text = "Settlor 2"
        '
        'LayoutControl20
        '
        Me.LayoutControl20.Controls.Add(Me.TextEdit68)
        Me.LayoutControl20.Controls.Add(Me.DateEdit10)
        Me.LayoutControl20.Controls.Add(Me.MemoEdit16)
        Me.LayoutControl20.Controls.Add(Me.TextEdit69)
        Me.LayoutControl20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl20.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl20.Name = "LayoutControl20"
        Me.LayoutControl20.Root = Me.LayoutControlGroup19
        Me.LayoutControl20.Size = New System.Drawing.Size(464, 479)
        Me.LayoutControl20.TabIndex = 0
        Me.LayoutControl20.Text = "LayoutControl20"
        '
        'TextEdit68
        '
        Me.TextEdit68.Location = New System.Drawing.Point(117, 38)
        Me.TextEdit68.Name = "TextEdit68"
        Me.TextEdit68.Size = New System.Drawing.Size(335, 38)
        Me.TextEdit68.StyleController = Me.LayoutControl20
        Me.TextEdit68.TabIndex = 4
        '
        'DateEdit10
        '
        Me.DateEdit10.EditValue = Nothing
        Me.DateEdit10.Location = New System.Drawing.Point(117, 80)
        Me.DateEdit10.Name = "DateEdit10"
        Me.DateEdit10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit10.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit10.Size = New System.Drawing.Size(335, 38)
        Me.DateEdit10.StyleController = Me.LayoutControl20
        Me.DateEdit10.TabIndex = 5
        '
        'MemoEdit16
        '
        Me.MemoEdit16.Location = New System.Drawing.Point(117, 122)
        Me.MemoEdit16.Name = "MemoEdit16"
        Me.MemoEdit16.Size = New System.Drawing.Size(335, 170)
        Me.MemoEdit16.StyleController = Me.LayoutControl20
        Me.MemoEdit16.TabIndex = 6
        '
        'TextEdit69
        '
        Me.TextEdit69.Location = New System.Drawing.Point(120, 296)
        Me.TextEdit69.Name = "TextEdit69"
        Me.TextEdit69.Size = New System.Drawing.Size(332, 38)
        Me.TextEdit69.StyleController = Me.LayoutControl20
        Me.TextEdit69.TabIndex = 7
        '
        'LayoutControlGroup19
        '
        Me.LayoutControlGroup19.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup19.GroupBordersVisible = false
        Me.LayoutControlGroup19.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem36, Me.EmptySpaceItem37, Me.LayoutControlItem170, Me.LayoutControlItem171, Me.LayoutControlItem172, Me.LayoutControlItem173, Me.EmptySpaceItem38})
        Me.LayoutControlGroup19.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup19.Size = New System.Drawing.Size(464, 479)
        Me.LayoutControlGroup19.TextVisible = false
        '
        'EmptySpaceItem36
        '
        Me.EmptySpaceItem36.AllowHotTrack = false
        Me.EmptySpaceItem36.Location = New System.Drawing.Point(0, 326)
        Me.EmptySpaceItem36.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem36.Size = New System.Drawing.Size(444, 133)
        Me.EmptySpaceItem36.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem37
        '
        Me.EmptySpaceItem37.AllowHotTrack = false
        Me.EmptySpaceItem37.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem37.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem37.Size = New System.Drawing.Size(444, 26)
        Me.EmptySpaceItem37.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem170
        '
        Me.LayoutControlItem170.Control = Me.TextEdit68
        Me.LayoutControlItem170.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem170.Name = "LayoutControlItem7"
        Me.LayoutControlItem170.Size = New System.Drawing.Size(444, 42)
        Me.LayoutControlItem170.Text = "Name"
        Me.LayoutControlItem170.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem170.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem170.TextToControlDistance = 5
        '
        'LayoutControlItem171
        '
        Me.LayoutControlItem171.Control = Me.DateEdit10
        Me.LayoutControlItem171.Location = New System.Drawing.Point(0, 68)
        Me.LayoutControlItem171.Name = "LayoutControlItem8"
        Me.LayoutControlItem171.Size = New System.Drawing.Size(444, 42)
        Me.LayoutControlItem171.Text = "Date of Birth"
        Me.LayoutControlItem171.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem171.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem171.TextToControlDistance = 5
        '
        'LayoutControlItem172
        '
        Me.LayoutControlItem172.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem172.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem172.Control = Me.MemoEdit16
        Me.LayoutControlItem172.Location = New System.Drawing.Point(0, 110)
        Me.LayoutControlItem172.Name = "LayoutControlItem9"
        Me.LayoutControlItem172.Size = New System.Drawing.Size(444, 174)
        Me.LayoutControlItem172.Text = "Address"
        Me.LayoutControlItem172.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem172.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem172.TextToControlDistance = 5
        '
        'LayoutControlItem173
        '
        Me.LayoutControlItem173.Control = Me.TextEdit69
        Me.LayoutControlItem173.Location = New System.Drawing.Point(108, 284)
        Me.LayoutControlItem173.Name = "LayoutControlItem10"
        Me.LayoutControlItem173.Size = New System.Drawing.Size(336, 42)
        Me.LayoutControlItem173.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem173.TextVisible = false
        '
        'EmptySpaceItem38
        '
        Me.EmptySpaceItem38.AllowHotTrack = false
        Me.EmptySpaceItem38.Location = New System.Drawing.Point(0, 284)
        Me.EmptySpaceItem38.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem38.Size = New System.Drawing.Size(108, 42)
        Me.EmptySpaceItem38.TextSize = New System.Drawing.Size(0, 0)
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.LayoutControl19)
        Me.GroupControl5.Location = New System.Drawing.Point(45, 27)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(439, 502)
        Me.GroupControl5.TabIndex = 5
        Me.GroupControl5.Text = "Settlor 1"
        '
        'LayoutControl19
        '
        Me.LayoutControl19.Controls.Add(Me.TextEdit66)
        Me.LayoutControl19.Controls.Add(Me.DateEdit9)
        Me.LayoutControl19.Controls.Add(Me.MemoEdit15)
        Me.LayoutControl19.Controls.Add(Me.TextEdit67)
        Me.LayoutControl19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl19.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl19.Name = "LayoutControl19"
        Me.LayoutControl19.Root = Me.LayoutControlGroup18
        Me.LayoutControl19.Size = New System.Drawing.Size(435, 479)
        Me.LayoutControl19.TabIndex = 0
        Me.LayoutControl19.Text = "LayoutControl19"
        '
        'TextEdit66
        '
        Me.TextEdit66.Location = New System.Drawing.Point(117, 38)
        Me.TextEdit66.Name = "TextEdit66"
        Me.TextEdit66.Size = New System.Drawing.Size(306, 38)
        Me.TextEdit66.StyleController = Me.LayoutControl19
        Me.TextEdit66.TabIndex = 4
        '
        'DateEdit9
        '
        Me.DateEdit9.EditValue = Nothing
        Me.DateEdit9.Location = New System.Drawing.Point(117, 80)
        Me.DateEdit9.Name = "DateEdit9"
        Me.DateEdit9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit9.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit9.Size = New System.Drawing.Size(306, 38)
        Me.DateEdit9.StyleController = Me.LayoutControl19
        Me.DateEdit9.TabIndex = 5
        '
        'MemoEdit15
        '
        Me.MemoEdit15.Location = New System.Drawing.Point(117, 122)
        Me.MemoEdit15.Name = "MemoEdit15"
        Me.MemoEdit15.Size = New System.Drawing.Size(306, 170)
        Me.MemoEdit15.StyleController = Me.LayoutControl19
        Me.MemoEdit15.TabIndex = 6
        '
        'TextEdit67
        '
        Me.TextEdit67.Location = New System.Drawing.Point(119, 296)
        Me.TextEdit67.Name = "TextEdit67"
        Me.TextEdit67.Size = New System.Drawing.Size(304, 38)
        Me.TextEdit67.StyleController = Me.LayoutControl19
        Me.TextEdit67.TabIndex = 7
        '
        'LayoutControlGroup18
        '
        Me.LayoutControlGroup18.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup18.GroupBordersVisible = false
        Me.LayoutControlGroup18.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem33, Me.EmptySpaceItem34, Me.LayoutControlItem166, Me.LayoutControlItem167, Me.LayoutControlItem168, Me.LayoutControlItem169, Me.EmptySpaceItem35})
        Me.LayoutControlGroup18.Name = "Root"
        Me.LayoutControlGroup18.Size = New System.Drawing.Size(435, 479)
        Me.LayoutControlGroup18.TextVisible = false
        '
        'EmptySpaceItem33
        '
        Me.EmptySpaceItem33.AllowHotTrack = false
        Me.EmptySpaceItem33.Location = New System.Drawing.Point(0, 326)
        Me.EmptySpaceItem33.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem33.Size = New System.Drawing.Size(415, 133)
        Me.EmptySpaceItem33.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem34
        '
        Me.EmptySpaceItem34.AllowHotTrack = false
        Me.EmptySpaceItem34.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem34.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem34.Size = New System.Drawing.Size(415, 26)
        Me.EmptySpaceItem34.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem166
        '
        Me.LayoutControlItem166.Control = Me.TextEdit66
        Me.LayoutControlItem166.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem166.Name = "LayoutControlItem3"
        Me.LayoutControlItem166.Size = New System.Drawing.Size(415, 42)
        Me.LayoutControlItem166.Text = "Name"
        Me.LayoutControlItem166.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem166.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem166.TextToControlDistance = 5
        '
        'LayoutControlItem167
        '
        Me.LayoutControlItem167.Control = Me.DateEdit9
        Me.LayoutControlItem167.Location = New System.Drawing.Point(0, 68)
        Me.LayoutControlItem167.Name = "LayoutControlItem4"
        Me.LayoutControlItem167.Size = New System.Drawing.Size(415, 42)
        Me.LayoutControlItem167.Text = "Date of Birth"
        Me.LayoutControlItem167.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem167.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem167.TextToControlDistance = 5
        '
        'LayoutControlItem168
        '
        Me.LayoutControlItem168.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem168.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem168.Control = Me.MemoEdit15
        Me.LayoutControlItem168.Location = New System.Drawing.Point(0, 110)
        Me.LayoutControlItem168.Name = "LayoutControlItem5"
        Me.LayoutControlItem168.Size = New System.Drawing.Size(415, 174)
        Me.LayoutControlItem168.Text = "Address"
        Me.LayoutControlItem168.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem168.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem168.TextToControlDistance = 5
        '
        'LayoutControlItem169
        '
        Me.LayoutControlItem169.Control = Me.TextEdit67
        Me.LayoutControlItem169.Location = New System.Drawing.Point(107, 284)
        Me.LayoutControlItem169.Name = "LayoutControlItem6"
        Me.LayoutControlItem169.Size = New System.Drawing.Size(308, 42)
        Me.LayoutControlItem169.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem169.TextVisible = false
        '
        'EmptySpaceItem35
        '
        Me.EmptySpaceItem35.AllowHotTrack = false
        Me.EmptySpaceItem35.Location = New System.Drawing.Point(0, 284)
        Me.EmptySpaceItem35.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem35.Size = New System.Drawing.Size(107, 42)
        Me.EmptySpaceItem35.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage23
        '
        Me.XtraTabPage23.Controls.Add(Me.SimpleButton17)
        Me.XtraTabPage23.Controls.Add(Me.SimpleButton16)
        Me.XtraTabPage23.Controls.Add(Me.LayoutControl22)
        Me.XtraTabPage23.Controls.Add(Me.NavigationPageTrusteeInformation)
        Me.XtraTabPage23.Name = "XtraTabPage23"
        Me.XtraTabPage23.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage23.Text = "Trustee Information"
        '
        'SimpleButton17
        '
        Me.SimpleButton17.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton17.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton17.Location = New System.Drawing.Point(215, 13)
        Me.SimpleButton17.Name = "SimpleButton17"
        Me.SimpleButton17.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton17.TabIndex = 21
        Me.SimpleButton17.Text = "Add a Trustee"
        '
        'SimpleButton16
        '
        Me.SimpleButton16.ImageOptions.Image = CType(resources.GetObject("SimpleButton16.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton16.Location = New System.Drawing.Point(24, 13)
        Me.SimpleButton16.Name = "SimpleButton16"
        Me.SimpleButton16.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton16.TabIndex = 21
        Me.SimpleButton16.Text = "Add Children"
        '
        'LayoutControl22
        '
        Me.LayoutControl22.Controls.Add(Me.GridControl12)
        Me.LayoutControl22.Controls.Add(Me.CheckEdit136)
        Me.LayoutControl22.Controls.Add(Me.CheckEdit137)
        Me.LayoutControl22.Controls.Add(Me.CheckEdit138)
        Me.LayoutControl22.Controls.Add(Me.CheckEdit139)
        Me.LayoutControl22.Controls.Add(Me.CheckEdit140)
        Me.LayoutControl22.Location = New System.Drawing.Point(12, 58)
        Me.LayoutControl22.Name = "LayoutControl22"
        Me.LayoutControl22.Root = Me.LayoutControlGroup21
        Me.LayoutControl22.Size = New System.Drawing.Size(987, 494)
        Me.LayoutControl22.TabIndex = 2
        Me.LayoutControl22.Text = "LayoutControl22"
        '
        'GridControl12
        '
        Me.GridControl12.Location = New System.Drawing.Point(12, 60)
        Me.GridControl12.MainView = Me.GridView12
        Me.GridControl12.Name = "GridControl12"
        Me.GridControl12.Size = New System.Drawing.Size(963, 422)
        Me.GridControl12.TabIndex = 9
        Me.GridControl12.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView12})
        '
        'GridView12
        '
        Me.GridView12.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn37, Me.GridColumn38, Me.GridColumn39, Me.GridColumn40})
        Me.GridView12.GridControl = Me.GridControl12
        Me.GridView12.Name = "GridView12"
        Me.GridView12.OptionsView.ShowGroupPanel = false
        Me.GridView12.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn37
        '
        Me.GridColumn37.Caption = "Trustee Name"
        Me.GridColumn37.Name = "GridColumn37"
        Me.GridColumn37.Visible = true
        Me.GridColumn37.VisibleIndex = 0
        Me.GridColumn37.Width = 379
        '
        'GridColumn38
        '
        Me.GridColumn38.Caption = "Relationship to Client 1"
        Me.GridColumn38.Name = "GridColumn38"
        Me.GridColumn38.Visible = true
        Me.GridColumn38.VisibleIndex = 1
        Me.GridColumn38.Width = 259
        '
        'GridColumn39
        '
        Me.GridColumn39.Caption = "Relationship to Client 2"
        Me.GridColumn39.Name = "GridColumn39"
        Me.GridColumn39.Visible = true
        Me.GridColumn39.VisibleIndex = 2
        Me.GridColumn39.Width = 258
        '
        'GridColumn40
        '
        Me.GridColumn40.Caption = "Action"
        Me.GridColumn40.Name = "GridColumn40"
        Me.GridColumn40.OptionsColumn.FixedWidth = true
        Me.GridColumn40.Visible = true
        Me.GridColumn40.VisibleIndex = 3
        Me.GridColumn40.Width = 100
        '
        'CheckEdit136
        '
        Me.CheckEdit136.Location = New System.Drawing.Point(166, 12)
        Me.CheckEdit136.Name = "CheckEdit136"
        Me.CheckEdit136.Properties.Caption = "NONE"
        Me.CheckEdit136.Size = New System.Drawing.Size(88, 44)
        Me.CheckEdit136.StyleController = Me.LayoutControl22
        Me.CheckEdit136.TabIndex = 4
        '
        'CheckEdit137
        '
        Me.CheckEdit137.Location = New System.Drawing.Point(258, 12)
        Me.CheckEdit137.Name = "CheckEdit137"
        Me.CheckEdit137.Properties.Caption = "SETTLOR(S) ONLY"
        Me.CheckEdit137.Size = New System.Drawing.Size(165, 44)
        Me.CheckEdit137.StyleController = Me.LayoutControl22
        Me.CheckEdit137.TabIndex = 5
        '
        'CheckEdit138
        '
        Me.CheckEdit138.Location = New System.Drawing.Point(427, 12)
        Me.CheckEdit138.Name = "CheckEdit138"
        Me.CheckEdit138.Properties.Caption = "OUR COMPANY"
        Me.CheckEdit138.Size = New System.Drawing.Size(147, 44)
        Me.CheckEdit138.StyleController = Me.LayoutControl22
        Me.CheckEdit138.TabIndex = 6
        '
        'CheckEdit139
        '
        Me.CheckEdit139.Location = New System.Drawing.Point(578, 12)
        Me.CheckEdit139.Name = "CheckEdit139"
        Me.CheckEdit139.Properties.Caption = "SETTLOR(S) WITH OTHERS"
        Me.CheckEdit139.Size = New System.Drawing.Size(215, 44)
        Me.CheckEdit139.StyleController = Me.LayoutControl22
        Me.CheckEdit139.TabIndex = 7
        '
        'CheckEdit140
        '
        Me.CheckEdit140.Location = New System.Drawing.Point(797, 12)
        Me.CheckEdit140.Name = "CheckEdit140"
        Me.CheckEdit140.Properties.Caption = "NAMED INDIVIDUALS"
        Me.CheckEdit140.Size = New System.Drawing.Size(178, 44)
        Me.CheckEdit140.StyleController = Me.LayoutControl22
        Me.CheckEdit140.TabIndex = 8
        '
        'LayoutControlGroup21
        '
        Me.LayoutControlGroup21.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup21.GroupBordersVisible = false
        Me.LayoutControlGroup21.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem180, Me.LayoutControlItem181, Me.LayoutControlItem182, Me.LayoutControlItem183, Me.LayoutControlItem184, Me.LayoutControlItem185})
        Me.LayoutControlGroup21.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup21.Size = New System.Drawing.Size(987, 494)
        Me.LayoutControlGroup21.TextVisible = false
        '
        'LayoutControlItem180
        '
        Me.LayoutControlItem180.Control = Me.CheckEdit136
        Me.LayoutControlItem180.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem180.Name = "LayoutControlItem11"
        Me.LayoutControlItem180.Size = New System.Drawing.Size(246, 48)
        Me.LayoutControlItem180.Text = "Type of Trustees Required"
        Me.LayoutControlItem180.TextSize = New System.Drawing.Size(151, 16)
        '
        'LayoutControlItem181
        '
        Me.LayoutControlItem181.Control = Me.CheckEdit137
        Me.LayoutControlItem181.Location = New System.Drawing.Point(246, 0)
        Me.LayoutControlItem181.Name = "LayoutControlItem12"
        Me.LayoutControlItem181.Size = New System.Drawing.Size(169, 48)
        Me.LayoutControlItem181.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem181.TextVisible = false
        '
        'LayoutControlItem182
        '
        Me.LayoutControlItem182.Control = Me.CheckEdit138
        Me.LayoutControlItem182.Location = New System.Drawing.Point(415, 0)
        Me.LayoutControlItem182.Name = "LayoutControlItem13"
        Me.LayoutControlItem182.Size = New System.Drawing.Size(151, 48)
        Me.LayoutControlItem182.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem182.TextVisible = false
        '
        'LayoutControlItem183
        '
        Me.LayoutControlItem183.Control = Me.CheckEdit139
        Me.LayoutControlItem183.Location = New System.Drawing.Point(566, 0)
        Me.LayoutControlItem183.Name = "LayoutControlItem14"
        Me.LayoutControlItem183.Size = New System.Drawing.Size(219, 48)
        Me.LayoutControlItem183.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem183.TextVisible = false
        '
        'LayoutControlItem184
        '
        Me.LayoutControlItem184.Control = Me.CheckEdit140
        Me.LayoutControlItem184.Location = New System.Drawing.Point(785, 0)
        Me.LayoutControlItem184.Name = "LayoutControlItem15"
        Me.LayoutControlItem184.Size = New System.Drawing.Size(182, 48)
        Me.LayoutControlItem184.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem184.TextVisible = false
        '
        'LayoutControlItem185
        '
        Me.LayoutControlItem185.Control = Me.GridControl12
        Me.LayoutControlItem185.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem185.Name = "LayoutControlItem16"
        Me.LayoutControlItem185.Size = New System.Drawing.Size(967, 426)
        Me.LayoutControlItem185.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem185.TextVisible = false
        '
        'NavigationPageTrusteeInformation
        '
        Me.NavigationPageTrusteeInformation.Caption = "Trustee Information"
        Me.NavigationPageTrusteeInformation.Controls.Add(Me.LayoutControl21)
        CustomHeaderButtonImageOptions1.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions1.Image"),System.Drawing.Image)
        CustomHeaderButtonImageOptions2.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions2.Image"),System.Drawing.Image)
        Me.NavigationPageTrusteeInformation.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add a Trustee", true, CustomHeaderButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject1, Nothing, -1), New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Children", true, CustomHeaderButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject2, Nothing, -1)})
        Me.NavigationPageTrusteeInformation.ImageOptions.Image = CType(resources.GetObject("NavigationPageTrusteeInformation.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageTrusteeInformation.Name = "NavigationPageTrusteeInformation"
        Me.NavigationPageTrusteeInformation.Size = New System.Drawing.Size(1038, 597)
        '
        'LayoutControl21
        '
        Me.LayoutControl21.Controls.Add(Me.GridControl11)
        Me.LayoutControl21.Controls.Add(Me.CheckEdit131)
        Me.LayoutControl21.Controls.Add(Me.CheckEdit132)
        Me.LayoutControl21.Controls.Add(Me.CheckEdit133)
        Me.LayoutControl21.Controls.Add(Me.CheckEdit134)
        Me.LayoutControl21.Controls.Add(Me.CheckEdit135)
        Me.LayoutControl21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl21.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl21.Name = "LayoutControl21"
        Me.LayoutControl21.Root = Me.LayoutControlGroup20
        Me.LayoutControl21.Size = New System.Drawing.Size(1038, 597)
        Me.LayoutControl21.TabIndex = 0
        Me.LayoutControl21.Text = "LayoutControl21"
        '
        'GridControl11
        '
        Me.GridControl11.Location = New System.Drawing.Point(12, 60)
        Me.GridControl11.MainView = Me.GridView11
        Me.GridControl11.Name = "GridControl11"
        Me.GridControl11.Size = New System.Drawing.Size(1014, 525)
        Me.GridControl11.TabIndex = 9
        Me.GridControl11.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView11})
        '
        'GridView11
        '
        Me.GridView11.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn33, Me.GridColumn34, Me.GridColumn35, Me.GridColumn36})
        Me.GridView11.GridControl = Me.GridControl11
        Me.GridView11.Name = "GridView11"
        '
        'GridColumn33
        '
        Me.GridColumn33.Caption = "Trustee Name"
        Me.GridColumn33.Name = "GridColumn33"
        Me.GridColumn33.Visible = true
        Me.GridColumn33.VisibleIndex = 0
        Me.GridColumn33.Width = 379
        '
        'GridColumn34
        '
        Me.GridColumn34.Caption = "Relationship to Client 1"
        Me.GridColumn34.Name = "GridColumn34"
        Me.GridColumn34.Visible = true
        Me.GridColumn34.VisibleIndex = 1
        Me.GridColumn34.Width = 259
        '
        'GridColumn35
        '
        Me.GridColumn35.Caption = "Relationship to Client 2"
        Me.GridColumn35.Name = "GridColumn35"
        Me.GridColumn35.Visible = true
        Me.GridColumn35.VisibleIndex = 2
        Me.GridColumn35.Width = 258
        '
        'GridColumn36
        '
        Me.GridColumn36.Caption = "Action"
        Me.GridColumn36.Name = "GridColumn36"
        Me.GridColumn36.OptionsColumn.FixedWidth = true
        Me.GridColumn36.Visible = true
        Me.GridColumn36.VisibleIndex = 3
        Me.GridColumn36.Width = 100
        '
        'CheckEdit131
        '
        Me.CheckEdit131.Location = New System.Drawing.Point(166, 12)
        Me.CheckEdit131.Name = "CheckEdit131"
        Me.CheckEdit131.Properties.Caption = "NONE"
        Me.CheckEdit131.Size = New System.Drawing.Size(101, 44)
        Me.CheckEdit131.StyleController = Me.LayoutControl21
        Me.CheckEdit131.TabIndex = 4
        '
        'CheckEdit132
        '
        Me.CheckEdit132.Location = New System.Drawing.Point(271, 12)
        Me.CheckEdit132.Name = "CheckEdit132"
        Me.CheckEdit132.Properties.Caption = "SETTLOR(S) ONLY"
        Me.CheckEdit132.Size = New System.Drawing.Size(174, 44)
        Me.CheckEdit132.StyleController = Me.LayoutControl21
        Me.CheckEdit132.TabIndex = 5
        '
        'CheckEdit133
        '
        Me.CheckEdit133.Location = New System.Drawing.Point(449, 12)
        Me.CheckEdit133.Name = "CheckEdit133"
        Me.CheckEdit133.Properties.Caption = "OUR COMPANY"
        Me.CheckEdit133.Size = New System.Drawing.Size(155, 44)
        Me.CheckEdit133.StyleController = Me.LayoutControl21
        Me.CheckEdit133.TabIndex = 6
        '
        'CheckEdit134
        '
        Me.CheckEdit134.Location = New System.Drawing.Point(608, 12)
        Me.CheckEdit134.Name = "CheckEdit134"
        Me.CheckEdit134.Properties.Caption = "SETTLOR(S) WITH OTHERS"
        Me.CheckEdit134.Size = New System.Drawing.Size(221, 44)
        Me.CheckEdit134.StyleController = Me.LayoutControl21
        Me.CheckEdit134.TabIndex = 7
        '
        'CheckEdit135
        '
        Me.CheckEdit135.Location = New System.Drawing.Point(833, 12)
        Me.CheckEdit135.Name = "CheckEdit135"
        Me.CheckEdit135.Properties.Caption = "NAMED INDIVIDUALS"
        Me.CheckEdit135.Size = New System.Drawing.Size(193, 44)
        Me.CheckEdit135.StyleController = Me.LayoutControl21
        Me.CheckEdit135.TabIndex = 8
        '
        'LayoutControlGroup20
        '
        Me.LayoutControlGroup20.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup20.GroupBordersVisible = false
        Me.LayoutControlGroup20.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem174, Me.LayoutControlItem175, Me.LayoutControlItem176, Me.LayoutControlItem177, Me.LayoutControlItem178, Me.LayoutControlItem179})
        Me.LayoutControlGroup20.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup20.Size = New System.Drawing.Size(1038, 597)
        Me.LayoutControlGroup20.TextVisible = false
        '
        'LayoutControlItem174
        '
        Me.LayoutControlItem174.Control = Me.CheckEdit131
        Me.LayoutControlItem174.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem174.Name = "LayoutControlItem11"
        Me.LayoutControlItem174.Size = New System.Drawing.Size(259, 48)
        Me.LayoutControlItem174.Text = "Type of Trustees Required"
        Me.LayoutControlItem174.TextSize = New System.Drawing.Size(151, 16)
        '
        'LayoutControlItem175
        '
        Me.LayoutControlItem175.Control = Me.CheckEdit132
        Me.LayoutControlItem175.Location = New System.Drawing.Point(259, 0)
        Me.LayoutControlItem175.Name = "LayoutControlItem12"
        Me.LayoutControlItem175.Size = New System.Drawing.Size(178, 48)
        Me.LayoutControlItem175.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem175.TextVisible = false
        '
        'LayoutControlItem176
        '
        Me.LayoutControlItem176.Control = Me.CheckEdit133
        Me.LayoutControlItem176.Location = New System.Drawing.Point(437, 0)
        Me.LayoutControlItem176.Name = "LayoutControlItem13"
        Me.LayoutControlItem176.Size = New System.Drawing.Size(159, 48)
        Me.LayoutControlItem176.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem176.TextVisible = false
        '
        'LayoutControlItem177
        '
        Me.LayoutControlItem177.Control = Me.CheckEdit134
        Me.LayoutControlItem177.Location = New System.Drawing.Point(596, 0)
        Me.LayoutControlItem177.Name = "LayoutControlItem14"
        Me.LayoutControlItem177.Size = New System.Drawing.Size(225, 48)
        Me.LayoutControlItem177.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem177.TextVisible = false
        '
        'LayoutControlItem178
        '
        Me.LayoutControlItem178.Control = Me.CheckEdit135
        Me.LayoutControlItem178.Location = New System.Drawing.Point(821, 0)
        Me.LayoutControlItem178.Name = "LayoutControlItem15"
        Me.LayoutControlItem178.Size = New System.Drawing.Size(197, 48)
        Me.LayoutControlItem178.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem178.TextVisible = false
        '
        'LayoutControlItem179
        '
        Me.LayoutControlItem179.Control = Me.GridControl11
        Me.LayoutControlItem179.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem179.Name = "LayoutControlItem16"
        Me.LayoutControlItem179.Size = New System.Drawing.Size(1018, 529)
        Me.LayoutControlItem179.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem179.TextVisible = false
        '
        'XtraTabPage24
        '
        Me.XtraTabPage24.Controls.Add(Me.SimpleButtonLookup5)
        Me.XtraTabPage24.Controls.Add(Me.TextEdit70)
        Me.XtraTabPage24.Controls.Add(Me.TextEdit71)
        Me.XtraTabPage24.Controls.Add(Me.TextEdit72)
        Me.XtraTabPage24.Controls.Add(Me.LabelControl58)
        Me.XtraTabPage24.Controls.Add(Me.LabelControl59)
        Me.XtraTabPage24.Controls.Add(Me.CheckEdit141)
        Me.XtraTabPage24.Controls.Add(Me.CheckEdit142)
        Me.XtraTabPage24.Controls.Add(Me.LabelControl57)
        Me.XtraTabPage24.Controls.Add(Me.SimpleButton18)
        Me.XtraTabPage24.Controls.Add(Me.SimpleButton19)
        Me.XtraTabPage24.Controls.Add(Me.GridControl13)
        Me.XtraTabPage24.Name = "XtraTabPage24"
        Me.XtraTabPage24.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage24.Text = "Beneficiaries"
        '
        'SimpleButtonLookup5
        '
        Me.SimpleButtonLookup5.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton20.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup5.Location = New System.Drawing.Point(879, 431)
        Me.SimpleButtonLookup5.Name = "SimpleButtonLookup5"
        Me.SimpleButtonLookup5.Size = New System.Drawing.Size(136, 38)
        Me.SimpleButtonLookup5.TabIndex = 32
        Me.SimpleButtonLookup5.Text = "Lookup"
        '
        'TextEdit70
        '
        Me.TextEdit70.Location = New System.Drawing.Point(667, 475)
        Me.TextEdit70.Name = "TextEdit70"
        Me.TextEdit70.Size = New System.Drawing.Size(206, 38)
        Me.TextEdit70.TabIndex = 29
        '
        'TextEdit71
        '
        Me.TextEdit71.Location = New System.Drawing.Point(667, 431)
        Me.TextEdit71.Name = "TextEdit71"
        Me.TextEdit71.Size = New System.Drawing.Size(206, 38)
        Me.TextEdit71.TabIndex = 30
        '
        'TextEdit72
        '
        Me.TextEdit72.Location = New System.Drawing.Point(24, 431)
        Me.TextEdit72.Name = "TextEdit72"
        Me.TextEdit72.Size = New System.Drawing.Size(470, 38)
        Me.TextEdit72.TabIndex = 31
        '
        'LabelControl58
        '
        Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl58.Appearance.Options.UseFont = true
        Me.LabelControl58.Location = New System.Drawing.Point(563, 486)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(100, 16)
        Me.LabelControl58.TabIndex = 27
        Me.LabelControl58.Text = "Charity Number"
        '
        'LabelControl59
        '
        Me.LabelControl59.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl59.Appearance.Options.UseFont = true
        Me.LabelControl59.Location = New System.Drawing.Point(563, 442)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(86, 16)
        Me.LabelControl59.TabIndex = 28
        Me.LabelControl59.Text = "Charity Name"
        '
        'CheckEdit141
        '
        Me.CheckEdit141.Location = New System.Drawing.Point(650, 383)
        Me.CheckEdit141.Name = "CheckEdit141"
        Me.CheckEdit141.Properties.Caption = "NO"
        Me.CheckEdit141.Size = New System.Drawing.Size(91, 44)
        Me.CheckEdit141.TabIndex = 26
        '
        'CheckEdit142
        '
        Me.CheckEdit142.Location = New System.Drawing.Point(553, 383)
        Me.CheckEdit142.Name = "CheckEdit142"
        Me.CheckEdit142.Properties.Caption = "YES"
        Me.CheckEdit142.Size = New System.Drawing.Size(91, 44)
        Me.CheckEdit142.TabIndex = 25
        '
        'LabelControl57
        '
        Me.LabelControl57.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl57.Appearance.Options.UseFont = true
        Me.LabelControl57.Location = New System.Drawing.Point(24, 397)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(471, 16)
        Me.LabelControl57.TabIndex = 24
        Me.LabelControl57.Text = "Does the Settlor(s) wish to state a specific charity as an ultimate backstop?"
        '
        'SimpleButton18
        '
        Me.SimpleButton18.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton18.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton18.Location = New System.Drawing.Point(215, 13)
        Me.SimpleButton18.Name = "SimpleButton18"
        Me.SimpleButton18.Size = New System.Drawing.Size(262, 45)
        Me.SimpleButton18.TabIndex = 22
        Me.SimpleButton18.Text = "Add Primary Beneficiary"
        '
        'SimpleButton19
        '
        Me.SimpleButton19.ImageOptions.Image = CType(resources.GetObject("SimpleButton19.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton19.Location = New System.Drawing.Point(24, 13)
        Me.SimpleButton19.Name = "SimpleButton19"
        Me.SimpleButton19.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton19.TabIndex = 23
        Me.SimpleButton19.Text = "Add Children"
        '
        'GridControl13
        '
        Me.GridControl13.Location = New System.Drawing.Point(24, 64)
        Me.GridControl13.MainView = Me.GridView13
        Me.GridControl13.Name = "GridControl13"
        Me.GridControl13.Size = New System.Drawing.Size(969, 306)
        Me.GridControl13.TabIndex = 3
        Me.GridControl13.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView13})
        '
        'GridView13
        '
        Me.GridView13.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn41, Me.GridColumn42, Me.GridColumn43, Me.GridColumn44, Me.GridColumn45})
        Me.GridView13.GridControl = Me.GridControl13
        Me.GridView13.Name = "GridView13"
        Me.GridView13.OptionsView.ShowGroupPanel = false
        '
        'GridColumn41
        '
        Me.GridColumn41.Caption = "Beneficiary Name"
        Me.GridColumn41.Name = "GridColumn41"
        Me.GridColumn41.Visible = true
        Me.GridColumn41.VisibleIndex = 0
        Me.GridColumn41.Width = 326
        '
        'GridColumn42
        '
        Me.GridColumn42.Caption = "Relationship to Client 1"
        Me.GridColumn42.Name = "GridColumn42"
        Me.GridColumn42.Visible = true
        Me.GridColumn42.VisibleIndex = 1
        Me.GridColumn42.Width = 247
        '
        'GridColumn43
        '
        Me.GridColumn43.Caption = "Relationship to Client 2"
        Me.GridColumn43.Name = "GridColumn43"
        Me.GridColumn43.Visible = true
        Me.GridColumn43.VisibleIndex = 2
        Me.GridColumn43.Width = 229
        '
        'GridColumn44
        '
        Me.GridColumn44.Caption = "Lapse/Issue"
        Me.GridColumn44.Name = "GridColumn44"
        Me.GridColumn44.Visible = true
        Me.GridColumn44.VisibleIndex = 3
        Me.GridColumn44.Width = 118
        '
        'GridColumn45
        '
        Me.GridColumn45.Caption = "Action"
        Me.GridColumn45.Name = "GridColumn45"
        Me.GridColumn45.OptionsColumn.FixedWidth = true
        Me.GridColumn45.Visible = true
        Me.GridColumn45.VisibleIndex = 4
        Me.GridColumn45.Width = 100
        '
        'XtraTabPage25
        '
        Me.XtraTabPage25.Controls.Add(Me.XtraTabControl5)
        Me.XtraTabPage25.Name = "XtraTabPage25"
        Me.XtraTabPage25.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage25.Text = "Trust Fund"
        '
        'XtraTabControl5
        '
        Me.XtraTabControl5.Location = New System.Drawing.Point(44, 34)
        Me.XtraTabControl5.Name = "XtraTabControl5"
        Me.XtraTabControl5.SelectedTabPage = Me.XtraTabPage29
        Me.XtraTabControl5.Size = New System.Drawing.Size(936, 491)
        Me.XtraTabControl5.TabIndex = 0
        Me.XtraTabControl5.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage29, Me.XtraTabPage30, Me.XtraTabPage31})
        '
        'XtraTabPage29
        '
        Me.XtraTabPage29.Controls.Add(Me.GridControl14)
        Me.XtraTabPage29.Controls.Add(Me.SimpleButton21)
        Me.XtraTabPage29.Name = "XtraTabPage29"
        Me.XtraTabPage29.Size = New System.Drawing.Size(934, 455)
        Me.XtraTabPage29.Text = "Property"
        '
        'GridControl14
        '
        Me.GridControl14.Location = New System.Drawing.Point(17, 55)
        Me.GridControl14.MainView = Me.GridView14
        Me.GridControl14.Name = "GridControl14"
        Me.GridControl14.Size = New System.Drawing.Size(905, 390)
        Me.GridControl14.TabIndex = 3
        Me.GridControl14.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView14})
        '
        'GridView14
        '
        Me.GridView14.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn46, Me.GridColumn47, Me.GridColumn48, Me.GridColumn49, Me.GridColumn50, Me.GridColumn51})
        Me.GridView14.GridControl = Me.GridControl14
        Me.GridView14.Name = "GridView14"
        Me.GridView14.OptionsView.ShowGroupPanel = false
        Me.GridView14.OptionsView.ShowIndicator = false
        Me.GridView14.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn46
        '
        Me.GridColumn46.Caption = "Asset Type"
        Me.GridColumn46.Name = "GridColumn46"
        Me.GridColumn46.Visible = true
        Me.GridColumn46.VisibleIndex = 0
        Me.GridColumn46.Width = 169
        '
        'GridColumn47
        '
        Me.GridColumn47.Caption = "Address"
        Me.GridColumn47.Name = "GridColumn47"
        Me.GridColumn47.Visible = true
        Me.GridColumn47.VisibleIndex = 1
        Me.GridColumn47.Width = 169
        '
        'GridColumn48
        '
        Me.GridColumn48.Caption = "Value"
        Me.GridColumn48.Name = "GridColumn48"
        Me.GridColumn48.Visible = true
        Me.GridColumn48.VisibleIndex = 2
        Me.GridColumn48.Width = 169
        '
        'GridColumn49
        '
        Me.GridColumn49.Caption = "Mortgage"
        Me.GridColumn49.Name = "GridColumn49"
        Me.GridColumn49.Visible = true
        Me.GridColumn49.VisibleIndex = 3
        Me.GridColumn49.Width = 169
        '
        'GridColumn50
        '
        Me.GridColumn50.Caption = "Include?"
        Me.GridColumn50.Name = "GridColumn50"
        Me.GridColumn50.Visible = true
        Me.GridColumn50.VisibleIndex = 4
        Me.GridColumn50.Width = 169
        '
        'GridColumn51
        '
        Me.GridColumn51.Caption = "Action"
        Me.GridColumn51.Name = "GridColumn51"
        Me.GridColumn51.OptionsColumn.FixedWidth = true
        Me.GridColumn51.Visible = true
        Me.GridColumn51.VisibleIndex = 5
        Me.GridColumn51.Width = 100
        '
        'SimpleButton21
        '
        Me.SimpleButton21.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton21.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton21.Location = New System.Drawing.Point(17, 16)
        Me.SimpleButton21.Name = "SimpleButton21"
        Me.SimpleButton21.Size = New System.Drawing.Size(233, 33)
        Me.SimpleButton21.TabIndex = 2
        Me.SimpleButton21.Text = "Add Property"
        '
        'XtraTabPage30
        '
        Me.XtraTabPage30.Controls.Add(Me.GridControl15)
        Me.XtraTabPage30.Controls.Add(Me.SimpleButton22)
        Me.XtraTabPage30.Name = "XtraTabPage30"
        Me.XtraTabPage30.Size = New System.Drawing.Size(934, 455)
        Me.XtraTabPage30.Text = "Specific Items"
        '
        'GridControl15
        '
        Me.GridControl15.Location = New System.Drawing.Point(17, 55)
        Me.GridControl15.MainView = Me.GridView15
        Me.GridControl15.Name = "GridControl15"
        Me.GridControl15.Size = New System.Drawing.Size(892, 381)
        Me.GridControl15.TabIndex = 4
        Me.GridControl15.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView15})
        '
        'GridView15
        '
        Me.GridView15.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn52, Me.GridColumn53, Me.GridColumn54, Me.GridColumn55, Me.GridColumn56})
        Me.GridView15.GridControl = Me.GridControl15
        Me.GridView15.Name = "GridView15"
        Me.GridView15.OptionsView.ShowGroupPanel = false
        Me.GridView15.OptionsView.ShowIndicator = false
        Me.GridView15.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn52
        '
        Me.GridColumn52.Caption = "Asset Type"
        Me.GridColumn52.Name = "GridColumn52"
        Me.GridColumn52.Visible = true
        Me.GridColumn52.VisibleIndex = 0
        Me.GridColumn52.Width = 169
        '
        'GridColumn53
        '
        Me.GridColumn53.Caption = "Description"
        Me.GridColumn53.Name = "GridColumn53"
        Me.GridColumn53.Visible = true
        Me.GridColumn53.VisibleIndex = 1
        Me.GridColumn53.Width = 169
        '
        'GridColumn54
        '
        Me.GridColumn54.Caption = "Value"
        Me.GridColumn54.Name = "GridColumn54"
        Me.GridColumn54.Visible = true
        Me.GridColumn54.VisibleIndex = 2
        Me.GridColumn54.Width = 169
        '
        'GridColumn55
        '
        Me.GridColumn55.Caption = "Include?"
        Me.GridColumn55.Name = "GridColumn55"
        Me.GridColumn55.Visible = true
        Me.GridColumn55.VisibleIndex = 3
        Me.GridColumn55.Width = 169
        '
        'GridColumn56
        '
        Me.GridColumn56.Caption = "Action"
        Me.GridColumn56.Name = "GridColumn56"
        Me.GridColumn56.OptionsColumn.FixedWidth = true
        Me.GridColumn56.Visible = true
        Me.GridColumn56.VisibleIndex = 4
        Me.GridColumn56.Width = 100
        '
        'SimpleButton22
        '
        Me.SimpleButton22.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton22.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton22.Location = New System.Drawing.Point(17, 16)
        Me.SimpleButton22.Name = "SimpleButton22"
        Me.SimpleButton22.Size = New System.Drawing.Size(233, 33)
        Me.SimpleButton22.TabIndex = 3
        Me.SimpleButton22.Text = "Add Specific Items"
        '
        'XtraTabPage31
        '
        Me.XtraTabPage31.Controls.Add(Me.GridControl16)
        Me.XtraTabPage31.Controls.Add(Me.SimpleButton23)
        Me.XtraTabPage31.Name = "XtraTabPage31"
        Me.XtraTabPage31.Size = New System.Drawing.Size(934, 455)
        Me.XtraTabPage31.Text = "Cash"
        '
        'GridControl16
        '
        Me.GridControl16.Location = New System.Drawing.Point(17, 55)
        Me.GridControl16.MainView = Me.GridView16
        Me.GridControl16.Name = "GridControl16"
        Me.GridControl16.Size = New System.Drawing.Size(900, 384)
        Me.GridControl16.TabIndex = 5
        Me.GridControl16.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView16})
        '
        'GridView16
        '
        Me.GridView16.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn57, Me.GridColumn58, Me.GridColumn59, Me.GridColumn60, Me.GridColumn61})
        Me.GridView16.GridControl = Me.GridControl16
        Me.GridView16.Name = "GridView16"
        Me.GridView16.OptionsView.ShowGroupPanel = false
        Me.GridView16.OptionsView.ShowIndicator = false
        Me.GridView16.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn57
        '
        Me.GridColumn57.Caption = "Asset Type"
        Me.GridColumn57.Name = "GridColumn57"
        Me.GridColumn57.Visible = true
        Me.GridColumn57.VisibleIndex = 0
        Me.GridColumn57.Width = 175
        '
        'GridColumn58
        '
        Me.GridColumn58.Caption = "Description"
        Me.GridColumn58.Name = "GridColumn58"
        Me.GridColumn58.Visible = true
        Me.GridColumn58.VisibleIndex = 1
        Me.GridColumn58.Width = 342
        '
        'GridColumn59
        '
        Me.GridColumn59.Caption = "Value"
        Me.GridColumn59.Name = "GridColumn59"
        Me.GridColumn59.Visible = true
        Me.GridColumn59.VisibleIndex = 2
        Me.GridColumn59.Width = 245
        '
        'GridColumn60
        '
        Me.GridColumn60.Caption = "Include?"
        Me.GridColumn60.Name = "GridColumn60"
        Me.GridColumn60.Visible = true
        Me.GridColumn60.VisibleIndex = 3
        Me.GridColumn60.Width = 121
        '
        'GridColumn61
        '
        Me.GridColumn61.Caption = "Action"
        Me.GridColumn61.Name = "GridColumn61"
        Me.GridColumn61.OptionsColumn.FixedWidth = true
        Me.GridColumn61.Visible = true
        Me.GridColumn61.VisibleIndex = 4
        Me.GridColumn61.Width = 100
        '
        'SimpleButton23
        '
        Me.SimpleButton23.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton23.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton23.Location = New System.Drawing.Point(17, 16)
        Me.SimpleButton23.Name = "SimpleButton23"
        Me.SimpleButton23.Size = New System.Drawing.Size(233, 33)
        Me.SimpleButton23.TabIndex = 4
        Me.SimpleButton23.Text = "Add Cash"
        '
        'XtraTabPage26
        '
        Me.XtraTabPage26.Controls.Add(Me.LayoutControl23)
        Me.XtraTabPage26.Name = "XtraTabPage26"
        Me.XtraTabPage26.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage26.Text = "Purpose of Fund"
        '
        'LayoutControl23
        '
        Me.LayoutControl23.Controls.Add(Me.CheckEdit143)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit144)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit145)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit146)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit147)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit148)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit149)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit150)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit151)
        Me.LayoutControl23.Controls.Add(Me.CheckEdit152)
        Me.LayoutControl23.Location = New System.Drawing.Point(16, 8)
        Me.LayoutControl23.Name = "LayoutControl23"
        Me.LayoutControl23.Root = Me.LayoutControlGroup22
        Me.LayoutControl23.Size = New System.Drawing.Size(926, 534)
        Me.LayoutControl23.TabIndex = 1
        Me.LayoutControl23.Text = "LayoutControl23"
        '
        'CheckEdit143
        '
        Me.CheckEdit143.Location = New System.Drawing.Point(12, 35)
        Me.CheckEdit143.Name = "CheckEdit143"
        Me.CheckEdit143.Properties.Caption = "My/our wishes to be specifically set out during our/my life for the benefit of th"& _ 
    "e family"
        Me.CheckEdit143.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit143.StyleController = Me.LayoutControl23
        Me.CheckEdit143.TabIndex = 4
        '
        'CheckEdit144
        '
        Me.CheckEdit144.Location = New System.Drawing.Point(12, 83)
        Me.CheckEdit144.Name = "CheckEdit144"
        Me.CheckEdit144.Properties.Caption = "I/We would like the Trustees to make sensible decisions for the welfare of the se"& _ 
    "ttlor(s)"
        Me.CheckEdit144.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit144.StyleController = Me.LayoutControl23
        Me.CheckEdit144.TabIndex = 5
        '
        'CheckEdit145
        '
        Me.CheckEdit145.Location = New System.Drawing.Point(12, 131)
        Me.CheckEdit145.Name = "CheckEdit145"
        Me.CheckEdit145.Properties.Caption = "To allow the Trustees to assist in taking proper and responsible steps if the set"& _ 
    "tlor(s) are incapacitated"
        Me.CheckEdit145.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit145.StyleController = Me.LayoutControl23
        Me.CheckEdit145.TabIndex = 6
        '
        'CheckEdit146
        '
        Me.CheckEdit146.Location = New System.Drawing.Point(12, 179)
        Me.CheckEdit146.Name = "CheckEdit146"
        Me.CheckEdit146.Properties.Caption = "To allow the Trustees to manage the property if the Settlor(s) are incapacitated"
        Me.CheckEdit146.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit146.StyleController = Me.LayoutControl23
        Me.CheckEdit146.TabIndex = 7
        '
        'CheckEdit147
        '
        Me.CheckEdit147.Location = New System.Drawing.Point(12, 227)
        Me.CheckEdit147.Name = "CheckEdit147"
        Me.CheckEdit147.Properties.Caption = "To prevent the assets of the Trust falling under the Court of Protection"
        Me.CheckEdit147.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit147.StyleController = Me.LayoutControl23
        Me.CheckEdit147.TabIndex = 8
        '
        'CheckEdit148
        '
        Me.CheckEdit148.Location = New System.Drawing.Point(12, 275)
        Me.CheckEdit148.Name = "CheckEdit148"
        Me.CheckEdit148.Properties.Caption = "To give some protection against sideways dis-inheritance"
        Me.CheckEdit148.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit148.StyleController = Me.LayoutControl23
        Me.CheckEdit148.TabIndex = 9
        '
        'CheckEdit149
        '
        Me.CheckEdit149.Location = New System.Drawing.Point(12, 323)
        Me.CheckEdit149.Name = "CheckEdit149"
        Me.CheckEdit149.Properties.Caption = "To allow the Trustees to have flexibility to loan money from the Trust"
        Me.CheckEdit149.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit149.StyleController = Me.LayoutControl23
        Me.CheckEdit149.TabIndex = 10
        '
        'CheckEdit150
        '
        Me.CheckEdit150.Location = New System.Drawing.Point(12, 371)
        Me.CheckEdit150.Name = "CheckEdit150"
        Me.CheckEdit150.Properties.Caption = "To allow an income to be taken from the funds if the property is sold"
        Me.CheckEdit150.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit150.StyleController = Me.LayoutControl23
        Me.CheckEdit150.TabIndex = 11
        '
        'CheckEdit151
        '
        Me.CheckEdit151.Location = New System.Drawing.Point(12, 419)
        Me.CheckEdit151.Name = "CheckEdit151"
        Me.CheckEdit151.Properties.Caption = "To reduce probate costs upon my/our death"
        Me.CheckEdit151.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit151.StyleController = Me.LayoutControl23
        Me.CheckEdit151.TabIndex = 12
        '
        'CheckEdit152
        '
        Me.CheckEdit152.Location = New System.Drawing.Point(12, 467)
        Me.CheckEdit152.Name = "CheckEdit152"
        Me.CheckEdit152.Properties.Caption = "To prevent your asset from being accessed by creditors"
        Me.CheckEdit152.Size = New System.Drawing.Size(902, 44)
        Me.CheckEdit152.StyleController = Me.LayoutControl23
        Me.CheckEdit152.TabIndex = 13
        '
        'LayoutControlGroup22
        '
        Me.LayoutControlGroup22.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup22.GroupBordersVisible = false
        Me.LayoutControlGroup22.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem39, Me.SimpleLabelItem9, Me.LayoutControlItem186, Me.LayoutControlItem187, Me.LayoutControlItem188, Me.LayoutControlItem189, Me.LayoutControlItem190, Me.LayoutControlItem191, Me.LayoutControlItem192, Me.LayoutControlItem193, Me.LayoutControlItem194, Me.LayoutControlItem195})
        Me.LayoutControlGroup22.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup22.Size = New System.Drawing.Size(926, 534)
        Me.LayoutControlGroup22.TextVisible = false
        '
        'EmptySpaceItem39
        '
        Me.EmptySpaceItem39.AllowHotTrack = false
        Me.EmptySpaceItem39.Location = New System.Drawing.Point(0, 503)
        Me.EmptySpaceItem39.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem39.Size = New System.Drawing.Size(906, 11)
        Me.EmptySpaceItem39.TextSize = New System.Drawing.Size(0, 0)
        '
        'SimpleLabelItem9
        '
        Me.SimpleLabelItem9.AllowHotTrack = false
        Me.SimpleLabelItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.SimpleLabelItem9.AppearanceItemCaption.Options.UseFont = true
        Me.SimpleLabelItem9.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem9.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem9.Size = New System.Drawing.Size(906, 23)
        Me.SimpleLabelItem9.Text = "Tick which statements are applicable"
        Me.SimpleLabelItem9.TextSize = New System.Drawing.Size(280, 19)
        '
        'LayoutControlItem186
        '
        Me.LayoutControlItem186.Control = Me.CheckEdit143
        Me.LayoutControlItem186.Location = New System.Drawing.Point(0, 23)
        Me.LayoutControlItem186.Name = "LayoutControlItem17"
        Me.LayoutControlItem186.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem186.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem186.TextVisible = false
        '
        'LayoutControlItem187
        '
        Me.LayoutControlItem187.Control = Me.CheckEdit144
        Me.LayoutControlItem187.Location = New System.Drawing.Point(0, 71)
        Me.LayoutControlItem187.Name = "LayoutControlItem18"
        Me.LayoutControlItem187.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem187.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem187.TextVisible = false
        '
        'LayoutControlItem188
        '
        Me.LayoutControlItem188.Control = Me.CheckEdit145
        Me.LayoutControlItem188.Location = New System.Drawing.Point(0, 119)
        Me.LayoutControlItem188.Name = "LayoutControlItem19"
        Me.LayoutControlItem188.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem188.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem188.TextVisible = false
        '
        'LayoutControlItem189
        '
        Me.LayoutControlItem189.Control = Me.CheckEdit146
        Me.LayoutControlItem189.Location = New System.Drawing.Point(0, 167)
        Me.LayoutControlItem189.Name = "LayoutControlItem20"
        Me.LayoutControlItem189.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem189.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem189.TextVisible = false
        '
        'LayoutControlItem190
        '
        Me.LayoutControlItem190.Control = Me.CheckEdit147
        Me.LayoutControlItem190.Location = New System.Drawing.Point(0, 215)
        Me.LayoutControlItem190.Name = "LayoutControlItem21"
        Me.LayoutControlItem190.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem190.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem190.TextVisible = false
        '
        'LayoutControlItem191
        '
        Me.LayoutControlItem191.Control = Me.CheckEdit148
        Me.LayoutControlItem191.Location = New System.Drawing.Point(0, 263)
        Me.LayoutControlItem191.Name = "LayoutControlItem22"
        Me.LayoutControlItem191.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem191.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem191.TextVisible = false
        '
        'LayoutControlItem192
        '
        Me.LayoutControlItem192.Control = Me.CheckEdit149
        Me.LayoutControlItem192.Location = New System.Drawing.Point(0, 311)
        Me.LayoutControlItem192.Name = "LayoutControlItem23"
        Me.LayoutControlItem192.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem192.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem192.TextVisible = false
        '
        'LayoutControlItem193
        '
        Me.LayoutControlItem193.Control = Me.CheckEdit150
        Me.LayoutControlItem193.Location = New System.Drawing.Point(0, 359)
        Me.LayoutControlItem193.Name = "LayoutControlItem24"
        Me.LayoutControlItem193.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem193.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem193.TextVisible = false
        '
        'LayoutControlItem194
        '
        Me.LayoutControlItem194.Control = Me.CheckEdit151
        Me.LayoutControlItem194.Location = New System.Drawing.Point(0, 407)
        Me.LayoutControlItem194.Name = "LayoutControlItem25"
        Me.LayoutControlItem194.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem194.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem194.TextVisible = false
        '
        'LayoutControlItem195
        '
        Me.LayoutControlItem195.Control = Me.CheckEdit152
        Me.LayoutControlItem195.Location = New System.Drawing.Point(0, 455)
        Me.LayoutControlItem195.Name = "LayoutControlItem26"
        Me.LayoutControlItem195.Size = New System.Drawing.Size(906, 48)
        Me.LayoutControlItem195.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem195.TextVisible = false
        '
        'XtraTabPage27
        '
        Me.XtraTabPage27.Controls.Add(Me.GridControl17)
        Me.XtraTabPage27.Name = "XtraTabPage27"
        Me.XtraTabPage27.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage27.Text = "Consultant Compliance"
        '
        'GridControl17
        '
        Me.GridControl17.Location = New System.Drawing.Point(27, 30)
        Me.GridControl17.MainView = Me.GridView17
        Me.GridControl17.Name = "GridControl17"
        Me.GridControl17.Size = New System.Drawing.Size(957, 523)
        Me.GridControl17.TabIndex = 1
        Me.GridControl17.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView17})
        '
        'GridView17
        '
        Me.GridView17.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn62, Me.GridColumn63, Me.GridColumn64})
        Me.GridView17.GridControl = Me.GridControl17
        Me.GridView17.Name = "GridView17"
        Me.GridView17.OptionsView.ShowGroupPanel = false
        Me.GridView17.OptionsView.ShowIndicator = false
        Me.GridView17.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn62
        '
        Me.GridColumn62.Caption = "Question"
        Me.GridColumn62.Name = "GridColumn62"
        Me.GridColumn62.Visible = true
        Me.GridColumn62.VisibleIndex = 0
        Me.GridColumn62.Width = 533
        '
        'GridColumn63
        '
        Me.GridColumn63.Caption = "Answer"
        Me.GridColumn63.Name = "GridColumn63"
        Me.GridColumn63.Visible = true
        Me.GridColumn63.VisibleIndex = 1
        Me.GridColumn63.Width = 302
        '
        'GridColumn64
        '
        Me.GridColumn64.Caption = "Action"
        Me.GridColumn64.Name = "GridColumn64"
        Me.GridColumn64.Visible = true
        Me.GridColumn64.VisibleIndex = 2
        Me.GridColumn64.Width = 185
        '
        'XtraTabPage28
        '
        Me.XtraTabPage28.Controls.Add(Me.PanelControl2)
        Me.XtraTabPage28.Controls.Add(Me.GridControl18)
        Me.XtraTabPage28.Name = "XtraTabPage28"
        Me.XtraTabPage28.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage28.Text = "Attendance Notes"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl60)
        Me.PanelControl2.Controls.Add(Me.MemoEdit17)
        Me.PanelControl2.Location = New System.Drawing.Point(655, 22)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(356, 515)
        Me.PanelControl2.TabIndex = 5
        '
        'LabelControl60
        '
        Me.LabelControl60.Appearance.BackColor = System.Drawing.Color.DimGray
        Me.LabelControl60.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl60.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl60.Appearance.Options.UseBackColor = true
        Me.LabelControl60.Appearance.Options.UseFont = true
        Me.LabelControl60.Appearance.Options.UseForeColor = true
        Me.LabelControl60.Appearance.Options.UseTextOptions = true
        Me.LabelControl60.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl60.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl60.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl60.Location = New System.Drawing.Point(2, 2)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(352, 38)
        Me.LabelControl60.TabIndex = 2
        Me.LabelControl60.Text = "Supplementary Information"
        '
        'MemoEdit17
        '
        Me.MemoEdit17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit17.Location = New System.Drawing.Point(2, 2)
        Me.MemoEdit17.Name = "MemoEdit17"
        Me.MemoEdit17.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MemoEdit17.Size = New System.Drawing.Size(352, 511)
        Me.MemoEdit17.TabIndex = 3
        '
        'GridControl18
        '
        Me.GridControl18.Location = New System.Drawing.Point(32, 22)
        Me.GridControl18.MainView = Me.GridView18
        Me.GridControl18.Name = "GridControl18"
        Me.GridControl18.Size = New System.Drawing.Size(600, 515)
        Me.GridControl18.TabIndex = 2
        Me.GridControl18.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView18})
        '
        'GridView18
        '
        Me.GridView18.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn65, Me.GridColumn66, Me.GridColumn67})
        Me.GridView18.GridControl = Me.GridControl18
        Me.GridView18.Name = "GridView18"
        Me.GridView18.OptionsView.ShowGroupPanel = false
        Me.GridView18.OptionsView.ShowIndicator = false
        Me.GridView18.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView18.OptionsView.ShowViewCaption = true
        Me.GridView18.ViewCaption = "Attendance Notes"
        '
        'GridColumn65
        '
        Me.GridColumn65.Caption = "Question"
        Me.GridColumn65.Name = "GridColumn65"
        Me.GridColumn65.Visible = true
        Me.GridColumn65.VisibleIndex = 0
        Me.GridColumn65.Width = 533
        '
        'GridColumn66
        '
        Me.GridColumn66.Caption = "Answer"
        Me.GridColumn66.Name = "GridColumn66"
        Me.GridColumn66.Visible = true
        Me.GridColumn66.VisibleIndex = 1
        Me.GridColumn66.Width = 302
        '
        'GridColumn67
        '
        Me.GridColumn67.Caption = "Action"
        Me.GridColumn67.Name = "GridColumn67"
        Me.GridColumn67.Visible = true
        Me.GridColumn67.VisibleIndex = 2
        Me.GridColumn67.Width = 185
        '
        'NavigationPageDigitalSignatures
        '
        Me.NavigationPageDigitalSignatures.Caption = "   Digital Signatures"
        Me.NavigationPageDigitalSignatures.ImageOptions.Image = CType(resources.GetObject("NavigationPageDigitalSignatures.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageDigitalSignatures.Name = "NavigationPageDigitalSignatures"
        Me.NavigationPageDigitalSignatures.Size = New System.Drawing.Size(1026, 602)
        '
        'NavigationPageProbatePlan
        '
        Me.NavigationPageProbatePlan.Caption = "   Probate Plan"
        Me.NavigationPageProbatePlan.Controls.Add(Me.XtraTabControl6)
        Me.NavigationPageProbatePlan.ImageOptions.Image = CType(resources.GetObject("NavigationPageProbatePlan.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageProbatePlan.Name = "NavigationPageProbatePlan"
        Me.NavigationPageProbatePlan.Size = New System.Drawing.Size(1026, 602)
        '
        'XtraTabControl6
        '
        Me.XtraTabControl6.Location = New System.Drawing.Point(3, 13)
        Me.XtraTabControl6.Name = "XtraTabControl6"
        Me.XtraTabControl6.SelectedTabPage = Me.XtraTabPage32
        Me.XtraTabControl6.Size = New System.Drawing.Size(998, 576)
        Me.XtraTabControl6.TabIndex = 1
        Me.XtraTabControl6.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage32, Me.XtraTabPage33})
        '
        'XtraTabPage32
        '
        Me.XtraTabPage32.Controls.Add(Me.CheckEdit153)
        Me.XtraTabPage32.Controls.Add(Me.CheckEdit154)
        Me.XtraTabPage32.Controls.Add(Me.LabelControl61)
        Me.XtraTabPage32.Controls.Add(Me.LayoutControl24)
        Me.XtraTabPage32.Controls.Add(Me.LayoutControl25)
        Me.XtraTabPage32.ImageOptions.Image = Global.Fortis_Fact_Find.My.Resources.Resources.info_32x32
        Me.XtraTabPage32.Name = "XtraTabPage32"
        Me.XtraTabPage32.Size = New System.Drawing.Size(996, 524)
        Me.XtraTabPage32.Text = "Client Information"
        '
        'CheckEdit153
        '
        Me.CheckEdit153.Location = New System.Drawing.Point(567, 480)
        Me.CheckEdit153.Name = "CheckEdit153"
        Me.CheckEdit153.Properties.Caption = "NO"
        Me.CheckEdit153.Size = New System.Drawing.Size(96, 44)
        Me.CheckEdit153.TabIndex = 4
        '
        'CheckEdit154
        '
        Me.CheckEdit154.Location = New System.Drawing.Point(465, 478)
        Me.CheckEdit154.Name = "CheckEdit154"
        Me.CheckEdit154.Properties.Caption = "YES"
        Me.CheckEdit154.Size = New System.Drawing.Size(96, 44)
        Me.CheckEdit154.TabIndex = 4
        '
        'LabelControl61
        '
        Me.LabelControl61.Appearance.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.LabelControl61.Appearance.Options.UseFont = true
        Me.LabelControl61.Location = New System.Drawing.Point(45, 491)
        Me.LabelControl61.Name = "LabelControl61"
        Me.LabelControl61.Size = New System.Drawing.Size(377, 19)
        Me.LabelControl61.TabIndex = 3
        Me.LabelControl61.Text = "Confirm that you wish to purchase a Probate Plan"
        '
        'LayoutControl24
        '
        Me.LayoutControl24.Controls.Add(Me.TextEdit73)
        Me.LayoutControl24.Controls.Add(Me.DateEdit11)
        Me.LayoutControl24.Controls.Add(Me.MemoEdit18)
        Me.LayoutControl24.Controls.Add(Me.TextEdit74)
        Me.LayoutControl24.Location = New System.Drawing.Point(30, 11)
        Me.LayoutControl24.Name = "LayoutControl24"
        Me.LayoutControl24.Root = Me.LayoutControlGroup23
        Me.LayoutControl24.Size = New System.Drawing.Size(441, 464)
        Me.LayoutControl24.TabIndex = 2
        Me.LayoutControl24.Text = "LayoutControl24"
        '
        'TextEdit73
        '
        Me.TextEdit73.Location = New System.Drawing.Point(129, 43)
        Me.TextEdit73.Name = "TextEdit73"
        Me.TextEdit73.Size = New System.Drawing.Size(288, 38)
        Me.TextEdit73.StyleController = Me.LayoutControl24
        Me.TextEdit73.TabIndex = 4
        '
        'DateEdit11
        '
        Me.DateEdit11.EditValue = Nothing
        Me.DateEdit11.Location = New System.Drawing.Point(129, 85)
        Me.DateEdit11.Name = "DateEdit11"
        Me.DateEdit11.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit11.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit11.Size = New System.Drawing.Size(288, 38)
        Me.DateEdit11.StyleController = Me.LayoutControl24
        Me.DateEdit11.TabIndex = 5
        '
        'MemoEdit18
        '
        Me.MemoEdit18.Location = New System.Drawing.Point(129, 127)
        Me.MemoEdit18.Name = "MemoEdit18"
        Me.MemoEdit18.Size = New System.Drawing.Size(288, 271)
        Me.MemoEdit18.StyleController = Me.LayoutControl24
        Me.MemoEdit18.TabIndex = 6
        '
        'TextEdit74
        '
        Me.TextEdit74.Location = New System.Drawing.Point(109, 402)
        Me.TextEdit74.Name = "TextEdit74"
        Me.TextEdit74.Size = New System.Drawing.Size(308, 38)
        Me.TextEdit74.StyleController = Me.LayoutControl24
        Me.TextEdit74.TabIndex = 8
        '
        'LayoutControlGroup23
        '
        Me.LayoutControlGroup23.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup23.GroupBordersVisible = false
        Me.LayoutControlGroup23.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup24})
        Me.LayoutControlGroup23.Name = "Root"
        Me.LayoutControlGroup23.Size = New System.Drawing.Size(441, 464)
        Me.LayoutControlGroup23.TextVisible = false
        '
        'LayoutControlGroup24
        '
        Me.LayoutControlGroup24.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem196, Me.LayoutControlItem197, Me.LayoutControlItem198, Me.LayoutControlItem199, Me.EmptySpaceItem40})
        Me.LayoutControlGroup24.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup24.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup24.Size = New System.Drawing.Size(421, 444)
        Me.LayoutControlGroup24.Text = "Client 1 "
        '
        'LayoutControlItem196
        '
        Me.LayoutControlItem196.Control = Me.TextEdit73
        Me.LayoutControlItem196.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem196.Name = "LayoutControlItem1"
        Me.LayoutControlItem196.Size = New System.Drawing.Size(397, 42)
        Me.LayoutControlItem196.Text = "Name"
        Me.LayoutControlItem196.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem196.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem196.TextToControlDistance = 5
        '
        'LayoutControlItem197
        '
        Me.LayoutControlItem197.Control = Me.DateEdit11
        Me.LayoutControlItem197.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem197.Name = "LayoutControlItem2"
        Me.LayoutControlItem197.Size = New System.Drawing.Size(397, 42)
        Me.LayoutControlItem197.Text = "Date of Birth"
        Me.LayoutControlItem197.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem197.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem197.TextToControlDistance = 5
        '
        'LayoutControlItem198
        '
        Me.LayoutControlItem198.Control = Me.MemoEdit18
        Me.LayoutControlItem198.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem198.Name = "LayoutControlItem3"
        Me.LayoutControlItem198.Size = New System.Drawing.Size(397, 275)
        Me.LayoutControlItem198.Text = "Address"
        Me.LayoutControlItem198.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem198.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem198.TextToControlDistance = 5
        '
        'LayoutControlItem199
        '
        Me.LayoutControlItem199.Control = Me.TextEdit74
        Me.LayoutControlItem199.Location = New System.Drawing.Point(85, 359)
        Me.LayoutControlItem199.Name = "LayoutControlItem5"
        Me.LayoutControlItem199.Size = New System.Drawing.Size(312, 42)
        Me.LayoutControlItem199.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem199.TextVisible = false
        '
        'EmptySpaceItem40
        '
        Me.EmptySpaceItem40.AllowHotTrack = false
        Me.EmptySpaceItem40.Location = New System.Drawing.Point(0, 359)
        Me.EmptySpaceItem40.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem40.Size = New System.Drawing.Size(85, 42)
        Me.EmptySpaceItem40.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControl25
        '
        Me.LayoutControl25.Controls.Add(Me.TextEdit75)
        Me.LayoutControl25.Controls.Add(Me.DateEdit12)
        Me.LayoutControl25.Controls.Add(Me.MemoEdit19)
        Me.LayoutControl25.Controls.Add(Me.TextEdit77)
        Me.LayoutControl25.Location = New System.Drawing.Point(508, 11)
        Me.LayoutControl25.Name = "LayoutControl25"
        Me.LayoutControl25.Root = Me.LayoutControlGroup25
        Me.LayoutControl25.Size = New System.Drawing.Size(462, 464)
        Me.LayoutControl25.TabIndex = 1
        Me.LayoutControl25.Text = "LayoutControl1"
        '
        'TextEdit75
        '
        Me.TextEdit75.Location = New System.Drawing.Point(129, 43)
        Me.TextEdit75.Name = "TextEdit75"
        Me.TextEdit75.Size = New System.Drawing.Size(309, 38)
        Me.TextEdit75.StyleController = Me.LayoutControl25
        Me.TextEdit75.TabIndex = 4
        '
        'DateEdit12
        '
        Me.DateEdit12.EditValue = Nothing
        Me.DateEdit12.Location = New System.Drawing.Point(129, 85)
        Me.DateEdit12.Name = "DateEdit12"
        Me.DateEdit12.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit12.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit12.Size = New System.Drawing.Size(309, 38)
        Me.DateEdit12.StyleController = Me.LayoutControl25
        Me.DateEdit12.TabIndex = 5
        '
        'MemoEdit19
        '
        Me.MemoEdit19.Location = New System.Drawing.Point(129, 127)
        Me.MemoEdit19.Name = "MemoEdit19"
        Me.MemoEdit19.Size = New System.Drawing.Size(309, 271)
        Me.MemoEdit19.StyleController = Me.LayoutControl25
        Me.MemoEdit19.TabIndex = 6
        '
        'TextEdit77
        '
        Me.TextEdit77.Location = New System.Drawing.Point(113, 402)
        Me.TextEdit77.Name = "TextEdit77"
        Me.TextEdit77.Size = New System.Drawing.Size(325, 38)
        Me.TextEdit77.StyleController = Me.LayoutControl25
        Me.TextEdit77.TabIndex = 8
        '
        'LayoutControlGroup25
        '
        Me.LayoutControlGroup25.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup25.GroupBordersVisible = false
        Me.LayoutControlGroup25.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup26})
        Me.LayoutControlGroup25.Name = "Root"
        Me.LayoutControlGroup25.Size = New System.Drawing.Size(462, 464)
        Me.LayoutControlGroup25.TextVisible = false
        '
        'LayoutControlGroup26
        '
        Me.LayoutControlGroup26.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem200, Me.LayoutControlItem201, Me.LayoutControlItem202, Me.LayoutControlItem203, Me.EmptySpaceItem41})
        Me.LayoutControlGroup26.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup26.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup26.Size = New System.Drawing.Size(442, 444)
        Me.LayoutControlGroup26.Text = "Client 2"
        '
        'LayoutControlItem200
        '
        Me.LayoutControlItem200.Control = Me.TextEdit75
        Me.LayoutControlItem200.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem200.Name = "LayoutControlItem1"
        Me.LayoutControlItem200.Size = New System.Drawing.Size(418, 42)
        Me.LayoutControlItem200.Text = "Name"
        Me.LayoutControlItem200.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem200.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem200.TextToControlDistance = 5
        '
        'LayoutControlItem201
        '
        Me.LayoutControlItem201.Control = Me.DateEdit12
        Me.LayoutControlItem201.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem201.Name = "LayoutControlItem2"
        Me.LayoutControlItem201.Size = New System.Drawing.Size(418, 42)
        Me.LayoutControlItem201.Text = "Date of Birth"
        Me.LayoutControlItem201.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem201.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem201.TextToControlDistance = 5
        '
        'LayoutControlItem202
        '
        Me.LayoutControlItem202.Control = Me.MemoEdit19
        Me.LayoutControlItem202.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem202.Name = "LayoutControlItem3"
        Me.LayoutControlItem202.Size = New System.Drawing.Size(418, 275)
        Me.LayoutControlItem202.Text = "Address"
        Me.LayoutControlItem202.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem202.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem202.TextToControlDistance = 5
        '
        'LayoutControlItem203
        '
        Me.LayoutControlItem203.Control = Me.TextEdit77
        Me.LayoutControlItem203.Location = New System.Drawing.Point(89, 359)
        Me.LayoutControlItem203.Name = "LayoutControlItem5"
        Me.LayoutControlItem203.Size = New System.Drawing.Size(329, 42)
        Me.LayoutControlItem203.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem203.TextVisible = false
        '
        'EmptySpaceItem41
        '
        Me.EmptySpaceItem41.AllowHotTrack = false
        Me.EmptySpaceItem41.Location = New System.Drawing.Point(0, 359)
        Me.EmptySpaceItem41.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem41.Size = New System.Drawing.Size(89, 42)
        Me.EmptySpaceItem41.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage33
        '
        Me.XtraTabPage33.Controls.Add(Me.LayoutControl26)
        Me.XtraTabPage33.ImageOptions.Image = CType(resources.GetObject("XtraTabPage33.ImageOptions.Image"),System.Drawing.Image)
        Me.XtraTabPage33.Name = "XtraTabPage33"
        Me.XtraTabPage33.Size = New System.Drawing.Size(996, 524)
        Me.XtraTabPage33.Text = "Attendance Notes"
        '
        'LayoutControl26
        '
        Me.LayoutControl26.Controls.Add(Me.MemoEdit20)
        Me.LayoutControl26.Location = New System.Drawing.Point(15, 17)
        Me.LayoutControl26.Name = "LayoutControl26"
        Me.LayoutControl26.Root = Me.LayoutControlGroup27
        Me.LayoutControl26.Size = New System.Drawing.Size(951, 468)
        Me.LayoutControl26.TabIndex = 0
        Me.LayoutControl26.Text = "LayoutControl3"
        '
        'MemoEdit20
        '
        Me.MemoEdit20.Location = New System.Drawing.Point(12, 33)
        Me.MemoEdit20.Name = "MemoEdit20"
        Me.MemoEdit20.Size = New System.Drawing.Size(927, 423)
        Me.MemoEdit20.StyleController = Me.LayoutControl26
        Me.MemoEdit20.TabIndex = 4
        '
        'LayoutControlGroup27
        '
        Me.LayoutControlGroup27.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup27.GroupBordersVisible = false
        Me.LayoutControlGroup27.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem204})
        Me.LayoutControlGroup27.Name = "Root"
        Me.LayoutControlGroup27.Size = New System.Drawing.Size(951, 468)
        Me.LayoutControlGroup27.TextVisible = false
        '
        'LayoutControlItem204
        '
        Me.LayoutControlItem204.AppearanceItemCaption.Font = New System.Drawing.Font("Arial", 12!)
        Me.LayoutControlItem204.AppearanceItemCaption.Options.UseFont = true
        Me.LayoutControlItem204.Control = Me.MemoEdit20
        Me.LayoutControlItem204.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem204.Name = "LayoutControlItem5"
        Me.LayoutControlItem204.Size = New System.Drawing.Size(931, 448)
        Me.LayoutControlItem204.Text = "Attendance Notes"
        Me.LayoutControlItem204.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem204.TextSize = New System.Drawing.Size(124, 18)
        '
        'NavigationPageProbateAssessment
        '
        Me.NavigationPageProbateAssessment.Caption = "   Probate Assessment"
        Me.NavigationPageProbateAssessment.Controls.Add(Me.XtraTabControl7)
        Me.NavigationPageProbateAssessment.ImageOptions.Image = CType(resources.GetObject("NavigationPageProbateAssessment.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageProbateAssessment.Name = "NavigationPageProbateAssessment"
        Me.NavigationPageProbateAssessment.Size = New System.Drawing.Size(1026, 602)
        '
        'XtraTabControl7
        '
        Me.XtraTabControl7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl7.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl7.Name = "XtraTabControl7"
        Me.XtraTabControl7.SelectedTabPage = Me.XtraTabPage42
        Me.XtraTabControl7.Size = New System.Drawing.Size(1026, 602)
        Me.XtraTabControl7.TabIndex = 0
        Me.XtraTabControl7.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage34, Me.XtraTabPage35, Me.XtraTabPage36, Me.XtraTabPage37, Me.XtraTabPage38, Me.XtraTabPage39, Me.XtraTabPage40, Me.XtraTabPage41, Me.XtraTabPage42, Me.XtraTabPage43, Me.XtraTabPage44})
        '
        'XtraTabPage42
        '
        Me.XtraTabPage42.Controls.Add(Me.GridControl26)
        Me.XtraTabPage42.Controls.Add(Me.SimpleButton33)
        Me.XtraTabPage42.Controls.Add(Me.CheckEdit160)
        Me.XtraTabPage42.Controls.Add(Me.CheckEdit159)
        Me.XtraTabPage42.Controls.Add(Me.LabelControl62)
        Me.XtraTabPage42.Name = "XtraTabPage42"
        Me.XtraTabPage42.Size = New System.Drawing.Size(1024, 566)
        Me.XtraTabPage42.Text = "Gifts During Lifetime"
        '
        'GridControl26
        '
        Me.GridControl26.Location = New System.Drawing.Point(19, 114)
        Me.GridControl26.MainView = Me.GridView26
        Me.GridControl26.Name = "GridControl26"
        Me.GridControl26.Size = New System.Drawing.Size(974, 425)
        Me.GridControl26.TabIndex = 32
        Me.GridControl26.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView26})
        '
        'GridView26
        '
        Me.GridView26.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn101, Me.GridColumn102, Me.GridColumn103, Me.GridColumn104, Me.GridColumn105})
        Me.GridView26.GridControl = Me.GridControl26
        Me.GridView26.Name = "GridView26"
        Me.GridView26.OptionsView.ShowGroupPanel = false
        Me.GridView26.OptionsView.ShowIndicator = false
        Me.GridView26.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn101
        '
        Me.GridColumn101.Caption = "Name"
        Me.GridColumn101.Name = "GridColumn101"
        Me.GridColumn101.Visible = true
        Me.GridColumn101.VisibleIndex = 0
        Me.GridColumn101.Width = 255
        '
        'GridColumn102
        '
        Me.GridColumn102.Caption = "Address"
        Me.GridColumn102.Name = "GridColumn102"
        Me.GridColumn102.Visible = true
        Me.GridColumn102.VisibleIndex = 1
        Me.GridColumn102.Width = 278
        '
        'GridColumn103
        '
        Me.GridColumn103.Caption = "Item"
        Me.GridColumn103.Name = "GridColumn103"
        Me.GridColumn103.Visible = true
        Me.GridColumn103.VisibleIndex = 3
        Me.GridColumn103.Width = 181
        '
        'GridColumn104
        '
        Me.GridColumn104.Caption = "Action"
        Me.GridColumn104.Name = "GridColumn104"
        Me.GridColumn104.Visible = true
        Me.GridColumn104.VisibleIndex = 4
        Me.GridColumn104.Width = 101
        '
        'GridColumn105
        '
        Me.GridColumn105.Caption = "Relationship"
        Me.GridColumn105.Name = "GridColumn105"
        Me.GridColumn105.Visible = true
        Me.GridColumn105.VisibleIndex = 2
        Me.GridColumn105.Width = 157
        '
        'SimpleButton33
        '
        Me.SimpleButton33.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton33.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton33.Location = New System.Drawing.Point(19, 63)
        Me.SimpleButton33.Name = "SimpleButton33"
        Me.SimpleButton33.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton33.TabIndex = 27
        Me.SimpleButton33.Text = "Add Gift Given"
        '
        'CheckEdit160
        '
        Me.CheckEdit160.Location = New System.Drawing.Point(859, 13)
        Me.CheckEdit160.Name = "CheckEdit160"
        Me.CheckEdit160.Properties.Caption = "NO"
        Me.CheckEdit160.Size = New System.Drawing.Size(83, 44)
        Me.CheckEdit160.TabIndex = 1
        '
        'CheckEdit159
        '
        Me.CheckEdit159.Location = New System.Drawing.Point(770, 13)
        Me.CheckEdit159.Name = "CheckEdit159"
        Me.CheckEdit159.Properties.Caption = "YES"
        Me.CheckEdit159.Size = New System.Drawing.Size(83, 44)
        Me.CheckEdit159.TabIndex = 1
        '
        'LabelControl62
        '
        Me.LabelControl62.Location = New System.Drawing.Point(19, 27)
        Me.LabelControl62.Name = "LabelControl62"
        Me.LabelControl62.Size = New System.Drawing.Size(707, 16)
        Me.LabelControl62.TabIndex = 0
        Me.LabelControl62.Text = "Are you aware of any transfers of assets e.g. cash, property, belongings worth mo"& _ 
    "re than £3,000 made in the last 7 years?"
        '
        'XtraTabPage34
        '
        Me.XtraTabPage34.Controls.Add(Me.LayoutControl27)
        Me.XtraTabPage34.Name = "XtraTabPage34"
        Me.XtraTabPage34.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage34.Text = "The Deceased"
        '
        'LayoutControl27
        '
        Me.LayoutControl27.Controls.Add(Me.TextEdit78)
        Me.LayoutControl27.Controls.Add(Me.TextEdit76)
        Me.LayoutControl27.Controls.Add(Me.SimpleButton24)
        Me.LayoutControl27.Controls.Add(Me.SimpleButton25)
        Me.LayoutControl27.Controls.Add(Me.MemoEdit21)
        Me.LayoutControl27.Controls.Add(Me.CheckEdit155)
        Me.LayoutControl27.Controls.Add(Me.CheckEdit156)
        Me.LayoutControl27.Controls.Add(Me.DateEdit13)
        Me.LayoutControl27.Controls.Add(Me.CheckEdit157)
        Me.LayoutControl27.Controls.Add(Me.CheckEdit158)
        Me.LayoutControl27.Controls.Add(Me.DateEdit14)
        Me.LayoutControl27.Location = New System.Drawing.Point(14, 10)
        Me.LayoutControl27.Name = "LayoutControl27"
        Me.LayoutControl27.Root = Me.LayoutControlGroup28
        Me.LayoutControl27.Size = New System.Drawing.Size(1000, 540)
        Me.LayoutControl27.TabIndex = 0
        Me.LayoutControl27.Text = "LayoutControl27"
        '
        'TextEdit78
        '
        Me.TextEdit78.Location = New System.Drawing.Point(167, 54)
        Me.TextEdit78.Name = "TextEdit78"
        Me.TextEdit78.Properties.AutoHeight = false
        Me.TextEdit78.Size = New System.Drawing.Size(470, 42)
        Me.TextEdit78.StyleController = Me.LayoutControl27
        Me.TextEdit78.TabIndex = 5
        '
        'TextEdit76
        '
        Me.TextEdit76.Location = New System.Drawing.Point(167, 12)
        Me.TextEdit76.Name = "TextEdit76"
        Me.TextEdit76.Size = New System.Drawing.Size(821, 38)
        Me.TextEdit76.StyleController = Me.LayoutControl27
        Me.TextEdit76.TabIndex = 4
        '
        'SimpleButton24
        '
        Me.SimpleButton24.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton24.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton24.Location = New System.Drawing.Point(641, 54)
        Me.SimpleButton24.Name = "SimpleButton24"
        Me.SimpleButton24.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButton24.StyleController = Me.LayoutControl27
        Me.SimpleButton24.TabIndex = 6
        Me.SimpleButton24.Text = "Lookup"
        '
        'SimpleButton25
        '
        Me.SimpleButton25.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton25.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton25.Location = New System.Drawing.Point(767, 54)
        Me.SimpleButton25.Name = "SimpleButton25"
        Me.SimpleButton25.Size = New System.Drawing.Size(221, 42)
        Me.SimpleButton25.StyleController = Me.LayoutControl27
        Me.SimpleButton25.TabIndex = 7
        Me.SimpleButton25.Text = "Copy from Client Record"
        '
        'MemoEdit21
        '
        Me.MemoEdit21.Location = New System.Drawing.Point(167, 100)
        Me.MemoEdit21.Name = "MemoEdit21"
        Me.MemoEdit21.Size = New System.Drawing.Size(821, 205)
        Me.MemoEdit21.StyleController = Me.LayoutControl27
        Me.MemoEdit21.TabIndex = 8
        '
        'CheckEdit155
        '
        Me.CheckEdit155.Location = New System.Drawing.Point(167, 309)
        Me.CheckEdit155.Name = "CheckEdit155"
        Me.CheckEdit155.Properties.Caption = "YES"
        Me.CheckEdit155.Size = New System.Drawing.Size(106, 44)
        Me.CheckEdit155.StyleController = Me.LayoutControl27
        Me.CheckEdit155.TabIndex = 9
        '
        'CheckEdit156
        '
        Me.CheckEdit156.Location = New System.Drawing.Point(277, 309)
        Me.CheckEdit156.Name = "CheckEdit156"
        Me.CheckEdit156.Properties.Caption = "NO"
        Me.CheckEdit156.Size = New System.Drawing.Size(711, 44)
        Me.CheckEdit156.StyleController = Me.LayoutControl27
        Me.CheckEdit156.TabIndex = 10
        '
        'DateEdit13
        '
        Me.DateEdit13.EditValue = Nothing
        Me.DateEdit13.Location = New System.Drawing.Point(167, 357)
        Me.DateEdit13.Name = "DateEdit13"
        Me.DateEdit13.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit13.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit13.Size = New System.Drawing.Size(821, 38)
        Me.DateEdit13.StyleController = Me.LayoutControl27
        Me.DateEdit13.TabIndex = 11
        '
        'CheckEdit157
        '
        Me.CheckEdit157.Location = New System.Drawing.Point(167, 399)
        Me.CheckEdit157.Name = "CheckEdit157"
        Me.CheckEdit157.Properties.Caption = "YES"
        Me.CheckEdit157.Size = New System.Drawing.Size(93, 44)
        Me.CheckEdit157.StyleController = Me.LayoutControl27
        Me.CheckEdit157.TabIndex = 12
        '
        'CheckEdit158
        '
        Me.CheckEdit158.Location = New System.Drawing.Point(264, 399)
        Me.CheckEdit158.Name = "CheckEdit158"
        Me.CheckEdit158.Properties.Caption = "NO"
        Me.CheckEdit158.Size = New System.Drawing.Size(238, 44)
        Me.CheckEdit158.StyleController = Me.LayoutControl27
        Me.CheckEdit158.TabIndex = 13
        '
        'DateEdit14
        '
        Me.DateEdit14.EditValue = Nothing
        Me.DateEdit14.Location = New System.Drawing.Point(611, 399)
        Me.DateEdit14.Name = "DateEdit14"
        Me.DateEdit14.Properties.AutoHeight = false
        Me.DateEdit14.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit14.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit14.Size = New System.Drawing.Size(377, 44)
        Me.DateEdit14.StyleController = Me.LayoutControl27
        Me.DateEdit14.TabIndex = 14
        '
        'LayoutControlGroup28
        '
        Me.LayoutControlGroup28.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup28.GroupBordersVisible = false
        Me.LayoutControlGroup28.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem205, Me.EmptySpaceItem42, Me.LayoutControlItem206, Me.LayoutControlItem207, Me.LayoutControlItem208, Me.LayoutControlItem209, Me.LayoutControlItem210, Me.LayoutControlItem211, Me.LayoutControlItem212, Me.LayoutControlItem213, Me.LayoutControlItem214, Me.LayoutControlItem215})
        Me.LayoutControlGroup28.Name = "LayoutControlGroup28"
        Me.LayoutControlGroup28.Size = New System.Drawing.Size(1000, 540)
        Me.LayoutControlGroup28.TextVisible = false
        '
        'LayoutControlItem205
        '
        Me.LayoutControlItem205.Control = Me.TextEdit76
        Me.LayoutControlItem205.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem205.Name = "LayoutControlItem205"
        Me.LayoutControlItem205.Size = New System.Drawing.Size(980, 42)
        Me.LayoutControlItem205.Text = "Client Name"
        Me.LayoutControlItem205.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem205.TextSize = New System.Drawing.Size(150, 20)
        Me.LayoutControlItem205.TextToControlDistance = 5
        '
        'EmptySpaceItem42
        '
        Me.EmptySpaceItem42.AllowHotTrack = false
        Me.EmptySpaceItem42.Location = New System.Drawing.Point(0, 435)
        Me.EmptySpaceItem42.Name = "EmptySpaceItem42"
        Me.EmptySpaceItem42.Size = New System.Drawing.Size(980, 85)
        Me.EmptySpaceItem42.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem206
        '
        Me.LayoutControlItem206.Control = Me.TextEdit78
        Me.LayoutControlItem206.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem206.Name = "LayoutControlItem206"
        Me.LayoutControlItem206.Size = New System.Drawing.Size(629, 46)
        Me.LayoutControlItem206.Text = "Address at Death"
        Me.LayoutControlItem206.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem206.TextSize = New System.Drawing.Size(150, 20)
        Me.LayoutControlItem206.TextToControlDistance = 5
        '
        'LayoutControlItem207
        '
        Me.LayoutControlItem207.Control = Me.SimpleButton24
        Me.LayoutControlItem207.Location = New System.Drawing.Point(629, 42)
        Me.LayoutControlItem207.Name = "LayoutControlItem207"
        Me.LayoutControlItem207.Size = New System.Drawing.Size(126, 46)
        Me.LayoutControlItem207.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem207.TextVisible = false
        '
        'LayoutControlItem208
        '
        Me.LayoutControlItem208.Control = Me.SimpleButton25
        Me.LayoutControlItem208.Location = New System.Drawing.Point(755, 42)
        Me.LayoutControlItem208.Name = "LayoutControlItem208"
        Me.LayoutControlItem208.Size = New System.Drawing.Size(225, 46)
        Me.LayoutControlItem208.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem208.TextVisible = false
        '
        'LayoutControlItem209
        '
        Me.LayoutControlItem209.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem209.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem209.Control = Me.MemoEdit21
        Me.LayoutControlItem209.Location = New System.Drawing.Point(0, 88)
        Me.LayoutControlItem209.Name = "LayoutControlItem209"
        Me.LayoutControlItem209.Size = New System.Drawing.Size(980, 209)
        Me.LayoutControlItem209.Text = "Address"
        Me.LayoutControlItem209.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem209.TextSize = New System.Drawing.Size(150, 20)
        Me.LayoutControlItem209.TextToControlDistance = 5
        '
        'LayoutControlItem210
        '
        Me.LayoutControlItem210.Control = Me.CheckEdit155
        Me.LayoutControlItem210.Location = New System.Drawing.Point(0, 297)
        Me.LayoutControlItem210.Name = "LayoutControlItem210"
        Me.LayoutControlItem210.Size = New System.Drawing.Size(265, 48)
        Me.LayoutControlItem210.Text = "Nursing Home"
        Me.LayoutControlItem210.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem210.TextSize = New System.Drawing.Size(150, 20)
        Me.LayoutControlItem210.TextToControlDistance = 5
        '
        'LayoutControlItem211
        '
        Me.LayoutControlItem211.Control = Me.CheckEdit156
        Me.LayoutControlItem211.Location = New System.Drawing.Point(265, 297)
        Me.LayoutControlItem211.Name = "LayoutControlItem211"
        Me.LayoutControlItem211.Size = New System.Drawing.Size(715, 48)
        Me.LayoutControlItem211.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem211.TextVisible = false
        '
        'LayoutControlItem212
        '
        Me.LayoutControlItem212.Control = Me.DateEdit13
        Me.LayoutControlItem212.Location = New System.Drawing.Point(0, 345)
        Me.LayoutControlItem212.Name = "LayoutControlItem212"
        Me.LayoutControlItem212.Size = New System.Drawing.Size(980, 42)
        Me.LayoutControlItem212.Text = "Date of Death"
        Me.LayoutControlItem212.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem212.TextSize = New System.Drawing.Size(150, 20)
        Me.LayoutControlItem212.TextToControlDistance = 5
        '
        'LayoutControlItem213
        '
        Me.LayoutControlItem213.Control = Me.CheckEdit157
        Me.LayoutControlItem213.Location = New System.Drawing.Point(0, 387)
        Me.LayoutControlItem213.Name = "LayoutControlItem213"
        Me.LayoutControlItem213.Size = New System.Drawing.Size(252, 48)
        Me.LayoutControlItem213.Text = "Is there a Will?"
        Me.LayoutControlItem213.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem213.TextSize = New System.Drawing.Size(150, 20)
        Me.LayoutControlItem213.TextToControlDistance = 5
        '
        'LayoutControlItem214
        '
        Me.LayoutControlItem214.Control = Me.CheckEdit158
        Me.LayoutControlItem214.Location = New System.Drawing.Point(252, 387)
        Me.LayoutControlItem214.Name = "LayoutControlItem214"
        Me.LayoutControlItem214.Size = New System.Drawing.Size(242, 48)
        Me.LayoutControlItem214.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem214.TextVisible = false
        '
        'LayoutControlItem215
        '
        Me.LayoutControlItem215.Control = Me.DateEdit14
        Me.LayoutControlItem215.Location = New System.Drawing.Point(494, 387)
        Me.LayoutControlItem215.Name = "LayoutControlItem215"
        Me.LayoutControlItem215.Size = New System.Drawing.Size(486, 48)
        Me.LayoutControlItem215.Text = "Date of Will"
        Me.LayoutControlItem215.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem215.TextSize = New System.Drawing.Size(100, 16)
        Me.LayoutControlItem215.TextToControlDistance = 5
        '
        'XtraTabPage35
        '
        Me.XtraTabPage35.Controls.Add(Me.SimpleButton26)
        Me.XtraTabPage35.Controls.Add(Me.GridControl19)
        Me.XtraTabPage35.Name = "XtraTabPage35"
        Me.XtraTabPage35.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage35.Text = "Surviving Relatives"
        '
        'SimpleButton26
        '
        Me.SimpleButton26.ImageOptions.Image = CType(resources.GetObject("SimpleButton26.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton26.Location = New System.Drawing.Point(26, 22)
        Me.SimpleButton26.Name = "SimpleButton26"
        Me.SimpleButton26.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton26.TabIndex = 22
        Me.SimpleButton26.Text = "Add Relative"
        '
        'GridControl19
        '
        Me.GridControl19.Location = New System.Drawing.Point(26, 73)
        Me.GridControl19.MainView = Me.GridView19
        Me.GridControl19.Name = "GridControl19"
        Me.GridControl19.Size = New System.Drawing.Size(974, 471)
        Me.GridControl19.TabIndex = 0
        Me.GridControl19.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView19})
        '
        'GridView19
        '
        Me.GridView19.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn68, Me.GridColumn69, Me.GridColumn70, Me.GridColumn71, Me.GridColumn72})
        Me.GridView19.GridControl = Me.GridControl19
        Me.GridView19.Name = "GridView19"
        Me.GridView19.OptionsView.ShowGroupPanel = false
        Me.GridView19.OptionsView.ShowIndicator = false
        Me.GridView19.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn68
        '
        Me.GridColumn68.Caption = "Name"
        Me.GridColumn68.Name = "GridColumn68"
        Me.GridColumn68.Visible = true
        Me.GridColumn68.VisibleIndex = 0
        Me.GridColumn68.Width = 191
        '
        'GridColumn69
        '
        Me.GridColumn69.Caption = "Address"
        Me.GridColumn69.Name = "GridColumn69"
        Me.GridColumn69.Visible = true
        Me.GridColumn69.VisibleIndex = 1
        Me.GridColumn69.Width = 191
        '
        'GridColumn70
        '
        Me.GridColumn70.Caption = "Relationship"
        Me.GridColumn70.Name = "GridColumn70"
        Me.GridColumn70.Visible = true
        Me.GridColumn70.VisibleIndex = 2
        Me.GridColumn70.Width = 191
        '
        'GridColumn71
        '
        Me.GridColumn71.Caption = "Financially Independent"
        Me.GridColumn71.Name = "GridColumn71"
        Me.GridColumn71.Visible = true
        Me.GridColumn71.VisibleIndex = 3
        Me.GridColumn71.Width = 191
        '
        'GridColumn72
        '
        Me.GridColumn72.Caption = "Action"
        Me.GridColumn72.Name = "GridColumn72"
        Me.GridColumn72.Visible = true
        Me.GridColumn72.VisibleIndex = 4
        Me.GridColumn72.Width = 100
        '
        'XtraTabPage36
        '
        Me.XtraTabPage36.Controls.Add(Me.SimpleButton27)
        Me.XtraTabPage36.Controls.Add(Me.GridControl20)
        Me.XtraTabPage36.Name = "XtraTabPage36"
        Me.XtraTabPage36.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage36.Text = "Executors"
        '
        'SimpleButton27
        '
        Me.SimpleButton27.ImageOptions.Image = CType(resources.GetObject("SimpleButton27.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton27.Location = New System.Drawing.Point(26, 22)
        Me.SimpleButton27.Name = "SimpleButton27"
        Me.SimpleButton27.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton27.TabIndex = 23
        Me.SimpleButton27.Text = "Add an Executor"
        '
        'GridControl20
        '
        Me.GridControl20.Location = New System.Drawing.Point(25, 73)
        Me.GridControl20.MainView = Me.GridView20
        Me.GridControl20.Name = "GridControl20"
        Me.GridControl20.Size = New System.Drawing.Size(974, 445)
        Me.GridControl20.TabIndex = 1
        Me.GridControl20.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView20})
        '
        'GridView20
        '
        Me.GridView20.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn73, Me.GridColumn74, Me.GridColumn75, Me.GridColumn76, Me.GridColumn77})
        Me.GridView20.GridControl = Me.GridControl20
        Me.GridView20.Name = "GridView20"
        Me.GridView20.OptionsView.ShowGroupPanel = false
        Me.GridView20.OptionsView.ShowIndicator = false
        Me.GridView20.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn73
        '
        Me.GridColumn73.Caption = "Name"
        Me.GridColumn73.Name = "GridColumn73"
        Me.GridColumn73.Visible = true
        Me.GridColumn73.VisibleIndex = 0
        Me.GridColumn73.Width = 191
        '
        'GridColumn74
        '
        Me.GridColumn74.Caption = "Address"
        Me.GridColumn74.Name = "GridColumn74"
        Me.GridColumn74.Visible = true
        Me.GridColumn74.VisibleIndex = 1
        Me.GridColumn74.Width = 191
        '
        'GridColumn75
        '
        Me.GridColumn75.Caption = "Executor Type"
        Me.GridColumn75.Name = "GridColumn75"
        Me.GridColumn75.Visible = true
        Me.GridColumn75.VisibleIndex = 2
        Me.GridColumn75.Width = 191
        '
        'GridColumn76
        '
        Me.GridColumn76.Caption = "Relationship To Deceased"
        Me.GridColumn76.Name = "GridColumn76"
        Me.GridColumn76.Visible = true
        Me.GridColumn76.VisibleIndex = 3
        Me.GridColumn76.Width = 191
        '
        'GridColumn77
        '
        Me.GridColumn77.Caption = "Action"
        Me.GridColumn77.Name = "GridColumn77"
        Me.GridColumn77.Visible = true
        Me.GridColumn77.VisibleIndex = 4
        Me.GridColumn77.Width = 100
        '
        'XtraTabPage37
        '
        Me.XtraTabPage37.Controls.Add(Me.SimpleButton28)
        Me.XtraTabPage37.Controls.Add(Me.GridControl21)
        Me.XtraTabPage37.Name = "XtraTabPage37"
        Me.XtraTabPage37.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage37.Text = "Advisers && Professionals"
        '
        'SimpleButton28
        '
        Me.SimpleButton28.ImageOptions.Image = CType(resources.GetObject("SimpleButton28.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton28.Location = New System.Drawing.Point(26, 22)
        Me.SimpleButton28.Name = "SimpleButton28"
        Me.SimpleButton28.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton28.TabIndex = 25
        Me.SimpleButton28.Text = "Add an Adviser"
        '
        'GridControl21
        '
        Me.GridControl21.Location = New System.Drawing.Point(25, 73)
        Me.GridControl21.MainView = Me.GridView21
        Me.GridControl21.Name = "GridControl21"
        Me.GridControl21.Size = New System.Drawing.Size(974, 457)
        Me.GridControl21.TabIndex = 24
        Me.GridControl21.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView21})
        '
        'GridView21
        '
        Me.GridView21.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn78, Me.GridColumn79, Me.GridColumn80, Me.GridColumn81, Me.GridColumn82})
        Me.GridView21.GridControl = Me.GridControl21
        Me.GridView21.Name = "GridView21"
        Me.GridView21.OptionsView.ShowGroupPanel = false
        Me.GridView21.OptionsView.ShowIndicator = false
        Me.GridView21.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn78
        '
        Me.GridColumn78.Caption = "Name"
        Me.GridColumn78.Name = "GridColumn78"
        Me.GridColumn78.Visible = true
        Me.GridColumn78.VisibleIndex = 0
        Me.GridColumn78.Width = 191
        '
        'GridColumn79
        '
        Me.GridColumn79.Caption = "Address"
        Me.GridColumn79.Name = "GridColumn79"
        Me.GridColumn79.Visible = true
        Me.GridColumn79.VisibleIndex = 1
        Me.GridColumn79.Width = 191
        '
        'GridColumn80
        '
        Me.GridColumn80.Caption = "Adviser Type"
        Me.GridColumn80.Name = "GridColumn80"
        Me.GridColumn80.Visible = true
        Me.GridColumn80.VisibleIndex = 2
        Me.GridColumn80.Width = 191
        '
        'GridColumn81
        '
        Me.GridColumn81.Caption = "Relationship To Deceased"
        Me.GridColumn81.Name = "GridColumn81"
        Me.GridColumn81.Visible = true
        Me.GridColumn81.VisibleIndex = 3
        Me.GridColumn81.Width = 191
        '
        'GridColumn82
        '
        Me.GridColumn82.Caption = "Action"
        Me.GridColumn82.Name = "GridColumn82"
        Me.GridColumn82.Visible = true
        Me.GridColumn82.VisibleIndex = 4
        Me.GridColumn82.Width = 100
        '
        'XtraTabPage38
        '
        Me.XtraTabPage38.Controls.Add(Me.SimpleButton29)
        Me.XtraTabPage38.Controls.Add(Me.GridControl22)
        Me.XtraTabPage38.Name = "XtraTabPage38"
        Me.XtraTabPage38.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage38.Text = "Assets"
        '
        'SimpleButton29
        '
        Me.SimpleButton29.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton29.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton29.Location = New System.Drawing.Point(26, 22)
        Me.SimpleButton29.Name = "SimpleButton29"
        Me.SimpleButton29.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton29.TabIndex = 27
        Me.SimpleButton29.Text = "Add an Asset"
        '
        'GridControl22
        '
        Me.GridControl22.Location = New System.Drawing.Point(25, 73)
        Me.GridControl22.MainView = Me.GridView22
        Me.GridControl22.Name = "GridControl22"
        Me.GridControl22.Size = New System.Drawing.Size(974, 463)
        Me.GridControl22.TabIndex = 26
        Me.GridControl22.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView22})
        '
        'GridView22
        '
        Me.GridView22.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn83, Me.GridColumn84, Me.GridColumn86, Me.GridColumn87})
        Me.GridView22.GridControl = Me.GridControl22
        Me.GridView22.Name = "GridView22"
        Me.GridView22.OptionsView.ShowGroupPanel = false
        Me.GridView22.OptionsView.ShowIndicator = false
        Me.GridView22.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn83
        '
        Me.GridColumn83.Caption = "Asset Type"
        Me.GridColumn83.Name = "GridColumn83"
        Me.GridColumn83.Visible = true
        Me.GridColumn83.VisibleIndex = 0
        Me.GridColumn83.Width = 275
        '
        'GridColumn84
        '
        Me.GridColumn84.Caption = "Detail"
        Me.GridColumn84.Name = "GridColumn84"
        Me.GridColumn84.Visible = true
        Me.GridColumn84.VisibleIndex = 1
        Me.GridColumn84.Width = 397
        '
        'GridColumn86
        '
        Me.GridColumn86.Caption = "Value"
        Me.GridColumn86.Name = "GridColumn86"
        Me.GridColumn86.Visible = true
        Me.GridColumn86.VisibleIndex = 2
        Me.GridColumn86.Width = 195
        '
        'GridColumn87
        '
        Me.GridColumn87.Caption = "Action"
        Me.GridColumn87.Name = "GridColumn87"
        Me.GridColumn87.Visible = true
        Me.GridColumn87.VisibleIndex = 3
        Me.GridColumn87.Width = 105
        '
        'XtraTabPage39
        '
        Me.XtraTabPage39.Controls.Add(Me.SimpleButton30)
        Me.XtraTabPage39.Controls.Add(Me.GridControl23)
        Me.XtraTabPage39.Name = "XtraTabPage39"
        Me.XtraTabPage39.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage39.Text = "Liabilities"
        '
        'SimpleButton30
        '
        Me.SimpleButton30.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton30.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton30.Location = New System.Drawing.Point(26, 22)
        Me.SimpleButton30.Name = "SimpleButton30"
        Me.SimpleButton30.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton30.TabIndex = 29
        Me.SimpleButton30.Text = "Add a Liability"
        '
        'GridControl23
        '
        Me.GridControl23.Location = New System.Drawing.Point(26, 73)
        Me.GridControl23.MainView = Me.GridView23
        Me.GridControl23.Name = "GridControl23"
        Me.GridControl23.Size = New System.Drawing.Size(974, 463)
        Me.GridControl23.TabIndex = 28
        Me.GridControl23.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView23})
        '
        'GridView23
        '
        Me.GridView23.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn85, Me.GridColumn88, Me.GridColumn89, Me.GridColumn90})
        Me.GridView23.GridControl = Me.GridControl23
        Me.GridView23.Name = "GridView23"
        Me.GridView23.OptionsView.ShowGroupPanel = false
        Me.GridView23.OptionsView.ShowIndicator = false
        Me.GridView23.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn85
        '
        Me.GridColumn85.Caption = "Liability Type"
        Me.GridColumn85.Name = "GridColumn85"
        Me.GridColumn85.Visible = true
        Me.GridColumn85.VisibleIndex = 0
        Me.GridColumn85.Width = 275
        '
        'GridColumn88
        '
        Me.GridColumn88.Caption = "Detail"
        Me.GridColumn88.Name = "GridColumn88"
        Me.GridColumn88.Visible = true
        Me.GridColumn88.VisibleIndex = 1
        Me.GridColumn88.Width = 397
        '
        'GridColumn89
        '
        Me.GridColumn89.Caption = "Value"
        Me.GridColumn89.Name = "GridColumn89"
        Me.GridColumn89.Visible = true
        Me.GridColumn89.VisibleIndex = 2
        Me.GridColumn89.Width = 195
        '
        'GridColumn90
        '
        Me.GridColumn90.Caption = "Action"
        Me.GridColumn90.Name = "GridColumn90"
        Me.GridColumn90.Visible = true
        Me.GridColumn90.VisibleIndex = 3
        Me.GridColumn90.Width = 105
        '
        'XtraTabPage40
        '
        Me.XtraTabPage40.Controls.Add(Me.SimpleButton31)
        Me.XtraTabPage40.Controls.Add(Me.GridControl24)
        Me.XtraTabPage40.Name = "XtraTabPage40"
        Me.XtraTabPage40.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage40.Text = "Utilities"
        '
        'SimpleButton31
        '
        Me.SimpleButton31.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton31.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton31.Location = New System.Drawing.Point(26, 22)
        Me.SimpleButton31.Name = "SimpleButton31"
        Me.SimpleButton31.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton31.TabIndex = 31
        Me.SimpleButton31.Text = "Add a Utility"
        '
        'GridControl24
        '
        Me.GridControl24.Location = New System.Drawing.Point(25, 76)
        Me.GridControl24.MainView = Me.GridView24
        Me.GridControl24.Name = "GridControl24"
        Me.GridControl24.Size = New System.Drawing.Size(974, 463)
        Me.GridControl24.TabIndex = 30
        Me.GridControl24.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView24})
        '
        'GridView24
        '
        Me.GridView24.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn91, Me.GridColumn92, Me.GridColumn93, Me.GridColumn94, Me.GridColumn95})
        Me.GridView24.GridControl = Me.GridControl24
        Me.GridView24.Name = "GridView24"
        Me.GridView24.OptionsView.ShowGroupPanel = false
        Me.GridView24.OptionsView.ShowIndicator = false
        Me.GridView24.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn91
        '
        Me.GridColumn91.Caption = "Utility Type"
        Me.GridColumn91.Name = "GridColumn91"
        Me.GridColumn91.Visible = true
        Me.GridColumn91.VisibleIndex = 0
        Me.GridColumn91.Width = 255
        '
        'GridColumn92
        '
        Me.GridColumn92.Caption = "Provider"
        Me.GridColumn92.Name = "GridColumn92"
        Me.GridColumn92.Visible = true
        Me.GridColumn92.VisibleIndex = 1
        Me.GridColumn92.Width = 278
        '
        'GridColumn93
        '
        Me.GridColumn93.Caption = "Meter Readings"
        Me.GridColumn93.Name = "GridColumn93"
        Me.GridColumn93.Visible = true
        Me.GridColumn93.VisibleIndex = 3
        Me.GridColumn93.Width = 181
        '
        'GridColumn94
        '
        Me.GridColumn94.Caption = "Action"
        Me.GridColumn94.Name = "GridColumn94"
        Me.GridColumn94.Visible = true
        Me.GridColumn94.VisibleIndex = 4
        Me.GridColumn94.Width = 101
        '
        'GridColumn95
        '
        Me.GridColumn95.Caption = "Account #"
        Me.GridColumn95.Name = "GridColumn95"
        Me.GridColumn95.Visible = true
        Me.GridColumn95.VisibleIndex = 2
        Me.GridColumn95.Width = 157
        '
        'XtraTabPage41
        '
        Me.XtraTabPage41.Controls.Add(Me.GridControl25)
        Me.XtraTabPage41.Controls.Add(Me.SimpleButton32)
        Me.XtraTabPage41.Name = "XtraTabPage41"
        Me.XtraTabPage41.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage41.Text = "Beneficiaries"
        '
        'GridControl25
        '
        Me.GridControl25.Location = New System.Drawing.Point(26, 73)
        Me.GridControl25.MainView = Me.GridView25
        Me.GridControl25.Name = "GridControl25"
        Me.GridControl25.Size = New System.Drawing.Size(974, 463)
        Me.GridControl25.TabIndex = 31
        Me.GridControl25.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView25})
        '
        'GridView25
        '
        Me.GridView25.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn96, Me.GridColumn97, Me.GridColumn98, Me.GridColumn99, Me.GridColumn100})
        Me.GridView25.GridControl = Me.GridControl25
        Me.GridView25.Name = "GridView25"
        Me.GridView25.OptionsView.ShowGroupPanel = false
        Me.GridView25.OptionsView.ShowIndicator = false
        Me.GridView25.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn96
        '
        Me.GridColumn96.Caption = "Name"
        Me.GridColumn96.Name = "GridColumn96"
        Me.GridColumn96.Visible = true
        Me.GridColumn96.VisibleIndex = 0
        Me.GridColumn96.Width = 255
        '
        'GridColumn97
        '
        Me.GridColumn97.Caption = "Address"
        Me.GridColumn97.Name = "GridColumn97"
        Me.GridColumn97.Visible = true
        Me.GridColumn97.VisibleIndex = 1
        Me.GridColumn97.Width = 278
        '
        'GridColumn98
        '
        Me.GridColumn98.Caption = "Item"
        Me.GridColumn98.Name = "GridColumn98"
        Me.GridColumn98.Visible = true
        Me.GridColumn98.VisibleIndex = 3
        Me.GridColumn98.Width = 181
        '
        'GridColumn99
        '
        Me.GridColumn99.Caption = "Action"
        Me.GridColumn99.Name = "GridColumn99"
        Me.GridColumn99.Visible = true
        Me.GridColumn99.VisibleIndex = 4
        Me.GridColumn99.Width = 101
        '
        'GridColumn100
        '
        Me.GridColumn100.Caption = "Relationship"
        Me.GridColumn100.Name = "GridColumn100"
        Me.GridColumn100.Visible = true
        Me.GridColumn100.VisibleIndex = 2
        Me.GridColumn100.Width = 157
        '
        'SimpleButton32
        '
        Me.SimpleButton32.ImageOptions.Image = CType(resources.GetObject("SimpleButton32.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton32.Location = New System.Drawing.Point(26, 22)
        Me.SimpleButton32.Name = "SimpleButton32"
        Me.SimpleButton32.Size = New System.Drawing.Size(185, 45)
        Me.SimpleButton32.TabIndex = 26
        Me.SimpleButton32.Text = "Add Beneficiary"
        '
        'XtraTabPage43
        '
        Me.XtraTabPage43.Controls.Add(Me.LayoutControl28)
        Me.XtraTabPage43.Name = "XtraTabPage43"
        Me.XtraTabPage43.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage43.Text = "Net State Value"
        '
        'LayoutControl28
        '
        Me.LayoutControl28.Controls.Add(Me.TextEdit79)
        Me.LayoutControl28.Controls.Add(Me.TextEdit80)
        Me.LayoutControl28.Controls.Add(Me.TextEdit81)
        Me.LayoutControl28.Controls.Add(Me.TextEdit82)
        Me.LayoutControl28.Controls.Add(Me.TextEdit83)
        Me.LayoutControl28.Location = New System.Drawing.Point(26, 25)
        Me.LayoutControl28.Name = "LayoutControl28"
        Me.LayoutControl28.Root = Me.LayoutControlGroup29
        Me.LayoutControl28.Size = New System.Drawing.Size(625, 518)
        Me.LayoutControl28.TabIndex = 0
        Me.LayoutControl28.Text = "LayoutControl28"
        '
        'TextEdit79
        '
        Me.TextEdit79.Location = New System.Drawing.Point(171, 12)
        Me.TextEdit79.Name = "TextEdit79"
        Me.TextEdit79.Size = New System.Drawing.Size(442, 38)
        Me.TextEdit79.StyleController = Me.LayoutControl28
        Me.TextEdit79.TabIndex = 4
        '
        'TextEdit80
        '
        Me.TextEdit80.Location = New System.Drawing.Point(171, 54)
        Me.TextEdit80.Name = "TextEdit80"
        Me.TextEdit80.Size = New System.Drawing.Size(442, 38)
        Me.TextEdit80.StyleController = Me.LayoutControl28
        Me.TextEdit80.TabIndex = 5
        '
        'TextEdit81
        '
        Me.TextEdit81.Location = New System.Drawing.Point(171, 96)
        Me.TextEdit81.Name = "TextEdit81"
        Me.TextEdit81.Size = New System.Drawing.Size(442, 38)
        Me.TextEdit81.StyleController = Me.LayoutControl28
        Me.TextEdit81.TabIndex = 6
        '
        'TextEdit82
        '
        Me.TextEdit82.Location = New System.Drawing.Point(171, 138)
        Me.TextEdit82.Name = "TextEdit82"
        Me.TextEdit82.Size = New System.Drawing.Size(442, 38)
        Me.TextEdit82.StyleController = Me.LayoutControl28
        Me.TextEdit82.TabIndex = 7
        '
        'TextEdit83
        '
        Me.TextEdit83.Location = New System.Drawing.Point(171, 180)
        Me.TextEdit83.Name = "TextEdit83"
        Me.TextEdit83.Size = New System.Drawing.Size(282, 38)
        Me.TextEdit83.StyleController = Me.LayoutControl28
        Me.TextEdit83.TabIndex = 8
        '
        'LayoutControlGroup29
        '
        Me.LayoutControlGroup29.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup29.GroupBordersVisible = false
        Me.LayoutControlGroup29.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem216, Me.LayoutControlItem217, Me.LayoutControlItem218, Me.LayoutControlItem219, Me.LayoutControlItem220, Me.EmptySpaceItem43, Me.SimpleLabelItem10})
        Me.LayoutControlGroup29.Name = "LayoutControlGroup29"
        Me.LayoutControlGroup29.Size = New System.Drawing.Size(625, 518)
        Me.LayoutControlGroup29.TextVisible = false
        '
        'LayoutControlItem216
        '
        Me.LayoutControlItem216.Control = Me.TextEdit79
        Me.LayoutControlItem216.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem216.Name = "LayoutControlItem216"
        Me.LayoutControlItem216.Size = New System.Drawing.Size(605, 42)
        Me.LayoutControlItem216.Text = "Total Value of Assets"
        Me.LayoutControlItem216.TextSize = New System.Drawing.Size(156, 16)
        '
        'LayoutControlItem217
        '
        Me.LayoutControlItem217.Control = Me.TextEdit80
        Me.LayoutControlItem217.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem217.Name = "LayoutControlItem217"
        Me.LayoutControlItem217.Size = New System.Drawing.Size(605, 42)
        Me.LayoutControlItem217.Text = "Total Value of Liabilities"
        Me.LayoutControlItem217.TextSize = New System.Drawing.Size(156, 16)
        '
        'LayoutControlItem218
        '
        Me.LayoutControlItem218.Control = Me.TextEdit81
        Me.LayoutControlItem218.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem218.Name = "LayoutControlItem218"
        Me.LayoutControlItem218.Size = New System.Drawing.Size(605, 42)
        Me.LayoutControlItem218.Text = "Net Asset Value"
        Me.LayoutControlItem218.TextSize = New System.Drawing.Size(156, 16)
        '
        'LayoutControlItem219
        '
        Me.LayoutControlItem219.Control = Me.TextEdit82
        Me.LayoutControlItem219.Location = New System.Drawing.Point(0, 126)
        Me.LayoutControlItem219.Name = "LayoutControlItem219"
        Me.LayoutControlItem219.Size = New System.Drawing.Size(605, 42)
        Me.LayoutControlItem219.Text = "Probate Fee"
        Me.LayoutControlItem219.TextSize = New System.Drawing.Size(156, 16)
        '
        'LayoutControlItem220
        '
        Me.LayoutControlItem220.Control = Me.TextEdit83
        Me.LayoutControlItem220.Location = New System.Drawing.Point(0, 168)
        Me.LayoutControlItem220.Name = "LayoutControlItem220"
        Me.LayoutControlItem220.Size = New System.Drawing.Size(445, 42)
        Me.LayoutControlItem220.Text = "Probate Cost"
        Me.LayoutControlItem220.TextSize = New System.Drawing.Size(156, 16)
        '
        'EmptySpaceItem43
        '
        Me.EmptySpaceItem43.AllowHotTrack = false
        Me.EmptySpaceItem43.Location = New System.Drawing.Point(0, 210)
        Me.EmptySpaceItem43.Name = "EmptySpaceItem43"
        Me.EmptySpaceItem43.Size = New System.Drawing.Size(605, 288)
        Me.EmptySpaceItem43.TextSize = New System.Drawing.Size(0, 0)
        '
        'SimpleLabelItem10
        '
        Me.SimpleLabelItem10.AllowHotTrack = false
        Me.SimpleLabelItem10.Location = New System.Drawing.Point(445, 168)
        Me.SimpleLabelItem10.Name = "SimpleLabelItem10"
        Me.SimpleLabelItem10.Size = New System.Drawing.Size(160, 42)
        Me.SimpleLabelItem10.Text = "(plus VAT && disbursement)"
        Me.SimpleLabelItem10.TextSize = New System.Drawing.Size(156, 16)
        '
        'XtraTabPage44
        '
        Me.XtraTabPage44.Controls.Add(Me.LayoutControl29)
        Me.XtraTabPage44.Controls.Add(Me.LabelControl63)
        Me.XtraTabPage44.Name = "XtraTabPage44"
        Me.XtraTabPage44.Size = New System.Drawing.Size(1029, 566)
        Me.XtraTabPage44.Text = "Attendance Notes"
        '
        'LayoutControl29
        '
        Me.LayoutControl29.Controls.Add(Me.MemoEdit22)
        Me.LayoutControl29.Location = New System.Drawing.Point(35, 70)
        Me.LayoutControl29.Name = "LayoutControl29"
        Me.LayoutControl29.Root = Me.LayoutControlGroup30
        Me.LayoutControl29.Size = New System.Drawing.Size(908, 433)
        Me.LayoutControl29.TabIndex = 2
        Me.LayoutControl29.Text = "LayoutControl29"
        '
        'MemoEdit22
        '
        Me.MemoEdit22.Location = New System.Drawing.Point(12, 12)
        Me.MemoEdit22.Name = "MemoEdit22"
        Me.MemoEdit22.Size = New System.Drawing.Size(884, 409)
        Me.MemoEdit22.StyleController = Me.LayoutControl29
        Me.MemoEdit22.TabIndex = 0
        '
        'LayoutControlGroup30
        '
        Me.LayoutControlGroup30.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup30.GroupBordersVisible = false
        Me.LayoutControlGroup30.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem221})
        Me.LayoutControlGroup30.Name = "LayoutControlGroup30"
        Me.LayoutControlGroup30.Size = New System.Drawing.Size(908, 433)
        Me.LayoutControlGroup30.TextVisible = false
        '
        'LayoutControlItem221
        '
        Me.LayoutControlItem221.Control = Me.MemoEdit22
        Me.LayoutControlItem221.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem221.Name = "LayoutControlItem221"
        Me.LayoutControlItem221.Size = New System.Drawing.Size(888, 413)
        Me.LayoutControlItem221.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem221.TextVisible = false
        '
        'LabelControl63
        '
        Me.LabelControl63.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl63.Appearance.Options.UseFont = true
        Me.LabelControl63.Location = New System.Drawing.Point(47, 48)
        Me.LabelControl63.Name = "LabelControl63"
        Me.LabelControl63.Size = New System.Drawing.Size(111, 16)
        Me.LabelControl63.TabIndex = 1
        Me.LabelControl63.Text = "Attendance Notes"
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Caption = "NavigationPage1"
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.Size = New System.Drawing.Size(1034, 592)
        '
        'NavigationPage2
        '
        Me.NavigationPage2.Caption = "NavigationPage2"
        Me.NavigationPage2.Name = "NavigationPage2"
        Me.NavigationPage2.Size = New System.Drawing.Size(1034, 592)
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.StatusBar", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'xWillInstruction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1240, 662)
        Me.Controls.Add(Me.NavigationPane1)
        Me.Name = "xWillInstruction"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Client Record"
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPane1.ResumeLayout(false)
        Me.NavigationPageClientDetails.ResumeLayout(false)
        CType(Me.NavigationFrame1,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationFrame1.ResumeLayout(false)
        Me.NavigationPageAddNewClient.ResumeLayout(false)
        Me.NavigationPageAddNewClient.PerformLayout
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl2.ResumeLayout(false)
        CType(Me.TextEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit10.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit13.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMs2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMiss2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMrs2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit14.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleSeparator2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl1.ResumeLayout(false)
        CType(Me.TextEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMs.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMiss.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMr.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMrs.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleSeparator1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditDual.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditSingle.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageRecommendationTool.ResumeLayout(false)
        CType(Me.TabPane1,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabPane1.ResumeLayout(false)
        Me.TabNavigationPage1.ResumeLayout(false)
        CType(Me.LayoutControl3,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl3.ResumeLayout(false)
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit13.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit14.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit15.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit16.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit17.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit18.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit19.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit20.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit21.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit22.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit23.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit24.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit25.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit26.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit27.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit28.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem30,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem31,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem32,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem33,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem34,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem35,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem36,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem37,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem38,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem39,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem40,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem41,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem42,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem43,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem44,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem45,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem46,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem47,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabNavigationPage2.ResumeLayout(false)
        Me.TabNavigationPage2.PerformLayout
        CType(Me.ComboBoxEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabNavigationPage3.ResumeLayout(false)
        CType(Me.LayoutControl4,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl4.ResumeLayout(false)
        CType(Me.CheckEdit29.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit30.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit15.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit16.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit17.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit18.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem48,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem49,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem50,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem51,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem52,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem53,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem54,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabNavigationPage4.ResumeLayout(false)
        Me.TabNavigationPage4.PerformLayout
        CType(Me.CheckEdit41.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit42.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit40.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit39.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit39.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit42.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit47.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit46.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit45.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit44.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit43.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit41.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit38.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit37.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit36.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit35.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl5,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl5.ResumeLayout(false)
        CType(Me.TextEdit19.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit31.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit20.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit32.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit21.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit33.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit25.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit26.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit27.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit28.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit29.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit30.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ButtonEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit34.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit35.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit36.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit37.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit38.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit31.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit32.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem55,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem56,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem57,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem58,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem59,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem60,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem67,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem68,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem69,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem70,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem71,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem72,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem73,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem74,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem75,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem76,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem77,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem79,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem78,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem80,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit34.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit33.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit40.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabNavigationPage5.ResumeLayout(false)
        CType(Me.LayoutControl6,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl6.ResumeLayout(false)
        CType(Me.TrackBarControl2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TrackBarControl2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TrackBarControl1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TrackBarControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit22.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit23.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit24.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ButtonEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem61,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem62,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem63,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem65,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem64,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem66,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageWillInstruction.ResumeLayout(false)
        CType(Me.XtraTabControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl1.ResumeLayout(false)
        Me.XtraTabPage1.ResumeLayout(false)
        Me.XtraTabPage1.PerformLayout
        CType(Me.ComboBoxEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit46.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit44.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit45.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit43.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GroupControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl2.ResumeLayout(false)
        CType(Me.LayoutControl8,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl8.ResumeLayout(false)
        CType(Me.MemoEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit4.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit50.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit51.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem85,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem86,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem87,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem88,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GroupControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl1.ResumeLayout(false)
        CType(Me.LayoutControl7,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl7.ResumeLayout(false)
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit3.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit48.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit49.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem81,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem82,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem83,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem84,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage2.ResumeLayout(false)
        CType(Me.GridControl2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl9,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl9.ResumeLayout(false)
        CType(Me.CheckEdit47.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit48.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem89,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem90,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem91,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage3.ResumeLayout(false)
        CType(Me.GridControl3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl10,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl10.ResumeLayout(false)
        CType(Me.CheckEdit49.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit50.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit51.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit52.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit53.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit54.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit55.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit56.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit57.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit58.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit59.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit60.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit61.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit62.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit63.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit64.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit65.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit66.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit67.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit68.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem110,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem92,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem93,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem94,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem95,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem96,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem97,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem98,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem99,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem100,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem101,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem102,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem103,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem104,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem105,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem106,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem107,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem108,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem109,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem111,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem112,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage4.ResumeLayout(false)
        CType(Me.GridControl4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView4,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage5.ResumeLayout(false)
        Me.XtraTabPage5.PerformLayout
        CType(Me.LayoutControl12,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl12.ResumeLayout(false)
        CType(Me.CheckEdit72.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit73.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit74.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit77.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit78.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem116,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem117,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem118,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem119,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem123,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem124,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl11,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl11.ResumeLayout(false)
        CType(Me.CheckEdit69.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit70.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit71.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit75.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit76.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem113,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem114,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem115,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem121,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem120,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem122,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage6.ResumeLayout(false)
        CType(Me.GridControl5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl13,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl13.ResumeLayout(false)
        CType(Me.CheckEdit79.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit80.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem125,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem126,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage7.ResumeLayout(false)
        Me.XtraTabPage7.PerformLayout
        CType(Me.XtraTabControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl2.ResumeLayout(false)
        Me.XtraTabPage11.ResumeLayout(false)
        Me.XtraTabPage11.PerformLayout
        CType(Me.GridControl6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit52.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SeparatorControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit84.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit53.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SeparatorControl2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit85.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SeparatorControl3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit86.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit87.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SeparatorControl4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit88.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit89.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SeparatorControl5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit90.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit91.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SeparatorControl6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit92.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit93.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit94.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit95.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit96.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit97.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl14,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl14.ResumeLayout(false)
        CType(Me.CheckEdit81.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit82.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit83.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem127,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem128,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem129,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageLPAInstruction.ResumeLayout(false)
        CType(Me.XtraTabControl3,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl3.ResumeLayout(false)
        Me.XtraTabPage13.ResumeLayout(false)
        CType(Me.GroupControl4,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl4.ResumeLayout(false)
        CType(Me.LayoutControl16,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl16.ResumeLayout(false)
        CType(Me.TextEdit56.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit6.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit57.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit100.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit101.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem136,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem137,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem138,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem139,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem140,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem141,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GroupControl3,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl3.ResumeLayout(false)
        CType(Me.LayoutControl15,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl15.ResumeLayout(false)
        CType(Me.TextEdit54.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit5.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit55.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit98.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit99.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem130,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem131,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem132,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem133,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem16,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem134,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem135,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem20,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage14.ResumeLayout(false)
        Me.XtraTabPage14.PerformLayout
        CType(Me.MemoEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit108.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit109.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit110.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit111.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit102.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit103.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit104.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit105.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit106.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit107.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage15.ResumeLayout(false)
        Me.XtraTabPage15.PerformLayout
        CType(Me.GridControl8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit112.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit113.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage16.ResumeLayout(false)
        Me.XtraTabPage16.PerformLayout
        CType(Me.LayoutControl17,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl17.ResumeLayout(false)
        CType(Me.TextEdit58.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit59.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit60.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit10.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit61.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit62.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit7.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit121.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup16,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem142,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem143,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem144,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem145,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem146,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem147,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem148,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem149,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem25,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem150,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit114.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit115.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit116.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit117.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit118.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit119.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit120.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage17.ResumeLayout(false)
        CType(Me.MemoEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage18.ResumeLayout(false)
        CType(Me.MemoEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage19.ResumeLayout(false)
        CType(Me.GridControl9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView9,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage20.ResumeLayout(false)
        CType(Me.LayoutControl18,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl18.ResumeLayout(false)
        CType(Me.CheckEdit122.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit123.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit124.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit125.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit126.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit127.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit128.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit129.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit130.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit63.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit13.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit64.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit65.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit8.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem151,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem152,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem153,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem154,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem155,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem27,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem156,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem157,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem158,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem159,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem160,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem161,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem28,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem162,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem163,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem164,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem29,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem30,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem31,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem165,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem32,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage21.ResumeLayout(false)
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl1.ResumeLayout(false)
        CType(Me.MemoEdit14.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView10,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageFAPTInstruction.ResumeLayout(false)
        CType(Me.XtraTabControl4,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl4.ResumeLayout(false)
        Me.XtraTabPage22.ResumeLayout(false)
        CType(Me.GroupControl6,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl6.ResumeLayout(false)
        CType(Me.LayoutControl20,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl20.ResumeLayout(false)
        CType(Me.TextEdit68.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit10.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit10.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit16.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit69.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem36,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem37,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem170,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem171,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem172,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem173,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem38,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GroupControl5,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl5.ResumeLayout(false)
        CType(Me.LayoutControl19,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl19.ResumeLayout(false)
        CType(Me.TextEdit66.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit9.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit15.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit67.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem33,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem34,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem166,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem167,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem168,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem169,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem35,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage23.ResumeLayout(false)
        CType(Me.LayoutControl22,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl22.ResumeLayout(false)
        CType(Me.GridControl12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit136.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit137.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit138.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit139.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit140.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem180,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem181,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem182,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem183,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem184,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem185,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageTrusteeInformation.ResumeLayout(false)
        CType(Me.LayoutControl21,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl21.ResumeLayout(false)
        CType(Me.GridControl11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit131.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit132.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit133.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit134.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit135.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup20,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem174,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem175,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem176,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem177,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem178,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem179,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage24.ResumeLayout(false)
        Me.XtraTabPage24.PerformLayout
        CType(Me.TextEdit70.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit71.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit72.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit141.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit142.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView13,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage25.ResumeLayout(false)
        CType(Me.XtraTabControl5,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl5.ResumeLayout(false)
        Me.XtraTabPage29.ResumeLayout(false)
        CType(Me.GridControl14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView14,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage30.ResumeLayout(false)
        CType(Me.GridControl15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView15,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage31.ResumeLayout(false)
        CType(Me.GridControl16,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView16,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage26.ResumeLayout(false)
        CType(Me.LayoutControl23,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl23.ResumeLayout(false)
        CType(Me.CheckEdit143.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit144.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit145.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit146.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit147.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit148.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit149.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit150.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit151.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit152.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem39,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem186,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem187,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem188,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem189,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem190,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem191,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem192,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem193,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem194,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem195,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage27.ResumeLayout(false)
        CType(Me.GridControl17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView17,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage28.ResumeLayout(false)
        CType(Me.PanelControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl2.ResumeLayout(false)
        CType(Me.MemoEdit17.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView18,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageProbatePlan.ResumeLayout(false)
        CType(Me.XtraTabControl6,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl6.ResumeLayout(false)
        Me.XtraTabPage32.ResumeLayout(false)
        Me.XtraTabPage32.PerformLayout
        CType(Me.CheckEdit153.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit154.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl24,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl24.ResumeLayout(false)
        CType(Me.TextEdit73.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit11.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit18.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit74.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem196,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem197,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem198,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem199,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem40,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl25,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl25.ResumeLayout(false)
        CType(Me.TextEdit75.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit12.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit19.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit77.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup25,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem200,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem201,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem202,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem203,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem41,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage33.ResumeLayout(false)
        CType(Me.LayoutControl26,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl26.ResumeLayout(false)
        CType(Me.MemoEdit20.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup27,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem204,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageProbateAssessment.ResumeLayout(false)
        CType(Me.XtraTabControl7,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl7.ResumeLayout(false)
        Me.XtraTabPage42.ResumeLayout(false)
        Me.XtraTabPage42.PerformLayout
        CType(Me.GridControl26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit160.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit159.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage34.ResumeLayout(false)
        CType(Me.LayoutControl27,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl27.ResumeLayout(false)
        CType(Me.TextEdit78.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit76.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit21.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit155.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit156.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit13.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit13.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit157.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit158.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit14.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit14.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup28,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem205,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem42,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem206,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem207,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem208,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem209,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem210,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem211,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem212,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem213,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem214,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem215,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage35.ResumeLayout(false)
        CType(Me.GridControl19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView19,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage36.ResumeLayout(false)
        CType(Me.GridControl20,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView20,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage37.ResumeLayout(false)
        CType(Me.GridControl21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView21,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage38.ResumeLayout(false)
        CType(Me.GridControl22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView22,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage39.ResumeLayout(false)
        CType(Me.GridControl23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView23,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage40.ResumeLayout(false)
        CType(Me.GridControl24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView24,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage41.ResumeLayout(false)
        CType(Me.GridControl25,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView25,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage43.ResumeLayout(false)
        CType(Me.LayoutControl28,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl28.ResumeLayout(false)
        CType(Me.TextEdit79.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit80.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit81.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit82.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit83.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup29,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem216,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem217,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem218,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem219,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem220,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem43,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem10,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage44.ResumeLayout(false)
        Me.XtraTabPage44.PerformLayout
        CType(Me.LayoutControl29,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl29.ResumeLayout(false)
        CType(Me.MemoEdit22.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup30,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem221,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DockManager1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents NavigationPane1 As DevExpress.XtraBars.Navigation.NavigationPane
    Friend WithEvents NavigationPageRecommendationTool As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageWillInstruction As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageLPAInstruction As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageFAPTInstruction As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageProbatePlan As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageDigitalSignatures As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageProbateAssessment As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageClientDetails As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage2 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationFrame1 As DevExpress.XtraBars.Navigation.NavigationFrame
    Friend WithEvents NavigationPageAddNewClient As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage4 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditDual As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSingle As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditMs As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMiss As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMr As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMrs As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem1 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleSeparator1 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditMs2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMiss2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMrs2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents CheckEdit9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem2 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleSeparator2 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TabPane1 As DevExpress.XtraBars.Navigation.TabPane
    Friend WithEvents TabNavigationPage1 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage2 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage3 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage4 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage5 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents CheckEdit12 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit11 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit13 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit14 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit15 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit16 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit17 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit18 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit19 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit20 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit21 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit22 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit23 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit24 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit25 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem44 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit26 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit27 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit28 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem45 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem46 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem47 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumnAssetType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnDetail As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnClient1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnClient2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnJointTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnAction As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit29 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit30 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem48 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem49 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem50 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem51 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem52 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem53 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem54 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit31 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit32 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem55 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem56 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem57 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem58 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit21 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit33 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem59 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem60 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl6 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit22 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem61 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents TrackBarControl2 As DevExpress.XtraEditors.TrackBarControl
    Friend WithEvents TrackBarControl1 As DevExpress.XtraEditors.TrackBarControl
    Friend WithEvents TextEdit23 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit24 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ButtonEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem62 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem63 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem65 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem3 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents SimpleLabelItem4 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem64 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleLabelItem5 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem66 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents TextEdit25 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit26 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit27 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit28 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit29 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit30 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem67 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem68 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem69 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem70 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem71 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem72 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem73 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ButtonEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit34 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit35 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit36 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit37 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit38 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit31 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit32 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem74 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem75 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem76 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem77 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem79 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem78 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem80 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit33 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit41 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit42 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit40 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit39 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit39 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit42 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit47 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit46 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit45 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit44 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit43 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit41 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit38 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit37 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit36 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit35 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit34 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit40 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage7 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage8 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage9 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage10 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl7 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents MemoEdit3 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEdit48 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit49 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem81 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem82 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem83 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem84 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ComboBoxEdit4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit44 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit43 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl8 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents MemoEdit4 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents DateEdit4 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEdit50 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit51 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem85 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem86 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem87 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem88 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit46 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit45 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumnName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnDOB As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnRelationShipToClient1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnRelationshipToClient2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnActionInWillInstruction As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl9 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit47 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit48 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem89 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem90 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem91 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl10 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit49 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit50 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit51 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit52 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit53 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit54 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit55 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit56 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit57 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit58 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit59 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit60 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit61 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem92 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem93 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem94 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem95 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem96 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem97 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem98 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem11 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem99 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem100 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem12 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem101 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem102 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem103 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem104 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit62 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit63 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit64 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit65 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit66 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit67 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit68 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem110 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem105 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem13 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem106 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem107 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem108 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem109 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem111 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem14 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem112 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl12 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit72 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit73 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit74 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents MemoEdit6 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem116 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem15 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem117 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem118 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem7 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents EmptySpaceItem19 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem119 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl11 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit69 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit70 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit71 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents MemoEdit5 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents CheckEdit75 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit76 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem113 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem114 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem115 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem6 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents EmptySpaceItem17 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem121 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem120 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem122 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit77 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit78 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem123 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem124 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl13 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit79 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit80 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup12 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem125 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem126 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup13 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem127 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem128 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem129 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControl14 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit81 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit82 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit83 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage12 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage11 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit52 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SeparatorControl1 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents CheckEdit84 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit53 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SeparatorControl2 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents CheckEdit85 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SeparatorControl3 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents CheckEdit86 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit87 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SeparatorControl4 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents CheckEdit88 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit89 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SeparatorControl5 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents CheckEdit90 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit91 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SeparatorControl6 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents CheckEdit92 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit93 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit94 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit95 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit96 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit97 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridControl6 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents XtraTabControl3 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage13 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage14 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage15 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage16 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage17 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage18 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage19 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage20 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage21 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl15 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit54 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit5 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit7 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit55 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit98 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit99 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup14 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem130 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem131 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem132 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem133 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem16 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem18 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem134 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem135 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem20 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl16 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit56 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit6 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit8 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit57 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit100 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit101 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup15 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem136 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem137 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem138 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem21 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem139 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem22 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem140 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem141 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem23 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents CheckEdit102 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit103 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit104 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit105 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit106 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit107 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl7 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEdit108 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit109 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit110 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit111 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit9 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit112 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit113 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl8 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView8 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton12 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit114 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit115 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit116 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit117 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit118 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit119 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit120 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControl17 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit58 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit59 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit60 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit10 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit61 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit62 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit7 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents CheckEdit121 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup16 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem142 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem143 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem144 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem145 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem146 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem147 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem148 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem149 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem24 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem25 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem26 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem150 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem8 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents MemoEdit11 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents MemoEdit12 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SimpleButton14 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl9 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView9 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl18 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit122 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit123 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit124 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit125 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit126 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit127 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit128 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit129 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit130 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit63 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit13 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit64 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit65 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit8 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup17 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem151 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem152 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem153 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem154 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem155 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem27 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem156 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem157 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem158 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem159 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem160 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem161 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem28 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem162 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem163 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem164 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem29 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem30 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem31 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem165 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem32 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GridControl10 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView10 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn31 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents MemoEdit14 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl4 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage22 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage23 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage24 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage25 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage26 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage27 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage28 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl19 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit66 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit9 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit15 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit67 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup18 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem33 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem34 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem166 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem167 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem168 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem169 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem35 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl20 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit68 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit10 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit16 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit69 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup19 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem36 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem37 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem170 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem171 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem172 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem173 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem38 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents NavigationPageTrusteeInformation As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents LayoutControl21 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents GridControl11 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView11 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn34 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn35 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn36 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEdit131 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit132 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit133 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit134 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit135 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup20 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem174 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem175 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem176 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem177 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem178 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem179 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton17 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton16 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControl22 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents GridControl12 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView12 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn37 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn38 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn39 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn40 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEdit136 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit137 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit138 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit139 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit140 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup21 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem180 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem181 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem182 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem183 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem184 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem185 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton18 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton19 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl13 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView13 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn41 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn42 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn43 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn44 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn45 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEdit141 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit142 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonLookup5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit70 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit71 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit72 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl5 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage29 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage30 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage31 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SimpleButton21 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl14 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView14 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn46 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn47 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn48 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn49 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn50 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn51 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton22 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl15 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView15 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn52 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn53 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn54 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn55 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn56 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton23 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl16 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView16 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn57 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn58 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn59 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn60 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn61 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl23 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit143 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit144 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit145 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit146 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit147 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit148 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit149 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit150 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit151 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit152 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup22 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem39 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleLabelItem9 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem186 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem187 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem188 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem189 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem190 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem191 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem192 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem193 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem194 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem195 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl17 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView17 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn62 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn63 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn64 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridControl18 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView18 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn65 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn66 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn67 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit17 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents XtraTabControl6 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage32 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CheckEdit153 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit154 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl61 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControl24 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit73 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit11 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit18 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit74 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup23 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup24 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem196 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem197 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem198 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem199 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem40 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControl25 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit75 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit12 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit19 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit77 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup25 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup26 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem200 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem201 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem202 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem203 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem41 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents XtraTabPage33 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl26 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents MemoEdit20 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlGroup27 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem204 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabControl7 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage34 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage35 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage36 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage37 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage38 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage39 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage40 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage41 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage42 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage43 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage44 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl27 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup28 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TextEdit78 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit76 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton24 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton25 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem205 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem42 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem206 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem207 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem208 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents MemoEdit21 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem209 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit155 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit156 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DateEdit13 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents CheckEdit157 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit158 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DateEdit14 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem210 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem211 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem212 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem213 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem214 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem215 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton26 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl19 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView19 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn68 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn69 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn70 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn71 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn72 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton27 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl20 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView20 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn73 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn74 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn75 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn76 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn77 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton28 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl21 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView21 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn78 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn79 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn80 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn81 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn82 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton29 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl22 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView22 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn83 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn84 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn86 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn87 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton30 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl23 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView23 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn85 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn88 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn89 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn90 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton31 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl24 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView24 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn91 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn92 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn93 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn94 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn95 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridControl25 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView25 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn96 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn97 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn98 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn99 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn100 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton32 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl62 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl26 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView26 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn101 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn102 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn103 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn104 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn105 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton33 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckEdit160 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit159 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl63 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit22 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControl28 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit79 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit80 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit81 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit82 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup29 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem216 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem217 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem218 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem219 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEdit83 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem220 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem43 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleLabelItem10 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControl29 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup30 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem221 As DevExpress.XtraLayout.LayoutControlItem
End Class
