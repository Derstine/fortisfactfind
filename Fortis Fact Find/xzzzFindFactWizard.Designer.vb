﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xzzzFindFactWizard
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xzzzFindFactWizard))
        Dim CustomHeaderButtonImageOptions1 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.NavigationPane1 = New DevExpress.XtraBars.Navigation.NavigationPane()
        Me.NavigationPageProductServices = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TablePanel2 = New DevExpress.Utils.Layout.TablePanel()
        Me.CheckEditC1FirstRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1FirstRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1SecondRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1ThirdRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1FourthRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1FifthRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1SixthRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1SeventhRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1EightRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1NinethRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1TenthRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1SecondRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1ThirdRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1FourthRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1FifthRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1SixthRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1SeventhRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1EightRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1NinethRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1TenthRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1EleventhRowY = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditC1EleventhRowN = New DevExpress.XtraEditors.CheckEdit()
        Me.TablePanel1 = New DevExpress.Utils.Layout.TablePanel()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit23 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit24 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit25 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit26 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit27 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit28 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit29 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit30 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit31 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit32 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit33 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit34 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit35 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit36 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit37 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit38 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit39 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit40 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit43 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit44 = New DevExpress.XtraEditors.CheckEdit()
        Me.NavigationPageAssetInformation = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.TablePanel3 = New DevExpress.Utils.Layout.TablePanel()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.NavigationPageClientOffer = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPageProbateForecast = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPageFuneralForecast = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPageSavingSummary = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPageIHTCalculatorSolutions = New DevExpress.XtraBars.Navigation.NavigationPage()
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPane1.SuspendLayout
        Me.NavigationPageProductServices.SuspendLayout
        CType(Me.TablePanel2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TablePanel2.SuspendLayout
        CType(Me.CheckEditC1FirstRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1FirstRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1SecondRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1ThirdRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1FourthRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1FifthRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1SixthRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1SeventhRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1EightRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1NinethRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1TenthRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1SecondRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1ThirdRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1FourthRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1FifthRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1SixthRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1SeventhRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1EightRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1NinethRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1TenthRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1EleventhRowY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditC1EleventhRowN.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TablePanel1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TablePanel1.SuspendLayout
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit23.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit24.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit25.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit26.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit27.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit28.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit29.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit30.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit31.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit32.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit33.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit34.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit35.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit36.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit37.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit38.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit39.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit40.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit43.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit44.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageAssetInformation.SuspendLayout
        CType(Me.TablePanel3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TablePanel3.SuspendLayout
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'NavigationPane1
        '
        Me.NavigationPane1.Controls.Add(Me.NavigationPageProductServices)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageAssetInformation)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageSavingSummary)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageFuneralForecast)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageProbateForecast)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageClientOffer)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageIHTCalculatorSolutions)
        Me.NavigationPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPane1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.PageProperties.ShowCollapseButton = false
        Me.NavigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText
        Me.NavigationPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPageProductServices, Me.NavigationPageAssetInformation, Me.NavigationPageClientOffer, Me.NavigationPageProbateForecast, Me.NavigationPageFuneralForecast, Me.NavigationPageSavingSummary, Me.NavigationPageIHTCalculatorSolutions})
        Me.NavigationPane1.RegularSize = New System.Drawing.Size(1248, 652)
        Me.NavigationPane1.SelectedPage = Me.NavigationPageProductServices
        Me.NavigationPane1.Size = New System.Drawing.Size(1248, 652)
        Me.NavigationPane1.TabIndex = 1
        Me.NavigationPane1.Text = "NavigationPane1"
        '
        'NavigationPageProductServices
        '
        Me.NavigationPageProductServices.Caption = "Product & Services"
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl1)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl15)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl14)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl2)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl13)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl12)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl11)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl10)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl9)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl8)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl7)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl6)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl5)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl4)
        Me.NavigationPageProductServices.Controls.Add(Me.LabelControl3)
        Me.NavigationPageProductServices.Controls.Add(Me.TablePanel2)
        Me.NavigationPageProductServices.Controls.Add(Me.TablePanel1)
        Me.NavigationPageProductServices.ImageOptions.Image = CType(resources.GetObject("NavigationPageProductServices.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageProductServices.Name = "NavigationPageProductServices"
        Me.NavigationPageProductServices.Properties.ShowCollapseButton = DevExpress.Utils.DefaultBoolean.[False]
        Me.NavigationPageProductServices.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText
        Me.NavigationPageProductServices.Size = New System.Drawing.Size(1036, 592)
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.Options.UseFont = true
        Me.LabelControl1.Appearance.Options.UseTextOptions = true
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(860, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(173, 16)
        Me.LabelControl1.TabIndex = 9
        Me.LabelControl1.Text = "Client 2"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.BackColor = System.Drawing.Color.Gray
        Me.LabelControl15.Appearance.Options.UseBackColor = true
        Me.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl15.Location = New System.Drawing.Point(860, 28)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(173, 2)
        Me.LabelControl15.TabIndex = 8
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl14.Appearance.Options.UseFont = true
        Me.LabelControl14.Appearance.Options.UseTextOptions = true
        Me.LabelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl14.Location = New System.Drawing.Point(653, 8)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(173, 16)
        Me.LabelControl14.TabIndex = 7
        Me.LabelControl14.Text = "Client 1"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.BackColor = System.Drawing.Color.Gray
        Me.LabelControl2.Appearance.Options.UseBackColor = true
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(653, 28)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(173, 2)
        Me.LabelControl2.TabIndex = 6
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl13.Appearance.Options.UseFont = true
        Me.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl13.Location = New System.Drawing.Point(32, 446)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl13.TabIndex = 5
        Me.LabelControl13.Text = "Do you want to protect your assets for your beneficiaries and future generations?"& _ 
    ""
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl12.Appearance.Options.UseFont = true
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl12.Location = New System.Drawing.Point(32, 406)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl12.TabIndex = 5
        Me.LabelControl12.Text = "Have you set up Trusts to protect your assets from sideways disinheritance?"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl11.Appearance.Options.UseFont = true
        Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl11.Location = New System.Drawing.Point(32, 366)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl11.TabIndex = 5
        Me.LabelControl11.Text = "Do you want to take away the burden on loved ones and save thousands by paying no"& _ 
    "w?"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl10.Appearance.Options.UseFont = true
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl10.Location = New System.Drawing.Point(32, 326)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl10.TabIndex = 5
        Me.LabelControl10.Text = "Have you pre-paid your future probate?"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Appearance.Options.UseFont = true
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl9.Location = New System.Drawing.Point(32, 286)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl9.TabIndex = 5
        Me.LabelControl9.Text = "Do you want today's prices or to pay double in 12 years time?"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl8.Appearance.Options.UseFont = true
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl8.Location = New System.Drawing.Point(32, 246)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl8.TabIndex = 5
        Me.LabelControl8.Text = "Do you have a pre-paid funeral plan?"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl7.Appearance.Options.UseFont = true
        Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl7.Location = New System.Drawing.Point(32, 206)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl7.TabIndex = 5
        Me.LabelControl7.Text = "Do you want LPA's so your loved ones are able to act if you lose capacity?"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl6.Appearance.Options.UseFont = true
        Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl6.Location = New System.Drawing.Point(32, 166)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl6.TabIndex = 5
        Me.LabelControl6.Text = "Have you organised all Lasting Powers of Attorney for Property && Finance?"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl5.Appearance.Options.UseFont = true
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Location = New System.Drawing.Point(32, 126)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl5.TabIndex = 5
        Me.LabelControl5.Text = "Have you organised all Lasting Powers of Attorney for Health && Welfare?"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl4.Appearance.Options.UseFont = true
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Location = New System.Drawing.Point(32, 86)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl4.TabIndex = 5
        Me.LabelControl4.Text = "Do you want a professionally written will?"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl3.Appearance.Options.UseFont = true
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(32, 46)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(547, 16)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Have you got well written, satisfactory Will, nominating a professional Trustee a"& _ 
    "s an Executor?"
        '
        'TablePanel2
        '
        Me.TablePanel2.Columns.AddRange(New DevExpress.Utils.Layout.TablePanelColumn() {New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 50!), New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 50!)})
        Me.TablePanel2.Controls.Add(Me.CheckEditC1FirstRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1FirstRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1SecondRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1ThirdRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1FourthRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1FifthRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1SixthRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1SeventhRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1EightRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1NinethRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1TenthRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1SecondRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1ThirdRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1FourthRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1FifthRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1SixthRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1SeventhRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1EightRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1NinethRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1TenthRowN)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1EleventhRowY)
        Me.TablePanel2.Controls.Add(Me.CheckEditC1EleventhRowN)
        Me.TablePanel2.Location = New System.Drawing.Point(653, 34)
        Me.TablePanel2.Name = "TablePanel2"
        Me.TablePanel2.Rows.AddRange(New DevExpress.Utils.Layout.TablePanelRow() {New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!)})
        Me.TablePanel2.Size = New System.Drawing.Size(173, 506)
        Me.TablePanel2.TabIndex = 3
        '
        'CheckEditC1FirstRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1FirstRowN, 1)
        Me.CheckEditC1FirstRowN.Location = New System.Drawing.Point(90, 3)
        Me.CheckEditC1FirstRowN.Name = "CheckEditC1FirstRowN"
        Me.CheckEditC1FirstRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1FirstRowN, 0)
        Me.CheckEditC1FirstRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1FirstRowN.TabIndex = 2
        '
        'CheckEditC1FirstRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1FirstRowY, 0)
        Me.CheckEditC1FirstRowY.Location = New System.Drawing.Point(3, 3)
        Me.CheckEditC1FirstRowY.Name = "CheckEditC1FirstRowY"
        Me.CheckEditC1FirstRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1FirstRowY, 0)
        Me.CheckEditC1FirstRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1FirstRowY.TabIndex = 1
        '
        'CheckEditC1SecondRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1SecondRowY, 0)
        Me.CheckEditC1SecondRowY.Location = New System.Drawing.Point(3, 43)
        Me.CheckEditC1SecondRowY.Name = "CheckEditC1SecondRowY"
        Me.CheckEditC1SecondRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1SecondRowY, 1)
        Me.CheckEditC1SecondRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1SecondRowY.TabIndex = 1
        '
        'CheckEditC1ThirdRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1ThirdRowY, 0)
        Me.CheckEditC1ThirdRowY.Location = New System.Drawing.Point(3, 83)
        Me.CheckEditC1ThirdRowY.Name = "CheckEditC1ThirdRowY"
        Me.CheckEditC1ThirdRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1ThirdRowY, 2)
        Me.CheckEditC1ThirdRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1ThirdRowY.TabIndex = 1
        '
        'CheckEditC1FourthRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1FourthRowY, 0)
        Me.CheckEditC1FourthRowY.Location = New System.Drawing.Point(3, 123)
        Me.CheckEditC1FourthRowY.Name = "CheckEditC1FourthRowY"
        Me.CheckEditC1FourthRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1FourthRowY, 3)
        Me.CheckEditC1FourthRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1FourthRowY.TabIndex = 1
        '
        'CheckEditC1FifthRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1FifthRowY, 0)
        Me.CheckEditC1FifthRowY.Location = New System.Drawing.Point(3, 163)
        Me.CheckEditC1FifthRowY.Name = "CheckEditC1FifthRowY"
        Me.CheckEditC1FifthRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1FifthRowY, 4)
        Me.CheckEditC1FifthRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1FifthRowY.TabIndex = 1
        '
        'CheckEditC1SixthRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1SixthRowY, 0)
        Me.CheckEditC1SixthRowY.Location = New System.Drawing.Point(3, 203)
        Me.CheckEditC1SixthRowY.Name = "CheckEditC1SixthRowY"
        Me.CheckEditC1SixthRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1SixthRowY, 5)
        Me.CheckEditC1SixthRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1SixthRowY.TabIndex = 1
        '
        'CheckEditC1SeventhRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1SeventhRowY, 0)
        Me.CheckEditC1SeventhRowY.Location = New System.Drawing.Point(3, 243)
        Me.CheckEditC1SeventhRowY.Name = "CheckEditC1SeventhRowY"
        Me.CheckEditC1SeventhRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1SeventhRowY, 6)
        Me.CheckEditC1SeventhRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1SeventhRowY.TabIndex = 1
        '
        'CheckEditC1EightRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1EightRowY, 0)
        Me.CheckEditC1EightRowY.Location = New System.Drawing.Point(3, 283)
        Me.CheckEditC1EightRowY.Name = "CheckEditC1EightRowY"
        Me.CheckEditC1EightRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1EightRowY, 7)
        Me.CheckEditC1EightRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1EightRowY.TabIndex = 1
        '
        'CheckEditC1NinethRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1NinethRowY, 0)
        Me.CheckEditC1NinethRowY.Location = New System.Drawing.Point(3, 323)
        Me.CheckEditC1NinethRowY.Name = "CheckEditC1NinethRowY"
        Me.CheckEditC1NinethRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1NinethRowY, 8)
        Me.CheckEditC1NinethRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1NinethRowY.TabIndex = 1
        '
        'CheckEditC1TenthRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1TenthRowY, 0)
        Me.CheckEditC1TenthRowY.Location = New System.Drawing.Point(3, 363)
        Me.CheckEditC1TenthRowY.Name = "CheckEditC1TenthRowY"
        Me.CheckEditC1TenthRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1TenthRowY, 9)
        Me.CheckEditC1TenthRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1TenthRowY.TabIndex = 1
        '
        'CheckEditC1SecondRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1SecondRowN, 1)
        Me.CheckEditC1SecondRowN.Location = New System.Drawing.Point(90, 43)
        Me.CheckEditC1SecondRowN.Name = "CheckEditC1SecondRowN"
        Me.CheckEditC1SecondRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1SecondRowN, 1)
        Me.CheckEditC1SecondRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1SecondRowN.TabIndex = 2
        '
        'CheckEditC1ThirdRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1ThirdRowN, 1)
        Me.CheckEditC1ThirdRowN.Location = New System.Drawing.Point(90, 83)
        Me.CheckEditC1ThirdRowN.Name = "CheckEditC1ThirdRowN"
        Me.CheckEditC1ThirdRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1ThirdRowN, 2)
        Me.CheckEditC1ThirdRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1ThirdRowN.TabIndex = 2
        '
        'CheckEditC1FourthRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1FourthRowN, 1)
        Me.CheckEditC1FourthRowN.Location = New System.Drawing.Point(90, 123)
        Me.CheckEditC1FourthRowN.Name = "CheckEditC1FourthRowN"
        Me.CheckEditC1FourthRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1FourthRowN, 3)
        Me.CheckEditC1FourthRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1FourthRowN.TabIndex = 2
        '
        'CheckEditC1FifthRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1FifthRowN, 1)
        Me.CheckEditC1FifthRowN.Location = New System.Drawing.Point(90, 163)
        Me.CheckEditC1FifthRowN.Name = "CheckEditC1FifthRowN"
        Me.CheckEditC1FifthRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1FifthRowN, 4)
        Me.CheckEditC1FifthRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1FifthRowN.TabIndex = 2
        '
        'CheckEditC1SixthRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1SixthRowN, 1)
        Me.CheckEditC1SixthRowN.Location = New System.Drawing.Point(90, 203)
        Me.CheckEditC1SixthRowN.Name = "CheckEditC1SixthRowN"
        Me.CheckEditC1SixthRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1SixthRowN, 5)
        Me.CheckEditC1SixthRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1SixthRowN.TabIndex = 2
        '
        'CheckEditC1SeventhRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1SeventhRowN, 1)
        Me.CheckEditC1SeventhRowN.Location = New System.Drawing.Point(90, 243)
        Me.CheckEditC1SeventhRowN.Name = "CheckEditC1SeventhRowN"
        Me.CheckEditC1SeventhRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1SeventhRowN, 6)
        Me.CheckEditC1SeventhRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1SeventhRowN.TabIndex = 2
        '
        'CheckEditC1EightRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1EightRowN, 1)
        Me.CheckEditC1EightRowN.Location = New System.Drawing.Point(90, 283)
        Me.CheckEditC1EightRowN.Name = "CheckEditC1EightRowN"
        Me.CheckEditC1EightRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1EightRowN, 7)
        Me.CheckEditC1EightRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1EightRowN.TabIndex = 2
        '
        'CheckEditC1NinethRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1NinethRowN, 1)
        Me.CheckEditC1NinethRowN.Location = New System.Drawing.Point(90, 323)
        Me.CheckEditC1NinethRowN.Name = "CheckEditC1NinethRowN"
        Me.CheckEditC1NinethRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1NinethRowN, 8)
        Me.CheckEditC1NinethRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1NinethRowN.TabIndex = 2
        '
        'CheckEditC1TenthRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1TenthRowN, 1)
        Me.CheckEditC1TenthRowN.Location = New System.Drawing.Point(90, 363)
        Me.CheckEditC1TenthRowN.Name = "CheckEditC1TenthRowN"
        Me.CheckEditC1TenthRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1TenthRowN, 9)
        Me.CheckEditC1TenthRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1TenthRowN.TabIndex = 2
        '
        'CheckEditC1EleventhRowY
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1EleventhRowY, 0)
        Me.CheckEditC1EleventhRowY.Location = New System.Drawing.Point(3, 403)
        Me.CheckEditC1EleventhRowY.Name = "CheckEditC1EleventhRowY"
        Me.CheckEditC1EleventhRowY.Properties.Caption = "YES"
        Me.TablePanel2.SetRow(Me.CheckEditC1EleventhRowY, 10)
        Me.CheckEditC1EleventhRowY.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1EleventhRowY.TabIndex = 1
        '
        'CheckEditC1EleventhRowN
        '
        Me.TablePanel2.SetColumn(Me.CheckEditC1EleventhRowN, 1)
        Me.CheckEditC1EleventhRowN.Location = New System.Drawing.Point(90, 403)
        Me.CheckEditC1EleventhRowN.Name = "CheckEditC1EleventhRowN"
        Me.CheckEditC1EleventhRowN.Properties.Caption = "NO"
        Me.TablePanel2.SetRow(Me.CheckEditC1EleventhRowN, 10)
        Me.CheckEditC1EleventhRowN.Size = New System.Drawing.Size(81, 34)
        Me.CheckEditC1EleventhRowN.TabIndex = 2
        '
        'TablePanel1
        '
        Me.TablePanel1.Columns.AddRange(New DevExpress.Utils.Layout.TablePanelColumn() {New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 50!), New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 50!)})
        Me.TablePanel1.Controls.Add(Me.CheckEdit2)
        Me.TablePanel1.Controls.Add(Me.CheckEdit1)
        Me.TablePanel1.Controls.Add(Me.CheckEdit23)
        Me.TablePanel1.Controls.Add(Me.CheckEdit24)
        Me.TablePanel1.Controls.Add(Me.CheckEdit25)
        Me.TablePanel1.Controls.Add(Me.CheckEdit26)
        Me.TablePanel1.Controls.Add(Me.CheckEdit27)
        Me.TablePanel1.Controls.Add(Me.CheckEdit28)
        Me.TablePanel1.Controls.Add(Me.CheckEdit29)
        Me.TablePanel1.Controls.Add(Me.CheckEdit30)
        Me.TablePanel1.Controls.Add(Me.CheckEdit31)
        Me.TablePanel1.Controls.Add(Me.CheckEdit32)
        Me.TablePanel1.Controls.Add(Me.CheckEdit33)
        Me.TablePanel1.Controls.Add(Me.CheckEdit34)
        Me.TablePanel1.Controls.Add(Me.CheckEdit35)
        Me.TablePanel1.Controls.Add(Me.CheckEdit36)
        Me.TablePanel1.Controls.Add(Me.CheckEdit37)
        Me.TablePanel1.Controls.Add(Me.CheckEdit38)
        Me.TablePanel1.Controls.Add(Me.CheckEdit39)
        Me.TablePanel1.Controls.Add(Me.CheckEdit40)
        Me.TablePanel1.Controls.Add(Me.CheckEdit43)
        Me.TablePanel1.Controls.Add(Me.CheckEdit44)
        Me.TablePanel1.Location = New System.Drawing.Point(860, 34)
        Me.TablePanel1.Name = "TablePanel1"
        Me.TablePanel1.Rows.AddRange(New DevExpress.Utils.Layout.TablePanelRow() {New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Absolute, 40!)})
        Me.TablePanel1.Size = New System.Drawing.Size(173, 506)
        Me.TablePanel1.TabIndex = 0
        '
        'CheckEdit2
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit2, 1)
        Me.CheckEdit2.Location = New System.Drawing.Point(90, 3)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit2, 0)
        Me.CheckEdit2.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit2.TabIndex = 2
        '
        'CheckEdit1
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit1, 0)
        Me.CheckEdit1.Location = New System.Drawing.Point(3, 3)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit1, 0)
        Me.CheckEdit1.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit1.TabIndex = 1
        '
        'CheckEdit23
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit23, 0)
        Me.CheckEdit23.Location = New System.Drawing.Point(3, 43)
        Me.CheckEdit23.Name = "CheckEdit23"
        Me.CheckEdit23.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit23, 1)
        Me.CheckEdit23.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit23.TabIndex = 1
        '
        'CheckEdit24
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit24, 0)
        Me.CheckEdit24.Location = New System.Drawing.Point(3, 83)
        Me.CheckEdit24.Name = "CheckEdit24"
        Me.CheckEdit24.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit24, 2)
        Me.CheckEdit24.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit24.TabIndex = 1
        '
        'CheckEdit25
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit25, 0)
        Me.CheckEdit25.Location = New System.Drawing.Point(3, 123)
        Me.CheckEdit25.Name = "CheckEdit25"
        Me.CheckEdit25.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit25, 3)
        Me.CheckEdit25.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit25.TabIndex = 1
        '
        'CheckEdit26
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit26, 0)
        Me.CheckEdit26.Location = New System.Drawing.Point(3, 163)
        Me.CheckEdit26.Name = "CheckEdit26"
        Me.CheckEdit26.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit26, 4)
        Me.CheckEdit26.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit26.TabIndex = 1
        '
        'CheckEdit27
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit27, 0)
        Me.CheckEdit27.Location = New System.Drawing.Point(3, 203)
        Me.CheckEdit27.Name = "CheckEdit27"
        Me.CheckEdit27.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit27, 5)
        Me.CheckEdit27.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit27.TabIndex = 1
        '
        'CheckEdit28
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit28, 0)
        Me.CheckEdit28.Location = New System.Drawing.Point(3, 243)
        Me.CheckEdit28.Name = "CheckEdit28"
        Me.CheckEdit28.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit28, 6)
        Me.CheckEdit28.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit28.TabIndex = 1
        '
        'CheckEdit29
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit29, 0)
        Me.CheckEdit29.Location = New System.Drawing.Point(3, 283)
        Me.CheckEdit29.Name = "CheckEdit29"
        Me.CheckEdit29.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit29, 7)
        Me.CheckEdit29.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit29.TabIndex = 1
        '
        'CheckEdit30
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit30, 0)
        Me.CheckEdit30.Location = New System.Drawing.Point(3, 323)
        Me.CheckEdit30.Name = "CheckEdit30"
        Me.CheckEdit30.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit30, 8)
        Me.CheckEdit30.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit30.TabIndex = 1
        '
        'CheckEdit31
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit31, 0)
        Me.CheckEdit31.Location = New System.Drawing.Point(3, 363)
        Me.CheckEdit31.Name = "CheckEdit31"
        Me.CheckEdit31.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit31, 9)
        Me.CheckEdit31.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit31.TabIndex = 1
        '
        'CheckEdit32
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit32, 1)
        Me.CheckEdit32.Location = New System.Drawing.Point(90, 43)
        Me.CheckEdit32.Name = "CheckEdit32"
        Me.CheckEdit32.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit32, 1)
        Me.CheckEdit32.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit32.TabIndex = 2
        '
        'CheckEdit33
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit33, 1)
        Me.CheckEdit33.Location = New System.Drawing.Point(90, 83)
        Me.CheckEdit33.Name = "CheckEdit33"
        Me.CheckEdit33.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit33, 2)
        Me.CheckEdit33.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit33.TabIndex = 2
        '
        'CheckEdit34
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit34, 1)
        Me.CheckEdit34.Location = New System.Drawing.Point(90, 123)
        Me.CheckEdit34.Name = "CheckEdit34"
        Me.CheckEdit34.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit34, 3)
        Me.CheckEdit34.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit34.TabIndex = 2
        '
        'CheckEdit35
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit35, 1)
        Me.CheckEdit35.Location = New System.Drawing.Point(90, 163)
        Me.CheckEdit35.Name = "CheckEdit35"
        Me.CheckEdit35.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit35, 4)
        Me.CheckEdit35.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit35.TabIndex = 2
        '
        'CheckEdit36
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit36, 1)
        Me.CheckEdit36.Location = New System.Drawing.Point(90, 203)
        Me.CheckEdit36.Name = "CheckEdit36"
        Me.CheckEdit36.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit36, 5)
        Me.CheckEdit36.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit36.TabIndex = 2
        '
        'CheckEdit37
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit37, 1)
        Me.CheckEdit37.Location = New System.Drawing.Point(90, 243)
        Me.CheckEdit37.Name = "CheckEdit37"
        Me.CheckEdit37.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit37, 6)
        Me.CheckEdit37.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit37.TabIndex = 2
        '
        'CheckEdit38
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit38, 1)
        Me.CheckEdit38.Location = New System.Drawing.Point(90, 283)
        Me.CheckEdit38.Name = "CheckEdit38"
        Me.CheckEdit38.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit38, 7)
        Me.CheckEdit38.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit38.TabIndex = 2
        '
        'CheckEdit39
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit39, 1)
        Me.CheckEdit39.Location = New System.Drawing.Point(90, 323)
        Me.CheckEdit39.Name = "CheckEdit39"
        Me.CheckEdit39.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit39, 8)
        Me.CheckEdit39.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit39.TabIndex = 2
        '
        'CheckEdit40
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit40, 1)
        Me.CheckEdit40.Location = New System.Drawing.Point(90, 363)
        Me.CheckEdit40.Name = "CheckEdit40"
        Me.CheckEdit40.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit40, 9)
        Me.CheckEdit40.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit40.TabIndex = 2
        '
        'CheckEdit43
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit43, 0)
        Me.CheckEdit43.Location = New System.Drawing.Point(3, 403)
        Me.CheckEdit43.Name = "CheckEdit43"
        Me.CheckEdit43.Properties.Caption = "YES"
        Me.TablePanel1.SetRow(Me.CheckEdit43, 10)
        Me.CheckEdit43.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit43.TabIndex = 1
        '
        'CheckEdit44
        '
        Me.TablePanel1.SetColumn(Me.CheckEdit44, 1)
        Me.CheckEdit44.Location = New System.Drawing.Point(90, 403)
        Me.CheckEdit44.Name = "CheckEdit44"
        Me.CheckEdit44.Properties.Caption = "NO"
        Me.TablePanel1.SetRow(Me.CheckEdit44, 10)
        Me.CheckEdit44.Size = New System.Drawing.Size(81, 34)
        Me.CheckEdit44.TabIndex = 2
        '
        'NavigationPageAssetInformation
        '
        Me.NavigationPageAssetInformation.Caption = "Asset Information"
        Me.NavigationPageAssetInformation.Controls.Add(Me.TablePanel3)
        Me.NavigationPageAssetInformation.Controls.Add(Me.GridControl1)
        CustomHeaderButtonImageOptions1.SvgImage = CType(resources.GetObject("CustomHeaderButtonImageOptions1.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.NavigationPageAssetInformation.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Asset", true, CustomHeaderButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject1, Nothing, -1)})
        Me.NavigationPageAssetInformation.ImageOptions.Image = CType(resources.GetObject("NavigationPageAssetInformation.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageAssetInformation.Name = "NavigationPageAssetInformation"
        Me.NavigationPageAssetInformation.Size = New System.Drawing.Size(1036, 579)
        '
        'TablePanel3
        '
        Me.TablePanel3.Columns.AddRange(New DevExpress.Utils.Layout.TablePanelColumn() {New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 99.25!), New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 32.75!), New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 26.5!), New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 32.5!), New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 29!), New DevExpress.Utils.Layout.TablePanelColumn(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 40!)})
        Me.TablePanel3.Controls.Add(Me.LabelControl16)
        Me.TablePanel3.Controls.Add(Me.LabelControl17)
        Me.TablePanel3.Controls.Add(Me.LabelControl18)
        Me.TablePanel3.Controls.Add(Me.LabelControl19)
        Me.TablePanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TablePanel3.Location = New System.Drawing.Point(0, 396)
        Me.TablePanel3.Name = "TablePanel3"
        Me.TablePanel3.Rows.AddRange(New DevExpress.Utils.Layout.TablePanelRow() {New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 26!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 26!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 26!), New DevExpress.Utils.Layout.TablePanelRow(DevExpress.Utils.Layout.TablePanelEntityStyle.Relative, 26!)})
        Me.TablePanel3.Size = New System.Drawing.Size(1036, 183)
        Me.TablePanel3.TabIndex = 1
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl16.Appearance.Options.UseFont = true
        Me.LabelControl16.Appearance.Options.UseTextOptions = true
        Me.LabelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.TablePanel3.SetColumn(Me.LabelControl16, 0)
        Me.LabelControl16.Location = New System.Drawing.Point(3, 15)
        Me.LabelControl16.Name = "LabelControl16"
        Me.TablePanel3.SetRow(Me.LabelControl16, 0)
        Me.LabelControl16.Size = New System.Drawing.Size(389, 16)
        Me.LabelControl16.TabIndex = 0
        Me.LabelControl16.Text = "Total Assets"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl17.Appearance.Options.UseFont = true
        Me.LabelControl17.Appearance.Options.UseTextOptions = true
        Me.LabelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.TablePanel3.SetColumn(Me.LabelControl17, 0)
        Me.LabelControl17.Location = New System.Drawing.Point(3, 61)
        Me.LabelControl17.Name = "LabelControl17"
        Me.TablePanel3.SetRow(Me.LabelControl17, 1)
        Me.LabelControl17.Size = New System.Drawing.Size(389, 16)
        Me.LabelControl17.TabIndex = 0
        Me.LabelControl17.Text = "Total gross income including everything"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl18.Appearance.Options.UseFont = true
        Me.LabelControl18.Appearance.Options.UseTextOptions = true
        Me.LabelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.TablePanel3.SetColumn(Me.LabelControl18, 0)
        Me.LabelControl18.Location = New System.Drawing.Point(3, 107)
        Me.LabelControl18.Name = "LabelControl18"
        Me.TablePanel3.SetRow(Me.LabelControl18, 2)
        Me.LabelControl18.Size = New System.Drawing.Size(389, 16)
        Me.LabelControl18.TabIndex = 0
        Me.LabelControl18.Text = "How much emergency cash should you keep in the bank?"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl19.Appearance.Options.UseFont = true
        Me.LabelControl19.Appearance.Options.UseTextOptions = true
        Me.LabelControl19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.TablePanel3.SetColumn(Me.LabelControl19, 0)
        Me.LabelControl19.Location = New System.Drawing.Point(3, 152)
        Me.LabelControl19.Name = "LabelControl19"
        Me.TablePanel3.SetRow(Me.LabelControl19, 3)
        Me.LabelControl19.Size = New System.Drawing.Size(389, 16)
        Me.LabelControl19.TabIndex = 0
        Me.LabelControl19.Text = "How do you feel about your current levels of return?"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1036, 395)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Asset Type"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = true
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 112
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Detail"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = true
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 112
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Client 1"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = true
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 112
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Client 2"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = true
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 112
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Joint Total"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = true
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 112
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "In Trust"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = true
        Me.GridColumn6.VisibleIndex = 5
        Me.GridColumn6.Width = 112
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Income"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = true
        Me.GridColumn7.VisibleIndex = 6
        Me.GridColumn7.Width = 112
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Returns"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = true
        Me.GridColumn8.VisibleIndex = 7
        Me.GridColumn8.Width = 140
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Action"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = true
        Me.GridColumn9.VisibleIndex = 8
        Me.GridColumn9.Width = 91
        '
        'NavigationPageClientOffer
        '
        Me.NavigationPageClientOffer.Caption = "Client Offer"
        Me.NavigationPageClientOffer.ImageOptions.Image = CType(resources.GetObject("NavigationPageClientOffer.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageClientOffer.Name = "NavigationPageClientOffer"
        Me.NavigationPageClientOffer.Size = New System.Drawing.Size(1036, 592)
        '
        'NavigationPageProbateForecast
        '
        Me.NavigationPageProbateForecast.Caption = "Probate Forecast"
        Me.NavigationPageProbateForecast.ImageOptions.Image = CType(resources.GetObject("NavigationPageProbateForecast.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageProbateForecast.Name = "NavigationPageProbateForecast"
        Me.NavigationPageProbateForecast.Size = New System.Drawing.Size(1036, 592)
        '
        'NavigationPageFuneralForecast
        '
        Me.NavigationPageFuneralForecast.Caption = "Funeral Forecast"
        Me.NavigationPageFuneralForecast.ImageOptions.Image = CType(resources.GetObject("NavigationPageFuneralForecast.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageFuneralForecast.Name = "NavigationPageFuneralForecast"
        Me.NavigationPageFuneralForecast.Size = New System.Drawing.Size(1036, 592)
        '
        'NavigationPageSavingSummary
        '
        Me.NavigationPageSavingSummary.Caption = "Saving Summary"
        Me.NavigationPageSavingSummary.ImageOptions.Image = CType(resources.GetObject("NavigationPageSavingSummary.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageSavingSummary.Name = "NavigationPageSavingSummary"
        Me.NavigationPageSavingSummary.Size = New System.Drawing.Size(1036, 592)
        '
        'NavigationPageIHTCalculatorSolutions
        '
        Me.NavigationPageIHTCalculatorSolutions.Caption = "IHT Calculator Solutions"
        Me.NavigationPageIHTCalculatorSolutions.ImageOptions.Image = CType(resources.GetObject("NavigationPageIHTCalculatorSolutions.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageIHTCalculatorSolutions.Name = "NavigationPageIHTCalculatorSolutions"
        Me.NavigationPageIHTCalculatorSolutions.Size = New System.Drawing.Size(1036, 592)
        '
        'xzzzFindFactWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1248, 652)
        Me.Controls.Add(Me.NavigationPane1)
        Me.Name = "xzzzFindFactWizard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Find Fact Wizard"
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPane1.ResumeLayout(false)
        Me.NavigationPageProductServices.ResumeLayout(false)
        CType(Me.TablePanel2,System.ComponentModel.ISupportInitialize).EndInit
        Me.TablePanel2.ResumeLayout(false)
        CType(Me.CheckEditC1FirstRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1FirstRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1SecondRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1ThirdRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1FourthRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1FifthRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1SixthRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1SeventhRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1EightRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1NinethRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1TenthRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1SecondRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1ThirdRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1FourthRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1FifthRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1SixthRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1SeventhRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1EightRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1NinethRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1TenthRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1EleventhRowY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditC1EleventhRowN.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TablePanel1,System.ComponentModel.ISupportInitialize).EndInit
        Me.TablePanel1.ResumeLayout(false)
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit23.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit24.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit25.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit26.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit27.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit28.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit29.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit30.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit31.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit32.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit33.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit34.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit35.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit36.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit37.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit38.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit39.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit40.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit43.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit44.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageAssetInformation.ResumeLayout(false)
        CType(Me.TablePanel3,System.ComponentModel.ISupportInitialize).EndInit
        Me.TablePanel3.ResumeLayout(false)
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents NavigationPane1 As DevExpress.XtraBars.Navigation.NavigationPane
    Friend WithEvents NavigationPageProductServices As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageAssetInformation As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageClientOffer As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageProbateForecast As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageFuneralForecast As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageSavingSummary As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageIHTCalculatorSolutions As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents TablePanel1 As DevExpress.Utils.Layout.TablePanel
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TablePanel2 As DevExpress.Utils.Layout.TablePanel
    Friend WithEvents CheckEditC1FirstRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1FirstRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditC1SecondRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1ThirdRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1FourthRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1FifthRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1SixthRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1SeventhRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1EightRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1NinethRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1TenthRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1SecondRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1ThirdRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1FourthRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1FifthRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1SixthRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1SeventhRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1EightRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1NinethRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1TenthRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit23 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit24 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit25 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit26 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit27 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit28 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit29 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit30 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit31 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit32 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit33 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit34 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit35 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit36 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit37 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit38 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit39 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit40 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditC1EleventhRowY As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditC1EleventhRowN As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit43 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit44 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TablePanel3 As DevExpress.Utils.Layout.TablePanel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
End Class
