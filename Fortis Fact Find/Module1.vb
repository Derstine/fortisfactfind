﻿Imports System.Configuration
Imports System.Web
Imports System.Net
Imports System.IO
Imports System.Text


Public Module GlobalFunctions

    ''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''
    '' this holds global fields that allow easy
    '' customisation for other clients ;-)
    ''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''

    Public baseDatabaseFilename As String = "fortis.accdb"
    Public remoteDatabaseSourceURL As String = "https://factfind.fortis-law.co.uk/db/"
    Public remoteSoftwareURL As String = "https://factfind.fortis-law.co.uk/software/"
    Public remoteDatabaseSourceAddress As String = remoteDatabaseSourceURL + baseDatabaseFilename

    Public ftpAddress = "ftp://109.228.60.121/"
    Public ftpUsername As String = "ff-fortislaw"
    Public ftpPassword As String = "Pr1vate2007"

    Public remoteDebugFTPAddress As String = ftpAddress + "debug/"
    Public remoteBackupFTPAddress As String = ftpAddress + "adviserBackups/"
    Public remoteRecordingFTPAddress As String = ftpAddress + "recordings/"
    Public remoteSignaturesFTPAddress As String = ftpAddress + "signatures/"

    Public remoteSyncURL As String = "http://factfind.fortis-law.co.uk/sync/"

    Public vatMultiplier As Double = 1.2
    Public inflationRate As Double = 3



    ''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''
    '' this module holds all the global 
    '' functions, variables and connections
    ''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''

    Public localFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)

    Public filesPath As String = localFolder
    Public connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filesPath + "\" + baseDatabaseFilename + ";Persist Security Info=True"

    Public clientID As String = ""
    Public rowID As String = ""
    Public VAT As Single = 1.2  'set the VAT rate for all calculations
    Public searchPostcode As String = ""

    Public fee_will As Single = 180 'these fees will be downloaded from the server so it's always the most up to date
    Public fee_trust As Single = 1800
    Public fee_lpa_hw As Single = 322
    Public fee_lpa_pf As Single = 322
    Public fee_funeral As Single = 0
    Public fee_probate As Single = 0
    Public whoDied As String = ""
    Public username As String = ""
    Public internetFlag As String = ""



    Public Sub CenterForm(ByVal frm As Form, Optional ByVal parent As Form = Nothing)
        '' Note: call this from frm's Load event!
        Dim r As Rectangle
        If parent IsNot Nothing Then
            r = parent.RectangleToScreen(parent.ClientRectangle)
        Else
            r = Screen.FromPoint(frm.Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - frm.Width) \ 2
        Dim y = r.Top + (r.Height - frm.Height) \ 2
        frm.Location = New Point(x, y)
    End Sub

    Public Function resetForm(ByVal frm As Form)
        ' this function clears all data entry boxes in any particular form as VB has a horrible habit of remembering keyed in data
        Dim a As Control

        For Each a In frm.Controls

            If TypeOf a Is TextBox Then
                a.Text = Nothing

            End If

            If TypeOf a Is ComboBox Then
                a.Text = Nothing
            End If
        Next
        frm.Refresh()
        Return True

    End Function

    Public Function SqlSafe(strInput As String)
        ''' *** reformats the data field in preparation for SQL string to allow for quotes and apostrophes within data values
        If strInput IsNot Nothing Then
            Dim SqlSafeTxt As String
            SqlSafeTxt = strInput.Replace("'", "''")
            SqlSafeTxt = SqlSafeTxt.Replace("""", """""")
            Return SqlSafeTxt
        Else
            Return ""
        End If


    End Function

    Public Function runSQL(sql As String)
        ''' *** runs the SQL string, without returning a value
        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        command.ExecuteNonQuery()
        dbConn.Close()
        Return True
    End Function

    Public Function runSQLwithID(sql As String)
        ''' *** runs the SQL string and returns the first field 
        Dim returnVal As String = ""
        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        If dbreader.HasRows Then
            dbreader.Read()
            returnVal = dbreader.GetValue(0).ToString
        Else
            returnVal = ""
        End If
        dbreader.Close()
        dbConn.Close()
        Return returnVal
    End Function

    Public Function runSQLwithArray(sql As String)
        ''' *** runs the SQL string and returns the first field 
        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader

        If dbreader.HasRows Then
            Dim numfields As Int16 = dbreader.FieldCount
            Dim returnVal(numfields) As String
            dbreader.Read()
            For x = 0 To (numfields - 1)
                returnVal(x) = dbreader.GetValue(x).ToString
            Next
            dbreader.Close()
            dbConn.Close()
            Return returnVal
        Else
            Dim returnval(1) As String
            returnval(0) = "ERROR"
            dbreader.Close()
            dbConn.Close()
            Return returnval
        End If
        dbreader.Close()
        dbConn.Close()
        Return False
    End Function

    Public Function getWillLookup(fieldName As String)
        Dim sql As String
        sql = "select fieldValue from client_wills_lookup where willClientSequence=" + clientID + " and fieldName='" + fieldName + "'"
        Return runSQLwithID(sql)
    End Function

    Public Function setWillLookup(fieldName As String, fieldValue As String)
        Dim sql As String

        'first delete lookup value if it exists
        sql = "delete from client_wills_lookup where willClientSequence=" + clientID + " and fieldName='" + fieldName + "'"
        runSQL(sql)

        'now insert
        sql = "insert into client_wills_lookup (willClientSequence,fieldName,fieldValue) values (" + clientID + ",'" + SqlSafe(fieldName) + "','" + SqlSafe(fieldValue) + "')"
        runSQL(sql)

        Return True
    End Function

    Public Function setLPALookup(fieldName As String, fieldValue As String)
        Dim sql As String

        'first delete lookup value if it exists
        sql = "delete from client_LPA_lookup where willClientSequence=" + clientID + " and fieldName='" + fieldName + "'"
        runSQL(sql)

        'now insert
        sql = "insert into client_LPA_lookup (willClientSequence,fieldName,fieldValue) values (" + clientID + ",'" + SqlSafe(fieldName) + "','" + SqlSafe(fieldValue) + "')"
        runSQL(sql)

        Return True
    End Function

    Public Function getLPALookup(fieldName As String)
        Dim sql As String
        sql = "select fieldValue from client_LPA_lookup where willClientSequence=" + clientID + " and fieldName='" + fieldName + "'"
        Return runSQLwithID(sql)
    End Function



    Public Function clientType()
        Dim sql As String
        sql = "select typeOfWill from will_instruction_stage1_data where willClientSequence=" + clientID
        Return runSQLwithID(sql)
    End Function

    Public Function getClientFullName(ByVal whichClient As String)
        Dim sql As String
        Dim clientName As Array
        Dim forenames As Array
        Dim returnVal As String = ""

        If whichClient = "client1" Then
            sql = "select person1Forenames,person1Surname from will_instruction_stage2_data where willClientSequence=" + clientID
        Else
            sql = "select person2Forenames,person2Surname from will_instruction_stage2_data where willClientSequence=" + clientID
        End If

        clientName = runSQLwithArray(sql)
        forenames = clientName(0).split(" ")
        returnVal = forenames(0) + " " + clientName(1)
        Return returnVal
    End Function

    Public Function getClientFirstName(ByVal whichClient As String)
        Dim sql As String
        Dim clientName As Array
        Dim forenames As Array
        Dim returnVal As String = ""

        If whichClient = "client1" Then
            sql = "select person1Forenames,person1Surname from will_instruction_stage2_data where willClientSequence=" + clientID
        Else
            sql = "select person2Forenames,person2Surname from will_instruction_stage2_data where willClientSequence=" + clientID
        End If

        clientName = runSQLwithArray(sql)
        forenames = clientName(0).split(" ")
        returnVal = forenames(0)
        Return returnVal
    End Function

    Public Function getClientSurname(ByVal whichClient As String)
        Dim sql As String
        Dim clientName As Array
        Dim returnVal As String = ""

        If whichClient = "client1" Then
            sql = "select person1Forenames,person1Surname from will_instruction_stage2_data where willClientSequence=" + clientID
        Else
            sql = "select person2Forenames,person2Surname from will_instruction_stage2_data where willClientSequence=" + clientID
        End If

        clientName = runSQLwithArray(sql)
        returnVal = clientName(1)
        Return returnVal
    End Function

    Public Function touchscreenEnabled()
        Dim sql As String
        Dim returnVal As Boolean
        sql = "select misc1 from userdata"

        If runSQLwithID(sql) = "N" Then returnVal = False Else returnVal = True

        Return returnVal
    End Function

    Public Function getEPCName()
        Dim sql As String
        Dim EPCName As Array
        Dim returnVal As String

        sql = "select forename,surname from userdata"
        EPCName = runSQLwithArray(sql)

        returnVal = EPCName(0) + " " + EPCName(1)

        Return returnVal
    End Function

    Public Function spy(ByVal lineItem As String)
        Dim sql = ""
        If clientID <> "" Then
            sql = "insert into user_activity (activityTimestamp,activity,clientID) values ('" + SqlSafe(Now) + "','" + SqlSafe(lineItem) + "','" + clientID + "')"
        Else
            sql = "insert into user_activity (activityTimestamp,activity) values ('" + SqlSafe(Now) + "','" + SqlSafe(lineItem) + "')"
        End If

        runSQL(sql)
'        Return True

    End Function

    Public Function recordLastTouched()
        'this function records when the user last opened the client record screen
        'we need this as we are going to synchronise ALL fact find data to the server even if user doesn't want to
        'this means that the head office can see what's going on
        'misc2 field says "opened", once synchronisation has been undertaken, it'll be reset to blank

        Dim sql As String
        Dim result As Boolean
        If clientID <> "" Then

            sql = "update will_instruction_stage0_data set misc2='touched' where sequence=" + clientID
            runSQL(sql)
            result = True
        Else
            result = False
        End If

        Return result

    End Function

    Public Function runActivitySpyReport()

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage2_data"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader

        Dim spySQL As String = ""
        Dim willClientSequence As String
        Dim clientReferenceNumber As String

        Dim username As String = runSQLwithID("select username from userdata")  'this is used to help create the unique record ID on the server given there are multiple users without a clientReference Number
        Dim apikey As String = runSQLwithID("select apikey from userdata")

        While dbreader.Read
            clientID = dbreader.GetValue(0).ToString

            willClientSequence = dbreader.GetValue(0).ToString
            clientReferenceNumber = dbreader.GetValue(75).ToString
            If clientReferenceNumber = "" Then clientReferenceNumber = username + "-" + willClientSequence

            spySQL = "update spyData set "
            'has the wizard been run?
            sql = "select * from client_wizard where willClientSequence=" + willClientSequence
            Dim wizardData As Array = runSQLwithArray(sql)
            Dim productList As String = ""
            If wizardData(0) <> "ERROR" Then
                'we have wizard data
                productList = ""
                spySQL = spySQL + " wizardRunFlag='Y',"

                If wizardData(3) = "Y" Then productList = productList + " Will Review,"
                If wizardData(4) = "Y" Then productList = productList + " PPT,"
                If wizardData(5) = "Y" Then productList = productList + " Basic Will,"
                If wizardData(6) = "Y" Then productList = productList + " HW LPA,"
                If wizardData(7) = "Y" Then productList = productList + " PF LPA,"
                If wizardData(8) = "Y" Then productList = productList + " FAPT,"
                If wizardData(9) = "Y" Then productList = productList + " Probate,"
                If wizardData(10) = "Y" Then productList = productList + " " + SqlSafe(wizardData(12)) + " for " + wizardData(11) + ","
                'if there are products, then need to remove last character as it's a comma
                If productList <> "" Then
                    productList = productList.Trim().Substring(0, productList.Length - 1)
                    spySQL = spySQL + " productList='" + productList + "',"
                End If
            Else
                'wizard was not run so we need to report this fact
                spySQL = spySQL + " wizardRunFlag='N',"
            End If

            'MsgBox(spySQL)

            'get payment amount
            spySQL = spySQL + "paymentAmount='" + getWillLookup("paymentAmount") + "',"

            ' MsgBox(spySQL)

            'is there any willData?
            sql = "select sequence from will_instruction_stage3_data where  willClientSequence=" + willClientSequence
            If runSQLwithID(sql) <> "ERROR" Then spySQL = spySQL + " willData='Y',"

            'is there any lpaData?
            sql = "select sequence from lpa_attorneys where  willClientSequence=" + willClientSequence
            If runSQLwithID(sql) <> "ERROR" Then spySQL = spySQL + " lpaData='Y',"

            'is there any faptData?
            sql = "select sequence from fapt_trustees where  willClientSequence=" + willClientSequence
            If runSQLwithID(sql) <> "ERROR" Then spySQL = spySQL + " faptData='Y',"

            'is there appointment data?
            sql = "select appointmentStart from client_appointment where willClientSequence=" + willClientSequence
            If runSQLwithID(sql) <> "ERROR" Then spySQL = spySQL + " appointmentDateTime='" + mySQLDateFormat(runSQLwithID(sql)) + "',"

            'now we need to get the recording information/file

            'remove last comma character from string
            spySQL = spySQL.Trim().Substring(0, spySQL.Length - 1)

            'complete SQL update command
            spySQL = spySQL + " where clientReferenceNumber='" + clientReferenceNumber + "'"

            'MsgBox(spySQL)
            'now send to the website, but need to make sure that there is a row first on the server to allow for this update command
            spySQL = WebUtility.HtmlEncode(spySQL)
            spySQL = Replace(spySQL, "&", ";;DANAND;;")
            spySQL = Replace(spySQL, "£", ";;DANPOUND;;")
            spySQL = "crn=" + clientReferenceNumber + "&spyData=Y&apikey=" + apikey + "&publicKey=" + synchronise.Rot13(spySQL).Replace(" ", "^").Replace("=", "*%*")
            Dim responseFromServer As String = synchronise.PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", spySQL)
            ' MsgBox(responseFromServer)
        End While

        dbreader.Close()
        dbConn.Close()
        clientID = ""   'reset global variable
        Return True
    End Function
    Public Function mySQLDateFormat(ByVal accessDate As String)

        If accessDate <> "" Then
            Dim appYear As String
            Dim appMonth As String
            Dim appDay As String
            Dim appHour As String
            Dim appMinute As String
            Dim appSecond As String

            appYear = accessDate.Substring(6, 4)
            appMonth = accessDate.Substring(3, 2)
            appDay = accessDate.Substring(0, 2)

            appHour = accessDate.Substring(11, 2)
            appMinute = accessDate.Substring(14, 2)
            appSecond = "00"

            Dim mySQLdate = appYear + "-" + appMonth + "-" + appDay + " " + appHour + ":" + appMinute + ":00"
            Return mySQLdate
        Else
            Return accessDate
        End If


    End Function
    Public Function getAudioDir()

        'need to be mindful of multiple recordings for the same client
        'how many rows are in the electronic files wih the willClientSequence
        Dim filename As String

        If clientID <> "" Then
            Dim sql As String = "select count(sequence) from electronicFiles where willClientSequence=" + clientID.ToString
            Dim counter As String = runSQLwithID(sql)

            counter = (Val(counter) + 1).ToString
            filename = "\ff-" + clientID.ToString + "-" + counter + ".wav"
        Else

            filename = "\ff-newclient" + Date.Today.Year.ToString + Date.Today.Month.ToString + Date.Today.Day.ToString + "-1.wav"
        End If

        ' Combine directory with file name.
        Dim audioDir As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + filename

        Return audioDir

    End Function

    Public Function getNewCases()
        Dim username As String = runSQLwithID("select username from userdata")
        'MsgBox(username)
        Dim itemsAddedCounter As Integer = 0

        If internetFlag = "N" Then
            MsgBox("Cannot collect leads as you are currently offline" + vbCrLf + vbCrLf + "Please connect to the internet and then restart this software")
        Else
            Try
                Dim url As String = remoteSyncURL + "get_appointments.php?username=" + username

                System.Net.ServicePointManager.Expect100Continue = False
                Dim request As System.Net.WebRequest = System.Net.WebRequest.Create(url)
                request.Method = "POST" 'method
                Dim postData = "" 'Data
                Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
                request.ContentType = "application/x-www-form-urlencoded"
                request.ContentLength = byteArray.Length
                Dim dataStream As Stream = request.GetRequestStream()
                dataStream.Write(byteArray, 0, byteArray.Length)
                dataStream.Close()
                Dim response As WebResponse = request.GetResponse()
                dataStream = response.GetResponseStream()
                Dim reader As New StreamReader(dataStream)
                Dim responseFromServer As String = reader.ReadToEnd()
                reader.Close()
                dataStream.Close()
                response.Close()

                'MsgBox(responseFromServer)

                Dim sql As String = ""
                Dim clients As String() = responseFromServer.Split(New Char() {"~"c})

                Dim client As String
                For Each client In clients
                    If client <> "" Then
                        Dim allfields As String() = client.Split(New Char() {"|"c})
                        'MsgBox(client)
                        'we have data fields that we need to split out

                        'does this client ID exist anywhere...if it does, then ignore it as we've already got the client data, if it doesn't then we need to insert the information
                        sql = "select willClientSequence from will_instruction_stage2_data where clientReferenceNumber='" + SqlSafe(allfields(0)) + "'"
                        ' MsgBox(runSQLwithID(sql))
                        If runSQLwithID(sql) = "ERROR" Or runSQLwithID(sql) = "" Then
                            'this is a new record so we have to add it
                            itemsAddedCounter = itemsAddedCounter + 1

                            'first thing is to insert a record and get the willClientSequence back 
                            'insert record to get willClientSequence (as we're using WWP database structure), but we call it clientID within this software
                            sql = "insert into will_instruction_stage0_data (status,statusDate) values ('offline','11/10/2019')"
                            runSQL(sql)

                            'get clientID i.e. row sequence
                            sql = "select sequence from will_instruction_stage0_data order by sequence DESC"
                            clientID = runSQLwithID(sql)

                            'is it a mirror or single client/will
                            sql = "insert into will_instruction_stage1_data (willClientSequence,typeOfWill) values (" + clientID + ",'" + SqlSafe(allfields(1)) + "') "
                            'MsgBox(sql)
                            runSQL(sql)

                            'insert row - but is it mirror or single?
                            If allfields(1).ToLower = "single" Then
                                sql = "insert into will_instruction_stage2_data (willClientSequence,clientReferenceNumber,person1Title,person1Forenames,person1Surname,person1DOB,person1Email,person1MaritalStatus,person1Address,person1Postcode,person1HomeNumber,person1MobileNumber) values (" + clientID + ",'" + SqlSafe(allfields(0)) + "','" + SqlSafe(allfields(2)) + "','" + SqlSafe(allfields(3)) + "','" + SqlSafe(allfields(4)) + "','" + SqlSafe(allfields(5)) + "','" + SqlSafe(allfields(6)) + "','" + SqlSafe(allfields(7)) + "','" + SqlSafe(allfields(8)) + "','" + SqlSafe(allfields(9)) + "','" + SqlSafe(allfields(18)) + "','" + SqlSafe(allfields(19)) + "')"
                            Else
                                sql = "insert into will_instruction_stage2_data (willClientSequence,clientReferenceNumber,person1Title,person1Forenames,person1Surname,person1DOB,person1Email,person1MaritalStatus,person1Address,person1Postcode,person2Title,person2Forenames,person2Surname,person2DOB,person2Email,person2MaritalStatus,person2Address,person2Postcode,person1HomeNumber,person1MobileNumber,person2HomeNumber,person2MobileNumber) values (" + clientID + ",'" + SqlSafe(allfields(0)) + "','" + SqlSafe(allfields(2)) + "','" + SqlSafe(allfields(3)) + "','" + SqlSafe(allfields(4)) + "','" + SqlSafe(allfields(5)) + "','" + SqlSafe(allfields(6)) + "','" + SqlSafe(allfields(7)) + "','" + SqlSafe(allfields(8)) + "','" + SqlSafe(allfields(9)) + "','" + SqlSafe(allfields(10)) + "','" + SqlSafe(allfields(11)) + "','" + SqlSafe(allfields(12)) + "','" + SqlSafe(allfields(13)) + "','" + SqlSafe(allfields(14)) + "','" + SqlSafe(allfields(15)) + "','" + SqlSafe(allfields(16)) + "','" + SqlSafe(allfields(17)) + "','" + SqlSafe(allfields(18)) + "','" + Left(SqlSafe(allfields(19)), 10) + "','" + SqlSafe(allfields(20)) + "','" + SqlSafe(allfields(21)) + "')"
                            End If
                            'MsgBox(sql)
                            runSQL(sql)

                            'now we need to insert the storage information
                            sql = "insert into client_storage (willClientSequence,sourceClientID,storageFlag,storageType,storageAmount,storagePaymentMethod,storageSortCode,storageAccountNumber) values(" + clientID + ",'" + SqlSafe(allfields(0)) + "','" + SqlSafe(allfields(27)) + "','" + SqlSafe(allfields(28)) + "','" + SqlSafe(allfields(29)) + "','" + SqlSafe(allfields(30)) + "','" + SqlSafe(allfields(31)) + "','" + SqlSafe(allfields(32)) + "')"
                            'MsgBox("STORAGE = " + sql)
                            runSQL(sql)

                            'now we need to store the appointment information, especially marketing campaign code
                            sql = "insert into client_appointment (willClientSequence,sourceClientID,appointmentID,appointmentStart,appointmentEnd,appointmentTitle,campaignID) values(" + clientID + ",'" + SqlSafe(allfields(0)) + "','" + SqlSafe(allfields(22)) + "','" + SqlSafe(allfields(23)) + "','" + SqlSafe(allfields(24)) + "','" + SqlSafe(allfields(25)) + "','" + SqlSafe(allfields(26)) + "')"
                            ' MsgBox("APPOINTMENT = " + sql)
                            runSQL(sql)

                        End If
                    End If

                Next
                ' MsgBox("There have been " + itemsAddedCounter.ToString + " clients added")
            Catch ex As Exception
                Dim error1 As String = ErrorToString()
                If error1 = "Invalid URI: The format of the URI could not be determined." Then
                    MsgBox("ERROR! Must have HTTP:// before the URL.")
                Else
                    MsgBox(error1)
                End If
                MsgBox("ERROR")
            End Try
        End If

        Return itemsAddedCounter
    End Function
End Module
