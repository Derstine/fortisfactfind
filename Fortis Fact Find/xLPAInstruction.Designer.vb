﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xLPAInstruction
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xLPAInstruction))
        Dim CustomHeaderButtonImageOptions1 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions2 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions3 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.NavigationPane1 = New DevExpress.XtraBars.Navigation.NavigationPane()
        Me.NavigationPageDonorInformation = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.NavigationPageAttorneys = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.MemoEdit3 = New DevExpress.XtraEditors.MemoEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckEdit14 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit13 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit12 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit8 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit11 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.NavigationPageReplacementAttorneys = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckEdit16 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit15 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.NavigationPageCertificateProvider = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit4 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.CheckEdit24 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem1 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit18 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit23 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit22 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit20 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit21 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit19 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit17 = New DevExpress.XtraEditors.CheckEdit()
        Me.NavigationPageInstructions = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.MemoEdit5 = New DevExpress.XtraEditors.MemoEdit()
        Me.NavigationPagePreferences = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.MemoEdit6 = New DevExpress.XtraEditors.MemoEdit()
        Me.NavigationPagePersonsToBeTold = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.NavigationPageRegistration = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit25 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit26 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit27 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit28 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit29 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit30 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit31 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit32 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit34 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit7 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit4 = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem11 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem12 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem13 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem14 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem15 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.NavigationPageAttendanceNotes = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.MemoEdit8 = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPane1.SuspendLayout
        Me.NavigationPageDonorInformation.SuspendLayout
        CType(Me.GroupControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl2.SuspendLayout
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl2.SuspendLayout
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GroupControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl1.SuspendLayout
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl1.SuspendLayout
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageAttorneys.SuspendLayout
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit14.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit13.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit10.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageReplacementAttorneys.SuspendLayout
        CType(Me.GridControl2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit16.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit15.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageCertificateProvider.SuspendLayout
        CType(Me.LayoutControl3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl3.SuspendLayout
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit3.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit24.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit18.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit23.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit22.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit20.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit21.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit19.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit17.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageInstructions.SuspendLayout
        CType(Me.MemoEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPagePreferences.SuspendLayout
        CType(Me.MemoEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPagePersonsToBeTold.SuspendLayout
        CType(Me.GridControl3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageRegistration.SuspendLayout
        CType(Me.LayoutControl4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl4.SuspendLayout
        CType(Me.CheckEdit25.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit26.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit27.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit28.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit29.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit30.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit31.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit32.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit34.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit10.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit4.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem31,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem32,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem30,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem33,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem34,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem35,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem36,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem15,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageAttendanceNotes.SuspendLayout
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelControl1.SuspendLayout
        CType(Me.MemoEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'NavigationPane1
        '
        Me.NavigationPane1.Controls.Add(Me.NavigationPageDonorInformation)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageAttorneys)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageReplacementAttorneys)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageCertificateProvider)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageInstructions)
        Me.NavigationPane1.Controls.Add(Me.NavigationPagePreferences)
        Me.NavigationPane1.Controls.Add(Me.NavigationPagePersonsToBeTold)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageRegistration)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageAttendanceNotes)
        Me.NavigationPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPane1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText
        Me.NavigationPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPageDonorInformation, Me.NavigationPageAttorneys, Me.NavigationPageReplacementAttorneys, Me.NavigationPageCertificateProvider, Me.NavigationPageInstructions, Me.NavigationPagePreferences, Me.NavigationPagePersonsToBeTold, Me.NavigationPageRegistration, Me.NavigationPageAttendanceNotes})
        Me.NavigationPane1.RegularSize = New System.Drawing.Size(1240, 662)
        Me.NavigationPane1.SelectedPage = Me.NavigationPageAttorneys
        Me.NavigationPane1.Size = New System.Drawing.Size(1240, 662)
        Me.NavigationPane1.TabIndex = 0
        Me.NavigationPane1.Text = "NavigationPane1"
        '
        'NavigationPageDonorInformation
        '
        Me.NavigationPageDonorInformation.Caption = "Donor Information"
        Me.NavigationPageDonorInformation.Controls.Add(Me.GroupControl2)
        Me.NavigationPageDonorInformation.Controls.Add(Me.GroupControl1)
        Me.NavigationPageDonorInformation.ImageOptions.Image = CType(resources.GetObject("NavigationPageDonorInformation.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageDonorInformation.Name = "NavigationPageDonorInformation"
        Me.NavigationPageDonorInformation.Size = New System.Drawing.Size(1009, 602)
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.LayoutControl2)
        Me.GroupControl2.Location = New System.Drawing.Point(579, 21)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(415, 565)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Donor 2"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.TextEdit2)
        Me.LayoutControl2.Controls.Add(Me.DateEdit2)
        Me.LayoutControl2.Controls.Add(Me.MemoEdit2)
        Me.LayoutControl2.Controls.Add(Me.TextEdit4)
        Me.LayoutControl2.Controls.Add(Me.CheckEdit3)
        Me.LayoutControl2.Controls.Add(Me.CheckEdit4)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(411, 542)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(117, 12)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(282, 38)
        Me.TextEdit2.StyleController = Me.LayoutControl2
        Me.TextEdit2.TabIndex = 4
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(117, 54)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(282, 38)
        Me.DateEdit2.StyleController = Me.LayoutControl2
        Me.DateEdit2.TabIndex = 5
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(117, 96)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Size = New System.Drawing.Size(282, 215)
        Me.MemoEdit2.StyleController = Me.LayoutControl2
        Me.MemoEdit2.TabIndex = 6
        '
        'TextEdit4
        '
        Me.TextEdit4.Location = New System.Drawing.Point(119, 315)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Size = New System.Drawing.Size(280, 38)
        Me.TextEdit4.StyleController = Me.LayoutControl2
        Me.TextEdit4.TabIndex = 7
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(117, 357)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Caption = "Property && Finance"
        Me.CheckEdit3.Size = New System.Drawing.Size(282, 44)
        Me.CheckEdit3.StyleController = Me.LayoutControl2
        Me.CheckEdit3.TabIndex = 8
        '
        'CheckEdit4
        '
        Me.CheckEdit4.Location = New System.Drawing.Point(119, 405)
        Me.CheckEdit4.Name = "CheckEdit4"
        Me.CheckEdit4.Properties.Caption = "Health Welfare"
        Me.CheckEdit4.Size = New System.Drawing.Size(280, 44)
        Me.CheckEdit4.StyleController = Me.LayoutControl2
        Me.CheckEdit4.TabIndex = 9
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = false
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.EmptySpaceItem4, Me.LayoutControlItem10, Me.EmptySpaceItem5, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.EmptySpaceItem6})
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(411, 542)
        Me.LayoutControlGroup1.TextVisible = false
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.TextEdit2
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(391, 42)
        Me.LayoutControlItem7.Text = "Name"
        Me.LayoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem7.TextToControlDistance = 5
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.DateEdit2
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(391, 42)
        Me.LayoutControlItem8.Text = "Date of Birth"
        Me.LayoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem8.TextToControlDistance = 5
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem9.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem9.Control = Me.MemoEdit2
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(391, 219)
        Me.LayoutControlItem9.Text = "Address"
        Me.LayoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem9.TextToControlDistance = 5
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = false
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 441)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(391, 81)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.TextEdit4
        Me.LayoutControlItem10.Location = New System.Drawing.Point(107, 303)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(284, 42)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = false
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = false
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 303)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(107, 42)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.CheckEdit3
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 345)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(391, 48)
        Me.LayoutControlItem11.Text = "LPAs Required"
        Me.LayoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem11.TextToControlDistance = 5
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.CheckEdit4
        Me.LayoutControlItem12.Location = New System.Drawing.Point(107, 393)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(284, 48)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = false
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = false
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 393)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(107, 48)
        Me.EmptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LayoutControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(26, 21)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(450, 565)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Donor 1"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.TextEdit1)
        Me.LayoutControl1.Controls.Add(Me.DateEdit1)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit1)
        Me.LayoutControl1.Controls.Add(Me.TextEdit3)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit1)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit2)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(446, 542)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(117, 12)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(317, 38)
        Me.TextEdit1.StyleController = Me.LayoutControl1
        Me.TextEdit1.TabIndex = 4
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(117, 54)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(317, 38)
        Me.DateEdit1.StyleController = Me.LayoutControl1
        Me.DateEdit1.TabIndex = 5
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(117, 96)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(317, 194)
        Me.MemoEdit1.StyleController = Me.LayoutControl1
        Me.MemoEdit1.TabIndex = 6
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(119, 294)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Size = New System.Drawing.Size(315, 38)
        Me.TextEdit3.StyleController = Me.LayoutControl1
        Me.TextEdit3.TabIndex = 8
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(117, 336)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "Property && Finance"
        Me.CheckEdit1.Size = New System.Drawing.Size(317, 44)
        Me.CheckEdit1.StyleController = Me.LayoutControl1
        Me.CheckEdit1.TabIndex = 9
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(119, 384)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Caption = "Health && Welfare"
        Me.CheckEdit2.Size = New System.Drawing.Size(315, 44)
        Me.CheckEdit2.StyleController = Me.LayoutControl1
        Me.CheckEdit2.TabIndex = 10
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = false
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem5, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.LayoutControlItem4, Me.LayoutControlItem6, Me.EmptySpaceItem3})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(446, 542)
        Me.Root.TextVisible = false
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.TextEdit1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(426, 42)
        Me.LayoutControlItem1.Text = "Name"
        Me.LayoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem1.TextToControlDistance = 5
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.DateEdit1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(426, 42)
        Me.LayoutControlItem2.Text = "Date of Birth"
        Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem2.TextToControlDistance = 5
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem3.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem3.Control = Me.MemoEdit1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(426, 198)
        Me.LayoutControlItem3.Text = "Address"
        Me.LayoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem3.TextToControlDistance = 5
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.TextEdit3
        Me.LayoutControlItem5.Location = New System.Drawing.Point(107, 282)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(319, 42)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = false
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = false
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 420)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(426, 102)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = false
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 282)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(107, 42)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.CheckEdit1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 324)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(426, 48)
        Me.LayoutControlItem4.Text = "LPAs Required"
        Me.LayoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem4.TextToControlDistance = 5
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.CheckEdit2
        Me.LayoutControlItem6.Location = New System.Drawing.Point(107, 372)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(319, 48)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = false
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = false
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 372)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(107, 48)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'NavigationPageAttorneys
        '
        Me.NavigationPageAttorneys.Caption = "Attorneys"
        Me.NavigationPageAttorneys.Controls.Add(Me.MemoEdit3)
        Me.NavigationPageAttorneys.Controls.Add(Me.GridControl1)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit14)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit13)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit12)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit10)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit8)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit6)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit11)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit9)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit7)
        Me.NavigationPageAttorneys.Controls.Add(Me.CheckEdit5)
        Me.NavigationPageAttorneys.Controls.Add(Me.LabelControl5)
        Me.NavigationPageAttorneys.Controls.Add(Me.LabelControl4)
        Me.NavigationPageAttorneys.Controls.Add(Me.LabelControl3)
        Me.NavigationPageAttorneys.Controls.Add(Me.LabelControl2)
        Me.NavigationPageAttorneys.Controls.Add(Me.LabelControl1)
        CustomHeaderButtonImageOptions1.SvgImage = CType(resources.GetObject("CustomHeaderButtonImageOptions1.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.NavigationPageAttorneys.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Attorney", true, CustomHeaderButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject1, Nothing, -1)})
        Me.NavigationPageAttorneys.ImageOptions.Image = CType(resources.GetObject("NavigationPageAttorneys.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageAttorneys.Name = "NavigationPageAttorneys"
        Me.NavigationPageAttorneys.Size = New System.Drawing.Size(1009, 589)
        '
        'MemoEdit3
        '
        Me.MemoEdit3.Location = New System.Drawing.Point(246, 524)
        Me.MemoEdit3.Name = "MemoEdit3"
        Me.MemoEdit3.Size = New System.Drawing.Size(738, 45)
        Me.MemoEdit3.TabIndex = 3
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(29, 175)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(955, 218)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = false
        Me.GridView1.OptionsView.ShowIndicator = false
        Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Attorney Name"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = true
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 278
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Relationship to Client 1"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = true
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 278
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Relationship to Client 2"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = true
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 278
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Action"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = true
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 100
        '
        'CheckEdit14
        '
        Me.CheckEdit14.Location = New System.Drawing.Point(487, 466)
        Me.CheckEdit14.Name = "CheckEdit14"
        Me.CheckEdit14.Properties.Caption = "JOINTLY FOR SOME, JOINTLY &&  SEVERALLY FOR OTHERS"
        Me.CheckEdit14.Size = New System.Drawing.Size(332, 44)
        Me.CheckEdit14.TabIndex = 1
        '
        'CheckEdit13
        '
        Me.CheckEdit13.Location = New System.Drawing.Point(246, 466)
        Me.CheckEdit13.Name = "CheckEdit13"
        Me.CheckEdit13.Properties.Caption = "JOINTLY && SEVERALLY"
        Me.CheckEdit13.Size = New System.Drawing.Size(195, 44)
        Me.CheckEdit13.TabIndex = 1
        '
        'CheckEdit12
        '
        Me.CheckEdit12.Location = New System.Drawing.Point(487, 416)
        Me.CheckEdit12.Name = "CheckEdit12"
        Me.CheckEdit12.Properties.Caption = "JOINTLY"
        Me.CheckEdit12.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit12.TabIndex = 1
        '
        'CheckEdit10
        '
        Me.CheckEdit10.Location = New System.Drawing.Point(453, 115)
        Me.CheckEdit10.Name = "CheckEdit10"
        Me.CheckEdit10.Properties.Caption = "NO"
        Me.CheckEdit10.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit10.TabIndex = 1
        '
        'CheckEdit8
        '
        Me.CheckEdit8.Location = New System.Drawing.Point(453, 65)
        Me.CheckEdit8.Name = "CheckEdit8"
        Me.CheckEdit8.Properties.Caption = "NO"
        Me.CheckEdit8.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit8.TabIndex = 1
        '
        'CheckEdit6
        '
        Me.CheckEdit6.Location = New System.Drawing.Point(453, 15)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Caption = "NO"
        Me.CheckEdit6.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit6.TabIndex = 1
        '
        'CheckEdit11
        '
        Me.CheckEdit11.Location = New System.Drawing.Point(246, 416)
        Me.CheckEdit11.Name = "CheckEdit11"
        Me.CheckEdit11.Properties.Caption = "SOLELY"
        Me.CheckEdit11.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit11.TabIndex = 1
        '
        'CheckEdit9
        '
        Me.CheckEdit9.Location = New System.Drawing.Point(332, 115)
        Me.CheckEdit9.Name = "CheckEdit9"
        Me.CheckEdit9.Properties.Caption = "YES"
        Me.CheckEdit9.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit9.TabIndex = 1
        '
        'CheckEdit7
        '
        Me.CheckEdit7.Location = New System.Drawing.Point(332, 65)
        Me.CheckEdit7.Name = "CheckEdit7"
        Me.CheckEdit7.Properties.Caption = "YES"
        Me.CheckEdit7.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit7.TabIndex = 1
        '
        'CheckEdit5
        '
        Me.CheckEdit5.Location = New System.Drawing.Point(332, 15)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Caption = "YES"
        Me.CheckEdit5.Size = New System.Drawing.Size(97, 44)
        Me.CheckEdit5.TabIndex = 1
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(29, 525)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(118, 16)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Details of how to act"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(28, 430)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(193, 16)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Is anyone else to be an attorney?"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(29, 129)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(193, 16)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Is anyone else to be an attorney?"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(29, 79)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(184, 16)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Are all children to be attorneys?"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(29, 29)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(198, 16)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Is the spouse/partner an attorney?"
        '
        'NavigationPageReplacementAttorneys
        '
        Me.NavigationPageReplacementAttorneys.Caption = "Replacement Attoryneys"
        Me.NavigationPageReplacementAttorneys.Controls.Add(Me.GridControl2)
        Me.NavigationPageReplacementAttorneys.Controls.Add(Me.CheckEdit16)
        Me.NavigationPageReplacementAttorneys.Controls.Add(Me.CheckEdit15)
        Me.NavigationPageReplacementAttorneys.Controls.Add(Me.LabelControl6)
        CustomHeaderButtonImageOptions2.SvgImage = CType(resources.GetObject("CustomHeaderButtonImageOptions2.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.NavigationPageReplacementAttorneys.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Replacement Attorney", true, CustomHeaderButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject2, Nothing, -1)})
        Me.NavigationPageReplacementAttorneys.ImageOptions.Image = CType(resources.GetObject("NavigationPageReplacementAttorneys.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageReplacementAttorneys.Name = "NavigationPageReplacementAttorneys"
        Me.NavigationPageReplacementAttorneys.Size = New System.Drawing.Size(1009, 589)
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(21, 58)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(967, 514)
        Me.GridControl2.TabIndex = 2
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Replacement Attorney Name"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = true
        Me.GridColumn5.VisibleIndex = 0
        Me.GridColumn5.Width = 279
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Relationship to Client 1"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = true
        Me.GridColumn6.VisibleIndex = 1
        Me.GridColumn6.Width = 279
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Relationship To Client 2"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = true
        Me.GridColumn7.VisibleIndex = 2
        Me.GridColumn7.Width = 279
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Action"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.OptionsColumn.FixedWidth = true
        Me.GridColumn8.Visible = true
        Me.GridColumn8.VisibleIndex = 3
        Me.GridColumn8.Width = 100
        '
        'CheckEdit16
        '
        Me.CheckEdit16.Location = New System.Drawing.Point(395, 10)
        Me.CheckEdit16.Name = "CheckEdit16"
        Me.CheckEdit16.Properties.Caption = "NO"
        Me.CheckEdit16.Size = New System.Drawing.Size(90, 44)
        Me.CheckEdit16.TabIndex = 1
        '
        'CheckEdit15
        '
        Me.CheckEdit15.Location = New System.Drawing.Point(299, 10)
        Me.CheckEdit15.Name = "CheckEdit15"
        Me.CheckEdit15.Properties.Caption = "YES"
        Me.CheckEdit15.Size = New System.Drawing.Size(90, 44)
        Me.CheckEdit15.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(21, 24)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(224, 16)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Do you require replacement attorneys?"
        '
        'NavigationPageCertificateProvider
        '
        Me.NavigationPageCertificateProvider.Caption = "Certificate Provider"
        Me.NavigationPageCertificateProvider.Controls.Add(Me.LayoutControl3)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.LabelControl7)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.CheckEdit18)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.CheckEdit23)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.CheckEdit22)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.CheckEdit20)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.CheckEdit21)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.CheckEdit19)
        Me.NavigationPageCertificateProvider.Controls.Add(Me.CheckEdit17)
        Me.NavigationPageCertificateProvider.ImageOptions.Image = CType(resources.GetObject("NavigationPageCertificateProvider.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageCertificateProvider.Name = "NavigationPageCertificateProvider"
        Me.NavigationPageCertificateProvider.Size = New System.Drawing.Size(1009, 602)
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.TextEdit5)
        Me.LayoutControl3.Controls.Add(Me.TextEdit6)
        Me.LayoutControl3.Controls.Add(Me.TextEdit7)
        Me.LayoutControl3.Controls.Add(Me.SimpleButtonLookup)
        Me.LayoutControl3.Controls.Add(Me.MemoEdit4)
        Me.LayoutControl3.Controls.Add(Me.TextEdit8)
        Me.LayoutControl3.Controls.Add(Me.TextEdit9)
        Me.LayoutControl3.Controls.Add(Me.DateEdit3)
        Me.LayoutControl3.Controls.Add(Me.CheckEdit24)
        Me.LayoutControl3.Location = New System.Drawing.Point(85, 117)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup2
        Me.LayoutControl3.Size = New System.Drawing.Size(824, 431)
        Me.LayoutControl3.TabIndex = 3
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'TextEdit5
        '
        Me.TextEdit5.Location = New System.Drawing.Point(217, 12)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Size = New System.Drawing.Size(595, 38)
        Me.TextEdit5.StyleController = Me.LayoutControl3
        Me.TextEdit5.TabIndex = 4
        '
        'TextEdit6
        '
        Me.TextEdit6.Location = New System.Drawing.Point(217, 54)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Size = New System.Drawing.Size(595, 38)
        Me.TextEdit6.StyleController = Me.LayoutControl3
        Me.TextEdit6.TabIndex = 5
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(217, 96)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.AutoHeight = false
        Me.TextEdit7.Size = New System.Drawing.Size(469, 42)
        Me.TextEdit7.StyleController = Me.LayoutControl3
        Me.TextEdit7.TabIndex = 6
        '
        'SimpleButtonLookup
        '
        Me.SimpleButtonLookup.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup.Location = New System.Drawing.Point(690, 96)
        Me.SimpleButtonLookup.Name = "SimpleButtonLookup"
        Me.SimpleButtonLookup.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButtonLookup.StyleController = Me.LayoutControl3
        Me.SimpleButtonLookup.TabIndex = 7
        Me.SimpleButtonLookup.Text = "Lookup"
        '
        'MemoEdit4
        '
        Me.MemoEdit4.Location = New System.Drawing.Point(217, 142)
        Me.MemoEdit4.Name = "MemoEdit4"
        Me.MemoEdit4.Size = New System.Drawing.Size(595, 125)
        Me.MemoEdit4.StyleController = Me.LayoutControl3
        Me.MemoEdit4.TabIndex = 8
        '
        'TextEdit8
        '
        Me.TextEdit8.Location = New System.Drawing.Point(217, 271)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Size = New System.Drawing.Size(231, 38)
        Me.TextEdit8.StyleController = Me.LayoutControl3
        Me.TextEdit8.TabIndex = 9
        '
        'TextEdit9
        '
        Me.TextEdit9.Location = New System.Drawing.Point(217, 313)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Size = New System.Drawing.Size(231, 38)
        Me.TextEdit9.StyleController = Me.LayoutControl3
        Me.TextEdit9.TabIndex = 10
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = Nothing
        Me.DateEdit3.Location = New System.Drawing.Point(462, 290)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.AutoHeight = false
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Size = New System.Drawing.Size(350, 43)
        Me.DateEdit3.StyleController = Me.LayoutControl3
        Me.DateEdit3.TabIndex = 11
        '
        'CheckEdit24
        '
        Me.CheckEdit24.Location = New System.Drawing.Point(220, 355)
        Me.CheckEdit24.Name = "CheckEdit24"
        Me.CheckEdit24.Properties.Caption = "I confirm that the chosen person is allowed to be a certificate provide"
        Me.CheckEdit24.Size = New System.Drawing.Size(592, 44)
        Me.CheckEdit24.StyleController = Me.LayoutControl3
        Me.CheckEdit24.TabIndex = 12
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = false
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem17, Me.LayoutControlItem16, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.EmptySpaceItem7, Me.EmptySpaceItem9, Me.EmptySpaceItem10, Me.LayoutControlItem21, Me.SimpleLabelItem1})
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(824, 431)
        Me.LayoutControlGroup2.TextVisible = false
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.TextEdit5
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(804, 42)
        Me.LayoutControlItem13.Text = "First Name(s)"
        Me.LayoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem13.TextToControlDistance = 5
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.TextEdit6
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(804, 42)
        Me.LayoutControlItem14.Text = "Surname"
        Me.LayoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem14.TextToControlDistance = 5
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.TextEdit7
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(678, 46)
        Me.LayoutControlItem15.Text = "Postcode"
        Me.LayoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem15.TextToControlDistance = 5
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem17.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem17.Control = Me.MemoEdit4
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 130)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(804, 129)
        Me.LayoutControlItem17.Text = "Address"
        Me.LayoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(200, 16)
        Me.LayoutControlItem17.TextToControlDistance = 5
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.SimpleButtonLookup
        Me.LayoutControlItem16.Location = New System.Drawing.Point(678, 84)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(126, 46)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = false
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.TextEdit8
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 259)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(440, 42)
        Me.LayoutControlItem18.Text = "Relationship To Client 1"
        Me.LayoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem18.TextToControlDistance = 5
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.TextEdit9
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 301)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(440, 42)
        Me.LayoutControlItem19.Text = "Relationship to Client 2"
        Me.LayoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(200, 20)
        Me.LayoutControlItem19.TextToControlDistance = 5
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.DateEdit3
        Me.LayoutControlItem20.Location = New System.Drawing.Point(450, 259)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(354, 66)
        Me.LayoutControlItem20.Text = "Date of Birth"
        Me.LayoutControlItem20.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(350, 16)
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = false
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(440, 259)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(10, 84)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem9
        '
        Me.EmptySpaceItem9.AllowHotTrack = false
        Me.EmptySpaceItem9.Location = New System.Drawing.Point(450, 325)
        Me.EmptySpaceItem9.Name = "EmptySpaceItem9"
        Me.EmptySpaceItem9.Size = New System.Drawing.Size(354, 18)
        Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem10
        '
        Me.EmptySpaceItem10.AllowHotTrack = false
        Me.EmptySpaceItem10.Location = New System.Drawing.Point(0, 343)
        Me.EmptySpaceItem10.Name = "EmptySpaceItem10"
        Me.EmptySpaceItem10.Size = New System.Drawing.Size(208, 68)
        Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.CheckEdit24
        Me.LayoutControlItem21.Location = New System.Drawing.Point(208, 343)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(596, 48)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = false
        '
        'SimpleLabelItem1
        '
        Me.SimpleLabelItem1.AllowHotTrack = false
        Me.SimpleLabelItem1.Location = New System.Drawing.Point(208, 391)
        Me.SimpleLabelItem1.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem1.Size = New System.Drawing.Size(596, 20)
        Me.SimpleLabelItem1.Text = "(If in doubt, please check the list of those that are prohibited)"
        Me.SimpleLabelItem1.TextSize = New System.Drawing.Size(350, 16)
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(85, 81)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(24, 16)
        Me.LabelControl7.TabIndex = 2
        Me.LabelControl7.Text = "Title"
        '
        'CheckEdit18
        '
        Me.CheckEdit18.Location = New System.Drawing.Point(407, 17)
        Me.CheckEdit18.Name = "CheckEdit18"
        Me.CheckEdit18.Properties.Caption = "OTHER (MUST HAVE KNOWN THEM FOR ATLEAST 2 YEARS)"
        Me.CheckEdit18.Size = New System.Drawing.Size(462, 44)
        Me.CheckEdit18.TabIndex = 0
        '
        'CheckEdit23
        '
        Me.CheckEdit23.Location = New System.Drawing.Point(642, 67)
        Me.CheckEdit23.Name = "CheckEdit23"
        Me.CheckEdit23.Properties.Caption = "DOCTOR"
        Me.CheckEdit23.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit23.TabIndex = 0
        '
        'CheckEdit22
        '
        Me.CheckEdit22.Location = New System.Drawing.Point(533, 67)
        Me.CheckEdit22.Name = "CheckEdit22"
        Me.CheckEdit22.Properties.Caption = "MS"
        Me.CheckEdit22.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit22.TabIndex = 0
        '
        'CheckEdit20
        '
        Me.CheckEdit20.Location = New System.Drawing.Point(298, 67)
        Me.CheckEdit20.Name = "CheckEdit20"
        Me.CheckEdit20.Properties.Caption = "MRS"
        Me.CheckEdit20.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit20.TabIndex = 0
        '
        'CheckEdit21
        '
        Me.CheckEdit21.Location = New System.Drawing.Point(407, 67)
        Me.CheckEdit21.Name = "CheckEdit21"
        Me.CheckEdit21.Properties.Caption = "MISS"
        Me.CheckEdit21.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit21.TabIndex = 0
        '
        'CheckEdit19
        '
        Me.CheckEdit19.Location = New System.Drawing.Point(172, 67)
        Me.CheckEdit19.Name = "CheckEdit19"
        Me.CheckEdit19.Properties.Caption = "MR"
        Me.CheckEdit19.Size = New System.Drawing.Size(103, 44)
        Me.CheckEdit19.TabIndex = 0
        '
        'CheckEdit17
        '
        Me.CheckEdit17.Location = New System.Drawing.Point(172, 17)
        Me.CheckEdit17.Name = "CheckEdit17"
        Me.CheckEdit17.Properties.Caption = "PROFESSIONAL"
        Me.CheckEdit17.Size = New System.Drawing.Size(205, 44)
        Me.CheckEdit17.TabIndex = 0
        '
        'NavigationPageInstructions
        '
        Me.NavigationPageInstructions.Caption = "Instructions"
        Me.NavigationPageInstructions.Controls.Add(Me.MemoEdit5)
        Me.NavigationPageInstructions.ImageOptions.Image = CType(resources.GetObject("NavigationPageInstructions.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageInstructions.Name = "NavigationPageInstructions"
        Me.NavigationPageInstructions.Size = New System.Drawing.Size(1009, 602)
        '
        'MemoEdit5
        '
        Me.MemoEdit5.Location = New System.Drawing.Point(34, 40)
        Me.MemoEdit5.Name = "MemoEdit5"
        Me.MemoEdit5.Properties.NullText = "Add your intructions here"
        Me.MemoEdit5.Size = New System.Drawing.Size(959, 520)
        Me.MemoEdit5.TabIndex = 0
        '
        'NavigationPagePreferences
        '
        Me.NavigationPagePreferences.Caption = "Preferences"
        Me.NavigationPagePreferences.Controls.Add(Me.MemoEdit6)
        Me.NavigationPagePreferences.ImageOptions.Image = CType(resources.GetObject("NavigationPagePreferences.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPagePreferences.Name = "NavigationPagePreferences"
        Me.NavigationPagePreferences.Size = New System.Drawing.Size(1014, 602)
        '
        'MemoEdit6
        '
        Me.MemoEdit6.EditValue = "Add your desired preferences"
        Me.MemoEdit6.Location = New System.Drawing.Point(34, 41)
        Me.MemoEdit6.Name = "MemoEdit6"
        Me.MemoEdit6.Properties.NullText = "Add your intructions here"
        Me.MemoEdit6.Size = New System.Drawing.Size(943, 520)
        Me.MemoEdit6.TabIndex = 1
        '
        'NavigationPagePersonsToBeTold
        '
        Me.NavigationPagePersonsToBeTold.Caption = "Persons To Be Told"
        Me.NavigationPagePersonsToBeTold.Controls.Add(Me.GridControl3)
        CustomHeaderButtonImageOptions3.SvgImage = CType(resources.GetObject("CustomHeaderButtonImageOptions3.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.NavigationPagePersonsToBeTold.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Person to be Told", true, CustomHeaderButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject3, Nothing, -1)})
        Me.NavigationPagePersonsToBeTold.ImageOptions.Image = CType(resources.GetObject("NavigationPagePersonsToBeTold.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPagePersonsToBeTold.Name = "NavigationPagePersonsToBeTold"
        Me.NavigationPagePersonsToBeTold.Size = New System.Drawing.Size(1014, 589)
        '
        'GridControl3
        '
        Me.GridControl3.Location = New System.Drawing.Point(19, 27)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(972, 548)
        Me.GridControl3.TabIndex = 0
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12})
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Name"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = true
        Me.GridColumn9.VisibleIndex = 0
        Me.GridColumn9.Width = 280
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Relationship to Client 1"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = true
        Me.GridColumn10.VisibleIndex = 1
        Me.GridColumn10.Width = 280
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Relationship to Client 2"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = true
        Me.GridColumn11.VisibleIndex = 2
        Me.GridColumn11.Width = 280
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Action"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = true
        Me.GridColumn12.VisibleIndex = 3
        Me.GridColumn12.Width = 100
        '
        'NavigationPageRegistration
        '
        Me.NavigationPageRegistration.Caption = "NavigationPageRegistration"
        Me.NavigationPageRegistration.Controls.Add(Me.LayoutControl4)
        Me.NavigationPageRegistration.ImageOptions.Image = CType(resources.GetObject("NavigationPageRegistration.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageRegistration.Name = "NavigationPageRegistration"
        Me.NavigationPageRegistration.Size = New System.Drawing.Size(1014, 602)
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.CheckEdit25)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit26)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit27)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit28)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit29)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit30)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit31)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit32)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit34)
        Me.LayoutControl4.Controls.Add(Me.TextEdit10)
        Me.LayoutControl4.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl4.Controls.Add(Me.MemoEdit7)
        Me.LayoutControl4.Controls.Add(Me.TextEdit11)
        Me.LayoutControl4.Controls.Add(Me.TextEdit12)
        Me.LayoutControl4.Controls.Add(Me.DateEdit4)
        Me.LayoutControl4.Location = New System.Drawing.Point(27, 23)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup3
        Me.LayoutControl4.Size = New System.Drawing.Size(971, 563)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'CheckEdit25
        '
        Me.CheckEdit25.Location = New System.Drawing.Point(317, 12)
        Me.CheckEdit25.Name = "CheckEdit25"
        Me.CheckEdit25.Properties.Caption = "DONOR"
        Me.CheckEdit25.Size = New System.Drawing.Size(94, 44)
        Me.CheckEdit25.StyleController = Me.LayoutControl4
        Me.CheckEdit25.TabIndex = 4
        '
        'CheckEdit26
        '
        Me.CheckEdit26.Location = New System.Drawing.Point(317, 60)
        Me.CheckEdit26.Name = "CheckEdit26"
        Me.CheckEdit26.Properties.Caption = "DONOR"
        Me.CheckEdit26.Size = New System.Drawing.Size(94, 44)
        Me.CheckEdit26.StyleController = Me.LayoutControl4
        Me.CheckEdit26.TabIndex = 5
        '
        'CheckEdit27
        '
        Me.CheckEdit27.Location = New System.Drawing.Point(415, 12)
        Me.CheckEdit27.Name = "CheckEdit27"
        Me.CheckEdit27.Properties.Caption = "ATTORNEY"
        Me.CheckEdit27.Size = New System.Drawing.Size(544, 44)
        Me.CheckEdit27.StyleController = Me.LayoutControl4
        Me.CheckEdit27.TabIndex = 6
        '
        'CheckEdit28
        '
        Me.CheckEdit28.Location = New System.Drawing.Point(415, 60)
        Me.CheckEdit28.Name = "CheckEdit28"
        Me.CheckEdit28.Properties.Caption = "ATTORNEY"
        Me.CheckEdit28.Size = New System.Drawing.Size(116, 44)
        Me.CheckEdit28.StyleController = Me.LayoutControl4
        Me.CheckEdit28.TabIndex = 7
        '
        'CheckEdit29
        '
        Me.CheckEdit29.Location = New System.Drawing.Point(535, 60)
        Me.CheckEdit29.Name = "CheckEdit29"
        Me.CheckEdit29.Properties.Caption = "PROFESSIONAL (Enter details below)"
        Me.CheckEdit29.Size = New System.Drawing.Size(424, 44)
        Me.CheckEdit29.StyleController = Me.LayoutControl4
        Me.CheckEdit29.TabIndex = 8
        '
        'CheckEdit30
        '
        Me.CheckEdit30.Location = New System.Drawing.Point(317, 108)
        Me.CheckEdit30.Name = "CheckEdit30"
        Me.CheckEdit30.Properties.Caption = "MR"
        Me.CheckEdit30.Size = New System.Drawing.Size(67, 44)
        Me.CheckEdit30.StyleController = Me.LayoutControl4
        Me.CheckEdit30.TabIndex = 9
        '
        'CheckEdit31
        '
        Me.CheckEdit31.Location = New System.Drawing.Point(388, 108)
        Me.CheckEdit31.Name = "CheckEdit31"
        Me.CheckEdit31.Properties.Caption = "MRS"
        Me.CheckEdit31.Size = New System.Drawing.Size(119, 44)
        Me.CheckEdit31.StyleController = Me.LayoutControl4
        Me.CheckEdit31.TabIndex = 10
        '
        'CheckEdit32
        '
        Me.CheckEdit32.Location = New System.Drawing.Point(511, 108)
        Me.CheckEdit32.Name = "CheckEdit32"
        Me.CheckEdit32.Properties.Caption = "MISS"
        Me.CheckEdit32.Size = New System.Drawing.Size(99, 44)
        Me.CheckEdit32.StyleController = Me.LayoutControl4
        Me.CheckEdit32.TabIndex = 11
        '
        'CheckEdit34
        '
        Me.CheckEdit34.Location = New System.Drawing.Point(614, 108)
        Me.CheckEdit34.Name = "CheckEdit34"
        Me.CheckEdit34.Properties.Caption = "MS"
        Me.CheckEdit34.Size = New System.Drawing.Size(345, 44)
        Me.CheckEdit34.StyleController = Me.LayoutControl4
        Me.CheckEdit34.TabIndex = 13
        '
        'TextEdit10
        '
        Me.TextEdit10.Location = New System.Drawing.Point(317, 282)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.AutoHeight = false
        Me.TextEdit10.Size = New System.Drawing.Size(375, 42)
        Me.TextEdit10.StyleController = Me.LayoutControl4
        Me.TextEdit10.TabIndex = 14
        '
        'SimpleButton2
        '
        Me.SimpleButton2.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton2.Location = New System.Drawing.Point(696, 282)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButton2.StyleController = Me.LayoutControl4
        Me.SimpleButton2.TabIndex = 15
        Me.SimpleButton2.Text = "Lookup"
        '
        'MemoEdit7
        '
        Me.MemoEdit7.Location = New System.Drawing.Point(317, 328)
        Me.MemoEdit7.Name = "MemoEdit7"
        Me.MemoEdit7.Size = New System.Drawing.Size(501, 127)
        Me.MemoEdit7.StyleController = Me.LayoutControl4
        Me.MemoEdit7.TabIndex = 16
        '
        'TextEdit11
        '
        Me.TextEdit11.Location = New System.Drawing.Point(317, 156)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Size = New System.Drawing.Size(375, 38)
        Me.TextEdit11.StyleController = Me.LayoutControl4
        Me.TextEdit11.TabIndex = 17
        '
        'TextEdit12
        '
        Me.TextEdit12.Location = New System.Drawing.Point(317, 198)
        Me.TextEdit12.Name = "TextEdit12"
        Me.TextEdit12.Size = New System.Drawing.Size(375, 38)
        Me.TextEdit12.StyleController = Me.LayoutControl4
        Me.TextEdit12.TabIndex = 18
        '
        'DateEdit4
        '
        Me.DateEdit4.EditValue = Nothing
        Me.DateEdit4.Location = New System.Drawing.Point(317, 240)
        Me.DateEdit4.Name = "DateEdit4"
        Me.DateEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Size = New System.Drawing.Size(375, 38)
        Me.DateEdit4.StyleController = Me.LayoutControl4
        Me.DateEdit4.TabIndex = 19
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = false
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.EmptySpaceItem8, Me.LayoutControlItem27, Me.LayoutControlItem28, Me.LayoutControlItem29, Me.LayoutControlItem31, Me.LayoutControlItem32, Me.LayoutControlItem30, Me.EmptySpaceItem11, Me.LayoutControlItem33, Me.LayoutControlItem34, Me.LayoutControlItem35, Me.EmptySpaceItem12, Me.EmptySpaceItem13, Me.EmptySpaceItem14, Me.LayoutControlItem36, Me.EmptySpaceItem15})
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(971, 563)
        Me.LayoutControlGroup3.TextVisible = false
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.CheckEdit25
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(403, 48)
        Me.LayoutControlItem22.Text = "Who is goint to register LPA?"
        Me.LayoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(300, 16)
        Me.LayoutControlItem22.TextToControlDistance = 5
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.CheckEdit26
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(403, 48)
        Me.LayoutControlItem23.Text = "Who is the correspondent"
        Me.LayoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem23.TextToControlDistance = 5
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.CheckEdit27
        Me.LayoutControlItem24.Location = New System.Drawing.Point(403, 0)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(548, 48)
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = false
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.CheckEdit28
        Me.LayoutControlItem25.Location = New System.Drawing.Point(403, 48)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(120, 48)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = false
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.CheckEdit29
        Me.LayoutControlItem26.Location = New System.Drawing.Point(523, 48)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(428, 48)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = false
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = false
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(0, 447)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(951, 96)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.CheckEdit30
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(376, 48)
        Me.LayoutControlItem27.Text = "Title"
        Me.LayoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem27.TextToControlDistance = 5
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.CheckEdit31
        Me.LayoutControlItem28.Location = New System.Drawing.Point(376, 96)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(123, 48)
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = false
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.CheckEdit32
        Me.LayoutControlItem29.Location = New System.Drawing.Point(499, 96)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(103, 48)
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = false
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.CheckEdit34
        Me.LayoutControlItem31.Location = New System.Drawing.Point(602, 96)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(349, 48)
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = false
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.TextEdit10
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 270)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(684, 46)
        Me.LayoutControlItem32.Text = "Postcode"
        Me.LayoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem32.TextToControlDistance = 5
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.SimpleButton2
        Me.LayoutControlItem30.Location = New System.Drawing.Point(684, 270)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(126, 46)
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = false
        '
        'EmptySpaceItem11
        '
        Me.EmptySpaceItem11.AllowHotTrack = false
        Me.EmptySpaceItem11.Location = New System.Drawing.Point(810, 270)
        Me.EmptySpaceItem11.Name = "EmptySpaceItem11"
        Me.EmptySpaceItem11.Size = New System.Drawing.Size(141, 46)
        Me.EmptySpaceItem11.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem33.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem33.Control = Me.MemoEdit7
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 316)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(810, 131)
        Me.LayoutControlItem33.Text = "Address"
        Me.LayoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem33.TextToControlDistance = 5
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.TextEdit11
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(684, 42)
        Me.LayoutControlItem34.Text = "Firstname(s)"
        Me.LayoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem34.TextToControlDistance = 5
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.TextEdit12
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 186)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(684, 42)
        Me.LayoutControlItem35.Text = "Surname"
        Me.LayoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem35.TextToControlDistance = 5
        '
        'EmptySpaceItem12
        '
        Me.EmptySpaceItem12.AllowHotTrack = false
        Me.EmptySpaceItem12.Location = New System.Drawing.Point(684, 144)
        Me.EmptySpaceItem12.Name = "EmptySpaceItem12"
        Me.EmptySpaceItem12.Size = New System.Drawing.Size(267, 42)
        Me.EmptySpaceItem12.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem13
        '
        Me.EmptySpaceItem13.AllowHotTrack = false
        Me.EmptySpaceItem13.Location = New System.Drawing.Point(684, 186)
        Me.EmptySpaceItem13.Name = "EmptySpaceItem13"
        Me.EmptySpaceItem13.Size = New System.Drawing.Size(267, 42)
        Me.EmptySpaceItem13.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem14
        '
        Me.EmptySpaceItem14.AllowHotTrack = false
        Me.EmptySpaceItem14.Location = New System.Drawing.Point(810, 316)
        Me.EmptySpaceItem14.Name = "EmptySpaceItem14"
        Me.EmptySpaceItem14.Size = New System.Drawing.Size(141, 131)
        Me.EmptySpaceItem14.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.DateEdit4
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 228)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(684, 42)
        Me.LayoutControlItem36.Text = "Date of Birth"
        Me.LayoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(300, 20)
        Me.LayoutControlItem36.TextToControlDistance = 5
        '
        'EmptySpaceItem15
        '
        Me.EmptySpaceItem15.AllowHotTrack = false
        Me.EmptySpaceItem15.Location = New System.Drawing.Point(684, 228)
        Me.EmptySpaceItem15.Name = "EmptySpaceItem15"
        Me.EmptySpaceItem15.Size = New System.Drawing.Size(267, 42)
        Me.EmptySpaceItem15.TextSize = New System.Drawing.Size(0, 0)
        '
        'NavigationPageAttendanceNotes
        '
        Me.NavigationPageAttendanceNotes.Caption = "Attendance Notes"
        Me.NavigationPageAttendanceNotes.Controls.Add(Me.PanelControl1)
        Me.NavigationPageAttendanceNotes.Controls.Add(Me.GridControl4)
        Me.NavigationPageAttendanceNotes.ImageOptions.Image = CType(resources.GetObject("NavigationPageAttendanceNotes.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageAttendanceNotes.Name = "NavigationPageAttendanceNotes"
        Me.NavigationPageAttendanceNotes.Size = New System.Drawing.Size(1014, 602)
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.MemoEdit8)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Location = New System.Drawing.Point(654, 25)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(335, 552)
        Me.PanelControl1.TabIndex = 1
        '
        'MemoEdit8
        '
        Me.MemoEdit8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit8.Location = New System.Drawing.Point(2, 39)
        Me.MemoEdit8.Name = "MemoEdit8"
        Me.MemoEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.MemoEdit8.Size = New System.Drawing.Size(331, 511)
        Me.MemoEdit8.TabIndex = 1
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(225,Byte),Integer), CType(CType(226,Byte),Integer), CType(CType(231,Byte),Integer))
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(112,Byte),Integer), CType(CType(112,Byte),Integer), CType(CType(112,Byte),Integer))
        Me.LabelControl8.Appearance.Options.UseBackColor = true
        Me.LabelControl8.Appearance.Options.UseFont = true
        Me.LabelControl8.Appearance.Options.UseForeColor = true
        Me.LabelControl8.Appearance.Options.UseTextOptions = true
        Me.LabelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl8.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl8.Location = New System.Drawing.Point(2, 2)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(331, 37)
        Me.LabelControl8.TabIndex = 0
        Me.LabelControl8.Text = "Supplementary Information"
        '
        'GridControl4
        '
        Me.GridControl4.Location = New System.Drawing.Point(21, 25)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(608, 552)
        Me.GridControl4.TabIndex = 0
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn13, Me.GridColumn14, Me.GridColumn15})
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsView.ShowGroupPanel = false
        Me.GridView4.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsView.ShowViewCaption = true
        Me.GridView4.ViewCaption = "Attendance Notes"
        '
        'GridColumn13
        '
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = true
        Me.GridColumn13.VisibleIndex = 0
        '
        'GridColumn14
        '
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = true
        Me.GridColumn14.VisibleIndex = 1
        '
        'GridColumn15
        '
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = true
        Me.GridColumn15.VisibleIndex = 2
        '
        'xLPAInstruction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1240, 662)
        Me.Controls.Add(Me.NavigationPane1)
        Me.Name = "xLPAInstruction"
        Me.Text = "LPA Instruction"
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPane1.ResumeLayout(false)
        Me.NavigationPageDonorInformation.ResumeLayout(false)
        CType(Me.GroupControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl2.ResumeLayout(false)
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl2.ResumeLayout(false)
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GroupControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl1.ResumeLayout(false)
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl1.ResumeLayout(false)
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageAttorneys.ResumeLayout(false)
        Me.NavigationPageAttorneys.PerformLayout
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit14.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit13.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit10.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageReplacementAttorneys.ResumeLayout(false)
        Me.NavigationPageReplacementAttorneys.PerformLayout
        CType(Me.GridControl2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit16.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit15.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageCertificateProvider.ResumeLayout(false)
        Me.NavigationPageCertificateProvider.PerformLayout
        CType(Me.LayoutControl3,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl3.ResumeLayout(false)
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit3.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit24.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit18.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit23.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit22.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit20.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit21.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit19.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit17.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageInstructions.ResumeLayout(false)
        CType(Me.MemoEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPagePreferences.ResumeLayout(false)
        CType(Me.MemoEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPagePersonsToBeTold.ResumeLayout(false)
        CType(Me.GridControl3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView3,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageRegistration.ResumeLayout(false)
        CType(Me.LayoutControl4,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl4.ResumeLayout(false)
        CType(Me.CheckEdit25.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit26.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit27.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit28.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit29.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit30.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit31.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit32.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit34.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit10.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit4.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem31,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem32,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem30,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem33,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem34,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem35,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem36,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem15,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageAttendanceNotes.ResumeLayout(false)
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl1.ResumeLayout(false)
        CType(Me.MemoEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView4,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents NavigationPane1 As DevExpress.XtraBars.Navigation.NavigationPane
    Friend WithEvents NavigationPageDonorInformation As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageAttorneys As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageReplacementAttorneys As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageCertificateProvider As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageInstructions As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPagePreferences As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPagePersonsToBeTold As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageRegistration As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageAttendanceNotes As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit3 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEdit14 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit13 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit12 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit11 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEdit16 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit15 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit18 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit17 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit19 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit23 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit22 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit20 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit21 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit4 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents CheckEdit24 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem1 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents MemoEdit5 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents MemoEdit6 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit25 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit26 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit27 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit28 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit29 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit30 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit31 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit32 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit34 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit7 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem11 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit4 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem12 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem13 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem14 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem15 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MemoEdit8 As DevExpress.XtraEditors.MemoEdit
End Class
