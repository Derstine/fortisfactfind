﻿Imports DevExpress.XtraEditors

Public Class xAsset
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Private Sub CheckEditYes_CheckedChanged_2(sender As Object, e As EventArgs) Handles CheckEditYes.CheckedChanged
        If CheckEditYes.Checked Then
            CheckEditNo.Checked = false
            CheckEditNotApplicable.Checked = false
        End if
    End Sub


    Private Sub CheckEditNo_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditNo.CheckedChanged  
        If CheckEditNo.Checked Then
            CheckEditYes.Checked = false
            CheckEditNotApplicable.Checked = false
            End if
    End Sub

    Private Sub CheckEditNotApplicable_CheckStateChanged(sender As Object, e As EventArgs) Handles CheckEditNotApplicable.CheckStateChanged 
        If CheckEditNotApplicable.Checked Then
            CheckEditYes.Checked = false
            CheckEditNo.Checked = false
        End if
    End Sub

    Private Sub SimpleButtonClose_Click(sender As Object, e As EventArgs) Handles SimpleButtonClose.Click 
        Close()
    End Sub

    Private Sub xAsset_Load(sender As Object, e As EventArgs) Handles MyBase.Load 

        Me.ComboBoxEditAssetCategory.SelectedIndex = 0

        If clientType() = "single" Then
            CheckEditClient1.Text = getClientFirstName("client1")
            CheckEditClient2.Enabled = False
            CheckEditJointly.Enabled = False
            CheckEditClient1.Checked = True
            CheckEditClient1.Enabled = False
        Else
            CheckEditClient1.Text = getClientFirstName("client1")
            CheckEditClient2.Text = getClientFirstName("client2")
            CheckEditClient2.Enabled = True
            CheckEditJointly.Enabled = True
            CheckEditClient1.Enabled = True
        End If
    End Sub

    Private Sub SimpleButtonSave_Click(sender As Object, e As EventArgs) Handles SimpleButtonSave.Click 
        Dim tablename = "fapt_trust_fund_futher"  'this is just the default
        Dim sql = ""
        Dim whichClient = ""

        Dim errorcount = 0
        Dim errortext = ""

        If ComboBoxEditAssetCategory.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset category" + vbCrLf
        If MemoEditDescription.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter a brief description" + vbCrLf
        If Val(TextEditValue.Text) = 0 Or TextEditValue.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset value" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            If ComboBoxEditAssetCategory.Text <> "Select..." Then

                If ComboBoxEditAssetCategory.Text = "Property" Then tablename = "fapt_trust_fund_property"
                If ComboBoxEditAssetCategory.Text = "Cars/Bikes/Motorhomes" Then tablename = "fapt_trust_fund_chattels"
                If ComboBoxEditAssetCategory.Text = "Valuables" Then tablename = "fapt_trust_fund_chattels"
                If ComboBoxEditAssetCategory.Text = "Contents" Then tablename = "fapt_trust_fund_chattels"
                If ComboBoxEditAssetCategory.Text = "Bank Accounts" Then tablename = "fapt_trust_fund_cash"
                If ComboBoxEditAssetCategory.Text = "ISA" Then tablename = "fapt_trust_fund_cash"
                If ComboBoxEditAssetCategory.Text = "Bond" Then tablename = "fapt_trust_fund_cash"
                If ComboBoxEditAssetCategory.Text = "Other Investments" Then tablename = "fapt_trust_fund_cash"

                If CheckEditClient1.Checked = True Then whichClient = "client1"
                If CheckEditClient2.Checked = True Then whichClient = "client2"
                If CheckEditJointly.Checked = True Then whichClient = "both"

                If tablename = "fapt_trust_fund_property" Then
                    Dim mortgageFlag = ""
                    If CheckEditYes.Checked = True Then mortgageFlag = "Y"
                    If CheckEditNo.Checked = True Then mortgageFlag = "N"
                    If CheckEditNotApplicable.Checked = True Then mortgageFlag = "Z"
                    sql = "insert into " + tablename + "(willClientSequence,description,worth,whichClient,ComboBoxEditAssetCategory,mortgageFlag) values ('" + clientID + "','" + SqlSafe(MemoEditDescription.Text) + "','" + SqlSafe(TextEditValue.Text) + "','" + whichClient + "','" + SqlSafe(ComboBoxEditAssetCategory.Text) + "','" + SqlSafe(mortgageFlag) + "')"
                ElseIf tablename = "fapt_trust_fund_cash" Then
                    Dim currentCharges As String = Me.TextEditCurrentCharges.Text

                    sql = "insert into " + tablename + "(willClientSequence,description,worth,whichClient,ComboBoxEditAssetCategory,currentCharges) values ('" + clientID + "','" + SqlSafe(MemoEditDescription.Text) + "','" + SqlSafe(TextEditValue.Text) + "','" + whichClient + "','" + SqlSafe(ComboBoxEditAssetCategory.Text) + "','" + SqlSafe(currentCharges) + "')"
                Else
                    sql = "insert into " + tablename + "(willClientSequence,description,worth,whichClient,ComboBoxEditAssetCategory) values ('" + clientID + "','" + SqlSafe(MemoEditDescription.Text) + "','" + SqlSafe(TextEditValue.Text) + "','" + whichClient + "','" + SqlSafe(ComboBoxEditAssetCategory.Text) + "')"
                End If
                runSQL(sql)
                Me.Close()
            End If

        End If
    End Sub

    Private Sub ComboBoxEditAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxEditAssetCategory.SelectedIndexChanged 
        If Me.ComboBoxEditAssetCategory.SelectedIndex <> 1 Then
            CheckEditNotApplicable.Checked = True
            
            CheckEditYes.Enabled=false
            CheckEditNo.Enabled=false
            CheckEditNotApplicable.Enabled=false


        Else
            CheckEditYes.Enabled=True
            CheckEditNo.Enabled=True
            CheckEditNotApplicable.Enabled=True
        End If

        If Me.ComboBoxEditAssetCategory.SelectedIndex = 6 Or Me.ComboBoxEditAssetCategory.SelectedIndex = 7 Or Me.ComboBoxEditAssetCategory.SelectedIndex = 8 Then
            TextEditCurrentCharges.Enabled=True
        Else
            TextEditCurrentCharges.Enabled=False
        End If
    End Sub

    Private Sub CheckEditClient1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditClient1.CheckedChanged
        If CheckEditClient1.Checked Then
            CheckEditClient2.Checked = false
            CheckEditJointly.Checked = false
        End if
    End Sub

    Private Sub CheckEditClient2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditClient2.CheckedChanged
        If CheckEditClient2.Checked Then
            CheckEditClient1.Checked = false
            CheckEditJointly.Checked = false
        End if
    End Sub

    Private Sub CheckEditJointly_CheckedChanged_1(sender As Object, e As EventArgs) Handles CheckEditJointly.CheckedChanged
        If CheckEditJointly.Checked Then
            CheckEditClient1.Checked = false
            CheckEditClient2.Checked = false
        End if
    End Sub
End Class