﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xAsset
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xAsset))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.ComboBoxEditAssetCategory = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEditValue = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditYes = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditNo = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditNotApplicable = New DevExpress.XtraEditors.CheckEdit()
        Me.MemoEditDescription = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEditCurrentCharges = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditClient1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditClient2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditJointly = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButtonClose = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl1.SuspendLayout
        CType(Me.ComboBoxEditAssetCategory.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditValue.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditYes.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditNo.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditNotApplicable.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEditDescription.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditCurrentCharges.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditClient1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditClient2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditJointly.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEditAssetCategory)
        Me.LayoutControl1.Controls.Add(Me.TextEditValue)
        Me.LayoutControl1.Controls.Add(Me.CheckEditYes)
        Me.LayoutControl1.Controls.Add(Me.CheckEditNo)
        Me.LayoutControl1.Controls.Add(Me.CheckEditNotApplicable)
        Me.LayoutControl1.Controls.Add(Me.MemoEditDescription)
        Me.LayoutControl1.Controls.Add(Me.TextEditCurrentCharges)
        Me.LayoutControl1.Controls.Add(Me.CheckEditClient1)
        Me.LayoutControl1.Controls.Add(Me.CheckEditClient2)
        Me.LayoutControl1.Controls.Add(Me.CheckEditJointly)
        Me.LayoutControl1.Controls.Add(Me.SimpleButtonClose)
        Me.LayoutControl1.Controls.Add(Me.SimpleButtonSave)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(653, 409)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'ComboBoxEditAssetCategory
        '
        Me.ComboBoxEditAssetCategory.Location = New System.Drawing.Point(119, 33)
        Me.ComboBoxEditAssetCategory.Name = "ComboBoxEditAssetCategory"
        Me.ComboBoxEditAssetCategory.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditAssetCategory.Properties.Items.AddRange(New Object() {"Property", "Cars/BIkes/Motorhomes", "Valuebles", "Contents", "Bank Accounts", "ISA", "Bond", "Other Investment"})
        Me.ComboBoxEditAssetCategory.Properties.NullValuePrompt = "Select Category"
        Me.ComboBoxEditAssetCategory.Size = New System.Drawing.Size(520, 38)
        Me.ComboBoxEditAssetCategory.StyleController = Me.LayoutControl1
        Me.ComboBoxEditAssetCategory.TabIndex = 4
        '
        'TextEditValue
        '
        Me.TextEditValue.Location = New System.Drawing.Point(119, 173)
        Me.TextEditValue.Name = "TextEditValue"
        Me.TextEditValue.Size = New System.Drawing.Size(520, 38)
        Me.TextEditValue.StyleController = Me.LayoutControl1
        Me.TextEditValue.TabIndex = 6
        '
        'CheckEditYes
        '
        Me.CheckEditYes.Location = New System.Drawing.Point(119, 215)
        Me.CheckEditYes.Name = "CheckEditYes"
        Me.CheckEditYes.Properties.Caption = "Yes"
        Me.CheckEditYes.Size = New System.Drawing.Size(127, 44)
        Me.CheckEditYes.StyleController = Me.LayoutControl1
        Me.CheckEditYes.TabIndex = 7
        '
        'CheckEditNo
        '
        Me.CheckEditNo.Location = New System.Drawing.Point(250, 215)
        Me.CheckEditNo.Name = "CheckEditNo"
        Me.CheckEditNo.Properties.Caption = "No"
        Me.CheckEditNo.Size = New System.Drawing.Size(138, 44)
        Me.CheckEditNo.StyleController = Me.LayoutControl1
        Me.CheckEditNo.TabIndex = 8
        '
        'CheckEditNotApplicable
        '
        Me.CheckEditNotApplicable.Location = New System.Drawing.Point(392, 215)
        Me.CheckEditNotApplicable.Name = "CheckEditNotApplicable"
        Me.CheckEditNotApplicable.Properties.Caption = "Not Applicable"
        Me.CheckEditNotApplicable.Size = New System.Drawing.Size(247, 44)
        Me.CheckEditNotApplicable.StyleController = Me.LayoutControl1
        Me.CheckEditNotApplicable.TabIndex = 9
        '
        'MemoEditDescription
        '
        Me.MemoEditDescription.Location = New System.Drawing.Point(119, 75)
        Me.MemoEditDescription.Name = "MemoEditDescription"
        Me.MemoEditDescription.Size = New System.Drawing.Size(520, 94)
        Me.MemoEditDescription.StyleController = Me.LayoutControl1
        Me.MemoEditDescription.TabIndex = 5
        '
        'TextEditCurrentCharges
        '
        Me.TextEditCurrentCharges.Location = New System.Drawing.Point(119, 263)
        Me.TextEditCurrentCharges.Name = "TextEditCurrentCharges"
        Me.TextEditCurrentCharges.Size = New System.Drawing.Size(520, 38)
        Me.TextEditCurrentCharges.StyleController = Me.LayoutControl1
        Me.TextEditCurrentCharges.TabIndex = 10
        '
        'CheckEditClient1
        '
        Me.CheckEditClient1.Location = New System.Drawing.Point(119, 305)
        Me.CheckEditClient1.Name = "CheckEditClient1"
        Me.CheckEditClient1.Properties.Caption = "Client 1"
        Me.CheckEditClient1.Size = New System.Drawing.Size(127, 44)
        Me.CheckEditClient1.StyleController = Me.LayoutControl1
        Me.CheckEditClient1.TabIndex = 11
        '
        'CheckEditClient2
        '
        Me.CheckEditClient2.Location = New System.Drawing.Point(250, 305)
        Me.CheckEditClient2.Name = "CheckEditClient2"
        Me.CheckEditClient2.Properties.Caption = "Client 2"
        Me.CheckEditClient2.Size = New System.Drawing.Size(138, 44)
        Me.CheckEditClient2.StyleController = Me.LayoutControl1
        Me.CheckEditClient2.TabIndex = 12
        '
        'CheckEditJointly
        '
        Me.CheckEditJointly.Location = New System.Drawing.Point(392, 305)
        Me.CheckEditJointly.Name = "CheckEditJointly"
        Me.CheckEditJointly.Properties.Caption = "Jointly"
        Me.CheckEditJointly.Size = New System.Drawing.Size(247, 44)
        Me.CheckEditJointly.StyleController = Me.LayoutControl1
        Me.CheckEditJointly.TabIndex = 13
        '
        'SimpleButtonClose
        '
        Me.SimpleButtonClose.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonClose.Location = New System.Drawing.Point(364, 353)
        Me.SimpleButtonClose.Name = "SimpleButtonClose"
        Me.SimpleButtonClose.Size = New System.Drawing.Size(275, 42)
        Me.SimpleButtonClose.StyleController = Me.LayoutControl1
        Me.SimpleButtonClose.TabIndex = 14
        Me.SimpleButtonClose.Text = "Cancel"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonSave.Location = New System.Drawing.Point(130, 353)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(230, 42)
        Me.SimpleButtonSave.StyleController = Me.LayoutControl1
        Me.SimpleButtonSave.TabIndex = 15
        Me.SimpleButtonSave.Text = "Save Asset"
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[False]
        Me.Root.GroupBordersVisible = false
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup1})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(653, 409)
        Me.Root.TextVisible = false
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.EmptySpaceItem3, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem8})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(653, 409)
        Me.LayoutControlGroup1.Text = "Asset Information"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.ComboBoxEditAssetCategory
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(629, 42)
        Me.LayoutControlItem1.Text = "Asset Category"
        Me.LayoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem1.TextToControlDistance = 5
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem2.Control = Me.MemoEditDescription
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 42)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(629, 98)
        Me.LayoutControlItem2.Text = "Description"
        Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem2.TextToControlDistance = 5
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.TextEditValue
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 140)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(629, 42)
        Me.LayoutControlItem3.Text = "Value"
        Me.LayoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem3.TextToControlDistance = 5
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.CheckEditYes
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 182)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(236, 48)
        Me.LayoutControlItem4.Text = "Mortgage?"
        Me.LayoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem4.TextToControlDistance = 5
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.CheckEditNo
        Me.LayoutControlItem5.Location = New System.Drawing.Point(236, 182)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(142, 48)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = false
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.CheckEditNotApplicable
        Me.LayoutControlItem6.Location = New System.Drawing.Point(378, 182)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(251, 48)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = false
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.TextEditCurrentCharges
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 230)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(629, 42)
        Me.LayoutControlItem7.Text = "Current Charges"
        Me.LayoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem7.TextToControlDistance = 5
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.CheckEditClient1
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 272)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(236, 48)
        Me.LayoutControlItem8.Text = "Owned By"
        Me.LayoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem8.TextToControlDistance = 5
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.CheckEditClient2
        Me.LayoutControlItem9.Location = New System.Drawing.Point(236, 272)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(142, 48)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = false
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.CheckEditJointly
        Me.LayoutControlItem10.Location = New System.Drawing.Point(378, 272)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(251, 48)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = false
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.SimpleButtonClose
        Me.LayoutControlItem11.Location = New System.Drawing.Point(350, 320)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(279, 46)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = false
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.SimpleButtonSave
        Me.LayoutControlItem12.Location = New System.Drawing.Point(116, 320)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(234, 46)
        Me.LayoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextToControlDistance = 0
        Me.LayoutControlItem12.TextVisible = false
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = false
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 320)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(116, 46)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'xAsset
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 409)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "xAsset"
        Me.Text = "xAsset"
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl1.ResumeLayout(false)
        CType(Me.ComboBoxEditAssetCategory.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditValue.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditYes.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditNo.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditNotApplicable.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEditDescription.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditCurrentCharges.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditClient1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditClient2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditJointly.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ComboBoxEditAssetCategory As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TextEditValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditYes As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditNo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditNotApplicable As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents MemoEditDescription As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEditCurrentCharges As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditClient1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditClient2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditJointly As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButtonClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
End Class
