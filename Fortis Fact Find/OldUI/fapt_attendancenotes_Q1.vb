﻿Public Class fapt_attendancenotes_Q1
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'check to see all sections filled in
        Dim errorCount = 0
        Dim errorList = ""

        If Me.TextBox1.Text = "" Then
            errorCount = errorCount + 1
            errorList = errorList + "You must enter the trust name in the space provided" + vbCrLf
        End If

        If errorCount > 0 Then
            MsgBox("You have the following error(s):" + vbCrLf + vbCrLf + errorList, MsgBoxStyle.Exclamation)
        Else

            'save data
            Dim tick = "1"
            Dim detail As String

            If Me.TextBox1.Text <> "" Then
                detail = SqlSafe(Me.TextBox1.Text)
            Else
                detail = ""
            End If

            Dim sql As String = "update fapt_attendance_notes set q1='" + detail + "', q1tick=" + tick + " where willClientSequence=" + clientID
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub will_attendance_notesQ1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If factfind_fapt.attendanceMode.Text = "edit" Then
            Dim sql As String = "select q1,q1tick from fapt_attendance_notes  where willClientSequence=" + clientID
            Dim results As Array = runSQLwithArray(sql)

            Me.TextBox1.Text = results(0)

        End If

    End Sub
End Class