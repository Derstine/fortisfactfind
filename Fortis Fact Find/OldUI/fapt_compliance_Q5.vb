﻿Public Class fapt_compliance_Q5
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged

        If Me.RadioButton2.Checked = True Then
            Me.TextBox1.Enabled = True
        Else
            Me.TextBox1.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'check to see all sections filled in
        Dim errorCount = 0
        Dim errorList = ""

        If Me.RadioButton1.Checked = False And Me.RadioButton2.Checked = False Then
            errorCount = errorCount + 1
            errorList = errorList + "You must answer the question" + vbCrLf
        End If

        If Me.RadioButton2.Checked = True And Me.TextBox1.Text = "" Then
            errorCount = errorCount + 1
            errorList = errorList + "You must enter further details in the space provided" + vbCrLf
        End If

        If errorCount > 0 Then
            MsgBox("You have the following error(s):" + vbCrLf + vbCrLf + errorList, MsgBoxStyle.Exclamation)
        Else

            'save data
            Dim tick As String
            Dim detail As String

            If Me.RadioButton2.Checked = True Then
                tick = "1"
            Else
                tick = "-1"
            End If

            If Me.TextBox1.Text <> "" Then
                detail = SqlSafe(Me.TextBox1.Text)
            Else
                detail = ""
            End If

            Dim sql As String = "update fapt_compliance set q5='" + detail + "', q5tick=" + tick + " where willClientSequence=" + clientID
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub will_attendance_notesQ1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If factfind_fapt.attendanceMode.Text = "edit" Then
            Dim sql As String = "select q5,q5tick from fapt_compliance  where willClientSequence=" + clientID
            Dim results As Array = runSQLwithArray(sql)

            If results(1) = "1" Then
                Me.RadioButton2.Checked = True
                Me.TextBox1.Text = results(0)
            Else
                Me.RadioButton1.Checked = True
                Me.TextBox1.Text = ""
            End If
        End If

    End Sub
End Class