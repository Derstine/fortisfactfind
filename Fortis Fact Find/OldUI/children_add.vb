﻿Public Class children_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.childTitle.SelectedIndex = 0
        Me.relationToClient1.SelectedIndex = 0
        Me.relationToClient2.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")
        Button4.Text = "copy from " + getClientFirstName("client1")
        Button5.Text = "copy from " + getClientFirstName("client5")

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.relationToClient2.Visible = True
            Button5.Visible = True
        Else
            Me.Label3.Visible = False
            Me.relationToClient2.Visible = False
            Button5.Visible = False
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If childTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the child's title" + vbCrLf
        If childForenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the child's name" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into will_instruction_stage4_data_detail (willClientSequence,childTitle,childForenames,childSurname,relationToClient1,relationToClient2,address,childPostcode,childDOB) values ('" + clientID + "','" + SqlSafe(childTitle.SelectedItem) + "','" + SqlSafe(childForenames.Text) + "','" + SqlSafe(childSurname.Text) + "','" + SqlSafe(relationToClient1.SelectedItem) + "','" + SqlSafe(relationToClient2.SelectedItem) + "','" + SqlSafe(address.Text) + "','" + SqlSafe(childPostcode.Text) + "','" + SqlSafe(childDOB.Value) + "')"
            runSQL(sql)
            End If

            Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        childPostcode.Text = childPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = childPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.address.Text = f.FoundAddress()
            Me.childPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim sql As String = "select person1Postcode,person1Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        childPostcode.Text = results(0)
        address.Text = results(1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim sql As String = "select person2Postcode,person2Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        childPostcode.Text = results(0)
        address.Text = results(1)
    End Sub
End Class