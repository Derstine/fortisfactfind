﻿Public Class factfind_lpa
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        spy("LPA Fact Find screen opened")

        'this make take a while so show wait cursor
        Me.Cursor = Cursors.WaitCursor

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        'reset cursor
        Me.Cursor = Cursors.Default

    End Sub

    Private Function setDefaults()

        GroupBox2.Visible = False   'donor details
        CheckBox1.Checked = False   'lpa choices on Donor screen
        CheckBox2.Checked = False
        CheckBox3.Checked = False
        CheckBox4.Checked = False

        RadioButton2.Checked = True 'lpa attorney
        RadioButton3.Checked = True 'lpa attorney
        RadioButton5.Checked = True 'lpa attorney
        RadioButton9.Checked = True 'lpa attorney
        DataGridView5.Rows.Clear()    'lpa attorney
        DataGridView5.Visible = False
        Button6.Visible = False
        TextBox1.Visible = False
        Label9.Visible = False

        RadioButton12.Checked = True 'lpa replacement attorney
        DataGridView1.Rows.Clear()    'lpa replacement attorney list
        DataGridView1.Visible = False
        Button1.Visible = False

        RadioButton13.Checked = True 'certificate provider
        Panel7.Visible = False

        RadioButton17.Checked = True 'lpa registration
        RadioButton15.Checked = True
        Panel10.Visible = False

        DataGridView2.Rows.Clear()    'people to be told

        DataGridView7.Rows.Clear()    'attendance notes

        Return True
    End Function

    Private Function populateScreen()
        Dim sql = ""
        Dim results As Array
        Dim result = ""

        If clientType() = "mirror" Then GroupBox2.Visible = True Else GroupBox2.Visible = False

        'client details tab

        sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            TextBox2.Text = results(12) + " " + results(14) + " " + results(13)
            TextBox4.Text = results(16)
            TextBox5.Text = results(67)
            If results(18).ToString <> "" Then
                TextBox3.Text = results(18).ToString.Substring(0, 10)
            Else
                TextBox3.Text = ""
            End If

            TextBox9.Text = results(23) + " " + results(25) + " " + results(24)
            If results(29).ToString <> "" Then
                TextBox8.Text = results(29).ToString.Substring(0, 10)
            Else
                TextBox8.Text = ""
            End If
            TextBox7.Text = results(27)
            TextBox6.Text = results(68)
            End If

            If getLPALookup("Client1PF") = "Y" Then Me.CheckBox1.Checked = True
        If getLPALookup("Client1HW") = "Y" Then Me.CheckBox2.Checked = True
        If getLPALookup("Client2PF") = "Y" Then Me.CheckBox4.Checked = True
        If getLPALookup("Client2HW") = "Y" Then Me.CheckBox3.Checked = True

        ''''''''''''''
        'attorneys
        ''''''''''''''
        sql = "select * from lpa_attorneys where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            If results(5) = "solely" Then RadioButton7.Checked = True
            If results(5) = "jointly" Then RadioButton8.Checked = True
            If results(5) = "both" Then RadioButton9.Checked = True
            If results(5) = "mixed" Then RadioButton10.Checked = True
            Me.TextBox1.Text = results(6)
        Else
            'no data so therefore do nowt
        End If

        If getLPALookup("spouseAttorney") = "Y" Then Me.RadioButton1.Checked = True
        If getLPALookup("spouseAttorney") = "N" Then Me.RadioButton2.Checked = True
        If getLPALookup("kidsAttorney") = "Y" Then Me.RadioButton4.Checked = True
        If getLPALookup("kidsAttorney") = "N" Then Me.RadioButton3.Checked = True
        If getLPALookup("otherAttorney") = "Y" Then Me.RadioButton6.Checked = True
        If getLPALookup("otherAttorney") = "N" Then Me.RadioButton5.Checked = True

        If getLPALookup("howToAct") = "solely" Then Me.RadioButton7.Checked = True
        If getLPALookup("howToAct") = "jointly" Then Me.RadioButton8.Checked = True
        If getLPALookup("howToAct") = "both" Then Me.RadioButton9.Checked = True
        If getLPALookup("howToAct") = "mixed" Then Me.RadioButton10.Checked = True

        redrawAttorneysGrid()
        ''''''''''''''
        'replacement attorneys
        ''''''''''''''
        sql = "select * from lpa_replacement_attorneys where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            If results(2) = "joint" Then RadioButton11.Checked = True
        Else
            'no data so therefore do nowt
        End If
        redrawReplacementAttorneysGrid()

        ''''''''''''''
        'certificate provider details
        ''''''''''''''
        sql = "select * from lpa_certificate_provider_details where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            If results(2) = "prof" Then RadioButton13.Checked = True

            If results(2) = "sole" Then
                RadioButton14.Checked = True

                Me.certificateNames.Text = results(3)
                Me.certificateAddress.Text = results(7)
                Me.certificateRelationshipClient1.SelectedItem = results(5)
                Me.certificateRelationshipClient2.SelectedItem = results(6)
                Me.certificateTitle.SelectedItem = results(13)
                Me.certificateSurname.Text = results(14)
                Me.certificatePostcode.Text = results(15)

            End If

        Else
            'no data so therefore do nowt
        End If

        ''''''''''''''
        'preferences & instructions
        ''''''''''''''
        sql = "select * from lpa_preferences where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            Me.mustDoNotes.Text = results(5)
            Me.shouldDoNotes.Text = results(6)
        Else
            'no data so therefore do nowt
        End If

        ''''''''''''''
        'registration
        ''''''''''''''
        sql = "select * from lpa_register where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            If results(4) = "donor" Then RadioButton17.Checked = True
            If results(4) = "attorney" Then RadioButton18.Checked = True

            If results(9) = "Donor" Then RadioButton15.Checked = True
            If results(9) = "Attorney" Then RadioButton16.Checked = True
            If results(9) = "Professional" Then RadioButton19.Checked = True

            corresTitle.SelectedItem = results(22)
            corresNames.Text = results(10)
            corresSurname.Text = results(23)
            corresPostcode.Text = results(24)
            corresAddress.Text = results(12)
        Else
            'no data so therefore do nowt
        End If

        ''''''''''''''
        'attendance notes
        ''''''''''''''
        redrawPersonsGrid()

        ''''''''''''''
        'attendance notes
        ''''''''''''''
        sql = "select notes from lpa_attendance_notes where willClientSequence=" + clientID
        results = runSQLwithArray(Sql)
        If results(0) <> "ERROR" Then
            Me.TextBox20.Text = results(0)
        Else
            'no data so therefore insert row
            sql = "insert into lpa_attendance_notes (willClientSequence) values (" + clientID + ")"
            runSQL(Sql)
        End If
        redrawAttendanceNotesGrid()

        Return True
    End Function


    Function redrawAttendanceNotesGrid()
        Dim sql = ""
        Dim answer(2) As String
        DataGridView7.Rows.Clear()

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from lpa_attendance_notes  where willClientSequence=" + clientID
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        If dbreader.Read() Then

            For i = 1 To 10
                answer = getAnswer(i)
                DataGridView7.Rows.Add(i, "Q" + i.ToString, answer(0), answer(1))
            Next

        End If
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function getAnswer(ByVal i As Integer) As Array

        Dim returnvalue(2) As String

        Dim sql As String = "select q" + i.ToString + "tick from lpa_attendance_notes where willClientSequence=" + clientID
        Dim result As String = runSQLwithID(sql)

        If result = "" Then
            returnvalue(0) = "not answered"
            returnvalue(1) = "answer"
        Else
            'first 5 questions
            If i <= 5 Then
                If Val(result) > 0 Then
                    returnvalue(0) = "Yes"
                Else
                    returnvalue(0) = "No"
                End If
            End If

            'q6 is only Health & Welfare
            If i = 6 Then
                If Val(result) > 0 Then
                    returnvalue(0) = "Yes"
                ElseIf Val(result) < 1 Then
                    returnvalue(0) = "No"

                Else
                    returnvalue(0) = "Not Applicable"
                End If
            End If

            'q7
            If i = 7 Then
                If Val(result) > 0 Then
                    returnvalue(0) = "Consultant"
                Else
                    returnvalue(0) = "Acquaintance"
                End If

            End If

            'q8
            If i = 8 Then
                If Val(result) > 0 Then
                    returnvalue(0) = "WARNING!!!"
                Else
                    returnvalue(0) = "OK"
                End If

            End If

            'q9
            If i = 9 Then
                If Val(result) > 0 Then
                    returnvalue(0) = "Later"
                Else
                    returnvalue(0) = "Now"
                End If

            End If

            If i = 10 Then
                returnvalue(0) = "Not Answered"
                If Val(result) = 1 Then returnvalue(0) = "Claiming Benefits"
                If Val(result) = 2 Then returnvalue(0) = "Universal Credit"
                If Val(result) = 3 Then returnvalue(0) = "Low Income"
                If Val(result) = 4 Then returnvalue(0) = "No Reduction"

            End If
            returnvalue(1) = "edit"
        End If

        Return returnvalue
    End Function


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        '  NEED TO SAVE THE DATA 
        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        Dim sql As String
        Dim results As Array
        Dim whichClient = ""
        Dim whichDocument = ""

        'this might take a while, so show the wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.Enabled = False

        ''''''''''''''
        'whichLPAs required
        ''''''''''''''
        If Me.CheckBox1.Checked = True Then setLPALookup("Client1PF", "Y") Else setLPALookup("Client1PF", "N")
        If Me.CheckBox2.Checked = True Then setLPALookup("Client1HW", "Y") Else setLPALookup("Client1HW", "N")
        If Me.CheckBox4.Checked = True Then setLPALookup("Client2PF", "Y") Else setLPALookup("Client2PF", "N")
        If Me.CheckBox3.Checked = True Then setLPALookup("Client2HW", "Y") Else setLPALookup("Client2HW", "N")

        ''''''''''''''
        'attorneys
        ''''''''''''''
        sql = "delete from lpa_attorneys where willClientSequence=" + clientID
        runSQL(sql)

        Dim attorneyType = "joint"    'sets the default
        Dim howToAct = ""
        Dim howToActNotes As String = Me.TextBox1.Text

        If Me.RadioButton1.Checked = True And Me.RadioButton3.Checked = True And Me.RadioButton5.Checked = True Then attorneyType = "sole"
        If Me.RadioButton1.Checked = True And (Me.RadioButton4.Checked = True Or Me.RadioButton6.Checked = True) Then attorneyType = "spousejoint"

        If Me.RadioButton7.Checked = True Then howToAct = "solely"
        If Me.RadioButton8.Checked = True Then howToAct = "jointly"
        If Me.RadioButton9.Checked = True Then howToAct = "both"
        If Me.RadioButton10.Checked = True Then howToAct = "mixed"

        setLPALookup("howToAct", howToAct)

        If Me.CheckBox1.Checked = True Then whichClient = "client1" : whichDocument = "PFA" : sql = "insert into lpa_attorneys (willClientSequence,whichClient,whichDocument,attorneyType,howToAct,howToActNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(attorneyType) + "','" + SqlSafe(howToAct) + "','" + SqlSafe(howToActNotes) + "')" : runSQL(sql)
        If Me.CheckBox2.Checked = True Then whichClient = "client1" : whichDocument = "HW" : sql = "insert into lpa_attorneys (willClientSequence,whichClient,whichDocument,attorneyType,howToAct,howToActNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(attorneyType) + "','" + SqlSafe(howToAct) + "','" + SqlSafe(howToActNotes) + "')" : runSQL(sql)
        If Me.CheckBox4.Checked = True Then whichClient = "client2" : whichDocument = "PFA" : sql = "insert into lpa_attorneys (willClientSequence,whichClient,whichDocument,attorneyType,howToAct,howToActNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(attorneyType) + "','" + SqlSafe(howToAct) + "','" + SqlSafe(howToActNotes) + "')" : runSQL(sql)
        If Me.CheckBox3.Checked = True Then whichClient = "client2" : whichDocument = "HW" : sql = "insert into lpa_attorneys (willClientSequence,whichClient,whichDocument,attorneyType,howToAct,howToActNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(attorneyType) + "','" + SqlSafe(howToAct) + "','" + SqlSafe(howToActNotes) + "')" : runSQL(sql)
        runSQL(sql)

        If Me.RadioButton1.Checked = True Then setLPALookup("spouseAttorney", "Y")
        If Me.RadioButton2.Checked = True Then setLPALookup("spouseAttorney", "N")
        If Me.RadioButton4.Checked = True Then setLPALookup("kidsAttorney", "Y")
        If Me.RadioButton3.Checked = True Then setLPALookup("kidsAttorney", "N")
        If Me.RadioButton6.Checked = True Then setLPALookup("otherAttorney", "Y")
        If Me.RadioButton5.Checked = True Then setLPALookup("otherAttorney", "N")

        ''''''''''''''
        'attorneys details 
        ''''''''''''''

        'NEED TO READ IN THE ROWS THEN REPLICATE FOR EACH OF THE 4 POSSIBLE TYPES
        'HOWEVER, WILL DO THIS WHEN WE SYNCHRONISE OTHERWISE IN THE DATAGRID THE ATTORNEYS
        'WILL BE REPEATED UP TO 4 TIMES

        ''''''''''''''
        'replacement attorneys
        ''''''''''''''
        sql = "delete from lpa_replacement_attorneys where willClientSequence=" + clientID
        runSQL(sql)

        If RadioButton11.Checked = True Then
            If Me.CheckBox1.Checked = True Then whichClient = "client1" : whichDocument = "PFA" : sql = "insert into lpa_replacement_attorneys (willClientSequence,whichClient,whichDocument,attorneyType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','joint')" : runSQL(sql)
            If Me.CheckBox2.Checked = True Then whichClient = "client1" : whichDocument = "HW" : sql = "insert into lpa_replacement_attorneys (willClientSequence,whichClient,whichDocument,attorneyType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','joint')" : runSQL(sql)
            If Me.CheckBox4.Checked = True Then whichClient = "client2" : whichDocument = "PFA" : sql = "insert into lpa_replacement_attorneys (willClientSequence,whichClient,whichDocument,attorneyType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','joint')" : runSQL(sql)
            If Me.CheckBox3.Checked = True Then whichClient = "client2" : whichDocument = "HW" : sql = "insert into lpa_replacement_attorneys (willClientSequence,whichClient,whichDocument,attorneyType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','joint')" : runSQL(sql)
            runSQL(sql)
        End If

        ''''''''''''''
        'replacement attorneys details 
        ''''''''''''''

        'NEED TO READ IN THE ROWS THEN REPLICATE FOR EACH OF THE 4 POSSIBLE TYPES
        'HOWEVER, WILL DO THIS WHEN WE SYNCHRONISE OTHERWISE IN THE DATAGRID THE ATTORNEYS
        'WILL BE REPEATED UP TO 4 TIMES


        ''''''''''''''
        'certificate provider
        ''''''''''''''
        sql = "delete from lpa_certificate_provider where willClientSequence=" + clientID
        runSQL(sql)

        If Me.CheckBox1.Checked = True Then whichClient = "client1" : whichDocument = "PFA" : sql = "insert into lpa_certificate_provider (willClientSequence,whichClient,whichDocument,certificateType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','sole')" : runSQL(sql)
        If Me.CheckBox2.Checked = True Then whichClient = "client1" : whichDocument = "HW" : sql = "insert into lpa_certificate_provider (willClientSequence,whichClient,whichDocument,certificateType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','sole')" : runSQL(sql)
        If Me.CheckBox4.Checked = True Then whichClient = "client2" : whichDocument = "PFA" : sql = "insert into lpa_certificate_provider (willClientSequence,whichClient,whichDocument,certificateType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','sole')" : runSQL(sql)
        If Me.CheckBox3.Checked = True Then whichClient = "client2" : whichDocument = "HW" : sql = "insert into lpa_certificate_provider (willClientSequence,whichClient,whichDocument,certificateType) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','sole')" : runSQL(sql)


        ''''''''''''''
        'certificate provider details
        ''''''''''''''
        sql = "delete from lpa_certificate_provider_details where willClientSequence=" + clientID
        runSQL(sql)

        Dim certificateType = ""
        Dim certificateNames = ""
        Dim certificateAddress = ""
        Dim certificateRelationshipClient1 = ""
        Dim certificateRelationshipClient2 = ""
        Dim certificateTitle = ""
        Dim certificateSurname = ""
        Dim certificatePostcode = ""

        If RadioButton13.Checked = True Then
            certificateType = "prof"
            'is there data to lookup?
            sql = "select * from userdata"
            results = runSQLwithArray(sql)
            If results(0) <> "ERROR" And results(0) <> "" Then
                certificateNames = results(2)
                certificateAddress = results(4)
                certificateRelationshipClient1 = "Professional Consultant"
                certificateRelationshipClient2 = "Professional Consultant"
                certificateTitle = results(1)
                certificateSurname = results(3)
                certificatePostcode = results(5)
            End If
        End If

        If RadioButton14.Checked = True Then
            certificateType = "sole"
            certificateNames = Me.certificateNames.Text
            certificateAddress = Me.certificateAddress.Text
            certificateRelationshipClient1 = Me.certificateRelationshipClient1.SelectedItem
            certificateRelationshipClient2 = Me.certificateRelationshipClient2.SelectedItem
            certificateTitle = Me.certificateTitle.SelectedItem
            certificateSurname = Me.certificateSurname.Text
            certificatePostcode = Me.certificatePostcode.Text
        End If

        If Me.CheckBox1.Checked = True Then whichClient = "client1" : whichDocument = "PFA" : sql = "insert into lpa_certificate_provider_details (willClientSequence,whichClient,whichDocument,certificateType,certificateTitle,certificateNames,certificateSurname,certificateAddress,certificatePostcode,certificateRelationshipClient1,certificateRelationshipClient2) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(certificateType) + "','" + SqlSafe(certificateTitle) + "','" + SqlSafe(certificateNames) + "','" + SqlSafe(certificateSurname) + "','" + SqlSafe(certificateAddress) + "','" + SqlSafe(certificatePostcode) + "','" + SqlSafe(certificateRelationshipClient1) + "','" + SqlSafe(certificateRelationshipClient2) + "')" : runSQL(sql)
        If Me.CheckBox2.Checked = True Then whichClient = "client1" : whichDocument = "HW" : sql = "insert into lpa_certificate_provider_details (willClientSequence,whichClient,whichDocument,certificateType,certificateTitle,certificateNames,certificateSurname,certificateAddress,certificatePostcode,certificateRelationshipClient1,certificateRelationshipClient2) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(certificateType) + "','" + SqlSafe(certificateTitle) + "','" + SqlSafe(certificateNames) + "','" + SqlSafe(certificateSurname) + "','" + SqlSafe(certificateAddress) + "','" + SqlSafe(certificatePostcode) + "','" + SqlSafe(certificateRelationshipClient1) + "','" + SqlSafe(certificateRelationshipClient2) + "')" : runSQL(sql)
        If Me.CheckBox4.Checked = True Then whichClient = "client2" : whichDocument = "PFA" : sql = "insert into lpa_certificate_provider_details (willClientSequence,whichClient,whichDocument,certificateType,certificateTitle,certificateNames,certificateSurname,certificateAddress,certificatePostcode,certificateRelationshipClient1,certificateRelationshipClient2) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(certificateType) + "','" + SqlSafe(certificateTitle) + "','" + SqlSafe(certificateNames) + "','" + SqlSafe(certificateSurname) + "','" + SqlSafe(certificateAddress) + "','" + SqlSafe(certificatePostcode) + "','" + SqlSafe(certificateRelationshipClient1) + "','" + SqlSafe(certificateRelationshipClient2) + "')" : runSQL(sql)
        If Me.CheckBox3.Checked = True Then whichClient = "client2" : whichDocument = "HW" : sql = "insert into lpa_certificate_provider_details (willClientSequence,whichClient,whichDocument,certificateType,certificateTitle,certificateNames,certificateSurname,certificateAddress,certificatePostcode,certificateRelationshipClient1,certificateRelationshipClient2) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(certificateType) + "','" + SqlSafe(certificateTitle) + "','" + SqlSafe(certificateNames) + "','" + SqlSafe(certificateSurname) + "','" + SqlSafe(certificateAddress) + "','" + SqlSafe(certificatePostcode) + "','" + SqlSafe(certificateRelationshipClient1) + "','" + SqlSafe(certificateRelationshipClient2) + "')" : runSQL(sql)


        ''''''''''''''
        'preferences & instructions
        ''''''''''''''
        sql = "delete from lpa_preferences where willClientSequence=" + clientID
        runSQL(sql)

        Dim mustDoNotes As String = Me.mustDoNotes.Text
        Dim shouldDoNotes As String = Me.shouldDoNotes.Text

        If Me.CheckBox1.Checked = True Then whichClient = "client1" : whichDocument = "PFA" : sql = "insert into lpa_preferences (willClientSequence,whichClient,whichDocument,mustDoNotes,shouldDoNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(mustDoNotes) + "','" + SqlSafe(shouldDoNotes) + "')" : runSQL(sql)
        If Me.CheckBox2.Checked = True Then whichClient = "client1" : whichDocument = "HW" : sql = "insert into lpa_preferences (willClientSequence,whichClient,whichDocument,mustDoNotes,shouldDoNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(mustDoNotes) + "','" + SqlSafe(shouldDoNotes) + "')" : runSQL(sql)
        If Me.CheckBox4.Checked = True Then whichClient = "client2" : whichDocument = "PFA" : sql = "insert into lpa_preferences (willClientSequence,whichClient,whichDocument,mustDoNotes,shouldDoNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(mustDoNotes) + "','" + SqlSafe(shouldDoNotes) + "')" : runSQL(sql)
        If Me.CheckBox3.Checked = True Then whichClient = "client2" : whichDocument = "HW" : sql = "insert into lpa_preferences (willClientSequence,whichClient,whichDocument,mustDoNotes,shouldDoNotes) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(mustDoNotes) + "','" + SqlSafe(shouldDoNotes) + "')" : runSQL(sql)


        ''''''''''''''
        'registration
        ''''''''''''''
        sql = "delete from lpa_register where willClientSequence=" + clientID
        runSQL(sql)

        Dim corresTitle = ""
        Dim corresNames = ""
        Dim corresSurname = ""
        Dim corresPostcode = ""
        Dim corresAddress = ""

        Dim registerType = ""
        If RadioButton17.Checked = True Then registerType = "donor"
        If RadioButton18.Checked = True Then registerType = "attorney"

        Dim corresType = ""
        If RadioButton15.Checked = True Then corresType = "Donor"
        If RadioButton16.Checked = True Then corresType = "Attorney"
        If RadioButton19.Checked = True Then
            corresType = "Professional"
            corresTitle = Me.corresTitle.SelectedItem
            corresNames = Me.corresNames.Text
            corresSurname = Me.corresSurname.Text
            corresPostcode = Me.corresPostcode.Text
            corresAddress = Me.corresAddress.Text
        End If
        'save 4 times if needed
        If Me.CheckBox1.Checked = True Then whichClient = "client1" : whichDocument = "PFA" : sql = "insert into lpa_register (willClientSequence,whichClient,whichDocument,registerType,corresType,corresTitle,corresNames,corresSurname,corresPostcode,corresAddress) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(registerType) + "','" + SqlSafe(corresType) + "','" + SqlSafe(corresTitle) + "','" + SqlSafe(corresNames) + "','" + SqlSafe(corresSurname) + "','" + SqlSafe(corresPostcode) + "','" + SqlSafe(corresAddress) + "')" : runSQL(sql)
        If Me.CheckBox2.Checked = True Then whichClient = "client1" : whichDocument = "HW" : sql = "insert into lpa_register (willClientSequence,whichClient,whichDocument,registerType,corresType,corresTitle,corresNames,corresSurname,corresPostcode,corresAddress) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(registerType) + "','" + SqlSafe(corresType) + "','" + SqlSafe(corresTitle) + "','" + SqlSafe(corresNames) + "','" + SqlSafe(corresSurname) + "','" + SqlSafe(corresPostcode) + "','" + SqlSafe(corresAddress) + "')" : runSQL(sql)
        If Me.CheckBox4.Checked = True Then whichClient = "client2" : whichDocument = "PFA" : sql = "insert into lpa_register (willClientSequence,whichClient,whichDocument,registerType,corresType,corresTitle,corresNames,corresSurname,corresPostcode,corresAddress) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(registerType) + "','" + SqlSafe(corresType) + "','" + SqlSafe(corresTitle) + "','" + SqlSafe(corresNames) + "','" + SqlSafe(corresSurname) + "','" + SqlSafe(corresPostcode) + "','" + SqlSafe(corresAddress) + "')" : runSQL(sql)
        If Me.CheckBox3.Checked = True Then whichClient = "client2" : whichDocument = "HW" : sql = "insert into lpa_register (willClientSequence,whichClient,whichDocument,registerType,corresType,corresTitle,corresNames,corresSurname,corresPostcode,corresAddress) values (" + clientID + ",'" + SqlSafe(whichClient) + "','" + SqlSafe(whichDocument) + "','" + SqlSafe(registerType) + "','" + SqlSafe(corresType) + "','" + SqlSafe(corresTitle) + "','" + SqlSafe(corresNames) + "','" + SqlSafe(corresSurname) + "','" + SqlSafe(corresPostcode) + "','" + SqlSafe(corresAddress) + "')" : runSQL(sql)


        ''''''''''''''
        'attendance notes
        ''''''''''''''
        sql = "select count(sequence) from lpa_attendance_notes where willClientSequence=" + clientID
        If runSQLwithID(sql) <> "0" And runSQLwithID(sql) <> "ERROR" Then
            'we have a row, so we need to update
            sql = "update lpa_attendance_notes set notes='" + SqlSafe(Me.TextBox20.Text) + "' where willClientSequence=" + clientID
            runSQL(sql)
        End If

        If runSQLwithID(sql) = "0" Then
            'we have no row, so we need to insert
            sql = "insert into lpa_attendance_notes (willClientSequence,notes) values (" + clientID + ",'" + SqlSafe(Me.TextBox20.Text) + "')"
            runSQL(sql)
        End If


        'reset cursor
        Me.Enabled = True
        Me.Cursor = Cursors.Default

        spy("LPA Fact Find screen closed")

        Me.Close()
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            If RadioButton5.Checked = True Or RadioButton6.Checked = False Then
                DataGridView5.Visible = False
                Button6.Visible = False

            End If
            Dim sql As String = "delete from lpa_attorneys_detail where willClientSequence=" + clientID + " and occupation='CHILD'"
            runSQL(sql)
            ' MsgBox(sql)
            redrawAttorneysGrid()
        End If
    End Sub

    Private Sub RadioButton4_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton4.CheckedChanged
        If RadioButton4.Checked = True Then
            DataGridView5.Visible = True
            Button6.Visible = True

            Dim attorneyRelationshipClient1 = ""
            Dim attorneyRelationshipClient2 = ""

            'we need to populate the attorney list with the names of the children from the WILL, if they exist
            'BUT this function can be called when the screen loads for the first time, so we need to make sure that there are no "CHILD" occipations on the table for this client
            'BECASUE if there are then we've already done this and we could end up multipling the number of children

            Dim rowcount As String = runSQLwithID("Select count(sequence) from lpa_attorneys_detail where willClientSequence=" + clientID + " and occupation='CHILD'")
            If Val(rowcount) > 0 Then
                'do nothing as there is data aleady
            Else
                'populate the attorney list as there are no CHILD attorneys specified

                Dim sql As String = "select * from will_instruction_stage4_data_detail where willClientSequence=" + clientID
                Dim dbConn As New OleDb.OleDbConnection
                Dim command As New OleDb.OleDbCommand
                Dim dbreader As OleDb.OleDbDataReader
                dbConn.ConnectionString = connectionString
                dbConn.Open()
                command.Connection = dbConn
                command.CommandText = Sql
                dbreader = command.ExecuteReader
                While dbreader.Read

                    If Trim(dbreader.GetValue(9).ToString) = "" Then
                        'unknown title
                        attorneyRelationshipClient1 = ""
                        attorneyRelationshipClient2 = ""

                    ElseIf Trim(dbreader.GetValue(9).ToString.ToUpper) = "MR" Then
                        'it's male, but is it step or bloodline
                        If dbreader.GetValue(4).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient1 = "Son"
                        If dbreader.GetValue(4).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient1 = "Stepson"

                        If dbreader.GetValue(5).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient2 = "Son"
                        If dbreader.GetValue(5).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient2 = "Stepson"

                    Else
                        'it must be female
                        If dbreader.GetValue(4).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient1 = "Daughter"
                        If dbreader.GetValue(4).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient1 = "Step Daughter"

                        If dbreader.GetValue(5).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient2 = "Daughter"
                        If dbreader.GetValue(5).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient2 = "Step Daughter"
                    End If

                    'insert sql required
                    sql = "insert into lpa_attorneys_detail (willClientSequence,attorneyTitle,attorneyNames,attorneySurname,attorneyAddress,attorneyPostcode,attorneyRelationshipClient1,attorneyRelationshipClient2,occupation) values (" + clientID + ",'" + SqlSafe(dbreader.GetValue(9).ToString) + "','" + SqlSafe(dbreader.GetValue(3).ToString) + "','" + SqlSafe(dbreader.GetValue(2).ToString) + "','" + SqlSafe(dbreader.GetValue(6).ToString) + "','" + SqlSafe(dbreader.GetValue(10).ToString) + "','" + attorneyRelationshipClient1 + "','" + attorneyRelationshipClient2 + "','CHILD')"
                    runSQL(sql)
                End While

                dbreader.Close()
                dbConn.Close()

            End If
            'finally show them on the screen
            redrawAttorneysGrid()
        End If

    End Sub



    Private Sub RadioButton5_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton5.CheckedChanged
        If RadioButton5.Checked = True Then
            DataGridView5.Visible = False
            Button6.Visible = False
        Else
            DataGridView5.Visible = True
            Button6.Visible = True
        End If
    End Sub

    Private Sub RadioButton10_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton10.CheckedChanged
        If RadioButton10.Checked = True Then
            TextBox1.Visible = True
            Label9.Visible = True
        Else
            TextBox1.Visible = False
            Label9.Visible = False
        End If

    End Sub

    Private Sub RadioButton12_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton12.CheckedChanged
        If RadioButton12.Checked = True Then
            DataGridView1.Visible = False
            Button1.Visible = False

            Dim sql As String = "delete from lpa_replacement_attorneys_detail where willClientSequence=" + clientID
            runSQL(sql)

            sql = "delete from lpa_replacement_attorneys where willClientSequence=" + clientID
            runSQL(sql)

            redrawAttorneysGrid()

        Else
            DataGridView1.Visible = True
            Button1.Visible = True
        End If
    End Sub

    Private Sub RadioButton13_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton13.CheckedChanged
        If RadioButton13.Checked = True Then
            Panel7.Visible = False
        Else
            Panel7.Visible = True

        End If
    End Sub

    Private Sub RadioButton19_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton19.CheckedChanged
        If RadioButton19.Checked = True Then
            Panel10.Visible = True
        Else
            Panel10.Visible = False

        End If
    End Sub

    Private Sub DataGridView7_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView7.CellContentClick
        If e.ColumnIndex = 3 Then

            If DataGridView7.Item(3, e.RowIndex).Value = "edit" Then Me.attendanceMode.Text = "edit"
            If DataGridView7.Item(3, e.RowIndex).Value = "answer" Then Me.attendanceMode.Text = "answer"

            rowID = DataGridView7.Item(0, e.RowIndex).Value.ToString

            If rowID = 1 Then lpa_attendance_notesQ1.ShowDialog(Me)
            If rowID = 2 Then lpa_attendance_notesQ2.ShowDialog(Me)
            If rowID = 3 Then lpa_attendance_notesQ3.ShowDialog(Me)
            If rowID = 4 Then lpa_attendance_notesQ4.ShowDialog(Me)
            If rowID = 5 Then lpa_attendance_notesQ5.ShowDialog(Me)
            If rowID = 6 Then lpa_attendance_notesQ6.ShowDialog(Me)
            If rowID = 7 Then lpa_attendance_notesQ7.ShowDialog(Me)
            If rowID = 8 Then lpa_attendance_notesQ8.ShowDialog(Me)
            If rowID = 9 Then lpa_attendance_notesQ9.ShowDialog(Me)
            If rowID = 10 Then lpa_attendance_notesQ10.ShowDialog(Me)


            rowID = ""    'reset global variable
            redrawAttendanceNotesGrid()
        End If

        Me.attendanceMode.Text = "attendanceMode"    'reset

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        corresPostcode.Text = corresPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = corresPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.corresAddress.Text = f.FoundAddress()
            Me.corresPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        certificatePostcode.Text = certificatePostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = corresPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.certificateAddress.Text = f.FoundAddress()
            Me.certificatePostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        If e.ColumnIndex = 4 And DataGridView2.Item(4, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView2.Item(0, e.RowIndex).Value.ToString
            lpa_person_tobetold_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawPersonsGrid()
        End If
    End Sub

    Private Sub DataGridView5_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView5.CellContentClick
        If e.ColumnIndex = 4 And DataGridView5.Item(4, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView5.Item(0, e.RowIndex).Value.ToString
            lpa_attorney_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawAttorneysGrid()
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 4 And DataGridView1.Item(4, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView1.Item(0, e.RowIndex).Value.ToString
            lpa_replacement_attorney_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawReplacementAttorneysGrid()
        End If
    End Sub

    Function redrawPersonsGrid()
        Dim sql = ""
        DataGridView2.Rows.Clear()

        Dim personName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from lpa_persons_to_be_told  where willClientSequence=" + clientID + " order by personNames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            personName = dbreader.GetValue(3).ToString + " " + dbreader.GetValue(12).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(5).ToString
            relationToClient2 = dbreader.GetValue(6).ToString

            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, personName, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawAttorneysGrid()
        Dim sql = ""
        DataGridView5.Rows.Clear()

        Dim attorneyName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from lpa_attorneys_detail  where willClientSequence=" + clientID + " order by attorneyNames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            attorneyName = dbreader.GetValue(3).ToString + " " + dbreader.GetValue(15).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(5).ToString
            relationToClient2 = dbreader.GetValue(6).ToString

            DataGridView5.Rows.Add(dbreader.GetValue(0).ToString, attorneyName, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()
        Return True
    End Function

    Function redrawReplacementAttorneysGrid()
        Dim sql = ""
        DataGridView1.Rows.Clear()

        Dim attorneyName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from lpa_replacement_attorneys_detail  where willClientSequence=" + clientID + " order by attorneyNames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            attorneyName = dbreader.GetValue(3).ToString + " " + dbreader.GetValue(15).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(5).ToString
            relationToClient2 = dbreader.GetValue(6).ToString

            DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, attorneyName, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()
        Return True
    End Function

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        lpa_attorney_add.ShowDialog(Me)
        redrawAttorneysGrid()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        lpa_replacement_attorney_add.ShowDialog(Me)
        redrawReplacementAttorneysGrid()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        lpa_person_tobetold_add.ShowDialog(Me)
        redrawPersonsGrid()
    End Sub


End Class