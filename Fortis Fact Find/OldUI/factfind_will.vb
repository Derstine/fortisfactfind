﻿Public Class factfind_will
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        spy("Will Fact Find screen opened")

        'this make take a while so show wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.firstRunComplete.Text = "N"

        TabControl1.SelectedIndex = 0   'always show client tab information first

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        'reset cursor
        Me.Cursor = Cursors.Default
        Me.firstRunComplete.Text = "Y"

    End Sub

    Private Function setDefaults()

        GroupBox2.Visible = False   'client details
        RadioButton3.Checked = True
        RadioButton5.Checked = True
        Label2.Visible = False
        TextBox1.Visible = False

        DataGridView1.Rows.Clear()      'children details
        DataGridView1.Visible = False
        Button1.Visible = False


        RadioButton13.Checked = True    'executors
        RadioButton9.Checked = True
        Panel4.Visible = False
        RadioButton11.Checked = True

        RadioButton16.Checked = True
        Panel7.Visible = False
        RadioButton17.Checked = True

        RadioButton21.Checked = True
        Panel9.Visible = False
        RadioButton23.Checked = True

        Panel6.Visible = False
        Panel7.Visible = False
        Panel8.Visible = False
        Panel9.Visible = False
        Panel10.Visible = True
        DataGridView5.Visible = False
        Button6.Visible = False

        RadioButton26.Checked = True
        DataGridView5.Rows.Clear()      'children details
        DataGridView5.Visible = False
        Button6.Visible = False

        GroupBox3.Visible = False   'funeral 
        RadioButton28.Checked = True
        RadioButton29.Checked = True
        RadioButton33.Checked = True
        RadioButton36.Checked = True

        RadioButton38.Checked = True 'legacies

        GroupBox5.Visible = False    'potential claims
        GroupBox6.Visible = False
        RadioButton40.Checked = True

        DataGridView7.Rows.Clear()   'attendance notes

        RadioButton43.Checked = True 'Trusts before residue
        TabControl2.Visible = False
        RadioButton44.Checked = True
        RadioButton46.Checked = True
        RadioButton49.Checked = True
        RadioButton50.Checked = True
        RadioButton52.Checked = True
        RadioButton54.Checked = True
        DataGridView4.Rows.Clear()
        DataGridView4.Visible = False
        Button5.Visible = False
        RadioButton60.Checked = True
        RadioButton58.Checked = True
        RadioButton56.Checked = True
        DataGridView6.Rows.Clear()
        DataGridView6.Visible = False
        Button7.Visible = False

        RadioButton62.Checked = True 'Residuary Estate
        TabControl3.Visible = False
        RadioButton70.Checked = True
        RadioButton68.Checked = True
        RadioButton66.Checked = True
        RadioButton73.Checked = True
        DataGridView8.Rows.Clear()
        DataGridView8.Visible = False
        Button9.Visible = False
        RadioButton74.Checked = True
        RadioButton76.Checked = True
        DataGridView10.Rows.Clear()
        DataGridView10.Visible = False
        Button11.Visible = False

        RadioButton78.Checked = True
        RadioButton80.Checked = True
        RadioButton82.Checked = True
        RadioButton85.Checked = True
        DataGridView9.Rows.Clear()
        DataGridView9.Visible = False
        Button10.Visible = False

        Panel31.Visible = True
        Return True
    End Function

    Private Function populateScreen()
        If clientType() = "mirror" Then GroupBox2.Visible = True Else GroupBox2.Visible = False
        If clientType() = "mirror" Then GroupBox3.Visible = True Else GroupBox3.Visible = False

        'client details tab
        Dim result = ""
        Dim results As Array
        Dim sql As String

        'personal details
        sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            TextBox2.Text = results(12) + " " + results(14) + " " + results(13)
            TextBox4.Text = results(16)
            TextBox5.Text = results(67)

            If results(18).ToString <> "" Then
                TextBox3.Text = results(18).ToString.Substring(0, 10)
            Else
                TextBox3.Text = ""
            End If

            TextBox9.Text = results(23) + " " + results(25) + " " + results(24)
            If results(29).ToString <> "" Then
                TextBox8.Text = results(29).ToString.Substring(0, 10)
            Else
                TextBox8.Text = ""
            End If
            TextBox7.Text = results(27)
            TextBox6.Text = results(68)

            'will in another country
            If results(40) = "Y" Then
                RadioButton4.Checked = True
                TextBox1.Text = results(41)
            Else
                RadioButton3.Checked = True
                TextBox1.Text = ""
            End If

            'single?
            If results(17) = "Married" Then
                Panel2.Visible = False
                RadioButton5.Checked = True
            Else
                Panel2.Visible = True
                If results(42) = "Y" Then
                    RadioButton1.Checked = True
                ElseIf results(42) = "N" Then
                    RadioButton2.Checked = True
                Else
                    RadioButton3.Checked = True
                End If

            End If
        End If


        '''''''''''''
        'children tab
        '''''''''''''
        sql = "select noChildrenConfirm from will_instruction_stage4_data where willClientSequence=" + clientID
        '  MsgBox(runSQLwithID(sql))
        If runSQLwithID(sql) = "N" Then RadioButton7.Checked = True : Button1.Visible = True : DataGridView1.Visible = True
        If runSQLwithID(sql) = "Y" Then RadioButton6.Checked = True : Button1.Visible = False : DataGridView1.Visible = False
        redrawChildrenGrid()

        ''''''''''''''
        'executor tab
        ''''''''''''''

        Panel3.Visible = True
        Panel4.Visible = True
        Panel6.Visible = True
        Panel7.Visible = True
        Panel8.Visible = True
        Panel9.Visible = True
        Panel10.Visible = True

        ' pppp
        If getWillLookup("PPPP Required") = "Y" Then RadioButton12.Checked = True Else RadioButton13.Checked = True

        'profesional
        If getWillLookup("Professional Executor") = "Y" Then RadioButton8.Checked = True Else RadioButton9.Checked = True
        If getWillLookup("Professional Executor Type") = "Sole" Then RadioButton14.Checked = True
        If getWillLookup("Professional Executor Type") = "Joint" Then RadioButton10.Checked = True
        If getWillLookup("Professional Executor Type") = "Substitute" Then RadioButton11.Checked = True

        'spouse
        If getWillLookup("Spouse Executor") = "Y" Then RadioButton15.Checked = True Else RadioButton16.Checked = True
        If getWillLookup("Spouse Executor Type") = "Sole" Then RadioButton17.Checked = True
        If getWillLookup("Spouse Executor Type") = "Joint" Then RadioButton18.Checked = True
        If getWillLookup("Spouse Executor Type") = "Substitute" Then RadioButton19.Checked = True

        'kids
        If getWillLookup("Kids Executor") = "Y" Then RadioButton20.Checked = True Else RadioButton21.Checked = True
        If getWillLookup("Kids Executor Type") = "Sole" Then RadioButton22.Checked = True
        If getWillLookup("Kids Executor Type") = "Joint" Then RadioButton23.Checked = True
        If getWillLookup("Kids Executor Type") = "Substitute" Then RadioButton24.Checked = True

        'others
        If getWillLookup("Other Executor") = "Y" Then RadioButton25.Checked = True Else RadioButton26.Checked = True

        'now do some basic panel maintenance

        If RadioButton9.Checked = True Then
            Panel4.Visible = False
        End If

        If RadioButton16.Checked = True Then
            Panel7.Visible = False
        End If

        If RadioButton21.Checked = True Then
            Panel9.Visible = False
        End If



        redrawExecutorGrid()

        ''''''''''''''
        'guardians tab
        ''''''''''''''
        '''
        redrawGuardianGrid()

        ''''''''''''''
        'funeral data tab
        ''''''''''''''
        sql = "select * from will_instruction_stage6_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)

        If results(0) <> "ERROR" Then
            If results(2) = "cremated" Then
                RadioButton29.Checked = True

            ElseIf results(2) = "buried" Then
                RadioButton30.Checked = True
            Else
                RadioButton31.Checked = True
            End If

            If results(4) = "cremated" Then
                RadioButton33.Checked = True

            ElseIf results(4) = "buried" Then
                RadioButton34.Checked = True
            Else
                RadioButton32.Checked = True
            End If

            If results(8) = "Y" Then RadioButton27.Checked = True Else RadioButton28.Checked = True
            If results(9) = "Y" Then RadioButton35.Checked = True Else RadioButton36.Checked = True
            TextBox15.Text = results(3)
            TextBox10.Text = results(5)
        End If


        ''''''''''''''
        'legacies
        ''''''''''''''
        If getWillLookup("Memorandum of Wishes") = "Y" Then RadioButton37.Checked = True Else RadioButton38.Checked = True
        redrawLegacyGrid()


        ''''''''''''''
        'trusts before residue
        ''''''''''''''
        sql = "select * from will_instruction_stage8_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then

            RadioButton43.Checked = True
            If results(2) = "Life Interest" Then RadioButton41.Checked = True
            If results(2) = "Right To Reside" Then RadioButton41.Checked = True : Me.RadioButton45.Checked = True : Me.TextBox12.Text = results(8)
            If results(2) = "Nil Rate Band" Then RadioButton42.Checked = True

            If results(10) = "Spouse" Then RadioButton46.Checked = True
            If results(10) = "Named Individual" Then RadioButton47.Checked = True : Me.TextBox13.Text = results(11)
            If results(16) = "Y" Then RadioButton48.Checked = True
            If results(13) = "Y" Then RadioButton50.Checked = True
            If results(13) = "N" Then RadioButton51.Checked = True
            If results(15) = "Bloodline" Then RadioButton52.Checked = True
            If results(15) = "Named Individual" Then RadioButton53.Checked = True

            If results(2) = "Nil Rate Band" Then
                Me.TextBox14.Text = results(19)
                If getWillLookup("NRBSpousePrimary") = "Y" Then RadioButton60.Checked = True Else RadioButton61.Checked = True
                If getWillLookup("NRBBloodlineSecondary") = "Y" Then RadioButton58.Checked = True Else RadioButton59.Checked = True
                If getWillLookup("NRBBloodlineToIssue") = "Y" Then RadioButton56.Checked = True Else RadioButton57.Checked = True

            End If

            Me.Refresh()
        End If

        redrawNRBGrid()
        redrawPPTGrid()

        ''''''''''''''
        'Residuary Estate
        ''''''''''''''

        RadioButton62.Checked = True
        sql = "select * from will_instruction_stage9_data where residueLevel=1 and willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then

            Dim residueSequence As String = results(0)

            If results(3) = "Absolute Trust" Then
                'it's an absolute trust
                RadioButton63.Checked = True

                'is spouse level 1?
                sql = "select count(sequence) from will_instruction_stage9_data_absolute_trust where residueSequence=" + residueSequence + " and beneficiaryType='Spouse'"
                If runSQLwithID(sql) = "1" Then RadioButton70.Checked = True

                'are kids level 2
                sql = "select * from will_instruction_stage9_data where residueLevel=2 and willClientSequence=" + clientID
                results = runSQLwithArray(sql)
                ' MsgBox(results(0))
                If results(0) <> "ERROR" And results(0) <> "" Then
                    Dim residueSequenceLevel2 As String = results(0)
                    sql = "select count(sequence) from will_instruction_stage9_data_absolute_trust where residueSequence=" + residueSequenceLevel2 + " and beneficiaryType='Bloodline'"
                    If runSQLwithID(sql) = "1" Then
                        RadioButton68.Checked = True
                        sql = "select lapseOrIssue from will_instruction_stage9_data_absolute_trust where residueSequence=" + residueSequenceLevel2
                        If runSQLwithID(sql) = "issue" Then
                            RadioButton66.Checked = True
                        End If
                    Else
                        RadioButton69.Checked = True 'second level exists, but it's no bloodline kids
                    End If
                Else
                    RadioButton69.Checked = True ' no second level, so can't be kids
                End If

                'are there other levels?
                sql = "select count(sequence) from will_instruction_stage9_data_factfind_3rd_level where willClientSequence=" + clientID
                results = runSQLwithArray(sql)
                'MsgBox("other levels=" + results(0))
                If results(0) <> "ERROR" And results(0) <> "0" Then
                    RadioButton72.Checked = True
                Else
                    RadioButton73.Checked = True
                End If


            ElseIf results(3) = "Discretionary Trust" Then
                'it's a discretionary trust
                RadioButton64.Checked = True
                Me.TextBox16.Text = results(7)

                'are the kids the beneficiaries
                sql = "select * from will_instruction_stage9_data_discretionary_trust where residueSequence=" + results(0)
                results = runSQLwithArray(sql)
                If results(0) <> "ERROR" And results(0) <> "" Then
                    Me.RadioButton76.Checked = True
                    If results(6) = "issue" Then RadioButton74.Checked = True Else RadioButton75.Checked = True
                Else
                    Me.RadioButton81.Checked = True
                End If

            ElseIf results(3) = "IPDI" Then
                'it's an IPDI/FLIT
                RadioButton65.Checked = True

                If results(10) = "BOTH" Then RadioButton82.Checked = True Else RadioButton83.Checked = True
                If results(13) = "Discretionary Trust" Then RadioButton84.Checked = True

                'now is it the kids or someone else
                sql = "select * from will_instruction_stage9_data_IPDI_bloodline where residueSequence=" + results(0)
                results = runSQLwithArray(sql)
                If results(0) <> "ERROR" And results(0) <> "" Then
                    Me.RadioButton80.Checked = True  'Then r2rBeneficialShare = "Bloodline Children" Else r2rBeneficialShare = "Named Individual"
                    If results(6) = "issue" Then RadioButton78.Checked = True Else RadioButton79.Checked = True
                Else
                    Me.RadioButton81.Checked = True
                End If
            End If

            ' then

        End If

        redrawResiduaryGrids()

        ''''''''''''''
        'potential claims data
        ''''''''''''''
        If getWillLookup("Deliberate Exclusions") = "Y" Then RadioButton39.Checked = True Else RadioButton40.Checked = True
        If getWillLookup("Deliberate Exclusions Client 1") <> "" Then TextBox17.Text = getWillLookup("Deliberate Exclusions Client 1")
        If getWillLookup("Deliberate Exclusions Client 2") <> "" Then TextBox11.Text = getWillLookup("Deliberate Exclusions Client 2")

        ''''''''''''''
        'attendance notes
        ''''''''''''''
        sql = "select notes from will_instruction_stage10_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            Me.TextBox20.Text = results(0)
        Else
            'no data so therefore insert row
            sql = "insert into will_instruction_stage10_data (willClientSequence) values (" + clientID + ")"
            runSQL(sql)
        End If
        redrawAttendanceNotesGrid()


        Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        '  NEED TO SAVE THE DATA 
        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        Dim sql As String

        'this might take a while, so show the wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.Enabled = False

        '''''''''''''''
        'children first
        '''''''''''''''

        'delete existing record if it exists
        sql = "delete from will_instruction_stage4_data where willClientSequence=" + clientID
        runSQL(sql)

        'now insert new record
        Dim noKidFlag = "Y"
        If RadioButton6.Checked = True Then noKidFlag = "Y"
        If RadioButton7.Checked = True Then noKidFlag = "N"
        ' MsgBox(nokidFlag)
        sql = "insert into will_instruction_stage4_data (willClientSequence,noChildrenConfirm) values (" + clientID + ",'" + noKidFlag + "')"
        runSQL(sql)

        If noKidFlag = "Y" Then
            'now delete any details from the details table (just in case there are any children details added but these are not actually required any more (basic data housekeeping)
            sql = "delete from will_instruction_stage4_data_detail where willClientSequence=" + clientID
            runSQL(sql)
        End If

        ' MsgBox(sql)
        '''''''''''''''
        'executors next
        '''''''''''''''
        ' pppp
        If RadioButton12.Checked = True Then setWillLookup("PPPP Required", "Y")
        If RadioButton13.Checked = True Then setWillLookup("PPPP Required", "N")

        'professional
        If RadioButton8.Checked = True Then setWillLookup("Professional Executor", "Y")
        If RadioButton9.Checked = True Then setWillLookup("Professional Executor", "N")
        If RadioButton14.Checked = True Then setWillLookup("Professional Executor Type", "Sole")
        If RadioButton10.Checked = True Then setWillLookup("Professional Executor Type", "Joint")
        If RadioButton11.Checked = True Then setWillLookup("Professional Executor Type", "Substitute")

        'spouse
        If RadioButton15.Checked = True Then setWillLookup("Spouse Executor", "Y")
        If RadioButton16.Checked = True Then setWillLookup("Spouse Executor", "N")
        If RadioButton17.Checked = True Then setWillLookup("Spouse Executor Type", "Sole")
        If RadioButton18.Checked = True Then setWillLookup("Spouse Executor Type", "Joint")
        If RadioButton19.Checked = True Then setWillLookup("Spouse Executor Type", "Substitute")

        'kids
        If RadioButton20.Checked = True Then setWillLookup("Kids Executor", "Y")
        If RadioButton21.Checked = True Then setWillLookup("Kids Executor", "N")
        If RadioButton22.Checked = True Then setWillLookup("Kids Executor Type", "Sole")
        If RadioButton23.Checked = True Then setWillLookup("Kids Executor Type", "Joint")
        If RadioButton24.Checked = True Then setWillLookup("Kids Executor Type", "Substitute")

        'others
        If RadioButton25.Checked = True Then setWillLookup("Other Executor", "Y")
        If RadioButton26.Checked = True Then setWillLookup("Other Executor", "N")

        '''''''''''''''
        'guardians next
        '''''''''''''''
        'need to put something here
        Dim guardianType = ""
        sql = "select COUNT(sequence) from will_instruction_stage5_data_detail where willClientSequence=" + clientID

        If Val(runSQLwithID(sql)) > 0 Then

            'workout guardian type
            If Val(runSQLwithID(sql)) = 1 Then
                guardianType = "Sole"
            Else
                sql = "select COUNT(sequence) from will_instruction_stage5_data_detail where guardianType='joint' and willClientSequence=" + clientID
                If Val(runSQLwithID(sql)) > 0 Then
                    guardianType = "joint"
                Else
                    guardianType = "substitute"
                End If
            End If

            sql = "delete from will_instruction_stage5_data where willClientSequence=" + clientID
            runSQL(sql)
            sql = "insert into will_instruction_stage5_data (willClientSequence,guardianType) values (" + clientID + ",'" + guardianType + "')"
        End If

        '''''''''''''''
        'funerals next
        '''''''''''''''
        sql = "delete from will_instruction_stage6_data where willClientSequence=" + clientID
        runSQL(sql)
        Dim funeralClient1Type = ""
        Dim funeralClient1Wishes = ""
        Dim funeralClient1plan = ""
        Dim funeralClient2Wishes = ""
        Dim funeralClient2Type = ""
        Dim funeralClient2plan = ""


        If RadioButton29.Checked = True Then funeralClient1Type = "cremated"
        If RadioButton30.Checked = True Then funeralClient1Type = "buried"
        If RadioButton31.Checked = True Then funeralClient1Type = "none"

        If RadioButton33.Checked = True Then funeralClient2Type = "cremated"
        If RadioButton34.Checked = True Then funeralClient2Type = "buried"
        If RadioButton32.Checked = True Then funeralClient2Type = "none"

        If RadioButton27.Checked = True Then funeralClient1plan = "Y"
        If RadioButton28.Checked = True Then funeralClient1plan = "N"

        If RadioButton35.Checked = True Then funeralClient2plan = "Y"
        If RadioButton36.Checked = True Then funeralClient2plan = "N"

        funeralClient1Wishes = TextBox15.Text
        funeralClient2Wishes = TextBox10.Text

        sql = "insert into will_instruction_stage6_data (willClientSequence,funeralClient1Type,funeralClient1Wishes,funeralClient1Plan,funeralClient2Type,funeralClient2Wishes,funeralClient2Plan) values (" + clientID + ",'" + SqlSafe(funeralClient1Type) + "','" + SqlSafe(funeralClient1Wishes) + "','" + SqlSafe(funeralClient1plan) + "','" + SqlSafe(funeralClient2Type) + "','" + SqlSafe(funeralClient2Wishes) + "','" + SqlSafe(funeralClient2plan) + "')"
        runSQL(sql)

        ''''''''''''''
        'legacies
        ''''''''''''''
        If RadioButton37.Checked = True Then
            setWillLookup("Memorandum of Wishes", "Y")
        Else
            setWillLookup("Memorandum of Wishes", "N")
        End If

        ''''''''''''''
        'trusts before residue
        ''''''''''''''

        If Me.RadioButton43.Checked = True Then
            'then there are no trusts before residue, so delete info
            sql = "delete from will_instruction_stage8_data where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_bloodline where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_charity where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_children where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_discretionary_trust where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_FLIT where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_individual where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_nilrate where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage8_data_trust where willClientSequence=" + clientID
            runSQL(sql)

        Else
            'there are trusts before residue
            sql = "delete from will_instruction_stage8_data where willClientSequence=" + clientID
            runSQL(sql)

            Dim trustType = ""
            Dim r2rWhoTo = ""
            Dim r2rNamed = ""
            Dim r2rTime = ""
            Dim r2rCeaseRemarry = ""
            Dim r2rMortgage = ""
            Dim r2rBeneficialShare = ""
            Dim dtWishes = ""

            If Me.RadioButton41.Checked = True And Me.RadioButton44.Checked = True Then
                trustType = "Life Interest"
                If RadioButton46.Checked = True Then r2rWhoTo = "Spouse"
                If RadioButton47.Checked = True Then r2rWhoTo = "Named Individual" : r2rNamed = Me.TextBox13.Text
                If RadioButton48.Checked = True Then r2rCeaseRemarry = "Y"
                If RadioButton50.Checked = True Then r2rMortgage = "Y" Else r2rMortgage = "N"
                If RadioButton52.Checked = True Then r2rBeneficialShare = "Bloodline"
                If RadioButton53.Checked = True Then r2rBeneficialShare = "Named Individual"

            End If

            If Me.RadioButton41.Checked = True And Me.RadioButton45.Checked = True Then
                trustType = "Right To Reside"
                r2rTime = Me.TextBox12.Text
                If RadioButton46.Checked = True Then r2rWhoTo = "Spouse"
                If RadioButton47.Checked = True Then r2rWhoTo = "Named Individual" : r2rNamed = Me.TextBox13.Text
                If RadioButton48.Checked = True Then r2rCeaseRemarry = "Y"
                If RadioButton50.Checked = True Then r2rMortgage = "Y" Else r2rMortgage = "N"
                If RadioButton52.Checked = True Then r2rBeneficialShare = "Bloodline"
                If RadioButton53.Checked = True Then r2rBeneficialShare = "Named Individual"
            End If

            If Me.RadioButton42.Checked = True Then
                trustType = "Nil Rate Band"
                dtWishes = Me.TextBox14.Text

                If RadioButton60.Checked = True Then setWillLookup("NRBSpousePrimary", "Y") Else setWillLookup("NRBSpousePrimary", "N")
                If RadioButton58.Checked = True Then setWillLookup("NRBBloodlineSecondary", "Y") Else setWillLookup("NRBBloodlineSecondary", "N")
                If RadioButton56.Checked = True Then setWillLookup("NRBBloodlineToIssue", "Y") Else setWillLookup("NRBBloodlineToIssue", "N")

            End If



            sql = "insert into will_instruction_stage8_data (willClientSequence,trustType,r2rTime,r2rWhoTo,r2rNamed,r2rCeaseRemarry,r2rMortgage,r2rBeneficialShare,dtWishes) values (" + clientID + ",'" + SqlSafe(trustType) + "','" + SqlSafe(r2rTime) + "','" + SqlSafe(r2rWhoTo) + "','" + SqlSafe(r2rNamed) + "','" + SqlSafe(r2rCeaseRemarry) + "','" + SqlSafe(r2rMortgage) + "','" + SqlSafe(r2rBeneficialShare) + "','" + SqlSafe(dtWishes) + "')"
            runSQL(sql)

            sql = "select top 1 sequence from will_instruction_stage8_data where willClientSequence=" + clientID
            Dim trustSequence = "0"
            trustSequence = runSQLwithID(sql)

            'now we've got the Trust Sequence, we can start populating some of the other data
            'also, a trustSequence of -1 means that we've created beneficiaries but not yet saved the data, so we need to change those records
            If r2rBeneficialShare = "Bloodline" Then
                Dim lapseissue = ""
                If Me.RadioButton54.Checked = True Then lapseissue = "issue"
                If Me.RadioButton55.Checked = True Then lapseissue = "lapse"

                'clean up first
                sql = "delete from will_instruction_stage8_data_bloodline where willClientSequence=" + clientID + " and trustSequence<>-1"
                runSQL(sql)

                sql = "insert into will_instruction_stage8_data_bloodline (willClientSequence, trustSequence,beneficiaryType,lapseOrIssue) values (" + clientID + "," + trustSequence + ",'Bloodline','" + SqlSafe(lapseissue) + "')"
                runSQL(sql)
            End If

            If trustType = "Nil Rate Band" Then
                'clean up first
                sql = "delete from will_instruction_stage8_data_nilrate where willClientSequence=" + clientID + " and trustSequence<>-1"
                runSQL(sql)

                If RadioButton60.Checked = True Then
                    sql = "insert into will_instruction_stage8_data_nilrate (willClientSequence,trustSequence,beneficiaryType,primaryBeneficiaryFlag) values (" + clientID + "," + trustSequence + ",'spouse','Y')"
                    runSQL(sql)
                End If

                If RadioButton58.Checked = True Then
                    Dim lapseissue = ""
                    If Me.RadioButton56.Checked = True Then lapseissue = "issue"
                    If Me.RadioButton57.Checked = True Then lapseissue = "lapse"
                    sql = "insert into will_instruction_stage8_data_nilrate (willClientSequence,trustSequence,beneficiaryType,primaryBeneficiaryFlag,lapseOrIssue) values (" + clientID + "," + trustSequence + ",'bloodline children','N','" + SqlSafe(lapseissue) + "')"
                    runSQL(sql)
                End If
            End If

            'because we've added people before saving, the trustSequence has been set to a default of -1, therefore we need to run an update
            sql = "update will_instruction_stage8_data_bloodline set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"
            runSQL(sql)
            sql = "update will_instruction_stage8_data_charity set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"
            runSQL(sql)
            sql = "update will_instruction_stage8_data_children set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"
            runSQL(sql)
            sql = "update will_instruction_stage8_data_discretionary_trust set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"
            runSQL(sql)
            sql = "update will_instruction_stage8_data_FLIT set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"
            runSQL(sql)
            sql = "update will_instruction_stage8_data_individual set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"
            runSQL(sql)
            sql = "update will_instruction_stage8_data_nilrate set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"
            runSQL(sql)
            sql = "update will_instruction_stage8_data_trust set trustSequence=" + trustSequence + " where willClientSequence=" + clientID + " and trustSequence=-1"

        End If


        ''''''''''''''
        'residuary estate
        ''''''''''''''

        If Me.RadioButton62.Checked = True Then
            'then there is no residuary estate
            sql = "delete from will_instruction_stage9_data where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage9_data_absolute_trust where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage9_data_discretionary_trust where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage9_data_group_definition where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage9_data_IPDI_bloodline where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage9_data_IPDI_children where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage9_data_IPDI_individual where willClientSequence=" + clientID
            runSQL(sql)
            sql = "delete from will_instruction_stage9_data_IPDI_trust where willClientSequence=" + clientID
            runSQL(sql)
        Else
            'there is a residuary estate ... God help me!!! :-) :-)
            Dim residueSequence = ""
            Dim residueLevel = 0

            If RadioButton63.Checked = True Then
                'it's an absolute trust
                'delete old data
                sql = "delete from will_instruction_stage9_data where willClientSequence=" + clientID
                runSQL(sql)

                If RadioButton70.Checked = True Then
                    residueLevel = residueLevel + 1

                    'spouse is first level
                    sql = "insert into will_instruction_stage9_data (willClientSequence,residueLevel,residueType) values (" + clientID + "," + residueLevel.ToString + ",'Absolute Trust')"
                    runSQL(sql)

                    'get residueSequence
                    sql = "select TOP 1 sequence from will_instruction_stage9_data where willClientSequence=" + clientID
                    residueSequence = runSQLwithID(sql)

                    'cleanup
                    sql = "delete from will_instruction_stage9_data_absolute_trust where willClientSequence=" + clientID + " and beneficiaryType='Spouse'"
                    runSQL(sql)

                    'now insert the row into the absolute trust table
                    sql = "insert into will_instruction_stage9_data_absolute_trust (willClientSequence,residueSequence,beneficiaryType) values (" + clientID + "," + residueSequence + ",'Spouse')"
                    runSQL(sql)
                Else
                    'spouse is not level 1, so delete them
                    sql = "delete from will_instruction_stage9_data_absolute_trust where willClientSequence=" + clientID + " and beneficiaryType='Spouse'"
                    runSQL(sql)
                End If


                If RadioButton68.Checked = True Then
                    residueLevel = residueLevel + 1
                    'kids are second level
                    sql = "insert into will_instruction_stage9_data (willClientSequence,residueLevel,residueType) values (" + clientID + "," + residueLevel.ToString + ",'Absolute Trust')"
                    runSQL(sql)

                    'get residueSequence
                    sql = "select TOP 1 sequence from will_instruction_stage9_data where willClientSequence=" + clientID
                    residueSequence = runSQLwithID(sql)

                    'cleanup
                    sql = "delete from will_instruction_stage9_data_absolute_trust where willClientSequence=" + clientID + " and beneficiaryType='Bloodline'"
                    runSQL(sql)

                    'is it lapse or issue?
                    If RadioButton66.Checked = True Then
                        sql = "insert into will_instruction_stage9_data_absolute_trust (willClientSequence,residueSequence,beneficiaryType,lapseOrIssue) values (" + clientID + "," + residueSequence + ",'Bloodline','issue')"
                        runSQL(sql)
                    End If

                    If RadioButton67.Checked = True Then
                        sql = "insert into will_instruction_stage9_data_absolute_trust (willClientSequence,residueSequence,beneficiaryType,lapseOrIssue) values (" + clientID + "," + residueSequence + ",'Bloodline','lapse')"
                        runSQL(sql)
                    End If

                    If RadioButton72.Checked Then
                        'another level of residue
                        residueLevel = residueLevel + 1


                    End If
                Else
                    'kids are not second level, but old data may have been there, so we need to delete it
                    sql = "delete from will_instruction_stage9_data_absolute_trust where willClientSequence=" + clientID + " and beneficiaryType='Bloodline'"
                    runSQL(sql)
                End If

            End If


            If RadioButton64.Checked = True Then
                'it's a discretionary trust
                residueLevel = residueLevel + 1

                'delete old data
                sql = "delete from will_instruction_stage9_data where willClientSequence=" + clientID
                runSQL(sql)

                Dim dtwishes As String = TextBox16.Text
                sql = "insert into will_instruction_stage9_data (willClientSequence,residueLevel,residueType,dtEstateType,dtWishes) values (" + clientID + "," + residueLevel.ToString + ",'Discretionary Trust','Residuary Estate','" + SqlSafe(dtwishes) + "')"
                runSQL(sql)

                'get residueSequence
                sql = "select TOP 1 sequence from will_instruction_stage9_data where willClientSequence=" + clientID
                residueSequence = runSQLwithID(sql)

                'see if bloodline kids need inserting
                If Me.RadioButton76.Checked = True Then
                    Dim lapseOrIssue = ""
                    If Me.RadioButton74.Checked = True Then lapseOrIssue = "issue"
                    If Me.RadioButton75.Checked = True Then lapseOrIssue = "lapse"

                    'however delete any records that may be there
                    sql = "delete from will_instruction_stage9_data_discretionary_trust where willClientSequence=" + clientID + " and beneficiaryType='bloodline children'"
                    runSQL(sql)
                    sql = "insert into will_instruction_stage9_data_discretionary_trust (willClientSequence,residueSequence,beneficiaryType,lapseOrIssue) values (" + clientID + "," + SqlSafe(residueSequence) + ",'bloodline children','" + SqlSafe(lapseOrIssue) + "')"
                    runSQL(sql)
                End If

                'now need to see if there are any other beneficiaries listed here

            End If

            If RadioButton65.Checked = True Then
                'it's a FLIT ... but I'm storing it as an IPDI becasue FLIT shuold have a trust before residue & so I think Tim has got it wrong
                residueLevel = residueLevel + 1

                'delete old data
                sql = "delete from will_instruction_stage9_data where willClientSequence=" + clientID
                runSQL(sql)

                Dim r2rBenefits = ""
                Dim r2rBeneficialShare = ""
                Dim dtwishes = ""

                If Me.RadioButton82.Checked = True Then r2rBenefits = "BOTH" Else r2rBenefits = "CAPITAL"
                If Me.RadioButton80.Checked = True Then r2rBeneficialShare = "Bloodline Children" Else r2rBeneficialShare = "Named Individual"
                If Me.RadioButton84.Checked = True Then r2rBeneficialShare = "Discretionary Trust"
                dtwishes = Me.TextBox19.Text
                sql = "insert into will_instruction_stage9_data (willClientSequence, residueType,residueLevel,dtEstateType,r2rWhoTo,r2rBenefits,r2rBeneficialShare,dtWishes,r2Occupy) values (" + clientID + ",'IPDI'," + residueLevel.ToString + ",'Residuary Estate','Spouse/Partner','" + SqlSafe(r2rBenefits) + "','" + SqlSafe(r2rBeneficialShare) + "','" + SqlSafe(dtwishes) + "','Y')"
                runSQL(sql)

                'get residueSequence
                sql = "select TOP 1 sequence from will_instruction_stage9_data where willClientSequence=" + clientID
                residueSequence = runSQLwithID(sql)

                'see if bloodline kids need inserting
                If Me.RadioButton80.Checked = True Then
                    Dim lapseOrIssue = ""
                    If Me.RadioButton78.Checked = True Then lapseOrIssue = "issue"
                    If Me.RadioButton79.Checked = True Then lapseOrIssue = "lapse"

                    sql = "insert into will_instruction_stage9_data_IPDI_bloodline (willClientSequence,residueSequence,lapseOrIssue) values (" + clientID + "," + SqlSafe(residueSequence) + ",'" + SqlSafe(lapseOrIssue) + "')"
                    runSQL(sql)

                End If
            End If
        End If



        ''''''''''''''
        'potential claims data
        ''''''''''''''
        If RadioButton39.Checked = True Then
            setWillLookup("Deliberate Exclusions", "Y")
            setWillLookup("Deliberate Exclusions Client 1", TextBox17.Text)
            setWillLookup("Deliberate Exclusions Client 2", TextBox11.Text)
        Else
            setWillLookup("Deliberate Exclusions", "N")
            setWillLookup("Deliberate Exclusions Client 1", "")
            setWillLookup("Deliberate Exclusions Client 2", "")
        End If

        ''''''''''''''
        'attendance notes
        ''''''''''''''
        sql = "select count(sequence) from will_instruction_stage10_data where willClientSequence=" + clientID
        If runSQLwithID(sql) <> "0" And runSQLwithID(sql) <> "ERROR" Then
            'we have a row, so we need to update
            sql = "update will_instruction_stage10_data set notes='" + SqlSafe(Me.TextBox20.Text) + "' where willClientSequence=" + clientID
            runSQL(sql)
        End If

        If runSQLwithID(sql) = "0" Then
            'we have no row, so we need to insert
            sql = "insert into will_instruction_stage10_data (willClientSequence,notes) values (" + clientID + ",'" + SqlSafe(Me.TextBox20.Text) + "')"
            runSQL(sql)
        End If

        'reset cursor
        Me.Enabled = True
        Me.Cursor = Cursors.Default

        spy("Will Fact Find screen closed")

        Me.Close()
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            Label2.Visible = False
            TextBox1.Visible = False
        Else
            Label2.Visible = True
            TextBox1.Visible = True
        End If

    End Sub

    Private Sub RadioButton4_CheckedChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub RadioButton6_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton6.CheckedChanged
        If RadioButton6.Checked = True Then
            DataGridView1.Visible = False
            Button1.Visible = False

            RadioButton17.Checked = False
            RadioButton18.Checked = False
            RadioButton19.Checked = False

        Else
            DataGridView1.Visible = True
            Button1.Visible = True
        End If
    End Sub


    Private Sub RadioButton40_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton40.CheckedChanged
        If RadioButton40.Checked = True Then
            GroupBox5.Visible = False
            GroupBox6.Visible = False
        Else
            GroupBox5.Visible = True
            GroupBox6.Visible = True
        End If
    End Sub

    Private Sub RadioButton43_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton43.CheckedChanged
        If RadioButton43.Checked = True Then
            TabControl2.Visible = False
        End If
    End Sub

    Private Sub RadioButton41_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton41.CheckedChanged
        If RadioButton41.Checked = True Then
            If Me.firstRunComplete.Text = "Y" Then
                TabControl2.Visible = True
                TabControl2.TabPages.Remove(TabPage11)
                TabControl2.TabPages.Remove(TabPage12)
                TabControl2.TabPages.Insert(0, TabPage11)
            Else
                TabControl2.Visible = True
                '  TabControl2.TabPages.Remove(TabPage11)
                TabControl2.TabPages.Remove(TabPage12)
                '  TabControl2.TabPages.Insert(0, TabPage11)
            End If


        End If
    End Sub

    Private Sub RadioButton42_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton42.CheckedChanged
        If RadioButton42.Checked = True Then
            If Me.firstRunComplete.Text = "Y" Then
                TabControl2.Visible = True
                TabControl2.TabPages.Remove(TabPage11)
                TabControl2.TabPages.Remove(TabPage12)
                TabControl2.TabPages.Insert(0, TabPage12)
            Else
                TabControl2.Visible = True
                TabControl2.TabPages.Remove(TabPage11)
                '  TabControl2.TabPages.Remove(TabPage12)
                '  TabControl2.TabPages.Insert(0, TabPage12)
            End If

        End If
    End Sub

    Private Sub RadioButton44_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton44.CheckedChanged
        If RadioButton44.Checked = True Then
            TextBox12.Visible = False
            Label42.Visible = False
        Else
            TextBox12.Visible = True
            Label42.Visible = True
        End If
    End Sub

    Private Sub RadioButton46_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton46.CheckedChanged
        If RadioButton46.Checked = True Then
            TextBox13.Visible = False
            Label43.Visible = False
        Else
            TextBox13.Visible = True
            Label43.Visible = True
        End If
    End Sub

    Private Sub RadioButton52_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton52.CheckedChanged
        If RadioButton52.Checked = True Then
            Panel23.Visible = True
        Else
            Panel23.Visible = False
        End If
    End Sub

    Private Sub RadioButton53_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton53.CheckedChanged
        If RadioButton53.Checked = True Then
            DataGridView4.Visible = True
            Button5.Visible = True
        Else
            DataGridView4.Visible = False
            Button5.Visible = False
        End If
    End Sub

    Private Sub RadioButton58_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton58.CheckedChanged
        If RadioButton58.Checked = True Then
            Panel24.Visible = True
        Else
            Panel24.Visible = False
        End If
    End Sub

    Private Sub RadioButton62_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton62.CheckedChanged
        If RadioButton62.Checked = True Then
            TabControl3.Visible = False
        Else
            TabControl3.Visible = True
        End If

    End Sub

    Private Sub RadioButton63_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton63.CheckedChanged
        If RadioButton63.Checked = True Then
            If Me.firstRunComplete.Text = "Y" Then
                TabControl3.Visible = True
                TabControl3.TabPages.Remove(TabPage13)
                TabControl3.TabPages.Remove(TabPage14)
                TabControl3.TabPages.Remove(TabPage15)
                TabControl3.TabPages.Insert(0, TabPage13)
            Else

                TabControl3.Visible = True
                ' TabControl3.TabPages.Remove(TabPage13)
                TabControl3.TabPages.Remove(TabPage14)
                TabControl3.TabPages.Remove(TabPage15)
                ' TabControl3.TabPages.Insert(0, TabPage13)
            End If


        End If
    End Sub

    Private Sub RadioButton65_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton65.CheckedChanged
        If RadioButton65.Checked = True Then
            If Me.firstRunComplete.Text = "Y" Then
                TabControl3.Visible = True
                TabControl3.TabPages.Remove(TabPage13)
                TabControl3.TabPages.Remove(TabPage14)
                TabControl3.TabPages.Remove(TabPage15)
                TabControl3.TabPages.Insert(0, TabPage14)
            Else
                TabControl3.Visible = True
                TabControl3.TabPages.Remove(TabPage13)
                ' TabControl3.TabPages.Remove(TabPage14)
                TabControl3.TabPages.Remove(TabPage15)
                ' TabControl3.TabPages.Insert(0, TabPage14)
            End If

        End If
    End Sub

    Private Sub RadioButton64_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton64.CheckedChanged
        If RadioButton64.Checked = True Then
            If Me.firstRunComplete.Text = "Y" Then
                TabControl3.Visible = True
                TabControl3.TabPages.Remove(TabPage13)
                TabControl3.TabPages.Remove(TabPage14)
                TabControl3.TabPages.Remove(TabPage15)
                TabControl3.TabPages.Insert(0, TabPage15)
            Else
                TabControl3.Visible = True
                TabControl3.TabPages.Remove(TabPage13)
                TabControl3.TabPages.Remove(TabPage14)
                ' TabControl3.TabPages.Remove(TabPage15)
                'TabControl3.TabPages.Insert(0, TabPage15)
            End If
        End If

    End Sub

    Private Sub RadioButton73_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton73.CheckedChanged
        If RadioButton73.Checked = True Then
            DataGridView8.Visible = False
            Button9.Visible = False
        Else
            DataGridView8.Visible = True
            redrawResiduaryGrids()
            Button9.Visible = True
        End If
    End Sub

    Private Sub RadioButton68_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton68.CheckedChanged
        If RadioButton68.Checked = True Then
            Panel28.Visible = True
        Else
            Panel28.Visible = False
        End If
    End Sub

    Private Sub RadioButton76_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton76.CheckedChanged
        If RadioButton76.Checked = True Then
            DataGridView10.Visible = False
            Button11.Visible = False
            Panel32.Visible = True
        Else
            DataGridView10.Visible = True
            Button11.Visible = True
            Panel32.Visible = False
        End If
    End Sub

    Private Sub RadioButton80_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton80.CheckedChanged
        If RadioButton80.Checked = True Then
            Panel34.Visible = True
            DataGridView9.Visible = False
            Button10.Visible = False
        Else
            Panel34.Visible = False
            DataGridView9.Visible = True
            Button10.Visible = True
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        children_add.ShowDialog(Me)
        redrawChildrenGrid()
    End Sub

    Function redrawChildrenGrid()
        Dim sql = ""
        DataGridView1.Rows.Clear()

        Dim childName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""
        Dim childDOB As String


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage4_data_detail  where willClientSequence=" + clientID + " order by childDOB ASC" 'oldest child first
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            childName = dbreader.GetValue(3).ToString + " " + dbreader.GetValue(2).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(4).ToString
            relationToClient2 = dbreader.GetValue(5).ToString
            childDOB = dbreader.GetValue(11).ToString.Substring(0, 10)
            DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, childName, childDOB, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawExecutorGrid()
        Dim sql = ""
        DataGridView5.Rows.Clear()

        Dim executorName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""
        Dim executorType As String


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage3_data_detail  where willClientSequence=" + clientID + " order by executorNames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            executorName = dbreader.GetValue(3).ToString + " " + dbreader.GetValue(12).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(5).ToString
            relationToClient2 = dbreader.GetValue(6).ToString
            executorType = dbreader.GetValue(2)
            DataGridView5.Rows.Add(dbreader.GetValue(0).ToString, executorName, executorType, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function


    Function redrawGuardianGrid()
        Dim sql = ""
        DataGridView2.Rows.Clear()

        Dim guardianName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""
        Dim guardianType As String


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage5_data_detail  where willClientSequence=" + clientID + " order by guardianNames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            guardianName = dbreader.GetValue(4).ToString + " " + dbreader.GetValue(3).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(12).ToString
            relationToClient2 = dbreader.GetValue(13).ToString
            guardianType = dbreader.GetValue(5)
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, guardianName, guardianType, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True

    End Function

    Function redrawLegacyGrid()
        Dim sql = ""
        DataGridView3.Rows.Clear()
        DataGridView3.DefaultCellStyle.WrapMode = DataGridViewTriState.True
        DataGridView3.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells

        Dim beneficiaryName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""
        Dim item As String
        Dim giftFrom As String

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        Dim dbConn2 As New OleDb.OleDbConnection
        Dim command2 As New OleDb.OleDbCommand
        Dim dbreader2 As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage7_data  where willClientSequence=" + clientID + " order by sequence ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn

        dbConn2.ConnectionString = connectionString
        dbConn2.Open()
        command2.Connection = dbConn


        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            item = dbreader.GetValue(5).ToString
            giftFrom = dbreader.GetValue(2).ToString
            If giftFrom = "client1" Then giftFrom = getClientFirstName("client1")
            If giftFrom = "client2" Then giftFrom = getClientFirstName("client2")
            If giftFrom = "both" Then giftFrom = "Both"

            relationToClient1 = ""
            relationToClient2 = ""
            beneficiaryName = ""

            'inner loop to get multiple beneficiaries of same item
            sql = "select * from will_instruction_stage7_data_detail where giftSequence=" + dbreader.GetValue(0).ToString
            command2.CommandText = sql
            dbreader2 = command2.ExecuteReader
            While dbreader2.Read()
                If dbreader2.GetValue(4).ToString <> "" Then
                    beneficiaryName = beneficiaryName + dbreader2.GetValue(4).ToString + vbCrLf
                ElseIf dbreader2.GetValue(7).ToString <> "" Then
                    beneficiaryName = beneficiaryName + dbreader2.GetValue(7).ToString + vbCrLf
                ElseIf dbreader2.GetValue(16).ToString <> "" Then
                    beneficiaryName = beneficiaryName + dbreader2.GetValue(15).ToString + " " + dbreader2.GetValue(16).ToString + vbCrLf
                Else
                    beneficiaryName = beneficiaryName + "unknown" + vbCrLf
                End If

                relationToClient1 = relationToClient1 + dbreader2.GetValue(12).ToString + vbCrLf
                relationToClient2 = relationToClient2 + dbreader2.GetValue(13).ToString + vbCrLf
            End While

            beneficiaryName = beneficiaryName.Trim().Substring(0, beneficiaryName.Length - 2)
            relationToClient1 = relationToClient1.Trim().Substring(0, relationToClient1.Length - 2)
            relationToClient2 = relationToClient2.Trim().Substring(0, relationToClient2.Length - 2)
            DataGridView3.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryName, giftFrom, item, relationToClient1, relationToClient2, "edit")

            dbreader2.Close()

        End While
        dbreader.Close()
        dbConn.Close()
        dbConn2.Close()


        Return True
    End Function

    Function redrawPPTGrid()
        Dim sql = ""
        DataGridView4.Rows.Clear()

        Dim beneficiaryName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""
        Dim lapseIssue = ""
        Dim percentage = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage8_data_individual  where willClientSequence=" + clientID + " order by beneficiaryForenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            beneficiaryName = dbreader.GetValue(14).ToString + " " + dbreader.GetValue(15).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(11).ToString
            relationToClient2 = dbreader.GetValue(12).ToString
            lapseIssue = dbreader.GetValue(7).ToString
            percentage = dbreader.GetValue(8).ToString

            If percentage = "" Or percentage = "0" Then percentage = "="
            DataGridView4.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryName, lapseIssue, percentage, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawNRBGrid()
        Dim sql = ""
        DataGridView6.Rows.Clear()

        Dim beneficiaryName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""
        Dim lapseIssue = ""
        Dim percentage = ""
        Dim primary = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage8_data_nilrate  where willClientSequence=" + clientID + " order by beneficiaryForenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            beneficiaryName = dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(12).ToString
            relationToClient2 = dbreader.GetValue(13).ToString
            lapseIssue = dbreader.GetValue(5).ToString
            percentage = dbreader.GetValue(8).ToString
            primary = dbreader.GetValue(9).ToString

            If percentage = "" Or percentage = "0" Then percentage = "="
            DataGridView6.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryName, lapseIssue, primary, percentage, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function


    Function redrawAttendanceNotesGrid()
        Dim sql = ""
        Dim answer(2) As String
        DataGridView7.Rows.Clear()

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage10_data  where willClientSequence=" + clientID
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        If dbreader.Read() Then

            For i = 1 To 18
                answer = getAnswer(i)
                DataGridView7.Rows.Add(i, "Q" + i.ToString, answer(0), answer(1))
            Next

        End If
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function getAnswer(ByVal i As Integer) As Array

        Dim returnvalue(2) As String

        Dim sql As String = "select q" + i.ToString + "tick from will_instruction_stage10_data where willClientSequence=" + clientID
        Dim result As String = runSQLwithID(sql)

        If result = "" Then
            returnvalue(0) = "not answered"
            returnvalue(1) = "answer"
        Else
            If i <> 3 Then

                If Val(result) > 0 Then
                    returnvalue(0) = "Yes"
                Else
                    returnvalue(0) = "No"
                End If
            Else
                If Val(result) > 0 Then
                    returnvalue(0) = "Can't Sign"
                ElseIf Val(result) = -2 Then
                    returnvalue(0) = "Can't Read"
                Else
                    returnvalue(0) = "No"
                End If
            End If
            returnvalue(1) = "edit"
        End If
        Return returnvalue
    End Function

    Private Sub RadioButton7_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton7.CheckedChanged

        If RadioButton7.Checked = True Then
            DataGridView1.Visible = True
            Button1.Visible = True

            RadioButton17.Checked = True
            RadioButton18.Checked = True
            RadioButton19.Checked = True

        Else
            DataGridView1.Visible = False
            Button1.Visible = False
        End If

        'need to set the no childrenCofnirm to "N", which means that there are children, but not sure if there is a row data yet
        Dim sql = ""

        sql = "select noChildrenConfirm from will_instruction_stage4_data where willClientSequence=" + clientID
        If runSQLwithID(sql) = "" Or runSQLwithID(sql) = "ERROR" Then
            'no row exitss, therefore insert
            If RadioButton7.Checked = True Then
                sql = "insert into will_instruction_stage4_data (willClientSequence,noChildrenConfirm) values ('" + clientID + "','N')"
            Else
                sql = "insert into will_instruction_stage4_data (willClientSequence,noChildrenConfirm) values ('" + clientID + "','Y')"
            End If
        Else
            'row exists, so it's an update
            If RadioButton7.Checked = True Then
                sql = "update  will_instruction_stage4_data set noChildrenConfirm='N' where willClientSequence=" + clientID
            Else
                sql = "update  will_instruction_stage4_data set noChildrenConfirm='Y' where willClientSequence=" + clientID
            End If
        End If

        runSQL(sql)
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 5 And DataGridView1.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView1.Item(0, e.RowIndex).Value.ToString
            children_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawChildrenGrid()
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        executor_add.ShowDialog(Me)
        redrawExecutorGrid()
    End Sub

    Private Sub DataGridView5_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView5.CellContentClick
        If e.ColumnIndex = 5 And DataGridView5.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView5.Item(0, e.RowIndex).Value.ToString
            executor_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawExecutorGrid()
        End If
    End Sub



    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        guardian_add.ShowDialog(Me)
        redrawGuardianGrid()

    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        If e.ColumnIndex = 5 And DataGridView2.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView2.Item(0, e.RowIndex).Value.ToString
            guardian_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawGuardianGrid()
        End If
    End Sub

    Private Sub RadioButton38_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton38.CheckedChanged
        ' If RadioButton38.Checked = True Then
        ' DataGridView3.Visible = False
        ' Button4.Visible = False
        'Else
        'DataGridView3.Visible = True
        'Button4.Visible = True
        'End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        gifts_add.ShowDialog(Me)
        redrawLegacyGrid()

    End Sub

    Private Sub DataGridView3_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView3.CellContentClick
        If e.ColumnIndex = 6 And DataGridView3.Item(6, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView3.Item(0, e.RowIndex).Value.ToString
            gifts_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawLegacyGrid()
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        ppt_beneficiary_add.ShowDialog()
        redrawPPTGrid()
    End Sub

    Private Sub DataGridView4_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView4.CellContentClick
        If e.ColumnIndex = 6 And DataGridView4.Item(6, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView4.Item(0, e.RowIndex).Value.ToString
            ppt_beneficiary_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawPPTGrid()
        End If
    End Sub



    Private Sub RadioButton59_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton59.CheckedChanged
        If RadioButton59.Checked = True Or RadioButton61.Checked = True Then
            DataGridView6.Visible = True
            Button7.Visible = True
        Else
            DataGridView6.Visible = False
            Button7.Visible = False
        End If
    End Sub

    Private Sub RadioButton61_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton61.CheckedChanged
        If RadioButton59.Checked = True Or RadioButton61.Checked = True Then
            DataGridView6.Visible = True
            Button7.Visible = True
        Else
            DataGridView6.Visible = False
            Button7.Visible = False
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        nrb_beneficiary_add.ShowDialog()
        redrawNRBGrid()
    End Sub

    Private Sub DataGridView6_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView6.CellContentClick
        If e.ColumnIndex = 7 And DataGridView6.Item(7, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView6.Item(0, e.RowIndex).Value.ToString
            nrb_beneficiary_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawNRBGrid()
        End If
    End Sub


    Private Sub DataGridView7_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView7.CellContentClick
        If e.ColumnIndex = 3 Then

            If DataGridView7.Item(3, e.RowIndex).Value = "edit" Then Me.attendanceMode.Text = "edit"
            If DataGridView7.Item(3, e.RowIndex).Value = "answer" Then Me.attendanceMode.Text = "answer"

            rowID = DataGridView7.Item(0, e.RowIndex).Value.ToString

            If rowID = 1 Then will_attendance_notesQ1.ShowDialog(Me)
            If rowID = 2 Then will_attendance_notesQ2.ShowDialog(Me)
            If rowID = 3 Then will_attendance_notesQ3.ShowDialog(Me)
            If rowID = 4 Then will_attendance_notesQ4.ShowDialog(Me)
            If rowID = 5 Then will_attendance_notesQ5.ShowDialog(Me)
            If rowID = 6 Then will_attendance_notesQ6.ShowDialog(Me)
            If rowID = 7 Then will_attendance_notesQ7.ShowDialog(Me)
            If rowID = 8 Then will_attendance_notesQ8.ShowDialog(Me)
            If rowID = 9 Then will_attendance_notesQ9.ShowDialog(Me)
            If rowID = 10 Then will_attendance_notesQ10.ShowDialog(Me)
            If rowID = 11 Then will_attendance_notesQ11.ShowDialog(Me)
            If rowID = 12 Then will_attendance_notesQ12.ShowDialog(Me)
            If rowID = 13 Then will_attendance_notesQ13.ShowDialog(Me)
            If rowID = 14 Then will_attendance_notesQ14.ShowDialog(Me)
            If rowID = 15 Then will_attendance_notesQ15.ShowDialog(Me)
            If rowID = 16 Then will_attendance_notesQ16.ShowDialog(Me)
            If rowID = 17 Then will_attendance_notesQ17.ShowDialog(Me)
            If rowID = 18 Then will_attendance_notesQ18.ShowDialog(Me)

            rowID = ""    'reset global variable
            redrawAttendanceNotesGrid()
        End If

        Me.attendanceMode.Text = "attendanceMode"    'reset

    End Sub

    '''''''''''''''''''''
    '''''''''''''''''''''
    ' executor radio buttons
    '''''''''''''''''''''
    '''''''''''''''''''''

    Private Sub RadioButton8_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton8.CheckedChanged
        If RadioButton8.Checked = True Then
            Panel4.Visible = True
        End If
    End Sub

    Private Sub RadioButton9_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton9.CheckedChanged
        If RadioButton9.Checked = True Then
            Panel4.Visible = False
            Panel6.Visible = True

            RadioButton10.Checked = False
            RadioButton11.Checked = False
            RadioButton14.Checked = False

        End If
    End Sub

    Private Sub RadioButton16_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton16.CheckedChanged
        If RadioButton16.Checked = True Then
            Panel7.Visible = False
            Panel8.Visible = True

            RadioButton17.Checked = False
            RadioButton18.Checked = False
            RadioButton19.Checked = False
        End If
    End Sub

    Private Sub RadioButton15_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton15.CheckedChanged
        Panel7.Visible = True
    End Sub

    Private Sub RadioButton21_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton21.CheckedChanged
        If RadioButton21.Checked = True Then
            Panel9.Visible = False
            Panel10.Visible = True

            RadioButton22.Checked = False
            RadioButton23.Checked = False
            RadioButton24.Checked = False
        End If
    End Sub

    Private Sub RadioButton20_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton20.CheckedChanged
        If RadioButton20.Checked = True Then
            Panel9.Visible = True
        End If
    End Sub



    Private Sub RadioButton14_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton14.CheckedChanged
        If RadioButton14.Checked = True Then
            RadioButton8.Checked = True
            Panel6.Visible = False
            Panel7.Visible = False
            Panel8.Visible = False
            Panel9.Visible = False
            Panel10.Visible = False
            Button6.Visible = False
        End If

        If RadioButton14.Checked = False Then
            Panel6.Visible = True
            Panel7.Visible = True
            Panel8.Visible = True
            Panel9.Visible = True
            Panel10.Visible = True
            Button6.Visible = True
        End If

    End Sub

    Private Sub RadioButton11_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton11.CheckedChanged
        If RadioButton11.Checked = True Then
            Panel6.Visible = True
            If RadioButton15.Checked = True Then Panel7.Visible = True
        End If
    End Sub

    Private Sub RadioButton10_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton10.CheckedChanged
        If RadioButton10.Checked = True Then
            Panel6.Visible = True
            If RadioButton15.Checked = True Then Panel7.Visible = True
        End If
    End Sub


    Private Sub RadioButton17_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton17.CheckedChanged
        If RadioButton17.Checked = True Then

            RadioButton15.Checked = True

            Panel3.Visible = True

            RadioButton9.Checked = True
            RadioButton10.Checked = False
            RadioButton11.Checked = False
            RadioButton14.Checked = False

            Panel4.Visible = False
            Panel8.Visible = False
            Panel9.Visible = False
            Panel10.Visible = False

        End If


    End Sub

    Private Sub RadioButton18_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton18.CheckedChanged
        If RadioButton18.Checked = True Then
            Panel3.Visible = True
            If RadioButton8.Checked = True Then Panel4.Visible = False
            Panel8.Visible = True
        End If
    End Sub

    Private Sub RadioButton19_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton19.CheckedChanged
        If RadioButton19.Checked = True Then
            Panel3.Visible = True
            If RadioButton8.Checked = True Then Panel4.Visible = False
            Panel8.Visible = True
        End If
    End Sub

    Private Sub RadioButton22_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton22.CheckedChanged
        If RadioButton22.Checked = True Then

            RadioButton20.Checked = True

            Panel3.Visible = True

            RadioButton9.Checked = True
            RadioButton14.Checked = False
            RadioButton10.Checked = False
            RadioButton11.Checked = False
            Panel4.Visible = False

            Panel6.Visible = True
            Panel7.Visible = False
            RadioButton16.Checked = True
            RadioButton17.Checked = False
            RadioButton18.Checked = False
            RadioButton19.Checked = False


            Panel10.Visible = False
        End If
    End Sub

    Private Sub RadioButton23_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton23.CheckedChanged
        If RadioButton23.Checked = True Then
            Panel3.Visible = True
            If RadioButton8.Checked = True Then Panel4.Visible = False
            Panel6.Visible = True
            If RadioButton15.Checked = True Then Panel7.Visible = False
            Panel3.Visible = True
            Panel10.Visible = True
        End If

    End Sub

    Private Sub RadioButton26_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton26.CheckedChanged
        If RadioButton26.Checked = True Then
            DataGridView5.Visible = False
            Button6.Visible = False
        End If
    End Sub

    Private Sub RadioButton25_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton25.CheckedChanged
        If RadioButton25.Checked = True Then
            DataGridView5.Visible = True
            Button6.Visible = True
        End If
    End Sub

    Private Sub RadioButton24_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton24.CheckedChanged
        If RadioButton24.Checked = True Then
            Panel3.Visible = True
            If RadioButton8.Checked = True Then Panel4.Visible = False
            Panel6.Visible = True
            If RadioButton15.Checked = True Then Panel7.Visible = False
            Panel3.Visible = True
            Panel10.Visible = True
        End If
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Me.residuaryType.Text = "direct distribution"
        residuary_beneficiary_add.ShowDialog(Me)
        redrawResiduaryGrids

    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Me.residuaryType.Text = "flit"
        residuary_beneficiary_add.ShowDialog(Me)
        redrawResiduaryGrids()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Me.residuaryType.Text = "discretionary trust"
        residuary_beneficiary_add.ShowDialog(Me)
        redrawResiduaryGrids()
    End Sub

    Private Sub DataGridView8_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView8.CellContentClick
        If e.ColumnIndex = 6 And DataGridView8.Item(6, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView8.Item(0, e.RowIndex).Value.ToString
            Me.residuaryType.Text = "direct distribution"
            residuary_beneficiary_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            Me.residuaryType.Text = "residuaryType"
            redrawResiduaryGrids()
        End If
    End Sub

    Private Sub DataGridView9_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView9.CellContentClick
        If e.ColumnIndex = 6 And DataGridView9.Item(6, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView9.Item(0, e.RowIndex).Value.ToString
            Me.residuaryType.Text = "flit"
            residuary_beneficiary_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            Me.residuaryType.Text = "residuaryType"
            redrawResiduaryGrids()
        End If
    End Sub

    Private Sub DataGridView10_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView10.CellContentClick
        If e.ColumnIndex = 6 And DataGridView10.Item(6, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView10.Item(0, e.RowIndex).Value.ToString
            Me.residuaryType.Text = "discretionary trust"
            residuary_beneficiary_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            Me.residuaryType.Text = "residuaryType"
            redrawResiduaryGrids()
        End If
    End Sub

    Function redrawResiduaryGrids()


        'direct distribution
        Dim sql = ""
        DataGridView8.Rows.Clear()

        Dim beneficiaryName = ""
        Dim relationToClient1 = ""
        Dim relationToClient2 = ""
        Dim lapseIssue = ""
        Dim percentage = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage9_data_factfind_3rd_level  where willClientSequence=" + clientID + " and residuaryType = 'direct distribution' order by beneficiaryForenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            beneficiaryName = ""
            '  beneficiaryName = dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(12).ToString
            relationToClient2 = dbreader.GetValue(13).ToString
            lapseIssue = dbreader.GetValue(18).ToString
            percentage = dbreader.GetValue(6).ToString


            If dbreader.GetValue(4).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(4).ToString + vbCrLf
            ElseIf dbreader.GetValue(7).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(7).ToString + vbCrLf
            ElseIf dbreader.GetValue(16).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString + vbCrLf
            Else
                beneficiaryName = beneficiaryName + "unknown" + vbCrLf
            End If

            If percentage = "" Or percentage = "0" Then percentage = "="
            DataGridView8.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryName, lapseIssue, percentage, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()

        'flit
        DataGridView9.Rows.Clear()
        sql = "select * from will_instruction_stage9_data_factfind_3rd_level  where willClientSequence=" + clientID + " and residuaryType = 'flit' order by beneficiaryForenames ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            beneficiaryName = dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(12).ToString
            relationToClient2 = dbreader.GetValue(13).ToString
            lapseIssue = dbreader.GetValue(18).ToString
            percentage = dbreader.GetValue(8).ToString


            If dbreader.GetValue(4).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(4).ToString + vbCrLf
            ElseIf dbreader.GetValue(7).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(7).ToString + vbCrLf
            ElseIf dbreader.GetValue(16).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString + vbCrLf
            Else
                beneficiaryName = beneficiaryName + "unknown" + vbCrLf
            End If

            If percentage = "" Or percentage = "0" Then percentage = "="
            DataGridView9.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryName, lapseIssue, percentage, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()


        'discretionary trust
        DataGridView10.Rows.Clear()
        sql = "select * from will_instruction_stage9_data_factfind_3rd_level  where willClientSequence=" + clientID + " and residuaryType = 'discretionary trust' order by beneficiaryForenames ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            relationToClient1 = ""
            relationToClient2 = ""
            beneficiaryName = dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString.ToUpper
            relationToClient1 = dbreader.GetValue(12).ToString
            relationToClient2 = dbreader.GetValue(13).ToString
            lapseIssue = dbreader.GetValue(18).ToString
            percentage = dbreader.GetValue(8).ToString


            If dbreader.GetValue(4).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(4).ToString + vbCrLf
            ElseIf dbreader.GetValue(7).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(7).ToString + vbCrLf
            ElseIf dbreader.GetValue(16).ToString <> "" Then
                beneficiaryName = beneficiaryName + dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString + vbCrLf
            Else
                beneficiaryName = beneficiaryName + "unknown" + vbCrLf
            End If

            If percentage = "" Or percentage = "0" Then percentage = "="
            DataGridView10.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryName, lapseIssue, percentage, relationToClient1, relationToClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True

    End Function

    Private Sub RadioButton72_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton72.CheckedChanged
        If RadioButton72.Checked = True Then
            DataGridView8.Visible = True
            redrawResiduaryGrids()
            Button9.Visible = True
        Else


            DataGridView8.Visible = False
            Button9.Visible = False
        End If
    End Sub
End Class