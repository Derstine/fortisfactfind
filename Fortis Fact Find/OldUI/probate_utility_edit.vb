﻿Public Class probate_utility_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.utilityType.SelectedIndex = 0

        'load data
        Dim sql As String = "select * from probate_utilities where sequence=" + rowID
        Dim results As Array
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            Me.utilityType.SelectedItem = results(3)
            Me.provider.Text = results(4)
            Me.providerAddress.Text = results(5)
            Me.providerPostcode.Text = results(6)
            Me.providerPhone.Text = results(7)
            Me.providerEmail.Text = results(8)
            Me.accountNumber.Text = results(9)
            Me.meterReading.Text = results(10)
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If utilityType.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the utility category" + vbCrLf
        If provider.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the provider's name" + vbCrLf
        If accountNumber.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the account number/reference" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update probate_utilities set utilityType='" + SqlSafe(utilityType.SelectedItem) + "',provider='" + SqlSafe(provider.Text) + "',providerAddress='" + SqlSafe(providerAddress.Text) + "',providerPostcode='" + SqlSafe(providerPostcode.Text) + "',providerPhone='" + SqlSafe(providerPhone.Text) + "',providerEmail='" + SqlSafe(providerEmail.Text) + "',accountNumber='" + SqlSafe(accountNumber.Text) + "',meterReading='" + SqlSafe(meterReading.Text) + "' where sequence=" + rowID
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        providerPostcode.Text = providerPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = providerPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.providerAddress.Text = f.FoundAddress()
            Me.providerPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this utility?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from probate_utilities where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class