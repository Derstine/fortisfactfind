﻿Public Class gifts_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)
        Me.whichDeath.SelectedItem = 0

        'we don't know recipients, but we need a place marker in the database, so might as well create an enty upon loading and deleting if it's cancelled
        Dim sql As String = "select * from will_instruction_stage7_data where sequence=" + rowID
        Dim results As Array

        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            Me.description.Text = results(5)
            ' Me.whichDeath.SelectedItem = results(2)
            If results(2) = "client1" Then Me.whichDeath.SelectedIndex = 1
            If results(2) = "client2" Then Me.whichDeath.SelectedIndex = 2
            If results(2) = "both" Then Me.whichDeath.SelectedIndex = 3

        End If

        Me.giftSequence.Text = rowID
        Me.recipientCount.Text = 0
        redrawGrid()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim sql = ""
        Dim errortext = ""
        Dim errorcount = 0
        If whichDeath.SelectedIndex = 0 Then

        End If

        If description.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the actual gift" + vbCrLf
        If whichDeath.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify when the gift is to be given" + vbCrLf
        If recipientCount.Text = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify at least one recipient" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            Dim giftfrom = ""
            If whichDeath.SelectedIndex = 1 Then giftfrom = "client1"
            If whichDeath.SelectedIndex = 2 Then giftfrom = "client2"
            If whichDeath.SelectedIndex = 3 Then giftfrom = "both"

            sql = "update will_instruction_stage7_data set itemDetails='" + SqlSafe(Me.description.Text) + "', giftFrom='" + SqlSafe(giftfrom) + "' where sequence=" + Me.giftSequence.Text
            runSQL(sql)

            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        gifts_recipient_add.ShowDialog(Me)
        redrawGrid()
    End Sub

    Function redrawGrid()
        DataGridView1.Rows.Clear()
        Dim sql = ""

        Dim counter = 0
        Dim beneficiaryName = ""
        Dim percentage = ""
        Dim age = ""
        Dim lapseissue = ""
        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from will_instruction_stage7_data_detail  where giftSequence=" + Me.giftSequence.Text + " order by sequence ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn

        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            counter = counter + 1
            percentage = dbreader.GetValue(6).ToString
            age = dbreader.GetValue(9).ToString
            lapseissue = dbreader.GetValue(18).ToString

            If dbreader.GetValue(3).ToString = "A Group" Then
                beneficiaryName = dbreader.GetValue(4).ToString
            ElseIf dbreader.GetValue(3).ToString = "Charity" Then
                beneficiaryName = dbreader.GetValue(7).ToString
            ElseIf dbreader.GetValue(3).ToString = "Named Individual" Then
                beneficiaryName = dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString.ToUpper
            Else
                beneficiaryName = "unknown"
            End If

            DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryName, percentage, age, lapseissue, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Me.recipientCount.Text = counter.ToString
        Return True
    End Function

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 5 And DataGridView1.Item(5, e.RowIndex).Value = "edit" Then
            Dim oldRowID = rowID 'need to "borrow" this global variable
            'its edit
            rowID = DataGridView1.Item(0, e.RowIndex).Value.ToString
            gifts_recipient_edit.ShowDialog(Me)
            rowID = oldRowID    'reset global variable
            oldRowID = ""

            redrawGrid()
        End If
    End Sub


    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this executor?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes

                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from will_instruction_stage7_data where sequence=" + rowID
                runSQL(sql)

                'delete underlying data
                sql = "delete from will_instruction_stage7_data_detail where giftSequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub

    Private Sub whichDeath_SelectedIndexChanged(sender As Object, e As EventArgs) Handles whichDeath.SelectedIndexChanged

    End Sub
End Class