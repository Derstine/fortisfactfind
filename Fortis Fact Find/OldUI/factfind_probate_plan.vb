﻿Public Class factfind_probate_plan
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        spy("Probate Plan Fact Find screen opened")

        'this make take a while so show wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.firstRunComplete.Text = "N"

        TabControl1.SelectedIndex = 0   'always show client tab information first

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        'reset cursor
        Me.Cursor = Cursors.Default
        Me.firstRunComplete.Text = "Y"

    End Sub

    Private Function setDefaults()

        GroupBox2.Visible = False   'client details

        Return True
    End Function

    Private Function populateScreen()
        If clientType() = "mirror" Then GroupBox2.Visible = True Else GroupBox2.Visible = False

        'client details tab
        Dim result = ""
        Dim results As Array
        Dim sql As String


        sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            TextBox2.Text = results(12) + " " + results(14) + " " + results(13)
            TextBox4.Text = results(16)
            TextBox5.Text = results(67)
            If results(18).ToString <> "" Then
                TextBox3.Text = results(18).ToString.Substring(0, 10)
            Else
                TextBox3.Text = ""
            End If

            TextBox9.Text = results(23) + " " + results(25) + " " + results(24)
            If results(29).ToString <> "" Then
                TextBox8.Text = results(29).ToString.Substring(0, 10)
            Else
                TextBox8.Text = ""
            End If
            TextBox7.Text = results(27)
            TextBox6.Text = results(68)
        End If

        'plan details
        sql = "select * from probate_plan where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            If results(2) = "Y" Then Me.RadioButton1.Checked = True
            If results(2) = "N" Then Me.RadioButton2.Checked = True
            TextBox20.Text = results(3)

        End If

        Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        '  NEED TO SAVE THE DATA 
        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        Dim sql As String

        'this might take a while, so show the wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.Enabled = False

        Dim probatePlanFlag = ""
        If RadioButton1.Checked = True Then probatePlanFlag = "Y"
        If RadioButton2.Checked = True Then probatePlanFlag = "N"

        'delete existing record if it exists
        sql = "delete from probate_plan where willClientSequence=" + clientID
        runSQL(sql)

        'now insert new record
        sql = "insert into probate_plan (willClientSequence,probatePlanFlag,attendanceNotes) values (" + clientID + ",'" + SqlSafe(probatePlanFlag) + "','" + SqlSafe(TextBox20.Text) + "')"
        runSQL(sql)


        'reset cursor
        Me.Enabled = True
        Me.Cursor = Cursors.Default

        spy("Probate Plan Fact Find screen closed")

        Me.Close()
    End Sub


End Class