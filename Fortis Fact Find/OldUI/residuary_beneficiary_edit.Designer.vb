﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class residuary_beneficiary_edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.beneficiaryTitle = New System.Windows.Forms.ComboBox()
        Me.beneficiaryForenames = New System.Windows.Forms.TextBox()
        Me.beneficiarySurname = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.beneficiaryAddress = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.beneficiaryPostcode = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.recipientType = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.relationToClient2 = New System.Windows.Forms.ComboBox()
        Me.relationToClient1 = New System.Windows.Forms.ComboBox()
        Me.atWhatAgeIssue = New System.Windows.Forms.ComboBox()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.giftOverBeneficiaries = New System.Windows.Forms.TextBox()
        Me.giftOverBeneficiariesLabel = New System.Windows.Forms.Label()
        Me.atWhatAgeIssueLabel = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.giftSharedPercentage = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.charityRegNumber = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.charityName = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.groupAtWhatAgeIssue = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.groupGiftOverBeneficiaries = New System.Windows.Forms.TextBox()
        Me.groupGiftOverBeneficiariesLabel = New System.Windows.Forms.Label()
        Me.groupAtWhatAgeIssueLabel = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.beneficiaryName = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(800, 608)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(91, 36)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Update"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(897, 608)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 36)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(260, 20)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Edit A Residuary Estate Beneficiary"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(131, 22)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(34, 18)
        Me.Label50.TabIndex = 45
        Me.Label50.Text = "Title"
        '
        'beneficiaryTitle
        '
        Me.beneficiaryTitle.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryTitle.FormattingEnabled = True
        Me.beneficiaryTitle.Items.AddRange(New Object() {"Select...", "Mr", "Mrs", "Miss", "Ms"})
        Me.beneficiaryTitle.Location = New System.Drawing.Point(180, 19)
        Me.beneficiaryTitle.Name = "beneficiaryTitle"
        Me.beneficiaryTitle.Size = New System.Drawing.Size(97, 26)
        Me.beneficiaryTitle.TabIndex = 0
        '
        'beneficiaryForenames
        '
        Me.beneficiaryForenames.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryForenames.Location = New System.Drawing.Point(180, 61)
        Me.beneficiaryForenames.Name = "beneficiaryForenames"
        Me.beneficiaryForenames.Size = New System.Drawing.Size(289, 23)
        Me.beneficiaryForenames.TabIndex = 1
        '
        'beneficiarySurname
        '
        Me.beneficiarySurname.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiarySurname.Location = New System.Drawing.Point(180, 100)
        Me.beneficiarySurname.Name = "beneficiarySurname"
        Me.beneficiarySurname.Size = New System.Drawing.Size(218, 23)
        Me.beneficiarySurname.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(88, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 18)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "Forenames"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(103, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 18)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Surname"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(273, 141)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Lookup"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'beneficiaryAddress
        '
        Me.beneficiaryAddress.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryAddress.Location = New System.Drawing.Point(180, 187)
        Me.beneficiaryAddress.Multiline = True
        Me.beneficiaryAddress.Name = "beneficiaryAddress"
        Me.beneficiaryAddress.Size = New System.Drawing.Size(268, 88)
        Me.beneficiaryAddress.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(106, 190)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 18)
        Me.Label8.TabIndex = 62
        Me.Label8.Text = "Address"
        '
        'beneficiaryPostcode
        '
        Me.beneficiaryPostcode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryPostcode.Location = New System.Drawing.Point(180, 142)
        Me.beneficiaryPostcode.Name = "beneficiaryPostcode"
        Me.beneficiaryPostcode.Size = New System.Drawing.Size(87, 23)
        Me.beneficiaryPostcode.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(99, 148)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 18)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Postcode"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 295)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 18)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "Relationship to Client 1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(22, 329)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(144, 18)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Relationship to Client 2"
        '
        'recipientType
        '
        Me.recipientType.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.recipientType.FormattingEnabled = True
        Me.recipientType.Items.AddRange(New Object() {"Select...", "Individual", "Charity", "Group"})
        Me.recipientType.Location = New System.Drawing.Point(138, 64)
        Me.recipientType.Name = "recipientType"
        Me.recipientType.Size = New System.Drawing.Size(279, 26)
        Me.recipientType.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 18)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Recipient Type"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.relationToClient2)
        Me.GroupBox1.Controls.Add(Me.relationToClient1)
        Me.GroupBox1.Controls.Add(Me.atWhatAgeIssue)
        Me.GroupBox1.Controls.Add(Me.Label50)
        Me.GroupBox1.Controls.Add(Me.ComboBox4)
        Me.GroupBox1.Controls.Add(Me.beneficiaryTitle)
        Me.GroupBox1.Controls.Add(Me.giftOverBeneficiaries)
        Me.GroupBox1.Controls.Add(Me.giftOverBeneficiariesLabel)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.atWhatAgeIssueLabel)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.beneficiaryForenames)
        Me.GroupBox1.Controls.Add(Me.RadioButton5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.RadioButton6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.RadioButton7)
        Me.GroupBox1.Controls.Add(Me.RadioButton8)
        Me.GroupBox1.Controls.Add(Me.beneficiarySurname)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.beneficiaryAddress)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.beneficiaryPostcode)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(32, 104)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(634, 536)
        Me.GroupBox1.TabIndex = 71
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Named Individual"
        '
        'relationToClient2
        '
        Me.relationToClient2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.relationToClient2.FormattingEnabled = True
        Me.relationToClient2.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.relationToClient2.Location = New System.Drawing.Point(181, 326)
        Me.relationToClient2.Name = "relationToClient2"
        Me.relationToClient2.Size = New System.Drawing.Size(204, 26)
        Me.relationToClient2.TabIndex = 91
        '
        'relationToClient1
        '
        Me.relationToClient1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.relationToClient1.FormattingEnabled = True
        Me.relationToClient1.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.relationToClient1.Location = New System.Drawing.Point(181, 292)
        Me.relationToClient1.Name = "relationToClient1"
        Me.relationToClient1.Size = New System.Drawing.Size(204, 26)
        Me.relationToClient1.TabIndex = 90
        '
        'atWhatAgeIssue
        '
        Me.atWhatAgeIssue.FormattingEnabled = True
        Me.atWhatAgeIssue.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.atWhatAgeIssue.Location = New System.Drawing.Point(385, 398)
        Me.atWhatAgeIssue.Name = "atWhatAgeIssue"
        Me.atWhatAgeIssue.Size = New System.Drawing.Size(63, 21)
        Me.atWhatAgeIssue.TabIndex = 89
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.ComboBox4.Location = New System.Drawing.Point(180, 397)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(63, 21)
        Me.ComboBox4.TabIndex = 12
        '
        'giftOverBeneficiaries
        '
        Me.giftOverBeneficiaries.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.giftOverBeneficiaries.Location = New System.Drawing.Point(180, 458)
        Me.giftOverBeneficiaries.Multiline = True
        Me.giftOverBeneficiaries.Name = "giftOverBeneficiaries"
        Me.giftOverBeneficiaries.Size = New System.Drawing.Size(431, 70)
        Me.giftOverBeneficiaries.TabIndex = 14
        '
        'giftOverBeneficiariesLabel
        '
        Me.giftOverBeneficiariesLabel.AutoSize = True
        Me.giftOverBeneficiariesLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.giftOverBeneficiariesLabel.Location = New System.Drawing.Point(30, 458)
        Me.giftOverBeneficiariesLabel.Name = "giftOverBeneficiariesLabel"
        Me.giftOverBeneficiariesLabel.Size = New System.Drawing.Size(137, 18)
        Me.giftOverBeneficiariesLabel.TabIndex = 81
        Me.giftOverBeneficiariesLabel.Text = "Giftover Beneficiaries"
        '
        'atWhatAgeIssueLabel
        '
        Me.atWhatAgeIssueLabel.AutoSize = True
        Me.atWhatAgeIssueLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.atWhatAgeIssueLabel.Location = New System.Drawing.Point(260, 398)
        Me.atWhatAgeIssueLabel.Name = "atWhatAgeIssueLabel"
        Me.atWhatAgeIssueLabel.Size = New System.Drawing.Size(117, 18)
        Me.atWhatAgeIssueLabel.TabIndex = 88
        Me.atWhatAgeIssueLabel.Text = "At what age issue?"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(81, 398)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(83, 18)
        Me.Label19.TabIndex = 87
        Me.Label19.Text = "At what age?"
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton5.Location = New System.Drawing.Point(470, 366)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(104, 22)
        Me.RadioButton5.TabIndex = 11
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "Giftover Only"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton6.Location = New System.Drawing.Point(320, 366)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(142, 22)
        Me.RadioButton6.TabIndex = 10
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "Issue Then Giftover"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.Location = New System.Drawing.Point(250, 365)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(56, 22)
        Me.RadioButton7.TabIndex = 9
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "Issue"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.Location = New System.Drawing.Point(180, 365)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(60, 22)
        Me.RadioButton8.TabIndex = 8
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Lapse"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(62, 366)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(97, 18)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Lapse or Issue?"
        '
        'giftSharedPercentage
        '
        Me.giftSharedPercentage.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.giftSharedPercentage.Location = New System.Drawing.Point(559, 65)
        Me.giftSharedPercentage.Name = "giftSharedPercentage"
        Me.giftSharedPercentage.Size = New System.Drawing.Size(45, 23)
        Me.giftSharedPercentage.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(436, 68)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(109, 18)
        Me.Label9.TabIndex = 72
        Me.Label9.Text = "Percentage Share"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.charityRegNumber)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.charityName)
        Me.GroupBox2.Location = New System.Drawing.Point(680, 41)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(489, 157)
        Me.GroupBox2.TabIndex = 73
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Charity"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(6, 85)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 18)
        Me.Label11.TabIndex = 72
        Me.Label11.Text = "Charity Number"
        '
        'charityRegNumber
        '
        Me.charityRegNumber.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.charityRegNumber.Location = New System.Drawing.Point(112, 82)
        Me.charityRegNumber.Name = "charityRegNumber"
        Me.charityRegNumber.Size = New System.Drawing.Size(166, 23)
        Me.charityRegNumber.TabIndex = 2
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(408, 42)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 1
        Me.Button4.Text = "Lookup"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 45)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 18)
        Me.Label10.TabIndex = 70
        Me.Label10.Text = "Charity Name"
        '
        'charityName
        '
        Me.charityName.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.charityName.Location = New System.Drawing.Point(112, 42)
        Me.charityName.Name = "charityName"
        Me.charityName.Size = New System.Drawing.Size(290, 23)
        Me.charityName.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.groupAtWhatAgeIssue)
        Me.GroupBox3.Controls.Add(Me.ComboBox1)
        Me.GroupBox3.Controls.Add(Me.groupGiftOverBeneficiaries)
        Me.GroupBox3.Controls.Add(Me.groupGiftOverBeneficiariesLabel)
        Me.GroupBox3.Controls.Add(Me.groupAtWhatAgeIssueLabel)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.RadioButton4)
        Me.GroupBox3.Controls.Add(Me.RadioButton3)
        Me.GroupBox3.Controls.Add(Me.RadioButton2)
        Me.GroupBox3.Controls.Add(Me.RadioButton1)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.beneficiaryName)
        Me.GroupBox3.Location = New System.Drawing.Point(672, 204)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(634, 270)
        Me.GroupBox3.TabIndex = 74
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Group"
        '
        'groupAtWhatAgeIssue
        '
        Me.groupAtWhatAgeIssue.FormattingEnabled = True
        Me.groupAtWhatAgeIssue.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.groupAtWhatAgeIssue.Location = New System.Drawing.Point(372, 106)
        Me.groupAtWhatAgeIssue.Name = "groupAtWhatAgeIssue"
        Me.groupAtWhatAgeIssue.Size = New System.Drawing.Size(63, 21)
        Me.groupAtWhatAgeIssue.TabIndex = 91
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.ComboBox1.Location = New System.Drawing.Point(171, 106)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(63, 21)
        Me.ComboBox1.TabIndex = 90
        '
        'groupGiftOverBeneficiaries
        '
        Me.groupGiftOverBeneficiaries.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupGiftOverBeneficiaries.Location = New System.Drawing.Point(171, 152)
        Me.groupGiftOverBeneficiaries.Multiline = True
        Me.groupGiftOverBeneficiaries.Name = "groupGiftOverBeneficiaries"
        Me.groupGiftOverBeneficiaries.Size = New System.Drawing.Size(440, 87)
        Me.groupGiftOverBeneficiaries.TabIndex = 7
        '
        'groupGiftOverBeneficiariesLabel
        '
        Me.groupGiftOverBeneficiariesLabel.AutoSize = True
        Me.groupGiftOverBeneficiariesLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupGiftOverBeneficiariesLabel.Location = New System.Drawing.Point(2, 152)
        Me.groupGiftOverBeneficiariesLabel.Name = "groupGiftOverBeneficiariesLabel"
        Me.groupGiftOverBeneficiariesLabel.Size = New System.Drawing.Size(137, 18)
        Me.groupGiftOverBeneficiariesLabel.TabIndex = 70
        Me.groupGiftOverBeneficiariesLabel.Text = "Giftover Beneficiaries"
        '
        'groupAtWhatAgeIssueLabel
        '
        Me.groupAtWhatAgeIssueLabel.AutoSize = True
        Me.groupAtWhatAgeIssueLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupAtWhatAgeIssueLabel.Location = New System.Drawing.Point(247, 107)
        Me.groupAtWhatAgeIssueLabel.Name = "groupAtWhatAgeIssueLabel"
        Me.groupAtWhatAgeIssueLabel.Size = New System.Drawing.Size(117, 18)
        Me.groupAtWhatAgeIssueLabel.TabIndex = 77
        Me.groupAtWhatAgeIssueLabel.Text = "At what age issue?"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(53, 107)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(83, 18)
        Me.Label14.TabIndex = 76
        Me.Label14.Text = "At what age?"
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.Location = New System.Drawing.Point(470, 70)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(104, 22)
        Me.RadioButton4.TabIndex = 4
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Giftover Only"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(320, 70)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(142, 22)
        Me.RadioButton3.TabIndex = 3
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Issue Then Giftover"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(250, 70)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(56, 22)
        Me.RadioButton2.TabIndex = 2
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Issue"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(171, 70)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(60, 22)
        Me.RadioButton1.TabIndex = 1
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Lapse"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(34, 72)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(97, 18)
        Me.Label12.TabIndex = 71
        Me.Label12.Text = "Lapse or Issue?"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(52, 37)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 18)
        Me.Label13.TabIndex = 70
        Me.Label13.Text = "Group Name"
        '
        'beneficiaryName
        '
        Me.beneficiaryName.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryName.Location = New System.Drawing.Point(171, 34)
        Me.beneficiaryName.Name = "beneficiaryName"
        Me.beneficiaryName.Size = New System.Drawing.Size(326, 23)
        Me.beneficiaryName.TabIndex = 0
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button5.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(680, 608)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(91, 36)
        Me.Button5.TabIndex = 75
        Me.Button5.Text = "Delete"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'residuary_beneficiary_edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1345, 656)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.giftSharedPercentage)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.recipientType)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Name = "residuary_beneficiary_edit"
        Me.Text = "Edit A Residuary Estate Beneficiary"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button3 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents beneficiaryTitle As ComboBox
    Friend WithEvents beneficiaryForenames As TextBox
    Friend WithEvents beneficiarySurname As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents beneficiaryAddress As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents beneficiaryPostcode As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents recipientType As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents giftSharedPercentage As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label11 As Label
    Friend WithEvents charityRegNumber As TextBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents charityName As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents beneficiaryName As TextBox
    Friend WithEvents ComboBox4 As ComboBox
    Friend WithEvents giftOverBeneficiaries As TextBox
    Friend WithEvents giftOverBeneficiariesLabel As Label
    Friend WithEvents atWhatAgeIssueLabel As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents RadioButton6 As RadioButton
    Friend WithEvents RadioButton7 As RadioButton
    Friend WithEvents RadioButton8 As RadioButton
    Friend WithEvents Label20 As Label
    Friend WithEvents groupGiftOverBeneficiaries As TextBox
    Friend WithEvents groupGiftOverBeneficiariesLabel As Label
    Friend WithEvents groupAtWhatAgeIssueLabel As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents atWhatAgeIssue As ComboBox
    Friend WithEvents groupAtWhatAgeIssue As ComboBox
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Button5 As Button
    Friend WithEvents relationToClient1 As ComboBox
    Friend WithEvents relationToClient2 As ComboBox
End Class
