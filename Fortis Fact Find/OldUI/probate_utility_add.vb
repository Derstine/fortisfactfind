﻿Public Class probate_utility_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.Visible = True Else Me.Button2.Visible = False

        Me.utilityType.SelectedIndex = 0

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If utilityType.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the utility category" + vbCrLf
        If provider.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the provider's name" + vbCrLf
        If accountNumber.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the account number/reference" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into probate_utilities (willClientSequence,whichClient,utilityType,provider,providerAddress,providerPostcode,providerPhone,providerEmail,accountNumber,meterReading) values ('" + clientID + "','" + whoDied + "','" + SqlSafe(utilityType.SelectedItem) + "','" + SqlSafe(provider.Text) + "','" + SqlSafe(providerAddress.Text) + "','" + SqlSafe(providerPostcode.Text) + "','" + SqlSafe(providerPhone.Text) + "','" + SqlSafe(providerEmail.Text) + "','" + SqlSafe(accountNumber.Text) + "','" + SqlSafe(meterReading.Text) + "')"
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        providerPostcode.Text = providerPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = providerPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.providerAddress.Text = f.FoundAddress()
            Me.providerPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub
End Class