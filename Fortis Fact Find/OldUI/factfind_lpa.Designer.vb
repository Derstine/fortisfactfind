﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class factfind_lpa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.DataGridView5 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn4 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn1 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.RadioButton11 = New System.Windows.Forms.RadioButton()
        Me.RadioButton12 = New System.Windows.Forms.RadioButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.certificateAddress = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.certificatePostcode = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.certificateRelationshipClient2 = New System.Windows.Forms.ComboBox()
        Me.certificateRelationshipClient1 = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.certificateTitle = New System.Windows.Forms.ComboBox()
        Me.certificateSurname = New System.Windows.Forms.TextBox()
        Me.certificateNames = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.RadioButton13 = New System.Windows.Forms.RadioButton()
        Me.RadioButton14 = New System.Windows.Forms.RadioButton()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.mustDoNotes = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.shouldDoNotes = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn2 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.corresAddress = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.corresPostcode = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.corresTitle = New System.Windows.Forms.ComboBox()
        Me.corresSurname = New System.Windows.Forms.TextBox()
        Me.corresNames = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.RadioButton19 = New System.Windows.Forms.RadioButton()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.RadioButton15 = New System.Windows.Forms.RadioButton()
        Me.RadioButton16 = New System.Windows.Forms.RadioButton()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.RadioButton17 = New System.Windows.Forms.RadioButton()
        Me.RadioButton18 = New System.Windows.Forms.RadioButton()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.DataGridView7 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn6 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.attendanceMode = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout
        Me.TabPage1.SuspendLayout
        Me.GroupBox2.SuspendLayout
        Me.GroupBox1.SuspendLayout
        Me.TabPage2.SuspendLayout
        Me.Panel4.SuspendLayout
        Me.Panel2.SuspendLayout
        Me.Panel3.SuspendLayout
        Me.Panel1.SuspendLayout
        CType(Me.DataGridView5,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabPage3.SuspendLayout
        CType(Me.DataGridView1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Panel5.SuspendLayout
        Me.TabPage4.SuspendLayout
        Me.Panel7.SuspendLayout
        Me.Panel6.SuspendLayout
        Me.TabPage5.SuspendLayout
        Me.TabPage6.SuspendLayout
        Me.TabPage7.SuspendLayout
        CType(Me.DataGridView2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.TabPage8.SuspendLayout
        Me.Panel10.SuspendLayout
        Me.Panel8.SuspendLayout
        Me.Panel9.SuspendLayout
        Me.TabPage9.SuspendLayout
        CType(Me.DataGridView7,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1120, 602)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(102, 47)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Close"
        Me.Button2.UseVisualStyleBackColor = false
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 63)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.Padding = New System.Drawing.Point(10, 10)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1209, 530)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Location = New System.Drawing.Point(4, 41)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Donor Information"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox3)
        Me.GroupBox2.Controls.Add(Me.TextBox6)
        Me.GroupBox2.Controls.Add(Me.CheckBox4)
        Me.GroupBox2.Controls.Add(Me.TextBox9)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.TextBox7)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.TextBox8)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(626, 70)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(541, 386)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Donor 2"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = true
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(129, 349)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(138, 22)
        Me.CheckBox3.TabIndex = 25
        Me.CheckBox3.Text = "Health && Welfare"
        Me.CheckBox3.UseVisualStyleBackColor = true
        '
        'TextBox6
        '
        Me.TextBox6.Enabled = false
        Me.TextBox6.Location = New System.Drawing.Point(129, 267)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(96, 24)
        Me.TextBox6.TabIndex = 26
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = true
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(129, 320)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(153, 22)
        Me.CheckBox4.TabIndex = 24
        Me.CheckBox4.Text = "Property && Finance"
        Me.CheckBox4.UseVisualStyleBackColor = true
        '
        'TextBox9
        '
        Me.TextBox9.Enabled = false
        Me.TextBox9.Location = New System.Drawing.Point(129, 41)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(385, 24)
        Me.TextBox9.TabIndex = 23
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 321)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 18)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "LPAs Required"
        '
        'TextBox7
        '
        Me.TextBox7.Enabled = false
        Me.TextBox7.Location = New System.Drawing.Point(129, 127)
        Me.TextBox7.Multiline = true
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(320, 134)
        Me.TextBox7.TabIndex = 25
        '
        'Label16
        '
        Me.Label16.AutoSize = true
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label16.Location = New System.Drawing.Point(56, 44)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(48, 18)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "Name"
        '
        'TextBox8
        '
        Me.TextBox8.Enabled = false
        Me.TextBox8.Location = New System.Drawing.Point(129, 84)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(228, 24)
        Me.TextBox8.TabIndex = 24
        '
        'Label15
        '
        Me.Label15.AutoSize = true
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label15.Location = New System.Drawing.Point(14, 87)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 18)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date of Birth"
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label14.Location = New System.Drawing.Point(42, 130)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(62, 18)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "Address"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBox2)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(16, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(541, 386)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Donor 1"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = true
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(130, 346)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(138, 22)
        Me.CheckBox2.TabIndex = 22
        Me.CheckBox2.Text = "Health && Welfare"
        Me.CheckBox2.UseVisualStyleBackColor = true
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = true
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(130, 317)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(153, 22)
        Me.CheckBox1.TabIndex = 21
        Me.CheckBox1.Text = "Property && Finance"
        Me.CheckBox1.UseVisualStyleBackColor = true
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 317)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 18)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "LPAs Required"
        '
        'TextBox5
        '
        Me.TextBox5.Enabled = false
        Me.TextBox5.Location = New System.Drawing.Point(130, 267)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(96, 24)
        Me.TextBox5.TabIndex = 19
        '
        'TextBox4
        '
        Me.TextBox4.Enabled = false
        Me.TextBox4.Location = New System.Drawing.Point(130, 127)
        Me.TextBox4.Multiline = true
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(320, 134)
        Me.TextBox4.TabIndex = 18
        '
        'TextBox3
        '
        Me.TextBox3.Enabled = false
        Me.TextBox3.Location = New System.Drawing.Point(130, 84)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(228, 24)
        Me.TextBox3.TabIndex = 17
        '
        'TextBox2
        '
        Me.TextBox2.Enabled = false
        Me.TextBox2.Location = New System.Drawing.Point(130, 41)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(385, 24)
        Me.TextBox2.TabIndex = 16
        '
        'Label13
        '
        Me.Label13.AutoSize = true
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label13.Location = New System.Drawing.Point(50, 130)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(62, 18)
        Me.Label13.TabIndex = 15
        Me.Label13.Text = "Address"
        '
        'Label12
        '
        Me.Label12.AutoSize = true
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label12.Location = New System.Drawing.Point(22, 87)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 18)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Date of Birth"
        '
        'Label11
        '
        Me.Label11.AutoSize = true
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label11.Location = New System.Drawing.Point(64, 44)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 18)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Name"
        '
        'Label10
        '
        Me.Label10.AutoSize = true
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(138, 20)
        Me.Label10.TabIndex = 16
        Me.Label10.Text = "Donor Information"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.Panel4)
        Me.TabPage2.Controls.Add(Me.Panel2)
        Me.TabPage2.Controls.Add(Me.Panel3)
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Controls.Add(Me.DataGridView5)
        Me.TabPage2.Controls.Add(Me.Button6)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 41)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Attorneys"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(173, 396)
        Me.TextBox1.Multiline = true
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(753, 75)
        Me.TextBox1.TabIndex = 38
        '
        'Label9
        '
        Me.Label9.AutoSize = true
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label9.Location = New System.Drawing.Point(24, 396)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(143, 18)
        Me.Label9.TabIndex = 37
        Me.Label9.Text = "Details of how to act"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.RadioButton10)
        Me.Panel4.Controls.Add(Me.RadioButton9)
        Me.Panel4.Controls.Add(Me.Label8)
        Me.Panel4.Controls.Add(Me.RadioButton7)
        Me.Panel4.Controls.Add(Me.RadioButton8)
        Me.Panel4.Location = New System.Drawing.Point(19, 355)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(939, 30)
        Me.Panel4.TabIndex = 37
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = true
        Me.RadioButton10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton10.Location = New System.Drawing.Point(580, 3)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(327, 22)
        Me.RadioButton10.TabIndex = 36
        Me.RadioButton10.TabStop = true
        Me.RadioButton10.Text = "Jointly for some, Jointly && Severally for others"
        Me.RadioButton10.UseVisualStyleBackColor = true
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = true
        Me.RadioButton9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton9.Location = New System.Drawing.Point(414, 4)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(145, 22)
        Me.RadioButton9.TabIndex = 35
        Me.RadioButton9.TabStop = true
        Me.RadioButton9.Text = "Jointly && Severally"
        Me.RadioButton9.UseVisualStyleBackColor = true
        '
        'Label8
        '
        Me.Label8.AutoSize = true
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label8.Location = New System.Drawing.Point(5, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(217, 18)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "How should your attorneys act?"
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = true
        Me.RadioButton7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton7.Location = New System.Drawing.Point(238, 5)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(66, 22)
        Me.RadioButton7.TabIndex = 33
        Me.RadioButton7.TabStop = true
        Me.RadioButton7.Text = "Solely"
        Me.RadioButton7.UseVisualStyleBackColor = true
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = true
        Me.RadioButton8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton8.Location = New System.Drawing.Point(325, 5)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(68, 22)
        Me.RadioButton8.TabIndex = 34
        Me.RadioButton8.TabStop = true
        Me.RadioButton8.Text = "Jointly"
        Me.RadioButton8.UseVisualStyleBackColor = true
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.RadioButton4)
        Me.Panel2.Controls.Add(Me.RadioButton3)
        Me.Panel2.Location = New System.Drawing.Point(19, 84)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(400, 30)
        Me.Panel2.TabIndex = 36
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label6.Location = New System.Drawing.Point(5, 5)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(213, 18)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Are all children to be attorneys?"
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = true
        Me.RadioButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton4.Location = New System.Drawing.Point(250, 5)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton4.TabIndex = 30
        Me.RadioButton4.TabStop = true
        Me.RadioButton4.Text = "Yes"
        Me.RadioButton4.UseVisualStyleBackColor = true
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = true
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(305, 5)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton3.TabIndex = 31
        Me.RadioButton3.TabStop = true
        Me.RadioButton3.Text = "No"
        Me.RadioButton3.UseVisualStyleBackColor = true
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.RadioButton6)
        Me.Panel3.Controls.Add(Me.RadioButton5)
        Me.Panel3.Location = New System.Drawing.Point(19, 123)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(400, 30)
        Me.Panel3.TabIndex = 36
        '
        'Label7
        '
        Me.Label7.AutoSize = true
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label7.Location = New System.Drawing.Point(5, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(224, 18)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Is anyone else to be an attorney?"
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = true
        Me.RadioButton6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton6.Location = New System.Drawing.Point(250, 5)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton6.TabIndex = 33
        Me.RadioButton6.TabStop = true
        Me.RadioButton6.Text = "Yes"
        Me.RadioButton6.UseVisualStyleBackColor = true
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = true
        Me.RadioButton5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton5.Location = New System.Drawing.Point(305, 5)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton5.TabIndex = 34
        Me.RadioButton5.TabStop = true
        Me.RadioButton5.Text = "No"
        Me.RadioButton5.UseVisualStyleBackColor = true
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Controls.Add(Me.RadioButton2)
        Me.Panel1.Location = New System.Drawing.Point(19, 45)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(400, 30)
        Me.Panel1.TabIndex = 35
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label5.Location = New System.Drawing.Point(5, 5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(231, 18)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Is the spouse/partner an attorney?"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = true
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(250, 5)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton1.TabIndex = 27
        Me.RadioButton1.TabStop = true
        Me.RadioButton1.Text = "Yes"
        Me.RadioButton1.UseVisualStyleBackColor = true
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = true
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(305, 5)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton2.TabIndex = 28
        Me.RadioButton2.TabStop = true
        Me.RadioButton2.Text = "No"
        Me.RadioButton2.UseVisualStyleBackColor = true
        '
        'DataGridView5
        '
        Me.DataGridView5.AllowUserToAddRows = false
        Me.DataGridView5.AllowUserToDeleteRows = false
        Me.DataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewLinkColumn4})
        Me.DataGridView5.Location = New System.Drawing.Point(16, 191)
        Me.DataGridView5.Name = "DataGridView5"
        Me.DataGridView5.ReadOnly = true
        Me.DataGridView5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView5.Size = New System.Drawing.Size(831, 138)
        Me.DataGridView5.TabIndex = 25
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = true
        Me.DataGridViewTextBoxColumn10.Visible = false
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Attorney Name"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = true
        Me.DataGridViewTextBoxColumn12.Width = 300
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = true
        Me.DataGridViewTextBoxColumn13.Width = 180
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = true
        Me.DataGridViewTextBoxColumn14.Width = 180
        '
        'DataGridViewLinkColumn4
        '
        Me.DataGridViewLinkColumn4.HeaderText = "Action"
        Me.DataGridViewLinkColumn4.Name = "DataGridViewLinkColumn4"
        Me.DataGridViewLinkColumn4.ReadOnly = true
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button6.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(611, 149)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(236, 36)
        Me.Button6.TabIndex = 24
        Me.Button6.Text = "Add An Attorney"
        Me.Button6.UseVisualStyleBackColor = false
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 20)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Attorneys"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage3.Controls.Add(Me.DataGridView1)
        Me.TabPage3.Controls.Add(Me.Button1)
        Me.TabPage3.Controls.Add(Me.Panel5)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Location = New System.Drawing.Point(4, 41)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Replacement Attorneys"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = false
        Me.DataGridView1.AllowUserToDeleteRows = false
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewLinkColumn1})
        Me.DataGridView1.Location = New System.Drawing.Point(19, 97)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = true
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.Size = New System.Drawing.Size(831, 138)
        Me.DataGridView1.TabIndex = 38
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = true
        Me.DataGridViewTextBoxColumn1.Visible = false
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Replacement Attorney Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = true
        Me.DataGridViewTextBoxColumn2.Width = 300
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = true
        Me.DataGridViewTextBoxColumn3.Width = 180
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = true
        Me.DataGridViewTextBoxColumn4.Width = 180
        '
        'DataGridViewLinkColumn1
        '
        Me.DataGridViewLinkColumn1.HeaderText = "Action"
        Me.DataGridViewLinkColumn1.Name = "DataGridViewLinkColumn1"
        Me.DataGridViewLinkColumn1.ReadOnly = true
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(596, 55)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(254, 36)
        Me.Button1.TabIndex = 37
        Me.Button1.Text = "Add A Replacement Attorney"
        Me.Button1.UseVisualStyleBackColor = false
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label18)
        Me.Panel5.Controls.Add(Me.RadioButton11)
        Me.Panel5.Controls.Add(Me.RadioButton12)
        Me.Panel5.Location = New System.Drawing.Point(19, 45)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(400, 30)
        Me.Panel5.TabIndex = 36
        '
        'Label18
        '
        Me.Label18.AutoSize = true
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label18.Location = New System.Drawing.Point(5, 5)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(263, 18)
        Me.Label18.TabIndex = 26
        Me.Label18.Text = "Do you require replacement attorneys?"
        '
        'RadioButton11
        '
        Me.RadioButton11.AutoSize = true
        Me.RadioButton11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton11.Location = New System.Drawing.Point(278, 4)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton11.TabIndex = 27
        Me.RadioButton11.TabStop = true
        Me.RadioButton11.Text = "Yes"
        Me.RadioButton11.UseVisualStyleBackColor = true
        '
        'RadioButton12
        '
        Me.RadioButton12.AutoSize = true
        Me.RadioButton12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton12.Location = New System.Drawing.Point(333, 4)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton12.TabIndex = 28
        Me.RadioButton12.TabStop = true
        Me.RadioButton12.Text = "No"
        Me.RadioButton12.UseVisualStyleBackColor = true
        '
        'Label17
        '
        Me.Label17.AutoSize = true
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label17.Location = New System.Drawing.Point(12, 20)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(176, 20)
        Me.Label17.TabIndex = 18
        Me.Label17.Text = "Replacement Attorneys"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage4.Controls.Add(Me.Panel7)
        Me.TabPage4.Controls.Add(Me.Panel6)
        Me.TabPage4.Controls.Add(Me.Label19)
        Me.TabPage4.Location = New System.Drawing.Point(4, 41)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Certificate Provider"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.AliceBlue
        Me.Panel7.Controls.Add(Me.Button5)
        Me.Panel7.Controls.Add(Me.certificateAddress)
        Me.Panel7.Controls.Add(Me.Label23)
        Me.Panel7.Controls.Add(Me.certificatePostcode)
        Me.Panel7.Controls.Add(Me.Label39)
        Me.Panel7.Controls.Add(Me.certificateRelationshipClient2)
        Me.Panel7.Controls.Add(Me.certificateRelationshipClient1)
        Me.Panel7.Controls.Add(Me.Label28)
        Me.Panel7.Controls.Add(Me.CheckBox5)
        Me.Panel7.Controls.Add(Me.Label27)
        Me.Panel7.Controls.Add(Me.Label26)
        Me.Panel7.Controls.Add(Me.Label25)
        Me.Panel7.Controls.Add(Me.DateTimePicker1)
        Me.Panel7.Controls.Add(Me.Label24)
        Me.Panel7.Controls.Add(Me.certificateTitle)
        Me.Panel7.Controls.Add(Me.certificateSurname)
        Me.Panel7.Controls.Add(Me.certificateNames)
        Me.Panel7.Controls.Add(Me.Label22)
        Me.Panel7.Controls.Add(Me.Label21)
        Me.Panel7.Controls.Add(Me.Label20)
        Me.Panel7.Location = New System.Drawing.Point(19, 92)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(773, 375)
        Me.Panel7.TabIndex = 37
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button5.Location = New System.Drawing.Point(280, 73)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 69
        Me.Button5.Text = "Lookup"
        Me.Button5.UseVisualStyleBackColor = true
        '
        'certificateAddress
        '
        Me.certificateAddress.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.certificateAddress.Location = New System.Drawing.Point(187, 113)
        Me.certificateAddress.Multiline = true
        Me.certificateAddress.Name = "certificateAddress"
        Me.certificateAddress.Size = New System.Drawing.Size(268, 102)
        Me.certificateAddress.TabIndex = 70
        '
        'Label23
        '
        Me.Label23.AutoSize = true
        Me.Label23.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label23.Location = New System.Drawing.Point(122, 116)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(54, 18)
        Me.Label23.TabIndex = 72
        Me.Label23.Text = "Address"
        '
        'certificatePostcode
        '
        Me.certificatePostcode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.certificatePostcode.Location = New System.Drawing.Point(187, 74)
        Me.certificatePostcode.Name = "certificatePostcode"
        Me.certificatePostcode.Size = New System.Drawing.Size(87, 23)
        Me.certificatePostcode.TabIndex = 68
        '
        'Label39
        '
        Me.Label39.AutoSize = true
        Me.Label39.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label39.Location = New System.Drawing.Point(108, 76)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(59, 18)
        Me.Label39.TabIndex = 71
        Me.Label39.Text = "Postcode"
        '
        'certificateRelationshipClient2
        '
        Me.certificateRelationshipClient2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.certificateRelationshipClient2.FormattingEnabled = true
        Me.certificateRelationshipClient2.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.certificateRelationshipClient2.Location = New System.Drawing.Point(187, 264)
        Me.certificateRelationshipClient2.Name = "certificateRelationshipClient2"
        Me.certificateRelationshipClient2.Size = New System.Drawing.Size(257, 26)
        Me.certificateRelationshipClient2.TabIndex = 34
        '
        'certificateRelationshipClient1
        '
        Me.certificateRelationshipClient1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.certificateRelationshipClient1.FormattingEnabled = true
        Me.certificateRelationshipClient1.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.certificateRelationshipClient1.Location = New System.Drawing.Point(187, 230)
        Me.certificateRelationshipClient1.Name = "certificateRelationshipClient1"
        Me.certificateRelationshipClient1.Size = New System.Drawing.Size(257, 26)
        Me.certificateRelationshipClient1.TabIndex = 33
        '
        'Label28
        '
        Me.Label28.AutoSize = true
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label28.Location = New System.Drawing.Point(17, 350)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(291, 13)
        Me.Label28.TabIndex = 32
        Me.Label28.Text = "(if in doubt, please check the list of those that are prohibited)"
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = true
        Me.CheckBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBox5.Location = New System.Drawing.Point(485, 336)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox5.TabIndex = 31
        Me.CheckBox5.UseVisualStyleBackColor = true
        '
        'Label27
        '
        Me.Label27.AutoSize = true
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label27.Location = New System.Drawing.Point(17, 332)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(462, 18)
        Me.Label27.TabIndex = 30
        Me.Label27.Text = "I confirm that the chosen person is allowed to be a certificate provider"
        '
        'Label26
        '
        Me.Label26.AutoSize = true
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label26.Location = New System.Drawing.Point(17, 265)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(164, 18)
        Me.Label26.TabIndex = 27
        Me.Label26.Text = "Relationship To Client 2"
        '
        'Label25
        '
        Me.Label25.AutoSize = true
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label25.Location = New System.Drawing.Point(17, 231)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(164, 18)
        Me.Label25.TabIndex = 26
        Me.Label25.Text = "Relationship To Client 1"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(587, 262)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(166, 23)
        Me.DateTimePicker1.TabIndex = 25
        Me.DateTimePicker1.Visible = false
        '
        'Label24
        '
        Me.Label24.AutoSize = true
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label24.Location = New System.Drawing.Point(584, 241)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(90, 18)
        Me.Label24.TabIndex = 20
        Me.Label24.Text = "Date of Birth"
        Me.Label24.Visible = false
        '
        'certificateTitle
        '
        Me.certificateTitle.FormattingEnabled = true
        Me.certificateTitle.Items.AddRange(New Object() {"Mr", "Mrs", "Miss", "Ms", "Doctor"})
        Me.certificateTitle.Location = New System.Drawing.Point(19, 25)
        Me.certificateTitle.Name = "certificateTitle"
        Me.certificateTitle.Size = New System.Drawing.Size(87, 26)
        Me.certificateTitle.TabIndex = 5
        '
        'certificateSurname
        '
        Me.certificateSurname.Location = New System.Drawing.Point(488, 25)
        Me.certificateSurname.Name = "certificateSurname"
        Me.certificateSurname.Size = New System.Drawing.Size(265, 23)
        Me.certificateSurname.TabIndex = 4
        '
        'certificateNames
        '
        Me.certificateNames.Location = New System.Drawing.Point(124, 25)
        Me.certificateNames.Name = "certificateNames"
        Me.certificateNames.Size = New System.Drawing.Size(337, 23)
        Me.certificateNames.TabIndex = 3
        '
        'Label22
        '
        Me.Label22.AutoSize = true
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label22.Location = New System.Drawing.Point(485, 6)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(68, 18)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Surname"
        '
        'Label21
        '
        Me.Label21.AutoSize = true
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label21.Location = New System.Drawing.Point(121, 6)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(99, 18)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "First Name(s)"
        '
        'Label20
        '
        Me.Label20.AutoSize = true
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label20.Location = New System.Drawing.Point(16, 6)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(35, 18)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Title"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.RadioButton13)
        Me.Panel6.Controls.Add(Me.RadioButton14)
        Me.Panel6.Location = New System.Drawing.Point(19, 45)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(551, 30)
        Me.Panel6.TabIndex = 36
        '
        'RadioButton13
        '
        Me.RadioButton13.AutoSize = true
        Me.RadioButton13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton13.Location = New System.Drawing.Point(19, 5)
        Me.RadioButton13.Name = "RadioButton13"
        Me.RadioButton13.Size = New System.Drawing.Size(109, 22)
        Me.RadioButton13.TabIndex = 27
        Me.RadioButton13.TabStop = true
        Me.RadioButton13.Text = "Professional"
        Me.RadioButton13.UseVisualStyleBackColor = true
        '
        'RadioButton14
        '
        Me.RadioButton14.AutoSize = true
        Me.RadioButton14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton14.Location = New System.Drawing.Point(153, 5)
        Me.RadioButton14.Name = "RadioButton14"
        Me.RadioButton14.Size = New System.Drawing.Size(383, 22)
        Me.RadioButton14.TabIndex = 28
        Me.RadioButton14.TabStop = true
        Me.RadioButton14.Text = "Other (you must have known them for at least 2 years)"
        Me.RadioButton14.UseVisualStyleBackColor = true
        '
        'Label19
        '
        Me.Label19.AutoSize = true
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label19.Location = New System.Drawing.Point(12, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(142, 20)
        Me.Label19.TabIndex = 19
        Me.Label19.Text = "Certificate Provider"
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage5.Controls.Add(Me.mustDoNotes)
        Me.TabPage5.Controls.Add(Me.Label29)
        Me.TabPage5.Location = New System.Drawing.Point(4, 41)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Instructions"
        '
        'mustDoNotes
        '
        Me.mustDoNotes.Location = New System.Drawing.Point(12, 50)
        Me.mustDoNotes.Multiline = true
        Me.mustDoNotes.Name = "mustDoNotes"
        Me.mustDoNotes.Size = New System.Drawing.Size(1015, 296)
        Me.mustDoNotes.TabIndex = 18
        '
        'Label29
        '
        Me.Label29.AutoSize = true
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label29.Location = New System.Drawing.Point(12, 20)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(92, 20)
        Me.Label29.TabIndex = 17
        Me.Label29.Text = "Instructions"
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage6.Controls.Add(Me.shouldDoNotes)
        Me.TabPage6.Controls.Add(Me.Label30)
        Me.TabPage6.Location = New System.Drawing.Point(4, 41)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Preferences"
        '
        'shouldDoNotes
        '
        Me.shouldDoNotes.Location = New System.Drawing.Point(12, 50)
        Me.shouldDoNotes.Multiline = true
        Me.shouldDoNotes.Name = "shouldDoNotes"
        Me.shouldDoNotes.Size = New System.Drawing.Size(1015, 296)
        Me.shouldDoNotes.TabIndex = 20
        '
        'Label30
        '
        Me.Label30.AutoSize = true
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label30.Location = New System.Drawing.Point(12, 20)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(95, 20)
        Me.Label30.TabIndex = 19
        Me.Label30.Text = "Preferences"
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage7.Controls.Add(Me.DataGridView2)
        Me.TabPage7.Controls.Add(Me.Button3)
        Me.TabPage7.Controls.Add(Me.Label31)
        Me.TabPage7.Location = New System.Drawing.Point(4, 41)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Persons To Be Told"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = false
        Me.DataGridView2.AllowUserToDeleteRows = false
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewLinkColumn2})
        Me.DataGridView2.Location = New System.Drawing.Point(16, 77)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = true
        Me.DataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView2.Size = New System.Drawing.Size(831, 194)
        Me.DataGridView2.TabIndex = 40
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = true
        Me.DataGridViewTextBoxColumn5.Visible = false
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = true
        Me.DataGridViewTextBoxColumn6.Width = 300
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = true
        Me.DataGridViewTextBoxColumn7.Width = 180
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = true
        Me.DataGridViewTextBoxColumn8.Width = 180
        '
        'DataGridViewLinkColumn2
        '
        Me.DataGridViewLinkColumn2.HeaderText = "Action"
        Me.DataGridViewLinkColumn2.Name = "DataGridViewLinkColumn2"
        Me.DataGridViewLinkColumn2.ReadOnly = true
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(632, 35)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(215, 36)
        Me.Button3.TabIndex = 39
        Me.Button3.Text = "Add Person To Be Told"
        Me.Button3.UseVisualStyleBackColor = false
        '
        'Label31
        '
        Me.Label31.AutoSize = true
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label31.Location = New System.Drawing.Point(12, 20)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(147, 20)
        Me.Label31.TabIndex = 20
        Me.Label31.Text = "Persons To Be Told"
        '
        'TabPage8
        '
        Me.TabPage8.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage8.Controls.Add(Me.Panel10)
        Me.TabPage8.Controls.Add(Me.Panel8)
        Me.TabPage8.Controls.Add(Me.Panel9)
        Me.TabPage8.Controls.Add(Me.Label32)
        Me.TabPage8.Location = New System.Drawing.Point(4, 41)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "Registration"
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.AliceBlue
        Me.Panel10.Controls.Add(Me.Button4)
        Me.Panel10.Controls.Add(Me.corresAddress)
        Me.Panel10.Controls.Add(Me.Label37)
        Me.Panel10.Controls.Add(Me.corresPostcode)
        Me.Panel10.Controls.Add(Me.Label38)
        Me.Panel10.Controls.Add(Me.DateTimePicker2)
        Me.Panel10.Controls.Add(Me.Label40)
        Me.Panel10.Controls.Add(Me.corresTitle)
        Me.Panel10.Controls.Add(Me.corresSurname)
        Me.Panel10.Controls.Add(Me.corresNames)
        Me.Panel10.Controls.Add(Me.Label41)
        Me.Panel10.Controls.Add(Me.Label42)
        Me.Panel10.Controls.Add(Me.Label43)
        Me.Panel10.Location = New System.Drawing.Point(19, 150)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(773, 243)
        Me.Panel10.TabIndex = 39
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button4.Location = New System.Drawing.Point(217, 66)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "Lookup"
        Me.Button4.UseVisualStyleBackColor = true
        '
        'corresAddress
        '
        Me.corresAddress.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.corresAddress.Location = New System.Drawing.Point(124, 106)
        Me.corresAddress.Multiline = true
        Me.corresAddress.Name = "corresAddress"
        Me.corresAddress.Size = New System.Drawing.Size(268, 102)
        Me.corresAddress.TabIndex = 5
        '
        'Label37
        '
        Me.Label37.AutoSize = true
        Me.Label37.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label37.Location = New System.Drawing.Point(59, 109)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(54, 18)
        Me.Label37.TabIndex = 67
        Me.Label37.Text = "Address"
        '
        'corresPostcode
        '
        Me.corresPostcode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.corresPostcode.Location = New System.Drawing.Point(124, 67)
        Me.corresPostcode.Name = "corresPostcode"
        Me.corresPostcode.Size = New System.Drawing.Size(87, 23)
        Me.corresPostcode.TabIndex = 3
        '
        'Label38
        '
        Me.Label38.AutoSize = true
        Me.Label38.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label38.Location = New System.Drawing.Point(45, 69)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(59, 18)
        Me.Label38.TabIndex = 66
        Me.Label38.Text = "Postcode"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Location = New System.Drawing.Point(587, 205)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(166, 23)
        Me.DateTimePicker2.TabIndex = 25
        Me.DateTimePicker2.Visible = false
        '
        'Label40
        '
        Me.Label40.AutoSize = true
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label40.Location = New System.Drawing.Point(479, 208)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(90, 18)
        Me.Label40.TabIndex = 20
        Me.Label40.Text = "Date of Birth"
        Me.Label40.Visible = false
        '
        'corresTitle
        '
        Me.corresTitle.FormattingEnabled = true
        Me.corresTitle.Items.AddRange(New Object() {"Mr", "Mrs", "Miss", "Ms"})
        Me.corresTitle.Location = New System.Drawing.Point(19, 25)
        Me.corresTitle.Name = "corresTitle"
        Me.corresTitle.Size = New System.Drawing.Size(87, 26)
        Me.corresTitle.TabIndex = 0
        '
        'corresSurname
        '
        Me.corresSurname.Location = New System.Drawing.Point(488, 25)
        Me.corresSurname.Name = "corresSurname"
        Me.corresSurname.Size = New System.Drawing.Size(265, 23)
        Me.corresSurname.TabIndex = 2
        '
        'corresNames
        '
        Me.corresNames.Location = New System.Drawing.Point(124, 25)
        Me.corresNames.Name = "corresNames"
        Me.corresNames.Size = New System.Drawing.Size(337, 23)
        Me.corresNames.TabIndex = 1
        '
        'Label41
        '
        Me.Label41.AutoSize = true
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label41.Location = New System.Drawing.Point(485, 6)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(68, 18)
        Me.Label41.TabIndex = 2
        Me.Label41.Text = "Surname"
        '
        'Label42
        '
        Me.Label42.AutoSize = true
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label42.Location = New System.Drawing.Point(121, 6)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(99, 18)
        Me.Label42.TabIndex = 1
        Me.Label42.Text = "First Name(s)"
        '
        'Label43
        '
        Me.Label43.AutoSize = true
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label43.Location = New System.Drawing.Point(16, 6)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(35, 18)
        Me.Label43.TabIndex = 0
        Me.Label43.Text = "Title"
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.RadioButton19)
        Me.Panel8.Controls.Add(Me.Label33)
        Me.Panel8.Controls.Add(Me.RadioButton15)
        Me.Panel8.Controls.Add(Me.RadioButton16)
        Me.Panel8.Location = New System.Drawing.Point(19, 90)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(738, 30)
        Me.Panel8.TabIndex = 38
        '
        'RadioButton19
        '
        Me.RadioButton19.AutoSize = true
        Me.RadioButton19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton19.Location = New System.Drawing.Point(431, 5)
        Me.RadioButton19.Name = "RadioButton19"
        Me.RadioButton19.Size = New System.Drawing.Size(245, 22)
        Me.RadioButton19.TabIndex = 2
        Me.RadioButton19.TabStop = true
        Me.RadioButton19.Text = "Professional (enter details below)"
        Me.RadioButton19.UseVisualStyleBackColor = true
        '
        'Label33
        '
        Me.Label33.AutoSize = true
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label33.Location = New System.Drawing.Point(5, 5)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(187, 18)
        Me.Label33.TabIndex = 29
        Me.Label33.Text = "Who is the correspondent?"
        '
        'RadioButton15
        '
        Me.RadioButton15.AutoSize = true
        Me.RadioButton15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton15.Location = New System.Drawing.Point(250, 5)
        Me.RadioButton15.Name = "RadioButton15"
        Me.RadioButton15.Size = New System.Drawing.Size(68, 22)
        Me.RadioButton15.TabIndex = 0
        Me.RadioButton15.TabStop = true
        Me.RadioButton15.Text = "Donor"
        Me.RadioButton15.UseVisualStyleBackColor = true
        '
        'RadioButton16
        '
        Me.RadioButton16.AutoSize = true
        Me.RadioButton16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton16.Location = New System.Drawing.Point(332, 5)
        Me.RadioButton16.Name = "RadioButton16"
        Me.RadioButton16.Size = New System.Drawing.Size(80, 22)
        Me.RadioButton16.TabIndex = 1
        Me.RadioButton16.TabStop = true
        Me.RadioButton16.Text = "Attorney"
        Me.RadioButton16.UseVisualStyleBackColor = true
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Label34)
        Me.Panel9.Controls.Add(Me.RadioButton17)
        Me.Panel9.Controls.Add(Me.RadioButton18)
        Me.Panel9.Location = New System.Drawing.Point(19, 51)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(607, 30)
        Me.Panel9.TabIndex = 37
        '
        'Label34
        '
        Me.Label34.AutoSize = true
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label34.Location = New System.Drawing.Point(5, 5)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(228, 18)
        Me.Label34.TabIndex = 26
        Me.Label34.Text = "Who is going to register the LPA?"
        '
        'RadioButton17
        '
        Me.RadioButton17.AutoSize = true
        Me.RadioButton17.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton17.Location = New System.Drawing.Point(250, 5)
        Me.RadioButton17.Name = "RadioButton17"
        Me.RadioButton17.Size = New System.Drawing.Size(68, 22)
        Me.RadioButton17.TabIndex = 0
        Me.RadioButton17.TabStop = true
        Me.RadioButton17.Text = "Donor"
        Me.RadioButton17.UseVisualStyleBackColor = true
        '
        'RadioButton18
        '
        Me.RadioButton18.AutoSize = true
        Me.RadioButton18.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton18.Location = New System.Drawing.Point(332, 5)
        Me.RadioButton18.Name = "RadioButton18"
        Me.RadioButton18.Size = New System.Drawing.Size(80, 22)
        Me.RadioButton18.TabIndex = 1
        Me.RadioButton18.TabStop = true
        Me.RadioButton18.Text = "Attorney"
        Me.RadioButton18.UseVisualStyleBackColor = true
        '
        'Label32
        '
        Me.Label32.AutoSize = true
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label32.Location = New System.Drawing.Point(12, 20)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(129, 20)
        Me.Label32.TabIndex = 18
        Me.Label32.Text = "LPA Registration"
        '
        'TabPage9
        '
        Me.TabPage9.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.TabPage9.Controls.Add(Me.TextBox20)
        Me.TabPage9.Controls.Add(Me.Label36)
        Me.TabPage9.Controls.Add(Me.DataGridView7)
        Me.TabPage9.Controls.Add(Me.Label35)
        Me.TabPage9.Location = New System.Drawing.Point(4, 41)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = "Attendance Notes"
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(513, 100)
        Me.TextBox20.Multiline = true
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(674, 369)
        Me.TextBox20.TabIndex = 40
        '
        'Label36
        '
        Me.Label36.AutoSize = true
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label36.Location = New System.Drawing.Point(510, 69)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(184, 18)
        Me.Label36.TabIndex = 39
        Me.Label36.Text = "Supplementary Information"
        '
        'DataGridView7
        '
        Me.DataGridView7.AllowUserToAddRows = false
        Me.DataGridView7.AllowUserToDeleteRows = false
        Me.DataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView7.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewLinkColumn6})
        Me.DataGridView7.Location = New System.Drawing.Point(16, 69)
        Me.DataGridView7.Name = "DataGridView7"
        Me.DataGridView7.ReadOnly = true
        Me.DataGridView7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView7.Size = New System.Drawing.Size(478, 369)
        Me.DataGridView7.TabIndex = 21
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = true
        Me.DataGridViewTextBoxColumn18.Visible = false
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Question"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = true
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Answer"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = true
        Me.DataGridViewTextBoxColumn20.Width = 200
        '
        'DataGridViewLinkColumn6
        '
        Me.DataGridViewLinkColumn6.HeaderText = "Action"
        Me.DataGridViewLinkColumn6.Name = "DataGridViewLinkColumn6"
        Me.DataGridViewLinkColumn6.ReadOnly = true
        '
        'Label35
        '
        Me.Label35.AutoSize = true
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label35.Location = New System.Drawing.Point(12, 20)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(138, 20)
        Me.Label35.TabIndex = 19
        Me.Label35.Text = "Attendance Notes"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(187, 29)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "LPA Instruction"
        '
        'attendanceMode
        '
        Me.attendanceMode.AutoSize = true
        Me.attendanceMode.Location = New System.Drawing.Point(1129, 27)
        Me.attendanceMode.Name = "attendanceMode"
        Me.attendanceMode.Size = New System.Drawing.Size(88, 13)
        Me.attendanceMode.TabIndex = 7
        Me.attendanceMode.Text = "attendanceMode"
        Me.attendanceMode.Visible = false
        '
        'factfind_lpa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221,Byte),Integer), CType(CType(244,Byte),Integer), CType(CType(250,Byte),Integer))
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.attendanceMode)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button2)
        Me.MaximizeBox = false
        Me.Name = "factfind_lpa"
        Me.Text = "Client Instruction - LPAs"
        Me.TabControl1.ResumeLayout(false)
        Me.TabPage1.ResumeLayout(false)
        Me.TabPage1.PerformLayout
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.TabPage2.ResumeLayout(false)
        Me.TabPage2.PerformLayout
        Me.Panel4.ResumeLayout(false)
        Me.Panel4.PerformLayout
        Me.Panel2.ResumeLayout(false)
        Me.Panel2.PerformLayout
        Me.Panel3.ResumeLayout(false)
        Me.Panel3.PerformLayout
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        CType(Me.DataGridView5,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabPage3.ResumeLayout(false)
        Me.TabPage3.PerformLayout
        CType(Me.DataGridView1,System.ComponentModel.ISupportInitialize).EndInit
        Me.Panel5.ResumeLayout(false)
        Me.Panel5.PerformLayout
        Me.TabPage4.ResumeLayout(false)
        Me.TabPage4.PerformLayout
        Me.Panel7.ResumeLayout(false)
        Me.Panel7.PerformLayout
        Me.Panel6.ResumeLayout(false)
        Me.Panel6.PerformLayout
        Me.TabPage5.ResumeLayout(false)
        Me.TabPage5.PerformLayout
        Me.TabPage6.ResumeLayout(false)
        Me.TabPage6.PerformLayout
        Me.TabPage7.ResumeLayout(false)
        Me.TabPage7.PerformLayout
        CType(Me.DataGridView2,System.ComponentModel.ISupportInitialize).EndInit
        Me.TabPage8.ResumeLayout(false)
        Me.TabPage8.PerformLayout
        Me.Panel10.ResumeLayout(false)
        Me.Panel10.PerformLayout
        Me.Panel8.ResumeLayout(false)
        Me.Panel8.PerformLayout
        Me.Panel9.ResumeLayout(false)
        Me.Panel9.PerformLayout
        Me.TabPage9.ResumeLayout(false)
        Me.TabPage9.PerformLayout
        CType(Me.DataGridView7,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents Button2 As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents TabPage9 As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents CheckBox3 As CheckBox
    Friend WithEvents CheckBox4 As CheckBox
    Friend WithEvents Label4 As Label
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents DataGridView5 As DataGridView
    Friend WithEvents Button6 As Button
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents RadioButton6 As RadioButton
    Friend WithEvents Label7 As Label
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents Label6 As Label
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Label5 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents RadioButton10 As RadioButton
    Friend WithEvents RadioButton9 As RadioButton
    Friend WithEvents Label8 As Label
    Friend WithEvents RadioButton7 As RadioButton
    Friend WithEvents RadioButton8 As RadioButton
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn4 As DataGridViewLinkColumn
    Friend WithEvents Label9 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Label18 As Label
    Friend WithEvents RadioButton11 As RadioButton
    Friend WithEvents RadioButton12 As RadioButton
    Friend WithEvents Label17 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn1 As DataGridViewLinkColumn
    Friend WithEvents Panel6 As Panel
    Friend WithEvents RadioButton13 As RadioButton
    Friend WithEvents RadioButton14 As RadioButton
    Friend WithEvents Label19 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Label24 As Label
    Friend WithEvents certificateTitle As ComboBox
    Friend WithEvents certificateSurname As TextBox
    Friend WithEvents certificateNames As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents CheckBox5 As CheckBox
    Friend WithEvents Label27 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents mustDoNotes As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents shouldDoNotes As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn2 As DataGridViewLinkColumn
    Friend WithEvents Button3 As Button
    Friend WithEvents Label31 As Label
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Label33 As Label
    Friend WithEvents RadioButton15 As RadioButton
    Friend WithEvents RadioButton16 As RadioButton
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Label34 As Label
    Friend WithEvents RadioButton17 As RadioButton
    Friend WithEvents RadioButton18 As RadioButton
    Friend WithEvents Label32 As Label
    Friend WithEvents RadioButton19 As RadioButton
    Friend WithEvents Panel10 As Panel
    Friend WithEvents DateTimePicker2 As DateTimePicker
    Friend WithEvents Label40 As Label
    Friend WithEvents corresTitle As ComboBox
    Friend WithEvents corresSurname As TextBox
    Friend WithEvents corresNames As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents DataGridView7 As DataGridView
    Friend WithEvents TextBox20 As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents attendanceMode As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents corresAddress As TextBox
    Friend WithEvents Label37 As Label
    Friend WithEvents corresPostcode As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents certificateRelationshipClient2 As ComboBox
    Friend WithEvents certificateRelationshipClient1 As ComboBox
    Friend WithEvents Button5 As Button
    Friend WithEvents certificateAddress As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents certificatePostcode As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn6 As DataGridViewLinkColumn
End Class
