﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ppt_beneficiary_add
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.percentage = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.beneficiaryPostcode = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.beneficiaryAddress = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.beneficiarySurname = New System.Windows.Forms.TextBox()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.beneficiaryForenames = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.atWhatAgeIssueLabel = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.beneficiaryTitle = New System.Windows.Forms.ComboBox()
        Me.atWhatAge = New System.Windows.Forms.ComboBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.atWhatAgeIssue = New System.Windows.Forms.ComboBox()
        Me.relationToClient1 = New System.Windows.Forms.ComboBox()
        Me.relationToClient2 = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.black
        Me.Button3.Location = New System.Drawing.Point(489, 498)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(146, 36)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Add Beneficiary"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.black
        Me.Button1.Location = New System.Drawing.Point(641, 498)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 36)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(168, 20)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Add A PPT Beneficiary"
        '
        'percentage
        '
        Me.percentage.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.percentage.Location = New System.Drawing.Point(179, 358)
        Me.percentage.Name = "percentage"
        Me.percentage.Size = New System.Drawing.Size(45, 22)
        Me.percentage.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(56, 361)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(117, 16)
        Me.Label9.TabIndex = 72
        Me.Label9.Text = "Percentage Share"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(105, 179)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 62
        Me.Label8.Text = "Address"
        '
        'beneficiaryPostcode
        '
        Me.beneficiaryPostcode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryPostcode.Location = New System.Drawing.Point(179, 131)
        Me.beneficiaryPostcode.Name = "beneficiaryPostcode"
        Me.beneficiaryPostcode.Size = New System.Drawing.Size(87, 22)
        Me.beneficiaryPostcode.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(98, 137)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 16)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Postcode"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(272, 130)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Lookup"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'beneficiaryAddress
        '
        Me.beneficiaryAddress.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryAddress.Location = New System.Drawing.Point(179, 176)
        Me.beneficiaryAddress.Multiline = True
        Me.beneficiaryAddress.Name = "beneficiaryAddress"
        Me.beneficiaryAddress.Size = New System.Drawing.Size(268, 88)
        Me.beneficiaryAddress.TabIndex = 5
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(62, 399)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(103, 16)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Lapse or Issue?"
        '
        'beneficiarySurname
        '
        Me.beneficiarySurname.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiarySurname.Location = New System.Drawing.Point(487, 83)
        Me.beneficiarySurname.Name = "beneficiarySurname"
        Me.beneficiarySurname.Size = New System.Drawing.Size(218, 22)
        Me.beneficiarySurname.TabIndex = 2
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.Location = New System.Drawing.Point(180, 398)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(64, 20)
        Me.RadioButton8.TabIndex = 8
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Lapse"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.Location = New System.Drawing.Point(250, 398)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(58, 20)
        Me.RadioButton7.TabIndex = 9
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "Issue"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(484, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 16)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Surname"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(21, 288)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 16)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "Relationship to Client 1"
        '
        'beneficiaryForenames
        '
        Me.beneficiaryForenames.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryForenames.Location = New System.Drawing.Point(179, 83)
        Me.beneficiaryForenames.Name = "beneficiaryForenames"
        Me.beneficiaryForenames.Size = New System.Drawing.Size(289, 22)
        Me.beneficiaryForenames.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(80, 440)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(84, 16)
        Me.Label19.TabIndex = 87
        Me.Label19.Text = "At what age?"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 322)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 16)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Relationship to Client 2"
        '
        'atWhatAgeIssueLabel
        '
        Me.atWhatAgeIssueLabel.AutoSize = True
        Me.atWhatAgeIssueLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.atWhatAgeIssueLabel.Location = New System.Drawing.Point(54, 475)
        Me.atWhatAgeIssueLabel.Name = "atWhatAgeIssueLabel"
        Me.atWhatAgeIssueLabel.Size = New System.Drawing.Size(119, 16)
        Me.atWhatAgeIssueLabel.TabIndex = 88
        Me.atWhatAgeIssueLabel.Text = "At what age issue?"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(176, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 16)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "Forenames"
        '
        'beneficiaryTitle
        '
        Me.beneficiaryTitle.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryTitle.FormattingEnabled = True
        Me.beneficiaryTitle.Items.AddRange(New Object() {"Select...", "Mr", "Mrs", "Miss", "Ms"})
        Me.beneficiaryTitle.Location = New System.Drawing.Point(51, 81)
        Me.beneficiaryTitle.Name = "beneficiaryTitle"
        Me.beneficiaryTitle.Size = New System.Drawing.Size(97, 24)
        Me.beneficiaryTitle.TabIndex = 0
        '
        'atWhatAge
        '
        Me.atWhatAge.FormattingEnabled = True
        Me.atWhatAge.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.atWhatAge.Location = New System.Drawing.Point(179, 439)
        Me.atWhatAge.Name = "atWhatAge"
        Me.atWhatAge.Size = New System.Drawing.Size(63, 21)
        Me.atWhatAge.TabIndex = 12
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(48, 62)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(34, 16)
        Me.Label50.TabIndex = 45
        Me.Label50.Text = "Title"
        '
        'atWhatAgeIssue
        '
        Me.atWhatAgeIssue.FormattingEnabled = True
        Me.atWhatAgeIssue.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.atWhatAgeIssue.Location = New System.Drawing.Point(179, 475)
        Me.atWhatAgeIssue.Name = "atWhatAgeIssue"
        Me.atWhatAgeIssue.Size = New System.Drawing.Size(63, 21)
        Me.atWhatAgeIssue.TabIndex = 89
        '
        'relationToClient1
        '
        Me.relationToClient1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.relationToClient1.FormattingEnabled = True
        Me.relationToClient1.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.relationToClient1.Location = New System.Drawing.Point(179, 285)
        Me.relationToClient1.Name = "relationToClient1"
        Me.relationToClient1.Size = New System.Drawing.Size(204, 24)
        Me.relationToClient1.TabIndex = 92
        '
        'relationToClient2
        '
        Me.relationToClient2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.relationToClient2.FormattingEnabled = True
        Me.relationToClient2.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.relationToClient2.Location = New System.Drawing.Point(179, 319)
        Me.relationToClient2.Name = "relationToClient2"
        Me.relationToClient2.Size = New System.Drawing.Size(204, 24)
        Me.relationToClient2.TabIndex = 93
        '
        'ppt_beneficiary_add
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(752, 546)
        Me.Controls.Add(Me.relationToClient2)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.relationToClient1)
        Me.Controls.Add(Me.percentage)
        Me.Controls.Add(Me.atWhatAgeIssue)
        Me.Controls.Add(Me.Label50)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.atWhatAge)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.beneficiaryTitle)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.beneficiaryForenames)
        Me.Controls.Add(Me.atWhatAgeIssueLabel)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.beneficiaryPostcode)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.beneficiaryAddress)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.RadioButton7)
        Me.Controls.Add(Me.beneficiarySurname)
        Me.Controls.Add(Me.RadioButton8)
        Me.Name = "ppt_beneficiary_add"
        Me.Text = "Add A PPT Beneficiary"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button3 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents percentage As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents beneficiaryPostcode As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents beneficiaryAddress As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents beneficiarySurname As TextBox
    Friend WithEvents RadioButton8 As RadioButton
    Friend WithEvents RadioButton7 As RadioButton
    Friend WithEvents Label5 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents beneficiaryForenames As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents atWhatAgeIssueLabel As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents beneficiaryTitle As ComboBox
    Friend WithEvents atWhatAge As ComboBox
    Friend WithEvents Label50 As Label
    Friend WithEvents atWhatAgeIssue As ComboBox
    Friend WithEvents relationToClient1 As ComboBox
    Friend WithEvents relationToClient2 As ComboBox
End Class
