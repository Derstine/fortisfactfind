﻿Public Class probate_review
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        spy("Probate screen for " + whoDied + " screen opened")

        'this make take a while so show wait cursor
        Me.Cursor = Cursors.WaitCursor

        Me.Refresh()
        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        'reset cursor
        Me.Cursor = Cursors.Default

    End Sub

    Function setDefaults()

        RadioButton3.Checked = True
        RadioButton2.Checked = True
        RadioButton6.Checked = True

        Return True
    End Function

    Function populateScreen()

        Dim sql As String
        Dim results As Array

        'we know from whoDied global variable who the client is
        sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)

        If whoDied = "client1" Then TextBox1.Text = results(14) + " " + results(13)
        If whoDied = "client2" Then TextBox1.Text = results(25) + " " + results(24)

        sql = "select * from probate_control where willClientSequence=" + clientID + " and whichClient='" + whoDied + "'"
        results = runSQLwithArray(sql)

        If results(0) <> "ERROR" Then
            deathAddress.Text = results(3)
            deathPostcode.Text = results(4)
            If results(5) = "Y" Then RadioButton1.Checked = True
            If results(5) = "N" Then RadioButton2.Checked = True
            deathDate.Value = results(6)
            If results(7) = "Y" Then RadioButton4.Checked = True
            If results(7) = "N" Then RadioButton3.Checked = True
            willDate.Value = results(8)
            Me.TextBox20.Text = results(9)
            Me.assetValue.Text = results(10)
            Me.liabilitiesValue.Text = results(11)
            Me.netAssetValue.Text = results(12)
            Me.probateFee.Text = results(13)
            Me.probateCost.Text = results(14)
        End If

        ''''''''''''''
        'surviving relatives
        ''''''''''''''
        redrawSurvivorsGrid()

        ''''''''''''''
        'executor
        ''''''''''''''
        redrawExecutorsGrid()

        ''''''''''''''
        'professional advisers
        ''''''''''''''
        redrawAdvisersGrid()

        ''''''''''''''
        'assets list
        ''''''''''''''
        redrawAssetsGrid()

        ''''''''''''''
        'liabilities
        ''''''''''''''
        redrawLiabilitiesGrid()

        ''''''''''''''
        'utilities
        ''''''''''''''
        redrawUtilitiesGrid()

        ''''''''''''''
        'beneficiaries
        ''''''''''''''
        redrawBeneficiariesGrid()

        ''''''''''''''
        'gifts during lifetime
        ''''''''''''''
        redrawGiftsGrid()


        Return True
    End Function



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        '  NEED TO SAVE THE DATA 
        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        Dim sql As String

        'this might take a while, so show the wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.Enabled = False

        ''''''''''''''
        'the dead person
        ''''''''''''''
        Dim whichClient = "Select..."
        Dim deathAddress As String = Me.deathAddress.Text
        Dim deathPostcode As String = Me.deathPostcode.Text
        Dim nursingHomeFlag = ""
        Dim willFlag = ""
        Dim giftsGivenFlag = ""

        Dim attendanceNotes As String = Me.TextBox20.Text
        Dim assetValue As String = Me.assetValue.text
        Dim liabilitiesValue As String = Me.liabilitiesValue.Text
        Dim netAssetValue As String = Me.netAssetValue.Text
        Dim probateFee As String = Me.probateFee.Text
        Dim probateCost As String = Me.probateCost.Text

        sql = "delete From probate_control where willClientSequence=" + clientID + " and whichClient='" + SqlSafe(whoDied) + "'"
        runSQL(sql)

        If Me.RadioButton1.Checked = True Then nursingHomeFlag = "Y"
        If Me.RadioButton2.Checked = True Then nursingHomeFlag = "N"

        If Me.RadioButton4.Checked = True Then willFlag = "Y"
        If Me.RadioButton3.Checked = True Then willFlag = "N"

        If Me.RadioButton5.Checked = True Then giftsGivenFlag = "Y"
        If Me.RadioButton6.Checked = True Then giftsGivenFlag = "N"

        sql = "insert into probate_control (willClientSequence,whichClient,deathAddress,deathPostcode,nursingHomeFlag,deathDate,willFlag,willDate,attendanceNotes,assetValue,liabilitiesValue,netAssetValue,probateFee,probateCost,giftsGivenFlag) values (" + clientID + ",'" + SqlSafe(whoDied) + "','" + SqlSafe(deathAddress) + "','" + SqlSafe(deathPostcode) + "','" + SqlSafe(nursingHomeFlag) + "','" + SqlSafe(deathDate.Value) + "','" + SqlSafe(willFlag) + "','" + SqlSafe(willDate.Value) + "','" + SqlSafe(TextBox20.Text) + "','" + SqlSafe(assetValue) + "','" + SqlSafe(liabilitiesValue) + "','" + SqlSafe(netAssetValue) + "','" + SqlSafe(probateFee) + "','" + SqlSafe(probateCost) + "','" + SqlSafe(giftsGivenFlag) + "')"
        runSQL(sql)



        'reset cursor
        Me.Enabled = True
        Me.Cursor = Cursors.Default

        spy("Probate screen for " + whoDied + " screen closed")

        Me.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        deathPostcode.Text = deathPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = deathPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.deathAddress.Text = f.FoundAddress()
            Me.deathPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            willDate.Visible = False
            Label18.Visible = False
        Else
            willDate.Visible = True
            Label18.Visible = True
        End If
    End Sub

    Private Sub RadioButton6_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton6.CheckedChanged
        If RadioButton6.Checked = True Then
            Button10.Visible = False
            DataGridView8.Visible = False
        Else
            Button10.Visible = True
            DataGridView8.Visible = True
        End If
    End Sub

    Function redrawSurvivorsGrid()
        Dim sql = ""
        DataGridView1.Rows.Clear()

        Dim name = ""
        Dim relation = ""
        Dim address = ""
        Dim dependant = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_relative where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by forenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            name = ""
            relation = ""
            address = ""
            dependant = ""

            name = dbreader.GetValue(4).ToString + " " + dbreader.GetValue(5).ToString.ToUpper
            address = dbreader.GetValue(6).ToString + " " + dbreader.GetValue(7).ToString.ToUpper
            relation = dbreader.GetValue(10).ToString
            dependant = dbreader.GetValue(11)
            DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, name, address, relation, dependant, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawExecutorsGrid()
        Dim sql = ""
        DataGridView5.Rows.Clear()

        Dim name = ""
        Dim relation = ""
        Dim address = ""
        Dim executorType = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_executors where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by forenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            name = ""
            relation = ""
            address = ""
            executorType = ""

            name = dbreader.GetValue(4).ToString + " " + dbreader.GetValue(5).ToString.ToUpper
            address = dbreader.GetValue(6).ToString + " " + dbreader.GetValue(7).ToString.ToUpper
            relation = dbreader.GetValue(10).ToString
            executorType = dbreader.GetValue(11)
            DataGridView5.Rows.Add(dbreader.GetValue(0).ToString, name, address, executorType, relation, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawAdvisersGrid()
        Dim sql = ""
        DataGridView2.Rows.Clear()

        Dim name = ""
        Dim relation = ""
        Dim address = ""
        Dim adviserType = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_advisers where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by forenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            name = ""
            relation = ""
            address = ""
            adviserType = ""

            name = dbreader.GetValue(4).ToString + " " + dbreader.GetValue(5).ToString.ToUpper
            address = dbreader.GetValue(6).ToString + " " + dbreader.GetValue(7).ToString.ToUpper
            relation = dbreader.GetValue(10).ToString
            adviserType = dbreader.GetValue(11)
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, name, address, adviserType, relation, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawAssetsGrid()
        Dim sql = ""
        DataGridView3.Rows.Clear()

        Dim assetType = ""
        Dim details = ""
        Dim worth = ""

        Dim probatePCT As Double
        Dim totalWorth As Double = 0

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_assets where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by assetType,details ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            assetType = ""
            details = ""
            worth = ""

            assetType = dbreader.GetValue(5).ToString
            details = dbreader.GetValue(3).ToString
            worth = dbreader.GetValue(4)
            totalWorth = totalWorth + Val(worth)
            DataGridView3.Rows.Add(dbreader.GetValue(0).ToString, assetType, details, worth, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Me.assetValue.Text = totalWorth.ToString

        Me.netAssetValue.Text = (Val(Me.assetValue.Text) - Val(Me.liabilitiesValue.Text)).ToString
        If Me.probateFee.Text = "" Then
            probatePct = 0
        Else
            probatePct = Val(probateFee.Text) / 100
        End If
        Me.probateCost.Text = (Val(Me.assetValue.Text) * probatePct).ToString("N2")

        Return True
    End Function

    Function redrawLiabilitiesGrid()
        Dim sql = ""
        DataGridView4.Rows.Clear()

        Dim liabilityType = ""
        Dim details = ""
        Dim worth = ""
        Dim probatePct As Double = 0
        Dim totalWorth As Double = 0

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_liabilities where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by liabilityType,details ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            liabilityType = ""
            details = ""
            worth = ""

            liabilityType = dbreader.GetValue(5).ToString
            details = dbreader.GetValue(3).ToString
            worth = dbreader.GetValue(4)
            totalWorth = totalWorth + Val(worth)

            DataGridView4.Rows.Add(dbreader.GetValue(0).ToString, liabilityType, details, worth, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Me.liabilitiesValue.Text = totalWorth.ToString
        Me.netAssetValue.Text = (Val(Me.assetValue.Text) - Val(Me.liabilitiesValue.Text)).ToString
        If Me.probateFee.Text = "" Then
            probatePct = 0
        Else
            probatePct = Val(probateFee.Text) / 100
        End If
        Me.probateCost.Text = (Val(Me.assetValue.Text) * probatePct).ToString("N2")

        Return True


    End Function


    Function redrawUtilitiesGrid()
        Dim sql = ""
        DataGridView6.Rows.Clear()

        Dim utilityType = ""
        Dim provider = ""
        Dim providerAddress = ""
        Dim accountNumber = ""
        Dim meterReading = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_utilities where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by utilityType,provider ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            utilityType = ""
            provider = ""
            accountNumber = ""
            meterReading = ""

            utilityType = dbreader.GetValue(3).ToString
            provider = dbreader.GetValue(4).ToString
            accountNumber = dbreader.GetValue(9).ToString
            meterReading = dbreader.GetValue(10)


            DataGridView6.Rows.Add(dbreader.GetValue(0).ToString, utilityType, provider, accountNumber, meterReading, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawBeneficiariesGrid()
        Dim sql = ""
        DataGridView7.Rows.Clear()

        Dim name = ""
        Dim relation = ""
        Dim address = ""
        Dim item = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_beneficiaries where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by forenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            name = ""
            relation = ""
            address = ""
            item = ""

            name = dbreader.GetValue(4).ToString + " " + dbreader.GetValue(5).ToString.ToUpper
            address = dbreader.GetValue(6).ToString + " " + dbreader.GetValue(7).ToString.ToUpper
            relation = dbreader.GetValue(10).ToString
            item = dbreader.GetValue(11)
            DataGridView7.Rows.Add(dbreader.GetValue(0).ToString, name, address, relation, item, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Function redrawGiftsGrid()
        Dim sql = ""
        DataGridView8.Rows.Clear()

        Dim name = ""
        Dim relation = ""
        Dim address = ""
        Dim item = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select * from probate_gifts where willClientSequence=" + clientID + " and whichClient='" + whoDied + "' order by forenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            name = ""
            relation = ""
            address = ""
            item = ""

            name = dbreader.GetValue(4).ToString + " " + dbreader.GetValue(5).ToString.ToUpper
            address = dbreader.GetValue(6).ToString + " " + dbreader.GetValue(7).ToString.ToUpper
            relation = dbreader.GetValue(10).ToString
            item = dbreader.GetValue(11)
            DataGridView8.Rows.Add(dbreader.GetValue(0).ToString, name, address, relation, item, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True
    End Function

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim sql As String
        Dim results As Array
        sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then

            If whoDied = "client1" Then
                Me.deathPostcode.Text = results(67)
                Me.deathAddress.Text = results(16)
            End If

            If whoDied = "client2" Then
                Me.deathPostcode.Text = results(68)
                Me.deathAddress.Text = results(27)
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        probate_relative_add.ShowDialog(Me)
        redrawSurvivorsGrid()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 5 And DataGridView1.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView1.Item(0, e.RowIndex).Value.ToString
            probate_relative_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawSurvivorsGrid()
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        probate_executor_add.ShowDialog(Me)
        redrawExecutorsGrid()
    End Sub

    Private Sub DataGridView5_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView5.CellContentClick
        If e.ColumnIndex = 5 And DataGridView5.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView5.Item(0, e.RowIndex).Value.ToString
            probate_executor_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawExecutorsGrid()
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        probate_adviser_add.ShowDialog(Me)
        redrawAdvisersGrid()
    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        If e.ColumnIndex = 5 And DataGridView2.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView2.Item(0, e.RowIndex).Value.ToString
            probate_adviser_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawAdvisersGrid()
        End If
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        probate_beneficiaries_add.ShowDialog(Me)
        redrawBeneficiariesGrid()
    End Sub

    Private Sub DataGridView7_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView7.CellContentClick
        If e.ColumnIndex = 5 And DataGridView7.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView7.Item(0, e.RowIndex).Value.ToString
            probate_beneficiaries_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawBeneficiariesGrid()
        End If
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        probate_gifts_add.ShowDialog(Me)
        redrawGiftsGrid()
    End Sub

    Private Sub DataGridView8_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView8.CellContentClick
        If e.ColumnIndex = 5 And DataGridView8.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView8.Item(0, e.RowIndex).Value.ToString
            probate_gifts_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawGiftsGrid()
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        probate_asset_add.ShowDialog(Me)
        redrawAssetsGrid()
    End Sub

    Private Sub DataGridView3_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView3.CellContentClick
        If e.ColumnIndex = 4 And DataGridView3.Item(4, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView3.Item(0, e.RowIndex).Value.ToString
            probate_asset_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawAssetsGrid()
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        probate_liability_add.ShowDialog(Me)
        redrawLiabilitiesGrid()
    End Sub

    Private Sub DataGridView4_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView4.CellContentClick
        If e.ColumnIndex = 4 And DataGridView4.Item(4, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView4.Item(0, e.RowIndex).Value.ToString
            probate_liability_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawLiabilitiesGrid()
        End If
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        probate_utility_add.ShowDialog(Me)
        redrawUtilitiesGrid()
    End Sub

    Private Sub DataGridView6_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView6.CellContentClick
        If e.ColumnIndex = 5 And DataGridView6.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView6.Item(0, e.RowIndex).Value.ToString
            probate_utility_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawUtilitiesGrid()
        End If
    End Sub

    Private Sub probateFee_TextChanged(sender As Object, e As EventArgs) Handles probateFee.TextChanged
        Dim probatePct As Double = 0

        If Me.probateFee.Text = "" Then
            probatePct = 0
        Else
            probatePct = Val(probateFee.Text) / 100
        End If
        Me.probateCost.Text = (Val(Me.assetValue.Text) * probatePct).ToString("N2")
    End Sub
End Class