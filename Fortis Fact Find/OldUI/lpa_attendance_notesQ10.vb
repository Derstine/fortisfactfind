﻿Public Class lpa_attendance_notesQ10
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs)

        If Me.RadioButton2.Checked = True Then
            ' Me.TextBox1.Enabled = False
        Else
            '  Me.TextBox1.Enabled = True
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'check to see all sections filled in
        Dim errorCount = 0
        Dim errorList = ""

        If Me.RadioButton1.Checked = False And Me.RadioButton2.Checked = False And Me.RadioButton3.Checked = False And Me.RadioButton4.Checked = False Then
            errorCount = errorCount + 1
            errorList = errorList + "You must answer the question" + vbCrLf
        End If

        If errorCount > 0 Then
            MsgBox("You have the following error(s):" + vbCrLf + vbCrLf + errorList, MsgBoxStyle.Exclamation)
        Else

            'save data
            Dim tick = "0"


            If Me.RadioButton1.Checked = True Then tick = "1"
            If Me.RadioButton2.Checked = True Then tick = "2"
            If Me.RadioButton3.Checked = True Then tick = "3"
            If Me.RadioButton4.Checked = True Then tick = "4"


            Dim sql As String = "update lpa_attendance_notes set q10='', q10tick=" + tick + " where willClientSequence=" + clientID
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub will_attendance_notesQ1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If factfind_lpa.attendanceMode.Text = "edit" Then
            Dim sql As String = "select q10,q10tick from lpa_attendance_notes  where willClientSequence=" + clientID
            Dim results As Array = runSQLwithArray(sql)

            If results(1) = "1" Then Me.RadioButton1.Checked = True
            If results(1) = "2" Then Me.RadioButton2.Checked = True
            If results(1) = "3" Then Me.RadioButton3.Checked = True
            If results(1) = "4" Then Me.RadioButton4.Checked = True

        End If

    End Sub
End Class