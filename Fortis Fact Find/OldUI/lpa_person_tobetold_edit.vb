﻿Public Class lpa_person_tobetold_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.personTitle.SelectedIndex = 0
        Me.personRelationshipClient1.SelectedIndex = 0
        Me.personRelationshipClient2.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        'load data
        Dim sql As String = "select * from lpa_persons_detail where sequence=" + rowID
        Dim results As Array
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            Me.personTitle.SelectedItem = results(11)
            Me.personNames.Text = results(3)
            Me.personPhone.Text = results(4)
            Me.personRelationshipClient1.SelectedItem = results(5)
            Me.personRelationshipClient2.SelectedItem = results(6)
            Me.personAddress.Text = results(7)
            Me.personEmail.Text = results(8)

            Me.personSurname.Text = results(12)
            Me.personPostcode.Text = results(13)
        End If

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.personRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.personRelationshipClient2.Visible = False
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If personTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the person's title" + vbCrLf
        If personNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the person's name" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update lpa_persons_to_be_told set personTitle='" + SqlSafe(personTitle.SelectedItem) + "',personNames='" + SqlSafe(personNames.Text) + "', personSurname='" + SqlSafe(personSurname.Text) + "', personRelationshipClient1='" + SqlSafe(personRelationshipClient1.SelectedItem) + "', personRelationshipClient2='" + SqlSafe(personRelationshipClient1.SelectedItem) + "',personAddress='" + SqlSafe(personAddress.Text) + "',personPostcode='" + SqlSafe(personPostcode.Text) + "',personEmail='" + SqlSafe(personEmail.Text) + "',personPhone='" + SqlSafe(personPhone.Text) + "'  where sequence=" + rowID
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        personPostcode.Text = personPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = personPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.personAddress.Text = f.FoundAddress()
            Me.personPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this person?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from lpa_persons_to_be_told where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class