﻿Public Class probate_asset_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        Me.assetType.SelectedIndex = 0

        'load data
        Dim sql As String = "select * from probate_assets where sequence=" + rowID
        Dim results As Array
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            assetType.SelectedItem = results(5)
            Me.details.Text = results(3)
            Me.worth.Text = results(5)
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If assetType.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset category" + vbCrLf
        If details.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the details of the asset" + vbCrLf
        If worth.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the value of the asset" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update probate_assets set assetType='" + SqlSafe(assetType.SelectedItem) + "',details='" + SqlSafe(details.Text) + "',worth='" + SqlSafe(worth.Text) + "' where sequence=" + rowID
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this asset?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from probate_assets where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class