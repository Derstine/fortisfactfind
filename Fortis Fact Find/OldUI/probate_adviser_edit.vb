﻿Public Class probate_adviser_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.title.SelectedIndex = 0
        Me.relationship.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName(whoDied)

        'load data
        Dim sql As String = "select * from probate_advisers where sequence=" + rowID
        Dim results As Array = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            Me.title.SelectedItem = results(3)
            Me.forenames.Text = results(4)
            Me.surname.Text = results(5)
            Me.address.Text = results(6)
            Me.postcode.Text = results(7)
            Me.phone.Text = results(8)
            Me.email.Text = results(9)
            Me.relationship.SelectedItem = results(10)
            Me.adviserType.Text = results(11)
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If forenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the adviser's name" + vbCrLf
        If adviserType.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the adviser's role" + vbCrLf
        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update probate_advisers set title='" + SqlSafe(title.SelectedItem) + "',forenames='" + SqlSafe(forenames.Text) + "',surname='" + SqlSafe(surname.Text) + "',relationship='" + SqlSafe(relationship.SelectedItem) + "',address='" + SqlSafe(address.Text) + "',postcode='" + SqlSafe(postcode.Text) + "',executorType='" + SqlSafe(adviserType.Text) + "',phone='" + SqlSafe(phone.Text) + "',email='" + SqlSafe(email.Text) + "' where sequence=" + rowID
            runSQL(sql)
            Me.Close()

        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        postcode.Text = postcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = postcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.address.Text = f.FoundAddress()
            Me.postcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this adviser?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from probate_advisers where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class