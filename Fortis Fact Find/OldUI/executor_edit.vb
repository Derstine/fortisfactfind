﻿Public Class executor_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Label12.Visible = False
        executorType.Visible = False
        Me.executorTitle.SelectedIndex = 0
        Me.executorRelationshipClient1.SelectedIndex = 0
        Me.executorRelationshipClient2.SelectedIndex = 0
        Me.trusteeFlag.SelectedIndex = 0
        Me.executorType.SelectedIndex = 0


        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        'get current data
        Dim sql As String = "select * from will_instruction_stage3_data_detail where sequence=" + rowID
        Dim results As Array = runSQLwithArray(sql)

        If results(11) = "Mr" Then executorTitle.SelectedIndex = 1
        If results(11) = "Mrs" Then executorTitle.SelectedIndex = 2
        If results(11) = "Miss" Then executorTitle.SelectedIndex = 3
        If results(11) = "Ms" Then executorTitle.SelectedIndex = 4

        executorRelationshipClient1.SelectedItem = results(5)
        executorRelationshipClient2.SelectedItem = results(6)

        executorNames.Text = results(3)
        executorSurname.Text = results(12)
        executorPostcode.Text = results(13)
        executorAddress.Text = results(7)

        executorPhone.Text = results(4)
        executorEmail.Text = results(8)

        trusteeFlag.SelectedItem = results(14)
        executorType.SelectedItem = results(2)

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.executorRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.executorRelationshipClient2.Visible = False
        End If


    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If executorTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the executor's title" + vbCrLf
        If executorNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the executor's name" + vbCrLf
        If trusteeFlag.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the executor's role" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update will_instruction_stage3_data_detail set executorTitle='" + SqlSafe(executorTitle.SelectedItem) + "',executorNames='" + SqlSafe(executorNames.Text) + "',executorSurname='" + SqlSafe(executorSurname.Text) + "',executorRelationshipClient1='" + SqlSafe(executorRelationshipClient1.SelectedItem) + "',executorRelationshipClient2='" + SqlSafe(executorRelationshipClient2.SelectedItem) + "',executorAddress='" + SqlSafe(executorAddress.Text) + "',executorPostcode='" + SqlSafe(executorPostcode.Text) + "',executorType='" + SqlSafe(executorType.SelectedItem) + "',trusteeFlag ='" + SqlSafe(trusteeFlag.SelectedItem) + "',executorPhone='" + SqlSafe(executorPhone.Text) + "',executorEmail='" + SqlSafe(executorEmail.Text) + "' where sequence=" + rowID
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        executorPostcode.Text = executorPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = executorPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.executorAddress.Text = f.FoundAddress()
            Me.executorPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person1Postcode,person1Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        executorPostcode.Text = results(0)
        executorAddress.Text = results(1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person2Postcode,person2Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        executorPostcode.Text = results(0)
        executorAddress.Text = results(1)
    End Sub



    Private Sub trusteeFlag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles trusteeFlag.SelectedIndexChanged
        If trusteeFlag.SelectedIndex = 1 Or trusteeFlag.SelectedIndex = 2 Then
            Label12.Visible = True
            executorType.Visible = True
        Else
            Label12.Visible = False
            executorType.Visible = False
        End If
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this executor?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from will_instruction_stage3_data_detail where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class