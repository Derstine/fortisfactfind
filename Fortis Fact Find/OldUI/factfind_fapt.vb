﻿Public Class factfind_fapt
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        spy("FAPT Fact Find screen opened")

        'this make take a while so show wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.firstRunComplete.Text = "N"
        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        'reset cursor
        Me.Cursor = Cursors.Default
        Me.firstRunComplete.Text = "Y"

    End Sub

    Private Function setDefaults()
        RadioButton2.Checked = True   'beneficiaries
        RadioButton4.Checked = True   'settlors

        CheckBox1.Checked = False
        CheckBox2.Checked = False
        CheckBox3.Checked = False
        CheckBox4.Checked = False
        CheckBox5.Checked = False
        CheckBox6.Checked = False
        CheckBox7.Checked = False
        CheckBox8.Checked = False
        CheckBox9.Checked = False
        CheckBox10.Checked = False

        DataGridView1.Rows.Clear()
        DataGridView2.Rows.Clear()
        DataGridView3.Rows.Clear()
        DataGridView4.Rows.Clear()
        DataGridView5.Rows.Clear()
        DataGridView6.Rows.Clear()
        DataGridView7.Rows.Clear()

        GroupBox2.Visible = False

        Return True
    End Function

    Private Function populateScreen()

        If clientType() = "mirror" Then GroupBox2.Visible = True Else GroupBox2.Visible = False

        'client details tab
        Dim result = ""
        Dim results As Array
        Dim sql As String

        'personal details
        sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            TextBox2.Text = results(12) + " " + results(14) + " " + results(13)
            TextBox4.Text = results(16)
            TextBox5.Text = results(67)
            If results(18).ToString <> "" Then
                TextBox3.Text = results(18).ToString.Substring(0, 10)
            Else
                TextBox3.Text = ""
            End If

            TextBox9.Text = results(23) + " " + results(25) + " " + results(24)
            If results(29).ToString <> "" Then
                TextBox8.Text = results(29).ToString.Substring(0, 10)
            Else
                TextBox8.Text = ""
            End If
            TextBox7.Text = results(27)
            TextBox6.Text = results(68)

        End If

        ''''''''''''''
        'trustees
        ''''''''''''''
        sql = "select * from fapt_trustees where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            If results(2) = "" Then RadioButton4.Checked = True
            If results(2) = "spouse" Then RadioButton3.Checked = True
            If results(2) = "spousejoint" Then RadioButton5.Checked = True
            If results(2) = "joint" Then RadioButton6.Checked = True
            If results(2) = "PTC" Then RadioButton7.Checked = True
        Else
            RadioButton4.Checked = True
        End If
        redrawTrusteeGrid()

        ''''''''''''''
        'beneficiaries
        ''''''''''''''
        '''
        DataGridView4.Columns(2).HeaderText = "Relationship to " + getClientFirstName("client1")
        If clientType() = "mirror" Then DataGridView4.Columns(3).HeaderText = "Relationship to " + getClientFirstName("client2") Else DataGridView4.Columns(3).HeaderText = "n/a"


        sql = "select * from fapt_beneficiaries where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            If results(2) = "N" Then RadioButton2.Checked = True
            If results(2) = "Y" Then RadioButton1.Checked = True
            Dim charityName = ""
            Dim charityNumber = ""

            If results(3).ToString.Contains("[") Then
                'there is a charity reg number we need to extract
                charityName = results(3)
                charityName = Trim(charityName.Substring(0, InStr(charityName, "[") - 1))
                charityNumber = results(3).ToString.Replace(charityName, "")
                charityNumber = charityNumber.ToString.Replace("[", "")
                charityNumber = charityNumber.ToString.Replace("]", "")
                charityNumber = Trim(charityNumber)

                Me.charityName.Text = charityName
                Me.charityRegNumber.Text = charityNumber

            Else
                'there is no charity reg number
                Me.charityName.Text = results(3)
                Me.charityRegNumber.Text = ""
            End If


            Me.TextBox1.Text = results(3)
        End If
        redrawBeneficiaryGrid()

        ''''''''''''''
        'Assets
        ''''''''''''''
        redrawAssetsGrid()


        ''''''''''''''
        'Purpose
        ''''''''''''''

        sql = "select * from fapt_purpose where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            'we have data
            If Val(results(2)) = 1 Then Me.CheckBox1.Checked = True
            If Val(results(3)) = 1 Then Me.CheckBox2.Checked = True
            If Val(results(4)) = 1 Then Me.CheckBox3.Checked = True
            If Val(results(5)) = 1 Then Me.CheckBox4.Checked = True
            If Val(results(6)) = 1 Then Me.CheckBox5.Checked = True
            If Val(results(7)) = 1 Then Me.CheckBox6.Checked = True
            If Val(results(8)) = 1 Then Me.CheckBox7.Checked = True
            If Val(results(9)) = 1 Then Me.CheckBox8.Checked = True
            If Val(results(10)) = 1 Then Me.CheckBox9.Checked = True
            If Val(results(11)) = 1 Then Me.CheckBox10.Checked = True

        Else
            'no data, do nothing
        End If

        ''''''''''''''
        'Compliance
        ''''''''''''''

        sql = "select willClientSequence from fapt_compliance where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            'do nothing
        Else
            'no data so therefore insert row
            sql = "insert into fapt_compliance (willClientSequence) values (" + clientID + ")"
            runSQL(sql)
        End If

        redrawFAPTComplianceGrid()

        ''''''''''''''
        'Attendance Notes
        ''''''''''''''
        sql = "select willClientSequence from fapt_attendance_notes where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            'do nothing
        Else
            'no data so therefore insert row
            sql = "insert into fapt_attendance_notes (willClientSequence) values (" + clientID + ")"
            runSQL(sql)
        End If
        redrawFAPTAttendanceGrid()

        Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        '  NEED TO SAVE THE DATA 
        ''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''
        Dim sql As String

        'this might take a while, so show the wait cursor
        Me.Cursor = Cursors.WaitCursor
        Me.Enabled = False

        ''''''''''''''
        'trustees
        ''''''''''''''
        sql = "delete from fapt_trustees where willClientSequence=" + clientID
        runSQL(sql)

        Dim attorneyType = ""
        If RadioButton4.Checked = True Then attorneyType = ""
        If RadioButton3.Checked = True Then attorneyType = "spouse"
        If RadioButton5.Checked = True Then attorneyType = "spousejoint"
        If RadioButton6.Checked = True Then attorneyType = "joint"
        If RadioButton7.Checked = True Then attorneyType = "PTC"

        sql = "insert into fapt_trustees (willClientSequence,attorneyType) values (" + clientID + ",'" + SqlSafe(attorneyType) + "')"
        runSQL(sql)

        ''''''''''''''
        'beneficiaries
        ''''''''''''''
        sql = "delete from fapt_beneficiaries where willClientSequence=" + clientID
        runSQL(sql)

        Dim charityBackstop = ""
        Dim charityDetails = ""

        If RadioButton1.Checked = True Then charityBackstop = "Y"
        If RadioButton2.Checked = True Then charityBackstop = "N"

        If charityBackstop = "Y" Then
            charityDetails = Me.charityName.Text
            If Me.charityRegNumber.Text <> "" Then charityDetails = charityDetails + " [" + Me.charityRegNumber.Text + "]"
        Else
            charityDetails = ""
        End If

        sql = "insert into fapt_beneficiaries (willClientSequence,charityBackstop,charityDetails) values (" + clientID + ",'" + SqlSafe(charityBackstop) + "','" + SqlSafe(charityDetails) + "')"
        runSQL(sql)

        ''''''''''''''
        'Purpose
        ''''''''''''''
        sql = "delete from fapt_purpose where willClientSequence=" + clientID
        runSQL(sql)

        Dim q1 = 0
        Dim q2 = 0
        Dim q3 = 0
        Dim q4 = 0
        Dim q5 = 0
        Dim q6 = 0
        Dim q7 = 0
        Dim q8 = 0
        Dim q9 = 0
        Dim q10 = 0

        If Me.CheckBox1.Checked = True Then q1 = 1 Else q1 = 0
        If Me.CheckBox2.Checked = True Then q2 = 1 Else q2 = 0
        If Me.CheckBox3.Checked = True Then q3 = 1 Else q3 = 0
        If Me.CheckBox4.Checked = True Then q4 = 1 Else q4 = 0
        If Me.CheckBox5.Checked = True Then q5 = 1 Else q5 = 0
        If Me.CheckBox6.Checked = True Then q6 = 1 Else q6 = 0
        If Me.CheckBox7.Checked = True Then q7 = 1 Else q7 = 0
        If Me.CheckBox8.Checked = True Then q8 = 1 Else q8 = 0
        If Me.CheckBox9.Checked = True Then q9 = 1 Else q9 = 0
        If Me.CheckBox10.Checked = True Then q10 = 1 Else q10 = 0

        sql = "insert into fapt_purpose (willClientSequence,q1,q2,q3,q4,q5,q6,q7,q8,q9,q10) values (" + clientID + ",'" + q1.ToString + "','" + q2.ToString + "','" + q3.ToString + "','" + q4.ToString + "','" + q5.ToString + "','" + q6.ToString + "','" + q7.ToString + "','" + q8.ToString + "','" + q9.ToString + "','" + q10.ToString + "')"
        runSQL(sql)


        ''''''''''''''
        'attendance notes
        ''''''''''''''
        sql = "select count(sequence) from fapt_attendance_notes where willClientSequence=" + clientID
        If runSQLwithID(sql) <> "0" And runSQLwithID(sql) <> "ERROR" Then
            'we have a row, so we need to update
            sql = "update fapt_attendance_notes set notes='" + SqlSafe(Me.TextBox20.Text) + "' where willClientSequence=" + clientID
            runSQL(sql)
        End If

        If runSQLwithID(sql) = "0" Then
            'we have no row, so we need to insert
            sql = "insert into fapt_attendance_notes (willClientSequence,notes) values (" + clientID + ",'" + SqlSafe(Me.TextBox20.Text) + "')"
            runSQL(sql)
        End If


        'reset cursor
        Me.Enabled = True
        Me.Cursor = Cursors.Default

        spy("FAPT Fact Find screen closed")

        Me.Close()
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then TextBox1.Enabled = False : Panel1.Visible = False
        If RadioButton1.Checked = True Then TextBox1.Enabled = True : Panel1.Visible = True
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton2.Checked = True Then TextBox1.Enabled = False : Panel1.Visible = False
        If RadioButton1.Checked = True Then TextBox1.Enabled = True : Panel1.Visible = True
    End Sub

    Private Sub RadioButton6_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton6.CheckedChanged

    End Sub

    Private Sub RadioButton4_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton4.CheckedChanged
        If RadioButton4.Checked = True Then Button6.Visible = False
        If RadioButton4.Checked = True Then DataGridView5.Visible = False
        If RadioButton4.Checked = False Then Button6.Visible = True
        If RadioButton4.Checked = False Then DataGridView5.Visible = True
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then Button6.Visible = False
        If RadioButton3.Checked = False Then Button6.Visible = True
    End Sub


    Function redrawAssetsGrid()

        Dim sql = ""
        Dim client1 As Single = 0
        Dim client2 As Single = 0
        Dim joint As Single = 0

        Dim assettype = ""
        Dim details = ""
        Dim worth = ""
        Dim mortgage = ""

        Dim includeInTrust = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn

        'let's do property first
        DataGridView1.Rows.Clear()
        sql = "select * from fapt_trust_fund_property where willClientSequence=" + clientID
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            assettype = ""
            details = ""
            worth = ""
            mortgage = ""
            assettype = dbreader.GetValue(10).ToString
            details = dbreader.GetValue(11).ToString
            worth = dbreader.GetValue(3).ToString
            mortgage = dbreader.GetValue(6).ToString
            If dbreader.GetValue(12).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbreader.GetValue(12).ToString

            If mortgage = "Z" Then mortgage = "n/a"
            'add row       

            DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, worth, mortgage, includeInTrust, "edit", "fapt_trust_fund_property")

        End While
        dbreader.Close()

        'let's do chattels next
        DataGridView2.Rows.Clear()
        sql = "select * from fapt_trust_fund_chattels where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            assettype = ""
            details = ""
            worth = ""
            assettype = dbreader.GetValue(5).ToString
            details = dbreader.GetValue(2).ToString
            worth = dbreader.GetValue(3).ToString
            If dbreader.GetValue(6).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbreader.GetValue(6).ToString

            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, worth, includeInTrust, "edit", "fapt_trust_fund_chattels")
        End While

        dbreader.Close()

        ' let's do cash next
        DataGridView3.Rows.Clear()
        sql = "select * from fapt_trust_fund_cash where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader

        While dbreader.Read()
            assettype = ""
            details = ""
            worth = ""
            assettype = dbreader.GetValue(5).ToString
            details = dbreader.GetValue(2).ToString
            worth = dbreader.GetValue(3).ToString
            If dbreader.GetValue(7).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbreader.GetValue(7).ToString

            'add row       
            DataGridView3.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, worth, includeInTrust, "edit", "fapt_trust_fund_cash")
        End While
        dbreader.Close()

        ' finally, let's do "misc" next
        sql = "select * from fapt_trust_fund_further where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader

        While dbreader.Read()
            assettype = ""
            details = ""
            worth = ""
            assettype = dbreader.GetValue(5).ToString
            details = dbreader.GetValue(2).ToString
            worth = dbreader.GetValue(3).ToString
            If dbreader.GetValue(6).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbreader.GetValue(6).ToString

            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, worth, includeInTrust, "edit", "fapt_trust_fund_further")
        End While
        dbreader.Close()
        dbConn.Close()

        Return True

    End Function

    Private Sub DataGridView6_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView6.CellContentClick
        If e.ColumnIndex = 3 Then

            If DataGridView6.Item(3, e.RowIndex).Value = "edit" Then Me.attendanceMode.Text = "edit"
            If DataGridView6.Item(3, e.RowIndex).Value = "answer" Then Me.attendanceMode.Text = "answer"

            rowID = DataGridView6.Item(0, e.RowIndex).Value.ToString

            If rowID = 1 Then fapt_compliance_Q1.ShowDialog(Me)
            If rowID = 2 Then fapt_compliance_Q2.ShowDialog(Me)
            If rowID = 3 Then fapt_compliance_Q3.ShowDialog(Me)
            If rowID = 4 Then fapt_compliance_Q4.ShowDialog(Me)
            If rowID = 5 Then fapt_compliance_Q5.ShowDialog(Me)
            If rowID = 6 Then fapt_compliance_Q6.ShowDialog(Me)
            If rowID = 7 Then fapt_compliance_Q7.ShowDialog(Me)
            If rowID = 8 Then fapt_compliance_Q8.ShowDialog(Me)
            If rowID = 9 Then fapt_compliance_Q9.ShowDialog(Me)
            If rowID = 10 Then fapt_compliance_Q10.ShowDialog(Me)


            rowID = ""    'reset global variable
            redrawFAPTComplianceGrid()
        End If

        Me.attendanceMode.Text = "attendanceMode"    'reset

    End Sub

    Function redrawFAPTComplianceGrid()
        Dim i As Integer
        Dim answer As Array

        DataGridView6.Rows.Clear()

        For i = 1 To 10
            answer = getComplianceAnswer(i)
            DataGridView6.Rows.Add(i, "Q" + i.ToString, answer(0), answer(1))
        Next

        Return True
    End Function

    Function getComplianceAnswer(ByVal i As Integer)

        Dim returnvalue(2) As String

        Dim sql As String = "select q" + i.ToString + ",q" + i.ToString + "tick from fapt_compliance where willClientSequence=" + clientID
        Dim result As Array
        result = runSQLwithArray(sql)

        If result(1) = "" Then
            returnvalue(0) = "not answered"
            returnvalue(1) = "answer"
        Else

            If Val(result(1)) = 1 Then
                returnvalue(0) = "Yes"
            Else
                returnvalue(0) = "No"
            End If

            returnvalue(1) = "edit"
        End If


        Return returnvalue
    End Function

    Private Sub DataGridView7_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView7.CellContentClick
        If e.ColumnIndex = 3 Then

            If DataGridView7.Item(3, e.RowIndex).Value = "edit" Then Me.attendanceMode.Text = "edit"
            If DataGridView7.Item(3, e.RowIndex).Value = "answer" Then Me.attendanceMode.Text = "answer"

            rowID = DataGridView7.Item(0, e.RowIndex).Value.ToString

            If rowID = 1 Then fapt_attendancenotes_Q1.ShowDialog(Me)
            If rowID = 2 Then fapt_attendancenotes_Q2.ShowDialog(Me)
            If rowID = 3 Then fapt_attendancenotes_Q3.ShowDialog(Me)
            If rowID = 4 Then fapt_attendancenotes_Q4.ShowDialog(Me)
            If rowID = 5 Then fapt_attendancenotes_Q5.ShowDialog(Me)
            If rowID = 6 Then fapt_attendancenotes_Q6.ShowDialog(Me)
            If rowID = 7 Then fapt_attendancenotes_Q7.ShowDialog(Me)
            If rowID = 8 Then fapt_attendancenotes_Q8.ShowDialog(Me)
            If rowID = 9 Then fapt_attendancenotes_Q9.ShowDialog(Me)


            rowID = ""    'reset global variable
            redrawFAPTAttendanceGrid()
        End If

        Me.attendanceMode.Text = "attendanceMode"    'reset

    End Sub

    Function redrawFAPTAttendanceGrid()
        Dim i As Integer
        Dim answer As Array

        DataGridView7.Rows.Clear()

        For i = 1 To 9
            answer = getAttendanceAnswer(i)
            DataGridView7.Rows.Add(i, "Q" + i.ToString, answer(0), answer(1))

        Next

        Return True
    End Function

    Function getAttendanceAnswer(ByVal i As Integer)

        Dim returnvalue(2) As String

        Dim sql As String = "select q" + i.ToString + ",q" + i.ToString + "tick from fapt_attendance_notes where willClientSequence=" + clientID
        Dim result As Array = runSQLwithArray(sql)

        If result(1) = "" Then
            returnvalue(0) = "not answered"
            returnvalue(1) = "answer"
        Else
            If i = 1 Then
                returnvalue(0) = result(0)
            ElseIf i = 2 Then
                If Val(result(1)) = 1 Then
                    returnvalue(0) = "Yes"
                ElseIf Val(result(1)) = 2 Then
                    returnvalue(0) = "No"
                ElseIf Val(result(1)) = 3 Then
                    returnvalue(0) = "Not Applicable"
                Else
                    returnvalue(0) = "Unknown"
                End If

            ElseIf i = 3 Then
                If Val(result(1)) > 0 Then
                    returnvalue(0) = "Yes"
                Else
                    returnvalue(0) = "No"
                End If

            ElseIf i = 4 Then
                If Val(result(1)) > 0 Then
                    returnvalue(0) = "Yes"
                Else
                    returnvalue(0) = "No"
                End If

            ElseIf i = 5 Then
                returnvalue(0) = result(0)

            ElseIf i = 6 Then
                If Val(result(1)) > 0 Then
                    returnvalue(0) = "Yes"
                Else
                    returnvalue(0) = "No"
                End If
            ElseIf i = 7 Then
                If Val(result(1)) > 0 Then
                    returnvalue(0) = "Yes"
                Else
                    returnvalue(0) = "No"
                End If
            ElseIf i = 8 Then
                returnvalue(0) = "answered"
            ElseIf i = 9 Then
                If Val(result(1)) > 0 Then
                    returnvalue(0) = "Joint"
                Else
                    returnvalue(0) = "Single"
                End If
            Else
                returnvalue(0) = "Unknown"
            End If

            returnvalue(1) = "edit"
        End If


        Return returnvalue
    End Function

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.assetMode.Text = "property"
        fapt_assets_add.ShowDialog(Me)
        Me.assetMode.Text = "assetMode"
        redrawAssetsGrid()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.assetMode.Text = "things"
        fapt_assets_add.ShowDialog(Me)
        Me.assetMode.Text = "assetMode"
        redrawAssetsGrid()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.assetMode.Text = "cash"
        fapt_assets_add.ShowDialog(Me)
        Me.assetMode.Text = "assetMode"
        redrawAssetsGrid()
    End Sub

    Function redrawTrusteeGrid()

        Dim sql = ""
        Dim attorneyNames = ""
        Dim relationshipClient1 = ""
        Dim relationshipClient2 = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn

        'let's do property first
        DataGridView5.Rows.Clear()
        sql = "select * from fapt_trustees_detail where willClientSequence=" + clientID + " order by attorneyForenames asc"
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            attorneyNames = ""
            relationshipClient1 = ""
            relationshipClient2 = ""

            attorneyNames = dbreader.GetValue(3).ToString + " " + dbreader.GetValue(15).ToString.ToUpper
            relationshipClient1 = dbreader.GetValue(5).ToString
            relationshipClient2 = dbreader.GetValue(6).ToString

            'add row       
            DataGridView5.Rows.Add(dbreader.GetValue(0).ToString, attorneyNames, relationshipClient1, relationshipClient2, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True

    End Function

    Function redrawBeneficiaryGrid()

        Dim sql = ""
        Dim beneficiaryNames = ""
        Dim relationshipClient1 = ""
        Dim relationshipClient2 = ""
        Dim lapseOrIssue = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn

        'let's do property first
        DataGridView4.Rows.Clear()
        sql = "select * from fapt_beneficiaries_detail where willClientSequence=" + clientID + " order by beneficiaryForenames asc"
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            beneficiaryNames = ""
            relationshipClient1 = ""
            relationshipClient2 = ""
            lapseOrIssue = ""

            beneficiaryNames = dbreader.GetValue(15).ToString + " " + dbreader.GetValue(16).ToString.ToUpper
            relationshipClient1 = dbreader.GetValue(10).ToString
            relationshipClient2 = dbreader.GetValue(11).ToString
            lapseOrIssue = dbreader.GetValue(5).ToString

            'add row       
            DataGridView4.Rows.Add(dbreader.GetValue(0).ToString, beneficiaryNames, relationshipClient1, relationshipClient2, lapseOrIssue, "edit")

        End While
        dbreader.Close()
        dbConn.Close()

        Return True

    End Function

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        fapt_beneficiary_add.ShowDialog(Me)
        redrawBeneficiaryGrid()
    End Sub

    Private Sub DataGridView4_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView4.CellContentClick
        If e.ColumnIndex = 5 And DataGridView4.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView4.Item(0, e.RowIndex).Value.ToString
            fapt_beneficiary_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawBeneficiaryGrid()
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        fapt_trustee_add.ShowDialog(Me)
        redrawTrusteeGrid()
    End Sub

    Private Sub DataGridView5_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView5.CellContentClick
        If e.ColumnIndex = 4 And DataGridView5.Item(4, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView5.Item(0, e.RowIndex).Value.ToString
            fapt_trustee_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            redrawTrusteeGrid()
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 6 And DataGridView1.Item(6, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView1.Item(0, e.RowIndex).Value.ToString
            Me.assetTableName.Text = DataGridView1.Item(7, e.RowIndex).Value.ToString
            fapt_assets_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            Me.assetTableName.Text = "assetTablename"
            redrawAssetsGrid()
        End If
    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        If e.ColumnIndex = 5 And DataGridView2.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView2.Item(0, e.RowIndex).Value.ToString
            Me.assetTableName.Text = DataGridView2.Item(6, e.RowIndex).Value.ToString
            fapt_assets_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            Me.assetTableName.Text = "assetTablename"
            redrawAssetsGrid()
        End If
    End Sub

    Private Sub DataGridView3_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView3.CellContentClick
        If e.ColumnIndex = 5 And DataGridView3.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView2.Item(0, e.RowIndex).Value.ToString
            Me.assetTableName.Text = DataGridView3.Item(6, e.RowIndex).Value.ToString
            fapt_assets_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            Me.assetTableName.Text = "assetTablename"
            redrawAssetsGrid()
        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton5.CheckedChanged

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        'we need to populate the beneficiary list with the names of the children from the WILL, if they exist
        Dim relationshipClient1 = ""
        Dim relationshipClient2 = ""

        Dim sql As String = "select * from will_instruction_stage4_data_detail where willClientSequence=" + clientID
        Dim dbConn As New OleDb.OleDbConnection
            Dim command As New OleDb.OleDbCommand
            Dim dbreader As OleDb.OleDbDataReader
            dbConn.ConnectionString = connectionString
            dbConn.Open()
            command.Connection = dbConn
            command.CommandText = sql
            dbreader = command.ExecuteReader
            While dbreader.Read

                If Trim(dbreader.GetValue(9).ToString) = "" Then
                'unknown title
                relationshipClient1 = ""
                relationshipClient2 = ""

            ElseIf Trim(dbreader.GetValue(9).ToString.ToUpper) = "MR" Then
                'it's male, but is it step or bloodline
                If dbreader.GetValue(4).ToString.ToUpper = "BLOODLINE" Then relationshipClient1 = "Son"
                If dbreader.GetValue(4).ToString.ToUpper = "STEPCHILD" Then relationshipClient1 = "Stepson"

                If dbreader.GetValue(5).ToString.ToUpper = "BLOODLINE" Then relationshipClient2 = "Son"
                If dbreader.GetValue(5).ToString.ToUpper = "STEPCHILD" Then relationshipClient2 = "Stepson"

            Else
                'it must be female
                If dbreader.GetValue(4).ToString.ToUpper = "BLOODLINE" Then relationshipClient1 = "Daughter"
                If dbreader.GetValue(4).ToString.ToUpper = "STEPCHILD" Then relationshipClient1 = "Step Daughter"

                If dbreader.GetValue(5).ToString.ToUpper = "BLOODLINE" Then relationshipClient2 = "Daughter"
                If dbreader.GetValue(5).ToString.ToUpper = "STEPCHILD" Then relationshipClient2 = "Step Daughter"
            End If

            'insert sql required
            sql = "insert into fapt_beneficiaries_detail (willClientSequence,beneficiaryType,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,lapseOrIssue,beneficiaryDOB,relationshipClient1,relationshipClient2) values (" + clientID + ",'Named Individual','" + SqlSafe(dbreader.GetValue(9).ToString) + "','" + SqlSafe(dbreader.GetValue(3).ToString) + "','" + SqlSafe(dbreader.GetValue(2).ToString) + "','issue','" + SqlSafe(dbreader.GetValue(11).ToString) + "','" + relationshipClient1 + "','" + relationshipClient2 + "')"
            runSQL(sql)
            End While

            dbreader.Close()
            dbConn.Close()


        'finally show them on the screen
        redrawBeneficiaryGrid()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        'we need to populate the beneficiary list with the names of the children from the WILL, if they exist
        Dim attorneyRelationshipClient1 = ""
        Dim attorneyRelationshipClient2 = ""

        Dim sql As String = "select * from will_instruction_stage4_data_detail where willClientSequence=" + clientID
        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read

            If Trim(dbreader.GetValue(9).ToString) = "" Then
                'unknown title
                attorneyRelationshipClient1 = ""
                attorneyRelationshipClient2 = ""

            ElseIf Trim(dbreader.GetValue(9).ToString.ToUpper) = "MR" Then
                'it's male, but is it step or bloodline
                If dbreader.GetValue(4).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient1 = "Son"
                If dbreader.GetValue(4).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient1 = "Stepson"

                If dbreader.GetValue(5).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient2 = "Son"
                If dbreader.GetValue(5).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient2 = "Stepson"

            Else
                'it must be female
                If dbreader.GetValue(4).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient1 = "Daughter"
                If dbreader.GetValue(4).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient1 = "Step Daughter"

                If dbreader.GetValue(5).ToString.ToUpper = "BLOODLINE" Then attorneyRelationshipClient2 = "Daughter"
                If dbreader.GetValue(5).ToString.ToUpper = "STEPCHILD" Then attorneyRelationshipClient2 = "Step Daughter"
            End If

            'insert sql required
            sql = "insert into fapt_trustees_detail (willClientSequence,attorneyTitle,attorneyNames,attorneySurname,attorneyAddress,attorneyPostcode,attorneyRelationshipClient1,attorneyRelationshipClient2) values (" + clientID + ",'" + SqlSafe(dbreader.GetValue(9).ToString) + "','" + SqlSafe(dbreader.GetValue(3).ToString) + "','" + SqlSafe(dbreader.GetValue(2).ToString) + "','" + SqlSafe(dbreader.GetValue(6).ToString) + "','" + SqlSafe(dbreader.GetValue(10).ToString) + "','" + attorneyRelationshipClient1 + "','" + attorneyRelationshipClient2 + "')"
            runSQL(sql)
        End While

        dbreader.Close()
        dbConn.Close()

        'finally show them on the screen
        redrawTrusteeGrid()
    End Sub
End Class