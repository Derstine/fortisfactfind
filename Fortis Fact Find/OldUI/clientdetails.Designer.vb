﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class clientdetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.person1MaritalStatus = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.person1Email = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.person1HomeNumber = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.person1MobileNumber = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.person1Address = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.person1Postcode = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.person1DOB = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.person1Surname = New System.Windows.Forms.TextBox()
        Me.person1Forenames = New System.Windows.Forms.TextBox()
        Me.person1Title = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.person2MaritalStatus = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.person2Email = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.person2HomeNumber = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.person2MobileNumber = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.person2Address = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.person2Postcode = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.person2DOB = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.person2Surname = New System.Windows.Forms.TextBox()
        Me.person2Forenames = New System.Windows.Forms.TextBox()
        Me.person2Title = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.clientReferenceNumber = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.black
        Me.Button2.Location = New System.Drawing.Point(1130, 614)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(92, 35)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(169, 29)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Client Details"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.black
        Me.Button1.Location = New System.Drawing.Point(1032, 614)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 35)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(23, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 16)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Type of Client"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(119, 70)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(64, 20)
        Me.RadioButton1.TabIndex = 11
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Single"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(189, 70)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(54, 20)
        Me.RadioButton2.TabIndex = 12
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Dual"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.person1MaritalStatus)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.person1Email)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.person1HomeNumber)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.person1MobileNumber)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.person1Address)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.person1Postcode)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.person1DOB)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.person1Surname)
        Me.GroupBox1.Controls.Add(Me.person1Forenames)
        Me.GroupBox1.Controls.Add(Me.person1Title)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(26, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(580, 483)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Client 1"
        '
        'person1MaritalStatus
        '
        Me.person1MaritalStatus.FormattingEnabled = True
        Me.person1MaritalStatus.Items.AddRange(New Object() {"Select...", "Married", "Single", "Partnered", "Widowed", "Divorced"})
        Me.person1MaritalStatus.Location = New System.Drawing.Point(113, 342)
        Me.person1MaritalStatus.Name = "person1MaritalStatus"
        Me.person1MaritalStatus.Size = New System.Drawing.Size(168, 24)
        Me.person1MaritalStatus.TabIndex = 7
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(19, 345)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 16)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "Marital Status"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(206, 174)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Lookup"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'person1Email
        '
        Me.person1Email.Location = New System.Drawing.Point(113, 444)
        Me.person1Email.Name = "person1Email"
        Me.person1Email.Size = New System.Drawing.Size(368, 22)
        Me.person1Email.TabIndex = 10
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(65, 447)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(42, 16)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Email"
        '
        'person1HomeNumber
        '
        Me.person1HomeNumber.Location = New System.Drawing.Point(113, 410)
        Me.person1HomeNumber.Name = "person1HomeNumber"
        Me.person1HomeNumber.Size = New System.Drawing.Size(170, 22)
        Me.person1HomeNumber.TabIndex = 9
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 413)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 16)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Home Number"
        '
        'person1MobileNumber
        '
        Me.person1MobileNumber.Location = New System.Drawing.Point(113, 376)
        Me.person1MobileNumber.Name = "person1MobileNumber"
        Me.person1MobileNumber.Size = New System.Drawing.Size(170, 22)
        Me.person1MobileNumber.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 379)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 16)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Mobile Number"
        '
        'person1Address
        '
        Me.person1Address.Location = New System.Drawing.Point(113, 209)
        Me.person1Address.Multiline = True
        Me.person1Address.Name = "person1Address"
        Me.person1Address.Size = New System.Drawing.Size(268, 122)
        Me.person1Address.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(48, 212)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Address"
        '
        'person1Postcode
        '
        Me.person1Postcode.Location = New System.Drawing.Point(113, 175)
        Me.person1Postcode.Name = "person1Postcode"
        Me.person1Postcode.Size = New System.Drawing.Size(87, 22)
        Me.person1Postcode.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(41, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 16)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Postcode"
        '
        'person1DOB
        '
        Me.person1DOB.Location = New System.Drawing.Point(113, 141)
        Me.person1DOB.Name = "person1DOB"
        Me.person1DOB.Size = New System.Drawing.Size(170, 22)
        Me.person1DOB.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(70, 146)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 16)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "DOB"
        '
        'person1Surname
        '
        Me.person1Surname.Location = New System.Drawing.Point(113, 104)
        Me.person1Surname.Name = "person1Surname"
        Me.person1Surname.Size = New System.Drawing.Size(198, 22)
        Me.person1Surname.TabIndex = 2
        '
        'person1Forenames
        '
        Me.person1Forenames.Location = New System.Drawing.Point(113, 70)
        Me.person1Forenames.Name = "person1Forenames"
        Me.person1Forenames.Size = New System.Drawing.Size(268, 22)
        Me.person1Forenames.TabIndex = 1
        '
        'person1Title
        '
        Me.person1Title.FormattingEnabled = True
        Me.person1Title.Items.AddRange(New Object() {"Select...", "Mr", "Mrs", "Miss", "Ms"})
        Me.person1Title.Location = New System.Drawing.Point(113, 34)
        Me.person1Title.Name = "person1Title"
        Me.person1Title.Size = New System.Drawing.Size(104, 24)
        Me.person1Title.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 107)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Surname"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Forename(s)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(73, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Title"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button5)
        Me.GroupBox2.Controls.Add(Me.person2MaritalStatus)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.person2Email)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.person2HomeNumber)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.person2MobileNumber)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.person2Address)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.person2Postcode)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.person2DOB)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.person2Surname)
        Me.GroupBox2.Controls.Add(Me.person2Forenames)
        Me.GroupBox2.Controls.Add(Me.person2Title)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(642, 112)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(580, 483)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Client 2"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(287, 175)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(103, 23)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "Copy Client 1"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'person2MaritalStatus
        '
        Me.person2MaritalStatus.FormattingEnabled = True
        Me.person2MaritalStatus.Items.AddRange(New Object() {"Select...", "Married", "Single", "Partnered", "Widowed", "Divorced"})
        Me.person2MaritalStatus.Location = New System.Drawing.Point(113, 342)
        Me.person2MaritalStatus.Name = "person2MaritalStatus"
        Me.person2MaritalStatus.Size = New System.Drawing.Size(168, 24)
        Me.person2MaritalStatus.TabIndex = 8
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(19, 345)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 16)
        Me.Label13.TabIndex = 20
        Me.Label13.Text = "Marital Status"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(206, 174)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "Lookup"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'person2Email
        '
        Me.person2Email.Location = New System.Drawing.Point(113, 444)
        Me.person2Email.Name = "person2Email"
        Me.person2Email.Size = New System.Drawing.Size(368, 22)
        Me.person2Email.TabIndex = 11
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(65, 447)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 16)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = "Email"
        '
        'person2HomeNumber
        '
        Me.person2HomeNumber.Location = New System.Drawing.Point(113, 410)
        Me.person2HomeNumber.Name = "person2HomeNumber"
        Me.person2HomeNumber.Size = New System.Drawing.Size(170, 22)
        Me.person2HomeNumber.TabIndex = 10
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(11, 413)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(96, 16)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "Home Number"
        '
        'person2MobileNumber
        '
        Me.person2MobileNumber.Location = New System.Drawing.Point(113, 376)
        Me.person2MobileNumber.Name = "person2MobileNumber"
        Me.person2MobileNumber.Size = New System.Drawing.Size(170, 22)
        Me.person2MobileNumber.TabIndex = 9
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(7, 379)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 16)
        Me.Label16.TabIndex = 13
        Me.Label16.Text = "Mobile Number"
        '
        'person2Address
        '
        Me.person2Address.Location = New System.Drawing.Point(113, 209)
        Me.person2Address.Multiline = True
        Me.person2Address.Name = "person2Address"
        Me.person2Address.Size = New System.Drawing.Size(268, 122)
        Me.person2Address.TabIndex = 7
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(48, 212)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(59, 16)
        Me.Label17.TabIndex = 11
        Me.Label17.Text = "Address"
        '
        'person2Postcode
        '
        Me.person2Postcode.Location = New System.Drawing.Point(113, 175)
        Me.person2Postcode.Name = "person2Postcode"
        Me.person2Postcode.Size = New System.Drawing.Size(87, 22)
        Me.person2Postcode.TabIndex = 4
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(41, 178)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(66, 16)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "Postcode"
        '
        'person2DOB
        '
        Me.person2DOB.Location = New System.Drawing.Point(113, 141)
        Me.person2DOB.Name = "person2DOB"
        Me.person2DOB.Size = New System.Drawing.Size(170, 22)
        Me.person2DOB.TabIndex = 3
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(70, 146)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(37, 16)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "DOB"
        '
        'person2Surname
        '
        Me.person2Surname.Location = New System.Drawing.Point(113, 104)
        Me.person2Surname.Name = "person2Surname"
        Me.person2Surname.Size = New System.Drawing.Size(198, 22)
        Me.person2Surname.TabIndex = 2
        '
        'person2Forenames
        '
        Me.person2Forenames.Location = New System.Drawing.Point(113, 70)
        Me.person2Forenames.Name = "person2Forenames"
        Me.person2Forenames.Size = New System.Drawing.Size(268, 22)
        Me.person2Forenames.TabIndex = 1
        '
        'person2Title
        '
        Me.person2Title.FormattingEnabled = True
        Me.person2Title.Items.AddRange(New Object() {"Select...", "Mr", "Mrs", "Miss", "Ms"})
        Me.person2Title.Location = New System.Drawing.Point(113, 34)
        Me.person2Title.Name = "person2Title"
        Me.person2Title.Size = New System.Drawing.Size(104, 24)
        Me.person2Title.TabIndex = 0
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(45, 107)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 16)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Surname"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(22, 73)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(85, 16)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Forename(s)"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(73, 37)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(34, 16)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Title"
        '
        'clientReferenceNumber
        '
        Me.clientReferenceNumber.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clientReferenceNumber.Location = New System.Drawing.Point(477, 68)
        Me.clientReferenceNumber.Name = "clientReferenceNumber"
        Me.clientReferenceNumber.Size = New System.Drawing.Size(122, 22)
        Me.clientReferenceNumber.TabIndex = 23
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(303, 72)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(168, 16)
        Me.Label23.TabIndex = 24
        Me.Label23.Text = "Internal Reference Number"
        '
        'clientdetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.clientReferenceNumber)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.MaximizeBox = False
        Me.Name = "clientdetails"
        Me.Text = "Client Details"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button2 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Button3 As Button
    Friend WithEvents person1Email As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents person1HomeNumber As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents person1MobileNumber As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents person1Address As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents person1Postcode As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents person1DOB As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents person1Surname As TextBox
    Friend WithEvents person1Forenames As TextBox
    Friend WithEvents person1Title As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents person1MaritalStatus As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents person2MaritalStatus As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents person2Email As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents person2HomeNumber As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents person2MobileNumber As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents person2Address As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents person2Postcode As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents person2DOB As DateTimePicker
    Friend WithEvents Label19 As Label
    Friend WithEvents person2Surname As TextBox
    Friend WithEvents person2Forenames As TextBox
    Friend WithEvents person2Title As ComboBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Button5 As Button
    Friend WithEvents clientReferenceNumber As TextBox
    Friend WithEvents Label23 As Label
End Class
