﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class factfind_will
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn1 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.RadioButton25 = New System.Windows.Forms.RadioButton()
        Me.RadioButton26 = New System.Windows.Forms.RadioButton()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.RadioButton20 = New System.Windows.Forms.RadioButton()
        Me.RadioButton21 = New System.Windows.Forms.RadioButton()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.RadioButton15 = New System.Windows.Forms.RadioButton()
        Me.RadioButton16 = New System.Windows.Forms.RadioButton()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.RadioButton22 = New System.Windows.Forms.RadioButton()
        Me.RadioButton23 = New System.Windows.Forms.RadioButton()
        Me.RadioButton24 = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.RadioButton17 = New System.Windows.Forms.RadioButton()
        Me.RadioButton18 = New System.Windows.Forms.RadioButton()
        Me.RadioButton19 = New System.Windows.Forms.RadioButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.RadioButton14 = New System.Windows.Forms.RadioButton()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.RadioButton11 = New System.Windows.Forms.RadioButton()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.RadioButton12 = New System.Windows.Forms.RadioButton()
        Me.RadioButton13 = New System.Windows.Forms.RadioButton()
        Me.DataGridView5 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn4 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn2 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.RadioButton32 = New System.Windows.Forms.RadioButton()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.RadioButton33 = New System.Windows.Forms.RadioButton()
        Me.RadioButton34 = New System.Windows.Forms.RadioButton()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.RadioButton35 = New System.Windows.Forms.RadioButton()
        Me.RadioButton36 = New System.Windows.Forms.RadioButton()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.RadioButton31 = New System.Windows.Forms.RadioButton()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.RadioButton29 = New System.Windows.Forms.RadioButton()
        Me.RadioButton30 = New System.Windows.Forms.RadioButton()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.RadioButton27 = New System.Windows.Forms.RadioButton()
        Me.RadioButton28 = New System.Windows.Forms.RadioButton()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn3 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.RadioButton37 = New System.Windows.Forms.RadioButton()
        Me.RadioButton38 = New System.Windows.Forms.RadioButton()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn5 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.RadioButton54 = New System.Windows.Forms.RadioButton()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.RadioButton55 = New System.Windows.Forms.RadioButton()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.RadioButton52 = New System.Windows.Forms.RadioButton()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.RadioButton53 = New System.Windows.Forms.RadioButton()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.RadioButton50 = New System.Windows.Forms.RadioButton()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.RadioButton51 = New System.Windows.Forms.RadioButton()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.RadioButton48 = New System.Windows.Forms.RadioButton()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.RadioButton49 = New System.Windows.Forms.RadioButton()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.RadioButton46 = New System.Windows.Forms.RadioButton()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.RadioButton47 = New System.Windows.Forms.RadioButton()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.RadioButton44 = New System.Windows.Forms.RadioButton()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.RadioButton45 = New System.Windows.Forms.RadioButton()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.DataGridView6 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn7 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.RadioButton56 = New System.Windows.Forms.RadioButton()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.RadioButton57 = New System.Windows.Forms.RadioButton()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.RadioButton58 = New System.Windows.Forms.RadioButton()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.RadioButton59 = New System.Windows.Forms.RadioButton()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.RadioButton60 = New System.Windows.Forms.RadioButton()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.RadioButton61 = New System.Windows.Forms.RadioButton()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.RadioButton43 = New System.Windows.Forms.RadioButton()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.RadioButton41 = New System.Windows.Forms.RadioButton()
        Me.RadioButton42 = New System.Windows.Forms.RadioButton()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.TabControl3 = New System.Windows.Forms.TabControl()
        Me.TabPage13 = New System.Windows.Forms.TabPage()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.DataGridView8 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn8 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Panel28 = New System.Windows.Forms.Panel()
        Me.RadioButton66 = New System.Windows.Forms.RadioButton()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.RadioButton67 = New System.Windows.Forms.RadioButton()
        Me.Panel29 = New System.Windows.Forms.Panel()
        Me.RadioButton68 = New System.Windows.Forms.RadioButton()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.RadioButton69 = New System.Windows.Forms.RadioButton()
        Me.Panel30 = New System.Windows.Forms.Panel()
        Me.RadioButton70 = New System.Windows.Forms.RadioButton()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.RadioButton71 = New System.Windows.Forms.RadioButton()
        Me.Panel31 = New System.Windows.Forms.Panel()
        Me.RadioButton72 = New System.Windows.Forms.RadioButton()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.RadioButton73 = New System.Windows.Forms.RadioButton()
        Me.TabPage14 = New System.Windows.Forms.TabPage()
        Me.Panel37 = New System.Windows.Forms.Panel()
        Me.RadioButton84 = New System.Windows.Forms.RadioButton()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.RadioButton85 = New System.Windows.Forms.RadioButton()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.DataGridView9 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn9 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Panel34 = New System.Windows.Forms.Panel()
        Me.RadioButton78 = New System.Windows.Forms.RadioButton()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.RadioButton79 = New System.Windows.Forms.RadioButton()
        Me.Panel35 = New System.Windows.Forms.Panel()
        Me.RadioButton80 = New System.Windows.Forms.RadioButton()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.RadioButton81 = New System.Windows.Forms.RadioButton()
        Me.Panel36 = New System.Windows.Forms.Panel()
        Me.RadioButton82 = New System.Windows.Forms.RadioButton()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.RadioButton83 = New System.Windows.Forms.RadioButton()
        Me.TabPage15 = New System.Windows.Forms.TabPage()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.DataGridView10 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn10 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Panel32 = New System.Windows.Forms.Panel()
        Me.RadioButton74 = New System.Windows.Forms.RadioButton()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.RadioButton75 = New System.Windows.Forms.RadioButton()
        Me.Panel33 = New System.Windows.Forms.Panel()
        Me.RadioButton76 = New System.Windows.Forms.RadioButton()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.RadioButton77 = New System.Windows.Forms.RadioButton()
        Me.Panel27 = New System.Windows.Forms.Panel()
        Me.RadioButton65 = New System.Windows.Forms.RadioButton()
        Me.RadioButton62 = New System.Windows.Forms.RadioButton()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.RadioButton63 = New System.Windows.Forms.RadioButton()
        Me.RadioButton64 = New System.Windows.Forms.RadioButton()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.RadioButton39 = New System.Windows.Forms.RadioButton()
        Me.RadioButton40 = New System.Windows.Forms.RadioButton()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.DataGridView7 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn6 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.firstRunComplete = New System.Windows.Forms.Label()
        Me.attendanceMode = New System.Windows.Forms.Label()
        Me.residuaryType = New System.Windows.Forms.Label()
        Me.trustSequence = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel15.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage11.SuspendLayout()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel23.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.TabPage12.SuspendLayout()
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel24.SuspendLayout()
        Me.Panel25.SuspendLayout()
        Me.Panel26.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.TabControl3.SuspendLayout()
        Me.TabPage13.SuspendLayout()
        CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel28.SuspendLayout()
        Me.Panel29.SuspendLayout()
        Me.Panel30.SuspendLayout()
        Me.Panel31.SuspendLayout()
        Me.TabPage14.SuspendLayout()
        Me.Panel37.SuspendLayout()
        CType(Me.DataGridView9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel34.SuspendLayout()
        Me.Panel35.SuspendLayout()
        Me.Panel36.SuspendLayout()
        Me.TabPage15.SuspendLayout()
        CType(Me.DataGridView10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel32.SuspendLayout()
        Me.Panel33.SuspendLayout()
        Me.Panel27.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.Panel16.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.black
        Me.Button2.Location = New System.Drawing.Point(1120, 602)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(102, 47)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Close"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 63)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.Padding = New System.Drawing.Point(10, 10)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1209, 530)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 39)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Client Information"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.RadioButton5)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.RadioButton1)
        Me.Panel2.Controls.Add(Me.RadioButton2)
        Me.Panel2.Location = New System.Drawing.Point(25, 409)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1151, 37)
        Me.Panel2.TabIndex = 23
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton5.Location = New System.Drawing.Point(851, 8)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton5.TabIndex = 21
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "n/a"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(11, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(680, 18)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "If you are single, do you want your will written to include the partner named her" &
    "e as your future spouse?"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(712, 9)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton1.TabIndex = 19
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Yes"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(784, 9)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton2.TabIndex = 20
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "No"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.RadioButton4)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.RadioButton3)
        Me.Panel1.Location = New System.Drawing.Point(25, 366)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(861, 37)
        Me.Panel1.TabIndex = 22
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(257, 18)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Do you have a will in another country?"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(533, 3)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(265, 26)
        Me.TextBox1.TabIndex = 20
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.Location = New System.Drawing.Point(278, 8)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton4.TabIndex = 19
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Yes"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(409, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(118, 18)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Name of country"
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(337, 8)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton3.TabIndex = 20
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "No"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox6)
        Me.GroupBox2.Controls.Add(Me.TextBox9)
        Me.GroupBox2.Controls.Add(Me.TextBox7)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.TextBox8)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(635, 50)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(541, 310)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Client 2"
        '
        'TextBox6
        '
        Me.TextBox6.Enabled = False
        Me.TextBox6.Location = New System.Drawing.Point(129, 267)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(96, 24)
        Me.TextBox6.TabIndex = 26
        '
        'TextBox9
        '
        Me.TextBox9.Enabled = False
        Me.TextBox9.Location = New System.Drawing.Point(129, 41)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(385, 24)
        Me.TextBox9.TabIndex = 23
        '
        'TextBox7
        '
        Me.TextBox7.Enabled = False
        Me.TextBox7.Location = New System.Drawing.Point(129, 127)
        Me.TextBox7.Multiline = True
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(320, 134)
        Me.TextBox7.TabIndex = 25
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Enabled = False
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(56, 44)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(48, 18)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "Name"
        '
        'TextBox8
        '
        Me.TextBox8.Enabled = False
        Me.TextBox8.Location = New System.Drawing.Point(129, 84)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(228, 24)
        Me.TextBox8.TabIndex = 24
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Enabled = False
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(14, 87)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 18)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date of Birth"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Enabled = False
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(42, 130)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(62, 18)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "Address"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(25, 50)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(541, 310)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Client 1"
        '
        'TextBox5
        '
        Me.TextBox5.Enabled = False
        Me.TextBox5.Location = New System.Drawing.Point(130, 267)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(96, 24)
        Me.TextBox5.TabIndex = 19
        '
        'TextBox4
        '
        Me.TextBox4.Enabled = False
        Me.TextBox4.Location = New System.Drawing.Point(130, 127)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(320, 134)
        Me.TextBox4.TabIndex = 18
        '
        'TextBox3
        '
        Me.TextBox3.Enabled = False
        Me.TextBox3.Location = New System.Drawing.Point(130, 84)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(228, 24)
        Me.TextBox3.TabIndex = 17
        '
        'TextBox2
        '
        Me.TextBox2.Enabled = False
        Me.TextBox2.Location = New System.Drawing.Point(130, 41)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(385, 24)
        Me.TextBox2.TabIndex = 16
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Enabled = False
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(43, 130)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(62, 18)
        Me.Label13.TabIndex = 15
        Me.Label13.Text = "Address"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Enabled = False
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(15, 87)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 18)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Date of Birth"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Enabled = False
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(57, 44)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 18)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Name"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(102, 20)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Client Details"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.DataGridView1)
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.RadioButton6)
        Me.TabPage2.Controls.Add(Me.RadioButton7)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage2.Location = New System.Drawing.Point(4, 39)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Children"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Beige
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Column4, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewLinkColumn1})
        Me.DataGridView1.Location = New System.Drawing.Point(16, 112)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.Size = New System.Drawing.Size(923, 270)
        Me.DataGridView1.TabIndex = 40
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 300
        '
        'Column4
        '
        Me.Column4.HeaderText = "DOB"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 180
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 180
        '
        'DataGridViewLinkColumn1
        '
        Me.DataGridViewLinkColumn1.HeaderText = "Action"
        Me.DataGridViewLinkColumn1.Name = "DataGridViewLinkColumn1"
        Me.DataGridViewLinkColumn1.ReadOnly = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.black
        Me.Button1.Location = New System.Drawing.Point(806, 70)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 36)
        Me.Button1.TabIndex = 39
        Me.Button1.Text = "Add A Child"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton6.Location = New System.Drawing.Point(240, 58)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton6.TabIndex = 21
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "No"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.Location = New System.Drawing.Point(183, 58)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton7.TabIndex = 20
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "Yes"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 60)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(154, 18)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Do you have children?"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 20)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Children"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage3.Controls.Add(Me.Panel10)
        Me.TabPage3.Controls.Add(Me.Panel8)
        Me.TabPage3.Controls.Add(Me.Panel6)
        Me.TabPage3.Controls.Add(Me.Panel9)
        Me.TabPage3.Controls.Add(Me.Panel3)
        Me.TabPage3.Controls.Add(Me.Panel7)
        Me.TabPage3.Controls.Add(Me.Panel4)
        Me.TabPage3.Controls.Add(Me.Panel5)
        Me.TabPage3.Controls.Add(Me.DataGridView5)
        Me.TabPage3.Controls.Add(Me.Button6)
        Me.TabPage3.Controls.Add(Me.Label6)
        Me.TabPage3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(4, 39)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Executors"
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.Label19)
        Me.Panel10.Controls.Add(Me.RadioButton25)
        Me.Panel10.Controls.Add(Me.RadioButton26)
        Me.Panel10.Location = New System.Drawing.Point(19, 189)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(523, 30)
        Me.Panel10.TabIndex = 45
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(5, 7)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(278, 18)
        Me.Label19.TabIndex = 29
        Me.Label19.Text = "Do you want to add any other executors?"
        '
        'RadioButton25
        '
        Me.RadioButton25.AutoSize = True
        Me.RadioButton25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton25.Location = New System.Drawing.Point(412, 5)
        Me.RadioButton25.Name = "RadioButton25"
        Me.RadioButton25.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton25.TabIndex = 30
        Me.RadioButton25.TabStop = True
        Me.RadioButton25.Text = "Yes"
        Me.RadioButton25.UseVisualStyleBackColor = True
        '
        'RadioButton26
        '
        Me.RadioButton26.AutoSize = True
        Me.RadioButton26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton26.Location = New System.Drawing.Point(467, 5)
        Me.RadioButton26.Name = "RadioButton26"
        Me.RadioButton26.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton26.TabIndex = 31
        Me.RadioButton26.TabStop = True
        Me.RadioButton26.Text = "No"
        Me.RadioButton26.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label18)
        Me.Panel8.Controls.Add(Me.RadioButton20)
        Me.Panel8.Controls.Add(Me.RadioButton21)
        Me.Panel8.Location = New System.Drawing.Point(19, 153)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(523, 30)
        Me.Panel8.TabIndex = 44
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(5, 7)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(375, 18)
        Me.Label18.TabIndex = 29
        Me.Label18.Text = "Do you wish ALL of your children  to be your executors?"
        '
        'RadioButton20
        '
        Me.RadioButton20.AutoSize = True
        Me.RadioButton20.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton20.Location = New System.Drawing.Point(412, 5)
        Me.RadioButton20.Name = "RadioButton20"
        Me.RadioButton20.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton20.TabIndex = 30
        Me.RadioButton20.TabStop = True
        Me.RadioButton20.Text = "Yes"
        Me.RadioButton20.UseVisualStyleBackColor = True
        '
        'RadioButton21
        '
        Me.RadioButton21.AutoSize = True
        Me.RadioButton21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton21.Location = New System.Drawing.Point(467, 5)
        Me.RadioButton21.Name = "RadioButton21"
        Me.RadioButton21.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton21.TabIndex = 31
        Me.RadioButton21.TabStop = True
        Me.RadioButton21.Text = "No"
        Me.RadioButton21.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label9)
        Me.Panel6.Controls.Add(Me.RadioButton15)
        Me.Panel6.Controls.Add(Me.RadioButton16)
        Me.Panel6.Location = New System.Drawing.Point(19, 117)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(523, 30)
        Me.Panel6.TabIndex = 42
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(5, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(317, 18)
        Me.Label9.TabIndex = 29
        Me.Label9.Text = "Do you wish your Spouse to be your executor?"
        '
        'RadioButton15
        '
        Me.RadioButton15.AutoSize = True
        Me.RadioButton15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton15.Location = New System.Drawing.Point(412, 5)
        Me.RadioButton15.Name = "RadioButton15"
        Me.RadioButton15.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton15.TabIndex = 30
        Me.RadioButton15.TabStop = True
        Me.RadioButton15.Text = "Yes"
        Me.RadioButton15.UseVisualStyleBackColor = True
        '
        'RadioButton16
        '
        Me.RadioButton16.AutoSize = True
        Me.RadioButton16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton16.Location = New System.Drawing.Point(467, 5)
        Me.RadioButton16.Name = "RadioButton16"
        Me.RadioButton16.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton16.TabIndex = 31
        Me.RadioButton16.TabStop = True
        Me.RadioButton16.Text = "No"
        Me.RadioButton16.UseVisualStyleBackColor = True
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Label22)
        Me.Panel9.Controls.Add(Me.RadioButton22)
        Me.Panel9.Controls.Add(Me.RadioButton23)
        Me.Panel9.Controls.Add(Me.RadioButton24)
        Me.Panel9.Location = New System.Drawing.Point(571, 153)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(393, 30)
        Me.Panel9.TabIndex = 45
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(3, 7)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(107, 18)
        Me.Label22.TabIndex = 37
        Me.Label22.Text = "Executor Type:"
        '
        'RadioButton22
        '
        Me.RadioButton22.AutoSize = True
        Me.RadioButton22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton22.Location = New System.Drawing.Point(131, 5)
        Me.RadioButton22.Name = "RadioButton22"
        Me.RadioButton22.Size = New System.Drawing.Size(56, 22)
        Me.RadioButton22.TabIndex = 36
        Me.RadioButton22.TabStop = True
        Me.RadioButton22.Text = "Sole"
        Me.RadioButton22.UseVisualStyleBackColor = True
        '
        'RadioButton23
        '
        Me.RadioButton23.AutoSize = True
        Me.RadioButton23.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton23.Location = New System.Drawing.Point(213, 5)
        Me.RadioButton23.Name = "RadioButton23"
        Me.RadioButton23.Size = New System.Drawing.Size(58, 22)
        Me.RadioButton23.TabIndex = 33
        Me.RadioButton23.TabStop = True
        Me.RadioButton23.Text = "Joint"
        Me.RadioButton23.UseVisualStyleBackColor = True
        '
        'RadioButton24
        '
        Me.RadioButton24.AutoSize = True
        Me.RadioButton24.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton24.Location = New System.Drawing.Point(297, 5)
        Me.RadioButton24.Name = "RadioButton24"
        Me.RadioButton24.Size = New System.Drawing.Size(91, 22)
        Me.RadioButton24.TabIndex = 34
        Me.RadioButton24.TabStop = True
        Me.RadioButton24.Text = "Substitute"
        Me.RadioButton24.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.RadioButton8)
        Me.Panel3.Controls.Add(Me.RadioButton9)
        Me.Panel3.Location = New System.Drawing.Point(19, 81)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(523, 30)
        Me.Panel3.TabIndex = 40
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(5, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(327, 18)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Do you want Our Company to be your executor?"
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.Location = New System.Drawing.Point(412, 5)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton8.TabIndex = 30
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Yes"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = True
        Me.RadioButton9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton9.Location = New System.Drawing.Point(467, 5)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton9.TabIndex = 31
        Me.RadioButton9.TabStop = True
        Me.RadioButton9.Text = "No"
        Me.RadioButton9.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Label21)
        Me.Panel7.Controls.Add(Me.RadioButton17)
        Me.Panel7.Controls.Add(Me.RadioButton18)
        Me.Panel7.Controls.Add(Me.RadioButton19)
        Me.Panel7.Location = New System.Drawing.Point(571, 117)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(393, 30)
        Me.Panel7.TabIndex = 43
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(3, 7)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(107, 18)
        Me.Label21.TabIndex = 36
        Me.Label21.Text = "Executor Type:"
        '
        'RadioButton17
        '
        Me.RadioButton17.AutoSize = True
        Me.RadioButton17.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton17.Location = New System.Drawing.Point(131, 5)
        Me.RadioButton17.Name = "RadioButton17"
        Me.RadioButton17.Size = New System.Drawing.Size(56, 22)
        Me.RadioButton17.TabIndex = 35
        Me.RadioButton17.TabStop = True
        Me.RadioButton17.Text = "Sole"
        Me.RadioButton17.UseVisualStyleBackColor = True
        '
        'RadioButton18
        '
        Me.RadioButton18.AutoSize = True
        Me.RadioButton18.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton18.Location = New System.Drawing.Point(213, 5)
        Me.RadioButton18.Name = "RadioButton18"
        Me.RadioButton18.Size = New System.Drawing.Size(58, 22)
        Me.RadioButton18.TabIndex = 33
        Me.RadioButton18.TabStop = True
        Me.RadioButton18.Text = "Joint"
        Me.RadioButton18.UseVisualStyleBackColor = True
        '
        'RadioButton19
        '
        Me.RadioButton19.AutoSize = True
        Me.RadioButton19.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton19.Location = New System.Drawing.Point(297, 5)
        Me.RadioButton19.Name = "RadioButton19"
        Me.RadioButton19.Size = New System.Drawing.Size(91, 22)
        Me.RadioButton19.TabIndex = 34
        Me.RadioButton19.TabStop = True
        Me.RadioButton19.Text = "Substitute"
        Me.RadioButton19.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label20)
        Me.Panel4.Controls.Add(Me.RadioButton14)
        Me.Panel4.Controls.Add(Me.RadioButton10)
        Me.Panel4.Controls.Add(Me.RadioButton11)
        Me.Panel4.Location = New System.Drawing.Point(571, 81)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(393, 30)
        Me.Panel4.TabIndex = 41
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(3, 5)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(107, 18)
        Me.Label20.TabIndex = 29
        Me.Label20.Text = "Executor Type:"
        '
        'RadioButton14
        '
        Me.RadioButton14.AutoSize = True
        Me.RadioButton14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton14.Location = New System.Drawing.Point(131, 3)
        Me.RadioButton14.Name = "RadioButton14"
        Me.RadioButton14.Size = New System.Drawing.Size(56, 22)
        Me.RadioButton14.TabIndex = 35
        Me.RadioButton14.TabStop = True
        Me.RadioButton14.Text = "Sole"
        Me.RadioButton14.UseVisualStyleBackColor = True
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton10.Location = New System.Drawing.Point(213, 3)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(58, 22)
        Me.RadioButton10.TabIndex = 33
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "Joint"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'RadioButton11
        '
        Me.RadioButton11.AutoSize = True
        Me.RadioButton11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton11.Location = New System.Drawing.Point(297, 3)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(91, 22)
        Me.RadioButton11.TabIndex = 34
        Me.RadioButton11.TabStop = True
        Me.RadioButton11.Text = "Substitute"
        Me.RadioButton11.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.RadioButton12)
        Me.Panel5.Controls.Add(Me.RadioButton13)
        Me.Panel5.Location = New System.Drawing.Point(19, 45)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(523, 30)
        Me.Panel5.TabIndex = 39
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(5, 5)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(214, 18)
        Me.Label17.TabIndex = 26
        Me.Label17.Text = "Do you require a Probate Plan?"
        '
        'RadioButton12
        '
        Me.RadioButton12.AutoSize = True
        Me.RadioButton12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton12.Location = New System.Drawing.Point(412, 5)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton12.TabIndex = 27
        Me.RadioButton12.TabStop = True
        Me.RadioButton12.Text = "Yes"
        Me.RadioButton12.UseVisualStyleBackColor = True
        '
        'RadioButton13
        '
        Me.RadioButton13.AutoSize = True
        Me.RadioButton13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton13.Location = New System.Drawing.Point(467, 5)
        Me.RadioButton13.Name = "RadioButton13"
        Me.RadioButton13.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton13.TabIndex = 28
        Me.RadioButton13.TabStop = True
        Me.RadioButton13.Text = "No"
        Me.RadioButton13.UseVisualStyleBackColor = True
        '
        'DataGridView5
        '
        Me.DataGridView5.AllowUserToAddRows = False
        Me.DataGridView5.AllowUserToDeleteRows = False
        Me.DataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn12, Me.Column1, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewLinkColumn4})
        Me.DataGridView5.Location = New System.Drawing.Point(19, 269)
        Me.DataGridView5.Name = "DataGridView5"
        Me.DataGridView5.ReadOnly = True
        Me.DataGridView5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView5.Size = New System.Drawing.Size(940, 188)
        Me.DataGridView5.TabIndex = 38
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 270
        '
        'Column1
        '
        Me.Column1.HeaderText = "Executor Type"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 150
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 180
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 180
        '
        'DataGridViewLinkColumn4
        '
        Me.DataGridViewLinkColumn4.HeaderText = "Action"
        Me.DataGridViewLinkColumn4.Name = "DataGridViewLinkColumn4"
        Me.DataGridViewLinkColumn4.ReadOnly = True
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button6.ForeColor = System.Drawing.Color.black
        Me.Button6.Location = New System.Drawing.Point(789, 227)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(170, 36)
        Me.Button6.TabIndex = 37
        Me.Button6.Text = "Add An Executor"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 20)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Executors"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage4.Controls.Add(Me.DataGridView2)
        Me.TabPage4.Controls.Add(Me.Button3)
        Me.TabPage4.Controls.Add(Me.Label23)
        Me.TabPage4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage4.Location = New System.Drawing.Point(4, 39)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Guardians"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewLinkColumn2})
        Me.DataGridView2.Location = New System.Drawing.Point(16, 68)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView2.Size = New System.Drawing.Size(940, 188)
        Me.DataGridView2.TabIndex = 40
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 270
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Guardian Type"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 150
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 180
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 180
        '
        'DataGridViewLinkColumn2
        '
        Me.DataGridViewLinkColumn2.HeaderText = "Action"
        Me.DataGridViewLinkColumn2.Name = "DataGridViewLinkColumn2"
        Me.DataGridViewLinkColumn2.ReadOnly = True
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.black
        Me.Button3.Location = New System.Drawing.Point(786, 26)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(170, 36)
        Me.Button3.TabIndex = 39
        Me.Button3.Text = "Add A Guardian"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(12, 20)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(83, 20)
        Me.Label23.TabIndex = 8
        Me.Label23.Text = "Guardians"
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage5.Controls.Add(Me.GroupBox3)
        Me.TabPage5.Controls.Add(Me.GroupBox4)
        Me.TabPage5.Controls.Add(Me.Label24)
        Me.TabPage5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage5.Location = New System.Drawing.Point(4, 39)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Funeral Arrangements"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Panel13)
        Me.GroupBox3.Controls.Add(Me.Panel14)
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.TextBox10)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(626, 59)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(541, 310)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Client 2"
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.RadioButton32)
        Me.Panel13.Controls.Add(Me.Label27)
        Me.Panel13.Controls.Add(Me.RadioButton33)
        Me.Panel13.Controls.Add(Me.RadioButton34)
        Me.Panel13.Location = New System.Drawing.Point(8, 35)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(498, 30)
        Me.Panel13.TabIndex = 46
        '
        'RadioButton32
        '
        Me.RadioButton32.AutoSize = True
        Me.RadioButton32.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton32.Location = New System.Drawing.Point(321, 5)
        Me.RadioButton32.Name = "RadioButton32"
        Me.RadioButton32.Size = New System.Drawing.Size(113, 22)
        Me.RadioButton32.TabIndex = 32
        Me.RadioButton32.TabStop = True
        Me.RadioButton32.Text = "Not Required"
        Me.RadioButton32.UseVisualStyleBackColor = True
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(5, 7)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(97, 18)
        Me.Label27.TabIndex = 29
        Me.Label27.Text = "Funeral Type:"
        '
        'RadioButton33
        '
        Me.RadioButton33.AutoSize = True
        Me.RadioButton33.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton33.Location = New System.Drawing.Point(133, 5)
        Me.RadioButton33.Name = "RadioButton33"
        Me.RadioButton33.Size = New System.Drawing.Size(95, 22)
        Me.RadioButton33.TabIndex = 30
        Me.RadioButton33.TabStop = True
        Me.RadioButton33.Text = "Cremation"
        Me.RadioButton33.UseVisualStyleBackColor = True
        '
        'RadioButton34
        '
        Me.RadioButton34.AutoSize = True
        Me.RadioButton34.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton34.Location = New System.Drawing.Point(243, 5)
        Me.RadioButton34.Name = "RadioButton34"
        Me.RadioButton34.Size = New System.Drawing.Size(63, 22)
        Me.RadioButton34.TabIndex = 31
        Me.RadioButton34.TabStop = True
        Me.RadioButton34.Text = "Burial"
        Me.RadioButton34.UseVisualStyleBackColor = True
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.Label29)
        Me.Panel14.Controls.Add(Me.RadioButton35)
        Me.Panel14.Controls.Add(Me.RadioButton36)
        Me.Panel14.Location = New System.Drawing.Point(8, 71)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(335, 30)
        Me.Panel14.TabIndex = 45
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(5, 7)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(190, 18)
        Me.Label29.TabIndex = 29
        Me.Label29.Text = "Do you have a funeral plan?"
        '
        'RadioButton35
        '
        Me.RadioButton35.AutoSize = True
        Me.RadioButton35.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton35.Location = New System.Drawing.Point(214, 5)
        Me.RadioButton35.Name = "RadioButton35"
        Me.RadioButton35.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton35.TabIndex = 30
        Me.RadioButton35.TabStop = True
        Me.RadioButton35.Text = "Yes"
        Me.RadioButton35.UseVisualStyleBackColor = True
        '
        'RadioButton36
        '
        Me.RadioButton36.AutoSize = True
        Me.RadioButton36.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton36.Location = New System.Drawing.Point(269, 5)
        Me.RadioButton36.Name = "RadioButton36"
        Me.RadioButton36.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton36.TabIndex = 31
        Me.RadioButton36.TabStop = True
        Me.RadioButton36.Text = "No"
        Me.RadioButton36.UseVisualStyleBackColor = True
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(13, 114)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(110, 18)
        Me.Label30.TabIndex = 43
        Me.Label30.Text = "Special Wishes"
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(129, 114)
        Me.TextBox10.Multiline = True
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(385, 177)
        Me.TextBox10.TabIndex = 44
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Panel12)
        Me.GroupBox4.Controls.Add(Me.Panel11)
        Me.GroupBox4.Controls.Add(Me.TextBox15)
        Me.GroupBox4.Controls.Add(Me.Label28)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(16, 59)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(541, 310)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Client 1"
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.RadioButton31)
        Me.Panel12.Controls.Add(Me.Label26)
        Me.Panel12.Controls.Add(Me.RadioButton29)
        Me.Panel12.Controls.Add(Me.RadioButton30)
        Me.Panel12.Location = New System.Drawing.Point(9, 35)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(498, 30)
        Me.Panel12.TabIndex = 42
        '
        'RadioButton31
        '
        Me.RadioButton31.AutoSize = True
        Me.RadioButton31.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton31.Location = New System.Drawing.Point(321, 5)
        Me.RadioButton31.Name = "RadioButton31"
        Me.RadioButton31.Size = New System.Drawing.Size(113, 22)
        Me.RadioButton31.TabIndex = 32
        Me.RadioButton31.TabStop = True
        Me.RadioButton31.Text = "Not Required"
        Me.RadioButton31.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(5, 7)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(97, 18)
        Me.Label26.TabIndex = 29
        Me.Label26.Text = "Funeral Type:"
        '
        'RadioButton29
        '
        Me.RadioButton29.AutoSize = True
        Me.RadioButton29.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton29.Location = New System.Drawing.Point(133, 5)
        Me.RadioButton29.Name = "RadioButton29"
        Me.RadioButton29.Size = New System.Drawing.Size(95, 22)
        Me.RadioButton29.TabIndex = 30
        Me.RadioButton29.TabStop = True
        Me.RadioButton29.Text = "Cremation"
        Me.RadioButton29.UseVisualStyleBackColor = True
        '
        'RadioButton30
        '
        Me.RadioButton30.AutoSize = True
        Me.RadioButton30.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton30.Location = New System.Drawing.Point(243, 5)
        Me.RadioButton30.Name = "RadioButton30"
        Me.RadioButton30.Size = New System.Drawing.Size(63, 22)
        Me.RadioButton30.TabIndex = 31
        Me.RadioButton30.TabStop = True
        Me.RadioButton30.Text = "Burial"
        Me.RadioButton30.UseVisualStyleBackColor = True
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.Label25)
        Me.Panel11.Controls.Add(Me.RadioButton27)
        Me.Panel11.Controls.Add(Me.RadioButton28)
        Me.Panel11.Location = New System.Drawing.Point(9, 71)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(335, 30)
        Me.Panel11.TabIndex = 41
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(5, 7)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(190, 18)
        Me.Label25.TabIndex = 29
        Me.Label25.Text = "Do you have a funeral plan?"
        '
        'RadioButton27
        '
        Me.RadioButton27.AutoSize = True
        Me.RadioButton27.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton27.Location = New System.Drawing.Point(214, 5)
        Me.RadioButton27.Name = "RadioButton27"
        Me.RadioButton27.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton27.TabIndex = 30
        Me.RadioButton27.TabStop = True
        Me.RadioButton27.Text = "Yes"
        Me.RadioButton27.UseVisualStyleBackColor = True
        '
        'RadioButton28
        '
        Me.RadioButton28.AutoSize = True
        Me.RadioButton28.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton28.Location = New System.Drawing.Point(269, 5)
        Me.RadioButton28.Name = "RadioButton28"
        Me.RadioButton28.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton28.TabIndex = 31
        Me.RadioButton28.TabStop = True
        Me.RadioButton28.Text = "No"
        Me.RadioButton28.UseVisualStyleBackColor = True
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(130, 114)
        Me.TextBox15.Multiline = True
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(385, 177)
        Me.TextBox15.TabIndex = 18
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(14, 114)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(110, 18)
        Me.Label28.TabIndex = 15
        Me.Label28.Text = "Special Wishes"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(12, 20)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(168, 20)
        Me.Label24.TabIndex = 6
        Me.Label24.Text = "Funeral Arrangements"
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage6.Controls.Add(Me.DataGridView3)
        Me.TabPage6.Controls.Add(Me.Button4)
        Me.TabPage6.Controls.Add(Me.Panel15)
        Me.TabPage6.Controls.Add(Me.Label31)
        Me.TabPage6.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage6.Location = New System.Drawing.Point(4, 39)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Legacies"
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToAddRows = False
        Me.DataGridView3.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Beige
        Me.DataGridView3.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.Column2, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewLinkColumn3})
        Me.DataGridView3.Location = New System.Drawing.Point(25, 105)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.ReadOnly = True
        Me.DataGridView3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView3.Size = New System.Drawing.Size(1149, 341)
        Me.DataGridView3.TabIndex = 42
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Recipient"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 270
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Gift From"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Width = 150
        '
        'Column2
        '
        Me.Column2.HeaderText = "Item"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 200
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Width = 180
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Width = 180
        '
        'DataGridViewLinkColumn3
        '
        Me.DataGridViewLinkColumn3.HeaderText = "Action"
        Me.DataGridViewLinkColumn3.Name = "DataGridViewLinkColumn3"
        Me.DataGridViewLinkColumn3.ReadOnly = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button4.ForeColor = System.Drawing.Color.black
        Me.Button4.Location = New System.Drawing.Point(1004, 63)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(170, 36)
        Me.Button4.TabIndex = 41
        Me.Button4.Text = "Add A Legacy"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.Label32)
        Me.Panel15.Controls.Add(Me.RadioButton37)
        Me.Panel15.Controls.Add(Me.RadioButton38)
        Me.Panel15.Location = New System.Drawing.Point(19, 45)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(479, 30)
        Me.Panel15.TabIndex = 40
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(3, 7)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(342, 18)
        Me.Label32.TabIndex = 26
        Me.Label32.Text = "Do you want to include a memorandum of wishes?"
        '
        'RadioButton37
        '
        Me.RadioButton37.AutoSize = True
        Me.RadioButton37.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton37.Location = New System.Drawing.Point(361, 5)
        Me.RadioButton37.Name = "RadioButton37"
        Me.RadioButton37.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton37.TabIndex = 27
        Me.RadioButton37.TabStop = True
        Me.RadioButton37.Text = "Yes"
        Me.RadioButton37.UseVisualStyleBackColor = True
        '
        'RadioButton38
        '
        Me.RadioButton38.AutoSize = True
        Me.RadioButton38.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton38.Location = New System.Drawing.Point(416, 5)
        Me.RadioButton38.Name = "RadioButton38"
        Me.RadioButton38.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton38.TabIndex = 28
        Me.RadioButton38.TabStop = True
        Me.RadioButton38.Text = "No"
        Me.RadioButton38.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(12, 20)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(73, 20)
        Me.Label31.TabIndex = 7
        Me.Label31.Text = "Legacies"
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage7.Controls.Add(Me.TabControl2)
        Me.TabPage7.Controls.Add(Me.Panel17)
        Me.TabPage7.Controls.Add(Me.Label38)
        Me.TabPage7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage7.Location = New System.Drawing.Point(4, 39)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Trusts"
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage11)
        Me.TabControl2.Controls.Add(Me.TabPage12)
        Me.TabControl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl2.Location = New System.Drawing.Point(19, 80)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.Padding = New System.Drawing.Point(5, 5)
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1167, 385)
        Me.TabControl2.TabIndex = 42
        '
        'TabPage11
        '
        Me.TabPage11.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage11.Controls.Add(Me.Button5)
        Me.TabPage11.Controls.Add(Me.DataGridView4)
        Me.TabPage11.Controls.Add(Me.Panel23)
        Me.TabPage11.Controls.Add(Me.Panel22)
        Me.TabPage11.Controls.Add(Me.Panel21)
        Me.TabPage11.Controls.Add(Me.Panel20)
        Me.TabPage11.Controls.Add(Me.Panel19)
        Me.TabPage11.Controls.Add(Me.Panel18)
        Me.TabPage11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage11.Location = New System.Drawing.Point(4, 29)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(5)
        Me.TabPage11.Size = New System.Drawing.Size(1159, 352)
        Me.TabPage11.TabIndex = 0
        Me.TabPage11.Text = "PPT Details"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button5.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button5.ForeColor = System.Drawing.Color.black
        Me.Button5.Location = New System.Drawing.Point(902, 146)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(159, 36)
        Me.Button5.TabIndex = 49
        Me.Button5.Text = "Add A Beneficiary"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'DataGridView4
        '
        Me.DataGridView4.AllowUserToAddRows = False
        Me.DataGridView4.AllowUserToDeleteRows = False
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24, Me.Column3, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn26, Me.DataGridViewLinkColumn5})
        Me.DataGridView4.Location = New System.Drawing.Point(8, 189)
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.ReadOnly = True
        Me.DataGridView4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView4.Size = New System.Drawing.Size(1053, 155)
        Me.DataGridView4.TabIndex = 48
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Width = 270
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "Lapse/Issue"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Width = 150
        '
        'Column3
        '
        Me.Column3.HeaderText = "Percentage"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Width = 180
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.Width = 180
        '
        'DataGridViewLinkColumn5
        '
        Me.DataGridViewLinkColumn5.HeaderText = "Action"
        Me.DataGridViewLinkColumn5.Name = "DataGridViewLinkColumn5"
        Me.DataGridViewLinkColumn5.ReadOnly = True
        '
        'Panel23
        '
        Me.Panel23.Controls.Add(Me.RadioButton54)
        Me.Panel23.Controls.Add(Me.Label48)
        Me.Panel23.Controls.Add(Me.RadioButton55)
        Me.Panel23.Location = New System.Drawing.Point(559, 149)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(236, 30)
        Me.Panel23.TabIndex = 47
        '
        'RadioButton54
        '
        Me.RadioButton54.AutoSize = True
        Me.RadioButton54.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton54.Location = New System.Drawing.Point(114, 5)
        Me.RadioButton54.Name = "RadioButton54"
        Me.RadioButton54.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton54.TabIndex = 29
        Me.RadioButton54.TabStop = True
        Me.RadioButton54.Text = "Yes"
        Me.RadioButton54.UseVisualStyleBackColor = True
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(3, 7)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(105, 18)
        Me.Label48.TabIndex = 26
        Me.Label48.Text = "Then to issue?"
        '
        'RadioButton55
        '
        Me.RadioButton55.AutoSize = True
        Me.RadioButton55.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton55.Location = New System.Drawing.Point(184, 5)
        Me.RadioButton55.Name = "RadioButton55"
        Me.RadioButton55.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton55.TabIndex = 27
        Me.RadioButton55.TabStop = True
        Me.RadioButton55.Text = "No"
        Me.RadioButton55.UseVisualStyleBackColor = True
        '
        'Panel22
        '
        Me.Panel22.Controls.Add(Me.RadioButton52)
        Me.Panel22.Controls.Add(Me.Label47)
        Me.Panel22.Controls.Add(Me.RadioButton53)
        Me.Panel22.Location = New System.Drawing.Point(8, 149)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(545, 30)
        Me.Panel22.TabIndex = 46
        '
        'RadioButton52
        '
        Me.RadioButton52.AutoSize = True
        Me.RadioButton52.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton52.Location = New System.Drawing.Point(423, 4)
        Me.RadioButton52.Name = "RadioButton52"
        Me.RadioButton52.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton52.TabIndex = 29
        Me.RadioButton52.TabStop = True
        Me.RadioButton52.Text = "Yes"
        Me.RadioButton52.UseVisualStyleBackColor = True
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(3, 7)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(392, 18)
        Me.Label47.TabIndex = 26
        Me.Label47.Text = "Beneficial interest passing EQUALLY to bloodline children?"
        '
        'RadioButton53
        '
        Me.RadioButton53.AutoSize = True
        Me.RadioButton53.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton53.Location = New System.Drawing.Point(493, 4)
        Me.RadioButton53.Name = "RadioButton53"
        Me.RadioButton53.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton53.TabIndex = 27
        Me.RadioButton53.TabStop = True
        Me.RadioButton53.Text = "No"
        Me.RadioButton53.UseVisualStyleBackColor = True
        '
        'Panel21
        '
        Me.Panel21.Controls.Add(Me.RadioButton50)
        Me.Panel21.Controls.Add(Me.Label45)
        Me.Panel21.Controls.Add(Me.RadioButton51)
        Me.Panel21.Location = New System.Drawing.Point(8, 113)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(693, 30)
        Me.Panel21.TabIndex = 45
        '
        'RadioButton50
        '
        Me.RadioButton50.AutoSize = True
        Me.RadioButton50.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton50.Location = New System.Drawing.Point(352, 5)
        Me.RadioButton50.Name = "RadioButton50"
        Me.RadioButton50.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton50.TabIndex = 29
        Me.RadioButton50.TabStop = True
        Me.RadioButton50.Text = "Yes"
        Me.RadioButton50.UseVisualStyleBackColor = True
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(3, 7)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(279, 18)
        Me.Label45.TabIndex = 26
        Me.Label45.Text = "Mortgage to be discharged from residue?"
        '
        'RadioButton51
        '
        Me.RadioButton51.AutoSize = True
        Me.RadioButton51.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton51.Location = New System.Drawing.Point(422, 5)
        Me.RadioButton51.Name = "RadioButton51"
        Me.RadioButton51.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton51.TabIndex = 27
        Me.RadioButton51.TabStop = True
        Me.RadioButton51.Text = "No"
        Me.RadioButton51.UseVisualStyleBackColor = True
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.RadioButton48)
        Me.Panel20.Controls.Add(Me.Label46)
        Me.Panel20.Controls.Add(Me.RadioButton49)
        Me.Panel20.Location = New System.Drawing.Point(8, 80)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(693, 30)
        Me.Panel20.TabIndex = 44
        '
        'RadioButton48
        '
        Me.RadioButton48.AutoSize = True
        Me.RadioButton48.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton48.Location = New System.Drawing.Point(352, 5)
        Me.RadioButton48.Name = "RadioButton48"
        Me.RadioButton48.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton48.TabIndex = 29
        Me.RadioButton48.TabStop = True
        Me.RadioButton48.Text = "Yes"
        Me.RadioButton48.UseVisualStyleBackColor = True
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(3, 7)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(343, 18)
        Me.Label46.TabIndex = 26
        Me.Label46.Text = "Occupation to cease on co-habitation/re-marriage?"
        '
        'RadioButton49
        '
        Me.RadioButton49.AutoSize = True
        Me.RadioButton49.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton49.Location = New System.Drawing.Point(422, 5)
        Me.RadioButton49.Name = "RadioButton49"
        Me.RadioButton49.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton49.TabIndex = 27
        Me.RadioButton49.TabStop = True
        Me.RadioButton49.Text = "No"
        Me.RadioButton49.UseVisualStyleBackColor = True
        '
        'Panel19
        '
        Me.Panel19.Controls.Add(Me.TextBox13)
        Me.Panel19.Controls.Add(Me.Label43)
        Me.Panel19.Controls.Add(Me.RadioButton46)
        Me.Panel19.Controls.Add(Me.Label44)
        Me.Panel19.Controls.Add(Me.RadioButton47)
        Me.Panel19.Location = New System.Drawing.Point(8, 44)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(693, 30)
        Me.Panel19.TabIndex = 43
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(422, 5)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(255, 22)
        Me.TextBox13.TabIndex = 31
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(355, 7)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(61, 18)
        Me.Label43.TabIndex = 30
        Me.Label43.Text = " -  who?"
        '
        'RadioButton46
        '
        Me.RadioButton46.AutoSize = True
        Me.RadioButton46.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton46.Location = New System.Drawing.Point(121, 5)
        Me.RadioButton46.Name = "RadioButton46"
        Me.RadioButton46.Size = New System.Drawing.Size(77, 22)
        Me.RadioButton46.TabIndex = 29
        Me.RadioButton46.TabStop = True
        Me.RadioButton46.Text = "Spouse"
        Me.RadioButton46.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(3, 7)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(98, 18)
        Me.Label44.TabIndex = 26
        Me.Label44.Text = "Residency to:"
        '
        'RadioButton47
        '
        Me.RadioButton47.AutoSize = True
        Me.RadioButton47.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton47.Location = New System.Drawing.Point(239, 5)
        Me.RadioButton47.Name = "RadioButton47"
        Me.RadioButton47.Size = New System.Drawing.Size(122, 22)
        Me.RadioButton47.TabIndex = 27
        Me.RadioButton47.TabStop = True
        Me.RadioButton47.Text = "Someone else"
        Me.RadioButton47.UseVisualStyleBackColor = True
        '
        'Panel18
        '
        Me.Panel18.Controls.Add(Me.TextBox12)
        Me.Panel18.Controls.Add(Me.Label42)
        Me.Panel18.Controls.Add(Me.RadioButton44)
        Me.Panel18.Controls.Add(Me.Label40)
        Me.Panel18.Controls.Add(Me.RadioButton45)
        Me.Panel18.Location = New System.Drawing.Point(8, 8)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(693, 30)
        Me.Panel18.TabIndex = 42
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(489, 5)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(188, 22)
        Me.TextBox12.TabIndex = 31
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(367, 7)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(115, 18)
        Me.Label42.TabIndex = 30
        Me.Label42.Text = " -  for how long?"
        '
        'RadioButton44
        '
        Me.RadioButton44.AutoSize = True
        Me.RadioButton44.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton44.Location = New System.Drawing.Point(121, 5)
        Me.RadioButton44.Name = "RadioButton44"
        Me.RadioButton44.Size = New System.Drawing.Size(101, 22)
        Me.RadioButton44.TabIndex = 29
        Me.RadioButton44.TabStop = True
        Me.RadioButton44.Text = "Life Interest"
        Me.RadioButton44.UseVisualStyleBackColor = True
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(3, 7)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(112, 18)
        Me.Label40.TabIndex = 26
        Me.Label40.Text = "Residency type:"
        '
        'RadioButton45
        '
        Me.RadioButton45.AutoSize = True
        Me.RadioButton45.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton45.Location = New System.Drawing.Point(239, 5)
        Me.RadioButton45.Name = "RadioButton45"
        Me.RadioButton45.Size = New System.Drawing.Size(132, 22)
        Me.RadioButton45.TabIndex = 27
        Me.RadioButton45.TabStop = True
        Me.RadioButton45.Text = "Right To Reside"
        Me.RadioButton45.UseVisualStyleBackColor = True
        '
        'TabPage12
        '
        Me.TabPage12.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage12.Controls.Add(Me.TextBox14)
        Me.TabPage12.Controls.Add(Me.Label52)
        Me.TabPage12.Controls.Add(Me.Button7)
        Me.TabPage12.Controls.Add(Me.DataGridView6)
        Me.TabPage12.Controls.Add(Me.Panel24)
        Me.TabPage12.Controls.Add(Me.Panel25)
        Me.TabPage12.Controls.Add(Me.Panel26)
        Me.TabPage12.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage12.Location = New System.Drawing.Point(4, 29)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Padding = New System.Windows.Forms.Padding(5)
        Me.TabPage12.Size = New System.Drawing.Size(1159, 352)
        Me.TabPage12.TabIndex = 1
        Me.TabPage12.Text = "NRB Details"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(136, 219)
        Me.TextBox14.Multiline = True
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox14.Size = New System.Drawing.Size(1004, 118)
        Me.TextBox14.TabIndex = 56
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.Location = New System.Drawing.Point(11, 219)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(119, 18)
        Me.Label52.TabIndex = 55
        Me.Label52.Text = "Letter Of Wishes"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button7.ForeColor = System.Drawing.Color.black
        Me.Button7.Location = New System.Drawing.Point(981, 38)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(159, 36)
        Me.Button7.TabIndex = 54
        Me.Button7.Text = "Add A Beneficiary"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'DataGridView6
        '
        Me.DataGridView6.AllowUserToAddRows = False
        Me.DataGridView6.AllowUserToDeleteRows = False
        Me.DataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView6.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.Column5, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewLinkColumn7})
        Me.DataGridView6.Location = New System.Drawing.Point(8, 84)
        Me.DataGridView6.Name = "DataGridView6"
        Me.DataGridView6.ReadOnly = True
        Me.DataGridView6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView6.Size = New System.Drawing.Size(1132, 115)
        Me.DataGridView6.TabIndex = 53
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.Visible = False
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.Width = 270
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "Lapse/Issue"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.Width = 150
        '
        'Column5
        '
        Me.Column5.HeaderText = "Primary?"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 80
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "Percentage"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.Width = 180
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        Me.DataGridViewTextBoxColumn32.Width = 180
        '
        'DataGridViewLinkColumn7
        '
        Me.DataGridViewLinkColumn7.HeaderText = "Action"
        Me.DataGridViewLinkColumn7.Name = "DataGridViewLinkColumn7"
        Me.DataGridViewLinkColumn7.ReadOnly = True
        '
        'Panel24
        '
        Me.Panel24.Controls.Add(Me.RadioButton56)
        Me.Panel24.Controls.Add(Me.Label49)
        Me.Panel24.Controls.Add(Me.RadioButton57)
        Me.Panel24.Location = New System.Drawing.Point(491, 44)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Size = New System.Drawing.Size(236, 30)
        Me.Panel24.TabIndex = 52
        '
        'RadioButton56
        '
        Me.RadioButton56.AutoSize = True
        Me.RadioButton56.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton56.Location = New System.Drawing.Point(114, 5)
        Me.RadioButton56.Name = "RadioButton56"
        Me.RadioButton56.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton56.TabIndex = 29
        Me.RadioButton56.TabStop = True
        Me.RadioButton56.Text = "Yes"
        Me.RadioButton56.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(3, 7)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(105, 18)
        Me.Label49.TabIndex = 26
        Me.Label49.Text = "Then to issue?"
        '
        'RadioButton57
        '
        Me.RadioButton57.AutoSize = True
        Me.RadioButton57.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton57.Location = New System.Drawing.Point(184, 5)
        Me.RadioButton57.Name = "RadioButton57"
        Me.RadioButton57.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton57.TabIndex = 27
        Me.RadioButton57.TabStop = True
        Me.RadioButton57.Text = "No"
        Me.RadioButton57.UseVisualStyleBackColor = True
        '
        'Panel25
        '
        Me.Panel25.Controls.Add(Me.RadioButton58)
        Me.Panel25.Controls.Add(Me.Label50)
        Me.Panel25.Controls.Add(Me.RadioButton59)
        Me.Panel25.Location = New System.Drawing.Point(8, 44)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Size = New System.Drawing.Size(477, 30)
        Me.Panel25.TabIndex = 51
        '
        'RadioButton58
        '
        Me.RadioButton58.AutoSize = True
        Me.RadioButton58.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton58.Location = New System.Drawing.Point(355, 5)
        Me.RadioButton58.Name = "RadioButton58"
        Me.RadioButton58.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton58.TabIndex = 29
        Me.RadioButton58.TabStop = True
        Me.RadioButton58.Text = "Yes"
        Me.RadioButton58.UseVisualStyleBackColor = True
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(3, 7)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(345, 18)
        Me.Label50.TabIndex = 26
        Me.Label50.Text = "Bloodline children EQUAL secondary beneficiaries?"
        '
        'RadioButton59
        '
        Me.RadioButton59.AutoSize = True
        Me.RadioButton59.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton59.Location = New System.Drawing.Point(425, 5)
        Me.RadioButton59.Name = "RadioButton59"
        Me.RadioButton59.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton59.TabIndex = 27
        Me.RadioButton59.TabStop = True
        Me.RadioButton59.Text = "No"
        Me.RadioButton59.UseVisualStyleBackColor = True
        '
        'Panel26
        '
        Me.Panel26.Controls.Add(Me.RadioButton60)
        Me.Panel26.Controls.Add(Me.Label51)
        Me.Panel26.Controls.Add(Me.RadioButton61)
        Me.Panel26.Location = New System.Drawing.Point(8, 8)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Size = New System.Drawing.Size(693, 30)
        Me.Panel26.TabIndex = 50
        '
        'RadioButton60
        '
        Me.RadioButton60.AutoSize = True
        Me.RadioButton60.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton60.Location = New System.Drawing.Point(285, 5)
        Me.RadioButton60.Name = "RadioButton60"
        Me.RadioButton60.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton60.TabIndex = 29
        Me.RadioButton60.TabStop = True
        Me.RadioButton60.Text = "Yes"
        Me.RadioButton60.UseVisualStyleBackColor = True
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(3, 7)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(257, 18)
        Me.Label51.TabIndex = 26
        Me.Label51.Text = "Is spouse/partner primary beneficiary?"
        '
        'RadioButton61
        '
        Me.RadioButton61.AutoSize = True
        Me.RadioButton61.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton61.Location = New System.Drawing.Point(355, 5)
        Me.RadioButton61.Name = "RadioButton61"
        Me.RadioButton61.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton61.TabIndex = 27
        Me.RadioButton61.TabStop = True
        Me.RadioButton61.Text = "No"
        Me.RadioButton61.UseVisualStyleBackColor = True
        '
        'Panel17
        '
        Me.Panel17.Controls.Add(Me.RadioButton43)
        Me.Panel17.Controls.Add(Me.Label39)
        Me.Panel17.Controls.Add(Me.RadioButton41)
        Me.Panel17.Controls.Add(Me.RadioButton42)
        Me.Panel17.Location = New System.Drawing.Point(19, 45)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(618, 30)
        Me.Panel17.TabIndex = 41
        '
        'RadioButton43
        '
        Me.RadioButton43.AutoSize = True
        Me.RadioButton43.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton43.Location = New System.Drawing.Point(187, 5)
        Me.RadioButton43.Name = "RadioButton43"
        Me.RadioButton43.Size = New System.Drawing.Size(62, 22)
        Me.RadioButton43.TabIndex = 29
        Me.RadioButton43.TabStop = True
        Me.RadioButton43.Text = "None"
        Me.RadioButton43.UseVisualStyleBackColor = True
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(3, 7)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(151, 18)
        Me.Label39.TabIndex = 26
        Me.Label39.Text = "Type of trust required:"
        '
        'RadioButton41
        '
        Me.RadioButton41.AutoSize = True
        Me.RadioButton41.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton41.Location = New System.Drawing.Point(272, 5)
        Me.RadioButton41.Name = "RadioButton41"
        Me.RadioButton41.Size = New System.Drawing.Size(190, 22)
        Me.RadioButton41.TabIndex = 27
        Me.RadioButton41.TabStop = True
        Me.RadioButton41.Text = "Protective Property Trust"
        Me.RadioButton41.UseVisualStyleBackColor = True
        '
        'RadioButton42
        '
        Me.RadioButton42.AutoSize = True
        Me.RadioButton42.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton42.Location = New System.Drawing.Point(485, 5)
        Me.RadioButton42.Name = "RadioButton42"
        Me.RadioButton42.Size = New System.Drawing.Size(116, 22)
        Me.RadioButton42.TabIndex = 28
        Me.RadioButton42.TabStop = True
        Me.RadioButton42.Text = "Nil Rate Band"
        Me.RadioButton42.UseVisualStyleBackColor = True
        Me.RadioButton42.Visible = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(12, 20)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(168, 20)
        Me.Label38.TabIndex = 8
        Me.Label38.Text = "Trusts Before Residue"
        '
        'TabPage8
        '
        Me.TabPage8.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage8.Controls.Add(Me.TabControl3)
        Me.TabPage8.Controls.Add(Me.Panel27)
        Me.TabPage8.Controls.Add(Me.Label53)
        Me.TabPage8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage8.Location = New System.Drawing.Point(4, 39)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = "Residuary Estate"
        '
        'TabControl3
        '
        Me.TabControl3.Controls.Add(Me.TabPage13)
        Me.TabControl3.Controls.Add(Me.TabPage14)
        Me.TabControl3.Controls.Add(Me.TabPage15)
        Me.TabControl3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl3.Location = New System.Drawing.Point(19, 81)
        Me.TabControl3.Name = "TabControl3"
        Me.TabControl3.Padding = New System.Drawing.Point(5, 5)
        Me.TabControl3.SelectedIndex = 0
        Me.TabControl3.Size = New System.Drawing.Size(1167, 385)
        Me.TabControl3.TabIndex = 43
        '
        'TabPage13
        '
        Me.TabPage13.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage13.Controls.Add(Me.Button9)
        Me.TabPage13.Controls.Add(Me.DataGridView8)
        Me.TabPage13.Controls.Add(Me.Panel28)
        Me.TabPage13.Controls.Add(Me.Panel29)
        Me.TabPage13.Controls.Add(Me.Panel30)
        Me.TabPage13.Controls.Add(Me.Panel31)
        Me.TabPage13.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage13.Location = New System.Drawing.Point(4, 29)
        Me.TabPage13.Name = "TabPage13"
        Me.TabPage13.Padding = New System.Windows.Forms.Padding(5)
        Me.TabPage13.Size = New System.Drawing.Size(1159, 352)
        Me.TabPage13.TabIndex = 0
        Me.TabPage13.Text = "Direct Distribution Details"
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button9.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button9.ForeColor = System.Drawing.Color.black
        Me.Button9.Location = New System.Drawing.Point(894, 107)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(159, 36)
        Me.Button9.TabIndex = 49
        Me.Button9.Text = "Add A Beneficiary"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'DataGridView8
        '
        Me.DataGridView8.AllowUserToAddRows = False
        Me.DataGridView8.AllowUserToDeleteRows = False
        Me.DataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView8.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34, Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38, Me.DataGridViewLinkColumn8})
        Me.DataGridView8.Location = New System.Drawing.Point(5, 149)
        Me.DataGridView8.Name = "DataGridView8"
        Me.DataGridView8.ReadOnly = True
        Me.DataGridView8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView8.Size = New System.Drawing.Size(1048, 155)
        Me.DataGridView8.TabIndex = 48
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.Width = 270
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "Lapse/Issue"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.ReadOnly = True
        Me.DataGridViewTextBoxColumn35.Width = 150
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.HeaderText = "Percentage"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        Me.DataGridViewTextBoxColumn36.ReadOnly = True
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        Me.DataGridViewTextBoxColumn37.ReadOnly = True
        Me.DataGridViewTextBoxColumn37.Width = 180
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        Me.DataGridViewTextBoxColumn38.ReadOnly = True
        Me.DataGridViewTextBoxColumn38.Width = 180
        '
        'DataGridViewLinkColumn8
        '
        Me.DataGridViewLinkColumn8.HeaderText = "Action"
        Me.DataGridViewLinkColumn8.Name = "DataGridViewLinkColumn8"
        Me.DataGridViewLinkColumn8.ReadOnly = True
        '
        'Panel28
        '
        Me.Panel28.Controls.Add(Me.RadioButton66)
        Me.Panel28.Controls.Add(Me.Label55)
        Me.Panel28.Controls.Add(Me.RadioButton67)
        Me.Panel28.Location = New System.Drawing.Point(490, 44)
        Me.Panel28.Name = "Panel28"
        Me.Panel28.Size = New System.Drawing.Size(236, 30)
        Me.Panel28.TabIndex = 47
        '
        'RadioButton66
        '
        Me.RadioButton66.AutoSize = True
        Me.RadioButton66.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton66.Location = New System.Drawing.Point(114, 5)
        Me.RadioButton66.Name = "RadioButton66"
        Me.RadioButton66.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton66.TabIndex = 29
        Me.RadioButton66.TabStop = True
        Me.RadioButton66.Text = "Yes"
        Me.RadioButton66.UseVisualStyleBackColor = True
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(3, 7)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(105, 18)
        Me.Label55.TabIndex = 26
        Me.Label55.Text = "Then to issue?"
        '
        'RadioButton67
        '
        Me.RadioButton67.AutoSize = True
        Me.RadioButton67.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton67.Location = New System.Drawing.Point(184, 5)
        Me.RadioButton67.Name = "RadioButton67"
        Me.RadioButton67.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton67.TabIndex = 27
        Me.RadioButton67.TabStop = True
        Me.RadioButton67.Text = "No"
        Me.RadioButton67.UseVisualStyleBackColor = True
        '
        'Panel29
        '
        Me.Panel29.Controls.Add(Me.RadioButton68)
        Me.Panel29.Controls.Add(Me.Label56)
        Me.Panel29.Controls.Add(Me.RadioButton69)
        Me.Panel29.Location = New System.Drawing.Point(8, 44)
        Me.Panel29.Name = "Panel29"
        Me.Panel29.Size = New System.Drawing.Size(476, 30)
        Me.Panel29.TabIndex = 46
        '
        'RadioButton68
        '
        Me.RadioButton68.AutoSize = True
        Me.RadioButton68.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton68.Location = New System.Drawing.Point(350, 5)
        Me.RadioButton68.Name = "RadioButton68"
        Me.RadioButton68.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton68.TabIndex = 29
        Me.RadioButton68.TabStop = True
        Me.RadioButton68.Text = "Yes"
        Me.RadioButton68.UseVisualStyleBackColor = True
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(3, 7)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(340, 18)
        Me.Label56.TabIndex = 26
        Me.Label56.Text = "Next level passing EQUALLY to bloodline children?"
        '
        'RadioButton69
        '
        Me.RadioButton69.AutoSize = True
        Me.RadioButton69.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton69.Location = New System.Drawing.Point(420, 5)
        Me.RadioButton69.Name = "RadioButton69"
        Me.RadioButton69.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton69.TabIndex = 27
        Me.RadioButton69.TabStop = True
        Me.RadioButton69.Text = "No"
        Me.RadioButton69.UseVisualStyleBackColor = True
        '
        'Panel30
        '
        Me.Panel30.Controls.Add(Me.RadioButton70)
        Me.Panel30.Controls.Add(Me.Label57)
        Me.Panel30.Controls.Add(Me.RadioButton71)
        Me.Panel30.Location = New System.Drawing.Point(8, 8)
        Me.Panel30.Name = "Panel30"
        Me.Panel30.Size = New System.Drawing.Size(476, 30)
        Me.Panel30.TabIndex = 45
        '
        'RadioButton70
        '
        Me.RadioButton70.AutoSize = True
        Me.RadioButton70.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton70.Location = New System.Drawing.Point(350, 5)
        Me.RadioButton70.Name = "RadioButton70"
        Me.RadioButton70.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton70.TabIndex = 29
        Me.RadioButton70.TabStop = True
        Me.RadioButton70.Text = "Yes"
        Me.RadioButton70.UseVisualStyleBackColor = True
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.Location = New System.Drawing.Point(3, 7)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(258, 18)
        Me.Label57.TabIndex = 26
        Me.Label57.Text = "1st level of residue to spouse/partner?"
        '
        'RadioButton71
        '
        Me.RadioButton71.AutoSize = True
        Me.RadioButton71.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton71.Location = New System.Drawing.Point(420, 5)
        Me.RadioButton71.Name = "RadioButton71"
        Me.RadioButton71.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton71.TabIndex = 27
        Me.RadioButton71.TabStop = True
        Me.RadioButton71.Text = "No"
        Me.RadioButton71.UseVisualStyleBackColor = True
        '
        'Panel31
        '
        Me.Panel31.Controls.Add(Me.RadioButton72)
        Me.Panel31.Controls.Add(Me.Label58)
        Me.Panel31.Controls.Add(Me.RadioButton73)
        Me.Panel31.Location = New System.Drawing.Point(8, 80)
        Me.Panel31.Name = "Panel31"
        Me.Panel31.Size = New System.Drawing.Size(476, 30)
        Me.Panel31.TabIndex = 44
        '
        'RadioButton72
        '
        Me.RadioButton72.AutoSize = True
        Me.RadioButton72.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton72.Location = New System.Drawing.Point(350, 5)
        Me.RadioButton72.Name = "RadioButton72"
        Me.RadioButton72.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton72.TabIndex = 29
        Me.RadioButton72.TabStop = True
        Me.RadioButton72.Text = "Yes"
        Me.RadioButton72.UseVisualStyleBackColor = True
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.Location = New System.Drawing.Point(3, 7)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(200, 18)
        Me.Label58.TabIndex = 26
        Me.Label58.Text = "Do you require further levels?"
        '
        'RadioButton73
        '
        Me.RadioButton73.AutoSize = True
        Me.RadioButton73.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton73.Location = New System.Drawing.Point(420, 5)
        Me.RadioButton73.Name = "RadioButton73"
        Me.RadioButton73.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton73.TabIndex = 27
        Me.RadioButton73.TabStop = True
        Me.RadioButton73.Text = "No"
        Me.RadioButton73.UseVisualStyleBackColor = True
        '
        'TabPage14
        '
        Me.TabPage14.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage14.Controls.Add(Me.Panel37)
        Me.TabPage14.Controls.Add(Me.TextBox19)
        Me.TabPage14.Controls.Add(Me.Label63)
        Me.TabPage14.Controls.Add(Me.Button10)
        Me.TabPage14.Controls.Add(Me.DataGridView9)
        Me.TabPage14.Controls.Add(Me.Panel34)
        Me.TabPage14.Controls.Add(Me.Panel35)
        Me.TabPage14.Controls.Add(Me.Panel36)
        Me.TabPage14.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage14.Location = New System.Drawing.Point(4, 29)
        Me.TabPage14.Name = "TabPage14"
        Me.TabPage14.Padding = New System.Windows.Forms.Padding(5)
        Me.TabPage14.Size = New System.Drawing.Size(1159, 352)
        Me.TabPage14.TabIndex = 1
        Me.TabPage14.Text = "FLIT Details"
        '
        'Panel37
        '
        Me.Panel37.Controls.Add(Me.RadioButton84)
        Me.Panel37.Controls.Add(Me.Label62)
        Me.Panel37.Controls.Add(Me.RadioButton85)
        Me.Panel37.Location = New System.Drawing.Point(8, 205)
        Me.Panel37.Name = "Panel37"
        Me.Panel37.Size = New System.Drawing.Size(568, 30)
        Me.Panel37.TabIndex = 51
        '
        'RadioButton84
        '
        Me.RadioButton84.AutoSize = True
        Me.RadioButton84.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton84.Location = New System.Drawing.Point(441, 5)
        Me.RadioButton84.Name = "RadioButton84"
        Me.RadioButton84.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton84.TabIndex = 29
        Me.RadioButton84.TabStop = True
        Me.RadioButton84.Text = "Yes"
        Me.RadioButton84.UseVisualStyleBackColor = True
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(3, 7)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(432, 18)
        Me.Label62.TabIndex = 26
        Me.Label62.Text = "Do you require the fund to be distributed via a discretionary trust?"
        '
        'RadioButton85
        '
        Me.RadioButton85.AutoSize = True
        Me.RadioButton85.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton85.Location = New System.Drawing.Point(511, 5)
        Me.RadioButton85.Name = "RadioButton85"
        Me.RadioButton85.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton85.TabIndex = 27
        Me.RadioButton85.TabStop = True
        Me.RadioButton85.Text = "No"
        Me.RadioButton85.UseVisualStyleBackColor = True
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(136, 241)
        Me.TextBox19.Multiline = True
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox19.Size = New System.Drawing.Size(1004, 96)
        Me.TextBox19.TabIndex = 56
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.Location = New System.Drawing.Point(11, 238)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(119, 18)
        Me.Label63.TabIndex = 55
        Me.Label63.Text = "Letter Of Wishes"
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button10.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button10.ForeColor = System.Drawing.Color.black
        Me.Button10.Location = New System.Drawing.Point(902, 41)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(159, 36)
        Me.Button10.TabIndex = 54
        Me.Button10.Text = "Add A Beneficiary"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'DataGridView9
        '
        Me.DataGridView9.AllowUserToAddRows = False
        Me.DataGridView9.AllowUserToDeleteRows = False
        Me.DataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView9.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41, Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn43, Me.DataGridViewTextBoxColumn44, Me.DataGridViewLinkColumn9})
        Me.DataGridView9.Location = New System.Drawing.Point(8, 84)
        Me.DataGridView9.Name = "DataGridView9"
        Me.DataGridView9.ReadOnly = True
        Me.DataGridView9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView9.Size = New System.Drawing.Size(1053, 115)
        Me.DataGridView9.TabIndex = 53
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        Me.DataGridViewTextBoxColumn39.ReadOnly = True
        Me.DataGridViewTextBoxColumn39.Visible = False
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        Me.DataGridViewTextBoxColumn40.ReadOnly = True
        Me.DataGridViewTextBoxColumn40.Width = 270
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.HeaderText = "Lapse/Issue"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        Me.DataGridViewTextBoxColumn41.ReadOnly = True
        Me.DataGridViewTextBoxColumn41.Width = 150
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.HeaderText = "Percentage"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        Me.DataGridViewTextBoxColumn42.ReadOnly = True
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        Me.DataGridViewTextBoxColumn43.ReadOnly = True
        Me.DataGridViewTextBoxColumn43.Width = 180
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        Me.DataGridViewTextBoxColumn44.ReadOnly = True
        Me.DataGridViewTextBoxColumn44.Width = 180
        '
        'DataGridViewLinkColumn9
        '
        Me.DataGridViewLinkColumn9.HeaderText = "Action"
        Me.DataGridViewLinkColumn9.Name = "DataGridViewLinkColumn9"
        Me.DataGridViewLinkColumn9.ReadOnly = True
        '
        'Panel34
        '
        Me.Panel34.Controls.Add(Me.RadioButton78)
        Me.Panel34.Controls.Add(Me.Label64)
        Me.Panel34.Controls.Add(Me.RadioButton79)
        Me.Panel34.Location = New System.Drawing.Point(491, 44)
        Me.Panel34.Name = "Panel34"
        Me.Panel34.Size = New System.Drawing.Size(236, 30)
        Me.Panel34.TabIndex = 52
        '
        'RadioButton78
        '
        Me.RadioButton78.AutoSize = True
        Me.RadioButton78.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton78.Location = New System.Drawing.Point(114, 5)
        Me.RadioButton78.Name = "RadioButton78"
        Me.RadioButton78.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton78.TabIndex = 29
        Me.RadioButton78.TabStop = True
        Me.RadioButton78.Text = "Yes"
        Me.RadioButton78.UseVisualStyleBackColor = True
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.Location = New System.Drawing.Point(3, 7)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(105, 18)
        Me.Label64.TabIndex = 26
        Me.Label64.Text = "Then to issue?"
        '
        'RadioButton79
        '
        Me.RadioButton79.AutoSize = True
        Me.RadioButton79.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton79.Location = New System.Drawing.Point(184, 5)
        Me.RadioButton79.Name = "RadioButton79"
        Me.RadioButton79.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton79.TabIndex = 27
        Me.RadioButton79.TabStop = True
        Me.RadioButton79.Text = "No"
        Me.RadioButton79.UseVisualStyleBackColor = True
        '
        'Panel35
        '
        Me.Panel35.Controls.Add(Me.RadioButton80)
        Me.Panel35.Controls.Add(Me.Label65)
        Me.Panel35.Controls.Add(Me.RadioButton81)
        Me.Panel35.Location = New System.Drawing.Point(8, 44)
        Me.Panel35.Name = "Panel35"
        Me.Panel35.Size = New System.Drawing.Size(477, 30)
        Me.Panel35.TabIndex = 51
        '
        'RadioButton80
        '
        Me.RadioButton80.AutoSize = True
        Me.RadioButton80.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton80.Location = New System.Drawing.Point(355, 5)
        Me.RadioButton80.Name = "RadioButton80"
        Me.RadioButton80.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton80.TabIndex = 29
        Me.RadioButton80.TabStop = True
        Me.RadioButton80.Text = "Yes"
        Me.RadioButton80.UseVisualStyleBackColor = True
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.Location = New System.Drawing.Point(3, 7)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(345, 18)
        Me.Label65.TabIndex = 26
        Me.Label65.Text = "Bloodline children EQUAL secondary beneficiaries?"
        '
        'RadioButton81
        '
        Me.RadioButton81.AutoSize = True
        Me.RadioButton81.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton81.Location = New System.Drawing.Point(425, 5)
        Me.RadioButton81.Name = "RadioButton81"
        Me.RadioButton81.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton81.TabIndex = 27
        Me.RadioButton81.TabStop = True
        Me.RadioButton81.Text = "No"
        Me.RadioButton81.UseVisualStyleBackColor = True
        '
        'Panel36
        '
        Me.Panel36.Controls.Add(Me.RadioButton82)
        Me.Panel36.Controls.Add(Me.Label66)
        Me.Panel36.Controls.Add(Me.RadioButton83)
        Me.Panel36.Location = New System.Drawing.Point(8, 8)
        Me.Panel36.Name = "Panel36"
        Me.Panel36.Size = New System.Drawing.Size(693, 30)
        Me.Panel36.TabIndex = 50
        '
        'RadioButton82
        '
        Me.RadioButton82.AutoSize = True
        Me.RadioButton82.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton82.Location = New System.Drawing.Point(240, 5)
        Me.RadioButton82.Name = "RadioButton82"
        Me.RadioButton82.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton82.TabIndex = 29
        Me.RadioButton82.TabStop = True
        Me.RadioButton82.Text = "Yes"
        Me.RadioButton82.UseVisualStyleBackColor = True
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.Location = New System.Drawing.Point(3, 7)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(231, 18)
        Me.Label66.TabIndex = 26
        Me.Label66.Text = "Do you wish to add a loan facility?"
        '
        'RadioButton83
        '
        Me.RadioButton83.AutoSize = True
        Me.RadioButton83.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton83.Location = New System.Drawing.Point(310, 5)
        Me.RadioButton83.Name = "RadioButton83"
        Me.RadioButton83.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton83.TabIndex = 27
        Me.RadioButton83.TabStop = True
        Me.RadioButton83.Text = "No"
        Me.RadioButton83.UseVisualStyleBackColor = True
        '
        'TabPage15
        '
        Me.TabPage15.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage15.Controls.Add(Me.TextBox16)
        Me.TabPage15.Controls.Add(Me.Label59)
        Me.TabPage15.Controls.Add(Me.Button11)
        Me.TabPage15.Controls.Add(Me.DataGridView10)
        Me.TabPage15.Controls.Add(Me.Panel32)
        Me.TabPage15.Controls.Add(Me.Panel33)
        Me.TabPage15.Location = New System.Drawing.Point(4, 29)
        Me.TabPage15.Name = "TabPage15"
        Me.TabPage15.Size = New System.Drawing.Size(1159, 352)
        Me.TabPage15.TabIndex = 2
        Me.TabPage15.Text = "Discretionary Trust Details"
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(141, 223)
        Me.TextBox16.Multiline = True
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox16.Size = New System.Drawing.Size(1004, 118)
        Me.TextBox16.TabIndex = 63
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(16, 223)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(119, 18)
        Me.Label59.TabIndex = 62
        Me.Label59.Text = "Letter Of Wishes"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button11.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button11.ForeColor = System.Drawing.Color.black
        Me.Button11.Location = New System.Drawing.Point(907, 34)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(159, 36)
        Me.Button11.TabIndex = 61
        Me.Button11.Text = "Add A Beneficiary"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'DataGridView10
        '
        Me.DataGridView10.AllowUserToAddRows = False
        Me.DataGridView10.AllowUserToDeleteRows = False
        Me.DataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView10.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn46, Me.DataGridViewTextBoxColumn47, Me.DataGridViewTextBoxColumn48, Me.DataGridViewTextBoxColumn49, Me.DataGridViewTextBoxColumn50, Me.DataGridViewLinkColumn10})
        Me.DataGridView10.Location = New System.Drawing.Point(13, 76)
        Me.DataGridView10.Name = "DataGridView10"
        Me.DataGridView10.ReadOnly = True
        Me.DataGridView10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView10.Size = New System.Drawing.Size(1053, 127)
        Me.DataGridView10.TabIndex = 60
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        Me.DataGridViewTextBoxColumn45.ReadOnly = True
        Me.DataGridViewTextBoxColumn45.Visible = False
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        Me.DataGridViewTextBoxColumn46.ReadOnly = True
        Me.DataGridViewTextBoxColumn46.Width = 270
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.HeaderText = "Lapse/Issue"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        Me.DataGridViewTextBoxColumn47.ReadOnly = True
        Me.DataGridViewTextBoxColumn47.Width = 150
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.HeaderText = "Percentage"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        Me.DataGridViewTextBoxColumn48.ReadOnly = True
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.HeaderText = "Relationship To Client 1"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        Me.DataGridViewTextBoxColumn49.ReadOnly = True
        Me.DataGridViewTextBoxColumn49.Width = 180
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.HeaderText = "Relationship To Client 2"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        Me.DataGridViewTextBoxColumn50.ReadOnly = True
        Me.DataGridViewTextBoxColumn50.Width = 180
        '
        'DataGridViewLinkColumn10
        '
        Me.DataGridViewLinkColumn10.HeaderText = "Action"
        Me.DataGridViewLinkColumn10.Name = "DataGridViewLinkColumn10"
        Me.DataGridViewLinkColumn10.ReadOnly = True
        '
        'Panel32
        '
        Me.Panel32.Controls.Add(Me.RadioButton74)
        Me.Panel32.Controls.Add(Me.Label60)
        Me.Panel32.Controls.Add(Me.RadioButton75)
        Me.Panel32.Location = New System.Drawing.Point(554, 8)
        Me.Panel32.Name = "Panel32"
        Me.Panel32.Size = New System.Drawing.Size(236, 30)
        Me.Panel32.TabIndex = 59
        '
        'RadioButton74
        '
        Me.RadioButton74.AutoSize = True
        Me.RadioButton74.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton74.Location = New System.Drawing.Point(114, 5)
        Me.RadioButton74.Name = "RadioButton74"
        Me.RadioButton74.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton74.TabIndex = 29
        Me.RadioButton74.TabStop = True
        Me.RadioButton74.Text = "Yes"
        Me.RadioButton74.UseVisualStyleBackColor = True
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(3, 7)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(105, 18)
        Me.Label60.TabIndex = 26
        Me.Label60.Text = "Then to issue?"
        '
        'RadioButton75
        '
        Me.RadioButton75.AutoSize = True
        Me.RadioButton75.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton75.Location = New System.Drawing.Point(184, 5)
        Me.RadioButton75.Name = "RadioButton75"
        Me.RadioButton75.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton75.TabIndex = 27
        Me.RadioButton75.TabStop = True
        Me.RadioButton75.Text = "No"
        Me.RadioButton75.UseVisualStyleBackColor = True
        '
        'Panel33
        '
        Me.Panel33.Controls.Add(Me.RadioButton76)
        Me.Panel33.Controls.Add(Me.Label61)
        Me.Panel33.Controls.Add(Me.RadioButton77)
        Me.Panel33.Location = New System.Drawing.Point(8, 8)
        Me.Panel33.Name = "Panel33"
        Me.Panel33.Size = New System.Drawing.Size(540, 30)
        Me.Panel33.TabIndex = 58
        '
        'RadioButton76
        '
        Me.RadioButton76.AutoSize = True
        Me.RadioButton76.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton76.Location = New System.Drawing.Point(420, 5)
        Me.RadioButton76.Name = "RadioButton76"
        Me.RadioButton76.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton76.TabIndex = 29
        Me.RadioButton76.TabStop = True
        Me.RadioButton76.Text = "Yes"
        Me.RadioButton76.UseVisualStyleBackColor = True
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.Location = New System.Drawing.Point(3, 7)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(408, 18)
        Me.Label61.TabIndex = 26
        Me.Label61.Text = "Are the bloodline children EQUAL discretionary beneficiaries?"
        '
        'RadioButton77
        '
        Me.RadioButton77.AutoSize = True
        Me.RadioButton77.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton77.Location = New System.Drawing.Point(490, 5)
        Me.RadioButton77.Name = "RadioButton77"
        Me.RadioButton77.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton77.TabIndex = 27
        Me.RadioButton77.TabStop = True
        Me.RadioButton77.Text = "No"
        Me.RadioButton77.UseVisualStyleBackColor = True
        '
        'Panel27
        '
        Me.Panel27.Controls.Add(Me.RadioButton65)
        Me.Panel27.Controls.Add(Me.RadioButton62)
        Me.Panel27.Controls.Add(Me.Label54)
        Me.Panel27.Controls.Add(Me.RadioButton63)
        Me.Panel27.Controls.Add(Me.RadioButton64)
        Me.Panel27.Location = New System.Drawing.Point(19, 45)
        Me.Panel27.Name = "Panel27"
        Me.Panel27.Size = New System.Drawing.Size(506, 30)
        Me.Panel27.TabIndex = 42
        '
        'RadioButton65
        '
        Me.RadioButton65.AutoSize = True
        Me.RadioButton65.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton65.Location = New System.Drawing.Point(274, 5)
        Me.RadioButton65.Name = "RadioButton65"
        Me.RadioButton65.Size = New System.Drawing.Size(55, 22)
        Me.RadioButton65.TabIndex = 30
        Me.RadioButton65.TabStop = True
        Me.RadioButton65.Text = "FLIT"
        Me.RadioButton65.UseVisualStyleBackColor = True
        '
        'RadioButton62
        '
        Me.RadioButton62.AutoSize = True
        Me.RadioButton62.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton62.Location = New System.Drawing.Point(53, 5)
        Me.RadioButton62.Name = "RadioButton62"
        Me.RadioButton62.Size = New System.Drawing.Size(62, 22)
        Me.RadioButton62.TabIndex = 29
        Me.RadioButton62.TabStop = True
        Me.RadioButton62.Text = "None"
        Me.RadioButton62.UseVisualStyleBackColor = True
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(3, 7)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(44, 18)
        Me.Label54.TabIndex = 26
        Me.Label54.Text = "Type:"
        '
        'RadioButton63
        '
        Me.RadioButton63.AutoSize = True
        Me.RadioButton63.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton63.Location = New System.Drawing.Point(123, 5)
        Me.RadioButton63.Name = "RadioButton63"
        Me.RadioButton63.Size = New System.Drawing.Size(143, 22)
        Me.RadioButton63.TabIndex = 27
        Me.RadioButton63.TabStop = True
        Me.RadioButton63.Text = "Direct Distribution"
        Me.RadioButton63.UseVisualStyleBackColor = True
        '
        'RadioButton64
        '
        Me.RadioButton64.AutoSize = True
        Me.RadioButton64.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton64.Location = New System.Drawing.Point(337, 5)
        Me.RadioButton64.Name = "RadioButton64"
        Me.RadioButton64.Size = New System.Drawing.Size(151, 22)
        Me.RadioButton64.TabIndex = 28
        Me.RadioButton64.TabStop = True
        Me.RadioButton64.Text = "Discretionary Trust"
        Me.RadioButton64.UseVisualStyleBackColor = True
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(12, 20)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(131, 20)
        Me.Label53.TabIndex = 9
        Me.Label53.Text = "Residuary Estate"
        '
        'TabPage9
        '
        Me.TabPage9.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage9.Controls.Add(Me.Panel16)
        Me.TabPage9.Controls.Add(Me.GroupBox5)
        Me.TabPage9.Controls.Add(Me.GroupBox6)
        Me.TabPage9.Controls.Add(Me.Label33)
        Me.TabPage9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage9.Location = New System.Drawing.Point(4, 39)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = "Potential Claims"
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.Label37)
        Me.Panel16.Controls.Add(Me.RadioButton39)
        Me.Panel16.Controls.Add(Me.RadioButton40)
        Me.Panel16.Location = New System.Drawing.Point(19, 45)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(969, 30)
        Me.Panel16.TabIndex = 45
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(5, 7)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(575, 18)
        Me.Label37.TabIndex = 29
        Me.Label37.Text = "Have you dliberately excluded anybody who may have a possible claim on your estat" &
    "e?"
        '
        'RadioButton39
        '
        Me.RadioButton39.AutoSize = True
        Me.RadioButton39.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton39.Location = New System.Drawing.Point(597, 5)
        Me.RadioButton39.Name = "RadioButton39"
        Me.RadioButton39.Size = New System.Drawing.Size(51, 22)
        Me.RadioButton39.TabIndex = 30
        Me.RadioButton39.TabStop = True
        Me.RadioButton39.Text = "Yes"
        Me.RadioButton39.UseVisualStyleBackColor = True
        '
        'RadioButton40
        '
        Me.RadioButton40.AutoSize = True
        Me.RadioButton40.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton40.Location = New System.Drawing.Point(652, 5)
        Me.RadioButton40.Name = "RadioButton40"
        Me.RadioButton40.Size = New System.Drawing.Size(46, 22)
        Me.RadioButton40.TabIndex = 31
        Me.RadioButton40.TabStop = True
        Me.RadioButton40.Text = "No"
        Me.RadioButton40.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBox11)
        Me.GroupBox5.Controls.Add(Me.Label34)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(635, 114)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(541, 220)
        Me.GroupBox5.TabIndex = 23
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Client 2"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(77, 31)
        Me.TextBox11.Multiline = True
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(449, 168)
        Me.TextBox11.TabIndex = 20
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(18, 31)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(53, 18)
        Me.Label34.TabIndex = 19
        Me.Label34.Text = "Details"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TextBox17)
        Me.GroupBox6.Controls.Add(Me.Label41)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(25, 114)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(541, 220)
        Me.GroupBox6.TabIndex = 22
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Client 1"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(77, 31)
        Me.TextBox17.Multiline = True
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(449, 168)
        Me.TextBox17.TabIndex = 18
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(18, 31)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(53, 18)
        Me.Label41.TabIndex = 13
        Me.Label41.Text = "Details"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(12, 20)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(122, 20)
        Me.Label33.TabIndex = 21
        Me.Label33.Text = "Potential Claims"
        '
        'TabPage10
        '
        Me.TabPage10.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage10.Controls.Add(Me.TextBox20)
        Me.TabPage10.Controls.Add(Me.Label36)
        Me.TabPage10.Controls.Add(Me.DataGridView7)
        Me.TabPage10.Controls.Add(Me.Label35)
        Me.TabPage10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage10.Location = New System.Drawing.Point(4, 39)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Size = New System.Drawing.Size(1201, 487)
        Me.TabPage10.TabIndex = 9
        Me.TabPage10.Text = "Attendance Notes"
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(398, 75)
        Me.TextBox20.Multiline = True
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(793, 392)
        Me.TextBox20.TabIndex = 44
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(395, 54)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(184, 18)
        Me.Label36.TabIndex = 43
        Me.Label36.Text = "Supplementary Information"
        '
        'DataGridView7
        '
        Me.DataGridView7.AllowUserToAddRows = False
        Me.DataGridView7.AllowUserToDeleteRows = False
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Beige
        Me.DataGridView7.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView7.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewLinkColumn6})
        Me.DataGridView7.Location = New System.Drawing.Point(16, 54)
        Me.DataGridView7.Name = "DataGridView7"
        Me.DataGridView7.ReadOnly = True
        Me.DataGridView7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView7.Size = New System.Drawing.Size(351, 424)
        Me.DataGridView7.TabIndex = 42
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Question"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Answer"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        '
        'DataGridViewLinkColumn6
        '
        Me.DataGridViewLinkColumn6.HeaderText = "Action"
        Me.DataGridViewLinkColumn6.Name = "DataGridViewLinkColumn6"
        Me.DataGridViewLinkColumn6.ReadOnly = True
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(12, 20)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(138, 20)
        Me.Label35.TabIndex = 20
        Me.Label35.Text = "Attendance Notes"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(184, 29)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Will Instruction"
        '
        'firstRunComplete
        '
        Me.firstRunComplete.AutoSize = True
        Me.firstRunComplete.Location = New System.Drawing.Point(1068, 22)
        Me.firstRunComplete.Name = "firstRunComplete"
        Me.firstRunComplete.Size = New System.Drawing.Size(43, 13)
        Me.firstRunComplete.TabIndex = 5
        Me.firstRunComplete.Text = "firstRun"
        Me.firstRunComplete.Visible = False
        '
        'attendanceMode
        '
        Me.attendanceMode.AutoSize = True
        Me.attendanceMode.Location = New System.Drawing.Point(1068, 47)
        Me.attendanceMode.Name = "attendanceMode"
        Me.attendanceMode.Size = New System.Drawing.Size(88, 13)
        Me.attendanceMode.TabIndex = 6
        Me.attendanceMode.Text = "attendanceMode"
        Me.attendanceMode.Visible = False
        '
        'residuaryType
        '
        Me.residuaryType.AutoSize = True
        Me.residuaryType.Location = New System.Drawing.Point(975, 19)
        Me.residuaryType.Name = "residuaryType"
        Me.residuaryType.Size = New System.Drawing.Size(73, 13)
        Me.residuaryType.TabIndex = 7
        Me.residuaryType.Text = "residuaryType"
        Me.residuaryType.Visible = False
        '
        'trustSequence
        '
        Me.trustSequence.AutoSize = True
        Me.trustSequence.Location = New System.Drawing.Point(972, 35)
        Me.trustSequence.Name = "trustSequence"
        Me.trustSequence.Size = New System.Drawing.Size(76, 13)
        Me.trustSequence.TabIndex = 8
        Me.trustSequence.Text = "trustSequence"
        Me.trustSequence.Visible = False
        '
        'factfind_will
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.trustSequence)
        Me.Controls.Add(Me.residuaryType)
        Me.Controls.Add(Me.attendanceMode)
        Me.Controls.Add(Me.firstRunComplete)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button2)
        Me.MaximizeBox = False
        Me.Name = "factfind_will"
        Me.Text = "Client Instruction - Wills"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage11.ResumeLayout(False)
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        Me.TabPage12.ResumeLayout(False)
        Me.TabPage12.PerformLayout()
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel24.ResumeLayout(False)
        Me.Panel24.PerformLayout()
        Me.Panel25.ResumeLayout(False)
        Me.Panel25.PerformLayout()
        Me.Panel26.ResumeLayout(False)
        Me.Panel26.PerformLayout()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage8.PerformLayout()
        Me.TabControl3.ResumeLayout(False)
        Me.TabPage13.ResumeLayout(False)
        CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel28.ResumeLayout(False)
        Me.Panel28.PerformLayout()
        Me.Panel29.ResumeLayout(False)
        Me.Panel29.PerformLayout()
        Me.Panel30.ResumeLayout(False)
        Me.Panel30.PerformLayout()
        Me.Panel31.ResumeLayout(False)
        Me.Panel31.PerformLayout()
        Me.TabPage14.ResumeLayout(False)
        Me.TabPage14.PerformLayout()
        Me.Panel37.ResumeLayout(False)
        Me.Panel37.PerformLayout()
        CType(Me.DataGridView9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel34.ResumeLayout(False)
        Me.Panel34.PerformLayout()
        Me.Panel35.ResumeLayout(False)
        Me.Panel35.PerformLayout()
        Me.Panel36.ResumeLayout(False)
        Me.Panel36.PerformLayout()
        Me.TabPage15.ResumeLayout(False)
        Me.TabPage15.PerformLayout()
        CType(Me.DataGridView10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel32.ResumeLayout(False)
        Me.Panel32.PerformLayout()
        Me.Panel33.ResumeLayout(False)
        Me.Panel33.PerformLayout()
        Me.Panel27.ResumeLayout(False)
        Me.Panel27.PerformLayout()
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage9.PerformLayout()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage10.PerformLayout()
        CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button2 As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents TabPage9 As TabPage
    Friend WithEvents TabPage10 As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents Label3 As Label
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents RadioButton6 As RadioButton
    Friend WithEvents RadioButton7 As RadioButton
    Friend WithEvents Label5 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label8 As Label
    Friend WithEvents RadioButton8 As RadioButton
    Friend WithEvents RadioButton9 As RadioButton
    Friend WithEvents Panel4 As Panel
    Friend WithEvents RadioButton10 As RadioButton
    Friend WithEvents RadioButton11 As RadioButton
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Label17 As Label
    Friend WithEvents RadioButton12 As RadioButton
    Friend WithEvents RadioButton13 As RadioButton
    Friend WithEvents DataGridView5 As DataGridView
    Friend WithEvents Button6 As Button
    Friend WithEvents RadioButton14 As RadioButton
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label9 As Label
    Friend WithEvents RadioButton15 As RadioButton
    Friend WithEvents RadioButton16 As RadioButton
    Friend WithEvents Panel7 As Panel
    Friend WithEvents RadioButton17 As RadioButton
    Friend WithEvents RadioButton18 As RadioButton
    Friend WithEvents RadioButton19 As RadioButton
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Label18 As Label
    Friend WithEvents RadioButton20 As RadioButton
    Friend WithEvents RadioButton21 As RadioButton
    Friend WithEvents Panel9 As Panel
    Friend WithEvents RadioButton23 As RadioButton
    Friend WithEvents RadioButton24 As RadioButton
    Friend WithEvents RadioButton22 As RadioButton
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Label19 As Label
    Friend WithEvents RadioButton25 As RadioButton
    Friend WithEvents RadioButton26 As RadioButton
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn4 As DataGridViewLinkColumn
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn2 As DataGridViewLinkColumn
    Friend WithEvents Button3 As Button
    Friend WithEvents Label23 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents TextBox15 As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents Label25 As Label
    Friend WithEvents RadioButton27 As RadioButton
    Friend WithEvents RadioButton28 As RadioButton
    Friend WithEvents Panel12 As Panel
    Friend WithEvents RadioButton31 As RadioButton
    Friend WithEvents Label26 As Label
    Friend WithEvents RadioButton29 As RadioButton
    Friend WithEvents RadioButton30 As RadioButton
    Friend WithEvents Panel13 As Panel
    Friend WithEvents RadioButton32 As RadioButton
    Friend WithEvents Label27 As Label
    Friend WithEvents RadioButton33 As RadioButton
    Friend WithEvents RadioButton34 As RadioButton
    Friend WithEvents Panel14 As Panel
    Friend WithEvents Label29 As Label
    Friend WithEvents RadioButton35 As RadioButton
    Friend WithEvents RadioButton36 As RadioButton
    Friend WithEvents Label30 As Label
    Friend WithEvents TextBox10 As TextBox
    Friend WithEvents Panel15 As Panel
    Friend WithEvents Label32 As Label
    Friend WithEvents RadioButton37 As RadioButton
    Friend WithEvents RadioButton38 As RadioButton
    Friend WithEvents Label31 As Label
    Friend WithEvents DataGridView3 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn3 As DataGridViewLinkColumn
    Friend WithEvents Button4 As Button
    Friend WithEvents Label35 As Label
    Friend WithEvents TextBox20 As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents DataGridView7 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn6 As DataGridViewLinkColumn
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents TextBox11 As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents TextBox17 As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Panel16 As Panel
    Friend WithEvents Label37 As Label
    Friend WithEvents RadioButton39 As RadioButton
    Friend WithEvents RadioButton40 As RadioButton
    Friend WithEvents Label38 As Label
    Friend WithEvents Panel17 As Panel
    Friend WithEvents RadioButton43 As RadioButton
    Friend WithEvents Label39 As Label
    Friend WithEvents RadioButton41 As RadioButton
    Friend WithEvents RadioButton42 As RadioButton
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage11 As TabPage
    Friend WithEvents Button5 As Button
    Friend WithEvents DataGridView4 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn22 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn5 As DataGridViewLinkColumn
    Friend WithEvents Panel23 As Panel
    Friend WithEvents RadioButton54 As RadioButton
    Friend WithEvents Label48 As Label
    Friend WithEvents RadioButton55 As RadioButton
    Friend WithEvents Panel22 As Panel
    Friend WithEvents RadioButton52 As RadioButton
    Friend WithEvents Label47 As Label
    Friend WithEvents RadioButton53 As RadioButton
    Friend WithEvents Panel21 As Panel
    Friend WithEvents RadioButton50 As RadioButton
    Friend WithEvents Label45 As Label
    Friend WithEvents RadioButton51 As RadioButton
    Friend WithEvents Panel20 As Panel
    Friend WithEvents RadioButton48 As RadioButton
    Friend WithEvents Label46 As Label
    Friend WithEvents RadioButton49 As RadioButton
    Friend WithEvents Panel19 As Panel
    Friend WithEvents TextBox13 As TextBox
    Friend WithEvents Label43 As Label
    Friend WithEvents RadioButton46 As RadioButton
    Friend WithEvents Label44 As Label
    Friend WithEvents RadioButton47 As RadioButton
    Friend WithEvents Panel18 As Panel
    Friend WithEvents TextBox12 As TextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents RadioButton44 As RadioButton
    Friend WithEvents Label40 As Label
    Friend WithEvents RadioButton45 As RadioButton
    Friend WithEvents TabPage12 As TabPage
    Friend WithEvents Button7 As Button
    Friend WithEvents DataGridView6 As DataGridView
    Friend WithEvents Panel24 As Panel
    Friend WithEvents RadioButton56 As RadioButton
    Friend WithEvents Label49 As Label
    Friend WithEvents RadioButton57 As RadioButton
    Friend WithEvents Panel25 As Panel
    Friend WithEvents RadioButton58 As RadioButton
    Friend WithEvents Label50 As Label
    Friend WithEvents RadioButton59 As RadioButton
    Friend WithEvents Panel26 As Panel
    Friend WithEvents RadioButton60 As RadioButton
    Friend WithEvents Label51 As Label
    Friend WithEvents RadioButton61 As RadioButton
    Friend WithEvents TextBox14 As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents Panel27 As Panel
    Friend WithEvents RadioButton62 As RadioButton
    Friend WithEvents Label54 As Label
    Friend WithEvents RadioButton63 As RadioButton
    Friend WithEvents RadioButton64 As RadioButton
    Friend WithEvents Label53 As Label
    Friend WithEvents TabControl3 As TabControl
    Friend WithEvents TabPage13 As TabPage
    Friend WithEvents Button9 As Button
    Friend WithEvents DataGridView8 As DataGridView
    Friend WithEvents Panel28 As Panel
    Friend WithEvents RadioButton66 As RadioButton
    Friend WithEvents Label55 As Label
    Friend WithEvents RadioButton67 As RadioButton
    Friend WithEvents Panel29 As Panel
    Friend WithEvents RadioButton68 As RadioButton
    Friend WithEvents Label56 As Label
    Friend WithEvents RadioButton69 As RadioButton
    Friend WithEvents Panel30 As Panel
    Friend WithEvents RadioButton70 As RadioButton
    Friend WithEvents Label57 As Label
    Friend WithEvents RadioButton71 As RadioButton
    Friend WithEvents Panel31 As Panel
    Friend WithEvents RadioButton72 As RadioButton
    Friend WithEvents Label58 As Label
    Friend WithEvents RadioButton73 As RadioButton
    Friend WithEvents TabPage14 As TabPage
    Friend WithEvents TextBox19 As TextBox
    Friend WithEvents Label63 As Label
    Friend WithEvents Button10 As Button
    Friend WithEvents DataGridView9 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn39 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn9 As DataGridViewLinkColumn
    Friend WithEvents Panel34 As Panel
    Friend WithEvents RadioButton78 As RadioButton
    Friend WithEvents Label64 As Label
    Friend WithEvents RadioButton79 As RadioButton
    Friend WithEvents Panel35 As Panel
    Friend WithEvents RadioButton80 As RadioButton
    Friend WithEvents Label65 As Label
    Friend WithEvents RadioButton81 As RadioButton
    Friend WithEvents Panel36 As Panel
    Friend WithEvents RadioButton82 As RadioButton
    Friend WithEvents Label66 As Label
    Friend WithEvents RadioButton83 As RadioButton
    Friend WithEvents TabPage15 As TabPage
    Friend WithEvents RadioButton65 As RadioButton
    Friend WithEvents TextBox16 As TextBox
    Friend WithEvents Label59 As Label
    Friend WithEvents Button11 As Button
    Friend WithEvents DataGridView10 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn45 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn49 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn10 As DataGridViewLinkColumn
    Friend WithEvents Panel32 As Panel
    Friend WithEvents RadioButton74 As RadioButton
    Friend WithEvents Label60 As Label
    Friend WithEvents RadioButton75 As RadioButton
    Friend WithEvents Panel33 As Panel
    Friend WithEvents RadioButton76 As RadioButton
    Friend WithEvents Label61 As Label
    Friend WithEvents RadioButton77 As RadioButton
    Friend WithEvents Panel37 As Panel
    Friend WithEvents RadioButton84 As RadioButton
    Friend WithEvents Label62 As Label
    Friend WithEvents RadioButton85 As RadioButton
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn1 As DataGridViewLinkColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn8 As DataGridViewLinkColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn7 As DataGridViewLinkColumn
    Friend WithEvents firstRunComplete As Label
    Friend WithEvents attendanceMode As Label
    Friend WithEvents residuaryType As Label
    Friend WithEvents trustSequence As Label
End Class
