﻿Imports System.Runtime.InteropServices

Public Class clientrecord
    <DllImport("winmm.dll")>
    Private Shared Function mciSendString(ByVal command As String, ByVal buffer As String, ByVal bufferSize As Integer, ByVal hwndCallback As IntPtr) As Integer
    End Function

    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        spy("client record opened")
        recordLastTouched()

        CenterForm(Me)
        resetForm(Me)
        Me.recordingFlag.Text = "ON"

        'now populate screen with client data
        populateScreen()

        'this starts the recording :-) :-)
        Dim i As Integer
        i = mciSendString("open new type waveaudio alias capture", Nothing, 0, 0)
        i = mciSendString("setaudio wav volume to 1000", Nothing, 0, 0)
        i = mciSendString("record capture", Nothing, 0, 0)

        'now show pop-up allowing user to cancel recording
        Dim result As DialogResult = MessageBox.Show("This meeting is being recorded for compliance monitoring." + vbCrLf + vbCrLf + "Please ask client if they are happy to be recorded. " + vbCrLf + vbCrLf + "If they are not, ask them to say out loud that they do not wish the meeting to be recorded and then press the 'NO' button below, otherwise press the 'YES' button", "Is Client Happy For Meeting To Be Recorded?", MessageBoxButtons.YesNo)
        If result = DialogResult.No Then
            'stop the recording, only if it has not already been stopped

            Dim audio_file_location As String = getAudioDir()
            i = mciSendString(String.Format("save capture ""{0}""", audio_file_location), Nothing, 0, 0)
            i = mciSendString("close capture", Nothing, 0, 0)

            If clientID <> "" Then
                'save the audio file somewhere in a database 
                Dim sql As String = "insert into electronicFiles (willClientSequence,title,filename) values (" + clientID + ",'pending upload','" + audio_file_location + "')"
                runSQL(sql)
            End If
            Me.recordingFlag.Text = "OFF"
            spy("client audio cancelled")
        Else
            Me.recordingFlag.Text = "ON"
            spy("client audio accepted")
        End If

    End Sub

    Function populateScreen()

        If clientID = "" Or clientID = "0" Then  'no client yet, show nothing
            Button1.Visible = False
            Button3.Visible = False
            Button4.Visible = False
            Button6.Visible = False
            Button7.Visible = False
            Button8.Visible = False '
            Button9.Visible = False
            Button10.Visible = False
            Button11.Visible = False
            Button12.Visible = False
            Button5.Enabled = True

        Else


            Button1.Visible = True
            Button3.Visible = True
            Button4.Visible = True
            Button6.Visible = True
            Button7.Visible = True
            Button8.Visible = True '
            Button9.Visible = True
            Button10.Visible = True
            Button11.Visible = True
            Button12.Visible = True
            Button5.Enabled = True

            If 1 = 2 Then


                'have we run the wizard yet?
                Dim sql As String
                sql = "select sequence From client_products where willClientSequence=" + clientID
                If runSQLwithID(sql) = "" Or runSQLwithID(sql) = "ERROR" Then
                    Button1.Visible = False
                    Button3.Visible = False
                    Button4.Visible = False
                    Button9.Visible = False

                Else

                    'reset buttons
                    Button1.Visible = False
                    Button3.Visible = False
                    Button4.Visible = False
                    Button9.Visible = False

                    'wizard has been run, so need to work on which product buttons need to be activated
                    sql = "select * from client_wizard where willClientSequence=" + clientID
                    Dim results As Array
                    results = runSQLwithArray(sql)
                    If results(0) <> "" And results(0) <> "ERROR" Then
                        'ignore, as no existing client data

                        'populate data
                        If results(3) = "Y" Then Button1.Visible = True
                        If results(4) = "Y" Then Button1.Visible = True
                        If results(5) = "Y" Then Button1.Visible = True
                        If results(6) = "Y" Then Button3.Visible = True
                        If results(7) = "Y" Then Button3.Visible = True
                        If results(8) = "Y" Then Button4.Visible = True : Button1.Visible = True
                        If results(9) = "Y" Then Button9.Visible = True
                        If results(10) = "Y" Then Button9.Visible = True : Button1.Visible = True : Button3.Visible = True : Button4.Visible = True  'there is a manual override product of OTHER, so need to unlock everything
                    End If

                End If

            End If

            Button6.Visible = True
            Button7.Visible = True


            Button7.Text = getClientFullName("client1")
            If clientType() = "mirror" Then
                If Trim(getClientFullName("client2")) <> "" Then Button7.Text = Button7.Text + " && " + getClientFullName("client2")
            End If

            Button5.Enabled = False

            'probate
            Button10.Visible = True  'probabet assessment is visible as long as there is a client
            Button8.Visible = True
            Button8.Text = "Probate For " + getClientFullName("client1")
            If clientType() = "single" Then
                Me.Button11.Visible = False

            End If

            If clientType() = "mirror" Then
                Me.Button11.Visible = True
                Button11.Text = "Probate For " + getClientFullName("client2")
            End If

        End If

        ' Button9.Visible = False 'this is probate plan, always hide it (21st jan 2020)

        Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        If Me.recordingFlag.Text = "ON" Then
            'stop the recording, only if it has not already been stopped
            Dim i As Integer
            Dim audio_file_location As String = getAudioDir()
            i = mciSendString(String.Format("save capture ""{0}""", audio_file_location), Nothing, 0, 0)
            i = mciSendString("close capture", Nothing, 0, 0)

            If clientID <> "" Then
                'save the audio file somewhere in a database 
                Dim sql As String = "insert into electronicFiles (willClientSequence,title,filename) values (" + clientID + ",'pending upload','" + audio_file_location + "')"
                runSQL(sql)
            End If
        End If

        spy("client record closed")

        clientID = ""
        rowID = ""
        Me.Close()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        factfind_will.ShowDialog(Me)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        factfind_lpa.ShowDialog(Me)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        factfind_fapt.ShowDialog(Me)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        factfind_wizard.ShowDialog(Me)
        populateScreen()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        clientdetails.ShowDialog(Me)
        populateScreen()
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        clientdetails.ShowDialog(Me)
        populateScreen()
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click

    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        whoDied = "client1"
        probate_review.ShowDialog(Me)
        populateScreen()
        whoDied = ""
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        whoDied = "client2"
        probate_review.ShowDialog(Me)
        populateScreen()
        whoDied = ""
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        factfind_probate_plan.ShowDialog(Me)
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        clientsignatures.ShowDialog(Me)
    End Sub
End Class