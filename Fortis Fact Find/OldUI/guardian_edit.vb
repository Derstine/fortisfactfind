﻿Public Class guardian_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.guardianTitle.SelectedIndex = 0
        Me.guardianRelationshipClient1.SelectedIndex = 0
        Me.guardianRelationshipClient2.SelectedIndex = 0
        Me.guardianType.SelectedIndex = 0
        Me.guardianType.SelectedIndex = 0


        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        'get current data
        Dim sql As String = "select * from will_instruction_stage5_data_detail where sequence=" + rowID
        Dim results As Array = runSQLwithArray(sql)

        If results(2) = "Mr" Then guardianTitle.SelectedIndex = 1
        If results(2) = "Mrs" Then guardianTitle.SelectedIndex = 2
        If results(2) = "Miss" Then guardianTitle.SelectedIndex = 3
        If results(2) = "Ms" Then guardianTitle.SelectedIndex = 4

        guardianRelationshipClient1.SelectedItem = results(12)
        guardianRelationshipClient2.SelectedItem = results(13)

        guardianNames.Text = results(4)
        guardianSurname.Text = results(3)
        guardianPostcode.Text = results(11)
        guardianAddress.Text = results(6)

        guardianPhone.Text = results(7)
        guardianEmail.Text = results(8)

        guardianType.SelectedItem = results(5)



        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.guardianRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.guardianRelationshipClient2.Visible = False
        End If


    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If guardianTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the guardian's title" + vbCrLf
        If guardianNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the guardian's name" + vbCrLf
        If guardianType.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the guardian's role" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update will_instruction_stage5_data_detail set guardianTitle='" + SqlSafe(guardianTitle.SelectedItem) + "',guardianNames='" + SqlSafe(guardianNames.Text) + "',guardianSurname='" + SqlSafe(guardianSurname.Text) + "',relationToClient1='" + SqlSafe(guardianRelationshipClient1.SelectedItem) + "',relationToClient2='" + SqlSafe(guardianRelationshipClient2.SelectedItem) + "',guardianAddress='" + SqlSafe(guardianAddress.Text) + "',guardianPostcode='" + SqlSafe(guardianPostcode.Text) + "',guardianType='" + SqlSafe(guardianType.SelectedItem) + "',guardianPhone='" + SqlSafe(guardianPhone.Text) + "',guardianEmail='" + SqlSafe(guardianEmail.Text) + "'  where sequence=" + rowID
            runSQL(sql)

            Me.Close()
        End If



    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        guardianPostcode.Text = guardianPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = guardianPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.guardianAddress.Text = f.FoundAddress()
            Me.guardianPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person1Postcode,person1Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        guardianPostcode.Text = results(0)
        guardianAddress.Text = results(1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person2Postcode,person2Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        guardianPostcode.Text = results(0)
        guardianAddress.Text = results(1)
    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this guardian?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from will_instruction_stage5_data_detail where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class