﻿Public Class clientdetails
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)
        Me.Refresh()
        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        If internetFlag = "Y" Then Me.Button3.Enabled = True Else Me.Button3.Enabled = False
        If internetFlag = "Y" Then Me.Button4.Enabled = True Else Me.Button4.Enabled = False

        spy("client details screen loaded")
    End Sub

    Function setDefaults()

        Me.RadioButton1.Checked = True
        person1Title.SelectedIndex = 0
        person1MaritalStatus.SelectedIndex = 0
        person2MaritalStatus.SelectedIndex = 0
        person2Title.SelectedIndex = 0

        'need to clear some data as it looks like resetForm doesn't work in my test
        person1Forenames.Text = ""
        person1Surname.Text = ""
        person1Address.Text = ""
        person1Postcode.Text = ""
        person1HomeNumber.Text = ""
        person1MobileNumber.Text = ""
        person1Email.Text = ""
        person1DOB.Value = Now

        person2Forenames.Text = ""
        person2Surname.Text = ""
        person2Address.Text = ""
        person2Postcode.Text = ""
        person2HomeNumber.Text = ""
        person2MobileNumber.Text = ""
        person2Email.Text = ""
        person2DOB.Value = Now



        Return True
    End Function

    Function populateScreen()

        If clientID <> "" And clientID <> "0" Then
            'we have some data to popultate
            Dim result As String
            Dim results As Array
            Dim sql As String

            'mirror or single
            sql = "select typeOfWill from will_instruction_stage1_data where willClientSequence=" + clientID
            result = runSQLwithID(sql)
            If result = "single" Then RadioButton1.Checked = True Else RadioButton2.Checked = True

            'personal details
            sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
            results = runSQLwithArray(sql)
            person1Title.SelectedItem = results(12)
            person1Surname.Text = results(13)
            person1Forenames.Text = results(14)
            person1Address.Text = results(16)
            person1Postcode.Text = results(67)
            person1MaritalStatus.SelectedItem = results(17)
            person1DOB.Value = results(18)
            person1HomeNumber.Text = results(19)
            person1MobileNumber.Text = results(21)
            person1Email.Text = results(22)
            person2Title.SelectedItem = results(23)
            person2Surname.Text = results(24)
            person2Forenames.Text = results(25)
            person2Address.Text = results(27)
            person2Postcode.Text = results(68)
            person2MaritalStatus.SelectedItem = results(28)
            ' MsgBox(results(29))
            If results(29) = "" Then
                person2DOB.Value = Now()
            Else
                person2DOB.Value = results(29)
            End If

            person2HomeNumber.Text = results(30)
            person2MobileNumber.Text = results(32)
            person2Email.Text = results(33)
            clientReferenceNumber.Text = results(75)

            'as it's an existing client, we need to change the button text
            Button1.Text = "Update"
        Else
            'its a new case, not an upate
            Button1.Text = "Save"
        End If
            Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        spy("client details screen closed - cancel button")

        Me.Close()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click

        Dim sql = ""

        If Me.clientReferenceNumber.Text = "" Then
            MsgBox("You must supply the Client Reference Number for this client")
        Else
            'we have a sourceClientID number, so we can continue


            If Button1.Text = "Save" Then
                'its a new record, so it's an insert

                'insert record to get willClientSequence (as we're using WWP database structure), but we call it clientID within this software
                sql = "insert into will_instruction_stage0_data (status,statusDate) values ('offline','" + SqlSafe(Now) + "')"
                runSQL(sql)

                'get clientID i.e. row sequence
                sql = "select * from will_instruction_stage0_data order by sequence DESC"
                clientID = runSQLwithID(sql)

                'is it a mirror or single client/will
                If RadioButton1.Checked = True Then 'single
                    sql = "insert into will_instruction_stage1_data (willClientSequence,typeOfWill) values ('" + clientID + "','single') "
                Else
                    sql = "insert into will_instruction_stage1_data (willClientSequence,typeOfWill) values ('" + clientID + "','mirror') "
                End If
                runSQL(sql)

                'now insert client details
                sql = "insert into will_instruction_stage2_data (willClientSequence,person1Title,person1Surname,person1Forenames,person1Address,person1Postcode,person1DOB,person1MaritalStatus,person1HomeNumber,person1MobileNumber,person1Email,person2Title,person2Surname,person2Forenames,person2Address,person2Postcode,person2DOB,person2MaritalStatus,person2HomeNumber,person2MobileNumber,person2Email,clientReferenceNumber) values ('" + clientID + "','" + SqlSafe(person1Title.SelectedItem) + "','" + SqlSafe(person1Surname.Text) + "','" + SqlSafe(person1Forenames.Text) + "','" + SqlSafe(person1Address.Text) + "','" + SqlSafe(person1Postcode.Text) + "','" + SqlSafe(person1DOB.Value) + "','" + SqlSafe(person1MaritalStatus.SelectedItem) + "','" + SqlSafe(person1HomeNumber.Text) + "','" + SqlSafe(person1MobileNumber.Text) + "','" + SqlSafe(person1Email.Text) + "','" + SqlSafe(person2Title.SelectedItem) + "','" + SqlSafe(person2Surname.Text) + "','" + SqlSafe(person2Forenames.Text) + "','" + SqlSafe(person2Address.Text) + "','" + SqlSafe(person2Postcode.Text) + "','" + SqlSafe(person2DOB.Value) + "','" + SqlSafe(person2MaritalStatus.SelectedItem) + "','" + SqlSafe(person2HomeNumber.Text) + "','" + SqlSafe(person2MobileNumber.Text) + "','" + SqlSafe(person2Email.Text) + "','" + SqlSafe(clientReferenceNumber.Text) + "')"
                runSQL(sql)
            Else
                'it's an update

                'is it a mirror or single client/will
                If RadioButton1.Checked = True Then 'single
                    sql = "update  will_instruction_stage1_data set typeOfWill='single' where willClientSequence=" + clientID
                Else
                    sql = "update  will_instruction_stage1_data set typeOfWill='mirror' where willClientSequence=" + clientID
                End If
                runSQL(sql)

                'now update clientdetails
                sql = "update will_instruction_stage2_data "
                sql = sql + "set person1Title='" + SqlSafe(person1Title.SelectedItem) + "',person1Surname='" + SqlSafe(person1Surname.Text) + "',person1Forenames='" + SqlSafe(person1Forenames.Text) + "',person1Address='" + SqlSafe(person1Address.Text) + "',person1Postcode='" + SqlSafe(person1Postcode.Text) + "',person1DOB='" + SqlSafe(person1DOB.Value) + "',person1MaritalStatus='" + SqlSafe(person1MaritalStatus.SelectedItem) + "',person1HomeNumber='" + SqlSafe(person1HomeNumber.Text) + "',person1MobileNumber='" + SqlSafe(person1MobileNumber.Text) + "',person1Email='" + SqlSafe(person1Email.Text) + "',"
                sql = sql + "person2Title='" + SqlSafe(person2Title.SelectedItem) + "',person2Surname='" + SqlSafe(person2Surname.Text) + "',person2Forenames='" + SqlSafe(person2Forenames.Text) + "',person2Address='" + SqlSafe(person2Address.Text) + "',person2Postcode='" + SqlSafe(person2Postcode.Text) + "',person2DOB='" + SqlSafe(person2DOB.Value) + "',person2MaritalStatus='" + SqlSafe(person2MaritalStatus.SelectedItem) + "',person2HomeNumber='" + SqlSafe(person2HomeNumber.Text) + "',person2MobileNumber='" + SqlSafe(person2MobileNumber.Text) + "',person2Email='" + SqlSafe(person2Email.Text) + "',clientReferenceNumber= '" + SqlSafe(clientReferenceNumber.Text) + "'"
                sql = sql + " where willClientSequence=" + clientID
                runSQL(sql)
            End If

            spy("client details screen closed - data saved/updated")

            Me.Close()
        End If


    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            GroupBox2.Visible = False

        Else
            GroupBox2.Visible = True
        End If
    End Sub

    Private Sub Label9_Click(sender As Object, e As EventArgs) Handles Label9.Click

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        person1Postcode.Text = person1Postcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = person1Postcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.person1Address.Text = f.FoundAddress()
            Me.person1Postcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()

        person1MaritalStatus.Select()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        person2Postcode.Text = person1Postcode.Text
        person2Address.Text = person1Address.Text
        person2Email.Text = person1Email.Text
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles person1MaritalStatus.SelectedIndexChanged

        If person1MaritalStatus.SelectedIndex = 1 Then
            'assume client 2 is the spouse
            person2MaritalStatus.SelectedIndex = person1MaritalStatus.SelectedIndex
            person2Surname.Text = person1Surname.Text
            person2Postcode.Text = person1Postcode.Text
            person2Address.Text = person1Address.Text
            person2Email.Text = person1Email.Text
        End If
    End Sub

    Private Sub person1HomeNumber_TextChanged(sender As Object, e As EventArgs) Handles person1HomeNumber.TextChanged
        If person1MaritalStatus.SelectedIndex = 1 Then
            person2HomeNumber.Text = person1HomeNumber.Text
        End If
    End Sub

    Private Sub person1Postcode_TextChanged(sender As Object, e As EventArgs) Handles person1Postcode.TextChanged

    End Sub

    Private Sub person1Address_TextChanged(sender As Object, e As EventArgs) Handles person1Address.TextChanged

    End Sub
End Class