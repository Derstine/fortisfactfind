﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class executor_edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.executorTitle = New System.Windows.Forms.ComboBox()
        Me.executorNames = New System.Windows.Forms.TextBox()
        Me.executorSurname = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.executorAddress = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.executorPostcode = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.trusteeFlag = New System.Windows.Forms.ComboBox()
        Me.executorPhone = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.executorEmail = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.executorType = New System.Windows.Forms.ComboBox()
        Me.executorRelationshipClient2 = New System.Windows.Forms.ComboBox()
        Me.executorRelationshipClient1 = New System.Windows.Forms.ComboBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.black
        Me.Button3.Location = New System.Drawing.Point(391, 534)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(171, 36)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Update Executor"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.black
        Me.Button1.Location = New System.Drawing.Point(568, 534)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 36)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(186, 20)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Edit An Executor/Trustee"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(24, 56)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(34, 16)
        Me.Label50.TabIndex = 45
        Me.Label50.Text = "Title"
        '
        'executorTitle
        '
        Me.executorTitle.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorTitle.FormattingEnabled = True
        Me.executorTitle.Items.AddRange(New Object() {"Select...", "Mr", "Mrs", "Miss", "Ms"})
        Me.executorTitle.Location = New System.Drawing.Point(27, 76)
        Me.executorTitle.Name = "executorTitle"
        Me.executorTitle.Size = New System.Drawing.Size(97, 24)
        Me.executorTitle.TabIndex = 0
        '
        'executorNames
        '
        Me.executorNames.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorNames.Location = New System.Drawing.Point(139, 77)
        Me.executorNames.Name = "executorNames"
        Me.executorNames.Size = New System.Drawing.Size(289, 22)
        Me.executorNames.TabIndex = 1
        '
        'executorSurname
        '
        Me.executorSurname.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorSurname.Location = New System.Drawing.Point(443, 77)
        Me.executorSurname.Name = "executorSurname"
        Me.executorSurname.Size = New System.Drawing.Size(218, 22)
        Me.executorSurname.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(136, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 16)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "Forenames"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(440, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 16)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Surname"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(232, 122)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Lookup"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'executorAddress
        '
        Me.executorAddress.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorAddress.Location = New System.Drawing.Point(139, 151)
        Me.executorAddress.Multiline = True
        Me.executorAddress.Name = "executorAddress"
        Me.executorAddress.Size = New System.Drawing.Size(268, 102)
        Me.executorAddress.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(74, 154)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 62
        Me.Label8.Text = "Address"
        '
        'executorPostcode
        '
        Me.executorPostcode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorPostcode.Location = New System.Drawing.Point(139, 123)
        Me.executorPostcode.Name = "executorPostcode"
        Me.executorPostcode.Size = New System.Drawing.Size(87, 22)
        Me.executorPostcode.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(60, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 16)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Postcode"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 357)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 16)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "Relationship to Client 1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(24, 387)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 16)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Relationship to Client 2"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(89, 432)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 16)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Role"
        '
        'trusteeFlag
        '
        Me.trusteeFlag.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.trusteeFlag.FormattingEnabled = True
        Me.trusteeFlag.Items.AddRange(New Object() {"Select...", "Executor & Trustee", "Executor Only", "Trustee Only"})
        Me.trusteeFlag.Location = New System.Drawing.Point(139, 429)
        Me.trusteeFlag.Name = "trusteeFlag"
        Me.trusteeFlag.Size = New System.Drawing.Size(170, 24)
        Me.trusteeFlag.TabIndex = 10
        '
        'executorPhone
        '
        Me.executorPhone.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorPhone.Location = New System.Drawing.Point(139, 268)
        Me.executorPhone.Name = "executorPhone"
        Me.executorPhone.Size = New System.Drawing.Size(218, 22)
        Me.executorPhone.TabIndex = 6
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(86, 271)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 16)
        Me.Label10.TabIndex = 72
        Me.Label10.Text = "Phone"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(86, 312)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(42, 16)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Email"
        '
        'executorEmail
        '
        Me.executorEmail.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorEmail.Location = New System.Drawing.Point(139, 309)
        Me.executorEmail.Name = "executorEmail"
        Me.executorEmail.Size = New System.Drawing.Size(522, 22)
        Me.executorEmail.TabIndex = 7
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(38, 472)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(95, 16)
        Me.Label12.TabIndex = 76
        Me.Label12.Text = "Executor Type"
        '
        'executorType
        '
        Me.executorType.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorType.FormattingEnabled = True
        Me.executorType.Items.AddRange(New Object() {"Select...", "Sole Executor", "Joint Executor", "Substitute Executor"})
        Me.executorType.Location = New System.Drawing.Point(139, 469)
        Me.executorType.Name = "executorType"
        Me.executorType.Size = New System.Drawing.Size(170, 24)
        Me.executorType.TabIndex = 11
        '
        'executorRelationshipClient2
        '
        Me.executorRelationshipClient2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorRelationshipClient2.FormattingEnabled = True
        Me.executorRelationshipClient2.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.executorRelationshipClient2.Location = New System.Drawing.Point(203, 384)
        Me.executorRelationshipClient2.Name = "executorRelationshipClient2"
        Me.executorRelationshipClient2.Size = New System.Drawing.Size(204, 24)
        Me.executorRelationshipClient2.TabIndex = 79
        '
        'executorRelationshipClient1
        '
        Me.executorRelationshipClient1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.executorRelationshipClient1.FormattingEnabled = True
        Me.executorRelationshipClient1.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.executorRelationshipClient1.Location = New System.Drawing.Point(203, 354)
        Me.executorRelationshipClient1.Name = "executorRelationshipClient1"
        Me.executorRelationshipClient1.Size = New System.Drawing.Size(204, 24)
        Me.executorRelationshipClient1.TabIndex = 78
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button4.ForeColor = System.Drawing.Color.black
        Me.Button4.Location = New System.Drawing.Point(12, 527)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(99, 36)
        Me.Button4.TabIndex = 80
        Me.Button4.Text = "Delete"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'executor_edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(678, 575)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.executorRelationshipClient2)
        Me.Controls.Add(Me.executorRelationshipClient1)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.executorType)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.executorEmail)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.executorPhone)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.trusteeFlag)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.executorAddress)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.executorPostcode)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.executorSurname)
        Me.Controls.Add(Me.executorNames)
        Me.Controls.Add(Me.executorTitle)
        Me.Controls.Add(Me.Label50)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Name = "executor_edit"
        Me.Text = "Edit An Executor/Trustee"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button3 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents executorTitle As ComboBox
    Friend WithEvents executorNames As TextBox
    Friend WithEvents executorSurname As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents executorAddress As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents executorPostcode As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents trusteeFlag As ComboBox
    Friend WithEvents executorPhone As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents executorEmail As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents executorType As ComboBox
    Friend WithEvents executorRelationshipClient2 As ComboBox
    Friend WithEvents executorRelationshipClient1 As ComboBox
    Friend WithEvents Button4 As Button
End Class
