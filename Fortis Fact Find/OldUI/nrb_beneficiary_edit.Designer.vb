﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class nrb_beneficiary_edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.beneficiaryTitle = New System.Windows.Forms.ComboBox()
        Me.beneficiaryForenames = New System.Windows.Forms.TextBox()
        Me.beneficiarySurname = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.beneficiaryAddress = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.beneficiaryPostcode = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.recipientType = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.primaryBeneficiaryFlag = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.relationToClient2 = New System.Windows.Forms.ComboBox()
        Me.relationToClient1 = New System.Windows.Forms.ComboBox()
        Me.atWhatAgeIssue = New System.Windows.Forms.ComboBox()
        Me.atWhatAge = New System.Windows.Forms.ComboBox()
        Me.atWhatAgeIssueLabel = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.percentage = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.groupPrimaryBeneficiaryFlag = New System.Windows.Forms.ComboBox()
        Me.groupAtWhatAgeIssue = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.groupAtWhatAge = New System.Windows.Forms.ComboBox()
        Me.groupAtWhatAgeIssueLabel = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.beneficiaryName = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.black
        Me.Button3.Location = New System.Drawing.Point(746, 613)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(89, 36)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Update"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.black
        Me.Button1.Location = New System.Drawing.Point(841, 613)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 36)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(181, 20)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Edit An NRB Beneficiary"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(131, 22)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(34, 16)
        Me.Label50.TabIndex = 45
        Me.Label50.Text = "Title"
        '
        'beneficiaryTitle
        '
        Me.beneficiaryTitle.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryTitle.FormattingEnabled = True
        Me.beneficiaryTitle.Items.AddRange(New Object() {"Select...", "Mr", "Mrs", "Miss", "Ms"})
        Me.beneficiaryTitle.Location = New System.Drawing.Point(180, 19)
        Me.beneficiaryTitle.Name = "beneficiaryTitle"
        Me.beneficiaryTitle.Size = New System.Drawing.Size(97, 24)
        Me.beneficiaryTitle.TabIndex = 0
        '
        'beneficiaryForenames
        '
        Me.beneficiaryForenames.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryForenames.Location = New System.Drawing.Point(180, 61)
        Me.beneficiaryForenames.Name = "beneficiaryForenames"
        Me.beneficiaryForenames.Size = New System.Drawing.Size(289, 22)
        Me.beneficiaryForenames.TabIndex = 1
        '
        'beneficiarySurname
        '
        Me.beneficiarySurname.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiarySurname.Location = New System.Drawing.Point(180, 100)
        Me.beneficiarySurname.Name = "beneficiarySurname"
        Me.beneficiarySurname.Size = New System.Drawing.Size(218, 22)
        Me.beneficiarySurname.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(88, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 16)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "Forenames"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(103, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 16)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Surname"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(273, 141)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Lookup"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'beneficiaryAddress
        '
        Me.beneficiaryAddress.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryAddress.Location = New System.Drawing.Point(180, 187)
        Me.beneficiaryAddress.Multiline = True
        Me.beneficiaryAddress.Name = "beneficiaryAddress"
        Me.beneficiaryAddress.Size = New System.Drawing.Size(268, 88)
        Me.beneficiaryAddress.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(106, 190)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 62
        Me.Label8.Text = "Address"
        '
        'beneficiaryPostcode
        '
        Me.beneficiaryPostcode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryPostcode.Location = New System.Drawing.Point(180, 142)
        Me.beneficiaryPostcode.Name = "beneficiaryPostcode"
        Me.beneficiaryPostcode.Size = New System.Drawing.Size(87, 22)
        Me.beneficiaryPostcode.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(99, 148)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 16)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Postcode"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 295)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 16)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "Relationship to Client 1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(22, 329)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 16)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Relationship to Client 2"
        '
        'recipientType
        '
        Me.recipientType.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.recipientType.FormattingEnabled = True
        Me.recipientType.Items.AddRange(New Object() {"Select...", "Individual", "Group"})
        Me.recipientType.Location = New System.Drawing.Point(145, 65)
        Me.recipientType.Name = "recipientType"
        Me.recipientType.Size = New System.Drawing.Size(259, 24)
        Me.recipientType.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 16)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Beneficiary Type"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.primaryBeneficiaryFlag)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.relationToClient2)
        Me.GroupBox1.Controls.Add(Me.relationToClient1)
        Me.GroupBox1.Controls.Add(Me.atWhatAgeIssue)
        Me.GroupBox1.Controls.Add(Me.Label50)
        Me.GroupBox1.Controls.Add(Me.atWhatAge)
        Me.GroupBox1.Controls.Add(Me.beneficiaryTitle)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.atWhatAgeIssueLabel)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.beneficiaryForenames)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.RadioButton7)
        Me.GroupBox1.Controls.Add(Me.RadioButton8)
        Me.GroupBox1.Controls.Add(Me.beneficiarySurname)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.beneficiaryAddress)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.beneficiaryPostcode)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(32, 104)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(634, 496)
        Me.GroupBox1.TabIndex = 71
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Named Individual"
        '
        'primaryBeneficiaryFlag
        '
        Me.primaryBeneficiaryFlag.FormattingEnabled = True
        Me.primaryBeneficiaryFlag.Items.AddRange(New Object() {"Select...", "Y", "N"})
        Me.primaryBeneficiaryFlag.Location = New System.Drawing.Point(180, 374)
        Me.primaryBeneficiaryFlag.Name = "primaryBeneficiaryFlag"
        Me.primaryBeneficiaryFlag.Size = New System.Drawing.Size(63, 21)
        Me.primaryBeneficiaryFlag.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(34, 375)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(131, 16)
        Me.Label10.TabIndex = 95
        Me.Label10.Text = "Primary Beneficiary?"
        '
        'relationToClient2
        '
        Me.relationToClient2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.relationToClient2.FormattingEnabled = True
        Me.relationToClient2.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.relationToClient2.Location = New System.Drawing.Point(180, 326)
        Me.relationToClient2.Name = "relationToClient2"
        Me.relationToClient2.Size = New System.Drawing.Size(204, 24)
        Me.relationToClient2.TabIndex = 7
        '
        'relationToClient1
        '
        Me.relationToClient1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.relationToClient1.FormattingEnabled = True
        Me.relationToClient1.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.relationToClient1.Location = New System.Drawing.Point(180, 292)
        Me.relationToClient1.Name = "relationToClient1"
        Me.relationToClient1.Size = New System.Drawing.Size(204, 24)
        Me.relationToClient1.TabIndex = 6
        '
        'atWhatAgeIssue
        '
        Me.atWhatAgeIssue.FormattingEnabled = True
        Me.atWhatAgeIssue.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.atWhatAgeIssue.Location = New System.Drawing.Point(385, 444)
        Me.atWhatAgeIssue.Name = "atWhatAgeIssue"
        Me.atWhatAgeIssue.Size = New System.Drawing.Size(63, 21)
        Me.atWhatAgeIssue.TabIndex = 12
        '
        'atWhatAge
        '
        Me.atWhatAge.FormattingEnabled = True
        Me.atWhatAge.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.atWhatAge.Location = New System.Drawing.Point(180, 443)
        Me.atWhatAge.Name = "atWhatAge"
        Me.atWhatAge.Size = New System.Drawing.Size(63, 21)
        Me.atWhatAge.TabIndex = 11
        '
        'atWhatAgeIssueLabel
        '
        Me.atWhatAgeIssueLabel.AutoSize = True
        Me.atWhatAgeIssueLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.atWhatAgeIssueLabel.Location = New System.Drawing.Point(260, 444)
        Me.atWhatAgeIssueLabel.Name = "atWhatAgeIssueLabel"
        Me.atWhatAgeIssueLabel.Size = New System.Drawing.Size(119, 16)
        Me.atWhatAgeIssueLabel.TabIndex = 88
        Me.atWhatAgeIssueLabel.Text = "At what age issue?"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(81, 444)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(84, 16)
        Me.Label19.TabIndex = 87
        Me.Label19.Text = "At what age?"
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.Location = New System.Drawing.Point(250, 411)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(58, 20)
        Me.RadioButton7.TabIndex = 10
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "Issue"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.Location = New System.Drawing.Point(180, 411)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(64, 20)
        Me.RadioButton8.TabIndex = 9
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Lapse"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(62, 412)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(103, 16)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Lapse or Issue?"
        '
        'percentage
        '
        Me.percentage.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.percentage.Location = New System.Drawing.Point(559, 65)
        Me.percentage.Name = "percentage"
        Me.percentage.Size = New System.Drawing.Size(45, 22)
        Me.percentage.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(436, 68)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(117, 16)
        Me.Label9.TabIndex = 72
        Me.Label9.Text = "Percentage Share"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.groupPrimaryBeneficiaryFlag)
        Me.GroupBox3.Controls.Add(Me.groupAtWhatAgeIssue)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.groupAtWhatAge)
        Me.GroupBox3.Controls.Add(Me.groupAtWhatAgeIssueLabel)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.RadioButton2)
        Me.GroupBox3.Controls.Add(Me.RadioButton1)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.beneficiaryName)
        Me.GroupBox3.Location = New System.Drawing.Point(672, 204)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(634, 270)
        Me.GroupBox3.TabIndex = 74
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Group"
        '
        'groupPrimaryBeneficiaryFlag
        '
        Me.groupPrimaryBeneficiaryFlag.FormattingEnabled = True
        Me.groupPrimaryBeneficiaryFlag.Items.AddRange(New Object() {"Select...", "Y", "N"})
        Me.groupPrimaryBeneficiaryFlag.Location = New System.Drawing.Point(169, 74)
        Me.groupPrimaryBeneficiaryFlag.Name = "groupPrimaryBeneficiaryFlag"
        Me.groupPrimaryBeneficiaryFlag.Size = New System.Drawing.Size(63, 21)
        Me.groupPrimaryBeneficiaryFlag.TabIndex = 1
        '
        'groupAtWhatAgeIssue
        '
        Me.groupAtWhatAgeIssue.FormattingEnabled = True
        Me.groupAtWhatAgeIssue.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.groupAtWhatAgeIssue.Location = New System.Drawing.Point(372, 154)
        Me.groupAtWhatAgeIssue.Name = "groupAtWhatAgeIssue"
        Me.groupAtWhatAgeIssue.Size = New System.Drawing.Size(63, 21)
        Me.groupAtWhatAgeIssue.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(23, 75)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(131, 16)
        Me.Label11.TabIndex = 97
        Me.Label11.Text = "Primary Beneficiary?"
        '
        'groupAtWhatAge
        '
        Me.groupAtWhatAge.FormattingEnabled = True
        Me.groupAtWhatAge.Items.AddRange(New Object() {"0", "18", "19", "20", "21", "22", "23", "24", "25"})
        Me.groupAtWhatAge.Location = New System.Drawing.Point(171, 154)
        Me.groupAtWhatAge.Name = "groupAtWhatAge"
        Me.groupAtWhatAge.Size = New System.Drawing.Size(63, 21)
        Me.groupAtWhatAge.TabIndex = 4
        '
        'groupAtWhatAgeIssueLabel
        '
        Me.groupAtWhatAgeIssueLabel.AutoSize = True
        Me.groupAtWhatAgeIssueLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupAtWhatAgeIssueLabel.Location = New System.Drawing.Point(247, 155)
        Me.groupAtWhatAgeIssueLabel.Name = "groupAtWhatAgeIssueLabel"
        Me.groupAtWhatAgeIssueLabel.Size = New System.Drawing.Size(119, 16)
        Me.groupAtWhatAgeIssueLabel.TabIndex = 77
        Me.groupAtWhatAgeIssueLabel.Text = "At what age issue?"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(53, 155)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(84, 16)
        Me.Label14.TabIndex = 76
        Me.Label14.Text = "At what age?"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(250, 118)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(58, 20)
        Me.RadioButton2.TabIndex = 3
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Issue"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(171, 118)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(64, 20)
        Me.RadioButton1.TabIndex = 2
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Lapse"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(34, 120)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(103, 16)
        Me.Label12.TabIndex = 71
        Me.Label12.Text = "Lapse or Issue?"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(52, 37)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 16)
        Me.Label13.TabIndex = 70
        Me.Label13.Text = "Group Name"
        '
        'beneficiaryName
        '
        Me.beneficiaryName.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beneficiaryName.Location = New System.Drawing.Point(171, 34)
        Me.beneficiaryName.Name = "beneficiaryName"
        Me.beneficiaryName.Size = New System.Drawing.Size(326, 22)
        Me.beneficiaryName.TabIndex = 0
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button4.ForeColor = System.Drawing.Color.black
        Me.Button4.Location = New System.Drawing.Point(12, 613)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 36)
        Me.Button4.TabIndex = 75
        Me.Button4.Text = "Delete"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'nrb_beneficiary_edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(952, 656)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.percentage)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.recipientType)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Name = "nrb_beneficiary_edit"
        Me.Text = "Edit An NRB Beneficiary"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button3 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents beneficiaryTitle As ComboBox
    Friend WithEvents beneficiaryForenames As TextBox
    Friend WithEvents beneficiarySurname As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents beneficiaryAddress As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents beneficiaryPostcode As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents recipientType As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents percentage As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents beneficiaryName As TextBox
    Friend WithEvents atWhatAge As ComboBox
    Friend WithEvents atWhatAgeIssueLabel As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents RadioButton7 As RadioButton
    Friend WithEvents RadioButton8 As RadioButton
    Friend WithEvents Label20 As Label
    Friend WithEvents groupAtWhatAgeIssueLabel As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents atWhatAgeIssue As ComboBox
    Friend WithEvents groupAtWhatAgeIssue As ComboBox
    Friend WithEvents groupAtWhatAge As ComboBox
    Friend WithEvents relationToClient2 As ComboBox
    Friend WithEvents relationToClient1 As ComboBox
    Friend WithEvents primaryBeneficiaryFlag As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents groupPrimaryBeneficiaryFlag As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Button4 As Button
End Class
