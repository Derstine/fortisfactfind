﻿Public Class probate_relative_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.title.SelectedIndex = 0
        Me.relationship.SelectedIndex = 0

        Me.dependant.SelectedIndex = 0
        Me.dependant.SelectedIndex = 0


        Me.Label2.Text = "Relationship to " + getClientFirstName(whoDied)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If title.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the guardian's title" + vbCrLf
        If forenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the guardian's name" + vbCrLf
        If dependant.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the guardian's role" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into probate_relative (willClientSequence,whichClient,title,forenames,surname,relationship,address,postcode,dependant,phone,email) values ('" + clientID + "','" + whoDied + "','" + SqlSafe(title.SelectedItem) + "','" + SqlSafe(forenames.Text) + "','" + SqlSafe(surname.Text) + "','" + SqlSafe(relationship.SelectedItem) + "','" + SqlSafe(address.Text) + "','" + SqlSafe(postcode.Text) + "','" + SqlSafe(dependant.SelectedItem) + "','" + SqlSafe(phone.Text) + "','" + SqlSafe(email.Text) + "')"
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        postcode.Text = postcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = postcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.address.Text = f.FoundAddress()
            Me.postcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person1Postcode,person1Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        postcode.Text = results(0)
        address.Text = results(1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person2Postcode,person2Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        postcode.Text = results(0)
        address.Text = results(1)
    End Sub

    Private Sub guardianPostcode_TextChanged(sender As Object, e As EventArgs) Handles postcode.TextChanged

    End Sub

    Private Sub guardianAddress_TextChanged(sender As Object, e As EventArgs) Handles address.TextChanged

    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs) Handles Label7.Click

    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click

    End Sub
End Class