﻿Imports System.Threading
Imports System.Globalization
Public Class factfind_wizard
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        spy("Wizard screen opened")

    End Sub

    Private Function setDefaults()


        DataGridView2.Rows.Clear()    'assets tab

        manBasicWillFlag.Checked = False
        manWillReviewFlag.Checked = False
        manWillPPTFlag.Checked = False
        manHWFlag.Checked = False
        manPFAFlag.Checked = False
        manFAPTFlag.Checked = False
        manProbatePlanFlag.Checked = False


        Me.propertyNo.Checked = True
        Me.mortgageFreeNo.Checked = True
        Me.protectPropertyNo.Checked = True
        Me.protectAssetsNo.Checked = True
        Me.lpaPFANo.Checked = True
        Me.anyoneDisabledNo.Checked = True
        Me.funeralPlanNo.Checked = True
        Me.probatePlanNo.Checked = True


        Me.client1Feelings.SelectedIndex = 0
        Me.client2Feelings.SelectedIndex = 0

        Panel4.Visible = False

        Me.howIsClientPaying.Text = ""
        Me.paymentAmount.Text = ""

        Me.client_storageButtonYes.Checked = False
        Me.client_storageButtonNo.Checked = False

        Me.probateInflation.Text = inflationRate

        Return True
    End Function

    Private Function populateScreen()

        Dim sql = ""
        Dim results As Array

        'make the label names user-friendly by putting client name in everywhere
        Dim client1Name As String = getClientFirstName("client1")
        Dim client2Name As String = getClientFirstName("client2")

        DataGridView2.Columns(3).HeaderText = client1Name

        If clientType() = "single" Then     'show/hide the appropriate panels/group boxes


            TextBox9.Visible = False
            TextBox10.Visible = False
            TextBox74.Visible = False
            Label76.Visible = False
            TextBox5.Visible = False
            TextBox2.Visible = False
            client2Feelings.Visible = False
            TextBox3.Visible = False
            TextBox4.Visible = False

            DataGridView2.Columns(4).HeaderText = "n/a"
            DataGridView2.Columns(5).HeaderText = "n/a"

        Else


            TextBox9.Visible = True
            TextBox10.Visible = True
            TextBox74.Visible = True
            Label76.Visible = True
            TextBox5.Visible = True
            TextBox2.Visible = True
            client2Feelings.Visible = True
            TextBox3.Visible = True
            TextBox4.Visible = True

            DataGridView2.Columns(4).HeaderText = client2Name
            DataGridView2.Columns(5).HeaderText = "Joint"


        End If

        'populate Products & Services Tab
        'client 1
        sql = "select * from client_products where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) = "Y" Then propertyYes.Checked = True Else propertyNo.Checked = True
            If results(4) = "Y" Then mortgageFreeYes.Checked = True Else mortgageFreeNo.Checked = True
            If results(5) = "Y" Then protectPropertyYes.Checked = True Else protectPropertyNo.Checked = True
            If results(6) = "Y" Then protectAssetsYes.Checked = True Else protectAssetsNo.Checked = True
            If results(7) = "Y" Then lpaPFAYes.Checked = True Else lpaPFANo.Checked = True

            If results(12) = "Y" Then anyoneDisabledYes.Checked = True Else anyoneDisabledNo.Checked = True

            If results(14) = "Y" Then funeralPlanYes.Checked = True Else funeralPlanNo.Checked = True
            If results(15) = "Y" Then probatePlanYes.Checked = True Else probatePlanNo.Checked = True
        End If

        'populate assets tab
        redrawAssetsGrid()

        'client 1
        sql = "select * from client_assets where willClientSequence=" + clientID + " and whichClient='client1'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox6.Text = results(3)
            If results(4) <> "0" Then TextBox1.Text = results(4)
            If results(5) <> "" Then client1Feelings.SelectedItem = results(5)
        End If
        'client 2
        sql = "select * from client_assets where willClientSequence=" + clientID + " and whichClient='client2'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox5.Text = results(3)
            If results(4) <> "0" Then TextBox2.Text = results(4)
            If results(5) <> "" Then client2Feelings.SelectedItem = results(5)
        End If

        'populate probate tab
        recalculate_probate_cost()


        'storage options tab
        sql = "select * from client_storage where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "" And results(0) <> "ERROR" Then

            If results(3) = "Y" Then
                client_storageButtonYes.Checked = True
            Else
                client_storageButtonNo.Checked = True
            End If

            storageProduct.Text = results(4)
            storageAmount.Text = results(5)
            storageSortcode.Text = results(6)
            storageAccountNumber.Text = results(7)
            storagePaymentMethod.Text = results(8)
        Else
            'no data to be found, therefore disable everything
            client_storageButtonNo.Checked = True

        End If


        'client offer tab
        sql = "select * from client_wizard where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) <> "" And results(0) <> "ERROR" Then
            'ignore, as no existing client data
            If results(2) = "Y" Then
                'populate data
                If results(3) = "Y" Then manWillReviewFlag.Checked = True Else manWillReviewFlag.Checked = False
                If results(4) = "Y" Then manWillPPTFlag.Checked = True Else manWillPPTFlag.Checked = False
                If results(5) = "Y" Then manBasicWillFlag.Checked = True Else manBasicWillFlag.Checked = False
                If results(6) = "Y" Then manHWFlag.Checked = True Else manHWFlag.Checked = False
                If results(7) = "Y" Then manPFAFlag.Checked = True Else manPFAFlag.Checked = False
                If results(8) = "Y" Then manFAPTFlag.Checked = True Else manFAPTFlag.Checked = False
                If results(9) = "Y" Then manProbatePlanFlag.Checked = True Else manProbatePlanFlag.Checked = False
                If results(10) = "Y" Then
                    manOtherFlag.Checked = True
                    manOtherPrice.Text = results(11)
                    manOtherDescription.Text = results(12)
                Else
                    manOtherFlag.Checked = False
                    manOtherPrice.Text = ""
                    manOtherDescription.Text = ""
                End If

                If results(13) = "Y" Then manPFAOPG.Checked = True Else manPFAOPG.Checked = False
                If results(14) = "Y" Then manHWOPG.Checked = True Else manHWOPG.Checked = False

            Else
                'its not a manual override, so wizard will work it out each time
            End If

            recalculateOffer()


        End If

        If getWillLookup("howIsClientPaying") <> "ERROR" And getWillLookup("howIsClientPaying") <> "" Then Me.howIsClientPaying.Text = getWillLookup("howIsClientPaying")
        If getWillLookup("paymentAmount") <> "ERROR" And getWillLookup("paymentAmount") <> "" Then Me.paymentAmount.Text = getWillLookup("paymentAmount")

        Return True

    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        saveData()
        spy("Wizard screen closed")

        Me.Close()
    End Sub


    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged

        Dim indexOfSelectedTab As Integer = TabControl1.SelectedIndex

        If indexOfSelectedTab = 0 Then spy("Wizard establishing facts tab opened")
        If indexOfSelectedTab = 1 Then spy("Wizard asset information tab opened")
        If indexOfSelectedTab = 2 Then spy("Wizard storage options tab opened")

        If indexOfSelectedTab = 3 Then
            spy("Wizard recommendations tab opened")
            'need to recalculate offer

            recalculateOffer()
        End If

        If indexOfSelectedTab = 4 Then
            spy("Wizard probate tab opened")

        End If

        If indexOfSelectedTab = 5 Then spy("Wizard client follow up tab opened")
        If indexOfSelectedTab = 6 Then spy("Wizard trustee switch tab opened")
        If indexOfSelectedTab = 7 Then spy("Wizard savings tab opened")
        If indexOfSelectedTab = 8 Then spy("Wizard funeral forecast tab opened")

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        assets_add.ShowDialog(Me)
        redrawAssetsGrid()
    End Sub


    Function redrawAssetsGrid()

        DataGridView2.Rows.Clear()

        Dim sql = ""
        Dim client1 As Single = 0
        Dim client2 As Single = 0
        Dim joint As Single = 0

        Dim assettype = ""
        Dim details = ""
        Dim client1Value = ""
        Dim client2Value = ""
        Dim jointValue = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn

        'let's do property first
        sql = "select * from fapt_trust_fund_property where willClientSequence=" + clientID
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            assettype = "Property"
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(11).ToString
            If dbreader.GetValue(9).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(9).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(9).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "edit", "fapt_trust_fund_property")
        End While
        dbreader.Close()

        'let's do chattels next
        sql = "select * from fapt_trust_fund_chattels where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            assettype = dbreader.GetValue(5).ToString
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(2).ToString
            If dbreader.GetValue(4).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "edit", "fapt_trust_fund_chattels")
        End While

        dbreader.Close()

        ' let's do cash next
        sql = "select * from fapt_trust_fund_cash where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader

        While dbreader.Read()
            assettype = dbreader.GetValue(5).ToString
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(2).ToString
            If dbreader.GetValue(4).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "edit", "fapt_trust_fund_cash")
        End While
        dbreader.Close()

        ' finally, let's do "misc" next
        sql = "select * from fapt_trust_fund_further where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader

        While dbreader.Read()
            assettype = dbreader.GetValue(5).ToString
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(2).ToString
            If dbreader.GetValue(4).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "edit", "fapt_trust_fund_further")
        End While
        dbreader.Close()

        TextBox11.Text = client1.ToString("N0")
        TextBox10.Text = client2.ToString("N0")
        TextBox9.Text = joint.ToString("N0")
        TextBox74.Text = (client1 + client2 + joint).ToString("N0")


        sql = "select top 1 worth from fapt_trust_fund_property where willClientSequence=" + clientID + " order by sequence ASC"
        Dim result As String = runSQLwithID(sql)



        dbConn.Close()
        Return True

    End Function

    Function getTotalPropertyValue()
        Dim returnval = ""
        Dim sql = ""

        sql = "select SUM(worth) from fapt_trust_fund_property where willClientSequence=" + clientID
        returnval = Val(runSQLwithID(sql)).ToString("N0")

        Return returnval
    End Function

    Private Sub TextBox6_TextChanged(sender As Object, e As EventArgs) Handles TextBox6.TextChanged
        TextBox4.Text = (Val(TextBox6.Text) + Val(TextBox5.Text)).ToString
    End Sub

    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TextBox5.TextChanged
        TextBox4.Text = (Val(TextBox6.Text) + Val(TextBox5.Text)).ToString
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        TextBox3.Text = (Val(TextBox1.Text) + Val(TextBox2.Text)).ToString
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        TextBox3.Text = (Val(TextBox1.Text) + Val(TextBox2.Text)).ToString
    End Sub

    Private Sub TextBox12_TextChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub TextBox26_TextChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub TextBox25_TextChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub TextBox24_TextChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub TextBox23_TextChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub TextBox22_TextChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub TextBox21_TextChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub CheckBox14_CheckedChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub CheckBox13_CheckedChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub CheckBox12_CheckedChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub CheckBox11_CheckedChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub CheckBox10_CheckedChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub CheckBox9_CheckedChanged(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub TextBox74_TextChanged(sender As Object, e As EventArgs) Handles TextBox74.TextChanged

        totalEstateValue.Text = TextBox74.Text
    End Sub



    Private Sub Button1_Click(sender As Object, e As EventArgs)
        recalculateOffer()
    End Sub

    Private Sub client_offer_tab_active(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged

        If TabControl1.SelectedIndex = 3 Then recalculateOffer()
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles manualOverrideFlag.CheckedChanged
        If manualOverrideFlag.Checked = True Then GroupBox2.Visible = True
        If manualOverrideFlag.Checked = False Then GroupBox2.Visible = False
        recalculateManualOverride()

    End Sub

    Private Sub manWillReviewFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manWillReviewFlag.CheckedChanged
        If manWillReviewFlag.Checked = False Then Me.manWillReviewPrice.Text = ""
        recalculateManualOverride()
    End Sub

    Private Sub manWillPPTFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manWillPPTFlag.CheckedChanged
        If manWillPPTFlag.Checked = False Then Me.manWillPPTPrice.Text = ""
        recalculateManualOverride()
    End Sub

    Private Sub manBasicWillFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manBasicWillFlag.CheckedChanged
        If manBasicWillFlag.Checked = False Then Me.manBasicWillPrice.Text = ""
        recalculateManualOverride()
    End Sub

    Private Sub manHWFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manHWFlag.CheckedChanged
        If manHWFlag.Checked = False Then Me.manHWPrice.Text = ""
        recalculateManualOverride()
    End Sub

    Private Sub manPFAFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manPFAFlag.CheckedChanged
        If manPFAFlag.Checked = False Then Me.manPFAPrice.Text = ""
        recalculateManualOverride()
    End Sub

    Private Sub manHWOPG_CheckedChanged(sender As Object, e As EventArgs) Handles manHWOPG.CheckedChanged
        recalculateManualOverride()
    End Sub

    Private Sub manPFAOPG_CheckedChanged(sender As Object, e As EventArgs) Handles manPFAOPG.CheckedChanged
        recalculateManualOverride()
    End Sub

    Private Sub manFAPTFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manFAPTFlag.CheckedChanged
        If manFAPTFlag.Checked = False Then Me.manFAPTPrice.Text = ""
        recalculateManualOverride()
    End Sub

    Private Sub manProbatePlanFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manProbatePlanFlag.CheckedChanged
        If manProbatePlanFlag.Checked = False Then Me.manProbatePlanPrice.Text = ""
        recalculateManualOverride()
    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        If e.ColumnIndex = 6 And DataGridView2.Item(6, e.RowIndex).Value = "edit" Then
            'its edit
            rowID = DataGridView2.Item(0, e.RowIndex).Value.ToString
            Me.assetTablename.Text = DataGridView2.Item(7, e.RowIndex).Value.ToString
            assets_edit.ShowDialog(Me)
            rowID = ""    'reset global variable
            Me.assetTablename.Text = "assetTablename"
            redrawAssetsGrid()
        End If
    End Sub

    Private Sub manOtherFlag_CheckedChanged(sender As Object, e As EventArgs) Handles manOtherFlag.CheckedChanged
        If manOtherFlag.Checked = True Then
            Panel4.Visible = True
        Else
            Panel4.Visible = False
        End If
        recalculateManualOverride()
    End Sub

    Private Sub manOtherDescription_TextChanged(sender As Object, e As EventArgs) Handles manOtherDescription.TextChanged
        If Me.manOtherDescription.Text <> "" Then Me.manOtherPrice.Enabled = True Else Me.manOtherPrice.Enabled = False
    End Sub

    Private Sub manOtherPrice_TextChanged(sender As Object, e As EventArgs) Handles manOtherPrice.TextChanged
        recalculateManualOverride()

    End Sub


    Function saveData()
        Dim sql As String

        '''''''''''''
        '''''''''''''
        ''' NOTE: Because this screen only has a 1-to-1 relationship with the database, I'm going to be lazy and just delete the existing row and then insert a new one
        ''' rather than determining between an INSERT and an UPDATE
        ''''''''''''' 
        '''''''''''''

        'ASSETS Table First
        'step 1 - delete
        sql = "delete from client_assets where willClientSequence=" + clientID
        runSQL(sql)
        'step 2 - insert client 1
        sql = "insert into client_assets (willClientSequence,whichClient,grossIncome,emergencyAmount,feelings) values ('" + clientID + "','client1'," + Val(SqlSafe(TextBox6.Text)).ToString + "," + Val(SqlSafe(TextBox1.Text)).ToString + ",'" + SqlSafe(client1Feelings.SelectedItem) + "')"
        runSQL(sql)
        'step 3 - insert client 2
        sql = "insert into client_assets (willClientSequence,whichClient,grossIncome,emergencyAmount,feelings) values ('" + clientID + "','client2','" + Val(SqlSafe(TextBox5.Text)).ToString + "','" + Val(SqlSafe(TextBox2.Text)).ToString + "','" + SqlSafe(client2Feelings.SelectedItem) + "')"
        runSQL(sql)

        'OFFER table next
        'step 1 - delete
        sql = "delete from client_wizard where willClientSequence=" + clientID
        runSQL(sql)
        'step 2 - need to do some encoding of values here :-(
        Dim manualOverride, willReviewFlag, willPPTFlag, willBasicFlag, lpaHWFlag, lpaPFAFlag, faptFlag, probateFlag, otherFlag, otherPrice, otherDescription, manPFAOPG, manHWOPG As String
        Dim rec_willReviewFlag, rec_willPPTFlag, rec_willBasicFlag, rec_lpaHWFlag, rec_lpaPFAFlag, rec_faptFlag, rec_probateFlag, rec_storageFlag, storageFlag, followUpFlag, followUpAppointmentDate, followUpNotes As String

        If Me.manualOverrideFlag.Checked = True Then
            manualOverride = "Y"
            If Me.manWillReviewFlag.Checked = True Then willReviewFlag = "Y" Else willReviewFlag = "N"
            If Me.manWillPPTFlag.Checked = True Then willPPTFlag = "Y" Else willPPTFlag = "N"
            If Me.manBasicWillFlag.Checked = True Then willBasicFlag = "Y" Else willBasicFlag = "N"
            If Me.manHWFlag.Checked = True Then lpaHWFlag = "Y" Else lpaHWFlag = "N"
            If Me.manPFAFlag.Checked = True Then lpaPFAFlag = "Y" Else lpaPFAFlag = "N"
            If Me.manFAPTFlag.Checked = True Then faptFlag = "Y" Else faptFlag = "N"
            If Me.manProbatePlanFlag.Checked = True Then probateFlag = "Y" Else probateFlag = "N"
            If Me.manOtherFlag.Checked = True Then
                otherFlag = "Y"
                otherPrice = manOtherPrice.Text
                otherDescription = manOtherDescription.Text
            Else
                otherFlag = "N"
            End If
            If Me.manStorageFlag.Checked = True Then storageFlag = "Y" Else storageFlag = "N"

            If Me.manPFAOPG.Checked = True Then manPFAOPG = "Y" Else manPFAOPG = "N"
            If Me.manHWOPG.Checked = True Then manHWOPG = "Y" Else manHWOPG = "N"


            'need to record what the wizard recommended though, so we can see overrides
            If Me.recWillReviewFlag.Checked = True Then rec_willReviewFlag = "Y" Else rec_willReviewFlag = "N"
            If Me.recWillPPTFlag.Checked = True Then rec_willPPTFlag = "Y" Else rec_willPPTFlag = "N"
            If Me.recBasicWillFlag.Checked = True Then rec_willBasicFlag = "Y" Else rec_willBasicFlag = "N"
            If Me.recHWFlag.Checked = True Then rec_lpaHWFlag = "Y" Else rec_lpaHWFlag = "N"
            If Me.recPFAFlag.Checked = True Then rec_lpaPFAFlag = "Y" Else rec_lpaPFAFlag = "N"
            If Me.recFAPTFlag.Checked = True Then rec_faptFlag = "Y" Else rec_faptFlag = "N"
            If Me.recProbatePlanFlag.Checked = True Then rec_probateFlag = "Y" Else rec_probateFlag = "N"
            If Me.recStorageFlag.Checked = True Then rec_storageFlag = "Y" Else rec_storageFlag = "N"

            sql = "insert into client_wizard (willClientSequence,manualOverride,willReviewFlag,willPPTFlag,willBasicFlag,lpaHWFlag,lpaPFAFlag,faptFlag,probateFlag, otherFlag, otherPrice, otherDescription,manPFAOPG,manHWOPG,rec_willReviewFlag,rec_willPPTFlag,rec_willBasicFlag,rec_lpaHWFlag,rec_lpaPFAFlag,rec_faptFlag,rec_probateFlag,followUpFlag,followUpAppointmentDate,followUpNotes,rec_storageFlag,storageFlag) values ('" + clientID + "','" + manualOverride + "','" + willReviewFlag + "','" + willPPTFlag + "','" + willBasicFlag + "','" + lpaHWFlag + "','" + lpaPFAFlag + "','" + faptFlag + "','" + probateFlag + "','" + otherFlag + "', '" + SqlSafe(otherPrice) + "', '" + SqlSafe(otherDescription) + "','" + SqlSafe(manPFAOPG) + "','" + SqlSafe(manHWOPG) + "','" + rec_willReviewFlag + "','" + rec_willPPTFlag + "','" + rec_willBasicFlag + "','" + rec_lpaHWFlag + "','" + rec_lpaPFAFlag + "','" + rec_faptFlag + "','" + rec_probateFlag + "','" + followUpFlag + "','" + followUpAppointmentDate + "','" + SqlSafe(followUpNotes) + "','" + rec_storageFlag + "','" + storageFlag + "')"

        Else
            manualOverride = "N"
            If Me.recWillReviewFlag.Checked = True Then rec_willReviewFlag = "Y" Else rec_willReviewFlag = "N"
            If Me.recWillPPTFlag.Checked = True Then rec_willPPTFlag = "Y" Else rec_willPPTFlag = "N"
            If Me.recBasicWillFlag.Checked = True Then rec_willBasicFlag = "Y" Else rec_willBasicFlag = "N"
            If Me.recHWFlag.Checked = True Then rec_lpaHWFlag = "Y" Else rec_lpaHWFlag = "N"
            If Me.recPFAFlag.Checked = True Then rec_lpaPFAFlag = "Y" Else rec_lpaPFAFlag = "N"
            If Me.recFAPTFlag.Checked = True Then rec_faptFlag = "Y" Else rec_faptFlag = "N"
            If Me.recProbatePlanFlag.Checked = True Then rec_probateFlag = "Y" Else rec_probateFlag = "N"
            otherFlag = "N"
            otherPrice = ""
            otherDescription = ""
            manPFAOPG = ""
            manHWOPG = ""

            sql = "insert into client_wizard (willClientSequence,manualOverride,rec_willReviewFlag,rec_willPPTFlag,rec_willBasicFlag,rec_lpaHWFlag,rec_lpaPFAFlag,rec_faptFlag,rec_probateFlag, otherFlag, otherPrice, otherDescription,manPFAOPG,manHWOPG,followUpFlag,followUpAppointmentDate,followUpNotes) values ('" + clientID + "','" + manualOverride + "','" + rec_willReviewFlag + "','" + rec_willPPTFlag + "','" + rec_willBasicFlag + "','" + rec_lpaHWFlag + "','" + rec_lpaPFAFlag + "','" + rec_faptFlag + "','" + rec_probateFlag + "','" + otherFlag + "', '" + SqlSafe(otherPrice) + "', '" + SqlSafe(otherDescription) + "','" + SqlSafe(manPFAOPG) + "','" + SqlSafe(manHWOPG) + "','" + followUpFlag + "','" + followUpAppointmentDate + "','" + SqlSafe(followUpNotes) + "')"

        End If

        'step 3 - insert
        runSQL(sql)

        'step 4 - confirmation of payment method and amount
        setWillLookup("howIsClientPaying", Me.howIsClientPaying.Text)
        setWillLookup("paymentAmount", Me.paymentAmount.Text)

        'step 5 - spy on what wizard said price should be and what was charged
        setWillLookup("wizardPrice", Me.recSubTotalPrice.Text)
        setWillLookup("wizardProbatePrice", Me.recProbatePlanPrice.Text)
        setWillLookup("wizardDiscountPrice", Me.recPackagePrice.Text)

        'PRODUCTS table next
        'step 1 - delete
        sql = "delete from client_products where willClientSequence=" + clientID
        runSQL(sql)

        'step 2 - need to do some encoding of values here :-(
        Dim q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15 As String
        If propertyYes.Checked = True Then q1 = "Y" Else q1 = "N"
        If mortgageFreeYes.Checked = True Then q2 = "Y" Else q2 = "N"
        If protectPropertyYes.Checked = True Then q3 = "Y" Else q3 = "N"
        If protectAssetsYes.Checked = True Then q4 = "Y" Else q4 = "N"
        If lpaPFAYes.Checked = True Then q5 = "Y" Else q5 = "N"

        If anyoneDisabledYes.Checked = True Then q10 = "Y" Else q10 = "N"

        If funeralPlanYes.Checked = True Then q12 = "Y" Else q12 = "N"
        If probatePlanYes.Checked = True Then q13 = "Y" Else q13 = "N"

        'step 3 - insert client 1
        sql = "insert into client_products (willClientSequence,whichClient, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13) values ('" + clientID + "','', '" + q1 + "','" + q2 + "','" + q3 + "','" + q4 + "','" + q5 + "','" + q6 + "','" + q7 + "','" + q8 + "','" + q9 + "','" + q10 + "','" + q11 + "','" + q12 + "','" + q13 + "')"
        runSQL(sql)

        Return True
    End Function

    '''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''
    ''' RECALCULATION FUNCTIONS '''
    '''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''

    Function recalculateOffer()
        Dim client1Subtotal = 0
        Dim client2Subtotal = 0

        '''''''''''''''''''''''''
        ' define prices
        '''''''''''''''''''''''''

        Dim willReviewPrice = 0
        Dim singleWillPrice = 195
        Dim mirrorWillPrice = 245
        Dim PPTPrice = 895
        Dim singleLPAPrice = 345
        Dim dualLPAPrice = 595
        Dim fourLPAPrice = 895
        Dim OPGFeePrice = 82
        'Dim singlePPTandLPAPrice As Integer = 1495
        'Dim dualPPTandLPAPrice As Integer = 1895
        Dim FAPTPrice = 2995
        Dim FAPTandLPAPrice = 3595
        Dim FAPTandFullLPAPrice = 3895

        Dim singleStoragePrice = "£3 p/m"
        Dim dualStoragePrice = "£6 p/m"

        ''''''''''''''''''''
        ' now work out logic
        ''''''''''''''''''''

        ' Wills first

        If Me.mortgageFreeYes.Checked = True Then   'has will
            If Me.protectPropertyYes.Checked = True Then 'has will reviewed
                'summary: has will, recently reviewed, therefore will review only
                Me.recWillReviewFlag.Checked = True
                Me.recWillReviewPrice.Text = willReviewPrice.ToString("N0")

            End If

            If Me.protectPropertyNo.Checked = True Then 'has old will
                If Me.propertyYes.Checked = True Then 'has property
                    'summary: has will, not reviewd, has property, therefore potential PPT 
                    Me.recWillPPTFlag.Checked = True
                    Me.recWillPPTPrice.Text = PPTPrice.ToString("N0")

                End If

                If Me.propertyNo.Checked = True Then 'no property
                    'summary: has will, not reviewd, no property, therefore new basic will
                    If clientType() = "single" Then
                        Me.recBasicWillFlag.Checked = True
                        Me.recBasicWillPrice.Text = singleWillPrice.ToString("N0")

                    Else
                        'its a mirror
                        Me.recBasicWillFlag.Checked = True
                        Me.recBasicWillPrice.Text = mirrorWillPrice.ToString("N0")

                    End If
                End If
            End If
        End If

        If Me.mortgageFreeNo.Checked = True Then   'no will :-)
            If Me.propertyYes.Checked = True Then 'has property
                'summary: no will, not reviewd, has property, therefore potential PPT 
                Me.recWillPPTFlag.Checked = True
                Me.recWillPPTPrice.Text = PPTPrice.ToString("N0")

            End If

            If Me.propertyNo.Checked = True Then 'no property
                'summary: no will, not reviewd, no property, therefore new basic will
                If clientType() = "single" Then
                    Me.recBasicWillFlag.Checked = True
                    Me.recBasicWillPrice.Text = singleWillPrice.ToString("N0")

                Else
                    'its a mirror
                    Me.recBasicWillFlag.Checked = True
                    Me.recBasicWillPrice.Text = mirrorWillPrice.ToString("N0")

                End If
            End If
        End If

        ' LPAs next


        ' FAPT Next





        If Me.propertyNo.Checked = True Then
            'they have no property, therefore no assets so FAPT not relevant
            'leave with whatever has been recommended above

        End If

        'probate plan
        If Me.funeralPlanYes.Checked = True Then
            If Me.probatePlanYes.Checked = True Then
                'can't do anything here than what's been agreed above
                'as they have a professional executor, therefore probate plan
                Me.recProbatePlanFlag.Checked = False
                Me.recProbatePlanPrice.Text = ""
            End If

            If Me.probatePlanNo.Checked = True Then
                'prof executors but NO funeral plan
                Me.recProbatePlanFlag.Checked = True

            End If
        End If


        'Funeral Plan
        If Me.funeralPlanNo.Checked = True Then
            'summary: no professional executor therefore no probate plan
            'need to recalculate probate, so we have the most uptodate figure
            '   recalculateProbate()
            If Me.probatePlanYes.Checked = True Then
                'can't do anything here than what's been agreed above
                'as they have a professional executor, therefore probate plan
                Me.recProbatePlanFlag.Checked = False
                Me.recProbatePlanPrice.Text = ""
            End If
            If Me.probatePlanNo.Checked = True Then
                Me.recProbatePlanFlag.Checked = True


                ' Me.manProbatePlanFlag.Checked = True
                'Me.manProbatePlanPrice.Text = Me.probateLiabiltyClientTotal.Text
            End If
        End If

        'FAPT Part 1
        If Me.protectAssetsYes.Checked = True Or client_storageButtonYes.Checked = True Then
            'client already has will storage
            Me.recStorageFlag.Checked = False
            Me.recStoragePrice.Text = ""
        Else
            'no storage therefore it should be recommended
            Me.recStorageFlag.Checked = True
            If clientType() = "single" Then
                Me.recStoragePrice.Text = singleStoragePrice
            Else
                Me.recStoragePrice.Text = dualStoragePrice
            End If

        End If

        'total up the above

        'probate is to be removed for time being 21st January 2020
        ' Me.manProbatePlanPrice.Text = "0"
        'Me.recProbatePlanPrice.Text = "0"


        Dim recSubTotal = 0
        Dim manSubTotal = 0

        recSubTotal = Val(Me.recWillReviewPrice.Text.Replace(",", "")) + Val(Me.recBasicWillPrice.Text.Replace(",", "")) + Val(Me.recWillPPTPrice.Text.Replace(",", "")) + Val(Me.recHWPrice.Text.Replace(",", "")) + Val(Me.recPFAPrice.Text.Replace(",", "")) + Val(Me.recFAPTPrice.Text.Replace(",", "")) + Val(Me.recProbatePlanPrice.Text.Replace(",", ""))
        Me.recSubTotalPrice.Text = (recSubTotal).ToString("N0")

        'now, is there a summary package we can do, to show mega savings

        'options are FAPT only, FAPT+2 LPAs, FAPT+4 LPAs
        Dim recPackagePrice As Integer = recSubTotal    'default as there is no package
        Dim manPackagePrice = 0

        Dim numLPAs = 0
        If clientType() = "single" Then
            If Me.recHWFlag.Checked = True Then numLPAs = numLPAs + 1
            If Me.recPFAFlag.Checked = True Then numLPAs = numLPAs + 1
        Else
            If Me.recHWFlag.Checked = True Then numLPAs = numLPAs + 2
            If Me.recPFAFlag.Checked = True Then numLPAs = numLPAs + 2
        End If

        If Me.recFAPTFlag.Checked = True And numLPAs = 0 Then recPackagePrice = 2995 + Val(Me.recProbatePlanPrice.Text.Replace(",", ""))
        If Me.recFAPTFlag.Checked = True And (numLPAs = 1 Or numLPAs = 2) Then recPackagePrice = 3595 + Val(Me.recProbatePlanPrice.Text.Replace(",", ""))
        If Me.recFAPTFlag.Checked = True And (numLPAs = 3 Or numLPAs = 4) Then recPackagePrice = 3895 + Val(Me.recProbatePlanPrice.Text.Replace(",", ""))

        ' Me.recPackagePrice.Text = (recPackagePrice * VAT).ToString ' 
        Me.recPackagePrice.Text = (recPackagePrice).ToString("N0")


        Return True
    End Function


    Function recalculateManualOverride()
        '''''''''''''''''''''''''
        ' define prices
        '''''''''''''''''''''''''

        Dim willReviewPrice = 0
        Dim singleWillPrice = 195
        Dim mirrorWillPrice = 245
        Dim PPTPrice = 895
        Dim singleLPAPrice = 345
        Dim dualLPAPrice = 595
        Dim fourLPAPrice = 895
        Dim OPGFeePrice = 82
        'Dim singlePPTandLPAPrice As Integer = 1495
        'Dim dualPPTandLPAPrice As Integer = 1895
        Dim FAPTPrice = 2995
        Dim FAPTandLPAPrice = 3595
        Dim FAPTandFullLPAPrice = 3895

        Dim singleStoragePrice = "£3 p/m"
        Dim dualStoragePrice = "£6 p/m"

        Dim totalPrice = 0

        'probate is to be removed for time being 21st January 2020
        'Me.manProbatePlanPrice.Text = 0
        'Me.recProbatePlanPrice.Text = 0

        If Me.manWillReviewFlag.Checked = True Then manWillReviewPrice.Text = willReviewPrice.ToString : totalPrice = totalPrice + willReviewPrice

        If Me.manWillPPTFlag.Checked = True Then
            manWillPPTPrice.Text = PPTPrice.ToString
            totalPrice = totalPrice + PPTPrice
        End If

        If Me.manBasicWillFlag.Checked = True Then
            If clientType() = "single" Then
                'single
                manBasicWillPrice.Text = singleWillPrice.ToString
                totalPrice = totalPrice + singleWillPrice
            Else

                'mirror
                manBasicWillPrice.Text = mirrorWillPrice.ToString
                totalPrice = totalPrice + mirrorWillPrice
            End If
        End If

        'Storage
        If Me.manStorageFlag.Checked = True Then

            If clientType() = "single" Then
                Me.manStoragePrice.Text = singleStoragePrice
            Else
                Me.manStoragePrice.Text = dualStoragePrice
            End If

        Else
            Me.manStoragePrice.Text = ""
        End If

        If Me.manHWFlag.Checked = True Then
            If clientType() = "single" Then
                'single
                If manHWOPG.Checked = True Then
                    manHWPrice.Text = (singleLPAPrice + OPGFeePrice).ToString
                Else
                    manHWPrice.Text = (singleLPAPrice).ToString

                End If
                ' totalPrice = totalPrice + (singleLPAPrice + OPGFeePrice)
            Else
                'mirror
                If manHWOPG.Checked = True Then
                    manHWPrice.Text = (dualLPAPrice + OPGFeePrice + OPGFeePrice).ToString
                Else
                    manHWPrice.Text = (dualLPAPrice).ToString
                End If
                ' totalPrice = totalPrice + (dualLPAPrice + OPGFeePrice + OPGFeePrice)
            End If
        End If

        If Me.manPFAFlag.Checked = True Then
            If clientType() = "single" Then
                'single
                If manPFAOPG.Checked = True Then
                    manPFAPrice.Text = (singleLPAPrice + OPGFeePrice).ToString
                Else
                    manPFAPrice.Text = (singleLPAPrice).ToString
                    ' totalPrice = totalPrice + (singleLPAPrice + OPGFeePrice)
                End If

            Else
                'mirror
                If manPFAOPG.Checked = True Then
                    manPFAPrice.Text = (dualLPAPrice + OPGFeePrice + OPGFeePrice).ToString
                Else
                    manPFAPrice.Text = (dualLPAPrice).ToString
                End If
                ' totalPrice = totalPrice + (dualLPAPrice + OPGFeePrice + OPGFeePrice)
            End If
        End If

        'count number of LPAs for bulk discount
        Dim numLPAs = 0
        If clientType() = "single" Then
            If Me.manHWFlag.Checked = True Then numLPAs = numLPAs + 1
            If Me.manPFAFlag.Checked = True Then numLPAs = numLPAs + 1

            If numLPAs = 2 Then
                'need to give discount...but only if OPG Fee is included
                If manPFAOPG.Checked = True And manHWOPG.Checked = True Then
                    If Me.manHWFlag.Checked = True Then Me.manHWPrice.Text = ((dualLPAPrice / 2) + OPGFeePrice).ToString("N0")
                    If Me.manPFAFlag.Checked = True Then Me.manPFAPrice.Text = ((dualLPAPrice / 2) + OPGFeePrice).ToString("N0")
                End If

            End If
        Else
            If Me.manHWFlag.Checked = True Then numLPAs = numLPAs + 2
            If Me.manPFAFlag.Checked = True Then numLPAs = numLPAs + 2

            If numLPAs = 2 Then
                'need to give discount...but only if OPG Fee is included
                If manPFAOPG.Checked = True And manHWOPG.Checked = True Then
                    If Me.manHWFlag.Checked = True Then Me.manHWPrice.Text = (dualLPAPrice + OPGFeePrice + OPGFeePrice).ToString("N0")
                    If Me.manPFAFlag.Checked = True Then Me.manPFAPrice.Text = (dualLPAPrice + OPGFeePrice + OPGFeePrice).ToString("N0")
                End If

            End If

            If numLPAs = 4 Then
                'need to give discount...but only if OPG Fee is included
                If manPFAOPG.Checked = True And manHWOPG.Checked = True Then
                    If Me.manHWFlag.Checked = True Then Me.manHWPrice.Text = ((fourLPAPrice / 2) + OPGFeePrice + OPGFeePrice).ToString("N0")
                    If Me.manPFAFlag.Checked = True Then Me.manPFAPrice.Text = ((fourLPAPrice / 2) + OPGFeePrice + OPGFeePrice).ToString("N0")
                End If

            End If
        End If

        totalPrice = totalPrice + Val(Me.manHWPrice.Text.Replace(",", "")) + Val(Me.manPFAPrice.Text.Replace(",", ""))


        'probate is to be removed for time being 21st January 2020
        ' Me.manProbatePlanPrice.Text = "0"
        'Me.recProbatePlanPrice.Text = "0"


        If Me.manFAPTFlag.Checked = True Then manFAPTPrice.Text = FAPTPrice.ToString : totalPrice = totalPrice + FAPTPrice

        If Me.manProbatePlanFlag.Checked = True Then manProbatePlanPrice.Text = Me.recProbatePlanPrice.Text : totalPrice = totalPrice + Val(Me.recProbatePlanPrice.Text.Replace(",", ""))

        If Me.manOtherFlag.Checked = True Then totalPrice = totalPrice + Val(Me.manOtherPrice.Text.Replace(",", ""))

        ' Me.manSubTotalPrice.Text = (totalPrice * VAT).ToString("N0")
        Me.manSubTotalPrice.Text = (totalPrice).ToString("N0")

        'now are there any group/package discounts ... mainly for LPAs


        'options are FAPT, FAPT+2 LPA, FAPT+4 LPA ONLY if OPG Fee is checked
        Dim manPackagePrice As Integer = totalPrice  'default as there is no package

        If Me.manFAPTFlag.Checked = True And numLPAs = 0 Then manPackagePrice = 2995 + Val(Me.manProbatePlanPrice.Text.Replace(",", ""))
        If Me.manFAPTFlag.Checked = True And ((numLPAs = 1 Or numLPAs = 2) And (manPFAOPG.Checked = True Or manHWOPG.Checked)) Then manPackagePrice = 3595 + Val(Me.manProbatePlanPrice.Text.Replace(",", ""))
        If Me.manFAPTFlag.Checked = True And ((numLPAs = 3 Or numLPAs = 4) And manPFAOPG.Checked = True And manHWOPG.Checked) Then manPackagePrice = 3595 + Val(Me.manProbatePlanPrice.Text.Replace(",", ""))

        Me.manPackagePrice.Text = (manPackagePrice).ToString("N0")

        Return True

    End Function

    Private Sub propertyNo_CheckedChanged(sender As Object, e As EventArgs) Handles propertyNo.CheckedChanged
        If Me.propertyNo.Checked = True Then
            'need to hide the question about mortgage free as it's irrelevant
            Me.mortgageFreeLabel.Visible = False
            Me.Panel20.Visible = False
            Me.protectPropertyLabel.Visible = False
            Me.Panel26.Visible = False
        End If
    End Sub

    Private Sub propertyYes_CheckedChanged(sender As Object, e As EventArgs) Handles propertyYes.CheckedChanged
        If Me.propertyYes.Checked = True Then
            'need to hide the question about mortgage free as it's irrelevant
            Me.mortgageFreeLabel.Visible = True
            Me.Panel20.Visible = True
            Me.protectPropertyLabel.Visible = True
            Me.Panel26.Visible = True
        End If
    End Sub

    Private Sub TrackBar1_Scroll(sender As Object, e As EventArgs) Handles TrackBar1.Scroll
        Me.probateFeePercentLabel.Text = (Val(TrackBar1.Value) / 10).ToString + " %"
        recalculate_probate_cost()

    End Sub

    Function recalculate_probate_cost()
        Dim estateValue As Integer
        Dim probateCost As Integer
        Dim yearsToLive As Integer
        Dim futureProbate As Integer

        estateValue = Val(Me.totalEstateValue.Text.Replace(",", ""))
        probateCost = estateValue * ((Val(TrackBar1.Value) / 10) / 100)
        probateCost = probateCost * vatMultiplier
        Me.forecastProbateCost.Text = probateCost.ToString("N0")

        yearsToLive = TrackBar2.Value
        futureProbate = probateCost * Math.Pow(1 + (inflationRate / 100), yearsToLive)
        Me.futureProbateCost.Text = futureProbate.ToString("N0")
        Return True

    End Function

    Private Sub TrackBar2_Scroll(sender As Object, e As EventArgs) Handles TrackBar2.Scroll
        Me.yearsToDeathLabel.Text = TrackBar2.Value
        recalculate_probate_cost()
    End Sub
End Class