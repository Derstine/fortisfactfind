﻿Public Class assets_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        Panel1.Enabled = False
        Panel2.Enabled = False
        Me.assetCategory.SelectedIndex = 0

        If clientType() = "single" Then
            RadioButton1.Text = getClientFirstName("client1")
            RadioButton2.Visible = False
            RadioButton3.Visible = False
            RadioButton1.Checked = True
            RadioButton1.Enabled = False
        Else
            RadioButton1.Text = getClientFirstName("client1")
            RadioButton2.Text = getClientFirstName("client2")
            RadioButton2.Visible = True
            RadioButton3.Visible = True
            RadioButton1.Enabled = True
        End If



        'load data
        Dim tablename As String = factfind_wizard.assetTablename.text

        Dim sql As String = "select * from " + tablename + " where sequence=" + rowID
        Dim results As Array
        results = runSQLwithArray(sql)

        If results(0) <> "ERROR" And results(0) <> "" Then
            If tablename = "fapt_trust_fund_property" Then
                Me.assetCategory.SelectedItem = results(10)
                Me.description.Text = results(10)
                Me.assetValue.Text = results(3)
                If results(9) = "client1" Then RadioButton1.Checked = True
                If results(9) = "client2" Then RadioButton2.Checked = True
                If results(9) = "joint" Then RadioButton3.Checked = True
                If results(9) = "both" Then RadioButton3.Checked = True

                If results(6) = "Y" Then RadioButton6.Checked = True
                If results(6) = "N" Then RadioButton5.Checked = True
                If results(6) = "Z" Then RadioButton4.Checked = True

            ElseIf tablename = "fapt_trust_fund_cash" Then
                Me.assetCategory.SelectedItem = results(5)
                Me.description.Text = results(2)
                Me.assetValue.Text = results(3)
                If results(4) = "client1" Then RadioButton1.Checked = True
                If results(4) = "client2" Then RadioButton2.Checked = True
                If results(4) = "joint" Then RadioButton3.Checked = True
                If results(4) = "both" Then RadioButton3.Checked = True
                Me.currentCharges.Text = results(6)
                RadioButton4.Checked = True

            Else
                Me.assetCategory.SelectedItem = results(5)
                Me.description.Text = results(2)
                Me.assetValue.Text = results(3)
                If results(4) = "client1" Then RadioButton1.Checked = True
                If results(4) = "client2" Then RadioButton2.Checked = True
                If results(4) = "joint" Then RadioButton3.Checked = True
                If results(4) = "both" Then RadioButton3.Checked = True
                RadioButton4.Checked = True
            End If
        Else
                MsgBox("Error retrieving asset")
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim tablename = "fapt_trust_fund_futher"  'this is just the default
        Dim sql = ""
        Dim whichClient = ""

        Dim errorcount = 0
        Dim errortext = ""

        If assetCategory.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset category" + vbCrLf
        If description.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter a brief description" + vbCrLf
        If Val(assetValue.Text) = 0 Or assetValue.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset value" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            If assetCategory.Text <> "Select..." Then

                If assetCategory.Text = "Property" Then tablename = "fapt_trust_fund_property"
                If assetCategory.Text = "Cars/Bikes/Motorhomes" Then tablename = "fapt_trust_fund_chattels"
                If assetCategory.Text = "Valuables" Then tablename = "fapt_trust_fund_chattels"
                If assetCategory.Text = "Contents" Then tablename = "fapt_trust_fund_chattels"
                If assetCategory.Text = "Bank Accounts" Then tablename = "fapt_trust_fund_cash"
                If assetCategory.Text = "ISA" Then tablename = "fapt_trust_fund_cash"
                If assetCategory.Text = "Bond" Then tablename = "fapt_trust_fund_cash"
                If assetCategory.Text = "Other Investments" Then tablename = "fapt_trust_fund_cash"

                If RadioButton1.Checked = True Then whichClient = "client1"
                If RadioButton2.Checked = True Then whichClient = "client2"
                If RadioButton3.Checked = True Then whichClient = "both"

                If tablename = "fapt_trust_fund_property" Then
                    Dim mortgageFlag = ""
                    If RadioButton6.Checked = True Then mortgageFlag = "Y"
                    If RadioButton5.Checked = True Then mortgageFlag = "N"
                    If RadioButton4.Checked = True Then mortgageFlag = "Z"
                    sql = "update " + tablename + " set description='" + SqlSafe(description.Text) + "', worth='" + SqlSafe(assetValue.Text) + "', whichClient='" + whichClient + "',assetCategory='" + SqlSafe(assetCategory.Text) + "',mortgageFlag='" + SqlSafe(mortgageFlag) + "' where sequence=" + rowID
                ElseIf tablename = "fapt_trust_fund_cash" Then
                    sql = "update " + tablename + " set description='" + SqlSafe(description.Text) + "', worth='" + SqlSafe(assetValue.Text) + "', whichClient='" + whichClient + "',assetCategory='" + SqlSafe(assetCategory.Text) + "',currentCharges='" + SqlSafe(Me.currentCharges.Text) + "' where sequence=" + rowID

                Else
                    sql = "update " + tablename + " set description='" + SqlSafe(description.Text) + "', worth='" + SqlSafe(assetValue.Text) + "', whichClient='" + whichClient + "',assetCategory='" + SqlSafe(assetCategory.Text) + "' where sequence=" + rowID
                End If
                runSQL(sql)
                Me.Close()
            End If

        End If

    End Sub

    Private Sub assetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles assetCategory.SelectedIndexChanged
        If Me.assetCategory.SelectedIndex <> 1 Then
            RadioButton4.Checked = True
            Panel1.Enabled = False
        Else
            Panel1.Enabled = True
        End If

        If Me.assetCategory.SelectedIndex = 6 Or Me.assetCategory.SelectedIndex = 7 Or Me.assetCategory.SelectedIndex = 8 Then
            Me.Panel2.Enabled = True
        Else
            Me.Panel2.Enabled = False
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this asset?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")

                Dim tablename As String = factfind_wizard.assetTablename.Text

                Dim sql As String
                sql = "delete from " + tablename + " where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class