﻿Public Class lpa_attorney_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.attorneyTitle.SelectedIndex = 0
        Me.attorneyRelationshipClient1.SelectedIndex = 0
        Me.attorneyRelationshipClient2.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.attorneyRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.attorneyRelationshipClient2.Visible = False
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If attorneyTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the attorney's title" + vbCrLf
        If attorneyNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the attorneys's name" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into lpa_attorneys_detail (willClientSequence,attorneyTitle,attorneyNames,attorneySurname,attorneyRelationshipClient1,attorneyRelationshipClient2,attorneyAddress,attorneyPostcode,attorneyDOB,attorneyEmail,attorneyPhone) values ('" + clientID + "','" + SqlSafe(attorneyTitle.SelectedItem) + "','" + SqlSafe(attorneyNames.Text) + "','" + SqlSafe(attorneySurname.Text) + "','" + SqlSafe(attorneyRelationshipClient1.SelectedItem) + "','" + SqlSafe(attorneyRelationshipClient2.SelectedItem) + "','" + SqlSafe(attorneyAddress.Text) + "','" + SqlSafe(attorneyPostcode.Text) + "','" + SqlSafe(attorneyDOB.Value) + "','" + SqlSafe(attorneyEmail.Text) + "','" + SqlSafe(attorneyPhone.Text) + "')"
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        attorneyPostcode.Text = attorneyPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = attorneyPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.attorneyAddress.Text = f.FoundAddress()
            Me.attorneyPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub


End Class