﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class probate_review
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.Deceased = New System.Windows.Forms.TabPage()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.willDate = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.deathDate = New System.Windows.Forms.DateTimePicker()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.deathAddress = New System.Windows.Forms.TextBox()
        Me.deathPostcode = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Survivors = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn1 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Executors = New System.Windows.Forms.TabPage()
        Me.DataGridView5 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn4 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.advisers = New System.Windows.Forms.TabPage()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn2 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Assets = New System.Windows.Forms.TabPage()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn3 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Liabilities = New System.Windows.Forms.TabPage()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn5 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Utilities = New System.Windows.Forms.TabPage()
        Me.DataGridView6 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn6 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Beneficiaries = New System.Windows.Forms.TabPage()
        Me.DataGridView7 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn7 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GiftsDuringLifetime = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.DataGridView8 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn8 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.EstateValue = New System.Windows.Forms.TabPage()
        Me.assetValue = New System.Windows.Forms.TextBox()
        Me.liabilitiesValue = New System.Windows.Forms.TextBox()
        Me.netAssetValue = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.probateCost = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.probateFee = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.AtttendanceNotes = New System.Windows.Forms.TabPage()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout
        Me.Deceased.SuspendLayout
        Me.Panel1.SuspendLayout
        Me.Survivors.SuspendLayout
        CType(Me.DataGridView1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Executors.SuspendLayout
        CType(Me.DataGridView5,System.ComponentModel.ISupportInitialize).BeginInit
        Me.advisers.SuspendLayout
        CType(Me.DataGridView2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Assets.SuspendLayout
        CType(Me.DataGridView3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Liabilities.SuspendLayout
        CType(Me.DataGridView4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Utilities.SuspendLayout
        CType(Me.DataGridView6,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Beneficiaries.SuspendLayout
        CType(Me.DataGridView7,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GiftsDuringLifetime.SuspendLayout
        Me.Panel2.SuspendLayout
        CType(Me.DataGridView8,System.ComponentModel.ISupportInitialize).BeginInit
        Me.EstateValue.SuspendLayout
        Me.AtttendanceNotes.SuspendLayout
        Me.SuspendLayout
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1130, 614)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(92, 35)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Close"
        Me.Button2.UseVisualStyleBackColor = false
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(228, 25)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Probate Assessment"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.Deceased)
        Me.TabControl1.Controls.Add(Me.Survivors)
        Me.TabControl1.Controls.Add(Me.Executors)
        Me.TabControl1.Controls.Add(Me.advisers)
        Me.TabControl1.Controls.Add(Me.Assets)
        Me.TabControl1.Controls.Add(Me.Liabilities)
        Me.TabControl1.Controls.Add(Me.Utilities)
        Me.TabControl1.Controls.Add(Me.Beneficiaries)
        Me.TabControl1.Controls.Add(Me.GiftsDuringLifetime)
        Me.TabControl1.Controls.Add(Me.EstateValue)
        Me.TabControl1.Controls.Add(Me.AtttendanceNotes)
        Me.TabControl1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 64)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.Padding = New System.Drawing.Point(10, 10)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1209, 530)
        Me.TabControl1.TabIndex = 9
        '
        'Deceased
        '
        Me.Deceased.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.Deceased.Controls.Add(Me.Button11)
        Me.Deceased.Controls.Add(Me.TextBox1)
        Me.Deceased.Controls.Add(Me.Panel1)
        Me.Deceased.Controls.Add(Me.deathDate)
        Me.Deceased.Controls.Add(Me.Label16)
        Me.Deceased.Controls.Add(Me.RadioButton2)
        Me.Deceased.Controls.Add(Me.RadioButton1)
        Me.Deceased.Controls.Add(Me.Label15)
        Me.Deceased.Controls.Add(Me.Button3)
        Me.Deceased.Controls.Add(Me.deathAddress)
        Me.Deceased.Controls.Add(Me.deathPostcode)
        Me.Deceased.Controls.Add(Me.Label14)
        Me.Deceased.Controls.Add(Me.Label13)
        Me.Deceased.Controls.Add(Me.Label10)
        Me.Deceased.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Deceased.Location = New System.Drawing.Point(4, 41)
        Me.Deceased.Name = "Deceased"
        Me.Deceased.Padding = New System.Windows.Forms.Padding(3)
        Me.Deceased.Size = New System.Drawing.Size(1201, 485)
        Me.Deceased.TabIndex = 0
        Me.Deceased.Text = "The Deceased"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(314, 109)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(167, 23)
        Me.Button11.TabIndex = 24
        Me.Button11.Text = "Copy From Client Record"
        Me.Button11.UseVisualStyleBackColor = true
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(140, 63)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(341, 23)
        Me.TextBox1.TabIndex = 23
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.willDate)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.RadioButton4)
        Me.Panel1.Controls.Add(Me.RadioButton3)
        Me.Panel1.Location = New System.Drawing.Point(36, 369)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(508, 44)
        Me.Panel1.TabIndex = 22
        '
        'Label17
        '
        Me.Label17.AutoSize = true
        Me.Label17.Location = New System.Drawing.Point(3, 12)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(95, 18)
        Me.Label17.TabIndex = 17
        Me.Label17.Text = "Is there a Will?"
        '
        'willDate
        '
        Me.willDate.Location = New System.Drawing.Point(319, 10)
        Me.willDate.Name = "willDate"
        Me.willDate.Size = New System.Drawing.Size(162, 23)
        Me.willDate.TabIndex = 21
        '
        'Label18
        '
        Me.Label18.AutoSize = true
        Me.Label18.Location = New System.Drawing.Point(237, 13)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(77, 18)
        Me.Label18.TabIndex = 20
        Me.Label18.Text = "Date of Will"
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = true
        Me.RadioButton4.Location = New System.Drawing.Point(113, 10)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(45, 22)
        Me.RadioButton4.TabIndex = 18
        Me.RadioButton4.TabStop = true
        Me.RadioButton4.Text = "Yes"
        Me.RadioButton4.UseVisualStyleBackColor = true
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = true
        Me.RadioButton3.Location = New System.Drawing.Point(169, 10)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(41, 22)
        Me.RadioButton3.TabIndex = 19
        Me.RadioButton3.TabStop = true
        Me.RadioButton3.Text = "No"
        Me.RadioButton3.UseVisualStyleBackColor = true
        '
        'deathDate
        '
        Me.deathDate.Location = New System.Drawing.Point(140, 314)
        Me.deathDate.Name = "deathDate"
        Me.deathDate.Size = New System.Drawing.Size(168, 23)
        Me.deathDate.TabIndex = 16
        '
        'Label16
        '
        Me.Label16.AutoSize = true
        Me.Label16.Location = New System.Drawing.Point(33, 319)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(89, 18)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Date of Death"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = true
        Me.RadioButton2.Location = New System.Drawing.Point(196, 278)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(41, 22)
        Me.RadioButton2.TabIndex = 14
        Me.RadioButton2.TabStop = true
        Me.RadioButton2.Text = "No"
        Me.RadioButton2.UseVisualStyleBackColor = true
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = true
        Me.RadioButton1.Location = New System.Drawing.Point(140, 278)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(45, 22)
        Me.RadioButton1.TabIndex = 13
        Me.RadioButton1.TabStop = true
        Me.RadioButton1.Text = "Yes"
        Me.RadioButton1.UseVisualStyleBackColor = true
        '
        'Label15
        '
        Me.Label15.AutoSize = true
        Me.Label15.Location = New System.Drawing.Point(30, 280)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(95, 18)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Nursing Home?"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(233, 108)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "Lookup"
        Me.Button3.UseVisualStyleBackColor = true
        '
        'deathAddress
        '
        Me.deathAddress.Location = New System.Drawing.Point(140, 143)
        Me.deathAddress.Multiline = true
        Me.deathAddress.Name = "deathAddress"
        Me.deathAddress.Size = New System.Drawing.Size(268, 122)
        Me.deathAddress.TabIndex = 11
        '
        'deathPostcode
        '
        Me.deathPostcode.Location = New System.Drawing.Point(140, 109)
        Me.deathPostcode.Name = "deathPostcode"
        Me.deathPostcode.Size = New System.Drawing.Size(87, 23)
        Me.deathPostcode.TabIndex = 9
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(10, 112)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(108, 18)
        Me.Label14.TabIndex = 8
        Me.Label14.Text = "Address At Death"
        '
        'Label13
        '
        Me.Label13.AutoSize = true
        Me.Label13.Location = New System.Drawing.Point(42, 66)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(79, 18)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Client Name"
        '
        'Label10
        '
        Me.Label10.AutoSize = true
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(160, 20)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "About The Deceased"
        '
        'Survivors
        '
        Me.Survivors.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.Survivors.Controls.Add(Me.DataGridView1)
        Me.Survivors.Controls.Add(Me.Button1)
        Me.Survivors.Controls.Add(Me.Label2)
        Me.Survivors.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Survivors.Location = New System.Drawing.Point(4, 41)
        Me.Survivors.Name = "Survivors"
        Me.Survivors.Padding = New System.Windows.Forms.Padding(3)
        Me.Survivors.Size = New System.Drawing.Size(1201, 485)
        Me.Survivors.TabIndex = 2
        Me.Survivors.Text = "Surviving Relatives"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = false
        Me.DataGridView1.AllowUserToDeleteRows = false
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Beige
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn3, Me.column1, Me.DataGridViewLinkColumn1})
        Me.DataGridView1.Location = New System.Drawing.Point(16, 98)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = true
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.Size = New System.Drawing.Size(1166, 360)
        Me.DataGridView1.TabIndex = 42
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = true
        Me.DataGridViewTextBoxColumn1.Visible = false
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = true
        Me.DataGridViewTextBoxColumn2.Width = 300
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Address"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = true
        Me.DataGridViewTextBoxColumn4.Width = 400
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Relationship"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = true
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'column1
        '
        Me.column1.HeaderText = "Financially Dependant"
        Me.column1.Name = "column1"
        Me.column1.ReadOnly = true
        Me.column1.Width = 150
        '
        'DataGridViewLinkColumn1
        '
        Me.DataGridViewLinkColumn1.HeaderText = "Action"
        Me.DataGridViewLinkColumn1.Name = "DataGridViewLinkColumn1"
        Me.DataGridViewLinkColumn1.ReadOnly = true
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(1049, 56)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 36)
        Me.Button1.TabIndex = 41
        Me.Button1.Text = "Add Relative"
        Me.Button1.UseVisualStyleBackColor = false
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(141, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Surviving Relatives"
        '
        'Executors
        '
        Me.Executors.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.Executors.Controls.Add(Me.DataGridView5)
        Me.Executors.Controls.Add(Me.Button6)
        Me.Executors.Controls.Add(Me.Label3)
        Me.Executors.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Executors.Location = New System.Drawing.Point(4, 41)
        Me.Executors.Name = "Executors"
        Me.Executors.Padding = New System.Windows.Forms.Padding(3)
        Me.Executors.Size = New System.Drawing.Size(1201, 485)
        Me.Executors.TabIndex = 1
        Me.Executors.Text = "Executors"
        '
        'DataGridView5
        '
        Me.DataGridView5.AllowUserToAddRows = false
        Me.DataGridView5.AllowUserToDeleteRows = false
        Me.DataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn12, Me.Column2, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn13, Me.DataGridViewLinkColumn4})
        Me.DataGridView5.Location = New System.Drawing.Point(16, 98)
        Me.DataGridView5.Name = "DataGridView5"
        Me.DataGridView5.ReadOnly = true
        Me.DataGridView5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView5.Size = New System.Drawing.Size(1163, 274)
        Me.DataGridView5.TabIndex = 40
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = true
        Me.DataGridViewTextBoxColumn10.Visible = false
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = true
        Me.DataGridViewTextBoxColumn12.Width = 270
        '
        'Column2
        '
        Me.Column2.HeaderText = "Address"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = true
        Me.Column2.Width = 400
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Executor Type"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = true
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Relationship To Deceased"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = true
        Me.DataGridViewTextBoxColumn13.Width = 180
        '
        'DataGridViewLinkColumn4
        '
        Me.DataGridViewLinkColumn4.HeaderText = "Action"
        Me.DataGridViewLinkColumn4.Name = "DataGridViewLinkColumn4"
        Me.DataGridViewLinkColumn4.ReadOnly = true
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button6.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(1009, 56)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(170, 36)
        Me.Button6.TabIndex = 39
        Me.Button6.Text = "Add An Executor"
        Me.Button6.UseVisualStyleBackColor = false
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Executors"
        '
        'advisers
        '
        Me.advisers.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.advisers.Controls.Add(Me.DataGridView2)
        Me.advisers.Controls.Add(Me.Button4)
        Me.advisers.Controls.Add(Me.Label4)
        Me.advisers.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.advisers.Location = New System.Drawing.Point(4, 41)
        Me.advisers.Name = "advisers"
        Me.advisers.Size = New System.Drawing.Size(1201, 485)
        Me.advisers.TabIndex = 10
        Me.advisers.Text = "Advisers && Professionals"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = false
        Me.DataGridView2.AllowUserToDeleteRows = false
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn11, Me.DataGridViewLinkColumn2})
        Me.DataGridView2.Location = New System.Drawing.Point(16, 98)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = true
        Me.DataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView2.Size = New System.Drawing.Size(1163, 332)
        Me.DataGridView2.TabIndex = 42
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = true
        Me.DataGridViewTextBoxColumn6.Visible = false
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = true
        Me.DataGridViewTextBoxColumn7.Width = 270
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Address"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = true
        Me.DataGridViewTextBoxColumn8.Width = 400
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Adviser Type"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = true
        Me.DataGridViewTextBoxColumn9.Width = 150
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Relationship To Deceased"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = true
        Me.DataGridViewTextBoxColumn11.Width = 180
        '
        'DataGridViewLinkColumn2
        '
        Me.DataGridViewLinkColumn2.HeaderText = "Action"
        Me.DataGridViewLinkColumn2.Name = "DataGridViewLinkColumn2"
        Me.DataGridViewLinkColumn2.ReadOnly = true
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button4.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(1009, 56)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(170, 36)
        Me.Button4.TabIndex = 41
        Me.Button4.Text = "Add An Adviser"
        Me.Button4.UseVisualStyleBackColor = false
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(183, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Advisers && Professionals"
        '
        'Assets
        '
        Me.Assets.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.Assets.Controls.Add(Me.DataGridView3)
        Me.Assets.Controls.Add(Me.Button5)
        Me.Assets.Controls.Add(Me.Label5)
        Me.Assets.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Assets.Location = New System.Drawing.Point(4, 41)
        Me.Assets.Name = "Assets"
        Me.Assets.Padding = New System.Windows.Forms.Padding(3)
        Me.Assets.Size = New System.Drawing.Size(1201, 485)
        Me.Assets.TabIndex = 3
        Me.Assets.Text = "Assets"
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToAddRows = false
        Me.DataGridView3.AllowUserToDeleteRows = false
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewLinkColumn3})
        Me.DataGridView3.Location = New System.Drawing.Point(16, 98)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.ReadOnly = true
        Me.DataGridView3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView3.Size = New System.Drawing.Size(1114, 272)
        Me.DataGridView3.TabIndex = 44
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = true
        Me.DataGridViewTextBoxColumn14.Visible = false
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Asset Type"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = true
        Me.DataGridViewTextBoxColumn15.Width = 150
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Detail"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = true
        Me.DataGridViewTextBoxColumn16.Width = 700
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Value"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = true
        '
        'DataGridViewLinkColumn3
        '
        Me.DataGridViewLinkColumn3.HeaderText = "Action"
        Me.DataGridViewLinkColumn3.Name = "DataGridViewLinkColumn3"
        Me.DataGridViewLinkColumn3.ReadOnly = true
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button5.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(1005, 56)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(125, 36)
        Me.Button5.TabIndex = 43
        Me.Button5.Text = "Add An Asset"
        Me.Button5.UseVisualStyleBackColor = false
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 20)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Assets"
        '
        'Liabilities
        '
        Me.Liabilities.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.Liabilities.Controls.Add(Me.DataGridView4)
        Me.Liabilities.Controls.Add(Me.Button7)
        Me.Liabilities.Controls.Add(Me.Label6)
        Me.Liabilities.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Liabilities.Location = New System.Drawing.Point(4, 41)
        Me.Liabilities.Name = "Liabilities"
        Me.Liabilities.Padding = New System.Windows.Forms.Padding(3)
        Me.Liabilities.Size = New System.Drawing.Size(1201, 485)
        Me.Liabilities.TabIndex = 5
        Me.Liabilities.Text = "Liabilities"
        '
        'DataGridView4
        '
        Me.DataGridView4.AllowUserToAddRows = false
        Me.DataGridView4.AllowUserToDeleteRows = false
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewLinkColumn5})
        Me.DataGridView4.Location = New System.Drawing.Point(16, 98)
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.ReadOnly = true
        Me.DataGridView4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView4.Size = New System.Drawing.Size(1114, 362)
        Me.DataGridView4.TabIndex = 46
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = true
        Me.DataGridViewTextBoxColumn18.Visible = false
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Liability Type"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = true
        Me.DataGridViewTextBoxColumn19.Width = 150
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Detail"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = true
        Me.DataGridViewTextBoxColumn20.Width = 700
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Value"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = true
        '
        'DataGridViewLinkColumn5
        '
        Me.DataGridViewLinkColumn5.HeaderText = "Action"
        Me.DataGridViewLinkColumn5.Name = "DataGridViewLinkColumn5"
        Me.DataGridViewLinkColumn5.ReadOnly = true
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button7.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(994, 56)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 45
        Me.Button7.Text = "Add A Liability"
        Me.Button7.UseVisualStyleBackColor = false
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 20)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Liabilities"
        '
        'Utilities
        '
        Me.Utilities.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.Utilities.Controls.Add(Me.DataGridView6)
        Me.Utilities.Controls.Add(Me.Button8)
        Me.Utilities.Controls.Add(Me.Label7)
        Me.Utilities.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Utilities.Location = New System.Drawing.Point(4, 41)
        Me.Utilities.Name = "Utilities"
        Me.Utilities.Padding = New System.Windows.Forms.Padding(3)
        Me.Utilities.Size = New System.Drawing.Size(1201, 485)
        Me.Utilities.TabIndex = 4
        Me.Utilities.Text = "Utilities"
        '
        'DataGridView6
        '
        Me.DataGridView6.AllowUserToAddRows = false
        Me.DataGridView6.AllowUserToDeleteRows = false
        Me.DataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView6.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.Column3, Me.DataGridViewLinkColumn6})
        Me.DataGridView6.Location = New System.Drawing.Point(16, 98)
        Me.DataGridView6.Name = "DataGridView6"
        Me.DataGridView6.ReadOnly = true
        Me.DataGridView6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView6.Size = New System.Drawing.Size(1114, 362)
        Me.DataGridView6.TabIndex = 48
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = true
        Me.DataGridViewTextBoxColumn22.Visible = false
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "Utility Type"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = true
        Me.DataGridViewTextBoxColumn23.Width = 150
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "Provider"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = true
        Me.DataGridViewTextBoxColumn24.Width = 500
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "Account #"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = true
        Me.DataGridViewTextBoxColumn25.Width = 150
        '
        'Column3
        '
        Me.Column3.HeaderText = "Meter Readings"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = true
        Me.Column3.Width = 150
        '
        'DataGridViewLinkColumn6
        '
        Me.DataGridViewLinkColumn6.HeaderText = "Action"
        Me.DataGridViewLinkColumn6.Name = "DataGridViewLinkColumn6"
        Me.DataGridViewLinkColumn6.ReadOnly = true
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button8.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(994, 56)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 47
        Me.Button8.Text = "Add A Utilty"
        Me.Button8.UseVisualStyleBackColor = false
        '
        'Label7
        '
        Me.Label7.AutoSize = true
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label7.Location = New System.Drawing.Point(12, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(116, 20)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Utility Providers"
        '
        'Beneficiaries
        '
        Me.Beneficiaries.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.Beneficiaries.Controls.Add(Me.DataGridView7)
        Me.Beneficiaries.Controls.Add(Me.Button9)
        Me.Beneficiaries.Controls.Add(Me.Label8)
        Me.Beneficiaries.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Beneficiaries.Location = New System.Drawing.Point(4, 41)
        Me.Beneficiaries.Name = "Beneficiaries"
        Me.Beneficiaries.Padding = New System.Windows.Forms.Padding(3)
        Me.Beneficiaries.Size = New System.Drawing.Size(1201, 485)
        Me.Beneficiaries.TabIndex = 6
        Me.Beneficiaries.Text = "Beneficiaries"
        '
        'DataGridView7
        '
        Me.DataGridView7.AllowUserToAddRows = false
        Me.DataGridView7.AllowUserToDeleteRows = false
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Beige
        Me.DataGridView7.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView7.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn26, Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn30, Me.DataGridViewLinkColumn7})
        Me.DataGridView7.Location = New System.Drawing.Point(16, 98)
        Me.DataGridView7.Name = "DataGridView7"
        Me.DataGridView7.ReadOnly = true
        Me.DataGridView7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView7.Size = New System.Drawing.Size(1166, 360)
        Me.DataGridView7.TabIndex = 44
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = true
        Me.DataGridViewTextBoxColumn26.Visible = false
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = true
        Me.DataGridViewTextBoxColumn27.Width = 300
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Address"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = true
        Me.DataGridViewTextBoxColumn28.Width = 400
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "Relationship"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = true
        Me.DataGridViewTextBoxColumn29.Width = 150
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "Item"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = true
        Me.DataGridViewTextBoxColumn30.Width = 150
        '
        'DataGridViewLinkColumn7
        '
        Me.DataGridViewLinkColumn7.HeaderText = "Action"
        Me.DataGridViewLinkColumn7.Name = "DataGridViewLinkColumn7"
        Me.DataGridViewLinkColumn7.ReadOnly = true
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button9.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(1022, 56)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(160, 36)
        Me.Button9.TabIndex = 43
        Me.Button9.Text = "Add Beneficiary"
        Me.Button9.UseVisualStyleBackColor = false
        '
        'Label8
        '
        Me.Label8.AutoSize = true
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label8.Location = New System.Drawing.Point(12, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 20)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Beneficiaries"
        '
        'GiftsDuringLifetime
        '
        Me.GiftsDuringLifetime.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.GiftsDuringLifetime.Controls.Add(Me.Panel2)
        Me.GiftsDuringLifetime.Controls.Add(Me.DataGridView8)
        Me.GiftsDuringLifetime.Controls.Add(Me.Button10)
        Me.GiftsDuringLifetime.Controls.Add(Me.Label9)
        Me.GiftsDuringLifetime.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GiftsDuringLifetime.Location = New System.Drawing.Point(4, 41)
        Me.GiftsDuringLifetime.Name = "GiftsDuringLifetime"
        Me.GiftsDuringLifetime.Padding = New System.Windows.Forms.Padding(3)
        Me.GiftsDuringLifetime.Size = New System.Drawing.Size(1201, 485)
        Me.GiftsDuringLifetime.TabIndex = 7
        Me.GiftsDuringLifetime.Text = "Gifts During Lifetime"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.RadioButton5)
        Me.Panel2.Controls.Add(Me.RadioButton6)
        Me.Panel2.Location = New System.Drawing.Point(16, 60)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1166, 35)
        Me.Panel2.TabIndex = 47
        '
        'Label19
        '
        Me.Label19.AutoSize = true
        Me.Label19.Location = New System.Drawing.Point(3, 7)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(725, 18)
        Me.Label19.TabIndex = 17
        Me.Label19.Text = "Are you aware of any transfers of assets e.g. cash, property, belongings worth mo"& _ 
    "re than £3,000 made in the last 7 years?"
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = true
        Me.RadioButton5.Location = New System.Drawing.Point(746, 5)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(45, 22)
        Me.RadioButton5.TabIndex = 18
        Me.RadioButton5.TabStop = true
        Me.RadioButton5.Text = "Yes"
        Me.RadioButton5.UseVisualStyleBackColor = true
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = true
        Me.RadioButton6.Location = New System.Drawing.Point(802, 5)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(41, 22)
        Me.RadioButton6.TabIndex = 19
        Me.RadioButton6.TabStop = true
        Me.RadioButton6.Text = "No"
        Me.RadioButton6.UseVisualStyleBackColor = true
        '
        'DataGridView8
        '
        Me.DataGridView8.AllowUserToAddRows = false
        Me.DataGridView8.AllowUserToDeleteRows = false
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Beige
        Me.DataGridView8.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView8.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34, Me.DataGridViewTextBoxColumn35, Me.DataGridViewLinkColumn8})
        Me.DataGridView8.Location = New System.Drawing.Point(16, 181)
        Me.DataGridView8.Name = "DataGridView8"
        Me.DataGridView8.ReadOnly = true
        Me.DataGridView8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView8.Size = New System.Drawing.Size(1166, 257)
        Me.DataGridView8.TabIndex = 46
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = true
        Me.DataGridViewTextBoxColumn31.Visible = false
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = true
        Me.DataGridViewTextBoxColumn32.Width = 300
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "Address"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = true
        Me.DataGridViewTextBoxColumn33.Width = 400
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "Relationship"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = true
        Me.DataGridViewTextBoxColumn34.Width = 150
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "Item"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.ReadOnly = true
        Me.DataGridViewTextBoxColumn35.Width = 150
        '
        'DataGridViewLinkColumn8
        '
        Me.DataGridViewLinkColumn8.HeaderText = "Action"
        Me.DataGridViewLinkColumn8.Name = "DataGridViewLinkColumn8"
        Me.DataGridViewLinkColumn8.ReadOnly = true
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button10.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button10.ForeColor = System.Drawing.Color.Black
        Me.Button10.Location = New System.Drawing.Point(1022, 139)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(160, 36)
        Me.Button10.TabIndex = 45
        Me.Button10.Text = "Add Gift Given"
        Me.Button10.UseVisualStyleBackColor = false
        '
        'Label9
        '
        Me.Label9.AutoSize = true
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label9.Location = New System.Drawing.Point(12, 20)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(154, 20)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Gifts During Lifetime"
        '
        'EstateValue
        '
        Me.EstateValue.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.EstateValue.Controls.Add(Me.assetValue)
        Me.EstateValue.Controls.Add(Me.liabilitiesValue)
        Me.EstateValue.Controls.Add(Me.netAssetValue)
        Me.EstateValue.Controls.Add(Me.Label26)
        Me.EstateValue.Controls.Add(Me.probateCost)
        Me.EstateValue.Controls.Add(Me.Label25)
        Me.EstateValue.Controls.Add(Me.probateFee)
        Me.EstateValue.Controls.Add(Me.Label24)
        Me.EstateValue.Controls.Add(Me.Label23)
        Me.EstateValue.Controls.Add(Me.Label22)
        Me.EstateValue.Controls.Add(Me.Label21)
        Me.EstateValue.Controls.Add(Me.Label20)
        Me.EstateValue.Controls.Add(Me.Label11)
        Me.EstateValue.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.EstateValue.Location = New System.Drawing.Point(4, 41)
        Me.EstateValue.Name = "EstateValue"
        Me.EstateValue.Padding = New System.Windows.Forms.Padding(3)
        Me.EstateValue.Size = New System.Drawing.Size(1201, 485)
        Me.EstateValue.TabIndex = 8
        Me.EstateValue.Text = "Net Estate Value"
        '
        'assetValue
        '
        Me.assetValue.Location = New System.Drawing.Point(210, 75)
        Me.assetValue.Name = "assetValue"
        Me.assetValue.Size = New System.Drawing.Size(93, 23)
        Me.assetValue.TabIndex = 17
        '
        'liabilitiesValue
        '
        Me.liabilitiesValue.Location = New System.Drawing.Point(210, 126)
        Me.liabilitiesValue.Name = "liabilitiesValue"
        Me.liabilitiesValue.Size = New System.Drawing.Size(93, 23)
        Me.liabilitiesValue.TabIndex = 16
        '
        'netAssetValue
        '
        Me.netAssetValue.Location = New System.Drawing.Point(210, 177)
        Me.netAssetValue.Name = "netAssetValue"
        Me.netAssetValue.Size = New System.Drawing.Size(93, 23)
        Me.netAssetValue.TabIndex = 15
        '
        'Label26
        '
        Me.Label26.AutoSize = true
        Me.Label26.Location = New System.Drawing.Point(309, 282)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(168, 18)
        Me.Label26.TabIndex = 14
        Me.Label26.Text = "(plus VAT && disbursements)"
        '
        'probateCost
        '
        Me.probateCost.BackColor = System.Drawing.Color.LightGreen
        Me.probateCost.Location = New System.Drawing.Point(210, 279)
        Me.probateCost.Name = "probateCost"
        Me.probateCost.Size = New System.Drawing.Size(93, 23)
        Me.probateCost.TabIndex = 13
        '
        'Label25
        '
        Me.Label25.AutoSize = true
        Me.Label25.Location = New System.Drawing.Point(264, 231)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(16, 18)
        Me.Label25.TabIndex = 12
        Me.Label25.Text = "%"
        '
        'probateFee
        '
        Me.probateFee.Location = New System.Drawing.Point(210, 228)
        Me.probateFee.Name = "probateFee"
        Me.probateFee.Size = New System.Drawing.Size(48, 23)
        Me.probateFee.TabIndex = 11
        '
        'Label24
        '
        Me.Label24.AutoSize = true
        Me.Label24.Location = New System.Drawing.Point(105, 282)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(82, 18)
        Me.Label24.TabIndex = 10
        Me.Label24.Text = "Probate Cost"
        '
        'Label23
        '
        Me.Label23.AutoSize = true
        Me.Label23.Location = New System.Drawing.Point(108, 231)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(77, 18)
        Me.Label23.TabIndex = 9
        Me.Label23.Text = "Probate Fee"
        '
        'Label22
        '
        Me.Label22.AutoSize = true
        Me.Label22.Location = New System.Drawing.Point(87, 180)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(97, 18)
        Me.Label22.TabIndex = 8
        Me.Label22.Text = "Net Asset Value"
        '
        'Label21
        '
        Me.Label21.AutoSize = true
        Me.Label21.Location = New System.Drawing.Point(39, 129)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(150, 18)
        Me.Label21.TabIndex = 7
        Me.Label21.Text = "Total Value Of Liabilities"
        '
        'Label20
        '
        Me.Label20.AutoSize = true
        Me.Label20.Location = New System.Drawing.Point(54, 78)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(129, 18)
        Me.Label20.TabIndex = 6
        Me.Label20.Text = "Total Value Of Assets"
        '
        'Label11
        '
        Me.Label11.AutoSize = true
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label11.Location = New System.Drawing.Point(12, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(130, 20)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Net Estate Value"
        '
        'AtttendanceNotes
        '
        Me.AtttendanceNotes.BackColor = System.Drawing.Color.FromArgb(CType(CType(116,Byte),Integer), CType(CType(215,Byte),Integer), CType(CType(229,Byte),Integer))
        Me.AtttendanceNotes.Controls.Add(Me.TextBox20)
        Me.AtttendanceNotes.Controls.Add(Me.Label12)
        Me.AtttendanceNotes.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.AtttendanceNotes.Location = New System.Drawing.Point(4, 41)
        Me.AtttendanceNotes.Name = "AtttendanceNotes"
        Me.AtttendanceNotes.Padding = New System.Windows.Forms.Padding(3)
        Me.AtttendanceNotes.Size = New System.Drawing.Size(1201, 485)
        Me.AtttendanceNotes.TabIndex = 9
        Me.AtttendanceNotes.Text = "Attendance Notes"
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(16, 58)
        Me.TextBox20.Multiline = true
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(1179, 413)
        Me.TextBox20.TabIndex = 46
        '
        'Label12
        '
        Me.Label12.AutoSize = true
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label12.Location = New System.Drawing.Point(12, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(138, 20)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Attendance Notes"
        '
        'probate_review
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221,Byte),Integer), CType(CType(244,Byte),Integer), CType(CType(250,Byte),Integer))
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.MaximizeBox = false
        Me.Name = "probate_review"
        Me.Text = "Probate Assessment"
        Me.TabControl1.ResumeLayout(false)
        Me.Deceased.ResumeLayout(false)
        Me.Deceased.PerformLayout
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.Survivors.ResumeLayout(false)
        Me.Survivors.PerformLayout
        CType(Me.DataGridView1,System.ComponentModel.ISupportInitialize).EndInit
        Me.Executors.ResumeLayout(false)
        Me.Executors.PerformLayout
        CType(Me.DataGridView5,System.ComponentModel.ISupportInitialize).EndInit
        Me.advisers.ResumeLayout(false)
        Me.advisers.PerformLayout
        CType(Me.DataGridView2,System.ComponentModel.ISupportInitialize).EndInit
        Me.Assets.ResumeLayout(false)
        Me.Assets.PerformLayout
        CType(Me.DataGridView3,System.ComponentModel.ISupportInitialize).EndInit
        Me.Liabilities.ResumeLayout(false)
        Me.Liabilities.PerformLayout
        CType(Me.DataGridView4,System.ComponentModel.ISupportInitialize).EndInit
        Me.Utilities.ResumeLayout(false)
        Me.Utilities.PerformLayout
        CType(Me.DataGridView6,System.ComponentModel.ISupportInitialize).EndInit
        Me.Beneficiaries.ResumeLayout(false)
        Me.Beneficiaries.PerformLayout
        CType(Me.DataGridView7,System.ComponentModel.ISupportInitialize).EndInit
        Me.GiftsDuringLifetime.ResumeLayout(false)
        Me.GiftsDuringLifetime.PerformLayout
        Me.Panel2.ResumeLayout(false)
        Me.Panel2.PerformLayout
        CType(Me.DataGridView8,System.ComponentModel.ISupportInitialize).EndInit
        Me.EstateValue.ResumeLayout(false)
        Me.EstateValue.PerformLayout
        Me.AtttendanceNotes.ResumeLayout(false)
        Me.AtttendanceNotes.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents Button2 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents Deceased As TabPage
    Friend WithEvents Executors As TabPage
    Friend WithEvents Survivors As TabPage
    Friend WithEvents Assets As TabPage
    Friend WithEvents Utilities As TabPage
    Friend WithEvents Liabilities As TabPage
    Friend WithEvents Beneficiaries As TabPage
    Friend WithEvents GiftsDuringLifetime As TabPage
    Friend WithEvents EstateValue As TabPage
    Friend WithEvents AtttendanceNotes As TabPage
    Friend WithEvents advisers As TabPage
    Friend WithEvents Label10 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents deathAddress As TextBox
    Friend WithEvents deathPostcode As TextBox
    Friend WithEvents willDate As DateTimePicker
    Friend WithEvents Label18 As Label
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents Label17 As Label
    Friend WithEvents deathDate As DateTimePicker
    Friend WithEvents Label16 As Label
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Label15 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents column1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn1 As DataGridViewLinkColumn
    Friend WithEvents Button1 As Button
    Friend WithEvents DataGridView5 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn4 As DataGridViewLinkColumn
    Friend WithEvents Button6 As Button
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn2 As DataGridViewLinkColumn
    Friend WithEvents Button4 As Button
    Friend WithEvents DataGridView3 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn14 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn3 As DataGridViewLinkColumn
    Friend WithEvents Button5 As Button
    Friend WithEvents DataGridView4 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn18 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn5 As DataGridViewLinkColumn
    Friend WithEvents Button7 As Button
    Friend WithEvents DataGridView6 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn22 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn6 As DataGridViewLinkColumn
    Friend WithEvents Button8 As Button
    Friend WithEvents DataGridView7 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn26 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn7 As DataGridViewLinkColumn
    Friend WithEvents Button9 As Button
    Friend WithEvents TextBox20 As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label19 As Label
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents RadioButton6 As RadioButton
    Friend WithEvents DataGridView8 As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn31 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn8 As DataGridViewLinkColumn
    Friend WithEvents Button10 As Button
    Friend WithEvents assetValue As TextBox
    Friend WithEvents liabilitiesValue As TextBox
    Friend WithEvents netAssetValue As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents probateCost As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents probateFee As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Button11 As Button
End Class
