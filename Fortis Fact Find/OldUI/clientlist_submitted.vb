﻿Public Class clientlist_submitted
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

    End Sub

    Function setDefaults()

        clientID = ""
        Return True
    End Function

    Function populateScreen()

        DataGridView1.Rows.Clear()
        Dim sql = ""
        Dim rowCounter = 0
        Dim clientName = ""
        Dim client1Name = ""
        Dim client2Name = ""
        Dim clientAddress = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select willClientSequence,person1Forenames,person1Surname,person1Address,person1Postcode,person2Forenames,person2Surname,status from will_instruction_stage2_data inner join will_instruction_stage0_data on will_instruction_stage2_data.willClientSequence=will_instruction_stage0_data.sequence where will_instruction_stage0_data.status='archived' or will_instruction_stage0_data.status='synchronise' order by will_instruction_stage2_data.person1Surname, will_instruction_stage2_data.person1Forenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            client1Name = dbreader.GetValue(1).ToString + " " + dbreader.GetValue(2).ToString.ToUpper
            client2Name = dbreader.GetValue(5).ToString + " " + dbreader.GetValue(6).ToString.ToUpper

            clientAddress = Trim(dbreader.GetValue(3).ToString.Replace(Chr(13), ", "))

            If Trim(dbreader.GetValue(5).ToString) <> "" Then
                clientName = client1Name + " & " + client2Name
            Else
                clientName = client1Name
            End If

            DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, clientName, clientAddress, dbreader.GetValue(4).ToString, "Make Active")
            DataGridView1.Rows(rowCounter).DefaultCellStyle.BackColor = Color.White


            rowCounter = rowCounter + 1
        End While

        dbreader.Close()
        dbConn.Close()


        Return True
    End Function
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        clientID = "" 'reset global variable
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        ' MsgBox(e.ColumnIndex)
        'MsgBox(e.RowIndex)
        Dim sql = ""

        If e.ColumnIndex = 4 And DataGridView1.Item(4, e.RowIndex).Value = "Make Active" Then
            Dim result As Integer = MessageBox.Show("Are you sure you wish to reactivate this record for:" + vbCrLf + DataGridView1.Item(1, e.RowIndex).Value.ToString, "CONFIRMATION REQUIRED", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation)

            If result = DialogResult.Cancel Then
                ' MessageBox.Show("Cancel pressed")
            ElseIf result = DialogResult.No Then
                '  MessageBox.Show("No pressed")
            ElseIf result = DialogResult.Yes Then
                ' MessageBox.Show("Yes pressed")
                sql = "update will_instruction_stage0_data set status='offline',statusDate='" + Now() + "' where sequence=" + DataGridView1.Item(0, e.RowIndex).Value.ToString
                runSQL(sql)
            End If

        End If

        clientID = "" 'reset global value
        populateScreen()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        clientID = "" 'reset global value
        clientrecord.ShowDialog(Me)
        clientID = "" 'reset global value
        populateScreen()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Cursor = Cursors.WaitCursor

        Button2.Enabled = False

        DataGridView1.Enabled = False
        Me.Refresh()

        'run spyactivity report
        runActivitySpyReport()

        Dim itemsAddedCounter As Integer = getNewCases()
        Cursor = Cursors.Default
        MsgBox("There have been " + itemsAddedCounter.ToString + " clients added")


        Button2.Enabled = True

        DataGridView1.Enabled = True

        populateScreen()

    End Sub
End Class