﻿Public Class fapt_assets_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)
        Me.Panel1.Enabled = False
        Me.Panel2.Enabled = False
        Me.assetCategory.SelectedIndex = 0

        If clientType() = "single" Then
            RadioButton1.Text = getClientFirstName("client1")
            RadioButton2.Visible = False
            RadioButton3.Visible = False
            RadioButton1.Checked = True
            RadioButton1.Enabled = False
        Else
            RadioButton1.Text = getClientFirstName("client1")
            RadioButton2.Text = getClientFirstName("client2")
            RadioButton2.Visible = True
            RadioButton3.Visible = True
            RadioButton1.Enabled = True
        End If

        If factfind_fapt.assetMode.Text = "property" Then Me.assetCategory.SelectedIndex = 1

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim tablename = "fapt_trust_fund_futher"  'this is just the default

        'default for including asset in trust
        Dim includeInTrust = "Y"
        If Me.RadioButton7.Checked = True Then includeInTrust = "N"

        Dim sql = ""
        Dim whichClient = ""

        Dim errorcount = 0
        Dim errortext = ""

        If assetCategory.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset category" + vbCrLf
        If description.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter a brief description" + vbCrLf
        If Val(assetValue.Text) = 0 Or assetValue.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset value" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            If assetCategory.Text <> "Select..." Then

                If assetCategory.Text = "Property" Then tablename = "fapt_trust_fund_property"
                If assetCategory.Text = "Cars/Bikes/Motorhomes" Then tablename = "fapt_trust_fund_chattels"
                If assetCategory.Text = "Valuables" Then tablename = "fapt_trust_fund_chattels"
                If assetCategory.Text = "Contents" Then tablename = "fapt_trust_fund_chattels"
                If assetCategory.Text = "Bank Accounts" Then tablename = "fapt_trust_fund_cash"
                If assetCategory.Text = "ISA" Then tablename = "fapt_trust_fund_cash"
                If assetCategory.Text = "Bond" Then tablename = "fapt_trust_fund_cash"
                If assetCategory.Text = "Other Investments" Then tablename = "fapt_trust_fund_cash"

                If RadioButton1.Checked = True Then whichClient = "client1"
                If RadioButton2.Checked = True Then whichClient = "client2"
                If RadioButton3.Checked = True Then whichClient = "both"

                If tablename = "fapt_trust_fund_property" Then
                    Dim mortgageFlag = ""
                    If RadioButton6.Checked = True Then mortgageFlag = "Y"
                    If RadioButton5.Checked = True Then mortgageFlag = "N"
                    If RadioButton4.Checked = True Then mortgageFlag = "Z"

                    sql = "insert into " + tablename + "(willClientSequence,address,description,worth,whichClient,assetCategory,mortgageFlag,includeInTrust) values ('" + clientID + "','" + SqlSafe(description.Text) + "','" + SqlSafe(description.Text) + "','" + SqlSafe(assetValue.Text) + "','" + whichClient + "','" + SqlSafe(assetCategory.Text) + "','" + SqlSafe(mortgageFlag) + "','" + SqlSafe(includeInTrust) + "')"
                ElseIf tablename = "fapt_trust_fund_cash" Then
                    Dim currentCharges As String = Me.currentCharges.Text

                    sql = "insert into " + tablename + "(willClientSequence,description,worth,whichClient,assetCategory,currentCharges,includeInTrust) values ('" + clientID + "','" + SqlSafe(description.Text) + "','" + SqlSafe(assetValue.Text) + "','" + whichClient + "','" + SqlSafe(assetCategory.Text) + "','" + SqlSafe(currentCharges) + "','" + SqlSafe(includeInTrust) + "')"

                Else
                    sql = "insert into " + tablename + "(willClientSequence,description,worth,whichClient,assetCategory,includeInTrust) values ('" + clientID + "','" + SqlSafe(description.Text) + "','" + SqlSafe(assetValue.Text) + "','" + whichClient + "','" + SqlSafe(assetCategory.Text) + "','" + SqlSafe(includeInTrust) + "')"
                End If
                runSQL(sql)
                Me.Close()
            End If

        End If

    End Sub

    Private Sub assetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles assetCategory.SelectedIndexChanged
        If Me.assetCategory.SelectedIndex <> 1 Then
            RadioButton4.Checked = True
            Panel1.Enabled = False
        Else
            Panel1.Enabled = True
        End If

        If Me.assetCategory.SelectedIndex = 6 Or Me.assetCategory.SelectedIndex = 7 Or Me.assetCategory.SelectedIndex = 8 Then
            Me.Panel2.Enabled = True
        Else
            Me.Panel2.Enabled = False
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged

    End Sub
End Class