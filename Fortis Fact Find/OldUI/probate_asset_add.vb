﻿Public Class probate_asset_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        Me.assetType.SelectedIndex = 0

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If assetType.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the asset category" + vbCrLf
        If details.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the details of the asset" + vbCrLf
        If worth.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the value of the asset" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into probate_assets (willClientSequence,whichClient,assetType,details,worth) values ('" + clientID + "','" + whoDied + "','" + SqlSafe(assetType.SelectedItem) + "','" + SqlSafe(details.Text) + "','" + SqlSafe(worth.Text) + "')"
            runSQL(sql)
            Me.Close()
        End If

    End Sub


End Class