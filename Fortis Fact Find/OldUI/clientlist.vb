﻿Public Class clientlist
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

    End Sub

    Function setDefaults()

        clientID = ""
        Return True
    End Function

    Function populateScreen()

        DataGridView1.Rows.Clear()
        Dim sql = ""
        Dim rowCounter = 0
        Dim clientName = ""
        Dim client1Name = ""
        Dim client2Name = ""
        Dim clientAddress = ""

        Dim appointmentTime = ""

        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader

        sql = "select willClientSequence,person1Forenames,person1Surname,
                person1Address,person1Postcode,person2Forenames,
                person2Surname,status from will_instruction_stage2_data 
                inner join 
                will_instruction_stage0_data on will_instruction_stage2_data.willClientSequence=will_instruction_stage0_data.sequence 
                where will_instruction_stage0_data.status='offline' or will_instruction_stage0_data.status='synchronise' 
                order by will_instruction_stage2_data.person1Surname, will_instruction_stage2_data.person1Forenames ASC"
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn
        command.CommandText = Sql
        dbreader = command.ExecuteReader
        While dbreader.Read()

            client1Name = dbreader.GetValue(1).ToString + " " + dbreader.GetValue(2).ToString.ToUpper
            client2Name = dbreader.GetValue(5).ToString + " " + dbreader.GetValue(6).ToString.ToUpper

            clientAddress = Trim(dbreader.GetValue(3).ToString.Replace(Chr(13), ", "))

            If Trim(dbreader.GetValue(5).ToString) <> "" Then
                clientName = client1Name + " & " + client2Name
            Else
                clientName = client1Name
            End If

            'is there an appointment
            sql = "select appointmentStart from client_appointment where willClientSequence=" + dbreader.GetValue(0).ToString
            appointmentTime = runSQLwithID(sql)
            If appointmentTime <> "" Then
                appointmentTime = Mid(appointmentTime, 1, 16)
            End If

            If dbreader.GetValue(7).ToString = "offline" Then

                DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, appointmentTime, clientName, clientAddress, dbreader.GetValue(4).ToString, "edit", "delete", "archive", "mark as ready")
                DataGridView1.Rows(rowCounter).DefaultCellStyle.BackColor = Color.White
            Else
                DataGridView1.Rows.Add(dbreader.GetValue(0).ToString, appointmentTime, clientName, clientAddress, dbreader.GetValue(4).ToString, "", "", "", "mark NOT ready")
                DataGridView1.Rows(rowCounter).DefaultCellStyle.BackColor = Color.PaleGreen
            End If

            rowCounter = rowCounter + 1
        End While

        dbreader.Close()
        dbConn.Close()


        Return True
    End Function
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        clientID = "" 'reset global variable
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        ' MsgBox(e.ColumnIndex)
        'MsgBox(e.RowIndex)
        Dim oktorun As Integer
        Dim sql = ""
        If e.ColumnIndex = 5 And DataGridView1.Item(5, e.RowIndex).Value = "edit" Then
            'its edit
            clientID = DataGridView1.Item(0, e.RowIndex).Value.ToString
            ' MsgBox(clientID)
            clientrecord.ShowDialog(Me)
        End If

        If e.ColumnIndex = 6 And DataGridView1.Item(6, e.RowIndex).Value = "delete" Then
            Dim result As Integer = MessageBox.Show("Are you certain you want to delete the client record for:" + vbCrLf + DataGridView1.Item(2, e.RowIndex).Value.ToString, "CONFIRMATION REQUIRED", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation)

            If result = DialogResult.Cancel Then
                ' MessageBox.Show("Cancel pressed")
            ElseIf result = DialogResult.No Then
                '  MessageBox.Show("No pressed")
            ElseIf result = DialogResult.Yes Then
                ' MessageBox.Show("Yes pressed")
                sql = "update will_instruction_stage0_data set status='deleted',statusDate='" + Now() + "' where sequence=" + DataGridView1.Item(0, e.RowIndex).Value.ToString
                runSQL(sql)
            End If

        End If

        If e.ColumnIndex = 7 And DataGridView1.Item(7, e.RowIndex).Value = "archive" Then
            Dim result As Integer = MessageBox.Show("Are you sure you wish to archive this record for:" + vbCrLf + DataGridView1.Item(2, e.RowIndex).Value.ToString, "CONFIRMATION REQUIRED", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation)

            If result = DialogResult.Cancel Then
                ' MessageBox.Show("Cancel pressed")
            ElseIf result = DialogResult.No Then
                '  MessageBox.Show("No pressed")
            ElseIf result = DialogResult.Yes Then
                ' MessageBox.Show("Yes pressed")
                sql = "update will_instruction_stage0_data set status='manualArchive',statusDate='" + Now() + "' where sequence=" + DataGridView1.Item(0, e.RowIndex).Value.ToString
                runSQL(sql)
            End If
        End If

        If e.ColumnIndex = 8 Then
            oktorun = 0  'set default position to be "no"

            clientID = DataGridView1.Item(0, e.RowIndex).Value.ToString

            'check that user has recorded payment amount and type
            If getWillLookup("howIsClientPaying") <> "ERROR" And getWillLookup("howIsClientPaying") <> "" And getWillLookup("paymentAmount") <> "ERROR" And getWillLookup("paymentAmount") <> "" Then

                'should be no issues but we need to check for signatures

                'first, is touchscreen disabled
                sql = "select misc1 from userdata"
                If runSQLwithID(sql) = "N" Then
                    oktorun = 1   'signature has been disabled
                Else
                    'signature enabled, just need to check that there is a signature
                    If getWillLookup("signatureAdviser") <> "ERROR" And Trim(getWillLookup("signatureAdviser")) <> "" Then
                        'we have an adviser set, which means user must have saved the signature page, therefore should be a signature
                        oktorun = 1
                    End If
                End If

                If oktorun = 1 Then
                    'no isses and so we can do this
                    If DataGridView1.Item(8, e.RowIndex).Value.ToString = "mark NOT ready" Then
                        DataGridView1.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
                        DataGridView1.Item(8, e.RowIndex).Value = "mark as ready"
                        sql = "update will_instruction_stage0_data set status='offline',statusDate='" + Now() + "' where sequence=" + DataGridView1.Item(0, e.RowIndex).Value.ToString
                        DataGridView1.Item(5, e.RowIndex).Value = "edit"
                        DataGridView1.Item(6, e.RowIndex).Value = "delete"
                        DataGridView1.Item(7, e.RowIndex).Value = "archive"

                    Else
                        DataGridView1.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleGreen
                        DataGridView1.Item(8, e.RowIndex).Value = "mark NOT ready"
                        sql = "update will_instruction_stage0_data set status='synchronise',statusDate='" + Now() + "' where sequence=" + DataGridView1.Item(0, e.RowIndex).Value.ToString
                        DataGridView1.Item(5, e.RowIndex).Value = ""
                        DataGridView1.Item(6, e.RowIndex).Value = ""
                        DataGridView1.Item(7, e.RowIndex).Value = ""
                    End If
                    runSQL(sql)
                Else
                    'no signatures
                    MsgBox("You cannot synchronise this case yet." + vbCrLf + vbCrLf + "No client signatures have been captured." + vbCrLf + vbCrLf + "You need to go to the 'Digital Signatures' tab on the client record screen", vbCritical)

                End If


            Else
                'no amount or payment type, so cant sync
                MsgBox("You cannot synchronise this case yet." + vbCrLf + vbCrLf + "You need to confirm the amount that the client is paying AND how they are making that payment." + vbCrLf + vbCrLf + "You need to enter this information in the 'Client Recommendations' tab on the Wizard screen", vbCritical)
            End If
            clientID = ""


        End If

        clientID = "" 'reset global value
        populateScreen()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        clientID = "" 'reset global value
        clientrecord.ShowDialog(Me)
        clientID = "" 'reset global value
        populateScreen()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Cursor = Cursors.WaitCursor
        Button1.Text = "Checking..."
        Button1.Enabled = False
        Button2.Enabled = False
        Button3.Enabled = False
        DataGridView1.Enabled = False
        Me.Refresh()

        'run spyactivity report
        runActivitySpyReport()

        '        Dim itemsAddedCounter As Integer = getNewCases()
        '        Cursor = Cursors.Default
        '        MsgBox("There have been " + itemsAddedCounter.ToString + " clients added")
        '
        '        Button1.Text = "Check For New Cases"
        '        Button1.Enabled = True
        '        Button2.Enabled = True
        '        Button3.Enabled = True
        '        DataGridView1.Enabled = True
        '
        '        populateScreen()

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        clientlist_archived.ShowDialog()
        populateScreen()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        clientlist_submitted.ShowDialog()
        populateScreen()
    End Sub
End Class