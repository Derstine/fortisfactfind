﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class guardian_edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.guardianTitle = New System.Windows.Forms.ComboBox()
        Me.guardianNames = New System.Windows.Forms.TextBox()
        Me.guardianSurname = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.guardianAddress = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.guardianPostcode = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.guardianRelationshipClient1 = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.guardianType = New System.Windows.Forms.ComboBox()
        Me.guardianPhone = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.guardianEmail = New System.Windows.Forms.TextBox()
        Me.guardianRelationshipClient2 = New System.Windows.Forms.ComboBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.black
        Me.Button3.Location = New System.Drawing.Point(462, 477)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(99, 36)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Update"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.black
        Me.Button1.Location = New System.Drawing.Point(567, 477)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 36)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(147, 20)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Update A Guardian"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(24, 56)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(34, 16)
        Me.Label50.TabIndex = 45
        Me.Label50.Text = "Title"
        '
        'guardianTitle
        '
        Me.guardianTitle.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianTitle.FormattingEnabled = True
        Me.guardianTitle.Items.AddRange(New Object() {"Select...", "Mr", "Mrs", "Miss", "Ms"})
        Me.guardianTitle.Location = New System.Drawing.Point(27, 76)
        Me.guardianTitle.Name = "guardianTitle"
        Me.guardianTitle.Size = New System.Drawing.Size(97, 24)
        Me.guardianTitle.TabIndex = 0
        '
        'guardianNames
        '
        Me.guardianNames.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianNames.Location = New System.Drawing.Point(139, 77)
        Me.guardianNames.Name = "guardianNames"
        Me.guardianNames.Size = New System.Drawing.Size(289, 22)
        Me.guardianNames.TabIndex = 1
        '
        'guardianSurname
        '
        Me.guardianSurname.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianSurname.Location = New System.Drawing.Point(443, 77)
        Me.guardianSurname.Name = "guardianSurname"
        Me.guardianSurname.Size = New System.Drawing.Size(218, 22)
        Me.guardianSurname.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(136, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 16)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "Forenames"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(440, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 16)
        Me.Label5.TabIndex = 55
        Me.Label5.Text = "Surname"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(232, 122)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Lookup"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'guardianAddress
        '
        Me.guardianAddress.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianAddress.Location = New System.Drawing.Point(139, 151)
        Me.guardianAddress.Multiline = True
        Me.guardianAddress.Name = "guardianAddress"
        Me.guardianAddress.Size = New System.Drawing.Size(268, 102)
        Me.guardianAddress.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(74, 154)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 62
        Me.Label8.Text = "Address"
        '
        'guardianPostcode
        '
        Me.guardianPostcode.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianPostcode.Location = New System.Drawing.Point(139, 123)
        Me.guardianPostcode.Name = "guardianPostcode"
        Me.guardianPostcode.Size = New System.Drawing.Size(87, 22)
        Me.guardianPostcode.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(60, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 16)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Postcode"
        '
        'guardianRelationshipClient1
        '
        Me.guardianRelationshipClient1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianRelationshipClient1.FormattingEnabled = True
        Me.guardianRelationshipClient1.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.guardianRelationshipClient1.Location = New System.Drawing.Point(203, 354)
        Me.guardianRelationshipClient1.Name = "guardianRelationshipClient1"
        Me.guardianRelationshipClient1.Size = New System.Drawing.Size(204, 24)
        Me.guardianRelationshipClient1.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 357)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 16)
        Me.Label2.TabIndex = 67
        Me.Label2.Text = "Relationship to Client 1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(24, 387)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 16)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Relationship to Client 2"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(35, 432)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(98, 16)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Guardian Type"
        '
        'guardianType
        '
        Me.guardianType.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianType.FormattingEnabled = True
        Me.guardianType.Items.AddRange(New Object() {"Select...", "Sole", "Joint", "Substitute"})
        Me.guardianType.Location = New System.Drawing.Point(139, 429)
        Me.guardianType.Name = "guardianType"
        Me.guardianType.Size = New System.Drawing.Size(170, 24)
        Me.guardianType.TabIndex = 10
        '
        'guardianPhone
        '
        Me.guardianPhone.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianPhone.Location = New System.Drawing.Point(139, 268)
        Me.guardianPhone.Name = "guardianPhone"
        Me.guardianPhone.Size = New System.Drawing.Size(218, 22)
        Me.guardianPhone.TabIndex = 6
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(86, 271)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 16)
        Me.Label10.TabIndex = 72
        Me.Label10.Text = "Phone"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(86, 312)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(42, 16)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Email"
        '
        'guardianEmail
        '
        Me.guardianEmail.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianEmail.Location = New System.Drawing.Point(139, 309)
        Me.guardianEmail.Name = "guardianEmail"
        Me.guardianEmail.Size = New System.Drawing.Size(522, 22)
        Me.guardianEmail.TabIndex = 7
        '
        'guardianRelationshipClient2
        '
        Me.guardianRelationshipClient2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.guardianRelationshipClient2.FormattingEnabled = True
        Me.guardianRelationshipClient2.Items.AddRange(New Object() {"Select...", "Aunt", "Brother", "Brother-in-Law", "Daughter", "Daughter-in-Law", "Father", "Father-in-Law", "Friend", "Granddaughter", "Grandfather", "Grandmother", "Grandson", "Great Granddaughter", "Great Grandson", "Husband", "Mother", "Mother-in-Law", "Neice", "Nephew", "Partner", "Professional Consultant", "Sister", "Sister-in-Law", "Son", "Son-in-Law", "Step Brother", "Step Daughter", "Step Father", "Step Mother", "Step Sister", "Step Son", "Uncle", "Wife"})
        Me.guardianRelationshipClient2.Location = New System.Drawing.Point(203, 384)
        Me.guardianRelationshipClient2.Name = "guardianRelationshipClient2"
        Me.guardianRelationshipClient2.Size = New System.Drawing.Size(204, 24)
        Me.guardianRelationshipClient2.TabIndex = 77
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button4.ForeColor = System.Drawing.Color.black
        Me.Button4.Location = New System.Drawing.Point(12, 477)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(99, 36)
        Me.Button4.TabIndex = 78
        Me.Button4.Text = "Delete"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'guardian_edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(678, 525)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.guardianRelationshipClient2)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.guardianEmail)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.guardianPhone)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.guardianType)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.guardianRelationshipClient1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.guardianAddress)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.guardianPostcode)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.guardianSurname)
        Me.Controls.Add(Me.guardianNames)
        Me.Controls.Add(Me.guardianTitle)
        Me.Controls.Add(Me.Label50)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Name = "guardian_edit"
        Me.Text = "Update A Guardian"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button3 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents guardianTitle As ComboBox
    Friend WithEvents guardianNames As TextBox
    Friend WithEvents guardianSurname As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents guardianAddress As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents guardianPostcode As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents guardianRelationshipClient1 As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents guardianType As ComboBox
    Friend WithEvents guardianPhone As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents guardianEmail As TextBox
    Friend WithEvents guardianRelationshipClient2 As ComboBox
    Friend WithEvents Button4 As Button
End Class
