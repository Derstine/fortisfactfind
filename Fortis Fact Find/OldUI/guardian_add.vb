﻿Public Class guardian_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.guardianTitle.SelectedIndex = 0
        Me.guardianRelationshipClient1.SelectedIndex = 0
        Me.guardianRelationshipClient2.SelectedIndex = 0
        Me.guardianType.SelectedIndex = 0
        Me.guardianType.SelectedIndex = 0


        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.guardianRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.guardianRelationshipClient2.Visible = False
        End If


    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If guardianTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the guardian's title" + vbCrLf
        If guardianNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the guardian's name" + vbCrLf
        If guardianType.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the guardian's role" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into will_instruction_stage5_data_detail (willClientSequence,guardianTitle,guardianNames,guardianSurname,relationToClient1,relationToClient2,guardianAddress,guardianPostcode,guardianType,guardianPhone,guardianEmail) values ('" + clientID + "','" + SqlSafe(guardianTitle.SelectedItem) + "','" + SqlSafe(guardianNames.Text) + "','" + SqlSafe(guardianSurname.Text) + "','" + SqlSafe(guardianRelationshipClient1.SelectedItem) + "','" + SqlSafe(guardianRelationshipClient2.SelectedItem) + "','" + SqlSafe(guardianAddress.Text) + "','" + SqlSafe(guardianPostcode.Text) + "','" + SqlSafe(guardianType.SelectedItem) + "','" + SqlSafe(guardianPhone.Text) + "','" + SqlSafe(guardianEmail.Text) + "')"
            runSQL(sql)

            Me.Close()
        End If



    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        guardianPostcode.Text = guardianPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = guardianPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.guardianAddress.Text = f.FoundAddress()
            Me.guardianPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person1Postcode,person1Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        guardianPostcode.Text = results(0)
        guardianAddress.Text = results(1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person2Postcode,person2Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        guardianPostcode.Text = results(0)
        guardianAddress.Text = results(1)
    End Sub

    Private Sub guardianPostcode_TextChanged(sender As Object, e As EventArgs) Handles guardianPostcode.TextChanged

    End Sub

    Private Sub guardianAddress_TextChanged(sender As Object, e As EventArgs) Handles guardianAddress.TextChanged

    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs) Handles Label7.Click

    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click

    End Sub
End Class