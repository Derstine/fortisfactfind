﻿Public Class lpa_attendance_notesQ7
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged

        If Me.RadioButton2.Checked = True Then
            '  Me.TextBox1.Enabled = True
        Else
            '  Me.TextBox1.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'check to see all sections filled in
        Dim errorCount = 0
        Dim errorList = ""

        If Me.RadioButton1.Checked = False And Me.RadioButton2.Checked = False Then
            errorCount = errorCount + 1
            errorList = errorList + "You must answer the question" + vbCrLf
        End If



        If errorCount > 0 Then
            MsgBox("You have the following error(s):" + vbCrLf + vbCrLf + errorList, MsgBoxStyle.Exclamation)
        Else

            'save data
            Dim tick As String

            If Me.RadioButton2.Checked = True Then
                tick = "1"
            Else
                tick = "-1"
            End If



            Dim sql As String = "update lpa_attendance_notes set q7='', q7tick=" + tick + " where willClientSequence=" + clientID
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub will_attendance_notesQ1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If factfind_lpa.attendanceMode.Text = "edit" Then
            Dim sql As String = "select q7,q7tick from lpa_attendance_notes  where willClientSequence=" + clientID
            Dim results As Array = runSQLwithArray(sql)

            If results(1) = "1" Then
                Me.RadioButton2.Checked = True
            Else
                Me.RadioButton1.Checked = True
            End If
        End If

    End Sub
End Class