﻿Public Class executor_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)
        Label12.Visible = False
        executorType.Visible = False
        Me.executorTitle.SelectedIndex = 0
        Me.executorRelationshipClient1.SelectedIndex = 0
        Me.executorRelationshipClient2.SelectedIndex = 0
        Me.trusteeFlag.SelectedIndex = 0
        Me.executorType.SelectedIndex = 0


        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.executorRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.executorRelationshipClient2.Visible = False
        End If

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If executorTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the executor's title" + vbCrLf
        If executorNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the executor's name" + vbCrLf
        If trusteeFlag.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the executor's role" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into will_instruction_stage3_data_detail (willClientSequence,executorTitle,executorNames,executorSurname,executorRelationshipClient1,executorRelationshipClient2,executorAddress,executorPostcode,executorType,trusteeFlag,executorPhone,executorEmail) values ('" + clientID + "','" + SqlSafe(executorTitle.SelectedItem) + "','" + SqlSafe(executorNames.Text) + "','" + SqlSafe(executorSurname.Text) + "','" + SqlSafe(executorRelationshipClient1.SelectedItem) + "','" + SqlSafe(executorRelationshipClient2.SelectedItem) + "','" + SqlSafe(executorAddress.Text) + "','" + SqlSafe(executorPostcode.Text) + "','" + SqlSafe(executorType.SelectedItem) + "','" + SqlSafe(trusteeFlag.SelectedItem) + "','" + SqlSafe(executorPhone.Text) + "','" + SqlSafe(executorEmail.Text) + "')"
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        executorPostcode.Text = executorPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = executorPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.executorAddress.Text = f.FoundAddress()
            Me.executorPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person1Postcode,person1Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        executorPostcode.Text = results(0)
        executorAddress.Text = results(1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)
        Dim sql As String = "select person2Postcode,person2Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        executorPostcode.Text = results(0)
        executorAddress.Text = results(1)
    End Sub



    Private Sub trusteeFlag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles trusteeFlag.SelectedIndexChanged
        If trusteeFlag.SelectedIndex = 1 Or trusteeFlag.SelectedIndex = 2 Then
            Label12.Visible = True
            executorType.Visible = True
        Else
            Label12.Visible = False
            executorType.Visible = False
        End If
    End Sub
End Class