﻿Public Class residuary_beneficiary_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.beneficiaryTitle.SelectedIndex = 0
        Me.recipientType.SelectedIndex = 0

        Me.relationToClient1.SelectedIndex = 0
        Me.relationToClient2.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        atWhatAgeIssue.Visible = False
        atWhatAgeIssueLabel.Visible = False
        giftOverBeneficiaries.Visible = False
        giftOverBeneficiariesLabel.Visible = False

        groupAtWhatAgeIssue.Visible = False
        groupAtWhatAgeIssueLabel.Visible = False
        groupgiftOverBeneficiaries.Visible = False
        groupgiftOverBeneficiariesLabel.Visible = False

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.relationToClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.relationToClient2.Visible = False
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If recipientType.SelectedItem = "Individual" Then
            If beneficiaryTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the recipient's title" + vbCrLf
            If beneficiaryForenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the recipient's name" + vbCrLf
            If beneficiarySurname.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the recipient's surname" + vbCrLf
            If Me.RadioButton5.Checked = False And Me.RadioButton6.Checked = False And Me.RadioButton7.Checked = False And Me.RadioButton8.Checked = False Then errorcount = errorcount + 1 : errortext = errortext + "You must specify Lapse or Issue" + vbCrLf
        End If

        If recipientType.SelectedItem = "Charity" Then
            If charityName.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the charity name" + vbCrLf
        End If

        If recipientType.SelectedItem = "Group" Then
            If beneficiaryName.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the group" + vbCrLf
            If Me.RadioButton1.Checked = False And Me.RadioButton2.Checked = False And Me.RadioButton3.Checked = False And Me.RadioButton4.Checked = False Then errorcount = errorcount + 1 : errortext = errortext + "You must specify Lapse or Issue" + vbCrLf
        End If

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok to save

            'first, initialise variables
            Dim giftsequence As String = factfind_will.trustSequence.Text
            Dim beneficiaryType = ""
            Dim beneficiaryName = ""
            Dim giftSharedPercentage As String = Me.giftSharedPercentage.Text
            Dim charityName = ""
            Dim charityRegNumber = ""
            Dim atWhatAge = "0"
            Dim atWhatAgeIssue = "0"
            Dim relationToClient1 = ""
            Dim relationToClient2 = ""
            Dim beneficiaryTitle = ""
            Dim beneficiaryForenames = ""
            Dim beneficiarySurname = ""
            Dim lapseOrIssue = ""
            Dim beneficiaryAddress = ""
            Dim beneficiaryPostcode = ""
            Dim giftoverName = ""

            'next, depending upon what has been chosen, then populate the fields

            If recipientType.SelectedItem = "Individual" Then
                beneficiaryType = "Named Individual"
                beneficiaryTitle = Me.beneficiaryTitle.SelectedItem
                beneficiaryForenames = Me.beneficiaryForenames.Text
                beneficiarySurname = Me.beneficiarySurname.Text
                beneficiaryAddress = Me.beneficiaryAddress.Text
                beneficiaryPostcode = Me.beneficiaryPostcode.Text
                relationToClient1 = Me.relationToClient1.SelectedItem
                relationToClient2 = Me.relationToClient2.SelectedItem
                atWhatAge = Me.ComboBox4.SelectedItem
                atWhatAgeIssue = Me.atWhatAgeIssue.SelectedItem
                giftoverName = Me.giftOverBeneficiaries.Text

                If RadioButton8.Checked = True Then lapseOrIssue = "lapse"
                If RadioButton7.Checked = True Then lapseOrIssue = "issue"
                If RadioButton6.Checked = True Then lapseOrIssue = "issuegiftover"
                If RadioButton5.Checked = True Then lapseOrIssue = "giftover"

            End If

            If recipientType.SelectedItem = "Charity" Then
                beneficiaryType = "Charity"
                charityName = Me.charityName.Text
                charityRegNumber = Me.charityRegNumber.Text
            End If

            If recipientType.SelectedItem = "Group" Then
                beneficiaryType = "A Group"
                beneficiaryName = Me.beneficiaryName.Text
                atWhatAge = Me.ComboBox1.SelectedItem
                atWhatAgeIssue = Me.groupAtWhatAgeIssue.SelectedItem
                giftoverName = Me.groupGiftOverBeneficiaries.Text

                If RadioButton1.Checked = True Then lapseOrIssue = "lapse"
                If RadioButton2.Checked = True Then lapseOrIssue = "issue"
                If RadioButton3.Checked = True Then lapseOrIssue = "issuegiftover"
                If RadioButton4.Checked = True Then lapseOrIssue = "giftover"

            End If

            'before we run SQL, just a quick bit of data cleansing to avoid runtime errors
            If giftSharedPercentage = "" Then giftSharedPercentage = "0"
            If atWhatAge = "" Then atWhatAge = "0"
            If atWhatAgeIssue = "" Then atWhatAgeIssue = "0"

            Dim residuaryType As String = factfind_will.residuaryType.Text

            'finally, run the SQL insert command
            sql = "insert into will_instruction_stage9_data_factfind_3rd_level (residuaryType, willClientSequence,beneficiaryType,beneficiaryName,giftSharedPercentage,charityName,charityRegNumber,atWhatAge,atWhatAgeIssue,relationToClient1,relationToClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,lapseOrIssue,beneficiaryAddress,beneficiaryPostcode,giftoverName) "
            sql = sql + " values ('" + residuaryType + "', " + clientID + ",'" + SqlSafe(beneficiaryType) + "','" + SqlSafe(beneficiaryName) + "'," + SqlSafe(giftSharedPercentage) + ",'" + SqlSafe(charityName) + "','" + SqlSafe(charityRegNumber) + "'," + SqlSafe(atWhatAge) + "," + SqlSafe(atWhatAgeIssue) + ",'" + SqlSafe(relationToClient1) + "','" + SqlSafe(relationToClient2) + "','" + SqlSafe(beneficiaryTitle) + "','" + SqlSafe(beneficiaryForenames) + "','" + SqlSafe(beneficiarySurname) + "','" + SqlSafe(lapseOrIssue) + "','" + SqlSafe(beneficiaryAddress) + "','" + SqlSafe(beneficiaryPostcode) + "','" + SqlSafe(giftoverName) + "')"
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        beneficiaryPostcode.Text = beneficiaryPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = beneficiaryPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.beneficiaryAddress.Text = f.FoundAddress()
            Me.beneficiaryPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles recipientType.SelectedIndexChanged
        If recipientType.SelectedIndex = 0 Then GroupBox1.Visible = False : GroupBox2.Visible = False : GroupBox3.Visible = False
        If recipientType.SelectedIndex = 1 Then GroupBox1.Visible = True : GroupBox2.Visible = False : GroupBox3.Visible = False : GroupBox1.Location = New Point(32, 104)
        If recipientType.SelectedIndex = 2 Then GroupBox1.Visible = False : GroupBox2.Visible = True : GroupBox3.Visible = False : GroupBox2.Location = New Point(32, 104)
        If recipientType.SelectedIndex = 3 Then GroupBox1.Visible = False : GroupBox2.Visible = False : GroupBox3.Visible = True : GroupBox3.Location = New Point(32, 104)

        giftSharedPercentage.Select()

    End Sub

    Private Sub RadioButton7_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton7.CheckedChanged
        If Me.RadioButton7.Checked = True Then
            atWhatAgeIssue.Visible = True
            atWhatAgeIssueLabel.Visible = True
            giftOverBeneficiaries.Visible = False
            giftOverBeneficiariesLabel.Visible = False

        End If
    End Sub

    Private Sub RadioButton6_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton6.CheckedChanged
        If Me.RadioButton6.Checked = True Then
            atWhatAgeIssue.Visible = True
            atWhatAgeIssueLabel.Visible = True
            giftOverBeneficiaries.Visible = True
            giftOverBeneficiariesLabel.Visible = True
        End If
    End Sub

    Private Sub RadioButton8_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton8.CheckedChanged
        If Me.RadioButton8.Checked = True Then
            atWhatAgeIssue.Visible = False
            atWhatAgeIssueLabel.Visible = False
            giftOverBeneficiaries.Visible = False
            giftOverBeneficiariesLabel.Visible = False

        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton5.CheckedChanged
        If Me.RadioButton5.Checked = True Then
            atWhatAgeIssue.Visible = False
            atWhatAgeIssueLabel.Visible = False
            giftOverBeneficiaries.Visible = True
            giftOverBeneficiariesLabel.Visible = True
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            groupAtWhatAgeIssue.Visible = False
            groupAtWhatAgeIssueLabel.Visible = False
            groupGiftOverBeneficiaries.Visible = False
            groupGiftOverBeneficiariesLabel.Visible = False
        End If

    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            groupAtWhatAgeIssue.Visible = True
            groupAtWhatAgeIssueLabel.Visible = True
            groupGiftOverBeneficiaries.Visible = False
            groupGiftOverBeneficiariesLabel.Visible = False
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            groupAtWhatAgeIssue.Visible = True
            groupAtWhatAgeIssueLabel.Visible = True
            groupGiftOverBeneficiaries.Visible = True
            groupGiftOverBeneficiariesLabel.Visible = True
        End If
    End Sub

    Private Sub RadioButton4_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton4.CheckedChanged
        If RadioButton4.Checked = True Then
            groupAtWhatAgeIssue.Visible = False
            groupAtWhatAgeIssueLabel.Visible = False
            groupGiftOverBeneficiaries.Visible = True
            groupGiftOverBeneficiariesLabel.Visible = True
        End If
    End Sub
End Class