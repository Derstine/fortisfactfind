﻿Public Class lpa_person_tobetold_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.personTitle.SelectedIndex = 0
        Me.personRelationshipClient1.SelectedIndex = 0
        Me.personRelationshipClient2.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.personRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.personRelationshipClient2.Visible = False
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If personTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the person's title" + vbCrLf
        If personNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the person's name" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into lpa_persons_to_be_told (willClientSequence,personTitle,personNames,personSurname,personRelationshipClient1,personRelationshipClient2,personAddress,personPostcode,personEmail,personPhone) values ('" + clientID + "','" + SqlSafe(personTitle.SelectedItem) + "','" + SqlSafe(personNames.Text) + "','" + SqlSafe(personSurname.Text) + "','" + SqlSafe(personRelationshipClient1.SelectedItem) + "','" + SqlSafe(personRelationshipClient2.SelectedItem) + "','" + SqlSafe(personAddress.Text) + "','" + SqlSafe(personPostcode.Text) + "','" + SqlSafe(personEmail.Text) + "','" + SqlSafe(personPhone.Text) + "')"
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        personPostcode.Text = personPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = personPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.personAddress.Text = f.FoundAddress()
            Me.personPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub


End Class