﻿Public Class ppt_beneficiary_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.beneficiaryTitle.SelectedIndex = 0

        Me.relationToClient1.SelectedIndex = 0
        Me.relationToClient2.SelectedIndex = 0

        Me.atWhatAge.SelectedIndex = 0
        Me.atWhatAgeIssue.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        atWhatAgeIssue.Visible = False
        atWhatAgeIssueLabel.Visible = False

        'retrieve data
        Dim sql As String = "select * from will_instruction_stage8_data_individual where sequence=" + rowID
        Dim results As Array
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            Me.beneficiaryTitle.SelectedItem = results(13)
            Me.beneficiaryForenames.Text = results(14)
            Me.beneficiarySurname.Text = results(15)
            Me.beneficiaryAddress.Text = results(16)
            Me.beneficiaryPostcode.Text = results(17)
            Me.relationToClient1.SelectedItem = results(11)
            Me.relationToClient2.SelectedItem = results(12)
            Me.atWhatAge.SelectedItem = results(5)
            Me.atWhatAgeIssue.SelectedItem = results(6)
            If results(7) = "lapse" Then Me.RadioButton8.Checked = True
            If results(7) = "issue" Then Me.RadioButton7.Checked = True
            Me.percentage.Text = results(8)

        Else
            MsgBox("Could Not Retrieve PPT Beneficiary Detail")
        End If


        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.relationToClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.relationToClient2.Visible = False
        End If


    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If beneficiaryTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the beneficiary's title" + vbCrLf
        If beneficiaryForenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the beneficiary's name" + vbCrLf
        If beneficiarySurname.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the beneficiary's surname" + vbCrLf
        If Me.RadioButton7.Checked = False And Me.RadioButton8.Checked = False Then errorcount = errorcount + 1 : errortext = errortext + "You must specify Lapse or Issue" + vbCrLf


        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok to save

            'first, initialise variables
            Dim giftsequence As String = gifts_add.giftSequence.Text
            Dim beneficiaryType = "Named Individual"
            Dim beneficiaryName = ""
            Dim percentage As String = Me.percentage.Text
            Dim charityName = ""
            Dim charityRegNumber = ""
            Dim atWhatAge = "0"
            Dim atWhatAgeIssue = "0"
            Dim relationToClient1 = ""
            Dim relationToClient2 = ""
            Dim beneficiaryTitle = ""
            Dim beneficiaryForenames = ""
            Dim beneficiarySurname = ""
            Dim lapseOrIssue = ""
            Dim beneficiaryAddress = ""
            Dim beneficiaryPostcode = ""
            Dim giftoverName = ""


            beneficiaryTitle = Me.beneficiaryTitle.SelectedItem
            beneficiaryForenames = Me.beneficiaryForenames.Text
            beneficiarySurname = Me.beneficiarySurname.Text
            beneficiaryAddress = Me.beneficiaryAddress.Text
            beneficiaryPostcode = Me.beneficiaryPostcode.Text
            relationToClient1 = Me.relationToClient1.SelectedItem
            relationToClient2 = Me.relationToClient2.SelectedItem
            atWhatAge = Me.atWhatAge.SelectedItem
            atWhatAgeIssue = Me.atWhatAgeIssue.SelectedItem

            If RadioButton8.Checked = True Then lapseOrIssue = "lapse"
            If RadioButton7.Checked = True Then lapseOrIssue = "issue"

            'before we run SQL, just a quick bit of data cleansing to avoid runtime errors
            If percentage = "" Then percentage = "0"
            If atWhatAge = "" Then atWhatAge = "0"
            If atWhatAgeIssue = "" Then atWhatAgeIssue = "0"

            'finally, run the SQL insert command, with trustSequence set to -1 as we've not saved this data yet and so dont know the trust sequence
            sql = "update will_instruction_stage8_data_individual set beneficiaryType='" + SqlSafe(beneficiaryType) + "',beneficiaryName='" + SqlSafe(beneficiaryName) + "',percentage=" + SqlSafe(percentage) + ",atWhatAge=" + SqlSafe(atWhatAge) + ",atWhatAgeIssue=" + SqlSafe(atWhatAgeIssue) + ",relationToClient1='" + SqlSafe(relationToClient1) + "',relationToClient2='" + SqlSafe(relationToClient2) + "',beneficiaryTitle='" + SqlSafe(beneficiaryTitle) + "',beneficiaryForenames='" + SqlSafe(beneficiaryForenames) + "',beneficiarySurname='" + SqlSafe(beneficiarySurname) + "',lapseOrIssue='" + SqlSafe(lapseOrIssue) + "',beneficiaryAddress='" + SqlSafe(beneficiaryAddress) + "',beneficiaryPostcode='" + SqlSafe(beneficiaryPostcode) + "' where sequence=" + rowID
            runSQL(sql)

            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        beneficiaryPostcode.Text = beneficiaryPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = beneficiaryPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.beneficiaryAddress.Text = f.FoundAddress()
            Me.beneficiaryPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub RadioButton7_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton7.CheckedChanged
        If Me.RadioButton7.Checked = True Then
            atWhatAgeIssue.Visible = True
            atWhatAgeIssueLabel.Visible = True


        End If
    End Sub

    Private Sub RadioButton8_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton8.CheckedChanged
        If Me.RadioButton8.Checked = True Then
            atWhatAgeIssue.Visible = False
            atWhatAgeIssueLabel.Visible = False
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this beneficiary?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from will_instruction_stage8_data_individual where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class