﻿Public Class fapt_trustee_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.attorneyTitle.SelectedIndex = 0
        Me.attorneyRelationshipClient1.SelectedIndex = 0
        Me.attorneyRelationshipClient2.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        'get current data
        Dim sql As String = "select * from fapt_trustees_detail where sequence=" + rowID
        Dim results As Array = runSQLwithArray(sql)

        If results(13) = "Mr" Then attorneyTitle.SelectedIndex = 1
        If results(13) = "Mrs" Then attorneyTitle.SelectedIndex = 2
        If results(13) = "Miss" Then attorneyTitle.SelectedIndex = 3
        If results(13) = "Ms" Then attorneyTitle.SelectedIndex = 4

        attorneyRelationshipClient1.SelectedItem = results(5)
        attorneyRelationshipClient2.SelectedItem = results(6)

        attorneyNames.Text = results(3)
        attorneySurname.Text = results(15)
        attorneyPostcode.Text = results(16)
        attorneyAddress.Text = results(7)

        attorneyPhone.Text = results(4)
        attorneyEmail.Text = results(8)


        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.attorneyRelationshipClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.attorneyRelationshipClient2.Visible = False
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If attorneyTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the attorney's title" + vbCrLf
        If attorneyNames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the attorney's name" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update fapt_trustees_detail set attorneyTitle='" + SqlSafe(attorneyTitle.SelectedItem) + "',attorneyNames='" + SqlSafe(attorneyNames.Text) + "',attorneySurname='" + SqlSafe(attorneySurname.Text) + "',attorneyRelationshipClient1='" + SqlSafe(attorneyRelationshipClient1.SelectedItem) + "',attorneyRelationshipClient2='" + SqlSafe(attorneyRelationshipClient2.SelectedItem) + "',attorneyAddress='" + SqlSafe(attorneyAddress.Text) + "',attorneyPostcode='" + SqlSafe(attorneyPostcode.Text) + "',attorneyPhone='" + SqlSafe(attorneyPhone.Text) + "',attorneyEmail='" + SqlSafe(attorneyEmail.Text) + "'  where sequence=" + rowID
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        attorneyPostcode.Text = attorneyPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = attorneyPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.attorneyAddress.Text = f.FoundAddress()
            Me.attorneyPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub


    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this trustee?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from fapt_trustees_detail where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class