﻿Public Class nrb_beneficiary_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.beneficiaryTitle.SelectedIndex = 0
        Me.recipientType.SelectedIndex = 0

        Me.relationToClient1.SelectedIndex = 0
        Me.relationToClient2.SelectedIndex = 0

        Me.atWhatAge.SelectedIndex = 0
        Me.atWhatAgeIssue.SelectedIndex = 0
        Me.primaryBeneficiaryFlag.SelectedIndex = 0

        Me.groupAtWhatAge.SelectedIndex = 0
        Me.groupAtWhatAgeIssue.SelectedIndex = 0
        Me.groupPrimaryBeneficiaryFlag.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")

        atWhatAgeIssue.Visible = False
        atWhatAgeIssueLabel.Visible = False

        groupAtWhatAgeIssue.Visible = False
        groupAtWhatAgeIssueLabel.Visible = False

        'load in existing data
        Dim sql As String = "select * from will_instruction_stage8_data_nilrate where sequence=" + rowID
        Dim results As Array

        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" Then
            'we have data :-)
            If results(3) = "Named Individual" Then
                recipientType.SelectedItem = "Individual"
                Me.beneficiaryTitle.SelectedItem = results(14)
                Me.beneficiaryForenames.Text = results(15)
                Me.beneficiarySurname.Text = results(16)
                Me.beneficiaryAddress.Text = results(17)
                Me.beneficiaryPostcode.Text = results(18)
                Me.relationToClient1.SelectedItem = results(12)
                Me.relationToClient2.SelectedItem = results(13)
                Me.atWhatAge.SelectedItem = results(6)
                Me.atWhatAgeIssue.SelectedItem = results(7)

                If results(5) = "lapse" Then RadioButton8.Checked = True
                If results(5) = "issue" Then RadioButton7.Checked = True
                Me.percentage.Text = results(8)
                Me.primaryBeneficiaryFlag.SelectedItem = results(9)

            ElseIf results(3) = "A Group" Then
                recipientType.SelectedItem = "Group"
                Me.beneficiaryName.Text = results(4)
                Me.atWhatAge.SelectedItem = results(6)
                Me.groupAtWhatAgeIssue.SelectedItem = results(7)
                Me.groupPrimaryBeneficiaryFlag.SelectedItem = results(9)

                If results(5) = "lapse" Then RadioButton1.Checked = True
                If results(5) = "issue" Then RadioButton2.Checked = True

                Me.percentage.Text = results(8)
            Else
                MsgBox("Something has gone wrong - cannot retrieve NRB Beneficiary")
            End If
        Else
            MsgBox("Something has gone wrong - cannot retrieve NRB Beneficiary details")
        End If


        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.relationToClient2.Visible = True
        Else
            Me.Label3.Visible = False
            Me.relationToClient2.Visible = False
        End If



    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If recipientType.SelectedItem = "Individual" Then
            If beneficiaryTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the beneficiary's title" + vbCrLf
            If beneficiaryForenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the beneficiary's name" + vbCrLf
            If beneficiarySurname.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the beneficiary's surname" + vbCrLf
            If Me.RadioButton7.Checked = False And Me.RadioButton8.Checked = False Then errorcount = errorcount + 1 : errortext = errortext + "You must specify Lapse or Issue" + vbCrLf
            If Me.primaryBeneficiaryFlag.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify whether this is a Primary Beneficiary or not" + vbCrLf
        End If


        If recipientType.SelectedItem = "Group" Then
            If Me.beneficiaryName.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the group" + vbCrLf
            If Me.RadioButton1.Checked = False And Me.RadioButton2.Checked = False Then errorcount = errorcount + 1 : errortext = errortext + "You must specify Lapse or Issue" + vbCrLf
            If Me.groupPrimaryBeneficiaryFlag.SelectedIndex = 0 Then errorcount = errorcount + 1 : errortext = errortext + "You must specify whether this is a Primary Beneficiary or not" + vbCrLf
        End If

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok to save

            'first, initialise variables

            Dim beneficiaryType = ""
            Dim beneficiaryName = ""
            Dim percentage As String = Me.percentage.Text
            Dim atWhatAge = "0"
            Dim atWhatAgeIssue = "0"
            Dim relationToClient1 = ""
            Dim relationToClient2 = ""
            Dim beneficiaryTitle = ""
            Dim beneficiaryForenames = ""
            Dim beneficiarySurname = ""
            Dim lapseOrIssue = ""
            Dim beneficiaryAddress = ""
            Dim beneficiaryPostcode = ""
            Dim primaryBeneficiaryFlag = ""

            'next, depending upon what has been chosen, then populate the fields

            If recipientType.SelectedItem = "Individual" Then
                beneficiaryType = "Named Individual"
                beneficiaryTitle = Me.beneficiaryTitle.SelectedItem
                beneficiaryForenames = Me.beneficiaryForenames.Text
                beneficiarySurname = Me.beneficiarySurname.Text
                beneficiaryAddress = Me.beneficiaryAddress.Text
                beneficiaryPostcode = Me.beneficiaryPostcode.Text
                relationToClient1 = Me.relationToClient1.SelectedItem
                relationToClient2 = Me.relationToClient2.SelectedItem
                atWhatAge = Me.atWhatAge.SelectedItem
                atWhatAgeIssue = Me.atWhatAgeIssue.SelectedItem

                If RadioButton8.Checked = True Then lapseOrIssue = "lapse"
                If RadioButton7.Checked = True Then lapseOrIssue = "issue"
                primaryBeneficiaryFlag = Me.primaryBeneficiaryFlag.selecteditem
            End If

            If recipientType.SelectedItem = "Group" Then
                beneficiaryType = "A Group"
                beneficiaryName = Me.beneficiaryName.Text
                atWhatAge = Me.groupAtWhatAge.SelectedItem
                atWhatAgeIssue = Me.groupAtWhatAgeIssue.SelectedItem
                primaryBeneficiaryFlag = Me.groupPrimaryBeneficiaryFlag.SelectedItem

                If RadioButton1.Checked = True Then lapseOrIssue = "lapse"
                If RadioButton2.Checked = True Then lapseOrIssue = "issue"
            End If

            'before we run SQL, just a quick bit of data cleansing to avoid runtime errors
            If Me.percentage.Text = "" Then Me.percentage.Text = "0"
            If atWhatAge = "" Then atWhatAge = "0"
            If atWhatAgeIssue = "" Then atWhatAgeIssue = "0"

            'finally, run the SQL insert command
            sql = "update will_instruction_stage8_data_nilrate set beneficiaryType='" + SqlSafe(beneficiaryType) + "',beneficiaryName='" + SqlSafe(beneficiaryName) + "',giftSharedPercentage=" + SqlSafe(Me.percentage.Text) + ",atWhatAge=" + SqlSafe(atWhatAge) + ",atWhatAgeIssue=" + SqlSafe(atWhatAgeIssue) + ",relationToClient1='" + SqlSafe(relationToClient1) + "',relationToClient2='" + SqlSafe(relationToClient2) + "',beneficiaryTitle='" + SqlSafe(beneficiaryTitle) + "',beneficiaryForenames='" + SqlSafe(beneficiaryForenames) + "',beneficiarySurname='" + SqlSafe(beneficiarySurname) + "',lapseOrIssue='" + SqlSafe(lapseOrIssue) + "',beneficiaryAddress='" + SqlSafe(beneficiaryAddress) + "',beneficiaryPostcode='" + SqlSafe(beneficiaryPostcode) + "',primaryBeneficiaryFlag ='" + SqlSafe(primaryBeneficiaryFlag) + "' where sequence=" + rowID
            runSQL(sql)

            Me.Close()
        End If



    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        beneficiaryPostcode.Text = beneficiaryPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = beneficiaryPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.beneficiaryAddress.Text = f.FoundAddress()
            Me.beneficiaryPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles recipientType.SelectedIndexChanged
        If recipientType.SelectedIndex = 0 Then GroupBox1.Visible = False : GroupBox3.Visible = False
        If recipientType.SelectedIndex = 1 Then GroupBox1.Visible = True : GroupBox3.Visible = False : GroupBox1.Location = New Point(32, 104)
        If recipientType.SelectedIndex = 2 Then GroupBox1.Visible = False : GroupBox3.Visible = False
        If recipientType.SelectedIndex = 3 Then GroupBox1.Visible = False : GroupBox3.Visible = True : GroupBox3.Location = New Point(32, 104)

        percentage.Select()

    End Sub

    Private Sub RadioButton7_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton7.CheckedChanged
        If Me.RadioButton7.Checked = True Then
            atWhatAgeIssue.Visible = True
            atWhatAgeIssueLabel.Visible = True
        End If
    End Sub

    Private Sub RadioButton8_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton8.CheckedChanged
        If Me.RadioButton8.Checked = True Then
            atWhatAgeIssue.Visible = False
            atWhatAgeIssueLabel.Visible = False
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            groupAtWhatAgeIssue.Visible = False
            groupAtWhatAgeIssueLabel.Visible = False
        End If

    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            groupAtWhatAgeIssue.Visible = True
            groupAtWhatAgeIssueLabel.Visible = True
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this beneficiary?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from will_instruction_stage8_data_nilrate where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class