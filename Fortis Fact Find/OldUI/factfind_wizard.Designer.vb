﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class factfind_wizard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.lpaHWNo = New System.Windows.Forms.RadioButton()
        Me.lpaHWYes = New System.Windows.Forms.RadioButton()
        Me.lpaHWLabel = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.probatePlanNo = New System.Windows.Forms.RadioButton()
        Me.probatePlanYes = New System.Windows.Forms.RadioButton()
        Me.probatePlanLabel = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.funeralPlanNo = New System.Windows.Forms.RadioButton()
        Me.funeralPlanYes = New System.Windows.Forms.RadioButton()
        Me.funeralPlanLabel = New System.Windows.Forms.Label()
        Me.anyoneDisabledLabel = New System.Windows.Forms.Label()
        Me.Panel29 = New System.Windows.Forms.Panel()
        Me.anyoneDisabledNo = New System.Windows.Forms.RadioButton()
        Me.anyoneDisabledYes = New System.Windows.Forms.RadioButton()
        Me.Panel28 = New System.Windows.Forms.Panel()
        Me.lpaPFANo = New System.Windows.Forms.RadioButton()
        Me.lpaPFAYes = New System.Windows.Forms.RadioButton()
        Me.lpaPFALabel = New System.Windows.Forms.Label()
        Me.Panel27 = New System.Windows.Forms.Panel()
        Me.protectAssetsNo = New System.Windows.Forms.RadioButton()
        Me.protectAssetsYes = New System.Windows.Forms.RadioButton()
        Me.protectAssetsLabel = New System.Windows.Forms.Label()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.protectPropertyNo = New System.Windows.Forms.RadioButton()
        Me.protectPropertyYes = New System.Windows.Forms.RadioButton()
        Me.protectPropertyLabel = New System.Windows.Forms.Label()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.propertyNo = New System.Windows.Forms.RadioButton()
        Me.propertyYes = New System.Windows.Forms.RadioButton()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.mortgageFreeNo = New System.Windows.Forms.RadioButton()
        Me.mortgageFreeYes = New System.Windows.Forms.RadioButton()
        Me.propertyLabel = New System.Windows.Forms.Label()
        Me.mortgageFreeLabel = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.client2Feelings = New System.Windows.Forms.ComboBox()
        Me.client1Feelings = New System.Windows.Forms.ComboBox()
        Me.assetTablename = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.TextBox74 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewLinkColumn2 = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.client_storageButtonNo = New System.Windows.Forms.RadioButton()
        Me.client_storageButtonYes = New System.Windows.Forms.RadioButton()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.storagePaymentMethod = New System.Windows.Forms.TextBox()
        Me.Label133 = New System.Windows.Forms.Label()
        Me.storageAccountNumber = New System.Windows.Forms.TextBox()
        Me.storageSortcode = New System.Windows.Forms.TextBox()
        Me.storageAmount = New System.Windows.Forms.TextBox()
        Me.storageProduct = New System.Windows.Forms.TextBox()
        Me.Label132 = New System.Windows.Forms.Label()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.Label129 = New System.Windows.Forms.Label()
        Me.Label127 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.paymentAmount = New System.Windows.Forms.TextBox()
        Me.howIsClientPaying = New System.Windows.Forms.TextBox()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.Label120 = New System.Windows.Forms.Label()
        Me.manualOverrideFlag = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.recStorageFlag = New System.Windows.Forms.CheckBox()
        Me.recStoragePrice = New System.Windows.Forms.TextBox()
        Me.Label135 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.recPackagePrice = New System.Windows.Forms.TextBox()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.recProbatePlanFlag = New System.Windows.Forms.CheckBox()
        Me.recFAPTFlag = New System.Windows.Forms.CheckBox()
        Me.recPFAFlag = New System.Windows.Forms.CheckBox()
        Me.recHWFlag = New System.Windows.Forms.CheckBox()
        Me.recBasicWillFlag = New System.Windows.Forms.CheckBox()
        Me.recWillPPTFlag = New System.Windows.Forms.CheckBox()
        Me.recWillReviewFlag = New System.Windows.Forms.CheckBox()
        Me.recProbatePlanPrice = New System.Windows.Forms.TextBox()
        Me.recFAPTPrice = New System.Windows.Forms.TextBox()
        Me.recPFAPrice = New System.Windows.Forms.TextBox()
        Me.recHWPrice = New System.Windows.Forms.TextBox()
        Me.recBasicWillPrice = New System.Windows.Forms.TextBox()
        Me.recWillPPTPrice = New System.Windows.Forms.TextBox()
        Me.recWillReviewPrice = New System.Windows.Forms.TextBox()
        Me.recSubTotalPrice = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.manStorageFlag = New System.Windows.Forms.CheckBox()
        Me.manStoragePrice = New System.Windows.Forms.TextBox()
        Me.Label136 = New System.Windows.Forms.Label()
        Me.manPFAOPG = New System.Windows.Forms.CheckBox()
        Me.manHWOPG = New System.Windows.Forms.CheckBox()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.manOtherDescription = New System.Windows.Forms.TextBox()
        Me.manOtherPrice = New System.Windows.Forms.TextBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.manPackagePrice = New System.Windows.Forms.TextBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.manOtherFlag = New System.Windows.Forms.CheckBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.manSubTotalPrice = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.manProbatePlanFlag = New System.Windows.Forms.CheckBox()
        Me.manFAPTFlag = New System.Windows.Forms.CheckBox()
        Me.manPFAFlag = New System.Windows.Forms.CheckBox()
        Me.manHWFlag = New System.Windows.Forms.CheckBox()
        Me.manBasicWillFlag = New System.Windows.Forms.CheckBox()
        Me.manWillPPTFlag = New System.Windows.Forms.CheckBox()
        Me.manWillReviewFlag = New System.Windows.Forms.CheckBox()
        Me.manProbatePlanPrice = New System.Windows.Forms.TextBox()
        Me.manFAPTPrice = New System.Windows.Forms.TextBox()
        Me.manPFAPrice = New System.Windows.Forms.TextBox()
        Me.manHWPrice = New System.Windows.Forms.TextBox()
        Me.manBasicWillPrice = New System.Windows.Forms.TextBox()
        Me.manWillPPTPrice = New System.Windows.Forms.TextBox()
        Me.manWillReviewPrice = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TrackBar1 = New System.Windows.Forms.TrackBar()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.probateInflation = New System.Windows.Forms.TextBox()
        Me.totalEstateValue = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.forecastProbateCost = New System.Windows.Forms.TextBox()
        Me.futureProbateCost = New System.Windows.Forms.TextBox()
        Me.TrackBar2 = New System.Windows.Forms.TrackBar()
        Me.probateFeePercentLabel = New System.Windows.Forms.Label()
        Me.yearsToDeathLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel29.SuspendLayout()
        Me.Panel28.SuspendLayout()
        Me.Panel27.SuspendLayout()
        Me.Panel26.SuspendLayout()
        Me.Panel25.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage9.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1120, 602)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(102, 47)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "Close"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 66)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.Padding = New System.Drawing.Point(10, 10)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1209, 530)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.Panel6)
        Me.TabPage1.Controls.Add(Me.lpaHWLabel)
        Me.TabPage1.Controls.Add(Me.Panel3)
        Me.TabPage1.Controls.Add(Me.probatePlanLabel)
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.funeralPlanLabel)
        Me.TabPage1.Controls.Add(Me.anyoneDisabledLabel)
        Me.TabPage1.Controls.Add(Me.Panel29)
        Me.TabPage1.Controls.Add(Me.Panel28)
        Me.TabPage1.Controls.Add(Me.lpaPFALabel)
        Me.TabPage1.Controls.Add(Me.Panel27)
        Me.TabPage1.Controls.Add(Me.protectAssetsLabel)
        Me.TabPage1.Controls.Add(Me.Panel26)
        Me.TabPage1.Controls.Add(Me.protectPropertyLabel)
        Me.TabPage1.Controls.Add(Me.Panel25)
        Me.TabPage1.Controls.Add(Me.Panel20)
        Me.TabPage1.Controls.Add(Me.propertyLabel)
        Me.TabPage1.Controls.Add(Me.mortgageFreeLabel)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Location = New System.Drawing.Point(4, 41)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Establishing Facts"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.lpaHWNo)
        Me.Panel6.Controls.Add(Me.lpaHWYes)
        Me.Panel6.Location = New System.Drawing.Point(546, 272)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(122, 26)
        Me.Panel6.TabIndex = 32
        '
        'lpaHWNo
        '
        Me.lpaHWNo.AutoSize = True
        Me.lpaHWNo.Location = New System.Drawing.Point(59, 3)
        Me.lpaHWNo.Name = "lpaHWNo"
        Me.lpaHWNo.Size = New System.Drawing.Size(41, 22)
        Me.lpaHWNo.TabIndex = 1
        Me.lpaHWNo.TabStop = True
        Me.lpaHWNo.Text = "No"
        Me.lpaHWNo.UseVisualStyleBackColor = True
        '
        'lpaHWYes
        '
        Me.lpaHWYes.AutoSize = True
        Me.lpaHWYes.Location = New System.Drawing.Point(3, 3)
        Me.lpaHWYes.Name = "lpaHWYes"
        Me.lpaHWYes.Size = New System.Drawing.Size(45, 22)
        Me.lpaHWYes.TabIndex = 0
        Me.lpaHWYes.TabStop = True
        Me.lpaHWYes.Text = "Yes"
        Me.lpaHWYes.UseVisualStyleBackColor = True
        '
        'lpaHWLabel
        '
        Me.lpaHWLabel.AutoSize = True
        Me.lpaHWLabel.Location = New System.Drawing.Point(38, 276)
        Me.lpaHWLabel.Name = "lpaHWLabel"
        Me.lpaHWLabel.Size = New System.Drawing.Size(466, 18)
        Me.lpaHWLabel.TabIndex = 33
        Me.lpaHWLabel.Text = "Have you appointed anyone to look after your health affairs are you get older?"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.probatePlanNo)
        Me.Panel3.Controls.Add(Me.probatePlanYes)
        Me.Panel3.Location = New System.Drawing.Point(546, 356)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(122, 26)
        Me.Panel3.TabIndex = 30
        '
        'probatePlanNo
        '
        Me.probatePlanNo.AutoSize = True
        Me.probatePlanNo.Location = New System.Drawing.Point(59, 3)
        Me.probatePlanNo.Name = "probatePlanNo"
        Me.probatePlanNo.Size = New System.Drawing.Size(41, 22)
        Me.probatePlanNo.TabIndex = 1
        Me.probatePlanNo.TabStop = True
        Me.probatePlanNo.Text = "No"
        Me.probatePlanNo.UseVisualStyleBackColor = True
        '
        'probatePlanYes
        '
        Me.probatePlanYes.AutoSize = True
        Me.probatePlanYes.Location = New System.Drawing.Point(3, 3)
        Me.probatePlanYes.Name = "probatePlanYes"
        Me.probatePlanYes.Size = New System.Drawing.Size(45, 22)
        Me.probatePlanYes.TabIndex = 0
        Me.probatePlanYes.TabStop = True
        Me.probatePlanYes.Text = "Yes"
        Me.probatePlanYes.UseVisualStyleBackColor = True
        '
        'probatePlanLabel
        '
        Me.probatePlanLabel.AutoSize = True
        Me.probatePlanLabel.Location = New System.Drawing.Point(285, 360)
        Me.probatePlanLabel.Name = "probatePlanLabel"
        Me.probatePlanLabel.Size = New System.Drawing.Size(219, 18)
        Me.probatePlanLabel.TabIndex = 31
        Me.probatePlanLabel.Text = "Have you pre-paid for your probate?"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.funeralPlanNo)
        Me.Panel2.Controls.Add(Me.funeralPlanYes)
        Me.Panel2.Location = New System.Drawing.Point(546, 398)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(122, 26)
        Me.Panel2.TabIndex = 28
        '
        'funeralPlanNo
        '
        Me.funeralPlanNo.AutoSize = True
        Me.funeralPlanNo.Location = New System.Drawing.Point(59, 3)
        Me.funeralPlanNo.Name = "funeralPlanNo"
        Me.funeralPlanNo.Size = New System.Drawing.Size(41, 22)
        Me.funeralPlanNo.TabIndex = 1
        Me.funeralPlanNo.TabStop = True
        Me.funeralPlanNo.Text = "No"
        Me.funeralPlanNo.UseVisualStyleBackColor = True
        '
        'funeralPlanYes
        '
        Me.funeralPlanYes.AutoSize = True
        Me.funeralPlanYes.Location = New System.Drawing.Point(3, 3)
        Me.funeralPlanYes.Name = "funeralPlanYes"
        Me.funeralPlanYes.Size = New System.Drawing.Size(45, 22)
        Me.funeralPlanYes.TabIndex = 0
        Me.funeralPlanYes.TabStop = True
        Me.funeralPlanYes.Text = "Yes"
        Me.funeralPlanYes.UseVisualStyleBackColor = True
        '
        'funeralPlanLabel
        '
        Me.funeralPlanLabel.AutoSize = True
        Me.funeralPlanLabel.Location = New System.Drawing.Point(224, 402)
        Me.funeralPlanLabel.Name = "funeralPlanLabel"
        Me.funeralPlanLabel.Size = New System.Drawing.Size(280, 18)
        Me.funeralPlanLabel.TabIndex = 29
        Me.funeralPlanLabel.Text = "Have your arranged and pre-paid your funeral?"
        '
        'anyoneDisabledLabel
        '
        Me.anyoneDisabledLabel.AutoSize = True
        Me.anyoneDisabledLabel.Location = New System.Drawing.Point(251, 318)
        Me.anyoneDisabledLabel.Name = "anyoneDisabledLabel"
        Me.anyoneDisabledLabel.Size = New System.Drawing.Size(253, 18)
        Me.anyoneDisabledLabel.TabIndex = 23
        Me.anyoneDisabledLabel.Text = "Are any beneficiaries registered disabled?"
        '
        'Panel29
        '
        Me.Panel29.Controls.Add(Me.anyoneDisabledNo)
        Me.Panel29.Controls.Add(Me.anyoneDisabledYes)
        Me.Panel29.Location = New System.Drawing.Point(546, 314)
        Me.Panel29.Name = "Panel29"
        Me.Panel29.Size = New System.Drawing.Size(122, 26)
        Me.Panel29.TabIndex = 22
        '
        'anyoneDisabledNo
        '
        Me.anyoneDisabledNo.AutoSize = True
        Me.anyoneDisabledNo.Location = New System.Drawing.Point(59, 3)
        Me.anyoneDisabledNo.Name = "anyoneDisabledNo"
        Me.anyoneDisabledNo.Size = New System.Drawing.Size(41, 22)
        Me.anyoneDisabledNo.TabIndex = 1
        Me.anyoneDisabledNo.TabStop = True
        Me.anyoneDisabledNo.Text = "No"
        Me.anyoneDisabledNo.UseVisualStyleBackColor = True
        '
        'anyoneDisabledYes
        '
        Me.anyoneDisabledYes.AutoSize = True
        Me.anyoneDisabledYes.Location = New System.Drawing.Point(3, 3)
        Me.anyoneDisabledYes.Name = "anyoneDisabledYes"
        Me.anyoneDisabledYes.Size = New System.Drawing.Size(45, 22)
        Me.anyoneDisabledYes.TabIndex = 0
        Me.anyoneDisabledYes.TabStop = True
        Me.anyoneDisabledYes.Text = "Yes"
        Me.anyoneDisabledYes.UseVisualStyleBackColor = True
        '
        'Panel28
        '
        Me.Panel28.Controls.Add(Me.lpaPFANo)
        Me.Panel28.Controls.Add(Me.lpaPFAYes)
        Me.Panel28.Location = New System.Drawing.Point(546, 230)
        Me.Panel28.Name = "Panel28"
        Me.Panel28.Size = New System.Drawing.Size(122, 26)
        Me.Panel28.TabIndex = 20
        '
        'lpaPFANo
        '
        Me.lpaPFANo.AutoSize = True
        Me.lpaPFANo.Location = New System.Drawing.Point(59, 3)
        Me.lpaPFANo.Name = "lpaPFANo"
        Me.lpaPFANo.Size = New System.Drawing.Size(41, 22)
        Me.lpaPFANo.TabIndex = 1
        Me.lpaPFANo.TabStop = True
        Me.lpaPFANo.Text = "No"
        Me.lpaPFANo.UseVisualStyleBackColor = True
        '
        'lpaPFAYes
        '
        Me.lpaPFAYes.AutoSize = True
        Me.lpaPFAYes.Location = New System.Drawing.Point(3, 3)
        Me.lpaPFAYes.Name = "lpaPFAYes"
        Me.lpaPFAYes.Size = New System.Drawing.Size(45, 22)
        Me.lpaPFAYes.TabIndex = 0
        Me.lpaPFAYes.TabStop = True
        Me.lpaPFAYes.Text = "Yes"
        Me.lpaPFAYes.UseVisualStyleBackColor = True
        '
        'lpaPFALabel
        '
        Me.lpaPFALabel.AutoSize = True
        Me.lpaPFALabel.Location = New System.Drawing.Point(24, 234)
        Me.lpaPFALabel.Name = "lpaPFALabel"
        Me.lpaPFALabel.Size = New System.Drawing.Size(480, 18)
        Me.lpaPFALabel.TabIndex = 21
        Me.lpaPFALabel.Text = "Have you appointed anyone to look after your financial affairs are you get older?" &
    ""
        '
        'Panel27
        '
        Me.Panel27.Controls.Add(Me.protectAssetsNo)
        Me.Panel27.Controls.Add(Me.protectAssetsYes)
        Me.Panel27.Location = New System.Drawing.Point(546, 188)
        Me.Panel27.Name = "Panel27"
        Me.Panel27.Size = New System.Drawing.Size(122, 26)
        Me.Panel27.TabIndex = 18
        '
        'protectAssetsNo
        '
        Me.protectAssetsNo.AutoSize = True
        Me.protectAssetsNo.Location = New System.Drawing.Point(59, 3)
        Me.protectAssetsNo.Name = "protectAssetsNo"
        Me.protectAssetsNo.Size = New System.Drawing.Size(41, 22)
        Me.protectAssetsNo.TabIndex = 1
        Me.protectAssetsNo.TabStop = True
        Me.protectAssetsNo.Text = "No"
        Me.protectAssetsNo.UseVisualStyleBackColor = True
        '
        'protectAssetsYes
        '
        Me.protectAssetsYes.AutoSize = True
        Me.protectAssetsYes.Location = New System.Drawing.Point(3, 3)
        Me.protectAssetsYes.Name = "protectAssetsYes"
        Me.protectAssetsYes.Size = New System.Drawing.Size(45, 22)
        Me.protectAssetsYes.TabIndex = 0
        Me.protectAssetsYes.TabStop = True
        Me.protectAssetsYes.Text = "Yes"
        Me.protectAssetsYes.UseVisualStyleBackColor = True
        '
        'protectAssetsLabel
        '
        Me.protectAssetsLabel.AutoSize = True
        Me.protectAssetsLabel.Location = New System.Drawing.Point(222, 192)
        Me.protectAssetsLabel.Name = "protectAssetsLabel"
        Me.protectAssetsLabel.Size = New System.Drawing.Size(282, 18)
        Me.protectAssetsLabel.TabIndex = 19
        Me.protectAssetsLabel.Text = "Any other financial assets you want to protect?"
        '
        'Panel26
        '
        Me.Panel26.Controls.Add(Me.protectPropertyNo)
        Me.Panel26.Controls.Add(Me.protectPropertyYes)
        Me.Panel26.Location = New System.Drawing.Point(546, 146)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Size = New System.Drawing.Size(122, 26)
        Me.Panel26.TabIndex = 2
        '
        'protectPropertyNo
        '
        Me.protectPropertyNo.AutoSize = True
        Me.protectPropertyNo.Location = New System.Drawing.Point(59, 3)
        Me.protectPropertyNo.Name = "protectPropertyNo"
        Me.protectPropertyNo.Size = New System.Drawing.Size(41, 22)
        Me.protectPropertyNo.TabIndex = 1
        Me.protectPropertyNo.TabStop = True
        Me.protectPropertyNo.Text = "No"
        Me.protectPropertyNo.UseVisualStyleBackColor = True
        '
        'protectPropertyYes
        '
        Me.protectPropertyYes.AutoSize = True
        Me.protectPropertyYes.Location = New System.Drawing.Point(3, 3)
        Me.protectPropertyYes.Name = "protectPropertyYes"
        Me.protectPropertyYes.Size = New System.Drawing.Size(45, 22)
        Me.protectPropertyYes.TabIndex = 0
        Me.protectPropertyYes.TabStop = True
        Me.protectPropertyYes.Text = "Yes"
        Me.protectPropertyYes.UseVisualStyleBackColor = True
        '
        'protectPropertyLabel
        '
        Me.protectPropertyLabel.AutoSize = True
        Me.protectPropertyLabel.Location = New System.Drawing.Point(222, 150)
        Me.protectPropertyLabel.Name = "protectPropertyLabel"
        Me.protectPropertyLabel.Size = New System.Drawing.Size(282, 18)
        Me.protectPropertyLabel.TabIndex = 17
        Me.protectPropertyLabel.Text = "Are you interested in protecting your property?"
        '
        'Panel25
        '
        Me.Panel25.Controls.Add(Me.propertyNo)
        Me.Panel25.Controls.Add(Me.propertyYes)
        Me.Panel25.Location = New System.Drawing.Point(546, 62)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Size = New System.Drawing.Size(122, 26)
        Me.Panel25.TabIndex = 2
        '
        'propertyNo
        '
        Me.propertyNo.AutoSize = True
        Me.propertyNo.Location = New System.Drawing.Point(59, 2)
        Me.propertyNo.Name = "propertyNo"
        Me.propertyNo.Size = New System.Drawing.Size(41, 22)
        Me.propertyNo.TabIndex = 1
        Me.propertyNo.TabStop = True
        Me.propertyNo.Text = "No"
        Me.propertyNo.UseVisualStyleBackColor = True
        '
        'propertyYes
        '
        Me.propertyYes.AutoSize = True
        Me.propertyYes.Location = New System.Drawing.Point(3, 3)
        Me.propertyYes.Name = "propertyYes"
        Me.propertyYes.Size = New System.Drawing.Size(45, 22)
        Me.propertyYes.TabIndex = 0
        Me.propertyYes.TabStop = True
        Me.propertyYes.Text = "Yes"
        Me.propertyYes.UseVisualStyleBackColor = True
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.mortgageFreeNo)
        Me.Panel20.Controls.Add(Me.mortgageFreeYes)
        Me.Panel20.Location = New System.Drawing.Point(546, 104)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(122, 26)
        Me.Panel20.TabIndex = 0
        '
        'mortgageFreeNo
        '
        Me.mortgageFreeNo.AutoSize = True
        Me.mortgageFreeNo.Location = New System.Drawing.Point(59, 3)
        Me.mortgageFreeNo.Name = "mortgageFreeNo"
        Me.mortgageFreeNo.Size = New System.Drawing.Size(41, 22)
        Me.mortgageFreeNo.TabIndex = 1
        Me.mortgageFreeNo.TabStop = True
        Me.mortgageFreeNo.Text = "No"
        Me.mortgageFreeNo.UseVisualStyleBackColor = True
        '
        'mortgageFreeYes
        '
        Me.mortgageFreeYes.AutoSize = True
        Me.mortgageFreeYes.Location = New System.Drawing.Point(3, 3)
        Me.mortgageFreeYes.Name = "mortgageFreeYes"
        Me.mortgageFreeYes.Size = New System.Drawing.Size(45, 22)
        Me.mortgageFreeYes.TabIndex = 0
        Me.mortgageFreeYes.TabStop = True
        Me.mortgageFreeYes.Text = "Yes"
        Me.mortgageFreeYes.UseVisualStyleBackColor = True
        '
        'propertyLabel
        '
        Me.propertyLabel.AutoSize = True
        Me.propertyLabel.Location = New System.Drawing.Point(361, 66)
        Me.propertyLabel.Name = "propertyLabel"
        Me.propertyLabel.Size = New System.Drawing.Size(143, 18)
        Me.propertyLabel.TabIndex = 7
        Me.propertyLabel.Text = "Do you own a property?"
        '
        'mortgageFreeLabel
        '
        Me.mortgageFreeLabel.AutoSize = True
        Me.mortgageFreeLabel.Location = New System.Drawing.Point(379, 108)
        Me.mortgageFreeLabel.Name = "mortgageFreeLabel"
        Me.mortgageFreeLabel.Size = New System.Drawing.Size(125, 18)
        Me.mortgageFreeLabel.TabIndex = 6
        Me.mortgageFreeLabel.Text = "Is it Mortgage Free?"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(139, 20)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Establishing Facts"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.client2Feelings)
        Me.TabPage2.Controls.Add(Me.client1Feelings)
        Me.TabPage2.Controls.Add(Me.assetTablename)
        Me.TabPage2.Controls.Add(Me.Label76)
        Me.TabPage2.Controls.Add(Me.TextBox74)
        Me.TabPage2.Controls.Add(Me.TextBox9)
        Me.TabPage2.Controls.Add(Me.TextBox10)
        Me.TabPage2.Controls.Add(Me.TextBox11)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.TextBox4)
        Me.TabPage2.Controls.Add(Me.TextBox5)
        Me.TabPage2.Controls.Add(Me.TextBox6)
        Me.TabPage2.Controls.Add(Me.TextBox3)
        Me.TabPage2.Controls.Add(Me.TextBox2)
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.DataGridView2)
        Me.TabPage2.Controls.Add(Me.Button3)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Location = New System.Drawing.Point(4, 41)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Asset Information"
        '
        'client2Feelings
        '
        Me.client2Feelings.FormattingEnabled = True
        Me.client2Feelings.Items.AddRange(New Object() {"Select...", "Perfect", "OK", "Could Be Better", "Poor"})
        Me.client2Feelings.Location = New System.Drawing.Point(509, 436)
        Me.client2Feelings.Name = "client2Feelings"
        Me.client2Feelings.Size = New System.Drawing.Size(96, 26)
        Me.client2Feelings.TabIndex = 62
        '
        'client1Feelings
        '
        Me.client1Feelings.FormattingEnabled = True
        Me.client1Feelings.Items.AddRange(New Object() {"Select...", "Perfect", "OK", "Could Be Better", "Poor"})
        Me.client1Feelings.Location = New System.Drawing.Point(410, 436)
        Me.client1Feelings.Name = "client1Feelings"
        Me.client1Feelings.Size = New System.Drawing.Size(96, 26)
        Me.client1Feelings.TabIndex = 61
        '
        'assetTablename
        '
        Me.assetTablename.AutoSize = True
        Me.assetTablename.Location = New System.Drawing.Point(930, 33)
        Me.assetTablename.Name = "assetTablename"
        Me.assetTablename.Size = New System.Drawing.Size(101, 18)
        Me.assetTablename.TabIndex = 60
        Me.assetTablename.Text = "assetTablename"
        Me.assetTablename.Visible = False
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(710, 357)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(15, 18)
        Me.Label76.TabIndex = 59
        Me.Label76.Text = "="
        '
        'TextBox74
        '
        Me.TextBox74.Enabled = False
        Me.TextBox74.Location = New System.Drawing.Point(731, 354)
        Me.TextBox74.Name = "TextBox74"
        Me.TextBox74.Size = New System.Drawing.Size(96, 23)
        Me.TextBox74.TabIndex = 58
        '
        'TextBox9
        '
        Me.TextBox9.Enabled = False
        Me.TextBox9.Location = New System.Drawing.Point(608, 354)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(96, 23)
        Me.TextBox9.TabIndex = 57
        '
        'TextBox10
        '
        Me.TextBox10.Enabled = False
        Me.TextBox10.Location = New System.Drawing.Point(509, 354)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(96, 23)
        Me.TextBox10.TabIndex = 56
        '
        'TextBox11
        '
        Me.TextBox11.Enabled = False
        Me.TextBox11.Location = New System.Drawing.Point(410, 354)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(96, 23)
        Me.TextBox11.TabIndex = 55
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(321, 357)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(77, 18)
        Me.Label23.TabIndex = 54
        Me.Label23.Text = "Total Assets"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(95, 438)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(311, 18)
        Me.Label22.TabIndex = 51
        Me.Label22.Text = "How do you feel about your current levels of return?"
        '
        'TextBox4
        '
        Me.TextBox4.Enabled = False
        Me.TextBox4.Location = New System.Drawing.Point(731, 381)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(96, 23)
        Me.TextBox4.TabIndex = 50
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(509, 381)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(96, 23)
        Me.TextBox5.TabIndex = 2
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(410, 381)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(96, 23)
        Me.TextBox6.TabIndex = 1
        '
        'TextBox3
        '
        Me.TextBox3.Enabled = False
        Me.TextBox3.Location = New System.Drawing.Point(731, 408)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(96, 23)
        Me.TextBox3.TabIndex = 47
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(509, 408)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(96, 23)
        Me.TextBox2.TabIndex = 4
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(410, 408)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(96, 23)
        Me.TextBox1.TabIndex = 3
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(55, 411)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(337, 18)
        Me.Label21.TabIndex = 44
        Me.Label21.Text = "How much emergency cash should you keep in the bank?"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(281, 387)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(117, 18)
        Me.Label20.TabIndex = 43
        Me.Label20.Text = "Total gross income"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.Column1, Me.DataGridViewLinkColumn2, Me.Column2})
        Me.DataGridView2.Location = New System.Drawing.Point(16, 72)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView2.Size = New System.Drawing.Size(822, 272)
        Me.DataGridView2.TabIndex = 42
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Asset Type"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Detail"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Client 1"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Client 2"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'Column1
        '
        Me.Column1.HeaderText = "Joint Total"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'DataGridViewLinkColumn2
        '
        Me.DataGridViewLinkColumn2.HeaderText = "Action"
        Me.DataGridViewLinkColumn2.Name = "DataGridViewLinkColumn2"
        Me.DataGridViewLinkColumn2.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "tableName"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Visible = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(713, 30)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(125, 36)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "Add An Asset"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(135, 20)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Asset Information"
        '
        'TabPage9
        '
        Me.TabPage9.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage9.Controls.Add(Me.Panel5)
        Me.TabPage9.Controls.Add(Me.storagePaymentMethod)
        Me.TabPage9.Controls.Add(Me.Label133)
        Me.TabPage9.Controls.Add(Me.storageAccountNumber)
        Me.TabPage9.Controls.Add(Me.storageSortcode)
        Me.TabPage9.Controls.Add(Me.storageAmount)
        Me.TabPage9.Controls.Add(Me.storageProduct)
        Me.TabPage9.Controls.Add(Me.Label132)
        Me.TabPage9.Controls.Add(Me.Label131)
        Me.TabPage9.Controls.Add(Me.Label130)
        Me.TabPage9.Controls.Add(Me.Label129)
        Me.TabPage9.Controls.Add(Me.Label127)
        Me.TabPage9.Location = New System.Drawing.Point(4, 41)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = "Storage Options"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.client_storageButtonNo)
        Me.Panel5.Controls.Add(Me.client_storageButtonYes)
        Me.Panel5.Controls.Add(Me.Label128)
        Me.Panel5.Location = New System.Drawing.Point(35, 61)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(299, 37)
        Me.Panel5.TabIndex = 33
        '
        'client_storageButtonNo
        '
        Me.client_storageButtonNo.AutoSize = True
        Me.client_storageButtonNo.Location = New System.Drawing.Point(225, 9)
        Me.client_storageButtonNo.Name = "client_storageButtonNo"
        Me.client_storageButtonNo.Size = New System.Drawing.Size(41, 22)
        Me.client_storageButtonNo.TabIndex = 22
        Me.client_storageButtonNo.TabStop = True
        Me.client_storageButtonNo.Text = "No"
        Me.client_storageButtonNo.UseVisualStyleBackColor = True
        '
        'client_storageButtonYes
        '
        Me.client_storageButtonYes.AutoSize = True
        Me.client_storageButtonYes.Location = New System.Drawing.Point(169, 9)
        Me.client_storageButtonYes.Name = "client_storageButtonYes"
        Me.client_storageButtonYes.Size = New System.Drawing.Size(45, 22)
        Me.client_storageButtonYes.TabIndex = 21
        Me.client_storageButtonYes.TabStop = True
        Me.client_storageButtonYes.Text = "Yes"
        Me.client_storageButtonYes.UseVisualStyleBackColor = True
        '
        'Label128
        '
        Me.Label128.AutoSize = True
        Me.Label128.Location = New System.Drawing.Point(33, 11)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(109, 18)
        Me.Label128.TabIndex = 20
        Me.Label128.Text = "Existing Storage?"
        '
        'storagePaymentMethod
        '
        Me.storagePaymentMethod.Location = New System.Drawing.Point(204, 285)
        Me.storagePaymentMethod.Name = "storagePaymentMethod"
        Me.storagePaymentMethod.Size = New System.Drawing.Size(243, 23)
        Me.storagePaymentMethod.TabIndex = 32
        '
        'Label133
        '
        Me.Label133.AutoSize = True
        Me.Label133.Location = New System.Drawing.Point(73, 288)
        Me.Label133.Name = "Label133"
        Me.Label133.Size = New System.Drawing.Size(104, 18)
        Me.Label133.TabIndex = 31
        Me.Label133.Text = "Payment Method"
        '
        'storageAccountNumber
        '
        Me.storageAccountNumber.Location = New System.Drawing.Point(204, 241)
        Me.storageAccountNumber.Name = "storageAccountNumber"
        Me.storageAccountNumber.Size = New System.Drawing.Size(170, 23)
        Me.storageAccountNumber.TabIndex = 30
        '
        'storageSortcode
        '
        Me.storageSortcode.Location = New System.Drawing.Point(204, 197)
        Me.storageSortcode.Name = "storageSortcode"
        Me.storageSortcode.Size = New System.Drawing.Size(170, 23)
        Me.storageSortcode.TabIndex = 29
        '
        'storageAmount
        '
        Me.storageAmount.Location = New System.Drawing.Point(204, 153)
        Me.storageAmount.Name = "storageAmount"
        Me.storageAmount.Size = New System.Drawing.Size(59, 23)
        Me.storageAmount.TabIndex = 28
        '
        'storageProduct
        '
        Me.storageProduct.Location = New System.Drawing.Point(204, 109)
        Me.storageProduct.Name = "storageProduct"
        Me.storageProduct.Size = New System.Drawing.Size(435, 23)
        Me.storageProduct.TabIndex = 27
        '
        'Label132
        '
        Me.Label132.AutoSize = True
        Me.Label132.Location = New System.Drawing.Point(73, 244)
        Me.Label132.Name = "Label132"
        Me.Label132.Size = New System.Drawing.Size(103, 18)
        Me.Label132.TabIndex = 26
        Me.Label132.Text = "Account Number"
        '
        'Label131
        '
        Me.Label131.AutoSize = True
        Me.Label131.Location = New System.Drawing.Point(112, 200)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(65, 18)
        Me.Label131.TabIndex = 25
        Me.Label131.Text = "Sort Code"
        '
        'Label130
        '
        Me.Label130.AutoSize = True
        Me.Label130.Location = New System.Drawing.Point(83, 156)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(95, 18)
        Me.Label130.TabIndex = 24
        Me.Label130.Text = "Annual Amount"
        '
        'Label129
        '
        Me.Label129.AutoSize = True
        Me.Label129.Location = New System.Drawing.Point(75, 112)
        Me.Label129.Name = "Label129"
        Me.Label129.Size = New System.Drawing.Size(100, 18)
        Me.Label129.TabIndex = 23
        Me.Label129.Text = "Storage Product"
        '
        'Label127
        '
        Me.Label127.AutoSize = True
        Me.Label127.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label127.Location = New System.Drawing.Point(12, 20)
        Me.Label127.Name = "Label127"
        Me.Label127.Size = New System.Drawing.Size(125, 20)
        Me.Label127.TabIndex = 5
        Me.Label127.Text = "Storage Options"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage3.Controls.Add(Me.paymentAmount)
        Me.TabPage3.Controls.Add(Me.howIsClientPaying)
        Me.TabPage3.Controls.Add(Me.Label121)
        Me.TabPage3.Controls.Add(Me.Label120)
        Me.TabPage3.Controls.Add(Me.manualOverrideFlag)
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Location = New System.Drawing.Point(4, 41)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "  Client Recommendations"
        '
        'paymentAmount
        '
        Me.paymentAmount.Location = New System.Drawing.Point(481, 444)
        Me.paymentAmount.Name = "paymentAmount"
        Me.paymentAmount.Size = New System.Drawing.Size(90, 23)
        Me.paymentAmount.TabIndex = 5
        '
        'howIsClientPaying
        '
        Me.howIsClientPaying.Location = New System.Drawing.Point(187, 444)
        Me.howIsClientPaying.Name = "howIsClientPaying"
        Me.howIsClientPaying.Size = New System.Drawing.Size(181, 23)
        Me.howIsClientPaying.TabIndex = 4
        '
        'Label121
        '
        Me.Label121.AutoSize = True
        Me.Label121.Location = New System.Drawing.Point(374, 447)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(103, 18)
        Me.Label121.TabIndex = 46
        Me.Label121.Text = "Confirm Amount"
        '
        'Label120
        '
        Me.Label120.AutoSize = True
        Me.Label120.Location = New System.Drawing.Point(27, 447)
        Me.Label120.Name = "Label120"
        Me.Label120.Size = New System.Drawing.Size(153, 18)
        Me.Label120.TabIndex = 45
        Me.Label120.Text = "How is the client paying?"
        '
        'manualOverrideFlag
        '
        Me.manualOverrideFlag.AutoSize = True
        Me.manualOverrideFlag.Location = New System.Drawing.Point(452, 45)
        Me.manualOverrideFlag.Name = "manualOverrideFlag"
        Me.manualOverrideFlag.Size = New System.Drawing.Size(178, 22)
        Me.manualOverrideFlag.TabIndex = 19
        Me.manualOverrideFlag.Text = "Manual Override Required"
        Me.manualOverrideFlag.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.recStorageFlag)
        Me.GroupBox4.Controls.Add(Me.recStoragePrice)
        Me.GroupBox4.Controls.Add(Me.Label135)
        Me.GroupBox4.Controls.Add(Me.Label107)
        Me.GroupBox4.Controls.Add(Me.Label25)
        Me.GroupBox4.Controls.Add(Me.recPackagePrice)
        Me.GroupBox4.Controls.Add(Me.Label102)
        Me.GroupBox4.Controls.Add(Me.recProbatePlanFlag)
        Me.GroupBox4.Controls.Add(Me.recFAPTFlag)
        Me.GroupBox4.Controls.Add(Me.recPFAFlag)
        Me.GroupBox4.Controls.Add(Me.recHWFlag)
        Me.GroupBox4.Controls.Add(Me.recBasicWillFlag)
        Me.GroupBox4.Controls.Add(Me.recWillPPTFlag)
        Me.GroupBox4.Controls.Add(Me.recWillReviewFlag)
        Me.GroupBox4.Controls.Add(Me.recProbatePlanPrice)
        Me.GroupBox4.Controls.Add(Me.recFAPTPrice)
        Me.GroupBox4.Controls.Add(Me.recPFAPrice)
        Me.GroupBox4.Controls.Add(Me.recHWPrice)
        Me.GroupBox4.Controls.Add(Me.recBasicWillPrice)
        Me.GroupBox4.Controls.Add(Me.recWillPPTPrice)
        Me.GroupBox4.Controls.Add(Me.recWillReviewPrice)
        Me.GroupBox4.Controls.Add(Me.recSubTotalPrice)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.Label106)
        Me.GroupBox4.Controls.Add(Me.Label105)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.Label101)
        Me.GroupBox4.Controls.Add(Me.Label104)
        Me.GroupBox4.Location = New System.Drawing.Point(30, 45)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(396, 363)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Recommended Products"
        '
        'recStorageFlag
        '
        Me.recStorageFlag.AutoSize = True
        Me.recStorageFlag.Enabled = False
        Me.recStorageFlag.Location = New System.Drawing.Point(334, 333)
        Me.recStorageFlag.Name = "recStorageFlag"
        Me.recStorageFlag.Size = New System.Drawing.Size(15, 14)
        Me.recStorageFlag.TabIndex = 47
        Me.recStorageFlag.UseVisualStyleBackColor = True
        '
        'recStoragePrice
        '
        Me.recStoragePrice.Enabled = False
        Me.recStoragePrice.Location = New System.Drawing.Point(211, 328)
        Me.recStoragePrice.Name = "recStoragePrice"
        Me.recStoragePrice.Size = New System.Drawing.Size(90, 23)
        Me.recStoragePrice.TabIndex = 46
        '
        'Label135
        '
        Me.Label135.AutoSize = True
        Me.Label135.Location = New System.Drawing.Point(78, 331)
        Me.Label135.Name = "Label135"
        Me.Label135.Size = New System.Drawing.Size(115, 18)
        Me.Label135.TabIndex = 45
        Me.Label135.Text = "Document Storage"
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Location = New System.Drawing.Point(307, 298)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(50, 18)
        Me.Label107.TabIndex = 44
        Me.Label107.Text = "inc VAT"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(307, 265)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(50, 18)
        Me.Label25.TabIndex = 43
        Me.Label25.Text = "inc VAT"
        '
        'recPackagePrice
        '
        Me.recPackagePrice.Enabled = False
        Me.recPackagePrice.Location = New System.Drawing.Point(211, 295)
        Me.recPackagePrice.Name = "recPackagePrice"
        Me.recPackagePrice.Size = New System.Drawing.Size(90, 23)
        Me.recPackagePrice.TabIndex = 41
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.Location = New System.Drawing.Point(102, 298)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(87, 18)
        Me.Label102.TabIndex = 42
        Me.Label102.Text = "Package Price"
        '
        'recProbatePlanFlag
        '
        Me.recProbatePlanFlag.AutoSize = True
        Me.recProbatePlanFlag.Enabled = False
        Me.recProbatePlanFlag.Location = New System.Drawing.Point(334, 234)
        Me.recProbatePlanFlag.Name = "recProbatePlanFlag"
        Me.recProbatePlanFlag.Size = New System.Drawing.Size(15, 14)
        Me.recProbatePlanFlag.TabIndex = 40
        Me.recProbatePlanFlag.UseVisualStyleBackColor = True
        '
        'recFAPTFlag
        '
        Me.recFAPTFlag.AutoSize = True
        Me.recFAPTFlag.Enabled = False
        Me.recFAPTFlag.Location = New System.Drawing.Point(334, 201)
        Me.recFAPTFlag.Name = "recFAPTFlag"
        Me.recFAPTFlag.Size = New System.Drawing.Size(15, 14)
        Me.recFAPTFlag.TabIndex = 39
        Me.recFAPTFlag.UseVisualStyleBackColor = True
        '
        'recPFAFlag
        '
        Me.recPFAFlag.AutoSize = True
        Me.recPFAFlag.Enabled = False
        Me.recPFAFlag.Location = New System.Drawing.Point(334, 168)
        Me.recPFAFlag.Name = "recPFAFlag"
        Me.recPFAFlag.Size = New System.Drawing.Size(15, 14)
        Me.recPFAFlag.TabIndex = 38
        Me.recPFAFlag.UseVisualStyleBackColor = True
        '
        'recHWFlag
        '
        Me.recHWFlag.AutoSize = True
        Me.recHWFlag.Enabled = False
        Me.recHWFlag.Location = New System.Drawing.Point(334, 135)
        Me.recHWFlag.Name = "recHWFlag"
        Me.recHWFlag.Size = New System.Drawing.Size(15, 14)
        Me.recHWFlag.TabIndex = 37
        Me.recHWFlag.UseVisualStyleBackColor = True
        '
        'recBasicWillFlag
        '
        Me.recBasicWillFlag.AutoSize = True
        Me.recBasicWillFlag.Enabled = False
        Me.recBasicWillFlag.Location = New System.Drawing.Point(334, 102)
        Me.recBasicWillFlag.Name = "recBasicWillFlag"
        Me.recBasicWillFlag.Size = New System.Drawing.Size(15, 14)
        Me.recBasicWillFlag.TabIndex = 36
        Me.recBasicWillFlag.UseVisualStyleBackColor = True
        '
        'recWillPPTFlag
        '
        Me.recWillPPTFlag.AutoSize = True
        Me.recWillPPTFlag.Enabled = False
        Me.recWillPPTFlag.Location = New System.Drawing.Point(334, 69)
        Me.recWillPPTFlag.Name = "recWillPPTFlag"
        Me.recWillPPTFlag.Size = New System.Drawing.Size(15, 14)
        Me.recWillPPTFlag.TabIndex = 35
        Me.recWillPPTFlag.UseVisualStyleBackColor = True
        '
        'recWillReviewFlag
        '
        Me.recWillReviewFlag.AutoSize = True
        Me.recWillReviewFlag.Enabled = False
        Me.recWillReviewFlag.Location = New System.Drawing.Point(334, 36)
        Me.recWillReviewFlag.Name = "recWillReviewFlag"
        Me.recWillReviewFlag.Size = New System.Drawing.Size(15, 14)
        Me.recWillReviewFlag.TabIndex = 13
        Me.recWillReviewFlag.UseVisualStyleBackColor = True
        '
        'recProbatePlanPrice
        '
        Me.recProbatePlanPrice.Enabled = False
        Me.recProbatePlanPrice.Location = New System.Drawing.Point(211, 229)
        Me.recProbatePlanPrice.Name = "recProbatePlanPrice"
        Me.recProbatePlanPrice.Size = New System.Drawing.Size(90, 23)
        Me.recProbatePlanPrice.TabIndex = 34
        '
        'recFAPTPrice
        '
        Me.recFAPTPrice.Enabled = False
        Me.recFAPTPrice.Location = New System.Drawing.Point(211, 196)
        Me.recFAPTPrice.Name = "recFAPTPrice"
        Me.recFAPTPrice.Size = New System.Drawing.Size(90, 23)
        Me.recFAPTPrice.TabIndex = 33
        '
        'recPFAPrice
        '
        Me.recPFAPrice.Enabled = False
        Me.recPFAPrice.Location = New System.Drawing.Point(211, 163)
        Me.recPFAPrice.Name = "recPFAPrice"
        Me.recPFAPrice.Size = New System.Drawing.Size(90, 23)
        Me.recPFAPrice.TabIndex = 32
        '
        'recHWPrice
        '
        Me.recHWPrice.Enabled = False
        Me.recHWPrice.Location = New System.Drawing.Point(211, 130)
        Me.recHWPrice.Name = "recHWPrice"
        Me.recHWPrice.Size = New System.Drawing.Size(90, 23)
        Me.recHWPrice.TabIndex = 31
        '
        'recBasicWillPrice
        '
        Me.recBasicWillPrice.Enabled = False
        Me.recBasicWillPrice.Location = New System.Drawing.Point(211, 97)
        Me.recBasicWillPrice.Name = "recBasicWillPrice"
        Me.recBasicWillPrice.Size = New System.Drawing.Size(90, 23)
        Me.recBasicWillPrice.TabIndex = 30
        '
        'recWillPPTPrice
        '
        Me.recWillPPTPrice.Enabled = False
        Me.recWillPPTPrice.Location = New System.Drawing.Point(211, 64)
        Me.recWillPPTPrice.Name = "recWillPPTPrice"
        Me.recWillPPTPrice.Size = New System.Drawing.Size(90, 23)
        Me.recWillPPTPrice.TabIndex = 29
        '
        'recWillReviewPrice
        '
        Me.recWillReviewPrice.Enabled = False
        Me.recWillReviewPrice.Location = New System.Drawing.Point(211, 31)
        Me.recWillReviewPrice.Name = "recWillReviewPrice"
        Me.recWillReviewPrice.Size = New System.Drawing.Size(90, 23)
        Me.recWillReviewPrice.TabIndex = 28
        '
        'recSubTotalPrice
        '
        Me.recSubTotalPrice.Enabled = False
        Me.recSubTotalPrice.Location = New System.Drawing.Point(211, 262)
        Me.recSubTotalPrice.Name = "recSubTotalPrice"
        Me.recSubTotalPrice.Size = New System.Drawing.Size(90, 23)
        Me.recSubTotalPrice.TabIndex = 17
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(120, 265)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(75, 18)
        Me.Label19.TabIndex = 18
        Me.Label19.Text = "Grand Total"
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Location = New System.Drawing.Point(132, 100)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(64, 18)
        Me.Label106.TabIndex = 27
        Me.Label106.Text = "Basic Will"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Location = New System.Drawing.Point(103, 67)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(93, 18)
        Me.Label105.TabIndex = 26
        Me.Label105.Text = "Will with Trust"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(121, 34)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(75, 18)
        Me.Label16.TabIndex = 17
        Me.Label16.Text = "Will Review"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(59, 133)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(137, 18)
        Me.Label17.TabIndex = 19
        Me.Label17.Text = "H&&W LPA inc OPG Fee"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(65, 166)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(130, 18)
        Me.Label18.TabIndex = 20
        Me.Label18.Text = "P&&F LPA inc OPG Fee"
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.Location = New System.Drawing.Point(18, 199)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(178, 18)
        Me.Label101.TabIndex = 21
        Me.Label101.Text = "Family Asset Protection Trust"
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.Location = New System.Drawing.Point(113, 232)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(80, 18)
        Me.Label104.TabIndex = 23
        Me.Label104.Text = "Probate Plan"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.manStorageFlag)
        Me.GroupBox2.Controls.Add(Me.manStoragePrice)
        Me.GroupBox2.Controls.Add(Me.Label136)
        Me.GroupBox2.Controls.Add(Me.manPFAOPG)
        Me.GroupBox2.Controls.Add(Me.manHWOPG)
        Me.GroupBox2.Controls.Add(Me.Label122)
        Me.GroupBox2.Controls.Add(Me.Label123)
        Me.GroupBox2.Controls.Add(Me.Panel4)
        Me.GroupBox2.Controls.Add(Me.Label66)
        Me.GroupBox2.Controls.Add(Me.manPackagePrice)
        Me.GroupBox2.Controls.Add(Me.Label67)
        Me.GroupBox2.Controls.Add(Me.Label63)
        Me.GroupBox2.Controls.Add(Me.manOtherFlag)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.Label30)
        Me.GroupBox2.Controls.Add(Me.Label62)
        Me.GroupBox2.Controls.Add(Me.manSubTotalPrice)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.manProbatePlanFlag)
        Me.GroupBox2.Controls.Add(Me.manFAPTFlag)
        Me.GroupBox2.Controls.Add(Me.manPFAFlag)
        Me.GroupBox2.Controls.Add(Me.manHWFlag)
        Me.GroupBox2.Controls.Add(Me.manBasicWillFlag)
        Me.GroupBox2.Controls.Add(Me.manWillPPTFlag)
        Me.GroupBox2.Controls.Add(Me.manWillReviewFlag)
        Me.GroupBox2.Controls.Add(Me.manProbatePlanPrice)
        Me.GroupBox2.Controls.Add(Me.manFAPTPrice)
        Me.GroupBox2.Controls.Add(Me.manPFAPrice)
        Me.GroupBox2.Controls.Add(Me.manHWPrice)
        Me.GroupBox2.Controls.Add(Me.manBasicWillPrice)
        Me.GroupBox2.Controls.Add(Me.manWillPPTPrice)
        Me.GroupBox2.Controls.Add(Me.manWillReviewPrice)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.Label31)
        Me.GroupBox2.Controls.Add(Me.Label103)
        Me.GroupBox2.Location = New System.Drawing.Point(657, 45)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(521, 428)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Manually Choose Products"
        Me.GroupBox2.Visible = False
        '
        'manStorageFlag
        '
        Me.manStorageFlag.AutoSize = True
        Me.manStorageFlag.Enabled = False
        Me.manStorageFlag.Location = New System.Drawing.Point(336, 390)
        Me.manStorageFlag.Name = "manStorageFlag"
        Me.manStorageFlag.Size = New System.Drawing.Size(15, 14)
        Me.manStorageFlag.TabIndex = 84
        Me.manStorageFlag.UseVisualStyleBackColor = True
        '
        'manStoragePrice
        '
        Me.manStoragePrice.Enabled = False
        Me.manStoragePrice.Location = New System.Drawing.Point(213, 385)
        Me.manStoragePrice.Name = "manStoragePrice"
        Me.manStoragePrice.Size = New System.Drawing.Size(90, 23)
        Me.manStoragePrice.TabIndex = 83
        '
        'Label136
        '
        Me.Label136.AutoSize = True
        Me.Label136.Location = New System.Drawing.Point(80, 388)
        Me.Label136.Name = "Label136"
        Me.Label136.Size = New System.Drawing.Size(115, 18)
        Me.Label136.TabIndex = 82
        Me.Label136.Text = "Document Storage"
        '
        'manPFAOPG
        '
        Me.manPFAOPG.AutoSize = True
        Me.manPFAOPG.Location = New System.Drawing.Point(447, 168)
        Me.manPFAOPG.Name = "manPFAOPG"
        Me.manPFAOPG.Size = New System.Drawing.Size(15, 14)
        Me.manPFAOPG.TabIndex = 81
        Me.manPFAOPG.UseVisualStyleBackColor = True
        '
        'manHWOPG
        '
        Me.manHWOPG.AutoSize = True
        Me.manHWOPG.Location = New System.Drawing.Point(447, 135)
        Me.manHWOPG.Name = "manHWOPG"
        Me.manHWOPG.Size = New System.Drawing.Size(15, 14)
        Me.manHWOPG.TabIndex = 80
        Me.manHWOPG.UseVisualStyleBackColor = True
        '
        'Label122
        '
        Me.Label122.AutoSize = True
        Me.Label122.Location = New System.Drawing.Point(361, 133)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(79, 18)
        Me.Label122.TabIndex = 78
        Me.Label122.Text = "inc OPG Fee"
        '
        'Label123
        '
        Me.Label123.AutoSize = True
        Me.Label123.Location = New System.Drawing.Point(361, 166)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(79, 18)
        Me.Label123.TabIndex = 79
        Me.Label123.Text = "inc OPG Fee"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label65)
        Me.Panel4.Controls.Add(Me.Label64)
        Me.Panel4.Controls.Add(Me.manOtherDescription)
        Me.Panel4.Controls.Add(Me.manOtherPrice)
        Me.Panel4.Location = New System.Drawing.Point(110, 283)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(397, 40)
        Me.Panel4.TabIndex = 77
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(251, 6)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(36, 18)
        Me.Label65.TabIndex = 1
        Me.Label65.Text = "Price"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(19, 6)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(74, 18)
        Me.Label64.TabIndex = 75
        Me.Label64.Text = "Description"
        '
        'manOtherDescription
        '
        Me.manOtherDescription.Location = New System.Drawing.Point(101, 3)
        Me.manOtherDescription.Name = "manOtherDescription"
        Me.manOtherDescription.Size = New System.Drawing.Size(144, 23)
        Me.manOtherDescription.TabIndex = 0
        '
        'manOtherPrice
        '
        Me.manOtherPrice.Enabled = False
        Me.manOtherPrice.Location = New System.Drawing.Point(296, 3)
        Me.manOtherPrice.Name = "manOtherPrice"
        Me.manOtherPrice.Size = New System.Drawing.Size(90, 23)
        Me.manOtherPrice.TabIndex = 72
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(309, 354)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(50, 18)
        Me.Label66.TabIndex = 47
        Me.Label66.Text = "inc VAT"
        '
        'manPackagePrice
        '
        Me.manPackagePrice.Enabled = False
        Me.manPackagePrice.Location = New System.Drawing.Point(213, 351)
        Me.manPackagePrice.Name = "manPackagePrice"
        Me.manPackagePrice.Size = New System.Drawing.Size(90, 23)
        Me.manPackagePrice.TabIndex = 45
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(103, 354)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(87, 18)
        Me.Label67.TabIndex = 46
        Me.Label67.Text = "Package Price"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(160, 261)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(41, 18)
        Me.Label63.TabIndex = 74
        Me.Label63.Text = "Other"
        '
        'manOtherFlag
        '
        Me.manOtherFlag.AutoSize = True
        Me.manOtherFlag.Location = New System.Drawing.Point(213, 263)
        Me.manOtherFlag.Name = "manOtherFlag"
        Me.manOtherFlag.Size = New System.Drawing.Size(15, 14)
        Me.manOtherFlag.TabIndex = 7
        Me.manOtherFlag.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(132, 133)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(63, 18)
        Me.Label29.TabIndex = 69
        Me.Label29.Text = "H&&W LPA"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(138, 166)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(56, 18)
        Me.Label30.TabIndex = 70
        Me.Label30.Text = "P&&F LPA"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(309, 326)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(50, 18)
        Me.Label62.TabIndex = 68
        Me.Label62.Text = "inc VAT"
        '
        'manSubTotalPrice
        '
        Me.manSubTotalPrice.Enabled = False
        Me.manSubTotalPrice.Location = New System.Drawing.Point(213, 323)
        Me.manSubTotalPrice.Name = "manSubTotalPrice"
        Me.manSubTotalPrice.Size = New System.Drawing.Size(90, 23)
        Me.manSubTotalPrice.TabIndex = 66
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(121, 329)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(75, 18)
        Me.Label24.TabIndex = 67
        Me.Label24.Text = "Grand Total"
        '
        'manProbatePlanFlag
        '
        Me.manProbatePlanFlag.AutoSize = True
        Me.manProbatePlanFlag.Location = New System.Drawing.Point(321, 234)
        Me.manProbatePlanFlag.Name = "manProbatePlanFlag"
        Me.manProbatePlanFlag.Size = New System.Drawing.Size(15, 14)
        Me.manProbatePlanFlag.TabIndex = 6
        Me.manProbatePlanFlag.UseVisualStyleBackColor = True
        '
        'manFAPTFlag
        '
        Me.manFAPTFlag.AutoSize = True
        Me.manFAPTFlag.Location = New System.Drawing.Point(321, 201)
        Me.manFAPTFlag.Name = "manFAPTFlag"
        Me.manFAPTFlag.Size = New System.Drawing.Size(15, 14)
        Me.manFAPTFlag.TabIndex = 5
        Me.manFAPTFlag.UseVisualStyleBackColor = True
        '
        'manPFAFlag
        '
        Me.manPFAFlag.AutoSize = True
        Me.manPFAFlag.Location = New System.Drawing.Point(321, 168)
        Me.manPFAFlag.Name = "manPFAFlag"
        Me.manPFAFlag.Size = New System.Drawing.Size(15, 14)
        Me.manPFAFlag.TabIndex = 4
        Me.manPFAFlag.UseVisualStyleBackColor = True
        '
        'manHWFlag
        '
        Me.manHWFlag.AutoSize = True
        Me.manHWFlag.Location = New System.Drawing.Point(321, 135)
        Me.manHWFlag.Name = "manHWFlag"
        Me.manHWFlag.Size = New System.Drawing.Size(15, 14)
        Me.manHWFlag.TabIndex = 3
        Me.manHWFlag.UseVisualStyleBackColor = True
        '
        'manBasicWillFlag
        '
        Me.manBasicWillFlag.AutoSize = True
        Me.manBasicWillFlag.Location = New System.Drawing.Point(321, 102)
        Me.manBasicWillFlag.Name = "manBasicWillFlag"
        Me.manBasicWillFlag.Size = New System.Drawing.Size(15, 14)
        Me.manBasicWillFlag.TabIndex = 2
        Me.manBasicWillFlag.UseVisualStyleBackColor = True
        '
        'manWillPPTFlag
        '
        Me.manWillPPTFlag.AutoSize = True
        Me.manWillPPTFlag.Location = New System.Drawing.Point(321, 69)
        Me.manWillPPTFlag.Name = "manWillPPTFlag"
        Me.manWillPPTFlag.Size = New System.Drawing.Size(15, 14)
        Me.manWillPPTFlag.TabIndex = 1
        Me.manWillPPTFlag.UseVisualStyleBackColor = True
        '
        'manWillReviewFlag
        '
        Me.manWillReviewFlag.AutoSize = True
        Me.manWillReviewFlag.Location = New System.Drawing.Point(321, 36)
        Me.manWillReviewFlag.Name = "manWillReviewFlag"
        Me.manWillReviewFlag.Size = New System.Drawing.Size(15, 14)
        Me.manWillReviewFlag.TabIndex = 0
        Me.manWillReviewFlag.UseVisualStyleBackColor = True
        '
        'manProbatePlanPrice
        '
        Me.manProbatePlanPrice.Enabled = False
        Me.manProbatePlanPrice.Location = New System.Drawing.Point(213, 229)
        Me.manProbatePlanPrice.Name = "manProbatePlanPrice"
        Me.manProbatePlanPrice.Size = New System.Drawing.Size(90, 23)
        Me.manProbatePlanPrice.TabIndex = 59
        '
        'manFAPTPrice
        '
        Me.manFAPTPrice.Enabled = False
        Me.manFAPTPrice.Location = New System.Drawing.Point(213, 196)
        Me.manFAPTPrice.Name = "manFAPTPrice"
        Me.manFAPTPrice.Size = New System.Drawing.Size(90, 23)
        Me.manFAPTPrice.TabIndex = 58
        '
        'manPFAPrice
        '
        Me.manPFAPrice.Enabled = False
        Me.manPFAPrice.Location = New System.Drawing.Point(213, 163)
        Me.manPFAPrice.Name = "manPFAPrice"
        Me.manPFAPrice.Size = New System.Drawing.Size(90, 23)
        Me.manPFAPrice.TabIndex = 57
        '
        'manHWPrice
        '
        Me.manHWPrice.Enabled = False
        Me.manHWPrice.Location = New System.Drawing.Point(213, 130)
        Me.manHWPrice.Name = "manHWPrice"
        Me.manHWPrice.Size = New System.Drawing.Size(90, 23)
        Me.manHWPrice.TabIndex = 56
        '
        'manBasicWillPrice
        '
        Me.manBasicWillPrice.Enabled = False
        Me.manBasicWillPrice.Location = New System.Drawing.Point(213, 97)
        Me.manBasicWillPrice.Name = "manBasicWillPrice"
        Me.manBasicWillPrice.Size = New System.Drawing.Size(90, 23)
        Me.manBasicWillPrice.TabIndex = 55
        '
        'manWillPPTPrice
        '
        Me.manWillPPTPrice.Enabled = False
        Me.manWillPPTPrice.Location = New System.Drawing.Point(213, 64)
        Me.manWillPPTPrice.Name = "manWillPPTPrice"
        Me.manWillPPTPrice.Size = New System.Drawing.Size(90, 23)
        Me.manWillPPTPrice.TabIndex = 54
        '
        'manWillReviewPrice
        '
        Me.manWillReviewPrice.Enabled = False
        Me.manWillReviewPrice.Location = New System.Drawing.Point(213, 31)
        Me.manWillReviewPrice.Name = "manWillReviewPrice"
        Me.manWillReviewPrice.Size = New System.Drawing.Size(90, 23)
        Me.manWillReviewPrice.TabIndex = 53
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(133, 100)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(64, 18)
        Me.Label26.TabIndex = 52
        Me.Label26.Text = "Basic Will"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(104, 67)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(93, 18)
        Me.Label27.TabIndex = 51
        Me.Label27.Text = "Will with Trust"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(122, 34)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(75, 18)
        Me.Label28.TabIndex = 44
        Me.Label28.Text = "Will Review"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(19, 199)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(178, 18)
        Me.Label31.TabIndex = 49
        Me.Label31.Text = "Family Asset Protection Trust"
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.Location = New System.Drawing.Point(114, 232)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(80, 18)
        Me.Label103.TabIndex = 50
        Me.Label103.Text = "Probate Plan"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(187, 20)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Client Recommendations"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.FromArgb(CType(CType(116, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.TabPage4.Controls.Add(Me.Label8)
        Me.TabPage4.Controls.Add(Me.Label7)
        Me.TabPage4.Controls.Add(Me.Label5)
        Me.TabPage4.Controls.Add(Me.Label2)
        Me.TabPage4.Controls.Add(Me.yearsToDeathLabel)
        Me.TabPage4.Controls.Add(Me.probateFeePercentLabel)
        Me.TabPage4.Controls.Add(Me.TrackBar2)
        Me.TabPage4.Controls.Add(Me.futureProbateCost)
        Me.TabPage4.Controls.Add(Me.forecastProbateCost)
        Me.TabPage4.Controls.Add(Me.TrackBar1)
        Me.TabPage4.Controls.Add(Me.Label40)
        Me.TabPage4.Controls.Add(Me.probateInflation)
        Me.TabPage4.Controls.Add(Me.totalEstateValue)
        Me.TabPage4.Controls.Add(Me.Label39)
        Me.TabPage4.Controls.Add(Me.Label38)
        Me.TabPage4.Controls.Add(Me.Label3)
        Me.TabPage4.Location = New System.Drawing.Point(4, 41)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1201, 485)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Probate Forecast"
        '
        'TrackBar1
        '
        Me.TrackBar1.Location = New System.Drawing.Point(223, 109)
        Me.TrackBar1.Maximum = 50
        Me.TrackBar1.Minimum = 15
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.Size = New System.Drawing.Size(301, 45)
        Me.TrackBar1.TabIndex = 72
        Me.TrackBar1.Value = 15
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(263, 195)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(16, 18)
        Me.Label40.TabIndex = 12
        Me.Label40.Text = "%"
        '
        'probateInflation
        '
        Me.probateInflation.Enabled = False
        Me.probateInflation.Location = New System.Drawing.Point(223, 192)
        Me.probateInflation.Name = "probateInflation"
        Me.probateInflation.Size = New System.Drawing.Size(39, 23)
        Me.probateInflation.TabIndex = 11
        '
        'totalEstateValue
        '
        Me.totalEstateValue.Enabled = False
        Me.totalEstateValue.Location = New System.Drawing.Point(223, 66)
        Me.totalEstateValue.Name = "totalEstateValue"
        Me.totalEstateValue.Size = New System.Drawing.Size(125, 23)
        Me.totalEstateValue.TabIndex = 10
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(134, 195)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(57, 18)
        Me.Label39.TabIndex = 9
        Me.Label39.Text = "Inflation"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(64, 66)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(127, 18)
        Me.Label38.TabIndex = 8
        Me.Label38.Text = "Current Estate Value"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Probate Forecast"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(278, 29)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Recommendation Tool"
        '
        'forecastProbateCost
        '
        Me.forecastProbateCost.Enabled = False
        Me.forecastProbateCost.Location = New System.Drawing.Point(223, 144)
        Me.forecastProbateCost.Name = "forecastProbateCost"
        Me.forecastProbateCost.Size = New System.Drawing.Size(125, 23)
        Me.forecastProbateCost.TabIndex = 73
        '
        'futureProbateCost
        '
        Me.futureProbateCost.Enabled = False
        Me.futureProbateCost.Location = New System.Drawing.Point(223, 281)
        Me.futureProbateCost.Name = "futureProbateCost"
        Me.futureProbateCost.Size = New System.Drawing.Size(125, 23)
        Me.futureProbateCost.TabIndex = 74
        '
        'TrackBar2
        '
        Me.TrackBar2.Location = New System.Drawing.Point(223, 238)
        Me.TrackBar2.Maximum = 35
        Me.TrackBar2.Minimum = 1
        Me.TrackBar2.Name = "TrackBar2"
        Me.TrackBar2.Size = New System.Drawing.Size(301, 45)
        Me.TrackBar2.TabIndex = 75
        Me.TrackBar2.Value = 1
        '
        'probateFeePercentLabel
        '
        Me.probateFeePercentLabel.AutoSize = True
        Me.probateFeePercentLabel.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.probateFeePercentLabel.Location = New System.Drawing.Point(540, 109)
        Me.probateFeePercentLabel.Name = "probateFeePercentLabel"
        Me.probateFeePercentLabel.Size = New System.Drawing.Size(42, 22)
        Me.probateFeePercentLabel.TabIndex = 76
        Me.probateFeePercentLabel.Text = "1.5%"
        '
        'yearsToDeathLabel
        '
        Me.yearsToDeathLabel.AutoSize = True
        Me.yearsToDeathLabel.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.yearsToDeathLabel.Location = New System.Drawing.Point(540, 238)
        Me.yearsToDeathLabel.Name = "yearsToDeathLabel"
        Me.yearsToDeathLabel.Size = New System.Drawing.Size(18, 22)
        Me.yearsToDeathLabel.TabIndex = 77
        Me.yearsToDeathLabel.Text = "1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(109, 152)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 18)
        Me.Label2.TabIndex = 78
        Me.Label2.Text = "Probate Cost"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(83, 238)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 18)
        Me.Label5.TabIndex = 79
        Me.Label5.Text = "Years Until Death"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(67, 281)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(124, 18)
        Me.Label7.TabIndex = 80
        Me.Label7.Text = "Future Probate Cost"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(92, 109)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(99, 18)
        Me.Label8.TabIndex = 81
        Me.Label8.Text = "Probate Fee (%)"
        '
        'factfind_wizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button2)
        Me.MaximizeBox = False
        Me.Name = "factfind_wizard"
        Me.Text = "Client Instruction - Recommendation Tool"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel29.ResumeLayout(False)
        Me.Panel29.PerformLayout()
        Me.Panel28.ResumeLayout(False)
        Me.Panel28.PerformLayout()
        Me.Panel27.ResumeLayout(False)
        Me.Panel27.PerformLayout()
        Me.Panel26.ResumeLayout(False)
        Me.Panel26.PerformLayout()
        Me.Panel25.ResumeLayout(False)
        Me.Panel25.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage9.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button2 As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Panel20 As Panel
    Friend WithEvents mortgageFreeNo As RadioButton
    Friend WithEvents mortgageFreeYes As RadioButton
    Friend WithEvents propertyLabel As Label
    Friend WithEvents mortgageFreeLabel As Label
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents TextBox10 As TextBox
    Friend WithEvents TextBox11 As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents Button3 As Button
    Friend WithEvents Label40 As Label
    Friend WithEvents probateInflation As TextBox
    Friend WithEvents totalEstateValue As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents TextBox74 As TextBox
    Friend WithEvents Panel25 As Panel
    Friend WithEvents propertyNo As RadioButton
    Friend WithEvents propertyYes As RadioButton
    Friend WithEvents Panel3 As Panel
    Friend WithEvents probatePlanNo As RadioButton
    Friend WithEvents probatePlanYes As RadioButton
    Friend WithEvents probatePlanLabel As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents funeralPlanNo As RadioButton
    Friend WithEvents funeralPlanYes As RadioButton
    Friend WithEvents funeralPlanLabel As Label
    Friend WithEvents anyoneDisabledLabel As Label
    Friend WithEvents Panel29 As Panel
    Friend WithEvents anyoneDisabledNo As RadioButton
    Friend WithEvents anyoneDisabledYes As RadioButton
    Friend WithEvents Panel28 As Panel
    Friend WithEvents lpaPFANo As RadioButton
    Friend WithEvents lpaPFAYes As RadioButton
    Friend WithEvents lpaPFALabel As Label
    Friend WithEvents Panel27 As Panel
    Friend WithEvents protectAssetsNo As RadioButton
    Friend WithEvents protectAssetsYes As RadioButton
    Friend WithEvents protectAssetsLabel As Label
    Friend WithEvents Panel26 As Panel
    Friend WithEvents protectPropertyNo As RadioButton
    Friend WithEvents protectPropertyYes As RadioButton
    Friend WithEvents protectPropertyLabel As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents recPackagePrice As TextBox
    Friend WithEvents Label102 As Label
    Friend WithEvents recProbatePlanFlag As CheckBox
    Friend WithEvents recFAPTFlag As CheckBox
    Friend WithEvents recPFAFlag As CheckBox
    Friend WithEvents recHWFlag As CheckBox
    Friend WithEvents recBasicWillFlag As CheckBox
    Friend WithEvents recWillPPTFlag As CheckBox
    Friend WithEvents recWillReviewFlag As CheckBox
    Friend WithEvents recProbatePlanPrice As TextBox
    Friend WithEvents recFAPTPrice As TextBox
    Friend WithEvents recPFAPrice As TextBox
    Friend WithEvents recHWPrice As TextBox
    Friend WithEvents recBasicWillPrice As TextBox
    Friend WithEvents recWillPPTPrice As TextBox
    Friend WithEvents recWillReviewPrice As TextBox
    Friend WithEvents recSubTotalPrice As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label106 As Label
    Friend WithEvents Label105 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label101 As Label
    Friend WithEvents Label104 As Label
    Friend WithEvents manSubTotalPrice As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents manProbatePlanFlag As CheckBox
    Friend WithEvents manFAPTFlag As CheckBox
    Friend WithEvents manPFAFlag As CheckBox
    Friend WithEvents manHWFlag As CheckBox
    Friend WithEvents manBasicWillFlag As CheckBox
    Friend WithEvents manWillPPTFlag As CheckBox
    Friend WithEvents manWillReviewFlag As CheckBox
    Friend WithEvents manProbatePlanPrice As TextBox
    Friend WithEvents manFAPTPrice As TextBox
    Friend WithEvents manPFAPrice As TextBox
    Friend WithEvents manHWPrice As TextBox
    Friend WithEvents manBasicWillPrice As TextBox
    Friend WithEvents manWillPPTPrice As TextBox
    Friend WithEvents manWillReviewPrice As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents Label103 As Label
    Friend WithEvents Label107 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents manualOverrideFlag As CheckBox
    Friend WithEvents Label62 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents assetTablename As Label
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewLinkColumn2 As DataGridViewLinkColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents client2Feelings As ComboBox
    Friend WithEvents client1Feelings As ComboBox
    Friend WithEvents manOtherDescription As TextBox
    Friend WithEvents manOtherPrice As TextBox
    Friend WithEvents manOtherFlag As CheckBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label65 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents manPackagePrice As TextBox
    Friend WithEvents Label67 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents paymentAmount As TextBox
    Friend WithEvents howIsClientPaying As TextBox
    Friend WithEvents Label121 As Label
    Friend WithEvents Label120 As Label
    Friend WithEvents manPFAOPG As CheckBox
    Friend WithEvents manHWOPG As CheckBox
    Friend WithEvents Label122 As Label
    Friend WithEvents Label123 As Label
    Friend WithEvents TabPage9 As TabPage
    Friend WithEvents storagePaymentMethod As TextBox
    Friend WithEvents Label133 As Label
    Friend WithEvents storageAccountNumber As TextBox
    Friend WithEvents storageSortcode As TextBox
    Friend WithEvents storageAmount As TextBox
    Friend WithEvents storageProduct As TextBox
    Friend WithEvents Label132 As Label
    Friend WithEvents Label131 As Label
    Friend WithEvents Label130 As Label
    Friend WithEvents Label129 As Label
    Friend WithEvents client_storageButtonNo As RadioButton
    Friend WithEvents client_storageButtonYes As RadioButton
    Friend WithEvents Label128 As Label
    Friend WithEvents Label127 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents recStorageFlag As CheckBox
    Friend WithEvents recStoragePrice As TextBox
    Friend WithEvents Label135 As Label
    Friend WithEvents manStorageFlag As CheckBox
    Friend WithEvents manStoragePrice As TextBox
    Friend WithEvents Label136 As Label
    Friend WithEvents TrackBar1 As TrackBar
    Friend WithEvents Panel6 As Panel
    Friend WithEvents lpaHWNo As RadioButton
    Friend WithEvents lpaHWYes As RadioButton
    Friend WithEvents lpaHWLabel As Label
    Friend WithEvents yearsToDeathLabel As Label
    Friend WithEvents probateFeePercentLabel As Label
    Friend WithEvents TrackBar2 As TrackBar
    Friend WithEvents futureProbateCost As TextBox
    Friend WithEvents forecastProbateCost As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label2 As Label
End Class
