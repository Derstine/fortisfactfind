﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class assets_add
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.assetCategory = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.description = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RadioButtonClient1 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonClient2 = New System.Windows.Forms.RadioButton()
        Me.RadioButtonClientJointly = New System.Windows.Forms.RadioButton()
        Me.assetValue = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.currentCharges = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout
        Me.Panel2.SuspendLayout
        Me.SuspendLayout
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button3.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(345, 374)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(99, 36)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "Add Asset"
        Me.Button3.UseVisualStyleBackColor = false
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(450, 374)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(99, 36)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Cancel"
        Me.Button1.UseVisualStyleBackColor = false
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(135, 20)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Asset Information"
        '
        'Label50
        '
        Me.Label50.AutoSize = true
        Me.Label50.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label50.Location = New System.Drawing.Point(42, 65)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(96, 18)
        Me.Label50.TabIndex = 45
        Me.Label50.Text = "Asset Category"
        '
        'assetCategory
        '
        Me.assetCategory.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.assetCategory.FormattingEnabled = true
        Me.assetCategory.Items.AddRange(New Object() {"Select...", "Property", "Cars/Bikes/Motorhomes", "Valuables", "Contents", "Bank Accounts", "ISA", "Bond", "Other Investments"})
        Me.assetCategory.Location = New System.Drawing.Point(148, 62)
        Me.assetCategory.Name = "assetCategory"
        Me.assetCategory.Size = New System.Drawing.Size(199, 26)
        Me.assetCategory.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.Location = New System.Drawing.Point(66, 108)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 18)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Description"
        '
        'description
        '
        Me.description.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.description.Location = New System.Drawing.Point(148, 105)
        Me.description.Name = "description"
        Me.description.Size = New System.Drawing.Size(289, 23)
        Me.description.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 283)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 18)
        Me.Label2.TabIndex = 49
        Me.Label2.Text = "Owned By"
        '
        'RadioButtonClient1
        '
        Me.RadioButtonClient1.AutoSize = true
        Me.RadioButtonClient1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButtonClient1.Location = New System.Drawing.Point(148, 281)
        Me.RadioButtonClient1.Name = "RadioButtonClient1"
        Me.RadioButtonClient1.Size = New System.Drawing.Size(71, 22)
        Me.RadioButtonClient1.TabIndex = 5
        Me.RadioButtonClient1.TabStop = true
        Me.RadioButtonClient1.Text = "Client 1"
        Me.RadioButtonClient1.UseVisualStyleBackColor = true
        '
        'RadioButtonClient2
        '
        Me.RadioButtonClient2.AutoSize = true
        Me.RadioButtonClient2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButtonClient2.Location = New System.Drawing.Point(148, 307)
        Me.RadioButtonClient2.Name = "RadioButtonClient2"
        Me.RadioButtonClient2.Size = New System.Drawing.Size(71, 22)
        Me.RadioButtonClient2.TabIndex = 6
        Me.RadioButtonClient2.TabStop = true
        Me.RadioButtonClient2.Text = "Client 2"
        Me.RadioButtonClient2.UseVisualStyleBackColor = true
        '
        'RadioButtonClientJointly
        '
        Me.RadioButtonClientJointly.AutoSize = true
        Me.RadioButtonClientJointly.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButtonClientJointly.Location = New System.Drawing.Point(148, 333)
        Me.RadioButtonClientJointly.Name = "RadioButtonClientJointly"
        Me.RadioButtonClientJointly.Size = New System.Drawing.Size(64, 22)
        Me.RadioButtonClientJointly.TabIndex = 7
        Me.RadioButtonClientJointly.TabStop = true
        Me.RadioButtonClientJointly.Text = "Jointly"
        Me.RadioButtonClientJointly.UseVisualStyleBackColor = true
        '
        'assetValue
        '
        Me.assetValue.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.assetValue.Location = New System.Drawing.Point(148, 148)
        Me.assetValue.Name = "assetValue"
        Me.assetValue.Size = New System.Drawing.Size(113, 23)
        Me.assetValue.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label3.Location = New System.Drawing.Point(99, 151)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 18)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "Value"
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = true
        Me.RadioButton4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton4.Location = New System.Drawing.Point(249, 11)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(108, 22)
        Me.RadioButton4.TabIndex = 2
        Me.RadioButton4.TabStop = true
        Me.RadioButton4.Text = "Not Applicable"
        Me.RadioButton4.UseVisualStyleBackColor = true
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = true
        Me.RadioButton5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton5.Location = New System.Drawing.Point(174, 11)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(41, 22)
        Me.RadioButton5.TabIndex = 1
        Me.RadioButton5.TabStop = true
        Me.RadioButton5.Text = "No"
        Me.RadioButton5.UseVisualStyleBackColor = true
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = true
        Me.RadioButton6.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.RadioButton6.Location = New System.Drawing.Point(99, 11)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(45, 22)
        Me.RadioButton6.TabIndex = 0
        Me.RadioButton6.TabStop = true
        Me.RadioButton6.Text = "Yes"
        Me.RadioButton6.UseVisualStyleBackColor = true
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 18)
        Me.Label4.TabIndex = 57
        Me.Label4.Text = "Mortgaged?"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButton4)
        Me.Panel1.Controls.Add(Me.RadioButton5)
        Me.Panel1.Controls.Add(Me.RadioButton6)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(49, 180)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(387, 40)
        Me.Panel1.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.currentCharges)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Location = New System.Drawing.Point(26, 226)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(387, 40)
        Me.Panel2.TabIndex = 4
        '
        'currentCharges
        '
        Me.currentCharges.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.currentCharges.Location = New System.Drawing.Point(122, 10)
        Me.currentCharges.Name = "currentCharges"
        Me.currentCharges.Size = New System.Drawing.Size(113, 23)
        Me.currentCharges.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(103, 18)
        Me.Label5.TabIndex = 57
        Me.Label5.Text = "Current Charges"
        '
        'assets_add
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221,Byte),Integer), CType(CType(244,Byte),Integer), CType(CType(250,Byte),Integer))
        Me.ClientSize = New System.Drawing.Size(561, 422)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.assetValue)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.RadioButtonClientJointly)
        Me.Controls.Add(Me.RadioButtonClient2)
        Me.Controls.Add(Me.RadioButtonClient1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.description)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.assetCategory)
        Me.Controls.Add(Me.Label50)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Name = "assets_add"
        Me.Text = "Add An Asset"
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.Panel2.ResumeLayout(false)
        Me.Panel2.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents Button3 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents assetCategory As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents description As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents RadioButtonClient1 As RadioButton
    Friend WithEvents RadioButtonClient2 As RadioButton
    Friend WithEvents RadioButtonClientJointly As RadioButton
    Friend WithEvents assetValue As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents RadioButton6 As RadioButton
    Friend WithEvents Label4 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents currentCharges As TextBox
    Friend WithEvents Label5 As Label
End Class
