﻿Public Class probate_gifts_add
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.title.SelectedIndex = 0
        Me.relationship.SelectedIndex = 0

        Me.Label2.Text = "Relationship to " + getClientFirstName(whoDied)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If forenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the beneficiary's name" + vbCrLf
        If item.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the the item being gifted" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "insert into probate_gifts (willClientSequence,whichClient,title,forenames,surname,relationship,address,postcode,item,phone,email) values ('" + clientID + "','" + whoDied + "','" + SqlSafe(title.SelectedItem) + "','" + SqlSafe(forenames.Text) + "','" + SqlSafe(surname.Text) + "','" + SqlSafe(relationship.SelectedItem) + "','" + SqlSafe(address.Text) + "','" + SqlSafe(postcode.Text) + "','" + SqlSafe(item.Text) + "','" + SqlSafe(phone.Text) + "','" + SqlSafe(email.Text) + "')"
            runSQL(sql)
            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        postcode.Text = postcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = postcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.address.Text = f.FoundAddress()
            Me.postcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

End Class