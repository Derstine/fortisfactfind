﻿Public Class children_edit
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub assets_add_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)
        Me.childTitle.SelectedIndex = 0
        Me.relationToClient1.SelectedIndex = 0
        Me.relationToClient2.SelectedIndex = 0

        If internetFlag = "Y" Then Me.Button2.visible = True Else Me.Button2.visible = False

        Me.Label2.Text = "Relationship to " + getClientFirstName("client1")
        Me.Label3.Text = "Relationship to " + getClientFirstName("client2")
        Button4.Text = "copy from " + getClientFirstName("client1")
        Button5.Text = "copy from " + getClientFirstName("client5")

        'get current data
        Dim sql As String = "select * from will_instruction_stage4_data_detail where sequence=" + rowID
        Dim results As Array = runSQLwithArray(sql)

        If results(9) = "Mr" Then childTitle.SelectedIndex = 1
        If results(9) = "Mrs" Then childTitle.SelectedIndex = 2
        If results(9) = "Miss" Then childTitle.SelectedIndex = 3
        If results(9) = "Ms" Then childTitle.SelectedIndex = 4

        If results(4) = "Bloodline" Then relationToClient1.SelectedIndex = 1
        If results(4) = "Stepchild" Then relationToClient1.SelectedIndex = 1

        If results(5) = "Bloodline" Then relationToClient2.SelectedIndex = 1
        If results(5) = "Stepchild" Then relationToClient2.SelectedIndex = 1

        childForenames.Text = results(3)
        childSurname.Text = results(2)
        childPostcode.Text = results(10)
        address.Text = results(6)
        childDOB.Value = results(11)

        If clientType() = "mirror" Then
            Me.Label3.Visible = True
            Me.relationToClient2.Visible = True
            Button5.Visible = True
        Else
            Me.Label3.Visible = False
            Me.relationToClient2.Visible = False
            Button5.Visible = False
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim sql = ""

        Dim errorcount = 0
        Dim errortext = ""

        If childTitle.Text = "Select..." Then errorcount = errorcount + 1 : errortext = errortext + "You must specify the child's title" + vbCrLf
        If childForenames.Text = "" Then errorcount = errorcount + 1 : errortext = errortext + "You must enter the child's name" + vbCrLf

        If errorcount > 0 Then
            MsgBox(errortext)
        Else
            'it's ok
            sql = "update will_instruction_stage4_data_detail set childTitle='" + SqlSafe(childTitle.SelectedItem) + "',childForenames='" + SqlSafe(childForenames.Text) + "',childSurname='" + SqlSafe(childSurname.Text) + "',relationToClient1='" + SqlSafe(relationToClient1.SelectedItem) + "',relationToClient2='" + SqlSafe(relationToClient2.SelectedItem) + "',address='" + SqlSafe(address.Text) + "',childPostcode='" + SqlSafe(childPostcode.Text) + "',childDOB='" + SqlSafe(childDOB.Value) + "'  where sequence=" + rowID
            runSQL(sql)
        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        childPostcode.Text = childPostcode.Text.ToUpper
        Dim f As New addressLookup()
        searchPostcode = childPostcode.Text
        f.StartPosition = FormStartPosition.CenterParent
        If f.ShowDialog Then
            Me.address.Text = f.FoundAddress()
            Me.childPostcode.Text = f.FoundPostcode()
        End If
        f.Dispose()
        Me.Refresh()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim sql As String = "select person1Postcode,person1Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        childPostcode.Text = results(0)
        address.Text = results(1)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim sql As String = "select person2Postcode,person2Address from will_instruction_stage2_data where willClientSequence=" + clientID
        Dim results As Array = runSQLwithArray(sql)
        childPostcode.Text = results(0)
        address.Text = results(1)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Select Case MessageBox.Show("Are you sure you want to DELETE this child?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2)
            Case DialogResult.Yes
                'MsgBox("YES Clicked.")
                Dim sql As String
                sql = "delete from will_instruction_stage4_data_detail where sequence=" + rowID
                runSQL(sql)

                Me.Close()

            Case DialogResult.No
                ' MsgBox("NO Clicked.")
        End Select
    End Sub
End Class