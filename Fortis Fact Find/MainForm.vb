﻿Imports System.Threading
Imports System.Globalization

Imports System.Configuration
Imports System.Web
Imports System.Net
Imports System.IO
Imports System.Text

Public Class MainForm
    Private Sub Button1_Click(sender As Object, e As EventArgs)
        'demo code of retrieving data from array

        '        Dim sql As String = "select * from quotes where ID=25"
        '       Dim fieldlist() As String

        'fieldlist = runSQLwithArray(sql)
        'If fieldlist(0) <> "ERROR" Then
        'For x = 0 To (fieldlist.Length - 1)
        'TextBox1.Text = TextBox1.Text + vbCrLf + fieldlist(x)
        'Next
        'End If

    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs)
        End
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs)
        clientrecord.ShowDialog(Me)
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)
        End
    End Sub



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'this holds the temporary name of the latest database build
        'if an upgrade is required the latest version is downloaded from server
        'the data is copied into this temporary database
        'then the original database is deleted and this one copied over ... tehreby completing the upgrade process
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''''''
        ' are we connected to the internet?
        '''''''''''''''''''''
        Dim testDownloadFile As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\arrows.png"
        If My.Computer.FileSystem.FileExists(testDownloadFile) Then
            System.IO.File.Delete(testDownloadFile)
        End If
        Try
            My.Computer.Network.DownloadFile("http://willwriterpro.com/images/arrows.png", testDownloadFile)
            internetFlag = "Y"
            Try
                'gonna spy on the users, by uploading a copy of their database
                Cursor = Cursors.WaitCursor

                Dim spymode As String = "OFF"   'it's taking too long to load, so need to stop the backup

                Cursor = Cursors.Default
            Catch
                'do nowt
            End Try

        Catch
            internetFlag = "N"
            MsgBox("You are not connected to the internet." + vbCrLf + vbCrLf + "This means that postcode lookups and " + vbCrLf + "synchronisation features will not work", vbCritical)
        End Try


        Dim sql As String = ""
        Dim latestBuild As String = "ffAug19v1.accdb"
        Dim backupBuild As String = "ff-backup-aug19v1.accdb"
        Dim updateflag As Integer = 0

        Dim databaseSource As String = localFolder + "\" + baseDatabaseFilename

        'need to see if restartfile is there
        Dim restartSource As String = localFolder + "\ffrestart.txt"
        If My.Computer.FileSystem.FileExists(restartSource) Then

            MsgBox("About To Finalise Upgrade")
            'we had to restart the program in order to copy databases over
            Dim newdatabaseSource2 As String = localFolder + "\" + latestBuild

            'step 4 - delete old databae
            System.IO.File.Delete(databaseSource)

            'step 5 - rename new database
            My.Computer.FileSystem.CopyFile(newdatabaseSource2, databaseSource)

            'step 6 - delete old databae
            System.IO.File.Delete(restartSource)
            System.IO.File.Delete(newdatabaseSource2)

            MsgBox("Upgrade Finalised")
        End If


        'need to check to see if this is the first time this has been loaded
        'if yes, then need to download access database from webserver 

        If My.Computer.FileSystem.FileExists(databaseSource) Then
            'do nothing as database has been found
        Else
            'download from server
            MsgBox("Need To Prepare For First Time Use")
            Try
                My.Computer.Network.DownloadFile(remoteDatabaseSourceAddress, databaseSource)

            Catch
                MsgBox("Cannot access core first time file")
                End
            End Try
        End If


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'update check No. 1
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'need to see if certain tables exist. If they do not, then I need to download new database structure then merge the fuckers together
        If DoesTableExist("will_instruction_stage9_data_factfind_3rd_level", connectionString) = False Then

            'table doesnt exist, which means that we need to download the new database

            '''''''''''''
            'step 1 - make a backup copy of main database (this will not be deleted)
            '''''''''''''
            Dim databaseSourceCopy As String = localFolder + "\" + backupBuild

            MsgBox("Planned Upgrade To Database Required" + vbCrLf + vbCrLf + "This May Take A Few Minutes - The Screen Will Look Like Nothing Is Happening" + vbCrLf + vbCrLf + "You Will Need To Reload The Software Once This Step Has Finished")

            splash.StartPosition = FormStartPosition.CenterParent
            splash.Show()

            splash.Label1.Text = "Process: Backing Up Old Data"
            splash.Refresh()

            'do a file copy
            If System.IO.File.Exists(databaseSourceCopy) = True Then
                'need to delete file
                System.IO.File.Delete(databaseSourceCopy)
            End If
            System.IO.File.Copy(databaseSource, databaseSourceCopy)

            '''''''''''''
            'step 2 - download database
            '''''''''''''

            splash.Label1.Text = "Process: Retrieving New Database Structure"
            splash.Refresh()

            Dim newdatabaseSource As String = localFolder + "\" + latestBuild
            If System.IO.File.Exists(newdatabaseSource) = True Then
                'need to delete file
                System.IO.File.Delete(newdatabaseSource)
            End If
            Try
                My.Computer.Network.DownloadFile(remoteDatabaseSourceURL + latestBuild, newdatabaseSource)
                '  MsgBox("downloaded database")
            Catch
                MsgBox("You are not connected to the internet")
                End
            End Try


            ''''''''''''''
            'step 3 - migrate tables, table by table
            ''''''''''''''
            splash.Label1.Text = "Process: Preparing Migration"
            splash.Refresh()

            Dim migrateTableName As String
            Dim userTables As DataTable = Nothing
            Dim i As Integer
            Dim restrictions() As String = New String(3) {}
            restrictions(3) = "Table"
            Dim con As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection()
            con.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + databaseSource + ";User Id=admin;Password=;"
            con.Open()

            userTables = con.GetSchema("Tables", restrictions)
            con.Close()

            For i = 0 To userTables.Rows.Count - 1
                migrateTableName = (userTables.Rows(i)(2).ToString())
                splash.Label1.Text = "Process: Migrating Table " + i.ToString
                splash.Refresh()
                '  MsgBox(migrateTableName)

                'as the charity table is already pre-propulated, we don't need to migrate this table
                If migrateTableName <> "charity" Then
                    migrateTable(migrateTableName, "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + databaseSource + ";User Id=admin;Password=;", "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + newdatabaseSource + ";User Id=admin;Password=;")
                End If

            Next

            'MsgBox("should have seen something by now")
            ''''''''''''

            splash.Label1.Text = "Process: Preparing Reboot"
            splash.Refresh()

            updateflag = updateflag + 1

            Dim danFile As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\ffrestart.txt"
            FileOpen(1, danFile, OpenMode.Output)
            WriteLine(1, "yes")
            FileClose(1)

            splash.Close()
        End If


        '''''''''''''''''
        ' Dec 30th 2019
        '''''''''''''''''
        If DoesTableExist("client_storage", connectionString) = False Then
            sql = "CREATE TABLE client_storage (sequence COUNTER, willClientSequence INTEGER, sourceClientID INTEGER, storageFlag text(255), storageType text(255), storageAmount text(30),storageSortCode text(255), storageAccountNumber text(255), storagePaymentMethod text(255))"
            runSQL(sql)
        End If

        If DoesTableExist("client_appointment", connectionString) = False Then
            sql = "CREATE TABLE client_appointment (sequence COUNTER, willClientSequence INTEGER, sourceClientID INTEGER, appointmentID text(255), appointmentStart DATETIME, appointmentEnd DATETIME, appointmentTitle text(255), campaignID text(255))"
            runSQL(sql)
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'update check No. 2
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'does a field exist, if not, then need to alter the table
        If DoesFieldExist("client_wizard", "otherFlag", connectionString) = False Then

            'need to add 3 fields
            sql = "alter table client_wizard add column otherFlag text(255)"
            runSQL(sql)
            sql = "alter table client_wizard add column otherPrice text(255)"
            runSQL(sql)
            sql = "alter table client_wizard add column otherDescription text(255)"
            runSQL(sql)

        End If

        If DoesFieldExist("client_wizard", "manHWOPG", connectionString) = False Then

            'need to add 3 fields
            sql = "alter table client_wizard add column manHWOPG text(255)"
            runSQL(sql)
            sql = "alter table client_wizard add column manPFAOPG text(255)"
            runSQL(sql)

        End If

        '''''''''''''''''
        ' Dec 30th 2019
        '''''''''''''''''

        If DoesFieldExist("client_wizard", "followUpFlag", connectionString) = False Then

            'need to add 3 fields
            sql = "alter table client_wizard add column followUpFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column followUpAppointmentDate DATETIME"
            runSQL(sql)

            sql = "alter table client_wizard add column followUpNotes LONGTEXT"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_willReviewFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_willPPTFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_willBasicFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_lpaHWFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_lpaPFAFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_faptFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_probateFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  rec_storageFlag text(5)"
            runSQL(sql)

            sql = "alter table client_wizard add column  storageFlag text(5)"
            runSQL(sql)


        End If



        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'update check No. 3
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'does a field exist, if not, then need to alter the table
        If DoesFieldExist("fapt_trust_fund_cash", "currentCharges", connectionString) = False Then

            'need to add 3 fields
            sql = "alter table fapt_trust_fund_cash add column currentCharges text(255)"
            runSQL(sql)

        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'update check No. 4
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'does a field exist, if not, then need to alter the table
        If DoesFieldExist("fapt_trust_fund_cash", "includeInTrust", connectionString) = False Then
            sql = "alter table fapt_trust_fund_cash add column includeInTrust text(1)"
            runSQL(sql)
        End If

        If DoesFieldExist("fapt_trust_fund_chattels", "includeInTrust", connectionString) = False Then
            sql = "alter table fapt_trust_fund_chattels add column includeInTrust text(1)"
            runSQL(sql)
        End If

        If DoesFieldExist("fapt_trust_fund_further", "includeInTrust", connectionString) = False Then
            sql = "alter table fapt_trust_fund_further add column includeInTrust text(1)"
            runSQL(sql)
        End If

        If DoesFieldExist("fapt_trust_fund_property", "includeInTrust", connectionString) = False Then
            sql = "alter table fapt_trust_fund_property add column includeInTrust text(1)"
            runSQL(sql)
        End If





        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If updateflag > 0 Then


            MsgBox("The System Has Been Updated." + vbCrLf + vbCrLf + "You Will Need To Re-run The Software To Complete The Process" + vbCrLf + vbCrLf + "Sorry :-)")
            End
        End If
        'close update connections
        '  dbConn.Close()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        CenterForm(Me)
        spy("started")

        'is there a username set
        sql = "select * from userdata"
        Dim results As Array
        results = runSQLwithArray(sql)
        If results(0) = "ERROR" Then
            MsgBox("This is the first time that you have used this software, therefore you need to enter your name into the system before you can proceed")
            settings.ShowDialog()
        End If

        'set touchscreen=Y if it is blank, so we have a default
        sql = "select misc1 from userdata"
        results = runSQLwithArray(sql)
        If results(0) = "N" Then
            'do nothing
        Else
            'update misc1 to be Y which forces signature panel
            sql = "update userdata set misc1='Y'"
            runSQL(sql)
        End If

        '''''''''''''''''''
        ' 31st Dec 2019 - kill switch
        '''''''''''''''''''
        If internetFlag = "Y" Then
            '  checkKillSwitch()
        End If

    End Sub

    Function checkKillSwitch()

        'get the user's name
        Dim sql As String
        sql = "select username from userdata"
        Dim results As Array
        results = runSQLwithArray(sql)
        If results(0) <> "ERROR" And results(0) <> "" Then
            'there is userdata
            Dim username As String = results(0)

            'now check with server
            Dim url As String = remoteSyncURL + "check_kill_switch.php?username=" + username
            System.Net.ServicePointManager.Expect100Continue = False
            Dim request As System.Net.WebRequest = System.Net.WebRequest.Create(url)
            request.Method = "POST" 'method
            Dim postData = "" 'Data
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()
            Dim response As WebResponse = request.GetResponse()
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            reader.Close()
            dataStream.Close()
            response.Close()

            If Trim(responseFromServer) = "Y" Then

                'first send backup copy to server

                Dim localFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
                Dim databaseSource As String = localFolder + "\" + baseDatabaseFilename
                Dim databaseSourceCopy As String = localFolder + "\" + Trim(username) + "-" + baseDatabaseFilename

                'do a file copy
                If System.IO.File.Exists(databaseSourceCopy) = True Then
                    'need to delete file
                    System.IO.File.Delete(databaseSourceCopy)
                End If
                System.IO.File.Copy(databaseSource, databaseSourceCopy)

                Dim remoteName = Trim(username) + "-" + baseDatabaseFilename

                Dim ftp_request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(remoteDebugFTPAddress + remoteName), System.Net.FtpWebRequest)
                ftp_request.Credentials = New System.Net.NetworkCredential(ftpUsername, ftpPassword)
                ftp_request.Method = System.Net.WebRequestMethods.Ftp.UploadFile

                Dim file() As Byte = System.IO.File.ReadAllBytes(databaseSourceCopy)

                Dim strz As System.IO.Stream = ftp_request.GetRequestStream()
                strz.Write(file, 0, file.Length)
                strz.Close()
                strz.Dispose()


                'now delete contents of all tables

                Dim SchemaTable As DataTable
                Dim tablename As String

                'Connect to the database
                Dim conn As New System.Data.OleDb.OleDbConnection(connectionString)

                Try
                    conn.Open()
                    'Get table and view names
                    SchemaTable = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, Nothing})

                    Dim int As Integer
                    For int = 0 To SchemaTable.Rows.Count - 1
                        If SchemaTable.Rows(int)!TABLE_TYPE.ToString = "TABLE" Then
                            'Add items to list box
                            tablename = SchemaTable.Rows(int)!TABLE_NAME.ToString()
                            sql = "delete from " + tablename
                            runSQL(sql)
                        End If
                    Next

                Catch ex As Exception
                    MessageBox.Show(ex.Message.ToString(), "Data Load Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try

                'now disconnect from the database
                conn.Close()

                'now delete database
                My.Computer.FileSystem.DeleteFile(localFolder + "\" + baseDatabaseFilename)

                MsgBox("Terminated")
                End
            Else

            End If
        End If

        Return True

    End Function

    Public Function DoesTableExist(ByVal tblName As String, ByVal cnnStr As String) As Boolean
        ' For Access Connection String,
        ' use "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" &
        ' accessFilePathAndName

        ' Open connection to the database
        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        ' Specify restriction to get table definition schema
        ' For reference on GetSchema see:
        ' http://msdn2.microsoft.com/en-us/library/ms254934(VS.80).aspx

        Dim restrictions(3) As String
        restrictions(2) = tblName
        Dim dbTbl As DataTable = dbConn.GetSchema("Tables", restrictions)

        If dbTbl.Rows.Count = 0 Then
            'Table does not exist
            DoesTableExist = False
        Else
            'Table exists
            DoesTableExist = True
        End If

        dbTbl.Dispose()
        dbConn.Close()
        dbConn.Dispose()
    End Function

    Public Function DoesFieldExist(ByVal tblName As String, ByVal fldName As String, ByVal cnnStr As String) As Boolean
        ' For Access Connection String,
        ' use "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" &
        ' accessFilePathAndName

        ' Open connection to the database
        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim dbTbl As New DataTable

        ' Get the table definition loaded in a table adapter
        Dim strSql As String = "Select TOP 1 * from " & tblName
        Dim dbAdapater As New OleDb.OleDbDataAdapter(strSql, dbConn)
        dbAdapater.Fill(dbTbl)

        ' Get the index of the field name
        Dim i As Integer = dbTbl.Columns.IndexOf(fldName)

        If i = -1 Then
            'Field is missing
            DoesFieldExist = False
        Else
            'Field is there
            DoesFieldExist = True
        End If

        dbTbl.Dispose()
        dbConn.Close()
        dbConn.Dispose()
    End Function

    Public Function migrateTable(ByVal tableName As String, ByVal cnnStr As String, ByVal newcnnStr As String)
        Dim da1 As OleDb.OleDbDataAdapter, ds As DataSet, conn1 As OleDb.OleDbConnection
        conn1 = New OleDb.OleDbConnection
        conn1.ConnectionString = cnnStr
        ds = New DataSet
        da1 = New OleDb.OleDbDataAdapter
        da1.SelectCommand = New OleDb.OleDbCommand
        da1.SelectCommand.Connection = conn1
        da1.SelectCommand.CommandType = CommandType.Text
        da1.SelectCommand.CommandText = "Select * from " + tableName
        da1.FillSchema(ds, SchemaType.Mapped, tableName)
        da1.Fill(ds, tableName)

        ' MsgBox("About to die?")

        'dumping out data to show it's in the dataset
        Dim dr As DataRow
        Dim dt As DataTable

        dt = ds.Tables(tableName)


        Dim name(dt.Columns.Count) As String
        Dim i As Integer = 0
        For Each column As DataColumn In dt.Columns
            name(i) = column.ColumnName
            i = i + 1
        Next

        Dim numColumns = i
        Dim currentColumn As Integer
        Dim tempCounter As Integer

        '  MsgBox(tableName)
        Dim sqlInsert As String = "insert into " + tableName + " ("
        ' For currentColumn = 0 To numColumns - 2
        'sqlInsert = sqlInsert + name(currentColumn) + ","
        'tempCounter = currentColumn
        'Next
        'tempCounter = tempCounter + 1
        'sqlInsert = sqlInsert + name(tempCounter)
        'sqlInsert = sqlInsert + ") values ("

        Dim baseSQL As String = sqlInsert

        Dim leftSQL, rightSQL As String
        For Each dr In dt.Rows
            leftSQL = ""
            rightSQL = ""

            For currentColumn = 0 To numColumns - 1
                sqlInsert = sqlInsert + "'" + dr(currentColumn).ToString + "',"

                If dr(currentColumn).ToString <> "" Then
                    'we have data to insert
                    leftSQL = leftSQL + name(currentColumn) + ","
                    rightSQL = rightSQL + "'" + SqlSafe(dr(currentColumn)).ToString + "',"
                End If

            Next

            'remove last character from leftSQL and rightSQL
            leftSQL = leftSQL.Substring(0, leftSQL.Length - 1)
            rightSQL = rightSQL.Substring(0, rightSQL.Length - 1)
            'sqlInsert = sqlInsert + ")"

            runMigrateSQL(baseSQL + leftSQL + " ) values (" + rightSQL + ")", newcnnStr)

        Next

    End Function

    Public Function runMigrateSQL(ByVal sql As String, ByVal cnnStr As String) As Boolean


        Dim iniFile As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\dan.ini"
        FileOpen(1, iniFile, OpenMode.Output)
        WriteLine(1, sql)
        FileClose(1)


        ' Open connection to the database
        Dim dbConn As New OleDb.OleDbConnection
        Dim Command As New OleDb.OleDbCommand

        dbConn.ConnectionString = cnnStr
        dbConn.Open()

        Command.CommandText = sql
        Command.Connection = dbConn


        'now run the query
        Try
            Command.ExecuteNonQuery()
        Catch
            'do nowt, just ignore
        End Try
        dbConn.Close()

        Return True

    End Function

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBoxCurrentCase.Click
        spy("client list")
        clientlist.ShowDialog(Me)

'        CurrentCases.ShowDialog(Me)
    End Sub

    Private Sub PictureBox1_rollon(sender As Object, e As EventArgs) Handles PictureBoxCurrentCase.MouseEnter
        PictureBoxCurrentCase.BackgroundImage = My.Resources.button_current_cases_rollover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox1_rolloff(sender As Object, e As EventArgs) Handles PictureBoxCurrentCase.MouseLeave
        PictureBoxCurrentCase.BackgroundImage = My.Resources.button_current_cases
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        spy("new case button")
        clientrecord.ShowDialog(Me)
    End Sub
    Private Sub PictureBox2_rollon(sender As Object, e As EventArgs) Handles PictureBox2.MouseEnter
        PictureBox2.BackgroundImage = My.Resources.button_new_case_rollover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox2_rolloff(sender As Object, e As EventArgs) Handles PictureBox2.MouseLeave
        PictureBox2.BackgroundImage = My.Resources.button_new_case
        Me.Cursor = Cursors.Default
    End Sub



    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBoxSynchronize.Click
        synchronise.ShowDialog(Me)
    End Sub
    Private Sub PictureBox3_rollon(sender As Object, e As EventArgs) Handles PictureBoxSynchronize.MouseEnter
        PictureBoxSynchronize.BackgroundImage = My.Resources.button_synchronise_rollover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox3_rolloff(sender As Object, e As EventArgs) Handles PictureBoxSynchronize.MouseLeave
        PictureBoxSynchronize.BackgroundImage = My.Resources.button_synchronise
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBoxSettings.Click
        settings.ShowDialog(Me)
    End Sub

    Private Sub PictureBox4_rollon(sender As Object, e As EventArgs) Handles PictureBoxSettings.MouseEnter
        PictureBoxSettings.BackgroundImage = My.Resources.button_settings_rollover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox4_rolloff(sender As Object, e As EventArgs) Handles PictureBoxSettings.MouseLeave
        PictureBoxSettings.BackgroundImage = My.Resources.button_settings
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBoxTechnicalSupport.Click

        Try
            Dim username As String
            Dim sql As String

            sql = "select username from userdata"
            username = runSQLwithID(sql)

            Dim localFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
            Dim databaseSource As String = localFolder + "\" + baseDatabaseFilename
            Dim databaseSourceCopy As String = localFolder + "\" + Trim(username) + "-" + baseDatabaseFilename

            splashSending.StartPosition = FormStartPosition.CenterParent
            splashSending.Show()
            splashSending.Refresh()


            'do a file copy
            If System.IO.File.Exists(databaseSourceCopy) = True Then
                'need to delete file
                System.IO.File.Delete(databaseSourceCopy)
            End If
            System.IO.File.Copy(databaseSource, databaseSourceCopy)

            Dim remoteName = Trim(username) + "-" + baseDatabaseFilename

            Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(remoteDebugFTPAddress + remoteName), System.Net.FtpWebRequest)
            request.Credentials = New System.Net.NetworkCredential(ftpUsername, ftpPassword)
            request.Method = System.Net.WebRequestMethods.Ftp.UploadFile

                Dim file() As Byte = System.IO.File.ReadAllBytes(databaseSourceCopy)

                Dim strz As System.IO.Stream = request.GetRequestStream()
                strz.Write(file, 0, file.Length)
                strz.Close()
                strz.Dispose()
                splashSending.Close()

                MsgBox("Database Has Been Sent")
            Catch ex As Exception
            MsgBox("You are not connected to the internet" + vbCrLf + "Therefore, cannot send the database to Technical Support", vbOKOnly + vbCritical)
            splashSending.Close()
        End Try

    End Sub

    Private Sub PictureBox5_rollon(sender As Object, e As EventArgs) Handles PictureBoxTechnicalSupport.MouseEnter
        PictureBoxTechnicalSupport.BackgroundImage = My.Resources.button_techinical_support_rollover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox5_rolloff(sender As Object, e As EventArgs) Handles PictureBoxTechnicalSupport.MouseLeave
        PictureBoxTechnicalSupport.BackgroundImage = My.Resources.button_technical_support
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        spy("close")
        End

    End Sub

    Private Sub PictureBox6_rollon(sender As Object, e As EventArgs) Handles PictureBox6.MouseEnter
        PictureBox6.BackgroundImage = My.Resources.button_quit_rollover
        Me.Cursor = Cursors.Hand
    End Sub

    Private Sub PictureBox6_rolloff(sender As Object, e As EventArgs) Handles PictureBox6.MouseLeave
        PictureBox6.BackgroundImage = My.Resources.button_quit
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Button1_Click_2(sender As Object, e As EventArgs)
        getNewCases()
    End Sub

    Private Sub Form1_LocationChanged(sender As Object, e As EventArgs) Handles Me.LocationChanged

    End Sub
End Class
