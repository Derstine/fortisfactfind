﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxTechnicalSupport = New System.Windows.Forms.PictureBox()
        Me.PictureBoxSettings = New System.Windows.Forms.PictureBox()
        Me.PictureBoxSynchronize = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCurrentCase = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxTechnicalSupport,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxSettings,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxSynchronize,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBox2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxCurrentCase,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.BackgroundImage = Global.Fortis_Fact_Find.My.Resources.Resources.button_quit
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PictureBox6.Location = New System.Drawing.Point(1010, 597)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(212, 52)
        Me.PictureBox6.TabIndex = 12
        Me.PictureBox6.TabStop = false
        '
        'PictureBoxTechnicalSupport
        '
        Me.PictureBoxTechnicalSupport.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxTechnicalSupport.BackgroundImage = Global.Fortis_Fact_Find.My.Resources.Resources.button_technical_support
        Me.PictureBoxTechnicalSupport.Location = New System.Drawing.Point(29, 530)
        Me.PictureBoxTechnicalSupport.Name = "PictureBoxTechnicalSupport"
        Me.PictureBoxTechnicalSupport.Size = New System.Drawing.Size(804, 52)
        Me.PictureBoxTechnicalSupport.TabIndex = 11
        Me.PictureBoxTechnicalSupport.TabStop = false
        '
        'PictureBoxSettings
        '
        Me.PictureBoxSettings.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxSettings.BackgroundImage = Global.Fortis_Fact_Find.My.Resources.Resources.button_settings
        Me.PictureBoxSettings.Location = New System.Drawing.Point(29, 472)
        Me.PictureBoxSettings.Name = "PictureBoxSettings"
        Me.PictureBoxSettings.Size = New System.Drawing.Size(804, 52)
        Me.PictureBoxSettings.TabIndex = 10
        Me.PictureBoxSettings.TabStop = false
        '
        'PictureBoxSynchronize
        '
        Me.PictureBoxSynchronize.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxSynchronize.BackgroundImage = Global.Fortis_Fact_Find.My.Resources.Resources.button_synchronise
        Me.PictureBoxSynchronize.Enabled = false
        Me.PictureBoxSynchronize.Location = New System.Drawing.Point(29, 179)
        Me.PictureBoxSynchronize.Name = "PictureBoxSynchronize"
        Me.PictureBoxSynchronize.Size = New System.Drawing.Size(804, 52)
        Me.PictureBoxSynchronize.TabIndex = 9
        Me.PictureBoxSynchronize.TabStop = false
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"),System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(1010, 509)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(96, 52)
        Me.PictureBox2.TabIndex = 8
        Me.PictureBox2.TabStop = false
        Me.PictureBox2.Visible = false
        '
        'PictureBoxCurrentCase
        '
        Me.PictureBoxCurrentCase.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxCurrentCase.BackgroundImage = Global.Fortis_Fact_Find.My.Resources.Resources.button_current_cases
        Me.PictureBoxCurrentCase.Location = New System.Drawing.Point(29, 121)
        Me.PictureBoxCurrentCase.Name = "PictureBoxCurrentCase"
        Me.PictureBoxCurrentCase.Size = New System.Drawing.Size(804, 52)
        Me.PictureBoxCurrentCase.TabIndex = 7
        Me.PictureBoxCurrentCase.TabStop = false
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221,Byte),Integer), CType(CType(244,Byte),Integer), CType(CType(250,Byte),Integer))
        Me.BackgroundImage = Global.Fortis_Fact_Find.My.Resources.Resources.background1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBoxTechnicalSupport)
        Me.Controls.Add(Me.PictureBoxSettings)
        Me.Controls.Add(Me.PictureBoxSynchronize)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBoxCurrentCase)
        Me.MaximizeBox = false
        Me.MaximumSize = New System.Drawing.Size(1250, 700)
        Me.Name = "MainForm"
        Me.Text = "Fortis Law Fact Find v1.0.0.a                                       Technical Sup"& _ 
    "port (available 8am - 8pm) 07854 662 811"
        CType(Me.PictureBox6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxTechnicalSupport,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxSettings,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxSynchronize,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBox2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxCurrentCase,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents PictureBoxCurrentCase As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBoxSynchronize As PictureBox
    Friend WithEvents PictureBoxSettings As PictureBox
    Friend WithEvents PictureBoxTechnicalSupport As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
End Class
