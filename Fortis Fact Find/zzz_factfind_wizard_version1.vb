﻿Imports System.Threading
Imports System.Globalization
Public Class zzz_factfind_wizard_version1
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

    End Sub

    Private Function setDefaults()

        GroupBox2.Visible = False     'products and services
        RadioButton2.Checked = True
        RadioButton3.Checked = True
        RadioButton5.Checked = True
        RadioButton7.Checked = True
        RadioButton9.Checked = True
        RadioButton11.Checked = True
        RadioButton13.Checked = True
        RadioButton15.Checked = True
        RadioButton17.Checked = True
        RadioButton19.Checked = True

        RadioButton2.Checked = True
        RadioButton3.Checked = True
        RadioButton5.Checked = True
        RadioButton7.Checked = True
        RadioButton9.Checked = True
        RadioButton11.Checked = True
        RadioButton13.Checked = True
        RadioButton15.Checked = True
        RadioButton17.Checked = True
        RadioButton19.Checked = True
        RadioButton41.Checked = True
        RadioButton43.Checked = True


        RadioButton21.Checked = True
        RadioButton23.Checked = True
        RadioButton25.Checked = True
        RadioButton27.Checked = True
        RadioButton29.Checked = True
        RadioButton31.Checked = True
        RadioButton33.Checked = True
        RadioButton35.Checked = True
        RadioButton37.Checked = True
        RadioButton39.Checked = True
        RadioButton45.Checked = True

        DataGridView2.Rows.Clear()    'assets tab

        DataGridView1.Rows.Clear()    'client offer

        CheckBox1.Checked = False
        CheckBox2.Checked = False
        CheckBox3.Checked = False
        CheckBox4.Checked = False
        CheckBox5.Checked = False
        CheckBox6.Checked = False

        CheckBox9.Checked = False
        CheckBox10.Checked = False
        CheckBox11.Checked = False
        CheckBox12.Checked = False
        CheckBox13.Checked = False
        CheckBox14.Checked = False


        Return True
    End Function

    Private Function populateScreen()

        Dim sql As String = ""
        Dim results As Array

        'make the label names user-friendly by putting client name in everywhere
        Dim client1Name As String = getClientFirstName("client1")
        Dim client2Name As String = getClientFirstName("client2")
        GroupBox1.Text = client1Name
        GroupBox3.Text = client1Name
        GroupBox8.Text = client1Name
        GroupBox2.Text = client2Name
        GroupBox4.Text = client2Name
        GroupBox7.Text = client2Name
        Label50.Text = client1Name + "'s Allowance"
        Label51.Text = client2Name + "'s Allowance"
        DataGridView2.Columns(3).HeaderText = client1Name

        If clientType() = "single" Then     'show/hide the appropriate panels/group boxes
            GroupBox2.Visible = False
            GroupBox4.Visible = False
            GroupBox7.Visible = False
            TextBox9.Visible = False
            TextBox10.Visible = False
            TextBox74.Visible = False
            Label76.Visible = False
            TextBox5.Visible = False
            TextBox2.Visible = False
            TextBox7.Visible = False
            TextBox3.Visible = False
            TextBox4.Visible = False
            TextBox44.Visible = False
            Label51.Visible = False

            DataGridView2.Columns(4).HeaderText = "n/a"
            DataGridView2.Columns(5).HeaderText = "n/a"

        Else
            GroupBox2.Visible = True
            GroupBox4.Visible = True
            GroupBox7.Visible = True
            TextBox9.Visible = True
            TextBox10.Visible = True
            TextBox74.Visible = True
            Label76.Visible = True
            TextBox5.Visible = True
            TextBox2.Visible = True
            TextBox7.Visible = True
            TextBox3.Visible = True
            TextBox4.Visible = True
            TextBox44.Visible = True
            Label51.Visible = True

            DataGridView2.Columns(4).HeaderText = client2Name
            DataGridView2.Columns(5).HeaderText = "Joint"

        End If

        'populate Products & Services Tab
        'client 1
        sql = "select * from client_products where willClientSequence=" + clientID + " and whichClient='client1'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) = "Y" Then RadioButton1.Checked = True Else RadioButton2.Checked = True
            If results(4) = "Y" Then RadioButton4.Checked = True Else RadioButton3.Checked = True
            If results(5) = "Y" Then RadioButton6.Checked = True Else RadioButton5.Checked = True
            If results(6) = "Y" Then RadioButton8.Checked = True Else RadioButton7.Checked = True
            If results(7) = "Y" Then RadioButton10.Checked = True Else RadioButton9.Checked = True
            If results(8) = "Y" Then RadioButton12.Checked = True Else RadioButton11.Checked = True
            If results(9) = "Y" Then RadioButton18.Checked = True Else RadioButton17.Checked = True
            If results(10) = "Y" Then RadioButton14.Checked = True Else RadioButton13.Checked = True
            If results(11) = "Y" Then RadioButton16.Checked = True Else RadioButton15.Checked = True
            If results(12) = "Y" Then RadioButton42.Checked = True Else RadioButton41.Checked = True
            If results(13) = "Y" Then RadioButton44.Checked = True Else RadioButton43.Checked = True
        End If

        'client 2
        sql = "select * from client_products where willClientSequence=" + clientID + " and whichClient='client2'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) = "Y" Then RadioButton40.Checked = True Else RadioButton39.Checked = True
            If results(4) = "Y" Then RadioButton38.Checked = True Else RadioButton37.Checked = True
            If results(5) = "Y" Then RadioButton36.Checked = True Else RadioButton35.Checked = True
            If results(6) = "Y" Then RadioButton34.Checked = True Else RadioButton33.Checked = True
            If results(7) = "Y" Then RadioButton32.Checked = True Else RadioButton31.Checked = True
            If results(8) = "Y" Then RadioButton30.Checked = True Else RadioButton29.Checked = True
            If results(9) = "Y" Then RadioButton28.Checked = True Else RadioButton27.Checked = True
            If results(10) = "Y" Then RadioButton26.Checked = True Else RadioButton25.Checked = True
            If results(11) = "Y" Then RadioButton24.Checked = True Else RadioButton23.Checked = True
            If results(12) = "Y" Then RadioButton22.Checked = True Else RadioButton21.Checked = True
            If results(13) = "Y" Then RadioButton46.Checked = True Else RadioButton45.Checked = True
        End If


        'populate assets tab
        redrawAssetsGrid()
        'client 1
        sql = "select * from client_assets where willClientSequence=" + clientID + " and whichClient='client1'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox6.Text = results(3)
            If results(4) <> "0" Then TextBox1.Text = results(4)
            If results(5) <> "" Then TextBox8.Text = results(5)
        End If
        'client 2
        sql = "select * from client_assets where willClientSequence=" + clientID + " and whichClient='client2'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox5.Text = results(3)
            If results(4) <> "0" Then TextBox2.Text = results(4)
            If results(5) <> "" Then TextBox7.Text = results(5)
        End If

        'client offer tab
        'client 1
        sql = "select * from client_offer where willClientSequence=" + clientID + " and whichClient='client1'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox12.Text = results(3)
            If results(5) <> "0" Then TextBox13.Text = results(5)
            If results(7) <> "0" Then TextBox14.Text = results(7)
            If results(9) <> "0" Then TextBox15.Text = results(9)
            If results(11) <> "0" Then TextBox16.Text = results(11)
            If results(13) <> "0" Then TextBox17.Text = results(13)

            If results(4) = "Y" Then CheckBox1.Checked = True Else CheckBox1.Checked = False
            If results(6) = "Y" Then CheckBox2.Checked = True Else CheckBox2.Checked = False
            If results(8) = "Y" Then CheckBox3.Checked = True Else CheckBox3.Checked = False
            If results(10) = "Y" Then CheckBox4.Checked = True Else CheckBox4.Checked = False
            If results(12) = "Y" Then CheckBox5.Checked = True Else CheckBox5.Checked = False
            If results(14) = "Y" Then CheckBox6.Checked = True Else CheckBox6.Checked = False
        End If

        'client 2
        sql = "select * from client_offer where willClientSequence=" + clientID + " and whichClient='client2'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox26.Text = results(3)
            If results(5) <> "0" Then TextBox25.Text = results(5)
            If results(7) <> "0" Then TextBox24.Text = results(7)
            If results(9) <> "0" Then TextBox23.Text = results(9)
            If results(11) <> "0" Then TextBox22.Text = results(11)
            If results(13) <> "0" Then TextBox21.Text = results(13)

            If results(4) = "Y" Then CheckBox14.Checked = True Else CheckBox14.Checked = False
            If results(6) = "Y" Then CheckBox13.Checked = True Else CheckBox13.Checked = False
            If results(8) = "Y" Then CheckBox12.Checked = True Else CheckBox12.Checked = False
            If results(10) = "Y" Then CheckBox11.Checked = True Else CheckBox11.Checked = False
            If results(12) = "Y" Then CheckBox10.Checked = True Else CheckBox10.Checked = False
            If results(14) = "Y" Then CheckBox9.Checked = True Else CheckBox9.Checked = False
        End If
        'returns data
        sql = "select * from client_returns where willClientSequence=" + clientID
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            TextBox32.Text = results(2)
            TextBox31.Text = results(3)
            TextBox29.Text = results(4)
        End If
        recalculateClientTab()

        'populate probate tab
        'client1
        Dim dob As Date
        sql = "select person1DOB from will_instruction_stage2_data where willClientSequence=" + clientID
        dob = runSQLwithID(sql)
        Dim age As Integer
        age = Today.Year - dob.Year
        If (dob > Today.AddYears(-age)) Then age -= 1
        TextBox41.Text = age.ToString("F0")

        If clientType() = "mirror" Then
            sql = "select person2DOB from will_instruction_stage2_data where willClientSequence=" + clientID
            dob = runSQLwithID(sql)

            age = Today.Year - dob.Year
            If (dob > Today.AddYears(-age)) Then age -= 1
            TextBox42.Text = age.ToString("F0")
        End If

        If TextBox42.Text = "" Or TextBox42.Text = "0" Then
            TextBox75.Text = TextBox41.Text
        Else
            TextBox75.Text = ((Val(TextBox41.Text) + Val(TextBox42.Text)) / 2).ToString("F0")
        End If

        TextBox76.Text = (82 - Val(TextBox75.Text)).ToString("F0")
        TextBox35.Text = TextBox76.Text
        TextBox47.Text = TextBox76.Text

        'probate section
        TextBox34.Text = "2.50" 'sets default inflation rate
        recalculateProbate()

        'populate funeral tab
        'client 1
        sql = "select * from client_funeral where willClientSequence=" + clientID + " and whichClient='client1'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox43.Text = results(3)
        End If
        'client 1
        sql = "select * from client_funeral where willClientSequence=" + clientID + " and whichClient='client2'"
        results = runSQLwithArray(sql)
        If results(0) = "" Or results(0) = "ERROR" Then
            'ignore, as no existing client data
        Else
            If results(3) <> "0" Then TextBox44.Text = results(3)
        End If
        recalculateFuneralCost()

        Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        saveData()
        Me.Close()
    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub TextBox31_TextChanged(sender As Object, e As EventArgs) Handles TextBox31.TextChanged
        TextBox30.Text = (Val(TextBox19.Text) - Val(TextBox31.Text)).ToString
        recalculateLoanAmount()
    End Sub

    Private Sub TextBox29_TextChanged(sender As Object, e As EventArgs) Handles TextBox29.TextChanged
        recalculateLoanAmount()
        recalculateClientTab()
    End Sub

    Private Sub TextBox30_TextChanged(sender As Object, e As EventArgs) Handles TextBox30.TextChanged
        recalculateLoanAmount()
        recalculateClientTab()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        recalculateClientTab()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        recalculateProbate()

    End Sub

    Private Sub TextBox43_TextChanged(sender As Object, e As EventArgs) Handles TextBox43.TextChanged
        TextBox45.Text = (Val(TextBox43.Text) + Val(TextBox44.Text)).ToString("F0")

    End Sub

    Private Sub TextBox44_TextChanged(sender As Object, e As EventArgs) Handles TextBox44.TextChanged
        TextBox45.Text = (Val(TextBox43.Text) + Val(TextBox44.Text)).ToString("F0")

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        recalculateFuneralCost()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        assets_add.ShowDialog(Me)
        redrawAssetsGrid()
    End Sub

    Function redrawAssetsGrid()

        DataGridView2.Rows.Clear()

        Dim sql As String = ""
        Dim client1 As Single = 0
        Dim client2 As Single = 0
        Dim joint As Single = 0

        Dim assettype As String = ""
        Dim details As String = ""
        Dim client1Value As String = ""
        Dim client2Value As String = ""
        Dim jointValue As String = ""


        Dim dbConn As New OleDb.OleDbConnection
        Dim command As New OleDb.OleDbCommand
        Dim dbreader As OleDb.OleDbDataReader
        dbConn.ConnectionString = connectionString
        dbConn.Open()
        command.Connection = dbConn

        'let's do property first
        sql = "select * from fapt_trust_fund_property where willClientSequence=" + clientID
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            assettype = "Property"
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(11).ToString
            If dbreader.GetValue(9).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(9).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(9).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "", "", "", "", "edit")
        End While
        dbreader.Close()

        'let's do chattels next
        sql = "select * from fapt_trust_fund_chattels where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader
        While dbreader.Read()
            assettype = dbreader.GetValue(5).ToString
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(2).ToString
            If dbreader.GetValue(4).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "", "", "", "", "edit")
        End While

        dbreader.Close()

        ' let's do cash next
        sql = "select * from fapt_trust_fund_cash where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader

        While dbreader.Read()
            assettype = dbreader.GetValue(5).ToString
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(2).ToString
            If dbreader.GetValue(4).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "", "", "", "", "edit")
        End While
        dbreader.Close()

        ' finally, let's do "misc" next
        sql = "select * from fapt_trust_fund_further where willClientSequence=" + clientID + " order by assetCategory ASC"
        command.CommandText = sql
        dbreader = command.ExecuteReader

        While dbreader.Read()
            assettype = dbreader.GetValue(5).ToString
            details = ""
            client1Value = ""
            client2Value = ""
            jointValue = ""
            details = dbreader.GetValue(2).ToString
            If dbreader.GetValue(4).ToString = "client1" Then client1Value = dbreader.GetValue(3).ToString : client1 = client1 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "client2" Then client2Value = dbreader.GetValue(3).ToString : client2 = client2 + Val(dbreader.GetValue(3).ToString)
            If dbreader.GetValue(4).ToString = "both" Then jointValue = dbreader.GetValue(3).ToString : joint = joint + Val(dbreader.GetValue(3).ToString)
            'add row       
            DataGridView2.Rows.Add(dbreader.GetValue(0).ToString, assettype, details, client1Value, client2Value, jointValue, "", "", "", "", "edit")
        End While
        dbreader.Close()

        TextBox11.Text = client1.ToString("F0")
        TextBox10.Text = client2.ToString("F0")
        TextBox9.Text = joint.ToString("F0")
        TextBox74.Text = (client1 + client2 + joint).ToString("F0")
        dbConn.Close()
        Return True

    End Function

    Function getTotalPropertyValue()
        Dim returnval As String = ""
        Dim sql As String = ""

        sql = "select SUM(worth) from fapt_trust_fund_property where willClientSequence=" + clientID
        returnval = Val(runSQLwithID(sql)).ToString("F0")

        Return returnval
    End Function

    Private Sub TextBox6_TextChanged(sender As Object, e As EventArgs) Handles TextBox6.TextChanged
        TextBox4.Text = (Val(TextBox6.Text) + Val(TextBox5.Text)).ToString
    End Sub

    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TextBox5.TextChanged
        TextBox4.Text = (Val(TextBox6.Text) + Val(TextBox5.Text)).ToString
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        TextBox3.Text = (Val(TextBox1.Text) + Val(TextBox2.Text)).ToString
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        TextBox3.Text = (Val(TextBox1.Text) + Val(TextBox2.Text)).ToString
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged

        Dim indexOfSelectedTab As Integer = TabControl1.SelectedIndex

        If indexOfSelectedTab = 5 Then
            'need to recalculate savings page as the user has just clicked on this tab
            'MsgBox("recalc")
        End If

    End Sub

    Private Sub TextBox12_TextChanged(sender As Object, e As EventArgs) Handles TextBox12.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox13_TextChanged(sender As Object, e As EventArgs) Handles TextBox13.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox14_TextChanged(sender As Object, e As EventArgs) Handles TextBox14.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox15_TextChanged(sender As Object, e As EventArgs) Handles TextBox15.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox16_TextChanged(sender As Object, e As EventArgs) Handles TextBox16.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox17_TextChanged(sender As Object, e As EventArgs) Handles TextBox17.TextChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox4_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox4.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox5_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox5.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox6_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox6.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox26_TextChanged(sender As Object, e As EventArgs) Handles TextBox26.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox25_TextChanged(sender As Object, e As EventArgs) Handles TextBox25.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox24_TextChanged(sender As Object, e As EventArgs) Handles TextBox24.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox23_TextChanged(sender As Object, e As EventArgs) Handles TextBox23.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox22_TextChanged(sender As Object, e As EventArgs) Handles TextBox22.TextChanged
        recalculateOffer()
    End Sub

    Private Sub TextBox21_TextChanged(sender As Object, e As EventArgs) Handles TextBox21.TextChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox14_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox14.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox13_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox13.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox12_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox12.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox11_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox11.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox10_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox10.CheckedChanged
        recalculateOffer()
    End Sub

    Private Sub CheckBox9_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox9.CheckedChanged
        recalculateOffer()
    End Sub

    Function saveData()
        Dim sql As String

        '''''''''''''
        '''''''''''''
        ''' NOTE: Because this screen only has a 1-to-1 relationship with the database, I'm going to be lazy and just delete the existing row and then insert a new one
        ''' rather than determining between an INSERT and an UPDATE
        ''''''''''''' 
        '''''''''''''

        'ASSETS Table First
        'step 1 - delete
        sql = "delete from client_assets where willClientSequence=" + clientID
        runSQL(sql)
        'step 2 - insert client 1
        sql = "insert into client_assets (willClientSequence,whichClient,grossIncome,emergencyAmount,feelings) values ('" + clientID + "','client1'," + Val(SqlSafe(TextBox6.Text)).ToString + "," + Val(SqlSafe(TextBox1.Text)).ToString + ",'" + SqlSafe(TextBox8.Text) + "')"
        runSQL(sql)
        'step 3 - insert client 2
        sql = "insert into client_assets (willClientSequence,whichClient,grossIncome,emergencyAmount,feelings) values ('" + clientID + "','client2','" + Val(SqlSafe(TextBox5.Text)).ToString + "','" + Val(SqlSafe(TextBox2.Text)).ToString + "','" + SqlSafe(TextBox7.Text) + "')"
        runSQL(sql)

        'FUNERAL table next
        'step 1 - delete
        sql = "delete from client_funeral where willClientSequence=" + clientID
        runSQL(sql)
        'step 2 - insert client 1
        sql = "insert into client_funeral (willClientSequence,whichClient,funeralAllowance) values ('" + clientID + "','client1'," + Val(SqlSafe(TextBox43.Text)).ToString + ")"
        runSQL(sql)
        'step 3 - insert client 2
        sql = "insert into client_funeral (willClientSequence,whichClient,funeralAllowance) values ('" + clientID + "','client2'," + Val(SqlSafe(TextBox44.Text)).ToString + ")"
        runSQL(sql)

        'OFFER table next
        'step 1 - delete
        sql = "delete from client_offer where willClientSequence=" + clientID
        runSQL(sql)
        'step 2 - need to do some encoding of values here :-(
        Dim willsFlag, HWLPAFlag, PFLPAFlag, TrustsFlag, FuneralPlanFlag, ProbateFlag As String
        If CheckBox1.Checked = True Then willsFlag = "Y" Else willsFlag = "N"
        If CheckBox2.Checked = True Then HWLPAFlag = "Y" Else HWLPAFlag = "N"
        If CheckBox3.Checked = True Then PFLPAFlag = "Y" Else PFLPAFlag = "N"
        If CheckBox4.Checked = True Then TrustsFlag = "Y" Else TrustsFlag = "N"
        If CheckBox5.Checked = True Then FuneralPlanFlag = "Y" Else FuneralPlanFlag = "N"
        If CheckBox6.Checked = True Then ProbateFlag = "Y" Else ProbateFlag = "N"
        'step 3 - insert client 1
        sql = "insert into client_offer (willClientSequence,whichClient,willsPrice,HWLPAPrice,PFLPAPrice,TrustsPrice,FuneralPlanPrice,ProbatePrice,willsFlag,HWLPAFlag,PFLPAFlag,TrustsFlag,FuneralPlanFlag,ProbateFlag) values ('" + clientID + "','client1'," + Val(SqlSafe(TextBox12.Text)).ToString + "," + Val(SqlSafe(TextBox13.Text)).ToString + "," + Val(SqlSafe(TextBox14.Text)).ToString + "," + Val(SqlSafe(TextBox15.Text)).ToString + "," + Val(SqlSafe(TextBox16.Text)).ToString + "," + Val(SqlSafe(TextBox17.Text)).ToString + ",'" + willsFlag + "','" + HWLPAFlag + "','" + PFLPAFlag + "','" + TrustsFlag + "','" + FuneralPlanFlag + "','" + ProbateFlag + "')"
        runSQL(sql)
        'step 4 - need to do some encoding of values here :-(
        If CheckBox14.Checked = True Then willsFlag = "Y" Else willsFlag = "N"
        If CheckBox13.Checked = True Then HWLPAFlag = "Y" Else HWLPAFlag = "N"
        If CheckBox12.Checked = True Then PFLPAFlag = "Y" Else PFLPAFlag = "N"
        If CheckBox11.Checked = True Then TrustsFlag = "Y" Else TrustsFlag = "N"
        If CheckBox10.Checked = True Then FuneralPlanFlag = "Y" Else FuneralPlanFlag = "N"
        If CheckBox9.Checked = True Then ProbateFlag = "Y" Else ProbateFlag = "N"
        'step 5 - insert client 2
        sql = "insert into client_offer (willClientSequence,whichClient,willsPrice,HWLPAPrice,PFLPAPrice,TrustsPrice,FuneralPlanPrice,ProbatePrice,willsFlag,HWLPAFlag,PFLPAFlag,TrustsFlag,FuneralPlanFlag,ProbateFlag) values ('" + clientID + "','client2'," + Val(SqlSafe(TextBox26.Text)).ToString + "," + Val(SqlSafe(TextBox25.Text)).ToString + "," + Val(SqlSafe(TextBox24.Text)).ToString + "," + Val(SqlSafe(TextBox23.Text)).ToString + "," + Val(SqlSafe(TextBox22.Text)).ToString + "," + Val(SqlSafe(TextBox21.Text)).ToString + ",'" + willsFlag + "','" + HWLPAFlag + "','" + PFLPAFlag + "','" + TrustsFlag + "','" + FuneralPlanFlag + "','" + ProbateFlag + "')"
        runSQL(sql)

        'PRODUCTS table next
        'step 1 - delete
        sql = "delete from client_products where willClientSequence=" + clientID
        runSQL(sql)
        'step 2 - need to do some encoding of values here :-(
        Dim q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11 As String
        If RadioButton1.Checked = True Then q1 = "Y" Else q1 = "N"
        If RadioButton4.Checked = True Then q2 = "Y" Else q2 = "N"
        If RadioButton6.Checked = True Then q3 = "Y" Else q3 = "N"
        If RadioButton8.Checked = True Then q4 = "Y" Else q4 = "N"
        If RadioButton10.Checked = True Then q5 = "Y" Else q5 = "N"
        If RadioButton12.Checked = True Then q6 = "Y" Else q6 = "N"
        If RadioButton18.Checked = True Then q7 = "Y" Else q7 = "N"
        If RadioButton14.Checked = True Then q8 = "Y" Else q8 = "N"
        If RadioButton16.Checked = True Then q9 = "Y" Else q9 = "N"
        If RadioButton42.Checked = True Then q10 = "Y" Else q10 = "N"
        If RadioButton44.Checked = True Then q11 = "Y" Else q11 = "N"

        'step 3 - insert client 1
        sql = "insert into client_products (willClientSequence,whichClient, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11) values ('" + clientID + "','client1', '" + q1 + "','" + q2 + "','" + q3 + "','" + q4 + "','" + q5 + "','" + q6 + "','" + q7 + "','" + q8 + "','" + q9 + "','" + q10 + "','" + q11 + "')"
        runSQL(sql)
        'step 4 - need to do some encoding of values here :-(
        If RadioButton40.Checked = True Then q1 = "Y" Else q1 = "N"
        If RadioButton38.Checked = True Then q2 = "Y" Else q2 = "N"
        If RadioButton36.Checked = True Then q3 = "Y" Else q3 = "N"
        If RadioButton34.Checked = True Then q4 = "Y" Else q4 = "N"
        If RadioButton32.Checked = True Then q5 = "Y" Else q5 = "N"
        If RadioButton30.Checked = True Then q6 = "Y" Else q6 = "N"
        If RadioButton28.Checked = True Then q7 = "Y" Else q7 = "N"
        If RadioButton26.Checked = True Then q8 = "Y" Else q8 = "N"
        If RadioButton24.Checked = True Then q9 = "Y" Else q9 = "N"
        If RadioButton22.Checked = True Then q10 = "Y" Else q10 = "N"
        If RadioButton46.Checked = True Then q11 = "Y" Else q11 = "N"
        'step 5 - insert client 2
        sql = "insert into client_products (willClientSequence,whichClient, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11) values ('" + clientID + "','client2', '" + q1 + "','" + q2 + "','" + q3 + "','" + q4 + "','" + q5 + "','" + q6 + "','" + q7 + "','" + q8 + "','" + q9 + "','" + q10 + "','" + q11 + "')"
        runSQL(sql)

        'RETURNS table next
        'step 1 - delete
        sql = "delete from client_returns where willClientSequence=" + clientID
        runSQL(sql)
        'step 2 - insert data
        sql = "insert into client_returns (willClientSequence,investment,deposit,months) values ('" + clientID + "'," + Val(SqlSafe(TextBox32.Text)).ToString + "," + Val(SqlSafe(TextBox31.Text)).ToString + "," + Val(SqlSafe(TextBox29.Text)).ToString + ")"
        runSQL(sql)

        Return True
    End Function

    '''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''
    ''' RECALCULATION FUNCTIONS '''
    '''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''

    Function recalculateLoanAmount()

        Dim returnVal As String = ""

        'make sure we've got values in the boxes first, to avoid an error
        If TextBox29.Text <> "" And TextBox30.Text <> "" And TextBox29.Text <> "0" And TextBox30.Text <> "0" Then
            Dim payment As Single
            payment = (Pmt((0.08 / 12), TextBox29.Text, TextBox30.Text)) * -1
            returnVal = payment.ToString("F0")
        End If

        TextBox28.Text = returnVal
        Return True

    End Function

    Function recalculateClientTab()
        DataGridView1.Rows.Clear()

        'make sure we have all the data before bothering
        If TextBox29.Text = "0" Or TextBox29.Text = "" Or TextBox30.Text = "0" Or TextBox30.Text = "" Or TextBox32.Text = "0" Or TextBox32.Text = "" Then
            'can't do the calculation as there are zero's
        Else


            'this will run for the number of years that the joint clients are expected to live. Assumption is death at 82
            'hard coded example is for 12 years, just to tally up with the demo spreadsheet

            Dim numYears As Int16 = Val(TextBox76.Text)
            Dim netReturn As Single
            Dim investment As Single = Val(TextBox32.Text)
            Dim loanAmount As Single = Val(TextBox28.Text)

            Dim loopInvested As Single = investment   'initialise variable
            Dim loopInterest As Single
            Dim loopAMC As Single
            Dim loopLoan As Single = Val(TextBox28.Text)
            Dim repaymentTerm = TextBox29.Text
            Dim loopRepaymentCount As Int16 = 0
            Dim loopValue As Single
            Dim loopLoanPaid As String = ""

            Dim loopInterestRate As Single = 6 / 100 / 12
            Dim loopAMCRate As Single = 1.2 / 100 / 12


            For x = 1 To (numYears * 12)

                loopLoanPaid = ""
                loopInterest = loopInvested * loopInterestRate
                loopAMC = loopInvested * loopAMCRate
                loopValue = loopInvested + loopInterest - loopAMC

                loopRepaymentCount = loopRepaymentCount + 1
                If loopRepaymentCount <= repaymentTerm Then
                    'need to remove loan repayment amount
                    loopValue = loopValue - loopLoan
                    loopLoanPaid = TextBox28.Text
                End If

                'add row
                DataGridView1.Rows.Add(x.ToString, loopInvested.ToString("F0"), loopInterest.ToString("F0"), loopAMC.ToString("F0"), loopLoanPaid, loopValue.ToString("F0"))
                'next row initialisation calculations
                loopInvested = loopValue
            Next

            Dim netInterest As Single = loopInvested - investment
            netReturn = ((netInterest / investment) / numYears) * 100
            TextBox27.Text = netReturn.ToString("F0") + "%"
            TextBox53.Text = netInterest.ToString("F0")
            TextBox54.Text = TextBox32.Text
            TextBox55.Text = getTotalPropertyValue()
            TextBox56.Text = (Val(TextBox54.Text) + Val(TextBox55.Text)).ToString("F0")

        End If

        Return True
    End Function

    Function recalculateOffer()
        Dim client1Subtotal As Integer = 0
        Dim client2Subtotal As Integer = 0
        If CheckBox1.Checked = True Then client1Subtotal = client1Subtotal + Val(TextBox12.Text)
        If CheckBox2.Checked = True Then client1Subtotal = client1Subtotal + Val(TextBox13.Text)
        If CheckBox3.Checked = True Then client1Subtotal = client1Subtotal + Val(TextBox14.Text)
        If CheckBox4.Checked = True Then client1Subtotal = client1Subtotal + Val(TextBox15.Text)
        If CheckBox5.Checked = True Then client1Subtotal = client1Subtotal + Val(TextBox16.Text)
        If CheckBox6.Checked = True Then client1Subtotal = client1Subtotal + Val(TextBox17.Text)

        If CheckBox14.Checked = True Then client2Subtotal = client2Subtotal + Val(TextBox26.Text)
        If CheckBox13.Checked = True Then client2Subtotal = client2Subtotal + Val(TextBox25.Text)
        If CheckBox12.Checked = True Then client2Subtotal = client2Subtotal + Val(TextBox24.Text)
        If CheckBox11.Checked = True Then client2Subtotal = client2Subtotal + Val(TextBox23.Text)
        If CheckBox10.Checked = True Then client2Subtotal = client2Subtotal + Val(TextBox22.Text)
        If CheckBox9.Checked = True Then client2Subtotal = client2Subtotal + Val(TextBox21.Text)

        TextBox18.Text = client1Subtotal.ToString("F0")
        TextBox20.Text = client2Subtotal.ToString("F0")
        TextBox19.Text = (client1Subtotal + client2Subtotal).ToString("F0")

        'copy info over to summary tab
        If CheckBox1.Checked = True Then TextBox73.Text = TextBox12.Text Else TextBox73.Text = ""
        If CheckBox2.Checked = True Then TextBox72.Text = TextBox13.Text Else TextBox72.Text = ""
        If CheckBox3.Checked = True Then TextBox71.Text = TextBox14.Text Else TextBox71.Text = ""
        If CheckBox4.Checked = True Then TextBox70.Text = TextBox15.Text Else TextBox70.Text = ""
        If CheckBox5.Checked = True Then TextBox69.Text = TextBox16.Text Else TextBox69.Text = ""
        If CheckBox6.Checked = True Then TextBox68.Text = TextBox17.Text Else TextBox68.Text = ""

        If CheckBox14.Checked = True Then TextBox65.Text = TextBox26.Text Else TextBox65.Text = ""
        If CheckBox13.Checked = True Then TextBox64.Text = TextBox25.Text Else TextBox64.Text = ""
        If CheckBox12.Checked = True Then TextBox63.Text = TextBox24.Text Else TextBox63.Text = ""
        If CheckBox11.Checked = True Then TextBox62.Text = TextBox23.Text Else TextBox62.Text = ""
        If CheckBox10.Checked = True Then TextBox61.Text = TextBox22.Text Else TextBox61.Text = ""
        If CheckBox9.Checked = True Then TextBox60.Text = TextBox21.Text Else TextBox60.Text = ""

        TextBox67.Text = client1Subtotal.ToString("F0")
        TextBox59.Text = client2Subtotal.ToString("F0")
        TextBox66.Text = (client1Subtotal + client2Subtotal).ToString("F0")

        Return True
    End Function

    Function recalculateProbate()
        Dim futurevalue As Single
        Dim principal_amount As Single = Val(TextBox33.Text)
        Dim inflation_rate As Double = Val(TextBox34.Text) / 100
        For i As Integer = 1 To TextBox35.Text
            futurevalue = principal_amount * (1 + inflation_rate) ^ i
        Next i
        TextBox36.Text = futurevalue.ToString("F0")

        Dim probatefee As Single = futurevalue * ((Val(TextBox37.Text) / 100) * VAT)
        TextBox38.Text = probatefee.ToString("F0")

        'ppp cost
        Dim ageClient1 As Integer = Val(TextBox41.Text)
        Dim ageClient2 As Integer = Val(TextBox42.Text)

        If TextBox41.Text = "" Then ageClient1 = 0
        If TextBox42.Text = "" Then ageClient2 = 0

        Dim pppcost As Integer = ((ageClient1 * 30) + (ageClient2 * 12)) * VAT

        TextBox39.Text = pppcost.ToString("F0")

        Dim savings As Single = probatefee - pppcost

        TextBox40.Text = savings.ToString("F0")
        TextBox51.Text = savings.ToString("F0")
        Return True
    End Function

    Function recalculateFuneralCost()
        Dim futurevalue As Single
        Dim principal_amount As Double = (Val(TextBox43.Text) + Val(TextBox44.Text)) 'cant take the value from the actual total box as it doesnt always work for reasons that I don't understand
        Dim inflation_rate As Double = Val(TextBox48.Text) / 100

        For i As Integer = 1 To TextBox47.Text
            futurevalue = principal_amount * (1 + inflation_rate) ^ i
        Next i
        TextBox46.Text = futurevalue.ToString("F0")

        Dim savings As Single = (futurevalue - principal_amount).ToString("F0")

        TextBox49.Text = savings.ToString("F0")

        'put figure on summary savings sheet
        TextBox50.Text = savings.ToString("F0")

        Return True
    End Function

    Private Sub TextBox19_TextChanged(sender As Object, e As EventArgs) Handles TextBox19.TextChanged
        TextBox30.Text = TextBox19.Text
    End Sub

    Private Sub TextBox74_TextChanged(sender As Object, e As EventArgs) Handles TextBox74.TextChanged
        TextBox33.Text = Val(TextBox74.Text).ToString("F0")
    End Sub


End Class