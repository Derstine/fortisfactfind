﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xGiftRecepient
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xGiftRecepient))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEdit3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit8 = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboBoxEdit4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEdit5 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit11 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit12 = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboBoxEdit6 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEdit7 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.MemoEdit3 = New DevExpress.XtraEditors.MemoEdit()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.CheckEdit9 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl1.SuspendLayout
        CType(Me.ComboBoxEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit10.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ComboBoxEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TabbedControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem30,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem31,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem32,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem33,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit1)
        Me.LayoutControl1.Controls.Add(Me.TextEdit1)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit1)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit2)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit3)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit4)
        Me.LayoutControl1.Controls.Add(Me.TextEdit2)
        Me.LayoutControl1.Controls.Add(Me.TextEdit3)
        Me.LayoutControl1.Controls.Add(Me.TextEdit4)
        Me.LayoutControl1.Controls.Add(Me.SimpleButtonLookup)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit1)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit2)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit3)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit5)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit6)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit7)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit8)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit4)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit5)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit2)
        Me.LayoutControl1.Controls.Add(Me.TextEdit5)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.TextEdit6)
        Me.LayoutControl1.Controls.Add(Me.TextEdit7)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit10)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit11)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit12)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit6)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit7)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton4)
        Me.LayoutControl1.Location = New System.Drawing.Point(25, 11)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(622, 365, 824, 400)
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(833, 615)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(149, 12)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(316, 38)
        Me.ComboBoxEdit1.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit1.TabIndex = 4
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(606, 12)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(215, 38)
        Me.TextEdit1.StyleController = Me.LayoutControl1
        Me.TextEdit1.TabIndex = 5
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(161, 100)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "MR"
        Me.CheckEdit1.Size = New System.Drawing.Size(72, 44)
        Me.CheckEdit1.StyleController = Me.LayoutControl1
        Me.CheckEdit1.TabIndex = 6
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(237, 100)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Caption = "MRS"
        Me.CheckEdit2.Size = New System.Drawing.Size(105, 44)
        Me.CheckEdit2.StyleController = Me.LayoutControl1
        Me.CheckEdit2.TabIndex = 7
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(346, 100)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Caption = "MISS"
        Me.CheckEdit3.Size = New System.Drawing.Size(110, 44)
        Me.CheckEdit3.StyleController = Me.LayoutControl1
        Me.CheckEdit3.TabIndex = 8
        '
        'CheckEdit4
        '
        Me.CheckEdit4.Location = New System.Drawing.Point(460, 100)
        Me.CheckEdit4.Name = "CheckEdit4"
        Me.CheckEdit4.Properties.Caption = "MS"
        Me.CheckEdit4.Size = New System.Drawing.Size(349, 44)
        Me.CheckEdit4.StyleController = Me.LayoutControl1
        Me.CheckEdit4.TabIndex = 9
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(161, 148)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(648, 38)
        Me.TextEdit2.StyleController = Me.LayoutControl1
        Me.TextEdit2.TabIndex = 10
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(161, 190)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Size = New System.Drawing.Size(648, 38)
        Me.TextEdit3.StyleController = Me.LayoutControl1
        Me.TextEdit3.TabIndex = 11
        '
        'TextEdit4
        '
        Me.TextEdit4.Location = New System.Drawing.Point(161, 232)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.AutoHeight = false
        Me.TextEdit4.Size = New System.Drawing.Size(519, 42)
        Me.TextEdit4.StyleController = Me.LayoutControl1
        Me.TextEdit4.TabIndex = 12
        '
        'SimpleButtonLookup
        '
        Me.SimpleButtonLookup.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup.Location = New System.Drawing.Point(684, 232)
        Me.SimpleButtonLookup.Name = "SimpleButtonLookup"
        Me.SimpleButtonLookup.Size = New System.Drawing.Size(125, 42)
        Me.SimpleButtonLookup.StyleController = Me.LayoutControl1
        Me.SimpleButtonLookup.TabIndex = 13
        Me.SimpleButtonLookup.Text = "Lookup"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(161, 278)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(648, 43)
        Me.MemoEdit1.StyleController = Me.LayoutControl1
        Me.MemoEdit1.TabIndex = 14
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(161, 325)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(648, 38)
        Me.ComboBoxEdit2.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit2.TabIndex = 15
        '
        'ComboBoxEdit3
        '
        Me.ComboBoxEdit3.Location = New System.Drawing.Point(161, 367)
        Me.ComboBoxEdit3.Name = "ComboBoxEdit3"
        Me.ComboBoxEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit3.Size = New System.Drawing.Size(648, 38)
        Me.ComboBoxEdit3.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit3.TabIndex = 16
        '
        'CheckEdit5
        '
        Me.CheckEdit5.Location = New System.Drawing.Point(161, 409)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Caption = "Lapse"
        Me.CheckEdit5.Size = New System.Drawing.Size(87, 44)
        Me.CheckEdit5.StyleController = Me.LayoutControl1
        Me.CheckEdit5.TabIndex = 17
        '
        'CheckEdit6
        '
        Me.CheckEdit6.Location = New System.Drawing.Point(252, 409)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Caption = "Issue"
        Me.CheckEdit6.Size = New System.Drawing.Size(80, 44)
        Me.CheckEdit6.StyleController = Me.LayoutControl1
        Me.CheckEdit6.TabIndex = 18
        '
        'CheckEdit7
        '
        Me.CheckEdit7.Location = New System.Drawing.Point(336, 409)
        Me.CheckEdit7.Name = "CheckEdit7"
        Me.CheckEdit7.Properties.Caption = "Issue then Giftover"
        Me.CheckEdit7.Size = New System.Drawing.Size(158, 44)
        Me.CheckEdit7.StyleController = Me.LayoutControl1
        Me.CheckEdit7.TabIndex = 19
        '
        'CheckEdit8
        '
        Me.CheckEdit8.Location = New System.Drawing.Point(498, 409)
        Me.CheckEdit8.Name = "CheckEdit8"
        Me.CheckEdit8.Properties.Caption = "Giftover Only"
        Me.CheckEdit8.Size = New System.Drawing.Size(311, 44)
        Me.CheckEdit8.StyleController = Me.LayoutControl1
        Me.CheckEdit8.TabIndex = 20
        '
        'ComboBoxEdit4
        '
        Me.ComboBoxEdit4.Location = New System.Drawing.Point(161, 457)
        Me.ComboBoxEdit4.Name = "ComboBoxEdit4"
        Me.ComboBoxEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit4.Size = New System.Drawing.Size(333, 38)
        Me.ComboBoxEdit4.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit4.TabIndex = 21
        '
        'ComboBoxEdit5
        '
        Me.ComboBoxEdit5.Location = New System.Drawing.Point(635, 457)
        Me.ComboBoxEdit5.Name = "ComboBoxEdit5"
        Me.ComboBoxEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit5.Size = New System.Drawing.Size(174, 38)
        Me.ComboBoxEdit5.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit5.TabIndex = 22
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(161, 499)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Size = New System.Drawing.Size(648, 35)
        Me.MemoEdit2.StyleController = Me.LayoutControl1
        Me.MemoEdit2.TabIndex = 23
        '
        'TextEdit5
        '
        Me.TextEdit5.Location = New System.Drawing.Point(198, 117)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.AutoHeight = false
        Me.TextEdit5.Size = New System.Drawing.Size(415, 42)
        Me.TextEdit5.StyleController = Me.LayoutControl1
        Me.TextEdit5.TabIndex = 24
        '
        'SimpleButton2
        '
        Me.SimpleButton2.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton2.Location = New System.Drawing.Point(617, 117)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(125, 42)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 25
        Me.SimpleButton2.Text = "Lookup"
        '
        'TextEdit6
        '
        Me.TextEdit6.Location = New System.Drawing.Point(198, 163)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Size = New System.Drawing.Size(544, 38)
        Me.TextEdit6.StyleController = Me.LayoutControl1
        Me.TextEdit6.TabIndex = 26
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(198, 123)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Size = New System.Drawing.Size(611, 38)
        Me.TextEdit7.StyleController = Me.LayoutControl1
        Me.TextEdit7.TabIndex = 27
        '
        'CheckEdit10
        '
        Me.CheckEdit10.Location = New System.Drawing.Point(198, 165)
        Me.CheckEdit10.Name = "CheckEdit10"
        Me.CheckEdit10.Properties.Caption = "Lapse"
        Me.CheckEdit10.Size = New System.Drawing.Size(87, 44)
        Me.CheckEdit10.StyleController = Me.LayoutControl1
        Me.CheckEdit10.TabIndex = 28
        '
        'CheckEdit11
        '
        Me.CheckEdit11.Location = New System.Drawing.Point(289, 165)
        Me.CheckEdit11.Name = "CheckEdit11"
        Me.CheckEdit11.Properties.Caption = "Issue"
        Me.CheckEdit11.Size = New System.Drawing.Size(83, 44)
        Me.CheckEdit11.StyleController = Me.LayoutControl1
        Me.CheckEdit11.TabIndex = 29
        '
        'CheckEdit12
        '
        Me.CheckEdit12.Location = New System.Drawing.Point(376, 165)
        Me.CheckEdit12.Name = "CheckEdit12"
        Me.CheckEdit12.Properties.Caption = "Issue then Giftover"
        Me.CheckEdit12.Size = New System.Drawing.Size(433, 44)
        Me.CheckEdit12.StyleController = Me.LayoutControl1
        Me.CheckEdit12.TabIndex = 30
        '
        'ComboBoxEdit6
        '
        Me.ComboBoxEdit6.Location = New System.Drawing.Point(574, 213)
        Me.ComboBoxEdit6.Name = "ComboBoxEdit6"
        Me.ComboBoxEdit6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit6.Size = New System.Drawing.Size(235, 38)
        Me.ComboBoxEdit6.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit6.TabIndex = 31
        '
        'ComboBoxEdit7
        '
        Me.ComboBoxEdit7.Location = New System.Drawing.Point(198, 213)
        Me.ComboBoxEdit7.Name = "ComboBoxEdit7"
        Me.ComboBoxEdit7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit7.Size = New System.Drawing.Size(235, 38)
        Me.ComboBoxEdit7.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit7.TabIndex = 32
        '
        'MemoEdit3
        '
        Me.MemoEdit3.Location = New System.Drawing.Point(198, 255)
        Me.MemoEdit3.Name = "MemoEdit3"
        Me.MemoEdit3.Size = New System.Drawing.Size(611, 103)
        Me.MemoEdit3.StyleController = Me.LayoutControl1
        Me.MemoEdit3.TabIndex = 33
        '
        'SimpleButton3
        '
        Me.SimpleButton3.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton3.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton3.Location = New System.Drawing.Point(163, 561)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(317, 42)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 34
        Me.SimpleButton3.Text = "Save"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton4.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton4.Location = New System.Drawing.Point(484, 561)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(337, 42)
        Me.SimpleButton4.StyleController = Me.LayoutControl1
        Me.SimpleButton4.TabIndex = 35
        Me.SimpleButton4.Text = "Cancel"
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = false
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.TabbedControlGroup1, Me.LayoutControlItem32, Me.LayoutControlItem33, Me.EmptySpaceItem5, Me.EmptySpaceItem9})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(833, 615)
        Me.Root.TextVisible = false
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.ComboBoxEdit1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(457, 42)
        Me.LayoutControlItem1.Text = "Recepient Type"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.TextEdit1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(457, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(356, 42)
        Me.LayoutControlItem2.Text = "Percentage Share"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(134, 16)
        '
        'TabbedControlGroup1
        '
        Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 42)
        Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup1
        Me.TabbedControlGroup1.Size = New System.Drawing.Size(813, 496)
        Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup1, Me.LayoutControlGroup2, Me.LayoutControlGroup3})
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(789, 438)
        Me.LayoutControlGroup1.Text = "Named Individual"
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.CheckEdit1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(213, 48)
        Me.LayoutControlItem3.Text = "Title"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.CheckEdit2
        Me.LayoutControlItem4.Location = New System.Drawing.Point(213, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(109, 48)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = false
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.CheckEdit3
        Me.LayoutControlItem5.Location = New System.Drawing.Point(322, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(114, 48)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = false
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.CheckEdit4
        Me.LayoutControlItem6.Location = New System.Drawing.Point(436, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(353, 48)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = false
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.TextEdit2
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(789, 42)
        Me.LayoutControlItem7.Text = "Forenames"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.TextEdit3
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(789, 42)
        Me.LayoutControlItem8.Text = "Surname"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.TextEdit4
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 132)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(660, 46)
        Me.LayoutControlItem9.Text = "Postcode"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.SimpleButtonLookup
        Me.LayoutControlItem10.Location = New System.Drawing.Point(660, 132)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(129, 46)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = false
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem11.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem11.Control = Me.MemoEdit1
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 178)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(789, 47)
        Me.LayoutControlItem11.Text = "Address"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.ComboBoxEdit2
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 225)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(789, 42)
        Me.LayoutControlItem12.Text = "Relationship to Client 1"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.ComboBoxEdit3
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 267)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(789, 42)
        Me.LayoutControlItem13.Text = "Relationship to Client 2"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.CheckEdit5
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 309)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(228, 48)
        Me.LayoutControlItem14.Text = "Lapse or Issue?"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.CheckEdit6
        Me.LayoutControlItem15.Location = New System.Drawing.Point(228, 309)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(84, 48)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = false
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.CheckEdit7
        Me.LayoutControlItem16.Location = New System.Drawing.Point(312, 309)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(162, 48)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = false
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.CheckEdit8
        Me.LayoutControlItem17.Location = New System.Drawing.Point(474, 309)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(315, 48)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = false
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.ComboBoxEdit4
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 357)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(474, 42)
        Me.LayoutControlItem18.Text = "At what age?"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.ComboBoxEdit5
        Me.LayoutControlItem19.Location = New System.Drawing.Point(474, 357)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(315, 42)
        Me.LayoutControlItem19.Text = "At what age issue?"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem20.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem20.Control = Me.MemoEdit2
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 399)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(789, 39)
        Me.LayoutControlItem20.Text = "Giftover Beneficiaries"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem21, Me.LayoutControlItem22, Me.EmptySpaceItem3, Me.EmptySpaceItem4, Me.LayoutControlItem23, Me.EmptySpaceItem1, Me.EmptySpaceItem2})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(789, 438)
        Me.LayoutControlGroup2.Text = "Charity"
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.TextEdit5
        Me.LayoutControlItem21.Location = New System.Drawing.Point(37, 17)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(556, 46)
        Me.LayoutControlItem21.Text = "Charity Name"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.SimpleButton2
        Me.LayoutControlItem22.Location = New System.Drawing.Point(593, 17)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(129, 46)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = false
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = false
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(37, 438)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = false
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(722, 0)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(67, 438)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.TextEdit6
        Me.LayoutControlItem23.Location = New System.Drawing.Point(37, 63)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(685, 42)
        Me.LayoutControlItem23.Text = "Charity Number"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(134, 16)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = false
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(37, 105)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(685, 333)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = false
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(37, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(685, 17)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem6, Me.EmptySpaceItem7, Me.LayoutControlItem24, Me.EmptySpaceItem8, Me.LayoutControlItem26, Me.LayoutControlItem27, Me.LayoutControlItem28, Me.LayoutControlItem30, Me.LayoutControlItem29, Me.LayoutControlItem31})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(789, 438)
        Me.LayoutControlGroup3.Text = "Group"
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = false
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(37, 438)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = false
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(37, 0)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(752, 23)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.TextEdit7
        Me.LayoutControlItem24.Location = New System.Drawing.Point(37, 23)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(752, 42)
        Me.LayoutControlItem24.Text = "Group Name"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(134, 16)
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = false
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(37, 262)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(752, 176)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.CheckEdit10
        Me.LayoutControlItem26.Location = New System.Drawing.Point(37, 65)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(228, 48)
        Me.LayoutControlItem26.Text = "Lapse or Issue?"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.CheckEdit11
        Me.LayoutControlItem27.Location = New System.Drawing.Point(265, 65)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(87, 48)
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem27.TextVisible = false
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.CheckEdit12
        Me.LayoutControlItem28.Location = New System.Drawing.Point(352, 65)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(437, 48)
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = false
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.ComboBoxEdit7
        Me.LayoutControlItem30.Location = New System.Drawing.Point(37, 113)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(376, 42)
        Me.LayoutControlItem30.Text = "At what age?"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.ComboBoxEdit6
        Me.LayoutControlItem29.Location = New System.Drawing.Point(413, 113)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(376, 42)
        Me.LayoutControlItem29.Text = "At what issue?"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem31.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem31.Control = Me.MemoEdit3
        Me.LayoutControlItem31.Location = New System.Drawing.Point(37, 155)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(752, 107)
        Me.LayoutControlItem31.Text = "Giftover Beneficiaries"
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(134, 16)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.SimpleButton3
        Me.LayoutControlItem32.Location = New System.Drawing.Point(151, 549)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(321, 46)
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = false
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.SimpleButton4
        Me.LayoutControlItem33.Location = New System.Drawing.Point(472, 549)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(341, 46)
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem33.TextVisible = false
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = false
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 549)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(151, 46)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem9
        '
        Me.EmptySpaceItem9.AllowHotTrack = false
        Me.EmptySpaceItem9.Location = New System.Drawing.Point(0, 538)
        Me.EmptySpaceItem9.Name = "EmptySpaceItem9"
        Me.EmptySpaceItem9.Size = New System.Drawing.Size(813, 11)
        Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
        '
        'CheckEdit9
        '
        Me.CheckEdit9.Location = New System.Drawing.Point(24, 571)
        Me.CheckEdit9.Name = "CheckEdit9"
        Me.CheckEdit9.Properties.Caption = "CheckEdit9"
        Me.CheckEdit9.Size = New System.Drawing.Size(930, 44)
        Me.CheckEdit9.TabIndex = 28
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.CheckEdit9
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 380)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(890, 48)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = false
        '
        'xGiftRecepient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(871, 662)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "xGiftRecepient"
        Me.Text = "Gift Recipient"
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl1.ResumeLayout(false)
        CType(Me.ComboBoxEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit10.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ComboBoxEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TabbedControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem30,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem31,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem32,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem33,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEdit3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ComboBoxEdit4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEdit5 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents CheckEdit10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit11 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit12 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ComboBoxEdit6 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEdit7 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents MemoEdit3 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
End Class
