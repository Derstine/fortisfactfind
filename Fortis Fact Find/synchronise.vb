﻿Imports System.Net
Imports System.IO
Imports System.Text

Public Class synchronise
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub synchronise_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)
        resetForm(Me)

        Me.ProgressBar1.Value = 0
        Me.ProgressBar2.Value = 0

        Me.Button1.Enabled = False  'disable proceed button until we know what we are doing

        Dim sql As String = "select * from will_instruction_stage0_data where status='synchronise'"
        Dim results As Array
        results = runSQLwithArray(sql)
        If results(0) = "ERROR" Then
            'nothing has been set to send
            Me.Button1.Enabled = False
            Me.warningMessage.Visible = True
            Me.Label1.Visible = False
        Else
            'ok to send
            Me.Button1.Enabled = True
            Me.warningMessage.Visible = False
            Me.Label1.Visible = True
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim progress2max As Integer = 0

        Try

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '' BEFORE WE START, AM GOING TO SEND A COPY OF THE DATABASE TO THE SERVER BECAUSE WE CAN
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Dim username As String
            Dim apikey As String
            Dim sql As String

            Me.auditlog.Text = "Initialising transfer process..."

            sql = "select username from userdata"
            username = runSQLwithID(sql)

            sql = "select apikey from userdata"
            apikey = runSQLwithID(sql)

            Dim localFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
            Dim databaseSource As String = localFolder + "\" + baseDatabaseFilename
            Dim databaseSourceCopy As String = localFolder + "\SYNC-" + Trim(username) + "-" + baseDatabaseFilename

            'do a file copy
            If System.IO.File.Exists(databaseSourceCopy) = True Then
                'need to delete file
                System.IO.File.Delete(databaseSourceCopy)
            End If
            System.IO.File.Copy(databaseSource, databaseSourceCopy)

            Dim remoteName = Trim(username) + "-" + baseDatabaseFilename

            Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(remoteBackupFTPAddress + remoteName), System.Net.FtpWebRequest)
            request.Credentials = New System.Net.NetworkCredential(ftpUsername, ftpPassword)
            request.Method = System.Net.WebRequestMethods.Ftp.UploadFile

            Dim file() As Byte = System.IO.File.ReadAllBytes(databaseSourceCopy)

            Dim strz As System.IO.Stream = request.GetRequestStream()
            strz.Write(file, 0, file.Length)
            strz.Close()
            strz.Dispose()

            'now kill off the copy file
            If System.IO.File.Exists(databaseSourceCopy) = True Then
                'need to delete file
                System.IO.File.Delete(databaseSourceCopy)
            End If


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '' THIS IS THE MAIN LOOP FOR SYNCHRONISING - START
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim result As String = ""
            Dim recordCounter As Integer = 0
            Dim localWillClientSequence As Integer = 0
            Dim serverWillClientSequence As Integer = 0
            Dim typeOfTransfer As String = ""

            sql = "select count(sequence) from will_instruction_stage0_data where status='synchronise' or misc2='touched'"
            result = runSQLwithID(sql)
            If result <> "ERROR" And result <> "0" Then
                'we have data to start processing!!
                progress2max = Val(result)
                Me.ProgressBar2.Minimum = 0
                Me.ProgressBar2.Maximum = progress2max

                Dim dbConn As New OleDb.OleDbConnection
                Dim command As New OleDb.OleDbCommand
                Dim dbreader As OleDb.OleDbDataReader

                sql = "select * from will_instruction_stage0_data where status='synchronise' or misc2='touched'"

                dbConn.ConnectionString = connectionString
                dbConn.Open()
                command.Connection = dbConn
                command.CommandText = sql
                dbreader = command.ExecuteReader
                While dbreader.Read()
                    localWillClientSequence = dbreader.GetInt32(0)
                    If dbreader.GetValue(3).ToString = "synchronise" Then typeOfTransfer = "synchronise" Else typeOfTransfer = "provisional"

                    recordCounter = recordCounter + 1
                    Me.ProgressBar2.Value = recordCounter

                    Me.ProgressBar1.Minimum = 0
                    Me.ProgressBar1.Maximum = 61

                    ''''''''''''''''''''''''
                    'STAGE 1
                    ''''''''''''''''''''''''

                    '''' check to see if this has already been submitted before
                    If Val(dbreader.GetValue(9).ToString) > 0 Then
                        'this record has already been submitted
                        serverWillClientSequence = resetServerData(apikey, dbreader.GetValue(9).ToString)

                        'if record has been deleted from the server, then it returns -1, so in that case we'd need to create a new record as per usual
                        If serverWillClientSequence < 0 Then
                            serverWillClientSequence = step1(apikey, localWillClientSequence, typeOfTransfer)
                        End If
                    Else
                        'this record has never been submitted
                        serverWillClientSequence = step1(apikey, localWillClientSequence, typeOfTransfer)
                    End If

                    'assuming that we have a server sequence, then we can continue
                    If serverWillClientSequence <> -1 Then
                        'we have a server sequence, so can begin with the data transfer process

                        Me.ProgressBar1.Value = 2
                        If step2(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 2 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 3
                        If step3(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 3 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 4
                        If step4(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 4 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 5
                        If step5(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 5 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 6
                        If step6(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 6 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 7
                        If step7(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 7 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 8
                        If step8(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 8 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 9
                        If step9(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 9 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 10
                        If step10(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 10 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 11
                        If step11(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 11 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 12
                        If step12(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 12 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 13
                        If step13(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 13 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 14
                        If step14(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 14 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 15
                        If step15(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 15 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 16
                        If step16(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 16 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 17
                        'If step17(apikey, serverWillClientSequence) <> "OK" Then MsgBox("Step 17 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 18
                        If step18(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 18 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 19
                        If step19(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 19 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 20
                        If step20(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 20 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 21
                        If step21(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 21 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 22
                        If step22(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 22 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 23
                        If step23(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 23 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 24
                        If step24(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 24 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 24
                        If step24a(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 24a Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 24
                        If step24b(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 24b Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")


                        Me.ProgressBar1.Value = 25
                        If step25(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 25 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 26
                        If step26(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 26 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 27
                        If step27(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 27 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 28
                        If step28(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 28 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 29
                        ' If step29(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 29 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 30
                        If step30(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 30 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 31
                        If step31(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 31 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 32
                        If step32(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 32 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 33
                        If step33(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 33 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 34
                        If step34(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 34 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 35
                        If step35(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 35 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 36
                        If step36(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 36 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 37
                        If step37(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 37 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 38
                        If step38(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 38 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 39
                        If step39(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 39 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 40
                        If step40(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 40 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 41
                        If step41(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 41 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 42
                        If step42(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 42 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 43
                        If step43(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 43 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 44
                        If step44(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 44 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 45
                        If step45(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 45 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 46
                        If step46(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 46 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 47
                        If step47(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 47 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 48
                        If step48(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 48 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 49
                        If step49(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 49 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 50
                        If step50(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 50 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 51
                        If step51(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 51 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")
                        If step51a(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 51a Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")
                        If step51b(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 51b Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 52
                        If step52(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 52 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 53
                        If step53(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 53 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 54
                        If step54(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 54 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 55
                        If step55(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 55 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 56
                        If step56(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 56 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 57
                        If step57(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 57 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 58
                        If step58(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 58 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 59
                        If step59(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 59 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 60
                        If step60(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 60 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        Me.ProgressBar1.Value = 61
                        If step61(apikey, localWillClientSequence, serverWillClientSequence) <> "OK" Then MsgBox("Step 61 Failed (" + localWillClientSequence.ToString + " =>" + serverWillClientSequence.ToString + ")")

                        'send spy activity report
                        uploadSpyData(apikey, localWillClientSequence, serverWillClientSequence)

                        'upload audio files
                        uploadAudioFiles(apikey, localWillClientSequence, serverWillClientSequence)

                        'upload signature files
                        uploadSignatureFiles(apikey, localWillClientSequence, serverWillClientSequence)


                    Else
                        'there is no valid server sequence ... need to alert the user
                        MsgBox("No valid server sequence could be found" + vbCrLf + vbCrLf + "Please contact Technical Support on 01702 780102")
                    End If

                    'reset misc1 field to blank to mark that the client record has not be opened/edited again since the last time it was synchronised
                    sql = "update will_instruction_stage0_data set misc2='' where sequence=" + clientID
                    runSQL(sql)


                End While    'this ends the main while loop going through all the stage0 data records that have been marked as "ready to sync"

                ' MsgBox("kill me now")
                'finalise data upload
                step100()
                step101()

                dbreader.Close()
                dbConn.Close()

            End If

            'finished transfer, now need to mark records as archived
            sql = "update will_instruction_stage0_data set status='archived' where status='synchronise'"
            runSQL(sql)

            'now run the spy command so we know what's going on
            runActivitySpyReport()
            '  MsgBox("activity done")

            ProgressBar1.Value = 61
            ProgressBar2.Value = progress2max

            Dim numCases As Integer = getNewCases()

            If numCases > 0 Then
                MsgBox("Records Have Been Sent & " + numCases.ToString + " new records have been retrieved")
            Else
                MsgBox("Records Have Been Sent")
            End If


        Catch ex As Exception

            ProgressBar1.Value = 0
            ProgressBar2.Value = 0
            MsgBox(ex.Message)
            MsgBox("You are not connected to the internet" + vbCrLf + "Therefore, cannot send the data to the server for processing", vbOKOnly + vbCritical)

        End Try


        Me.Close()
    End Sub

    Function step1(apikey As String, localWillClientSequence As String, typeOfTransfer As String)

        'this stage simply gets the serverWillClientSequence
        Try
            System.Net.ServicePointManager.Expect100Continue = False
            Dim strURL As String = remoteSyncURL + "get_sequence.php?apikey=" + apikey
            Dim strOutput As String = ""
            Dim wrResponse As WebResponse
            Dim wrRequest As WebRequest = HttpWebRequest.Create(strURL)
            Dim sqlToSend As String = ""

            auditlog.Text = "Getting serverWillClientSequence..." & Environment.NewLine

            wrResponse = wrRequest.GetResponse()

            Using sr As New StreamReader(wrResponse.GetResponseStream())
                strOutput = sr.ReadToEnd()
                ' Close and clean up the StreamReader
                sr.Close()
            End Using

            auditlog.Text = strOutput
            ' MsgBox(strOutput)
            If strOutput = "Error" Then
                Return -1
            Else
                ''''''''''''''''''''''
                'update will_instruction_stage0_data with remote serverSequence ... this will allow historic records to "update" client records and re-run and not create new client record
                ''''''''''''''''''''''
                Dim sql As String
                sql = "update will_instruction_stage0_data set serverSequence='" + SqlSafe(strOutput) + "'    where sequence=" + localWillClientSequence
                ' MsgBox(sql)
                runSQL(sql)

                'now we know the server sequence, we need to set the misc2 value to reflect type of Transfer so it doesn't get processed as a ready-to-go case
                sqlToSend = "update will_instruction_stage0_data set misc2={{!" + typeOfTransfer + "!}} where sequence=" + SqlSafe(strOutput)

                sqlToSend = WebUtility.HtmlEncode(sqlToSend)
                sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
                sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
                sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
                Dim ignoreResponseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)

            End If

            Return strOutput

        Catch ex As Exception

            MsgBox(ex.Message)

        End Try

        Return -1   'default

    End Function

    Function step2(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        System.Net.ServicePointManager.Expect100Continue = False
        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage1_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading will type"
            Me.Refresh()

            'clean the local data
            Dim consultantSequence As Integer
            Dim typeOfWill As String

            If dbReader.GetValue(1).ToString = "" Then consultantSequence = 0 Else consultantSequence = dbReader.GetInt32(1)
            If dbReader.GetValue(3).ToString = "Mirror Will" Or dbReader.GetValue(3).ToString = "mirror" Then typeOfWill = "mirror" Else typeOfWill = "single"

            Dim sqlToSend As String = "insert into will_instruction_stage1_data (consultantSequence,willClientSequence,typeOfWill) values (" + consultantSequence.ToString + "," + serverWillClientSequence + ",{{!" + typeOfWill + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)
            dbReader.Close()
            dbConn.Close()

            'now udate will_instruction_stage0_data with the username
            'shouldn't be here really, but its a quick fix :-)
            Dim username As String
            sql = "select username from userdata"
            username = runSQLwithID(sql)

            sqlToSend = "update will_instruction_stage0_data set misc1={{!" + username + "!}} where sequence=" + serverWillClientSequence

            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            Dim ignoreResponseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)

            Return responseFromServer

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step3(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        System.Net.ServicePointManager.Expect100Continue = False
        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage2_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Transferring client details"
            Me.Refresh()


            'clean the local data
            Dim person1Title, person1Surname, person1Forenames, person1AKA, person1Address, person1MaritalStatus, person1DOB, person1HomeNumber, person1WorkNumber, person1MobileNumber, person1Email
            Dim person2Title, person2Surname, person2Forenames, person2AKA, person2Address, person2MaritalStatus, person2DOB, person2HomeNumber, person2WorkNumber, person2MobileNumber, person2Email
            Dim willInAnotherCountryFlag, willInAnotherCountryDetail, futureSpouseFlag, futureSpouseName, whoHasLPA As String
            Dim expatFlag, expatDetail, worldwideAssetsFlag, person1Occupation, person2Occupation, person1NINumber, person2NINumber, person1Postcode, person2Postcode, clientReferenceNumber, miscNotes As String

            If dbReader.GetValue(12).ToString = "" Then person1Title = "" Else person1Title = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then person1Surname = "" Else person1Surname = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then person1Forenames = "" Else person1Forenames = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then person1AKA = "" Else person1AKA = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then person1Address = "" Else person1Address = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then person1MaritalStatus = "" Else person1MaritalStatus = dbReader.GetValue(17).ToString

            If dbReader.GetValue(18).ToString = "" Then
                person1DOB = "1900-01-01"
            Else
                person1DOB = dbReader.GetDateTime(18).Year.ToString + "-" + dbReader.GetDateTime(18).Month.ToString + "-" + dbReader.GetDateTime(18).Day.ToString
            End If

            If dbReader.GetValue(19).ToString = "" Then person1HomeNumber = "" Else person1HomeNumber = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then person1WorkNumber = "" Else person1WorkNumber = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then person1MobileNumber = "" Else person1MobileNumber = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then person1Email = "" Else person1Email = dbReader.GetValue(22).ToString


            If dbReader.GetValue(23).ToString = "" Then person2Title = "" Else person2Title = dbReader.GetValue(23).ToString
            If dbReader.GetValue(24).ToString = "" Then person2Surname = "" Else person2Surname = dbReader.GetValue(24).ToString
            If dbReader.GetValue(25).ToString = "" Then person2Forenames = "" Else person2Forenames = dbReader.GetValue(25).ToString
            If dbReader.GetValue(26).ToString = "" Then person2AKA = "" Else person2AKA = dbReader.GetValue(26).ToString
            If dbReader.GetValue(27).ToString = "" Then person2Address = "" Else person2Address = dbReader.GetValue(27).ToString
            If dbReader.GetValue(28).ToString = "" Then person2MaritalStatus = "" Else person2MaritalStatus = dbReader.GetValue(28).ToString

            If dbReader.GetValue(29).ToString = "" Then
                person2DOB = "1900-01-01"
            Else
                person2DOB = dbReader.GetDateTime(29).Year.ToString + "-" + dbReader.GetDateTime(29).Month.ToString + "-" + dbReader.GetDateTime(29).Day.ToString
            End If

            If dbReader.GetValue(30).ToString = "" Then person2HomeNumber = "" Else person2HomeNumber = dbReader.GetValue(30).ToString
            If dbReader.GetValue(31).ToString = "" Then person2WorkNumber = "" Else person2WorkNumber = dbReader.GetValue(31).ToString
            If dbReader.GetValue(32).ToString = "" Then person2MobileNumber = "" Else person2MobileNumber = dbReader.GetValue(32).ToString
            If dbReader.GetValue(33).ToString = "" Then person2Email = "" Else person2Email = dbReader.GetValue(33).ToString

            If dbReader.GetValue(40).ToString = "" Then willInAnotherCountryFlag = "" Else willInAnotherCountryFlag = dbReader.GetValue(40).ToString
            If dbReader.GetValue(41).ToString = "" Then willInAnotherCountryDetail = "" Else willInAnotherCountryDetail = dbReader.GetValue(41).ToString
            If dbReader.GetValue(42).ToString = "" Then futureSpouseFlag = "" Else futureSpouseFlag = dbReader.GetValue(42).ToString
            If dbReader.GetValue(43).ToString = "" Then futureSpouseName = "" Else futureSpouseName = dbReader.GetValue(43).ToString

            If dbReader.GetValue(56).ToString = "" Then whoHasLPA = "N" Else whoHasLPA = dbReader.GetValue(56).ToString

            If dbReader.GetValue(60).ToString = "" Then expatFlag = "N" Else expatFlag = dbReader.GetValue(60).ToString
            If dbReader.GetValue(61).ToString = "" Then expatDetail = "" Else expatDetail = dbReader.GetValue(61).ToString
            If dbReader.GetValue(62).ToString = "" Then worldwideAssetsFlag = "N" Else worldwideAssetsFlag = dbReader.GetValue(62).ToString

            If dbReader.GetValue(63).ToString = "" Then person1Occupation = "" Else person1Occupation = dbReader.GetValue(63).ToString
            If dbReader.GetValue(64).ToString = "" Then person2Occupation = "" Else person2Occupation = dbReader.GetValue(64).ToString
            If dbReader.GetValue(65).ToString = "" Then person1NINumber = "" Else person1NINumber = dbReader.GetValue(65).ToString
            If dbReader.GetValue(66).ToString = "" Then person2NINumber = "" Else person2NINumber = dbReader.GetValue(66).ToString
            If dbReader.GetValue(67).ToString = "" Then person1Postcode = "" Else person1Postcode = dbReader.GetValue(67).ToString
            If dbReader.GetValue(68).ToString = "" Then person2Postcode = "" Else person2Postcode = dbReader.GetValue(68).ToString

            If dbReader.GetValue(75).ToString = "" Then clientReferenceNumber = "" Else clientReferenceNumber = dbReader.GetValue(75).ToString
            If dbReader.GetValue(76).ToString = "" Then miscNotes = "" Else miscNotes = dbReader.GetValue(76).ToString

            Dim sqlToSend As String = "insert into will_instruction_stage2_data (willClientSequence,person1Title, person1Surname, person1Forenames, person1AKA, person1Address, person1MaritalStatus, person1DOB, person1HomeNumber, person1WorkNumber, person1MobileNumber, person1Email,person2Title, person2Surname, person2Forenames, person2AKA, person2Address, person2MaritalStatus, person2DOB, person2HomeNumber, person2WorkNumber, person2MobileNumber, person2Email,willInAnotherCountryFlag, willInAnotherCountryDetail, futureSpouseFlag, futureSpouseName,expatFlag,expatDetail,worldwideAssetsFlag,person1Occupation,person2Occupation,person1NINumber,person2NINumber,person1Postcode,person2Postcode,clientReferenceNumber, whoHasLPA,miscNotes) "
            sqlToSend = sqlToSend + " values (" + serverWillClientSequence + ",{{!" + person1Title + "!}},{{!" + person1Surname + "!}},{{!" + person1Forenames + "!}},{{!" + person1AKA + "!}},{{!" + person1Address + "!}},{{!" + person1MaritalStatus + "!}},{{!" + person1DOB + "!}},{{!" + person1HomeNumber + "!}},{{!" + person1WorkNumber + "!}},{{!" + person1MobileNumber + "!}},{{!" + person1Email + "!}},{{!" + person2Title + "!}},{{!" + person2Surname + "!}},{{!" + person2Forenames + "!}},{{!" + person2AKA + "!}},{{!" + person2Address + "!}},{{!" + person2MaritalStatus + "!}},{{!" + person2DOB + "!}},{{!" + person2HomeNumber + "!}},{{!" + person2WorkNumber + "!}},{{!" + person2MobileNumber + "!}},{{!" + person2Email + "!}},{{!" + willInAnotherCountryFlag + "!}},{{!" + willInAnotherCountryDetail + "!}},{{!" + futureSpouseFlag + "!}},{{!" + futureSpouseName + "!}},{{!" + expatFlag + "!}},{{!" + expatDetail + "!}},{{!" + worldwideAssetsFlag + "!}},{{!" + person1Occupation + "!}},{{!" + person2Occupation + "!}},{{!" + person1NINumber + "!}},{{!" + person2NINumber + "!}},{{!" + person1Postcode + "!}},{{!" + person2Postcode + "!}},{{!" + clientReferenceNumber + "!}},{{!" + whoHasLPA + "!}},{{!" + miscNotes + "!}}) "
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            '  MsgBox(responseFromServer)

            dbReader.Close()
            dbConn.Close()
            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step4(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage3_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading executor type"
            Me.Refresh()

            'clean the local data
            Dim executorType As String

            If dbReader.GetValue(2).ToString = "" Then executorType = "" Else executorType = dbReader.GetValue(2).ToString

            Dim sqlToSend As String = "insert into will_instruction_stage3_data (willClientSequence,executorType) values (" + serverWillClientSequence + ",{{!" + executorType + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step5(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage3_data_detail where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading executor details"
        Me.Refresh()

        'clean the local data
        Dim executorType As String
        Dim executorNames As String
        Dim executorPhone As String
        Dim executorRelationshipClient1 As String
        Dim executorRelationshipClient2 As String
        Dim executorAddress As String
        Dim executorEmail As String
        Dim executorTitle As String
        Dim executorSurname As String
        Dim executorPostcode As String
        Dim trusteeFlag As String
        Dim executorDOB As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then executorType = "" Else executorType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then executorNames = "" Else executorNames = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then executorPhone = "" Else executorPhone = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then executorRelationshipClient1 = "" Else executorRelationshipClient1 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then executorRelationshipClient2 = "" Else executorRelationshipClient2 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then executorAddress = "" Else executorAddress = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then executorEmail = "" Else executorEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(11).ToString = "" Then executorTitle = "" Else executorTitle = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then executorSurname = "" Else executorSurname = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then executorPostcode = "" Else executorPostcode = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then trusteeFlag = "" Else trusteeFlag = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then executorDOB = "" Else executorDOB = dbReader.GetValue(15).ToString


            sqlToSend = "insert into will_instruction_stage3_data_detail (willClientSequence,executorType,executorNames,executorPhone,executorAddress,executorEmail,executorTitle,executorSurname,executorPostcode,executorDOB,executorRelationshipClient1,executorRelationshipClient2,trusteeFlag) values (" + serverWillClientSequence + ",{{!" + executorType + "!}},{{!" + executorNames + "!}},{{!" + executorPhone + "!}},{{!" + executorAddress + "!}},{{!" + executorEmail + "!}},{{!" + executorTitle + "!}},{{!" + executorSurname + "!}},{{!" + executorPostcode + "!}},{{!" + executorDOB + "!}},{{!" + executorRelationshipClient1 + "!}},{{!" + executorRelationshipClient2 + "!}},{{!" + trusteeFlag + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step6(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage4_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading children information"
            Me.Refresh()

            'clean the local data
            Dim specialNeeds As String
            Dim noChildrenConfirm As String

            If dbReader.GetValue(2).ToString = "" Then specialNeeds = "" Else specialNeeds = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then noChildrenConfirm = "" Else noChildrenConfirm = dbReader.GetValue(3).ToString

            Dim sqlToSend As String = "insert into will_instruction_stage4_data (willClientSequence,specialNeeds,noChildrenConfirm) values (" + serverWillClientSequence + ",{{!" + specialNeeds + "!}},{{!" + noChildrenConfirm + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step7(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage4_data_detail where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading child details"
        Me.Refresh()

        'clean the local data
        Dim childSurname As String
        Dim childForenames As String
        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim address As String
        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        Dim childTitle As String
        Dim childPostcode As String
        Dim childDOB As String

        Dim childEmail As String
        Dim childPhone As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then childSurname = "" Else childSurname = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then childForenames = "" Else childForenames = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then address = "" Else address = dbReader.GetValue(6).ToString

            If dbReader.GetValue(9).ToString = "" Then childTitle = "" Else childTitle = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then childPostcode = "" Else childPostcode = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then childDOB = "" Else childDOB = dbReader.GetValue(11).ToString

            If dbReader.GetValue(12).ToString = "" Then childEmail = "" Else childEmail = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then childPhone = "" Else childPhone = dbReader.GetValue(13).ToString

            sqlToSend = "insert into will_instruction_stage4_data_detail (willClientSequence,childSurname,childForenames,relationToClient1,relationToClient2,address,childTitle,childPostcode,childDOB,childEmail,childPhone) values (" + serverWillClientSequence + ",{{!" + childSurname + "!}},{{!" + childForenames + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + address + "!}},{{!" + childTitle + "!}},{{!" + childPostcode + "!}},{{!" + childDOB + "!}},{{!" + childEmail + "!}},{{!" + childPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If


    End Function

    Function step8(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage5_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading guardian information"
            Me.Refresh()

            'clean the local data
            Dim guardianType As String

            If dbReader.GetValue(2).ToString = "" Then guardianType = "" Else guardianType = dbReader.GetValue(2).ToString

            Dim sqlToSend As String = "insert into will_instruction_stage5_data (willClientSequence,guardianType) values (" + serverWillClientSequence + ",{{!" + guardianType + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step9(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage5_data_detail where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading guardian details"
        Me.Refresh()

        'clean the local data
        Dim guardianTitle As String
        Dim guardianSurname As String
        Dim guardianNames As String
        Dim guardianType As String
        Dim guardianAddress As String
        Dim guardianPhone As String
        Dim guardianEmail As String
        Dim sqlToSend As String
        Dim responseFromServer As String = ""
        Dim guardianPostcode As String
        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim guardianDOB As String
        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then guardianTitle = "" Else guardianTitle = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then guardianSurname = "" Else guardianSurname = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then guardianNames = "" Else guardianNames = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then guardianType = "" Else guardianType = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then guardianAddress = "" Else guardianAddress = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then guardianPhone = "" Else guardianPhone = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then guardianEmail = "" Else guardianEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(11).ToString = "" Then guardianPostcode = "" Else guardianPostcode = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then guardianDOB = "" Else guardianDOB = dbReader.GetValue(14).ToString

            sqlToSend = "insert into will_instruction_stage5_data_detail (willClientSequence,guardianTitle,guardianSurname,guardianNames,guardianType,guardianAddress,guardianPhone,guardianEmail,guardianPostcode,guardianDOB,relationshipClient1,relationshipClient2) values (" + serverWillClientSequence + ",{{!" + guardianTitle + "!}},{{!" + guardianSurname + "!}},{{!" + guardianNames + "!}},{{!" + guardianType + "!}},{{!" + guardianAddress + "!}},{{!" + guardianPhone + "!}},{{!" + guardianEmail + "!}},{{!" + guardianPostcode + "!}},{{!" + guardianDOB + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            '   MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If


    End Function

    Function step10(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage6_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Sending funeral information"
            Me.Refresh()

            'clean the local data
            Dim funeralClient1Type As String
            Dim funeralClient1Wishes As String
            Dim funeralClient2Type As String
            Dim funeralClient2Wishes As String

            If dbReader.GetValue(2).ToString = "" Then funeralClient1Type = "" Else funeralClient1Type = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then funeralClient1Wishes = "" Else funeralClient1Wishes = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then funeralClient2Type = "" Else funeralClient2Type = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then funeralClient2Wishes = "" Else funeralClient2Wishes = dbReader.GetValue(5).ToString

            Dim sqlToSend As String = "insert into will_instruction_stage6_data (willClientSequence,funeralClient1Type,funeralClient1Wishes,funeralClient2Type,funeralClient2Wishes) values (" + serverWillClientSequence + ",{{!" + funeralClient1Type + "!}},{{!" + funeralClient1Wishes + "!}},{{!" + funeralClient2Type + "!}},{{!" + funeralClient2Wishes + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)

            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step11(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        'this stage needs to insert gifts into table, then remember the sequence number of that gift, so it can be used in the detail table
        'yhep, it's horrible!!!!

        'logic is this:
        'loop through all gifts in stage 7,
        'hit the first gift, REMEMBER sequence number insert it
        'find sequence number of new record
        'find all gifts in detail table with old sequence number
        'insert those into server detail table
        'finish loop through all gifts


        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage7_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim localSequence As Int32
        Dim serverSequence As Int32
        Dim giftFrom As String
        Dim giftSharedFlag As String
        Dim typeOfGift As String
        Dim itemDetails As String
        Dim taxFree As String
        Dim soleJointGiftFlag As String
        Dim firstSecondDeathFlag As String
        Dim responseFromServer As String = "EMPTY"

        Dim sqlToSend As String
        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "Sending gift information"
            Me.Refresh()

            localSequence = dbReader.GetInt32(0)
            If dbReader.GetValue(2).ToString = "" Then
                giftFrom = ""
            Else
                giftFrom = dbReader.GetValue(2).ToString

                If giftFrom = "Client 1" Or giftFrom = "Client 1b" Then
                    giftFrom = "Yourself"
                ElseIf giftFrom = "Client 1a" Then
                    giftFrom = "Both"
                ElseIf giftFrom = "Client 2" Then
                    giftFrom = "Partner"
                Else
                    giftFrom = "Both"
                End If
            End If


            If dbReader.GetValue(3).ToString = "" Then giftSharedFlag = "" Else giftSharedFlag = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then
                typeOfGift = ""
            Else
                typeOfGift = dbReader.GetValue(4).ToString
                If typeOfGift = "Other" Then
                    typeOfGift = "other"
                ElseIf typeOfGift = "Agricultural or Business Property" Then
                    typeOfGift = "business"
                Else
                    typeOfGift = "Choose One"
                End If

            End If

            If dbReader.GetValue(5).ToString = "" Then itemDetails = "" Else itemDetails = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then taxFree = "" Else taxFree = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then soleJointGiftFlag = "" Else soleJointGiftFlag = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then firstSecondDeathFlag = "" Else firstSecondDeathFlag = dbReader.GetValue(8).ToString

            sqlToSend = "insert into will_instruction_stage7_data (willClientSequence,giftFrom,giftSharedFlag,typeOfGift,itemDetails,taxFree,soleJointGiftFlag,firstSecondDeathFlag) values (" + serverWillClientSequence + ",{{!" + giftFrom + "!}},{{!" + giftSharedFlag + "!}},{{!" + typeOfGift + "!}},{{!" + itemDetails + "!}},{{!" + taxFree + "!}},{{!" + soleJointGiftFlag + "!}},{{!" + firstSecondDeathFlag + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox("Step 11 - aout to send" + sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-GetSequence-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

            '***** the responseFromServer contains the sequence of the inserted row. Saves having to run another query on the server - clever, eh?
            serverSequence = Val(responseFromServer)

            'now, run a query against the detail data table but with the new and old sequence numbers
            step11a(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)


        End While

        dbReader.Close()
        dbConn.Close()
        ' MsgBox("Step 11 =>" + responseFromServer)
        If IsNumeric(responseFromServer) Or responseFromServer = "EMPTY" Then
            Return "OK"
        Else
            Return responseFromServer
        End If


    End Function

    Function step11a(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localGiftSequence As String, ByVal serverGiftSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage7_data_detail where willClientSequence=" + localWillClientSequence + " and giftSequence=" + localGiftSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading gift recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim giftSharedType As String
        Dim giftSharedPercentage As String
        Dim charityName As String
        Dim charityRegNumber As String
        Dim atWhatAge As String
        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String
        Dim giftoverName As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "bloodline children"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "named children"
                ElseIf beneficiaryType = "Named Individual" Then
                    beneficiaryType = "named individual"
                ElseIf beneficiaryType = "Charity" Then
                    beneficiaryType = "charity"
                Else
                    'do nothing as currently entered data is correct
                End If
            End If
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then giftSharedType = "" Else giftSharedType = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then giftSharedPercentage = "" Else giftSharedPercentage = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then charityName = "" Else charityName = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then charityRegNumber = "" Else charityRegNumber = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then atWhatAge = "0" Else atWhatAge = dbReader.GetValue(9).ToString

            If dbReader.GetValue(12).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then atWhatAgeIssue = "0" Else atWhatAgeIssue = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(18).ToString

            If dbReader.GetValue(19).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(22).ToString

            If dbReader.GetValue(23).ToString = "" Then giftoverName = "" Else giftoverName = dbReader.GetValue(23).ToString


            sqlToSend = "insert into will_instruction_stage7_data_detail (willClientSequence,giftSequence, beneficiaryType,beneficiaryName,giftSharedType,giftSharedPercentage,charityName,charityRegNumber,atWhatAge, relationToClient1,relationToClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname, atWhatAgeIssue, lapseOrIssue,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone,giftoverName) values (" + serverWillClientSequence + ",{{!" + serverGiftSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + giftSharedType + "!}},{{!" + giftSharedPercentage + "!}},{{!" + charityName + "!}},{{!" + charityRegNumber + "!}},{{!" + atWhatAge + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + lapseOrIssue + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}},{{!" + giftoverName + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'end

        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        'this stage needs to insert gifts into table, then remember the sequence number of that trust, so it can be used in the detail table
        'yhep, it's horrible!!!!

        'logic is this:
        'loop through all trusts in stage 8,
        'hit the first trust, REMEMBER sequence number insert it
        'find sequence number of new record
        'find all trust in detail table with old sequence number
        'insert those into server detail table
        'finish loop through all gifts


        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim localSequence As Int32
        Dim serverSequence As Int32
        Dim trustType As String
        Dim naConfirmationFlag As String
        Dim FLITtype As String
        Dim FLITname As String
        Dim r2rFlag As String
        Dim r2rYears As String
        Dim r2rTime As String
        Dim r2rLocation As String
        Dim r2rWhoTo As String
        Dim r2rNamed As String
        Dim r2rBenefits As String
        Dim r2rMortgage As String
        Dim r2rFurniture As String
        Dim r2rBeneficialShare As String
        Dim r2rCeaseRemarry As String
        Dim dtEstateType As String
        Dim dtEstate As String
        Dim dtWishes As String
        Dim backstopCharityName As String
        Dim backstopCharityNumber As String

        Dim responseFromServer As String = "EMPTY"

        Dim sqlToSend As String
        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "Sending trust before residue information"
            Me.Refresh()

            localSequence = dbReader.GetInt32(0)
            If dbReader.GetValue(2).ToString = "" Then trustType = "" Else trustType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then naConfirmationFlag = "" Else naConfirmationFlag = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then
                FLITtype = ""
            ElseIf dbReader.GetValue(4).ToString = "Spouse/Partner" Then
                FLITtype = "Spouse"
            Else
                FLITtype = dbReader.GetValue(4).ToString
            End If

            If dbReader.GetValue(5).ToString = "" Then FLITname = "" Else FLITname = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then r2rFlag = "" Else r2rFlag = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then r2rYears = "" Else r2rYears = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then r2rTime = "" Else r2rTime = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then r2rLocation = "" Else r2rLocation = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then
                r2rWhoTo = ""
            ElseIf dbReader.GetValue(10).ToString = "Spouse/Partner" Then
                r2rWhoTo = "Spouse"
            Else
                r2rWhoTo = dbReader.GetValue(10).ToString
            End If

            If dbReader.GetValue(11).ToString = "" Then r2rNamed = "" Else r2rNamed = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then r2rBenefits = "" Else r2rBenefits = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then r2rMortgage = "" Else r2rMortgage = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then r2rFurniture = "" Else r2rFurniture = dbReader.GetValue(14).ToString

            If dbReader.GetValue(15).ToString = "" Then
                r2rBeneficialShare = ""
            ElseIf dbReader.GetValue(15).ToString = "Bloodline Children" Then
                r2rBeneficialShare = "Bloodline"
            ElseIf dbReader.GetValue(15).ToString = "Named Children" Then
                r2rBeneficialShare = "Named Children"
            ElseIf dbReader.GetValue(15).ToString = "Named Individual" Then
                r2rBeneficialShare = "Named Individual"
            Else
                r2rBeneficialShare = dbReader.GetValue(15).ToString

            End If

            If dbReader.GetValue(16).ToString = "" Then r2rCeaseRemarry = "" Else r2rCeaseRemarry = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then dtEstateType = "" Else dtEstateType = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then dtEstate = "" Else dtEstate = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then dtWishes = "" Else dtWishes = dbReader.GetValue(19).ToString

            If dbReader.GetValue(22).ToString = "" Then backstopCharityName = "" Else backstopCharityName = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then backstopCharityNumber = "" Else backstopCharityNumber = dbReader.GetValue(23).ToString

            sqlToSend = "insert into will_instruction_stage8_data (willClientSequence,trustType,naConfirmationFlag,FLITtype,FLITname,r2rFlag,r2rYears,r2rTime,r2rLocation,r2rWhoTo,r2rNamed,r2rBenefits,r2rMortgage,r2rFurniture,r2rBeneficialShare,r2rCeaseRemarry,dtEstateType,dtEstate,dtWishes,backstopCharityName,backstopCharityNumber) values (" + serverWillClientSequence + ",{{!" + trustType + "!}},{{!" + naConfirmationFlag + "!}},{{!" + FLITtype + "!}},{{!" + FLITname + "!}},{{!" + r2rFlag + "!}},{{!" + r2rYears + "!}},{{!" + r2rTime + "!}},{{!" + r2rLocation + "!}},{{!" + r2rWhoTo + "!}},{{!" + r2rNamed + "!}},{{!" + r2rBenefits + "!}},{{!" + r2rMortgage + "!}},{{!" + r2rFurniture + "!}},{{!" + r2rBeneficialShare + "!}},{{!" + r2rCeaseRemarry + "!}},{{!" + dtEstateType + "!}},{{!" + dtEstate + "!}},{{!" + dtWishes + "!}},{{!" + backstopCharityName + "!}},{{!" + backstopCharityNumber + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-GetSequence-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)

            '***** the responseFromServer contains the sequence of the inserted row. Saves having to run another query on the server - clever, eh?
            serverSequence = Val(responseFromServer)

            'now, run a query against the detail data table but with the new and old sequence numbers
            step12a(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step12b(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step12c(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step12d(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step12e(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step12f(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step12g(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step12h(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)

        End While


        dbReader.Close()
        dbConn.Close()

        If IsNumeric(responseFromServer) Or responseFromServer = "EMPTY" Then
            Return "OK"
        Else
            MsgBox("Step 12 =>" + responseFromServer)
            Return responseFromServer
        End If



    End Function

    Function step12a(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_bloodline where willClientSequence=" + localWillClientSequence + " and trustSequence=" + localTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading bloodline recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(6).ToString


            sqlToSend = "insert into will_instruction_stage8_data_bloodline (willClientSequence,trustSequence, beneficiaryType,atWhatage,atWhatAgeIssue,lapseOrIssue) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + lapseOrIssue + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12b(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_charity where willClientSequence=" + localWillClientSequence + " and trustSequence=" + localTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading charity recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim charityRegNumber As String
        Dim lapseOrIssue As String
        Dim percentage As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then charityRegNumber = "" Else charityRegNumber = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(7).ToString


            sqlToSend = "insert into will_instruction_stage8_data_charity (willClientSequence,trustSequence, beneficiaryType,beneficiaryName,charityRegNumber,lapseOrIssue,percentage) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + charityRegNumber + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12c(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_children where willClientSequence=" + localWillClientSequence + " and trustSequence=" + localTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading named children recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim percentage As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(8).ToString

            If dbReader.GetValue(11).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(15).ToString


            sqlToSend = "insert into will_instruction_stage8_data_children (willClientSequence,trustSequence, beneficiaryType,beneficiaryName,atWhatage,atWhatAgeIssue,lapseOrIssue,percentage, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12d(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_discretionary_trust where willClientSequence=" + localWillClientSequence + " and trustSequence=" + localTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading discretionary trust recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim percentage As String
        Dim primaryBeneficiaryFlag As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "bloodline children"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "named children"
                ElseIf beneficiaryType = "Named Individual" Then
                    beneficiaryType = "named individual"
                Else
                    'do nothing as currently entered data is correct
                End If
            End If
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then primaryBeneficiaryFlag = "" Else primaryBeneficiaryFlag = dbReader.GetValue(9).ToString

            If dbReader.GetValue(12).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(16).ToString

            If dbReader.GetValue(17).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(20).ToString


            sqlToSend = "insert into will_instruction_stage8_data_discretionary_trust (willClientSequence,trustSequence, beneficiaryType,beneficiaryName,lapseOrIssue,percentage,atWhatage,atWhatAgeIssue,primaryBeneficiaryFlag, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + primaryBeneficiaryFlag + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12e(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_FLIT where willClientSequence=" + localWillClientSequence + " and trustSequence=" + localTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading FLIT recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim firstOrDefault As String
        Dim beneficiaryName As String
        Dim lapseOrIssue As String
        Dim atWhatAge As String
        Dim atWhatAgeIssue As String
        Dim percentage As String
        Dim primaryBeneficiaryFlag As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString



            If dbReader.GetValue(4).ToString = "" Then firstOrDefault = "" Else firstOrDefault = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then atWhatAge = "" Else atWhatAge = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then primaryBeneficiaryFlag = "" Else primaryBeneficiaryFlag = dbReader.GetValue(10).ToString

            If dbReader.GetValue(13).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(17).ToString

            If dbReader.GetValue(18).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(21).ToString

            sqlToSend = "insert into will_instruction_stage8_data_FLIT (willClientSequence,trustSequence, beneficiaryType,firstOrDefault,beneficiaryName,lapseOrIssue,atWhatage,atWhatAgeIssue,percentage,primaryBeneficiaryFlag, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + firstOrDefault + "!}},{{!" + beneficiaryName + "!}},{{!" + lapseOrIssue + "!}},{{!" + atWhatAge + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + percentage + "!}},{{!" + primaryBeneficiaryFlag + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12f(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_individual where willClientSequence=" + localWillClientSequence + " and trustSequence=" + localTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading named individual recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim percentage As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(8).ToString


            If dbReader.GetValue(11).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(15).ToString


            If dbReader.GetValue(16).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(19).ToString

            sqlToSend = "insert into will_instruction_stage8_data_individual (willClientSequence,trustSequence, beneficiaryType,beneficiaryName,atWhatage,atWhatAgeIssue,lapseOrIssue,percentage, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12g(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_nilrate where willClientSequence=" + localWillClientSequence + " and trustSequence=" + localTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn
        'MsgBox(sql)
        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading NRB recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim firstOrDefault As String
        Dim beneficiaryName As String
        Dim lapseOrIssue As String
        Dim atWhatAge As String
        Dim atWhatAgeIssue As String
        Dim percentage As String
        Dim primaryBeneficiaryFlag As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "bloodline children"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "named children"
                ElseIf beneficiaryType = "Named Individual" Then
                    beneficiaryType = "named individual"
                Else
                    'do nothing as currently entered data is correct
                End If
            End If
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then atWhatAge = "" Else atWhatAge = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then primaryBeneficiaryFlag = "" Else primaryBeneficiaryFlag = dbReader.GetValue(9).ToString

            If dbReader.GetValue(12).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(16).ToString

            If dbReader.GetValue(17).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(20).ToString

            sqlToSend = "insert into will_instruction_stage8_data_nilrate (willClientSequence,trustSequence, beneficiaryType,beneficiaryName,lapseOrIssue,atWhatage,atWhatAgeIssue,percentage,primaryBeneficiaryFlag, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + lapseOrIssue + "!}},{{!" + atWhatAge + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + percentage + "!}},{{!" + primaryBeneficiaryFlag + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"

            'MsgBox(sqlToSend)
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step12h(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localTrustSequence As String, ByVal serverTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage8_data_trust where willClientSequence=" + localWillClientSequence + " and trustSequence='" + localTrustSequence + "'"
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading trust recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim percentage As String
        Dim primaryBeneficiaryFlag As String
        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "bloodline children"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "named children"
                ElseIf beneficiaryType = "Named Individual" Then
                    beneficiaryType = "named individual"
                Else
                    'do nothing as currently entered data is correct
                End If
            End If
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then primaryBeneficiaryFlag = "" Else primaryBeneficiaryFlag = dbReader.GetValue(9).ToString

            If dbReader.GetValue(12).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(16).ToString

            If dbReader.GetValue(17).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(20).ToString

            sqlToSend = "insert into will_instruction_stage8_data_trust (willClientSequence,trustSequence, beneficiaryType,beneficiaryName,lapseOrIssue,percentage,atWhatage,atWhatAgeIssue,primaryBeneficiaryFlag, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverTrustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + primaryBeneficiaryFlag + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step13(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        'this stage needs to insert trust into table, then remember the sequence number of that trust, so it can be used in the detail table
        'yhep, it's horrible!!!!

        'logic is this:
        'loop through all trusts in stage 8,
        'hit the first trust, REMEMBER sequence number insert it
        'find sequence number of new record
        'find all trust in detail table with old sequence number
        'insert those into server detail table
        'finish loop through all gifts


        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim localSequence As Int32
        Dim serverSequence As Int32
        Dim residueLevel As String
        Dim residueType As String
        Dim bespokeText As String
        Dim dtEstateType As String
        Dim dtEstate As String
        Dim dtWishes As String

        Dim r2rWhoTo As String
        Dim r2rNamed As String
        Dim r2rBenefits As String
        Dim r2rBeneficialShare As String
        Dim r2Occupy As String

        Dim backstopCharityName As String
        Dim backstopCharityNumber As String

        Dim responseFromServer As String = "EMPTY"

        Dim sqlToSend As String
        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "Sending trust after residue information"
            Me.Refresh()

            localSequence = dbReader.GetInt32(0)
            If dbReader.GetValue(2).ToString = "" Then residueLevel = "" Else residueLevel = dbReader.GetValue(2).ToString

            If dbReader.GetValue(3).ToString = "" Then
                residueType = ""
            Else
                residueType = dbReader.GetValue(3).ToString

                If residueType = "Residue To Individuals" Then
                    residueType = "Absolute Trust"
                ElseIf residueType = "Discretionary Trust" Then
                    residueType = "Discretionary Trust"
                ElseIf residueType = "Flexible Life Interest Trust" Then
                    residueType = "IPDI"
                Else
                    residueType = dbReader.GetValue(3).ToString
                End If
            End If


            If dbReader.GetValue(4).ToString = "" Then bespokeText = "" Else bespokeText = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then dtEstateType = "" Else dtEstateType = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then dtEstate = "" Else dtEstate = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then dtWishes = "" Else dtWishes = dbReader.GetValue(7).ToString

            If dbReader.GetValue(10).ToString = "" Then r2rWhoTo = "" Else r2rWhoTo = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then r2rNamed = "" Else r2rNamed = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then r2rBenefits = "" Else r2rBenefits = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then r2rBeneficialShare = "" Else r2rBeneficialShare = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then r2Occupy = "" Else r2Occupy = dbReader.GetValue(14).ToString

            If dbReader.GetValue(15).ToString = "" Then backstopCharityName = "" Else backstopCharityName = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then backstopCharityNumber = "" Else backstopCharityNumber = dbReader.GetValue(16).ToString

            If r2rBenefits = "Add Loan Facility" Then r2rBenefits = "LOAN"
            If r2rBenefits = "Add Capital Facilty" Then r2rBenefits = "CAPITAL"
            If r2rBenefits = "Add Both Loan & Capital" Then r2rBenefits = "BOTH"

            If r2rWhoTo = "Bloodline Children" Then r2rWhoTo = "Bloodline"

            sqlToSend = "insert into will_instruction_stage9_data (willClientSequence,residueLevel,residueType,bespokeText,dtEstateType,dtEstate,dtWishes,r2rWhoTo,r2rNamed,r2rBenefits,r2rBeneficialShare,r2Occupy,backstopCharityName,backstopCharityNumber) values (" + serverWillClientSequence + ",{{!" + residueLevel + "!}},{{!" + residueType + "!}},{{!" + bespokeText + "!}},{{!" + dtEstateType + "!}},{{!" + dtEstate + "!}},{{!" + dtWishes + "!}},{{!" + r2rWhoTo + "!}},{{!" + r2rNamed + "!}},{{!" + r2rBenefits + "!}},{{!" + r2rBeneficialShare + "!}},{{!" + r2Occupy + "!}},{{!" + backstopCharityName + "!}},{{!" + backstopCharityNumber + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-GetSequence-encrypted.php", "POST", sqlToSend)
            ' MsgBox("step 13 - " + responseFromServer)

            '***** the responseFromServer contains the sequence of the inserted row. Saves having to run another query on the server - clever, eh?
            serverSequence = Val(responseFromServer)

            'now, run a query against the detail data table but with the new and old sequence numbers
            step13a(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step13b(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step13c(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step13d(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step13e(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)
            step13f(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)


        End While

        dbReader.Close()
        dbConn.Close()
        If IsNumeric(responseFromServer) Or responseFromServer = "EMPTY" Then
            Return "OK"
        Else
            Return responseFromServer
        End If


        Return "OK" ' default


    End Function

    Function step13a(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localResidueSequence As String, ByVal serverResidueSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_absolute_trust where willClientSequence=" + localWillClientSequence + " and residueSequence=" + localResidueSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Sending absolute trust recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim charityNumber As String
        Dim lapseOrIssue As String
        Dim percentage As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim atDistribution As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String
        Dim beneficiaryDOB As String
        Dim charityName As String
        Dim thirtyDayFlag As String


        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String


        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "Spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "Bloodline"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "Named Children"

                Else
                    'do nothing as currently entered data is correct
                End If
            End If


            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then charityNumber = "" Else charityNumber = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then atDistribution = "" Else atDistribution = dbReader.GetValue(10).ToString

            If dbReader.GetValue(13).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryDOB = "" Else beneficiaryDOB = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then charityName = "" Else charityName = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then thirtyDayFlag = "" Else thirtyDayFlag = dbReader.GetValue(20).ToString

            If dbReader.GetValue(21).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(23).ToString
            If dbReader.GetValue(24).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(24).ToString

            sqlToSend = "insert into will_instruction_stage9_data_absolute_trust (willClientSequence,residueSequence, beneficiaryType, beneficiaryName, charityNumber, lapseOrIssue, percentage, atWhatage, atWhatAgeIssue,atDistribution, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,charityName,thirtyDayFlag,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverResidueSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + charityNumber + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + atDistribution + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + charityName + "!}},{{!" + thirtyDayFlag + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            ' MsgBox(sqlToSend)
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            ' responseFromServer = PHPPost(remoteSyncURL+"submitSQL-encrypted.php", "POST", sqlToSend)

            'We need the sequence number back so we can use it for the group definition table if appropriate
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-GetSequence-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

            If beneficiaryType = "A Group" Then
                'we need to transfer the group definition data up to the server, remembering the NEW sequence number from the response
                Dim serverAbsoluteTrustSequence As Int32 = Val(responseFromServer)
                Dim localAbsoluteTrustSequence As Int32 = Val(dbReader.GetValue(0).ToString)
                'now, run a query against the detail data table but with the new and old sequence numbers
                step13aa(apikey, localWillClientSequence, serverWillClientSequence, localAbsoluteTrustSequence, serverAbsoluteTrustSequence)
            End If

        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step13aa(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localAbsoluteTrustSequence As String, ByVal serverAbsoluteTrustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_group_definition where willClientSequence=" + localWillClientSequence + " and absoluteTrustSequence=" + localAbsoluteTrustSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Sending absolute trust group definitions"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim charityNumber As String
        Dim lapseOrIssue As String
        Dim percentage As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim atDistribution As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String
        Dim beneficiaryDOB As String
        Dim charityName As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        Dim serverResidueSequence = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "Spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "Bloodline"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "Named Children"

                Else
                    'do nothing as currently entered data is correct
                End If
            End If


            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then charityNumber = "" Else charityNumber = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then atDistribution = "" Else atDistribution = dbReader.GetValue(10).ToString

            If dbReader.GetValue(13).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryDOB = "" Else beneficiaryDOB = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then charityName = "" Else charityName = dbReader.GetValue(19).ToString

            If dbReader.GetValue(21).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(23).ToString
            If dbReader.GetValue(24).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(24).ToString

            sqlToSend = "insert into will_instruction_stage9_data_group_definition (willClientSequence,residueSequence, beneficiaryType, beneficiaryName, charityNumber, lapseOrIssue, percentage, atWhatage, atWhatAgeIssue,atDistribution, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,charityName,absoluteTrustSequence,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverResidueSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + charityNumber + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + atDistribution + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + charityName + "!}},{{!" + serverAbsoluteTrustSequence + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            '  MsgBox(sqlToSend)
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)

        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step13b(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localResidueSequence As String, ByVal serverResidueSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_discretionary_trust where willClientSequence=" + localWillClientSequence + " and residueSequence=" + localResidueSequence
        Command.CommandText = sql
        Command.Connection = dbConn
        '  MsgBox(sql)
        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Sending discretionary trust recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim charityNumber As String
        Dim lapseOrIssue As String
        Dim percentage As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim primaryBeneficiaryFlag As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "bloodline children"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "named children"
                ElseIf beneficiaryType = "Named Individual" Then
                    beneficiaryType = "named individual"
                Else
                    'do nothing as currently entered data is correct
                End If
            End If

            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then charityNumber = "" Else charityNumber = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then primaryBeneficiaryFlag = "" Else primaryBeneficiaryFlag = dbReader.GetValue(10).ToString

            If dbReader.GetValue(13).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(17).ToString

            If dbReader.GetValue(18).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(21).ToString

            sqlToSend = "insert into will_instruction_stage9_data_discretionary_trust (willClientSequence,residueSequence, beneficiaryType, beneficiaryName, charityNumber, lapseOrIssue, percentage, atWhatage, atWhatAgeIssue,primaryBeneficiaryFlag, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverResidueSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + charityNumber + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + primaryBeneficiaryFlag + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}} )"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            '  MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step13c(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localResidueSequence As String, ByVal serverResidueSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_IPDI_bloodline where willClientSequence=" + localWillClientSequence + " and residueSequence=" + localResidueSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Sending IPDI Bloodline"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String


        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(6).ToString


            sqlToSend = "insert into will_instruction_stage9_data_IPDI_bloodline (willClientSequence,residueSequence, beneficiaryType, atWhatage, atWhatAgeIssue,lapseOrIssue) values (" + serverWillClientSequence + ",{{!" + serverResidueSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + lapseOrIssue + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)

    End Function

    Function step13d(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localResidueSequence As String, ByVal serverResidueSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_IPDI_children where willClientSequence=" + localWillClientSequence + " and residueSequence=" + localResidueSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Sending IPDI Children"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim percentage As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(8).ToString

            If dbReader.GetValue(11).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(15).ToString


            If dbReader.GetValue(16).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(19).ToString


            sqlToSend = "insert into will_instruction_stage9_data_IPDI_children (willClientSequence,residueSequence, beneficiaryType,beneficiaryName, atWhatage, atWhatAgeIssue,lapseOrIssue,percentage, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverResidueSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)

    End Function

    Function step13e(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localResidueSequence As String, ByVal serverResidueSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_IPDI_individual where willClientSequence=" + localWillClientSequence + " and residueSequence=" + localResidueSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Sending IPDI Children"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim percentage As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String

        Dim responseFromServer As String = ""
        Dim sqlToSend As String


        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(8).ToString

            If dbReader.GetValue(11).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(15).ToString


            If dbReader.GetValue(16).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(19).ToString

            sqlToSend = "insert into will_instruction_stage9_data_IPDI_individual (willClientSequence,residueSequence, beneficiaryType,beneficiaryName, atWhatage, atWhatAgeIssue,lapseOrIssue,percentage, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone) values (" + serverWillClientSequence + ",{{!" + serverResidueSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)

    End Function

    Function step13f(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localResidueSequence As String, ByVal serverResidueSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_IPDI_trust where willClientSequence=" + localWillClientSequence + " and residueSequence='" + localResidueSequence + "'"
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Sending IPDI discretionary trust recipients"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim lapseOrIssue As String
        Dim percentage As String
        Dim atWhatage As String
        Dim atWhatAgeIssue As String
        Dim primaryBeneficiaryFlag As String

        Dim relationToClient1 As String
        Dim relationToClient2 As String
        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String


        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String


        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then
                beneficiaryType = ""
            Else
                beneficiaryType = dbReader.GetValue(3).ToString

                If beneficiaryType = "Spouse/Partner" Then
                    beneficiaryType = "spouse"
                ElseIf beneficiaryType = "bloodline children" Or beneficiaryType = "Bloodline Children" Then
                    beneficiaryType = "bloodline children"
                ElseIf beneficiaryType = "Named Child" Or beneficiaryType = "Named Children" Then
                    beneficiaryType = "named children"
                ElseIf beneficiaryType = "Named Individual" Then
                    beneficiaryType = "named individual"
                Else
                    'do nothing as currently entered data is correct
                End If
            End If

            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then atWhatage = "" Else atWhatage = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then primaryBeneficiaryFlag = "" Else primaryBeneficiaryFlag = dbReader.GetValue(9).ToString

            If dbReader.GetValue(12).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(16).ToString


            If dbReader.GetValue(17).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(20).ToString

            sqlToSend = "insert into will_instruction_stage9_data_IPDI_trust (willClientSequence,residueSequence, beneficiaryType, beneficiaryName,lapseOrIssue, percentage, atWhatage, atWhatAgeIssue,primaryBeneficiaryFlag, relationshipClient1,relationshipClient2,beneficiaryTitle,beneficiaryForenames,beneficiarySurname,beneficiaryAddress,beneficiaryPostcode,beneficiaryEmail,beneficiaryPhone ) values (" + serverWillClientSequence + ",{{!" + serverResidueSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + atWhatage + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + primaryBeneficiaryFlag + "!}},{{!" + relationToClient1 + "!}},{{!" + relationToClient2 + "!}},{{!" + beneficiaryTitle + "!}},{{!" + beneficiaryForenames + "!}},{{!" + beneficiarySurname + "!}},{{!" + beneficiaryAddress + "!}},{{!" + beneficiaryPostcode + "!}},{{!" + beneficiaryEmail + "!}},{{!" + beneficiaryPhone + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step14(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage10_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading attendance notes"
            Me.Refresh()

            'clean the local data
            Dim notes As String
            Dim q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20 As String
            Dim q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick As String

            If dbReader.GetValue(2).ToString = "" Then notes = "" Else notes = dbReader.GetValue(2).ToString
            If dbReader.GetValue(4).ToString = "" Then q1 = "" Else q1 = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then q2 = "" Else q2 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then q3 = "" Else q3 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then q4 = "" Else q4 = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then q5 = "" Else q5 = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then q6 = "" Else q6 = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then q7 = "" Else q7 = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then q8 = "" Else q8 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then q9 = "" Else q9 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then q10 = "" Else q10 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then q11 = "" Else q11 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then q12 = "" Else q12 = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then q13 = "" Else q13 = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then q14 = "" Else q14 = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then q15 = "" Else q15 = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then q16 = "" Else q16 = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then q17 = "" Else q17 = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then q18 = "" Else q18 = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then q19 = "" Else q19 = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then q20 = "" Else q20 = dbReader.GetValue(23).ToString

            If dbReader.GetValue(24).ToString = "" Then q1tick = "" Else q1tick = dbReader.GetValue(24).ToString
            If dbReader.GetValue(25).ToString = "" Then q2tick = "" Else q2tick = dbReader.GetValue(25).ToString
            If dbReader.GetValue(26).ToString = "" Then q3tick = "" Else q3tick = dbReader.GetValue(26).ToString
            If dbReader.GetValue(27).ToString = "" Then q4tick = "" Else q4tick = dbReader.GetValue(27).ToString
            If dbReader.GetValue(28).ToString = "" Then q5tick = "" Else q5tick = dbReader.GetValue(28).ToString
            If dbReader.GetValue(29).ToString = "" Then q6tick = "" Else q6tick = dbReader.GetValue(29).ToString
            If dbReader.GetValue(30).ToString = "" Then q7tick = "" Else q7tick = dbReader.GetValue(30).ToString
            If dbReader.GetValue(31).ToString = "" Then q8tick = "" Else q8tick = dbReader.GetValue(31).ToString
            If dbReader.GetValue(32).ToString = "" Then q9tick = "" Else q9tick = dbReader.GetValue(32).ToString
            If dbReader.GetValue(33).ToString = "" Then q10tick = "" Else q10tick = dbReader.GetValue(33).ToString
            If dbReader.GetValue(34).ToString = "" Then q11tick = "" Else q11tick = dbReader.GetValue(34).ToString
            If dbReader.GetValue(35).ToString = "" Then q12tick = "" Else q12tick = dbReader.GetValue(35).ToString
            If dbReader.GetValue(36).ToString = "" Then q13tick = "" Else q13tick = dbReader.GetValue(36).ToString
            If dbReader.GetValue(37).ToString = "" Then q14tick = "" Else q14tick = dbReader.GetValue(37).ToString
            If dbReader.GetValue(38).ToString = "" Then q15tick = "" Else q15tick = dbReader.GetValue(38).ToString
            If dbReader.GetValue(39).ToString = "" Then q16tick = "" Else q16tick = dbReader.GetValue(39).ToString
            If dbReader.GetValue(40).ToString = "" Then q17tick = "" Else q17tick = dbReader.GetValue(40).ToString
            If dbReader.GetValue(41).ToString = "" Then q18tick = "" Else q18tick = dbReader.GetValue(41).ToString
            If dbReader.GetValue(42).ToString = "" Then q19tick = "" Else q19tick = dbReader.GetValue(42).ToString
            If dbReader.GetValue(43).ToString = "" Then q20tick = "" Else q20tick = dbReader.GetValue(43).ToString


            Dim sqlToSend As String = "insert into will_instruction_stage10_data (willClientSequence,notes, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20,q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick ) values (" + serverWillClientSequence + ",{{!" + notes + "!}},{{!" + q1 + "!}},{{!" + q2 + "!}},{{!" + q3 + "!}},{{!" + q4 + "!}},{{!" + q5 + "!}},{{!" + q6 + "!}},{{!" + q7 + "!}},{{!" + q8 + "!}},{{!" + q9 + "!}},{{!" + q10 + "!}},{{!" + q11 + "!}},{{!" + q12 + "!}},{{!" + q13 + "!}},{{!" + q14 + "!}},{{!" + q15 + "!}},{{!" + q16 + "!}},{{!" + q17 + "!}},{{!" + q18 + "!}},{{!" + q19 + "!}},{{!" + q20 + "!}},{{!" + q1tick + "!}},{{!" + q2tick + "!}},{{!" + q3tick + "!}},{{!" + q4tick + "!}},{{!" + q5tick + "!}},{{!" + q6tick + "!}},{{!" + q7tick + "!}},{{!" + q8tick + "!}},{{!" + q9tick + "!}},{{!" + q10tick + "!}},{{!" + q11tick + "!}},{{!" + q12tick + "!}},{{!" + q13tick + "!}},{{!" + q14tick + "!}},{{!" + q15tick + "!}},{{!" + q16tick + "!}},{{!" + q17tick + "!}},{{!" + q18tick + "!}},{{!" + q19tick + "!}},{{!" + q20tick + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            '    MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            '   MsgBox(responseFromServer)


            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step15(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        'not a required function of this installation
        Return "OK"

    End Function

    Function step16(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage12_data where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim whichClient As String
        Dim mainResidence As String
        Dim otherProperties As String
        Dim overseasProperties As String
        Dim bankAccounts As String
        Dim investments As String
        Dim lifePolicies As String
        Dim pension As String
        Dim businessAssets As String
        Dim businessType As String
        Dim businessValue As String
        Dim partnershipAgreement As String
        Dim shares As String
        Dim otherPartnersFlag As String
        Dim natureOfBusiness As String
        Dim totalBusinessAssets As String
        Dim broughtForward As String
        Dim mortgages As String
        Dim loans As String
        Dim liabilities As String
        Dim additionalInformation As String


        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "Transferring financial information"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then mainResidence = "0" Else mainResidence = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then otherProperties = "0" Else otherProperties = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then overseasProperties = "0" Else overseasProperties = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then bankAccounts = "0" Else bankAccounts = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then investments = "0" Else investments = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then lifePolicies = "0" Else lifePolicies = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then pension = "0" Else pension = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then businessAssets = "0" Else businessAssets = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then businessType = "" Else businessType = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then businessValue = "0" Else businessValue = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then partnershipAgreement = "0" Else partnershipAgreement = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then shares = "0" Else shares = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then otherPartnersFlag = "" Else otherPartnersFlag = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then natureOfBusiness = "0" Else natureOfBusiness = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then totalBusinessAssets = "0" Else totalBusinessAssets = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then broughtForward = "0" Else broughtForward = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then mortgages = "0" Else mortgages = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then loans = "0" Else loans = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then liabilities = "0" Else liabilities = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then additionalInformation = "" Else additionalInformation = dbReader.GetValue(22).ToString

            sqlToSend = "insert into will_instruction_stage12_data (willClientSequence,whichClient, mainResidence, otherProperties, overseasProperties,bankAccounts,investments,lifePolicies,pension,businessAssets,businessType,businessValue,partnershipAgreement,shares,otherPartnersFlag,natureOfBusiness,totalBusinessAssets,broughtForward,mortgages,loans,liabilities,additionalInformation) values  (" + serverWillClientSequence + ",{{!" + whichClient + "!}},{{!" + mainResidence + "!}},{{!" + otherProperties + "!}},{{!" + overseasProperties + "!}},{{!" + bankAccounts + "!}},{{!" + investments + "!}},{{!" + lifePolicies + "!}},{{!" + pension + "!}},{{!" + businessAssets + "!}},{{!" + businessType + "!}},{{!" + businessValue + "!}},{{!" + partnershipAgreement + "!}},{{!" + shares + "!}},{{!" + otherPartnersFlag + "!}},{{!" + natureOfBusiness + "!}},{{!" + totalBusinessAssets + "!}},{{!" + broughtForward + "!}},{{!" + mortgages + "!}},{{!" + loans + "!}},{{!" + liabilities + "!}},{{!" + additionalInformation + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step17(apikey As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        Dim toSend As String
        Dim responsefromserver As String
        toSend = "apikey=" + apikey + "&refNumber=" + serverWillClientSequence

        responsefromserver = PHPPost(remoteSyncURL + "workout-stages.php", "POST", toSend)
        Return responsefromserver


    End Function

    Function step18(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_attendance_notes where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        While dbReader.Read()
            'update audit log
            Me.auditlog.Text = "Uploading LPA attendance notes"
            Me.Refresh()

            'clean the local data
            Dim notes As String
            Dim q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20 As String
            Dim q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick As String
            Dim whichClient, whichDocument As String


            If dbReader.GetValue(2).ToString = "" Then notes = "" Else notes = dbReader.GetValue(2).ToString
            If dbReader.GetValue(4).ToString = "" Then q1 = "" Else q1 = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then q2 = "" Else q2 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then q3 = "" Else q3 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then q4 = "" Else q4 = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then q5 = "" Else q5 = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then q6 = "" Else q6 = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then q7 = "" Else q7 = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then q8 = "" Else q8 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then q9 = "" Else q9 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then q10 = "" Else q10 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then q11 = "" Else q11 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then q12 = "" Else q12 = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then q13 = "" Else q13 = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then q14 = "" Else q14 = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then q15 = "" Else q15 = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then q16 = "" Else q16 = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then q17 = "" Else q17 = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then q18 = "" Else q18 = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then q19 = "" Else q19 = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then q20 = "" Else q20 = dbReader.GetValue(23).ToString

            If dbReader.GetValue(24).ToString = "" Then q1tick = "" Else q1tick = dbReader.GetValue(24).ToString
            If dbReader.GetValue(25).ToString = "" Then q2tick = "" Else q2tick = dbReader.GetValue(25).ToString
            If dbReader.GetValue(26).ToString = "" Then q3tick = "" Else q3tick = dbReader.GetValue(26).ToString
            If dbReader.GetValue(27).ToString = "" Then q4tick = "" Else q4tick = dbReader.GetValue(27).ToString
            If dbReader.GetValue(28).ToString = "" Then q5tick = "" Else q5tick = dbReader.GetValue(28).ToString
            If dbReader.GetValue(29).ToString = "" Then q6tick = "" Else q6tick = dbReader.GetValue(29).ToString
            If dbReader.GetValue(30).ToString = "" Then q7tick = "" Else q7tick = dbReader.GetValue(30).ToString
            If dbReader.GetValue(31).ToString = "" Then q8tick = "" Else q8tick = dbReader.GetValue(31).ToString
            If dbReader.GetValue(32).ToString = "" Then q9tick = "" Else q9tick = dbReader.GetValue(32).ToString
            If dbReader.GetValue(33).ToString = "" Then q10tick = "" Else q10tick = dbReader.GetValue(33).ToString
            If dbReader.GetValue(34).ToString = "" Then q11tick = "" Else q11tick = dbReader.GetValue(34).ToString
            If dbReader.GetValue(35).ToString = "" Then q12tick = "" Else q12tick = dbReader.GetValue(35).ToString
            If dbReader.GetValue(36).ToString = "" Then q13tick = "" Else q13tick = dbReader.GetValue(36).ToString
            If dbReader.GetValue(37).ToString = "" Then q14tick = "" Else q14tick = dbReader.GetValue(37).ToString
            If dbReader.GetValue(38).ToString = "" Then q15tick = "" Else q15tick = dbReader.GetValue(38).ToString
            If dbReader.GetValue(39).ToString = "" Then q16tick = "" Else q16tick = dbReader.GetValue(39).ToString
            If dbReader.GetValue(40).ToString = "" Then q17tick = "" Else q17tick = dbReader.GetValue(40).ToString
            If dbReader.GetValue(41).ToString = "" Then q18tick = "" Else q18tick = dbReader.GetValue(41).ToString
            If dbReader.GetValue(42).ToString = "" Then q19tick = "" Else q19tick = dbReader.GetValue(42).ToString
            If dbReader.GetValue(43).ToString = "" Then q20tick = "" Else q20tick = dbReader.GetValue(43).ToString

            If dbReader.GetValue(44).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(44).ToString
            If dbReader.GetValue(45).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(45).ToString

            Dim sqlToSend As String = "insert into lpa_attendance_notes (willClientSequence,notes, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20,q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick,whichClient,whichDocument ) values (" + serverWillClientSequence + ",{{!" + notes + "!}},{{!" + q1 + "!}},{{!" + q2 + "!}},{{!" + q3 + "!}},{{!" + q4 + "!}},{{!" + q5 + "!}},{{!" + q6 + "!}},{{!" + q7 + "!}},{{!" + q8 + "!}},{{!" + q9 + "!}},{{!" + q10 + "!}},{{!" + q11 + "!}},{{!" + q12 + "!}},{{!" + q13 + "!}},{{!" + q14 + "!}},{{!" + q15 + "!}},{{!" + q16 + "!}},{{!" + q17 + "!}},{{!" + q18 + "!}},{{!" + q19 + "!}},{{!" + q20 + "!}},{{!" + q1tick + "!}},{{!" + q2tick + "!}},{{!" + q3tick + "!}},{{!" + q4tick + "!}},{{!" + q5tick + "!}},{{!" + q6tick + "!}},{{!" + q7tick + "!}},{{!" + q8tick + "!}},{{!" + q9tick + "!}},{{!" + q10tick + "!}},{{!" + q11tick + "!}},{{!" + q12tick + "!}},{{!" + q13tick + "!}},{{!" + q14tick + "!}},{{!" + q15tick + "!}},{{!" + q16tick + "!}},{{!" + q17tick + "!}},{{!" + q18tick + "!}},{{!" + q19tick + "!}},{{!" + q20tick + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            '  MsgBox(responseFromServer)

        End While

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step19(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_attorneys where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim attorneyType As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim howToAct As String
        Dim howToActNotes As String
        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "LPA Attorneys"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then attorneyType = "" Else attorneyType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then howToAct = "" Else howToAct = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then howToActNotes = "" Else howToActNotes = dbReader.GetValue(6).ToString

            sqlToSend = "insert into lpa_attorneys (willClientSequence,attorneyType, whichClient, whichDocument,howToAct,howToActNotes) values  (" + serverWillClientSequence + ",{{!" + attorneyType + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + howToAct + "!}},{{!" + howToActNotes + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step20(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_attorneys_detail where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading lpa attorney details"
        Me.Refresh()

        'clean the local data
        Dim attorneyType As String
        Dim attorneyNames As String
        Dim attorneyPhone As String
        Dim attorneyRelationshipClient1 As String
        Dim attorneyRelationshipClient2 As String
        Dim attorneyAddress As String
        Dim attorneyEmail As String
        Dim howToAct As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim occupation As String
        Dim attorneyDOB As String
        Dim attorneyTitle As String
        Dim attorneySurname As String
        Dim attorneyPostcode As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then attorneyType = "" Else attorneyType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then attorneyNames = "" Else attorneyNames = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then attorneyPhone = "" Else attorneyPhone = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then attorneyRelationshipClient1 = "" Else attorneyRelationshipClient1 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then attorneyRelationshipClient2 = "" Else attorneyRelationshipClient2 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then attorneyAddress = "" Else attorneyAddress = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then attorneyEmail = "" Else attorneyEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then howToAct = "" Else howToAct = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then occupation = "" Else occupation = dbReader.GetValue(12).ToString

            If dbReader.GetValue(13).ToString = "" Then
                attorneyDOB = "1900-01-01"
            Else
                attorneyDOB = dbReader.GetDateTime(13).Year.ToString + "-" + dbReader.GetDateTime(13).Month.ToString + "-" + dbReader.GetDateTime(13).Day.ToString
            End If

            If dbReader.GetValue(14).ToString = "" Then attorneyTitle = "" Else attorneyTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then attorneySurname = "" Else attorneySurname = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then attorneyPostcode = "" Else attorneyPostcode = dbReader.GetValue(16).ToString


            sqlToSend = "insert into lpa_attorneys_detail (willClientSequence,attorneyType,attorneyNames,attorneyPhone,attorneyRelationshipClient1,attorneyRelationshipClient2,attorneyAddress,attorneyEmail,howToAct,whichClient,whichDocument,occupation,attorneyDOB, attorneyTitle,attorneySurname,attorneyPostcode) values (" + serverWillClientSequence + ",{{!" + attorneyType + "!}},{{!" + attorneyNames + "!}},{{!" + attorneyPhone + "!}},{{!" + attorneyRelationshipClient1 + "!}},{{!" + attorneyRelationshipClient2 + "!}},{{!" + attorneyAddress + "!}},{{!" + attorneyEmail + "!}},{{!" + howToAct + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + occupation + "!}},{{!" + attorneyDOB + "!}}, {{!" + attorneyTitle + "!}},{{!" + attorneySurname + "!}},{{!" + attorneyPostcode + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step21(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_certificate_provider where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim certificateType As String
        Dim whichClient As String
        Dim whichDocument As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "LPA Certificate Provider"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then certificateType = "" Else certificateType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(4).ToString

            sqlToSend = "insert into lpa_certificate_provider (willClientSequence,certificateType, whichClient, whichDocument) values  (" + serverWillClientSequence + ",{{!" + certificateType + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step22(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_certificate_provider_details where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading lpa certificate provider details"
        Me.Refresh()

        'clean the local data
        Dim certificateType As String
        Dim certificateNames As String
        Dim certificatePhone As String
        Dim certificateRelationshipClient1 As String
        Dim certificateRelationshipClient2 As String
        Dim certificateAddress As String
        Dim certificateEmail As String
        Dim howToAct As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim occupation As String
        Dim certificateTitle As String
        Dim certificateSurname As String
        Dim certificatePostcode As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then certificateType = "" Else certificateType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then certificateNames = "" Else certificateNames = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then certificatePhone = "" Else certificatePhone = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then certificateRelationshipClient1 = "" Else certificateRelationshipClient1 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then certificateRelationshipClient2 = "" Else certificateRelationshipClient2 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then certificateAddress = "" Else certificateAddress = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then certificateEmail = "" Else certificateEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then howToAct = "" Else howToAct = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then occupation = "" Else occupation = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then certificateTitle = "" Else certificateTitle = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then certificateSurname = "" Else certificateSurname = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then certificatePostcode = "" Else certificatePostcode = dbReader.GetValue(15).ToString

            sqlToSend = "insert into lpa_certificate_provider_details (willClientSequence,certificateType,certificateNames,certificatePhone,certificateRelationshipClient1,certificateRelationshipClient2,certificateAddress,certificateEmail,howToAct,whichClient,whichDocument,occupation, certificateTitle, certificateSurname,certificatePostcode) values (" + serverWillClientSequence + ",{{!" + certificateType + "!}},{{!" + certificateNames + "!}},{{!" + certificatePhone + "!}},{{!" + certificateRelationshipClient1 + "!}},{{!" + certificateRelationshipClient2 + "!}},{{!" + certificateAddress + "!}},{{!" + certificateEmail + "!}},{{!" + howToAct + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + occupation + "!}}, {{!" + certificateTitle + "!}}, {{!" + certificateSurname + "!}},{{!" + certificatePostcode + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step23(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_documents where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim client1PFA As String
        Dim client2PFA As String
        Dim mirrorPFA As String
        Dim client1HW As String
        Dim client2HW As String
        Dim mirrorHW As String
        Dim status As String
        Dim statusDate As String
        Dim client1B As String
        Dim client2B As String
        Dim mirrorB As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "LPA Documents"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then client1PFA = "" Else client1PFA = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then client2PFA = "" Else client2PFA = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then mirrorPFA = "" Else mirrorPFA = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then client1HW = "" Else client1HW = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then client2HW = "" Else client2HW = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then mirrorHW = "" Else mirrorHW = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then status = "" Else status = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then
                statusDate = "1900-01-01"
            Else
                statusDate = dbReader.GetDateTime(9).Year.ToString + "-" + dbReader.GetDateTime(9).Month.ToString + "-" + dbReader.GetDateTime(9).Day.ToString
            End If

            If dbReader.GetValue(10).ToString = "" Then client1B = "" Else client1B = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then client2B = "" Else client2B = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then mirrorB = "" Else mirrorB = dbReader.GetValue(12).ToString

            sqlToSend = "insert into lpa_documents (willClientSequence,client1PFA, client2PFA, mirrorPFA,client1HW,client2HW,mirrorHW,status,statusDate,client1B,client2B,mirrorB) values  (" + serverWillClientSequence + ",{{!" + client1PFA + "!}},{{!" + client2PFA + "!}},{{!" + mirrorPFA + "!}},{{!" + client1HW + "!}},{{!" + client2HW + "!}},{{!" + mirrorHW + "!}},{{!" + status + "!}},{{!" + statusDate + "!}},{{!" + client1B + "!}},{{!" + client2B + "!}},{{!" + mirrorB + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step24(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_persons_to_be_told where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading lpa persons to be told"
        Me.Refresh()

        'clean the local data
        Dim personType As String
        Dim personNames As String
        Dim personPhone As String
        Dim personRelationshipClient1 As String
        Dim personRelationshipClient2 As String
        Dim personAddress As String
        Dim personEmail As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim personTitle As String
        Dim personSurname As String
        Dim personPostcode As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then personType = "" Else personType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then personNames = "" Else personNames = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then personPhone = "" Else personPhone = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then personRelationshipClient1 = "" Else personRelationshipClient1 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then personRelationshipClient2 = "" Else personRelationshipClient2 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then personAddress = "" Else personAddress = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then personEmail = "" Else personEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then personTitle = "" Else personTitle = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then personSurname = "" Else personSurname = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then personPostcode = "" Else personPostcode = dbReader.GetValue(13).ToString


            sqlToSend = "insert into lpa_persons_to_be_told (willClientSequence,personType,personNames,personPhone,personRelationshipClient1,personRelationshipClient2,personAddress,personEmail,whichClient,whichDocument,personTitle,personSurname,personPostcode) values (" + serverWillClientSequence + ",{{!" + personType + "!}},{{!" + personNames + "!}},{{!" + personPhone + "!}},{{!" + personRelationshipClient1 + "!}},{{!" + personRelationshipClient2 + "!}},{{!" + personAddress + "!}},{{!" + personEmail + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + personTitle + "!}},{{!" + personSurname + "!}},{{!" + personPostcode + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step24a(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_preferences where willClientSequence=" + localWillClientSequence
        '  MsgBox(sql)
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading lpa preferences"
        Me.Refresh()

        'clean the local data
        Dim attorneyType As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim mustDoNotes As String
        Dim shouldDoNotes As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then attorneyType = "" Else attorneyType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then mustDoNotes = "" Else mustDoNotes = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then shouldDoNotes = "" Else shouldDoNotes = dbReader.GetValue(6).ToString

            sqlToSend = "insert into lpa_preferences (willClientSequence,attorneyType,whichClient,whichDocument,mustDoNotes,shouldDoNotes) values (" + serverWillClientSequence + ",{{!" + attorneyType + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + mustDoNotes + "!}},{{!" + shouldDoNotes + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step24b(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_register where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading lpa registration information"
        Me.Refresh()

        'clean the local data
        Dim whichClient As String
        Dim whichDocument As String
        Dim registerType As String
        Dim registerNames As String
        Dim registerPhone As String
        Dim registerAddress As String
        Dim registerEmail As String
        Dim corresType As String
        Dim corresNames As String
        Dim corresPhone As String
        Dim corresAddress As String
        Dim corresEmail As String
        Dim repeatFlag As String
        Dim byPost As String
        Dim byEmail As String
        Dim byPhone As String
        Dim Welsh As String
        Dim registerTitle As String
        Dim registerSurname As String
        Dim registerPostcode As String
        Dim corresTitle As String
        Dim corresSurname As String
        Dim corresPostcode As String
        Dim witnessTitle As String
        Dim witnessNames As String
        Dim witnessSurname As String
        Dim witnessAddress As String
        Dim witnessPostcode As String
        Dim witnessPhone As String
        Dim witnessEmail As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then registerType = "" Else registerType = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then registerNames = "" Else registerNames = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then registerPhone = "" Else registerPhone = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then registerAddress = "" Else registerAddress = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then registerEmail = "" Else registerEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then corresType = "" Else corresType = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then corresNames = "" Else corresNames = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then corresPhone = "" Else corresPhone = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then corresAddress = "" Else corresAddress = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then corresEmail = "" Else corresEmail = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then repeatFlag = "" Else repeatFlag = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then byPost = "" Else byPost = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then byEmail = "" Else byEmail = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then byPhone = "" Else byPhone = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then Welsh = "" Else Welsh = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then registerTitle = "" Else registerTitle = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then registerSurname = "" Else registerSurname = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then registerPostcode = "" Else registerPostcode = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then corresTitle = "" Else corresTitle = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then corresSurname = "" Else corresSurname = dbReader.GetValue(23).ToString
            If dbReader.GetValue(24).ToString = "" Then corresPostcode = "" Else corresPostcode = dbReader.GetValue(24).ToString
            If dbReader.GetValue(25).ToString = "" Then witnessTitle = "" Else witnessTitle = dbReader.GetValue(25).ToString
            If dbReader.GetValue(26).ToString = "" Then witnessNames = "" Else witnessNames = dbReader.GetValue(26).ToString
            If dbReader.GetValue(27).ToString = "" Then witnessSurname = "" Else witnessSurname = dbReader.GetValue(27).ToString
            If dbReader.GetValue(28).ToString = "" Then witnessAddress = "" Else witnessAddress = dbReader.GetValue(28).ToString
            If dbReader.GetValue(29).ToString = "" Then witnessPostcode = "" Else witnessPostcode = dbReader.GetValue(29).ToString
            If dbReader.GetValue(30).ToString = "" Then witnessPhone = "" Else witnessPhone = dbReader.GetValue(30).ToString
            If dbReader.GetValue(31).ToString = "" Then witnessEmail = "" Else witnessEmail = dbReader.GetValue(31).ToString



            sqlToSend = "insert into lpa_register (willClientSequence,whichClient,whichDocument,registerType ,registerNames ,registerPhone ,registerAddress ,registerEmail ,corresType ,corresNames ,corresPhone ,corresAddress ,corresEmail ,repeatFlag ,byPost ,byEmail ,byPhone ,Welsh ,registerTitle ,registerSurname ,registerPostcode ,corresTitle ,corresSurname ,corresPostcode ,witnessTitle ,witnessNames ,witnessSurname ,witnessAddress ,witnessPostcode ,witnessPhone ,witnessEmail) values (" + serverWillClientSequence + ",{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + registerType + "!}} ,{{!" + registerNames + "!}} ,{{!" + registerPhone + "!}} ,{{!" + registerAddress + "!}} ,{{!" + registerEmail + "!}} ,{{!" + corresType + "!}} ,{{!" + corresNames + "!}} ,{{!" + corresPhone + "!}} ,{{!" + corresAddress + "!}} ,{{!" + corresEmail + "!}},{{!" + repeatFlag + "!}} ,{{!" + byPost + "!}} ,{{!" + byEmail + "!}} ,{{!" + byPhone + "!}} ,{{!" + Welsh + "!}} ,{{!" + registerTitle + "!}} ,{{!" + registerSurname + "!}} ,{{!" + registerPostcode + "!}} ,{{!" + corresTitle + "!}} ,{{!" + corresSurname + "!}} ,{{!" + corresPostcode + "!}} ,{{!" + witnessTitle + "!}} ,{{!" + witnessNames + "!}} ,{{!" + witnessSurname + "!}} ,{{!" + witnessAddress + "!}},{{!" + witnessPostcode + "!}} ,{{!" + witnessPhone + "!}},{{!" + witnessEmail + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step25(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_replacement_attorneys where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim attorneyType As String
        Dim whichClient As String
        Dim whichDocument As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "LPA Replacement Attorneys"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then attorneyType = "" Else attorneyType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(4).ToString

            sqlToSend = "insert into lpa_replacement_attorneys (willClientSequence,attorneyType, whichClient, whichDocument) values  (" + serverWillClientSequence + ",{{!" + attorneyType + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step26(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from lpa_replacement_attorneys_detail where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading lpa replacement attorney details"
        Me.Refresh()

        'clean the local data
        Dim attorneyType As String
        Dim attorneyNames As String
        Dim attorneyPhone As String
        Dim attorneyRelationshipClient1 As String
        Dim attorneyRelationshipClient2 As String
        Dim attorneyAddress As String
        Dim attorneyEmail As String
        Dim howToAct As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim occupation As String
        Dim attorneyDOB As String
        Dim attorneyTitle As String
        Dim attorneySurname As String
        Dim attorneyPostcode As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then attorneyType = "" Else attorneyType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then attorneyNames = "" Else attorneyNames = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then attorneyPhone = "" Else attorneyPhone = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then attorneyRelationshipClient1 = "" Else attorneyRelationshipClient1 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then attorneyRelationshipClient2 = "" Else attorneyRelationshipClient2 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then attorneyAddress = "" Else attorneyAddress = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then attorneyEmail = "" Else attorneyEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then howToAct = "" Else howToAct = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then occupation = "" Else occupation = dbReader.GetValue(12).ToString

            If dbReader.GetValue(13).ToString = "" Then
                attorneyDOB = "1900-01-01"
            Else
                attorneyDOB = dbReader.GetDateTime(13).Year.ToString + "-" + dbReader.GetDateTime(13).Month.ToString + "-" + dbReader.GetDateTime(13).Day.ToString
            End If

            If dbReader.GetValue(14).ToString = "" Then attorneyTitle = "" Else attorneyTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then attorneySurname = "" Else attorneySurname = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then attorneyPostcode = "" Else attorneyPostcode = dbReader.GetValue(16).ToString


            sqlToSend = "insert into lpa_replacement_attorneys_detail (willClientSequence,attorneyType,attorneyNames,attorneyPhone,attorneyRelationshipClient1,attorneyRelationshipClient2,attorneyAddress,attorneyEmail,howToAct,whichClient,whichDocument,occupation,attorneyDOB, attorneyTitle,attorneySurname,attorneyPostcode) values (" + serverWillClientSequence + ",{{!" + attorneyType + "!}},{{!" + attorneyNames + "!}},{{!" + attorneyPhone + "!}},{{!" + attorneyRelationshipClient1 + "!}},{{!" + attorneyRelationshipClient2 + "!}},{{!" + attorneyAddress + "!}},{{!" + attorneyEmail + "!}},{{!" + howToAct + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + occupation + "!}},{{!" + attorneyDOB + "!}}, {{!" + attorneyTitle + "!}},{{!" + attorneySurname + "!}},{{!" + attorneyPostcode + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step27(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_attendance_notes where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading FAPT attendance notes"
            Me.Refresh()

            'clean the local data
            Dim notes As String
            Dim q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20 As String
            Dim q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick As String
            Dim whichClient, whichDocument As String


            If dbReader.GetValue(2).ToString = "" Then notes = "" Else notes = dbReader.GetValue(2).ToString
            If dbReader.GetValue(4).ToString = "" Then q1 = "" Else q1 = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then q2 = "" Else q2 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then q3 = "" Else q3 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then q4 = "" Else q4 = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then q5 = "" Else q5 = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then q6 = "" Else q6 = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then q7 = "" Else q7 = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then q8 = "" Else q8 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then q9 = "" Else q9 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then q10 = "" Else q10 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then q11 = "" Else q11 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then q12 = "" Else q12 = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then q13 = "" Else q13 = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then q14 = "" Else q14 = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then q15 = "" Else q15 = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then q16 = "" Else q16 = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then q17 = "" Else q17 = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then q18 = "" Else q18 = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then q19 = "" Else q19 = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then q20 = "" Else q20 = dbReader.GetValue(23).ToString

            If dbReader.GetValue(24).ToString = "" Then q1tick = "" Else q1tick = dbReader.GetValue(24).ToString
            If dbReader.GetValue(25).ToString = "" Then q2tick = "" Else q2tick = dbReader.GetValue(25).ToString
            If dbReader.GetValue(26).ToString = "" Then q3tick = "" Else q3tick = dbReader.GetValue(26).ToString
            If dbReader.GetValue(27).ToString = "" Then q4tick = "" Else q4tick = dbReader.GetValue(27).ToString
            If dbReader.GetValue(28).ToString = "" Then q5tick = "" Else q5tick = dbReader.GetValue(28).ToString
            If dbReader.GetValue(29).ToString = "" Then q6tick = "" Else q6tick = dbReader.GetValue(29).ToString
            If dbReader.GetValue(30).ToString = "" Then q7tick = "" Else q7tick = dbReader.GetValue(30).ToString
            If dbReader.GetValue(31).ToString = "" Then q8tick = "" Else q8tick = dbReader.GetValue(31).ToString
            If dbReader.GetValue(32).ToString = "" Then q9tick = "" Else q9tick = dbReader.GetValue(32).ToString
            If dbReader.GetValue(33).ToString = "" Then q10tick = "" Else q10tick = dbReader.GetValue(33).ToString
            If dbReader.GetValue(34).ToString = "" Then q11tick = "" Else q11tick = dbReader.GetValue(34).ToString
            If dbReader.GetValue(35).ToString = "" Then q12tick = "" Else q12tick = dbReader.GetValue(35).ToString
            If dbReader.GetValue(36).ToString = "" Then q13tick = "" Else q13tick = dbReader.GetValue(36).ToString
            If dbReader.GetValue(37).ToString = "" Then q14tick = "" Else q14tick = dbReader.GetValue(37).ToString
            If dbReader.GetValue(38).ToString = "" Then q15tick = "" Else q15tick = dbReader.GetValue(38).ToString
            If dbReader.GetValue(39).ToString = "" Then q16tick = "" Else q16tick = dbReader.GetValue(39).ToString
            If dbReader.GetValue(40).ToString = "" Then q17tick = "" Else q17tick = dbReader.GetValue(40).ToString
            If dbReader.GetValue(41).ToString = "" Then q18tick = "" Else q18tick = dbReader.GetValue(41).ToString
            If dbReader.GetValue(42).ToString = "" Then q19tick = "" Else q19tick = dbReader.GetValue(42).ToString
            If dbReader.GetValue(43).ToString = "" Then q20tick = "" Else q20tick = dbReader.GetValue(43).ToString

            If dbReader.GetValue(43).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(43).ToString
            If dbReader.GetValue(43).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(43).ToString

            Dim sqlToSend As String = "insert into fapt_attendance_notes (willClientSequence,notes, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20,q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick,whichClient,whichDocument ) values (" + serverWillClientSequence + ",{{!" + notes + "!}},{{!" + q1 + "!}},{{!" + q2 + "!}},{{!" + q3 + "!}},{{!" + q4 + "!}},{{!" + q5 + "!}},{{!" + q6 + "!}},{{!" + q7 + "!}},{{!" + q8 + "!}},{{!" + q9 + "!}},{{!" + q10 + "!}},{{!" + q11 + "!}},{{!" + q12 + "!}},{{!" + q13 + "!}},{{!" + q14 + "!}},{{!" + q15 + "!}},{{!" + q16 + "!}},{{!" + q17 + "!}},{{!" + q18 + "!}},{{!" + q19 + "!}},{{!" + q20 + "!}},{{!" + q1tick + "!}},{{!" + q2tick + "!}},{{!" + q3tick + "!}},{{!" + q4tick + "!}},{{!" + q5tick + "!}},{{!" + q6tick + "!}},{{!" + q7tick + "!}},{{!" + q8tick + "!}},{{!" + q9tick + "!}},{{!" + q10tick + "!}},{{!" + q11tick + "!}},{{!" + q12tick + "!}},{{!" + q13tick + "!}},{{!" + q14tick + "!}},{{!" + q15tick + "!}},{{!" + q16tick + "!}},{{!" + q17tick + "!}},{{!" + q18tick + "!}},{{!" + q19tick + "!}},{{!" + q20tick + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step28(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_beneficiaries where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "OK"
        Dim sqlToSend As String = ""

        Dim charityBackstop As String
        Dim charityDetails As String
        Dim whichClient As String
        Dim letterOfWishes As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "FAPT Beneficiaries"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then charityBackstop = "" Else charityBackstop = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then charityDetails = "" Else charityDetails = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then letterOfWishes = "" Else letterOfWishes = dbReader.GetValue(5).ToString

            sqlToSend = "insert into fapt_beneficiaries (willClientSequence,charityBackstop, charityDetails, whichClient,letterOfWishes) values  (" + serverWillClientSequence + ",{{!" + charityBackstop + "!}},{{!" + charityDetails + "!}},{{!" + whichClient + "!}},{{!" + letterOfWishes + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)

            Dim trustSequence As Integer
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-GetSequence-encrypted.php", "POST", sqlToSend)
            '   MsgBox(responseFromServer)
            trustSequence = Val(responseFromServer)

            step29(apikey, localWillClientSequence, serverWillClientSequence, trustSequence)

            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        '  If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
        'Return "OK"
        'Else
        'MsgBox(responseFromServer)
        Return ("OK")
        'End If

    End Function

    Function step29(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal trustSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_beneficiaries_detail where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading FAPT beneficiaries detail"
        Me.Refresh()

        'clean the local data
        ' Dim trustSequence As String
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim lapseOrIssue As String
        Dim percentage As String
        Dim atWhatAge As String
        Dim atWhatAgeIssue As String
        Dim primaryBeneficiaryFlag As String
        Dim relationshipClient1 As String
        Dim relationshipClient2 As String
        Dim beneficiaryDOB As String
        Dim whichClient As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            ' If dbReader.GetValue(2).ToString = "" Then trustSequence = "" Else trustSequence = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then percentage = "" Else percentage = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then atWhatAge = "" Else atWhatAge = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then primaryBeneficiaryFlag = "" Else primaryBeneficiaryFlag = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then relationshipClient1 = "" Else relationshipClient1 = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then relationshipClient2 = "" Else relationshipClient2 = dbReader.GetValue(11).ToString

            If dbReader.GetValue(12).ToString = "" Then
                beneficiaryDOB = "1900-01-01"
            Else
                beneficiaryDOB = dbReader.GetDateTime(12).Year.ToString + "-" + dbReader.GetDateTime(12).Month.ToString + "-" + dbReader.GetDateTime(12).Day.ToString
            End If

            If dbReader.GetValue(13).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(13).ToString

            sqlToSend = "insert into fapt_beneficiaries_detail (willClientSequence,trustSequence,beneficiaryType,beneficiaryName,lapseOrIssue,percentage,atWhatAge,atWhatAgeIssue,primaryBeneficiaryFlag,relationshipClient1,relationshipClient2,beneficiaryDOB,whichClient) values (" + serverWillClientSequence + ",{{!" + trustSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + lapseOrIssue + "!}},{{!" + percentage + "!}},{{!" + atWhatAge + "!}},{{!" + atWhatAgeIssue + "!}},{{!" + primaryBeneficiaryFlag + "!}},{{!" + relationshipClient1 + "!}},{{!" + relationshipClient2 + "!}},{{!" + beneficiaryDOB + "!}},{{!" + whichClient + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step30(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_monetary_gifts where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim localSequence As Int32
        Dim serverSequence As Int32
        Dim giftFrom As String
        Dim giftSharedFlag As String
        Dim typeOfGift As String
        Dim itemDetails As String
        Dim taxfree As String
        Dim soleJointGiftFlag As String
        Dim firstSecondDeathFlag As String
        Dim whichClient As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "FAPT Monetary Gifts"
            Me.Refresh()

            'clean the local data
            localSequence = dbReader.GetInt32(0)
            If dbReader.GetValue(2).ToString = "" Then giftFrom = "" Else giftFrom = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then giftSharedFlag = "" Else giftSharedFlag = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then typeOfGift = "" Else typeOfGift = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then itemDetails = "" Else itemDetails = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then taxfree = "" Else taxfree = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then soleJointGiftFlag = "" Else soleJointGiftFlag = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then firstSecondDeathFlag = "" Else firstSecondDeathFlag = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(9).ToString

            sqlToSend = "insert into fapt_monetary_gifts (willClientSequence,giftFrom, giftSharedFlag, typeOfGift,itemDetails,taxfree,soleJointGiftFlag,firstSecondDeathFlag,whichClient) values  (" + serverWillClientSequence + ",{{!" + giftFrom + "!}},{{!" + giftSharedFlag + "!}},{{!" + typeOfGift + "!}},{{!" + itemDetails + "!}},{{!" + taxfree + "!}},{{!" + soleJointGiftFlag + "!}},{{!" + firstSecondDeathFlag + "!}},{{!" + whichClient + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-GetSequence-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

            '***** the responseFromServer contains the sequence of the inserted row. Saves having to run another query on the server - clever, eh?
            serverSequence = Val(responseFromServer)

            'now, run a query against the detail data table but with the new and old sequence numbers
            step30a(apikey, localWillClientSequence, serverWillClientSequence, localSequence, serverSequence)

        End While
        dbReader.Close()
        dbConn.Close()

        If IsNumeric(responseFromServer) Or responseFromServer = "EMPTY" Then
            Return "OK"
        Else
            Return responseFromServer
        End If

    End Function

    Function step30a(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String, ByVal localGiftSequence As String, ByVal serverGiftSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_monetary_gifts_detail where willClientSequence=" + localWillClientSequence + " and giftSequence=" + localGiftSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "FAPT monetary gifts detail"
        Me.Refresh()

        'clean the local data
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim giftSharedType As String
        Dim giftSharedPercentage As String
        Dim charityName As String
        Dim charityRegNumber As String
        Dim atWhatAge As String
        Dim whichClient As String
        Dim responseFromServer As String = ""
        Dim sqlToSend As String

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then giftSharedType = "" Else giftSharedType = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then giftSharedPercentage = "" Else giftSharedPercentage = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then charityName = "" Else charityName = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then charityRegNumber = "" Else charityRegNumber = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then atWhatAge = "0" Else atWhatAge = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then whichClient = "0" Else whichClient = dbReader.GetValue(10).ToString

            sqlToSend = "insert into fapt_monetary_gifts_detail (willClientSequence,giftSequence, beneficiaryType,beneficiaryName,giftSharedType,giftSharedPercentage,charityName,charityRegNumber,atWhatAge,whichClient) values (" + serverWillClientSequence + ",{{!" + serverGiftSequence + "!}},{{!" + beneficiaryType + "!}},{{!" + beneficiaryName + "!}},{{!" + giftSharedType + "!}},{{!" + giftSharedPercentage + "!}},{{!" + charityName + "!}},{{!" + charityRegNumber + "!}},{{!" + atWhatAge + "!}},{{!" + whichClient + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox("step 11a - " + sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)
            'end

        End While

        dbReader.Close()
        dbConn.Close()
        Return (responseFromServer)


    End Function

    Function step31(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_trust_fund_cash where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim description As String
        Dim worth As String
        Dim whichClient As String
        Dim includeInTrust As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "FAPT Trust Fund Cash"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then description = "" Else description = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then worth = "" Else worth = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(4).ToString
            If dbReader.GetValue(7).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbReader.GetValue(7).ToString

            sqlToSend = "insert into fapt_trust_fund_cash (willClientSequence,description, worth, whichClient,includeInTrust) values  (" + serverWillClientSequence + ",{{!" + description + "!}},{{!" + worth + "!}},{{!" + whichClient + "!}},{{!" + includeInTrust + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step32(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_trust_fund_chattels where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim description As String
        Dim worth As String
        Dim whichClient As String
        Dim includeInTrust As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "FAPT Trust Fund Chattels"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then description = "" Else description = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then worth = "" Else worth = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(4).ToString
            If dbReader.GetValue(6).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbReader.GetValue(6).ToString

            sqlToSend = "insert into fapt_trust_fund_chattels (willClientSequence,description, worth, whichClient,includeInTrust) values  (" + serverWillClientSequence + ",{{!" + description + "!}},{{!" + worth + "!}},{{!" + whichClient + "!}},{{!" + includeInTrust + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step33(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_trust_fund_further where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim description As String
        Dim worth As String
        Dim whichClient As String
        Dim includeInTrust As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "FAPT Trust Fund Further Provision"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then description = "" Else description = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then worth = "" Else worth = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(4).ToString
            If dbReader.GetValue(6).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbReader.GetValue(6).ToString

            sqlToSend = "insert into fapt_trust_fund_further (willClientSequence,description, worth, whichClient,includeInTrust) values  (" + serverWillClientSequence + ",{{!" + description + "!}},{{!" + worth + "!}},{{!" + whichClient + "!}},{{!" + includeInTrust + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step34(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_trust_fund_property where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim address As String
        Dim housevalue As String
        Dim registeredFlag As String
        Dim owner As String
        Dim mortgageFlag As String
        Dim mortgageValue As String
        Dim leaseOrFreehold As String
        Dim whichClient As String
        Dim includeInTrust As String
        Dim assetCategory As String
        Dim description As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "FAPT Trust Fund Property"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then address = "" Else address = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then housevalue = "" Else housevalue = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then registeredFlag = "" Else registeredFlag = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then owner = "" Else owner = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then mortgageFlag = "" Else mortgageFlag = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then mortgageValue = "" Else mortgageValue = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then leaseOrFreehold = "" Else leaseOrFreehold = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then assetCategory = "" Else assetCategory = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then description = "" Else description = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then includeInTrust = "Y" Else includeInTrust = dbReader.GetValue(12).ToString

            sqlToSend = "insert into fapt_trust_fund_property (willClientSequence,address, housevalue, registeredFlag,owner,mortgageFlag,mortgageValue,leaseOrFreehold,whichClient,includeInTrust,assetCategory,description) values  (" + serverWillClientSequence + ",{{!" + address + "!}},{{!" + housevalue + "!}},{{!" + registeredFlag + "!}},{{!" + owner + "!}},{{!" + mortgageFlag + "!}},{{!" + mortgageValue + "!}},{{!" + leaseOrFreehold + "!}},{{!" + whichClient + "!}},{{!" + includeInTrust + "!}},{{!" + assetCategory + "!}},{{!" + description + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step35(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_trustees where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        Dim responseFromServer As String = "EMPTY"
        Dim sqlToSend As String = ""

        Dim attorneyType As String
        Dim whichClient As String
        Dim whichDocument As String

        While dbReader.Read

            'update audit log
            Me.auditlog.Text = "FAPT Trustees"
            Me.Refresh()

            'clean the local data
            If dbReader.GetValue(2).ToString = "" Then attorneyType = "" Else attorneyType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(4).ToString

            sqlToSend = "insert into fapt_trustees (willClientSequence,attorneyType, whichClient, whichDocument) values  (" + serverWillClientSequence + ",{{!" + attorneyType + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)

        End While
        dbReader.Close()
        dbConn.Close()

        If responseFromServer = "EMPTY" Or responseFromServer = "OK" Then
            Return "OK"
        Else
            MsgBox(responseFromServer)
            Return (responseFromServer)
        End If

    End Function

    Function step36(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_trustees_detail where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading FAPT trustee details"
        Me.Refresh()

        'clean the local data
        Dim attorneyType As String
        Dim attorneyNames As String
        Dim attorneyPhone As String
        Dim attorneyRelationshipClient1 As String
        Dim attorneyRelationshipClient2 As String
        Dim attorneyAddress As String
        Dim attorneyEmail As String
        Dim howToAct As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim occupation As String
        Dim attorneyPostcode As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then attorneyType = "" Else attorneyType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then attorneyNames = "" Else attorneyNames = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then attorneyPhone = "" Else attorneyPhone = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then attorneyRelationshipClient1 = "" Else attorneyRelationshipClient1 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then attorneyRelationshipClient2 = "" Else attorneyRelationshipClient2 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then attorneyAddress = "" Else attorneyAddress = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then attorneyEmail = "" Else attorneyEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then howToAct = "" Else howToAct = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then occupation = "" Else occupation = dbReader.GetValue(12).ToString
            If dbReader.GetValue(16).ToString = "" Then attorneyPostcode = "" Else attorneyPostcode = dbReader.GetValue(16).ToString

            sqlToSend = "insert into fapt_trustees_detail (willClientSequence,attorneyType,attorneyNames,attorneyPhone,attorneyRelationshipClient1,attorneyRelationshipClient2,attorneyAddress,attorneyEmail,howToAct,whichClient,whichDocument,occupation,attorneyPostcode) values (" + serverWillClientSequence + ",{{!" + attorneyType + "!}},{{!" + attorneyNames + "!}},{{!" + attorneyPhone + "!}},{{!" + attorneyRelationshipClient1 + "!}},{{!" + attorneyRelationshipClient2 + "!}},{{!" + attorneyAddress + "!}},{{!" + attorneyEmail + "!}},{{!" + howToAct + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}},{{!" + occupation + "!}},{{!" + attorneyPostcode + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step37(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_compliance where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading FAPT compliance notes"
            Me.Refresh()

            'clean the local data
            Dim notes As String
            Dim q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20 As String
            Dim q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick As String
            Dim whichClient, whichDocument As String


            If dbReader.GetValue(2).ToString = "" Then notes = "" Else notes = dbReader.GetValue(2).ToString
            If dbReader.GetValue(4).ToString = "" Then q1 = "" Else q1 = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then q2 = "" Else q2 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then q3 = "" Else q3 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then q4 = "" Else q4 = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then q5 = "" Else q5 = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then q6 = "" Else q6 = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then q7 = "" Else q7 = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then q8 = "" Else q8 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then q9 = "" Else q9 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then q10 = "" Else q10 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then q11 = "" Else q11 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then q12 = "" Else q12 = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then q13 = "" Else q13 = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then q14 = "" Else q14 = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then q15 = "" Else q15 = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then q16 = "" Else q16 = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then q17 = "" Else q17 = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then q18 = "" Else q18 = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then q19 = "" Else q19 = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then q20 = "" Else q20 = dbReader.GetValue(23).ToString

            If dbReader.GetValue(24).ToString = "" Then q1tick = "" Else q1tick = dbReader.GetValue(24).ToString
            If dbReader.GetValue(25).ToString = "" Then q2tick = "" Else q2tick = dbReader.GetValue(25).ToString
            If dbReader.GetValue(26).ToString = "" Then q3tick = "" Else q3tick = dbReader.GetValue(26).ToString
            If dbReader.GetValue(27).ToString = "" Then q4tick = "" Else q4tick = dbReader.GetValue(27).ToString
            If dbReader.GetValue(28).ToString = "" Then q5tick = "" Else q5tick = dbReader.GetValue(28).ToString
            If dbReader.GetValue(29).ToString = "" Then q6tick = "" Else q6tick = dbReader.GetValue(29).ToString
            If dbReader.GetValue(30).ToString = "" Then q7tick = "" Else q7tick = dbReader.GetValue(30).ToString
            If dbReader.GetValue(31).ToString = "" Then q8tick = "" Else q8tick = dbReader.GetValue(31).ToString
            If dbReader.GetValue(32).ToString = "" Then q9tick = "" Else q9tick = dbReader.GetValue(32).ToString
            If dbReader.GetValue(33).ToString = "" Then q10tick = "" Else q10tick = dbReader.GetValue(33).ToString
            If dbReader.GetValue(34).ToString = "" Then q11tick = "" Else q11tick = dbReader.GetValue(34).ToString
            If dbReader.GetValue(35).ToString = "" Then q12tick = "" Else q12tick = dbReader.GetValue(35).ToString
            If dbReader.GetValue(36).ToString = "" Then q13tick = "" Else q13tick = dbReader.GetValue(36).ToString
            If dbReader.GetValue(37).ToString = "" Then q14tick = "" Else q14tick = dbReader.GetValue(37).ToString
            If dbReader.GetValue(38).ToString = "" Then q15tick = "" Else q15tick = dbReader.GetValue(38).ToString
            If dbReader.GetValue(39).ToString = "" Then q16tick = "" Else q16tick = dbReader.GetValue(39).ToString
            If dbReader.GetValue(40).ToString = "" Then q17tick = "" Else q17tick = dbReader.GetValue(40).ToString
            If dbReader.GetValue(41).ToString = "" Then q18tick = "" Else q18tick = dbReader.GetValue(41).ToString
            If dbReader.GetValue(42).ToString = "" Then q19tick = "" Else q19tick = dbReader.GetValue(42).ToString
            If dbReader.GetValue(43).ToString = "" Then q20tick = "" Else q20tick = dbReader.GetValue(43).ToString

            If dbReader.GetValue(43).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(43).ToString
            If dbReader.GetValue(43).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(43).ToString


            Dim sqlToSend As String = "insert into fapt_compliance (willClientSequence,notes, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20,q1tick, q2tick, q3tick, q4tick, q5tick, q6tick, q7tick, q8tick, q9tick, q10tick, q11tick, q12tick, q13tick, q14tick, q15tick, q16tick, q17tick, q18tick, q19tick, q20tick,whichClient,whichDocument ) values (" + serverWillClientSequence + ",{{!" + notes + "!}},{{!" + q1 + "!}},{{!" + q2 + "!}},{{!" + q3 + "!}},{{!" + q4 + "!}},{{!" + q5 + "!}},{{!" + q6 + "!}},{{!" + q7 + "!}},{{!" + q8 + "!}},{{!" + q9 + "!}},{{!" + q10 + "!}},{{!" + q11 + "!}},{{!" + q12 + "!}},{{!" + q13 + "!}},{{!" + q14 + "!}},{{!" + q15 + "!}},{{!" + q16 + "!}},{{!" + q17 + "!}},{{!" + q18 + "!}},{{!" + q19 + "!}},{{!" + q20 + "!}},{{!" + q1tick + "!}},{{!" + q2tick + "!}},{{!" + q3tick + "!}},{{!" + q4tick + "!}},{{!" + q5tick + "!}},{{!" + q6tick + "!}},{{!" + q7tick + "!}},{{!" + q8tick + "!}},{{!" + q9tick + "!}},{{!" + q10tick + "!}},{{!" + q11tick + "!}},{{!" + q12tick + "!}},{{!" + q13tick + "!}},{{!" + q14tick + "!}},{{!" + q15tick + "!}},{{!" + q16tick + "!}},{{!" + q17tick + "!}},{{!" + q18tick + "!}},{{!" + q19tick + "!}},{{!" + q20tick + "!}},{{!" + whichClient + "!}},{{!" + whichDocument + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default

    End Function

    Function step38(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)
        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from fapt_purpose where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader
        If dbReader.HasRows Then
            dbReader.Read()

            'update audit log
            Me.auditlog.Text = "Uploading FAPT purpose options"
            Me.Refresh()

            'clean the local data
            Dim notes As String
            Dim q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20 As String


            If dbReader.GetValue(2).ToString = "" Then q1 = "" Else q1 = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then q2 = "" Else q2 = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then q3 = "" Else q3 = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then q4 = "" Else q4 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then q5 = "" Else q5 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then q6 = "" Else q6 = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then q7 = "" Else q7 = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then q8 = "" Else q8 = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then q9 = "" Else q9 = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then q10 = "" Else q10 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then q11 = "" Else q11 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then q12 = "" Else q12 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then q13 = "" Else q13 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then q14 = "" Else q14 = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then q15 = "" Else q15 = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then q16 = "" Else q16 = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then q17 = "" Else q17 = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then q18 = "" Else q18 = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then q19 = "" Else q19 = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then q20 = "" Else q20 = dbReader.GetValue(21).ToString


            Dim sqlToSend As String = "insert into fapt_purpose (willClientSequence, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20 ) values (" + serverWillClientSequence + ",{{!" + q1 + "!}},{{!" + q2 + "!}},{{!" + q3 + "!}},{{!" + q4 + "!}},{{!" + q5 + "!}},{{!" + q6 + "!}},{{!" + q7 + "!}},{{!" + q8 + "!}},{{!" + q9 + "!}},{{!" + q10 + "!}},{{!" + q11 + "!}},{{!" + q12 + "!}},{{!" + q13 + "!}},{{!" + q14 + "!}},{{!" + q15 + "!}},{{!" + q16 + "!}},{{!" + q17 + "!}},{{!" + q18 + "!}},{{!" + q19 + "!}},{{!" + q20 + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            'MsgBox(sqlToSend)
            Dim responseFromServer As String = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


            dbReader.Close()
            dbConn.Close()

            Return (responseFromServer)

        End If

        dbReader.Close()
        dbConn.Close()
        Return "OK" ' default
    End Function

    Function step39(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        'not required in this installation
        Return "OK"

    End Function

    Function step40(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        'not required in this installation
        Return "OK"

    End Function

    Function step41(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        'not required in this installation
        Return "OK"

    End Function

    Function step42(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from name_list where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading autofill name details"
        Me.Refresh()

        'clean the local data
        Dim forenames As String
        Dim surname As String
        Dim fullname As String
        Dim address As String
        Dim phone As String
        Dim email As String
        Dim occupation As String
        Dim postcode As String
        Dim dob As String
        Dim title As String
        Dim relationshipClient1 As String
        Dim relationshipClient2 As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then forenames = "" Else forenames = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then surname = "" Else surname = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then fullname = "" Else fullname = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then address = "" Else address = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then phone = "" Else phone = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then email = "" Else email = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then occupation = "" Else occupation = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then postcode = "" Else postcode = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then dob = "" Else dob = dbReader.GetValue(10).ToString

            If dbReader.GetValue(10).ToString = "" Then
                dob = "1900-01-01"
            Else
                dob = dbReader.GetDateTime(10).Year.ToString + "-" + dbReader.GetDateTime(10).Month.ToString + "-" + dbReader.GetDateTime(10).Day.ToString
            End If


            If dbReader.GetValue(11).ToString = "" Then title = "" Else title = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then relationshipClient1 = "" Else relationshipClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationshipClient2 = "" Else relationshipClient2 = dbReader.GetValue(13).ToString

            sqlToSend = "insert into name_list (willClientSequence,forenames,surname,fullname,address,phone,email,occupation,postcode,dob,title,relationshipClient1,relationshipClient2) values (" + serverWillClientSequence + ",{{!" + SqlSafe(forenames) + "!}},{{!" + SqlSafe(surname) + "!}},{{!" + SqlSafe(fullname) + "!}},{{!" + SqlSafe(address) + "!}},{{!" + SqlSafe(phone) + "!}},{{!" + SqlSafe(email) + "!}},{{!" + SqlSafe(occupation) + "!}},{{!" + SqlSafe(postcode) + "!}},{{!" + SqlSafe(dob) + "!}},{{!" + SqlSafe(title) + "!}},{{!" + SqlSafe(relationshipClient1) + "!}},{{!" + SqlSafe(relationshipClient2) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step43(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from will_instruction_stage9_data_factfind_3rd_level where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading 3rd level beneficiaries"
        Me.Refresh()

        'clean the local data
        Dim residuaryType As String
        Dim beneficiaryType As String
        Dim beneficiaryName As String
        Dim giftSharedType As String
        Dim giftSharedPercentage As String
        Dim charityName As String
        Dim charityRegNumber As String
        Dim atWhatAge As String
        Dim whichClient As String
        Dim whichDocument As String
        Dim relationToClient1 As String
        Dim relationToClient2 As String

        Dim beneficiaryTitle As String
        Dim beneficiaryForenames As String
        Dim beneficiarySurname As String
        Dim atWhatAgeIssue As String
        Dim lapseOrIssue As String
        Dim beneficiaryAddress As String
        Dim beneficiaryPostcode As String
        Dim beneficiaryEmail As String
        Dim beneficiaryPhone As String
        Dim giftoverName As String



        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then residuaryType = "" Else residuaryType = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then beneficiaryType = "" Else beneficiaryType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then beneficiaryName = "" Else beneficiaryName = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then giftSharedType = "" Else giftSharedType = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then giftSharedPercentage = "" Else giftSharedPercentage = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then charityName = "" Else charityName = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then charityRegNumber = "" Else charityRegNumber = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then atWhatAge = "" Else atWhatAge = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then whichDocument = "" Else whichDocument = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then relationToClient1 = "" Else relationToClient1 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then relationToClient2 = "" Else relationToClient2 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then beneficiaryTitle = "" Else beneficiaryTitle = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then beneficiaryForenames = "" Else beneficiaryForenames = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then beneficiarySurname = "" Else beneficiarySurname = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then atWhatAgeIssue = "" Else atWhatAgeIssue = dbReader.GetValue(17).ToString
            If dbReader.GetValue(18).ToString = "" Then lapseOrIssue = "" Else lapseOrIssue = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then beneficiaryAddress = "" Else beneficiaryAddress = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then beneficiaryPostcode = "" Else beneficiaryPostcode = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then beneficiaryEmail = "" Else beneficiaryEmail = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then beneficiaryPhone = "" Else beneficiaryPhone = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then giftoverName = "" Else giftoverName = dbReader.GetValue(23).ToString


            sqlToSend = "insert into will_instruction_stage9_data_factfind_3rd_level (willClientSequence,residuaryType, beneficiaryType, beneficiaryName, giftSharedType, giftSharedPercentage, charityName, charityRegNumber, atWhatAge, whichClient, whichDocument, relationToClient1, relationToClient2, beneficiaryTitle, beneficiaryForenames, beneficiarySurname, atWhatAgeIssue, lapseOrIssue, beneficiaryAddress, beneficiaryPostcode, beneficiaryEmail, beneficiaryPhone,giftoverName) values (" + serverWillClientSequence + ",{{!" + SqlSafe(residuaryType) + "!}},{{!" + SqlSafe(beneficiaryType) + "!}},{{!" + SqlSafe(beneficiaryName) + "!}},{{!" + SqlSafe(giftSharedType) + "!}},{{!" + SqlSafe(giftSharedPercentage) + "!}},{{!" + SqlSafe(charityName) + "!}},{{!" + SqlSafe(charityRegNumber) + "!}},{{!" + SqlSafe(atWhatAge) + "!}},{{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(whichDocument) + "!}},{{!" + SqlSafe(relationToClient1) + "!}},{{!" + SqlSafe(relationToClient2) + "!}},{{!" + SqlSafe(beneficiaryTitle) + "!}},{{!" + SqlSafe(beneficiaryForenames) + "!}},{{!" + SqlSafe(beneficiarySurname) + "!}},{{!" + SqlSafe(atWhatAgeIssue) + "!}},{{!" + SqlSafe(lapseOrIssue) + "!}},{{!" + SqlSafe(beneficiaryAddress) + "!}},{{!" + SqlSafe(beneficiaryPostcode) + "!}},{{!" + SqlSafe(beneficiaryEmail) + "!}},{{!" + SqlSafe(beneficiaryPhone) + "!}},{{!" + SqlSafe(giftoverName) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step44(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_wills_lookup where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading will lookup data"
        Me.Refresh()

        'clean the local data
        Dim fieldname As String
        Dim fieldvalue As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then fieldname = "" Else fieldname = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then fieldvalue = "" Else fieldvalue = dbReader.GetValue(3).ToString

            sqlToSend = "insert into client_wills_lookup (willClientSequence,fieldname,fieldvalue) values (" + serverWillClientSequence + ",{{!" + SqlSafe(fieldname) + "!}},{{!" + SqlSafe(fieldvalue) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step45(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_lpa_lookup where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading lpa lookup data"
        Me.Refresh()

        'clean the local data
        Dim fieldname As String
        Dim fieldvalue As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then fieldname = "" Else fieldname = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then fieldvalue = "" Else fieldvalue = dbReader.GetValue(3).ToString

            sqlToSend = "insert into client_lpa_lookup (willClientSequence,fieldname,fieldvalue) values (" + serverWillClientSequence + ",{{!" + SqlSafe(fieldname) + "!}},{{!" + SqlSafe(fieldvalue) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step46(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_assets where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading asset data"
        Me.Refresh()

        'clean the local data
        Dim whichClient As String
        Dim grossIncome As String
        Dim emergencyAmount As String
        Dim feelings As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then grossIncome = "" Else grossIncome = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then emergencyAmount = "" Else emergencyAmount = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then feelings = "" Else feelings = dbReader.GetValue(5).ToString


            sqlToSend = "insert into client_assets (willClientSequence,whichClient,grossIncome,emergencyAmount,feelings) values (" + serverWillClientSequence + ",{{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(grossIncome) + "!}},{{!" + SqlSafe(emergencyAmount) + "!}},{{!" + SqlSafe(feelings) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step47(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_funeral where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading funeral data"
        Me.Refresh()

        'clean the local data
        Dim whichClient As String
        Dim funeralAllowance As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then funeralAllowance = "" Else funeralAllowance = dbReader.GetValue(3).ToString

            sqlToSend = "insert into client_funeral (willClientSequence,whichClient,funeralAllowance) values (" + serverWillClientSequence + ",{{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(funeralAllowance) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step48(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_offer where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading offer data"
        Me.Refresh()

        'clean the local data
        Dim whichClient As String
        Dim willsPrice As String
        Dim willsFlag As String
        Dim HWLPAPrice As String
        Dim HWLPAFlag As String
        Dim PFLPAPrice As String
        Dim PFLPAFlag As String
        Dim TrustsPrice As String
        Dim TrustsFlag As String
        Dim FuneralPlanPrice As String
        Dim FuneralPlanFlag As String
        Dim ProbatePrice As String
        Dim ProbateFlag As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then willsPrice = "" Else willsPrice = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then willsFlag = "" Else willsFlag = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then HWLPAPrice = "" Else HWLPAPrice = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then HWLPAFlag = "" Else HWLPAFlag = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then PFLPAPrice = "" Else PFLPAPrice = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then PFLPAFlag = "" Else PFLPAFlag = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then TrustsPrice = "" Else TrustsPrice = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then TrustsFlag = "" Else TrustsFlag = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then FuneralPlanPrice = "" Else FuneralPlanPrice = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then FuneralPlanFlag = "" Else FuneralPlanFlag = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then ProbatePrice = "" Else ProbatePrice = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then ProbateFlag = "" Else ProbateFlag = dbReader.GetValue(14).ToString


            sqlToSend = "insert into client_offer (willClientSequence,whichClient ,willsPrice ,willsFlag ,HWLPAPrice ,HWLPAFlag ,PFLPAPrice ,PFLPAFlag ,TrustsPrice ,TrustsFlag ,FuneralPlanPrice ,FuneralPlanFlag ,ProbatePrice ,ProbateFlag) values (" + serverWillClientSequence + ",{{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(willsPrice) + "!}},{{!" + SqlSafe(willsFlag) + "!}},{{!" + SqlSafe(HWLPAPrice) + "!}},{{!" + SqlSafe(HWLPAFlag) + "!}},{{!" + SqlSafe(PFLPAPrice) + "!}},{{!" + SqlSafe(PFLPAFlag) + "!}},{{!" + SqlSafe(TrustsPrice) + "!}},{{!" + SqlSafe(TrustsFlag) + "!}},{{!" + SqlSafe(FuneralPlanPrice) + "!}},{{!" + SqlSafe(FuneralPlanFlag) + "!}},{{!" + SqlSafe(ProbatePrice) + "!}},{{!" + SqlSafe(ProbateFlag) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step49(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_products where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading product data"
        Me.Refresh()

        'clean the local data
        Dim whichClient As String
        Dim q1 As String
        Dim q2 As String
        Dim q3 As String
        Dim q4 As String
        Dim q5 As String
        Dim q6 As String
        Dim q7 As String
        Dim q8 As String
        Dim q9 As String
        Dim q10 As String
        Dim q11 As String
        Dim q12 As String
        Dim q13 As String
        Dim q14 As String
        Dim q15 As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then q1 = "" Else q1 = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then q2 = "" Else q2 = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then q3 = "" Else q3 = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then q4 = "" Else q4 = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then q5 = "" Else q5 = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then q6 = "" Else q6 = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then q7 = "" Else q7 = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then q8 = "" Else q8 = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then q9 = "" Else q9 = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then q10 = "" Else q10 = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then q11 = "" Else q11 = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then q12 = "" Else q12 = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then q13 = "" Else q13 = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then q14 = "" Else q14 = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then q15 = "" Else q15 = dbReader.GetValue(17).ToString


            sqlToSend = "insert into client_products (willClientSequence,whichClient ,q1 ,q2 ,q3 ,q4 ,q5 ,q6 ,q7 ,q8 ,q9 ,q10 ,q11 ,q12 ,q13 ,q14 ,q15) values (" + serverWillClientSequence + ",{{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(q1) + "!}},{{!" + SqlSafe(q2) + "!}},{{!" + SqlSafe(q3) + "!}},{{!" + SqlSafe(q4) + "!}},{{!" + SqlSafe(q5) + "!}},{{!" + SqlSafe(q6) + "!}},{{!" + SqlSafe(q7) + "!}},{{!" + SqlSafe(q8) + "!}},{{!" + SqlSafe(q9) + "!}},{{!" + SqlSafe(q10) + "!}},{{!" + SqlSafe(q11) + "!}},{{!" + SqlSafe(q12) + "!}},{{!" + SqlSafe(q13) + "!}},{{!" + SqlSafe(q14) + "!}},{{!" + SqlSafe(q15) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step50(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_returns where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading returns data"
        Me.Refresh()

        'clean the local data
        Dim investment As String
        Dim deposit As String
        Dim months As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then investment = "" Else investment = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then deposit = "" Else deposit = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then months = "" Else months = dbReader.GetValue(4).ToString

            sqlToSend = "insert into client_returns (willClientSequence,investment ,deposit ,months) values (" + serverWillClientSequence + ",{{!" + SqlSafe(investment) + "!}},{{!" + SqlSafe(deposit) + "!}},{{!" + SqlSafe(months) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step51(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_wizard where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading wizard data"
        Me.Refresh()

        'clean the local data
        Dim rec_willReviewFlag As String
        Dim rec_willPPTFlag As String
        Dim rec_willBasicFlag As String
        Dim rec_lpaHWFlag As String
        Dim rec_lpaPFAFlag As String
        Dim rec_faptFlag As String
        Dim rec_probateFlag As String
        Dim rec_storageFlag As String

        Dim manualOverride As String
        Dim willReviewFlag As String
        Dim willPPTFlag As String
        Dim willBasicFlag As String
        Dim lpaHWFlag As String
        Dim lpaPFAFlag As String
        Dim faptFlag As String
        Dim probateFlag As String
        Dim otherFlag As String
        Dim otherPrice As String
        Dim otherDescription As String
        Dim manPFAOPG As String
        Dim manHWOPG As String
        Dim storageFlag As String

        Dim followUpAppointmentFlag As String
        Dim followUpAppointmentDate As String
        Dim followUpNotes As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then manualOverride = "" Else manualOverride = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then willReviewFlag = "" Else willReviewFlag = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then willPPTFlag = "" Else willPPTFlag = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then willBasicFlag = "" Else willBasicFlag = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then lpaHWFlag = "" Else lpaHWFlag = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then lpaPFAFlag = "" Else lpaPFAFlag = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then faptFlag = "" Else faptFlag = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then probateFlag = "" Else probateFlag = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then otherFlag = "" Else otherFlag = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then otherPrice = "" Else otherPrice = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then otherDescription = "" Else otherDescription = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then manPFAOPG = "" Else manPFAOPG = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then manHWOPG = "" Else manHWOPG = dbReader.GetValue(14).ToString

            If dbReader.GetValue(15).ToString = "" Then followUpAppointmentFlag = "" Else followUpAppointmentFlag = dbReader.GetValue(15).ToString
            If dbReader.GetValue(16).ToString = "" Then followUpAppointmentDate = "" Else followUpAppointmentDate = dbReader.GetValue(16).ToString
            If dbReader.GetValue(17).ToString = "" Then followUpNotes = "" Else followUpNotes = dbReader.GetValue(17).ToString

            If dbReader.GetValue(18).ToString = "" Then rec_willReviewFlag = "" Else rec_willReviewFlag = dbReader.GetValue(18).ToString
            If dbReader.GetValue(19).ToString = "" Then rec_willPPTFlag = "" Else rec_willPPTFlag = dbReader.GetValue(19).ToString
            If dbReader.GetValue(20).ToString = "" Then rec_willBasicFlag = "" Else rec_willBasicFlag = dbReader.GetValue(20).ToString
            If dbReader.GetValue(21).ToString = "" Then rec_lpaHWFlag = "" Else rec_lpaHWFlag = dbReader.GetValue(21).ToString
            If dbReader.GetValue(22).ToString = "" Then rec_lpaPFAFlag = "" Else rec_lpaPFAFlag = dbReader.GetValue(22).ToString
            If dbReader.GetValue(23).ToString = "" Then rec_faptFlag = "" Else rec_faptFlag = dbReader.GetValue(23).ToString
            If dbReader.GetValue(24).ToString = "" Then rec_probateFlag = "" Else rec_probateFlag = dbReader.GetValue(24).ToString

            If dbReader.GetValue(25).ToString = "" Then rec_storageFlag = "" Else rec_storageFlag = dbReader.GetValue(25).ToString
            If dbReader.GetValue(26).ToString = "" Then storageFlag = "" Else storageFlag = dbReader.GetValue(26).ToString


            sqlToSend = "insert into client_wizard (willClientSequence, rec_willReviewFlag , rec_willPPTFlag , rec_willBasicFlag , rec_lpaHWFlag , rec_lpaPFAFlag , rec_faptFlag , rec_probateFlag , manualOverride , willReviewFlag , willPPTFlag , willBasicFlag , lpaHWFlag , lpaPFAFlag , faptFlag , probateFlag , otherFlag , otherPrice , otherDescription ,manPFAOPG,manHWOPG,rec_storageFlag,storageFlag,followUpAppointmentFlag,followUpAppointmentDate, followUpNotes) values (" + serverWillClientSequence + ",{{!" + SqlSafe(rec_willReviewFlag) + "!}},{{!" + SqlSafe(rec_willPPTFlag) + "!}},{{!" + SqlSafe(rec_willBasicFlag) + "!}},{{!" + SqlSafe(rec_lpaHWFlag) + "!}},{{!" + SqlSafe(rec_lpaPFAFlag) + "!}},{{!" + SqlSafe(rec_faptFlag) + "!}},{{!" + SqlSafe(rec_probateFlag) + "!}}, {{!" + SqlSafe(manualOverride) + "!}},{{!" + SqlSafe(willReviewFlag) + "!}},{{!" + SqlSafe(willPPTFlag) + "!}},{{!" + SqlSafe(willBasicFlag) + "!}},{{!" + SqlSafe(lpaHWFlag) + "!}},{{!" + SqlSafe(lpaPFAFlag) + "!}},{{!" + SqlSafe(faptFlag) + "!}},{{!" + SqlSafe(probateFlag) + "!}},{{!" + SqlSafe(otherFlag) + "!}},{{!" + SqlSafe(otherPrice) + "!}},{{!" + SqlSafe(otherDescription) + "!}},{{!" + SqlSafe(manPFAOPG) + "!}},{{!" + SqlSafe(manHWOPG) + "!}},{{!" + SqlSafe(rec_storageFlag) + "!}},{{!" + SqlSafe(storageFlag) + "!}},{{!" + SqlSafe(followUpAppointmentFlag) + "!}},{{!" + SqlSafe(followUpAppointmentDate) + "!}},{{!" + SqlSafe(followUpNotes) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox("sending this - " + sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            ' MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step51a(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_appointment where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading appointment data"
        Me.Refresh()

        'clean the local data
        Dim sourceClientID As String
        Dim appointmentID As String
        Dim appointmentStart As String
        Dim appointmentEnd As String
        Dim appointmentTitle As String
        Dim campaignID As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then sourceClientID = "" Else sourceClientID = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then appointmentID = "" Else appointmentID = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then appointmentStart = "" Else appointmentStart = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then appointmentEnd = "" Else appointmentEnd = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then appointmentTitle = "" Else appointmentTitle = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then campaignID = "" Else campaignID = dbReader.GetValue(7).ToString

            sqlToSend = "insert into client_appointment (willClientSequence,sourceClientID,appointmentID,appointmentStart,appointmentEnd,appointmentTitle,campaignID) values (" + serverWillClientSequence + ",{{!" + SqlSafe(sourceClientID) + "!}},{{!" + SqlSafe(appointmentID) + "!}},{{!" + SqlSafe(appointmentStart) + "!}},{{!" + SqlSafe(appointmentEnd) + "!}},{{!" + SqlSafe(appointmentTitle) + "!}},{{!" + SqlSafe(campaignID) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step51b(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from client_storage where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading storage data"
        Me.Refresh()

        'clean the local data
        Dim sourceClientID As String
        Dim storageFlag As String
        Dim storageType As String
        Dim storageAmount As String
        Dim storageSortCode As String
        Dim storageAccountNumber As String
        Dim storagePaymentMethod As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then sourceClientID = "" Else sourceClientID = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then storageFlag = "" Else storageFlag = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then storageType = "" Else storageType = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then storageAmount = "" Else storageAmount = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then storageSortCode = "" Else storageSortCode = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then storageAccountNumber = "" Else storageAccountNumber = dbReader.GetValue(7).ToString
            If dbReader.GetValue(7).ToString = "" Then storagePaymentMethod = "" Else storagePaymentMethod = dbReader.GetValue(8).ToString

            sqlToSend = "insert into client_appointment (willClientSequence,sourceClientID,storageFlag,storageType,storageAmount,storageSortCode,storageAccountNumber,storagePaymentMethod) values (" + serverWillClientSequence + ",{{!" + SqlSafe(sourceClientID) + "!}},{{!" + SqlSafe(storageFlag) + "!}},{{!" + SqlSafe(storageType) + "!}},{{!" + SqlSafe(storageAmount) + "!}},{{!" + SqlSafe(storageSortCode) + "!}},{{!" + SqlSafe(storageAccountNumber) + "!}},{{!" + SqlSafe(storagePaymentMethod) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step52(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_advisers where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate Advisers data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim title As String
        Dim forenames As String
        Dim surname As String
        Dim address As String
        Dim postcode As String
        Dim phone As String
        Dim email As String
        Dim relationship As String
        Dim adviserType As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then title = "" Else title = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then forenames = "" Else forenames = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then surname = "" Else surname = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then address = "" Else address = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then postcode = "" Else postcode = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then phone = "" Else phone = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then email = "" Else email = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then relationship = "" Else relationship = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then adviserType = "" Else adviserType = dbReader.GetValue(11).ToString


            sqlToSend = "insert into probate_advisers (willClientSequence, whichClient , title , forenames , surname , address , postcode , phone , email , relationship , adviserType ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(title) + "!}},{{!" + SqlSafe(forenames) + "!}},{{!" + SqlSafe(surname) + "!}},{{!" + SqlSafe(address) + "!}},{{!" + SqlSafe(postcode) + "!}},{{!" + SqlSafe(phone) + "!}},{{!" + SqlSafe(email) + "!}},{{!" + SqlSafe(relationship) + "!}},{{!" + SqlSafe(adviserType) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step53(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_assets where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate asset data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim details As String
        Dim worth As String
        Dim assetType As String



        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then details = "" Else details = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then worth = "" Else worth = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then assetType = "" Else assetType = dbReader.GetValue(5).ToString
            sqlToSend = "insert into probate_assets (willClientSequence, whichClient , details , worth , assetType ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(details) + "!}},{{!" + SqlSafe(worth) + "!}},{{!" + SqlSafe(assetType) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step54(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_beneficiaries where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate beneficiary data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim title As String
        Dim forenames As String
        Dim surname As String
        Dim address As String
        Dim postcode As String
        Dim phone As String
        Dim email As String
        Dim relationship As String
        Dim item As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then title = "" Else title = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then forenames = "" Else forenames = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then surname = "" Else surname = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then address = "" Else address = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then postcode = "" Else postcode = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then phone = "" Else phone = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then email = "" Else email = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then relationship = "" Else relationship = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then item = "" Else item = dbReader.GetValue(11).ToString


            sqlToSend = "insert into probate_beneficiaries (willClientSequence, whichClient , title , forenames , surname , address , postcode , phone , email , relationship , item ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(title) + "!}},{{!" + SqlSafe(forenames) + "!}},{{!" + SqlSafe(surname) + "!}},{{!" + SqlSafe(address) + "!}},{{!" + SqlSafe(postcode) + "!}},{{!" + SqlSafe(phone) + "!}},{{!" + SqlSafe(email) + "!}},{{!" + SqlSafe(relationship) + "!}},{{!" + SqlSafe(item) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step55(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_control where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate control data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim deathAddress As String
        Dim deathPostcode As String
        Dim nursingHomeFlag As String
        Dim deathDate As String
        Dim willFlag As String
        Dim willDate As String
        Dim attendanceNotes As String
        Dim assetValue As String
        Dim liabilitiesValue As String
        Dim netAssetValue As String
        Dim probateFee As String
        Dim probateCost As String
        Dim giftsGivenFlag As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then deathAddress = "" Else deathAddress = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then deathPostcode = "" Else deathPostcode = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then nursingHomeFlag = "" Else nursingHomeFlag = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then deathDate = "" Else deathDate = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then willFlag = "" Else willFlag = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then willDate = "" Else willDate = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then attendanceNotes = "" Else attendanceNotes = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then assetValue = "" Else assetValue = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then liabilitiesValue = "" Else liabilitiesValue = dbReader.GetValue(11).ToString
            If dbReader.GetValue(12).ToString = "" Then netAssetValue = "" Else netAssetValue = dbReader.GetValue(12).ToString
            If dbReader.GetValue(13).ToString = "" Then probateFee = "" Else probateFee = dbReader.GetValue(13).ToString
            If dbReader.GetValue(14).ToString = "" Then probateCost = "" Else probateCost = dbReader.GetValue(14).ToString
            If dbReader.GetValue(15).ToString = "" Then giftsGivenFlag = "" Else giftsGivenFlag = dbReader.GetValue(15).ToString



            sqlToSend = "insert into probate_control (willClientSequence,whichClient ,deathAddress ,deathPostcode ,nursingHomeFlag ,deathDate ,willFlag ,willDate ,attendanceNotes ,assetValue ,liabilitiesValue ,netAssetValue ,probateFee ,probateCost ,giftsGivenFlag ) values (" + serverWillClientSequence + ",{{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(deathAddress) + "!}},{{!" + SqlSafe(deathPostcode) + "!}},{{!" + SqlSafe(nursingHomeFlag) + "!}},{{!" + SqlSafe(deathDate) + "!}},{{!" + SqlSafe(willFlag) + "!}},{{!" + SqlSafe(willDate) + "!}},{{!" + SqlSafe(attendanceNotes) + "!}},{{!" + SqlSafe(assetValue) + "!}},{{!" + SqlSafe(liabilitiesValue) + "!}},{{!" + SqlSafe(netAssetValue) + "!}},{{!" + SqlSafe(probateFee) + "!}},{{!" + SqlSafe(probateCost) + "!}},{{!" + SqlSafe(giftsGivenFlag) + "!}} )"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            '  MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step56(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_executors where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate executor data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim title As String
        Dim forenames As String
        Dim surname As String
        Dim address As String
        Dim postcode As String
        Dim phone As String
        Dim email As String
        Dim relationship As String
        Dim executorType As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then title = "" Else title = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then forenames = "" Else forenames = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then surname = "" Else surname = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then address = "" Else address = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then postcode = "" Else postcode = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then phone = "" Else phone = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then email = "" Else email = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then relationship = "" Else relationship = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then executorType = "" Else executorType = dbReader.GetValue(11).ToString


            sqlToSend = "insert into probate_executors (willClientSequence, whichClient , title , forenames , surname , address , postcode , phone , email , relationship , executorType ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(title) + "!}},{{!" + SqlSafe(forenames) + "!}},{{!" + SqlSafe(surname) + "!}},{{!" + SqlSafe(address) + "!}},{{!" + SqlSafe(postcode) + "!}},{{!" + SqlSafe(phone) + "!}},{{!" + SqlSafe(email) + "!}},{{!" + SqlSafe(relationship) + "!}},{{!" + SqlSafe(executorType) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step57(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_gifts where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate gift data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim title As String
        Dim forenames As String
        Dim surname As String
        Dim address As String
        Dim postcode As String
        Dim phone As String
        Dim email As String
        Dim relationship As String
        Dim item As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then title = "" Else title = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then forenames = "" Else forenames = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then surname = "" Else surname = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then address = "" Else address = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then postcode = "" Else postcode = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then phone = "" Else phone = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then email = "" Else email = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then relationship = "" Else relationship = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then item = "" Else item = dbReader.GetValue(11).ToString


            sqlToSend = "insert into probate_gifts (willClientSequence, whichClient , title , forenames , surname , address , postcode , phone , email , relationship , item ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(title) + "!}},{{!" + SqlSafe(forenames) + "!}},{{!" + SqlSafe(surname) + "!}},{{!" + SqlSafe(address) + "!}},{{!" + SqlSafe(postcode) + "!}},{{!" + SqlSafe(phone) + "!}},{{!" + SqlSafe(email) + "!}},{{!" + SqlSafe(relationship) + "!}},{{!" + SqlSafe(item) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step58(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_liabilities where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate liability data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim details As String
        Dim worth As String
        Dim liabilityType As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then details = "" Else details = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then worth = "" Else worth = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then liabilityType = "" Else liabilityType = dbReader.GetValue(5).ToString

            sqlToSend = "insert into probate_liabilities (willClientSequence, whichClient , details , worth , liabilityType  ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(details) + "!}},{{!" + SqlSafe(worth) + "!}},{{!" + SqlSafe(liabilityType) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step59(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_plan where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate plan data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim probatePlanFlag As String
        Dim attendanceNotes As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            ' If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(2).ToString = "" Then probatePlanFlag = "" Else probatePlanFlag = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then attendanceNotes = "" Else attendanceNotes = dbReader.GetValue(3).ToString

            sqlToSend = "insert into probate_plan (willClientSequence,  probatePlanFlag , attendanceNotes) values (" + serverWillClientSequence + ",{{!" + SqlSafe(probatePlanFlag) + "!}},{{!" + SqlSafe(attendanceNotes) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step60(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_relative where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate relative data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim title As String
        Dim forenames As String
        Dim surname As String
        Dim address As String
        Dim postcode As String
        Dim phone As String
        Dim email As String
        Dim relationship As String
        Dim dependant As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then title = "" Else title = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then forenames = "" Else forenames = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then surname = "" Else surname = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then address = "" Else address = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then postcode = "" Else postcode = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then phone = "" Else phone = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then email = "" Else email = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then relationship = "" Else relationship = dbReader.GetValue(10).ToString
            If dbReader.GetValue(11).ToString = "" Then dependant = "" Else dependant = dbReader.GetValue(11).ToString


            sqlToSend = "insert into probate_relative (willClientSequence, whichClient , title , forenames , surname , address , postcode , phone , email , relationship , dependant ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(title) + "!}},{{!" + SqlSafe(forenames) + "!}},{{!" + SqlSafe(surname) + "!}},{{!" + SqlSafe(address) + "!}},{{!" + SqlSafe(postcode) + "!}},{{!" + SqlSafe(phone) + "!}},{{!" + SqlSafe(email) + "!}},{{!" + SqlSafe(relationship) + "!}},{{!" + SqlSafe(dependant) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step61(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        System.Net.ServicePointManager.Expect100Continue = False
        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from probate_utilities where willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading Probate utility data"
        Me.Refresh()

        'clean the local data

        Dim whichClient As String
        Dim utilityType As String
        Dim provider As String
        Dim providerAddress As String
        Dim providerPostcode As String
        Dim providerPhone As String
        Dim providerEmail As String
        Dim accountNumber As String
        Dim meterReading As String


        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then whichClient = "" Else whichClient = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then utilityType = "" Else utilityType = dbReader.GetValue(3).ToString
            If dbReader.GetValue(4).ToString = "" Then provider = "" Else provider = dbReader.GetValue(4).ToString
            If dbReader.GetValue(5).ToString = "" Then providerAddress = "" Else providerAddress = dbReader.GetValue(5).ToString
            If dbReader.GetValue(6).ToString = "" Then providerPostcode = "" Else providerPostcode = dbReader.GetValue(6).ToString
            If dbReader.GetValue(7).ToString = "" Then providerPhone = "" Else providerPhone = dbReader.GetValue(7).ToString
            If dbReader.GetValue(8).ToString = "" Then providerEmail = "" Else providerEmail = dbReader.GetValue(8).ToString
            If dbReader.GetValue(9).ToString = "" Then accountNumber = "" Else accountNumber = dbReader.GetValue(9).ToString
            If dbReader.GetValue(10).ToString = "" Then meterReading = "" Else meterReading = dbReader.GetValue(10).ToString

            sqlToSend = "insert into probate_utilities (willClientSequence,whichClient ,utilityType ,provider ,providerAddress ,providerPostcode ,providerPhone ,providerEmail ,accountNumber ,meterReading ) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(whichClient) + "!}},{{!" + SqlSafe(utilityType) + "!}},{{!" + SqlSafe(provider) + "!}},{{!" + SqlSafe(providerAddress) + "!}},{{!" + SqlSafe(providerPostcode) + "!}},{{!" + SqlSafe(providerPhone) + "!}},{{!" + SqlSafe(providerEmail) + "!}},{{!" + SqlSafe(accountNumber) + "!}},{{!" + SqlSafe(meterReading) + "!}})"
            sqlToSend = sqlToSend.Replace("'", "~")
            'check for naughty characters
            sqlToSend = WebUtility.HtmlEncode(sqlToSend)
            sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
            sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
            sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
            ' MsgBox(sqlToSend)
            responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
            'MsgBox(responseFromServer)


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If
    End Function

    Function step100()

        Dim sql As String = "select apikey from userdata"
        Dim currentAPI As String = runSQLwithID(sql)
        Dim newAPI As String = ""

        'update audit log
        Me.auditlog.Text = "Regen encryption key"
        Me.Refresh()

        System.Net.ServicePointManager.Expect100Continue = False
        Dim toSend As String
        Dim responsefromserver As String
        toSend = "api=" + Rot13(currentAPI).Replace(" ", "^").Replace("=", "*%*")
        ' MsgBox(toSend)
        responsefromserver = PHPPost(remoteSyncURL + "regen-api-encrypted.php", "POST", toSend)
        ' MsgBox(responsefromserver)
        newAPI = responsefromserver
        sql = "update userdata set apikey='" + newAPI + "'"
        runSQL(sql)
        Return "OK"

    End Function

    Function step101()

        'update audit log
        Me.auditlog.Text = "Processing remote data - please wait"
        Me.Refresh()

        System.Net.ServicePointManager.Expect100Continue = False
        Dim toSend As String
        Dim responsefromserver As String
        toSend = "key=tr33Fr0g"
        ' MsgBox(toSend)
        responsefromserver = PHPPost(remoteSyncURL + "process-data-from-vb.php", "POST", toSend)

        '  MsgBox(responsefromserver)
        Return responsefromserver


    End Function

    Function uploadAudioFiles(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from electronicFiles where title='pending upload' and willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading audio files - this may take some time, please be patient"
        Me.Refresh()

        'clean the local data
        Dim counter As Integer = 0
        Dim title As String
        Dim localfilename As String
        Dim serverfilename As String
        Dim clientReferenceNumber As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read
            counter = counter + 1

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then title = "" Else title = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then localfilename = "" Else localfilename = dbReader.GetValue(3).ToString

            'work out a unique filename for the server
            clientReferenceNumber = runSQLwithID("select clientReferenceNumber from will_instruction_stage2_data where willClientSequence=" + localWillClientSequence)
            If clientReferenceNumber = "" Then clientReferenceNumber = username + "-" + localWillClientSequence
            serverfilename = clientReferenceNumber + "-" + counter.ToString + ".wav"

            Try

                'now upload local file to server
                Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(remoteRecordingFTPAddress + serverfilename), System.Net.FtpWebRequest)
                request.Credentials = New System.Net.NetworkCredential(ftpUsername, ftpPassword)
                request.Method = System.Net.WebRequestMethods.Ftp.UploadFile

                Dim file() As Byte = System.IO.File.ReadAllBytes(localfilename)

                Dim strz As System.IO.Stream = request.GetRequestStream()
                strz.Write(file, 0, file.Length)
                strz.Close()
                strz.Dispose()


                'now send sql data to server
                sqlToSend = "insert into electronicFiles (willClientSequence,filename,associateWith) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(serverfilename) + "!}},  {{!" + SqlSafe(clientReferenceNumber) + "!}})"
                sqlToSend = sqlToSend.Replace("'", "~")
                'check for naughty characters
                sqlToSend = WebUtility.HtmlEncode(sqlToSend)
                sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
                sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
                sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
                ' MsgBox(sqlToSend)
                responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
                'MsgBox(responseFromServer)

                'finally update local database to say recording sent
                runSQL("update electronicFiles set title='uploaded' where sequence=" + dbReader.GetValue(0).ToString)

            Catch ex As Exception

                MsgBox("Audio file could not be found")

            End Try


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If

        Return True

    End Function

    Function uploadSignatureFiles(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from electronicFiles where associateWith='Declaration' and willClientSequence=" + localWillClientSequence
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'update audit log
        Me.auditlog.Text = "Uploading signatures"
        Me.Refresh()

        'clean the local data
        Dim counter As Integer = 0
        Dim title As String
        Dim localfilename As String
        Dim serverfilename As String
        Dim clientReferenceNumber As String

        Dim sqlToSend As String
        Dim responseFromServer As String = ""

        While dbReader.Read
            counter = counter + 1

            responseFromServer = ""
            If dbReader.GetValue(2).ToString = "" Then title = "" Else title = dbReader.GetValue(2).ToString
            If dbReader.GetValue(3).ToString = "" Then localfilename = "" Else localfilename = dbReader.GetValue(3).ToString

            'work out a unique filename for the server
            clientReferenceNumber = runSQLwithID("select clientReferenceNumber from will_instruction_stage2_data where willClientSequence=" + localWillClientSequence)
            If clientReferenceNumber = "" Then clientReferenceNumber = username + "-" + localWillClientSequence
            serverfilename = clientReferenceNumber + "-" + Replace(title, " ", "_") + ".jpg"

            Try
                'now upload local file to server
                Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(remoteSignaturesFTPAddress + serverfilename), System.Net.FtpWebRequest)
                request.Credentials = New System.Net.NetworkCredential(ftpUsername, ftpPassword)
                request.Method = System.Net.WebRequestMethods.Ftp.UploadFile

                Dim file() As Byte = System.IO.File.ReadAllBytes(localfilename)

                Dim strz As System.IO.Stream = request.GetRequestStream()
                strz.Write(file, 0, file.Length)
                strz.Close()
                strz.Dispose()

                'now send sql data to server
                sqlToSend = "insert into electronicFiles (willClientSequence,title,filename,associateWith) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(title) + "!}}, {{!" + SqlSafe(serverfilename) + "!}},  {{!" + SqlSafe(clientReferenceNumber) + "!}})"
                sqlToSend = sqlToSend.Replace("'", "~")
                'check for naughty characters
                sqlToSend = WebUtility.HtmlEncode(sqlToSend)
                sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
                sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")
                sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")
                ' MsgBox(sqlToSend)
                responseFromServer = PHPPost(remoteSyncURL + "submitSQL-encrypted.php", "POST", sqlToSend)
                ' MsgBox(responseFromServer)
            Catch ex As Exception
                MsgBox("Signature file could not be found")
            End Try


        End While

        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If

        Return True

    End Function

    Function uploadSpyData(ByVal apikey As String, ByVal localWillClientSequence As String, ByVal serverWillClientSequence As String)

        'first off, tell server to remove all previous spy data for this client
        Try

            System.Net.ServicePointManager.Expect100Continue = False
            Dim strURL As String = remoteSyncURL + "reset_spy_data.php?apikey=" + apikey + "&willClientSequence=" + serverWillClientSequence

            'MsgBox(strURL)
            Dim strOutput As String = ""

            Dim wrResponse As WebResponse
            Dim wrRequest As WebRequest = HttpWebRequest.Create(strURL)

            auditlog.Text = "resetting serverWillClientSequence..." & Environment.NewLine

            wrResponse = wrRequest.GetResponse()

            Using sr As New StreamReader(wrResponse.GetResponseStream())
                strOutput = sr.ReadToEnd()
                ' Close and clean up the StreamReader
                sr.Close()
            End Using

            auditlog.Text = strOutput

            'MsgBox(strOutput)

        Catch ex As Exception

            MsgBox(ex.Message)

        End Try

        'now send all the spy data but as one long string??

        '''''''''''''''''''''''''''''''''''''''
        'GET DATA FROM ACCESS & CONVERT TO SQL
        '''''''''''''''''''''''''''''''''''''''

        Dim dbConn As New OleDb.OleDbConnection
        dbConn.ConnectionString = connectionString
        dbConn.Open()

        Dim Command As New OleDb.OleDbCommand
        Dim dbReader As OleDb.OleDbDataReader

        Dim sql As String = "select * from electronicFiles where willClientSequence=" + localWillClientSequence + " and associateWith='Spying'"
        Command.CommandText = sql
        Command.Connection = dbConn

        dbReader = Command.ExecuteReader

        'MsgBox(sql)

        'update audit log
        Me.auditlog.Text = "Uploading Activity Debugging Data"
        Me.Refresh()

        'clean the local data

        Dim rawEventTimestamp As String
        Dim eventDetail As String

        Dim sqlToSend As String = ""
        Dim responseFromServer As String = ""

        While dbReader.Read

            If dbReader.GetValue(2).ToString = "" Then rawEventTimestamp = "" Else rawEventTimestamp = dbReader.GetValue(2).ToString.Replace("/", "-")
            If dbReader.GetValue(3).ToString = "" Then eventDetail = "" Else eventDetail = dbReader.GetValue(3).ToString

            sqlToSend = sqlToSend + "insert into client_spy_data (willClientSequence,rawEventTimestamp ,eventDetail) values (" + serverWillClientSequence + ",  {{!" + SqlSafe(rawEventTimestamp) + "!}},{{!" + SqlSafe(eventDetail) + "!}})"
            'now add command delimiter
            sqlToSend = sqlToSend + "¬"

        End While

        'now send multi-line SQL command
        ' MsgBox("LOOPED=" + sqlToSend)
        'clean up data
        sqlToSend = sqlToSend.Replace("'", "~")

        'check for naughty characters
        sqlToSend = WebUtility.HtmlEncode(sqlToSend)
        sqlToSend = Replace(sqlToSend, "&", ";;DANAND;;")
        sqlToSend = Replace(sqlToSend, "£", ";;DANPOUND;;")

        sqlToSend = "apikey=" + apikey + "&publicKey=" + Rot13(sqlToSend).Replace(" ", "^").Replace("=", "*%*")

        ' MsgBox(sqlToSend)

        responseFromServer = PHPPost(remoteSyncURL + "receive_spy_data.php", "POST", sqlToSend)
        ' MsgBox(responseFromServer)
        dbReader.Close()
        dbConn.Close()
        If responseFromServer = "" Or responseFromServer = "OK" Then
            Return "OK"
        Else

            Return (responseFromServer)
        End If

        Return True

    End Function

    Function resetServerData(apikey As String, serverWillClientSequence As String)

        'this stage simply gets the serverWillClientSequence

        Try

            System.Net.ServicePointManager.Expect100Continue = False
            Dim strURL As String = remoteSyncURL + "prepare_for_updated_record.php?apikey=" + apikey + "&willClientSequence=" + serverWillClientSequence

            'MsgBox(strURL)
            Dim strOutput As String = ""

            Dim wrResponse As WebResponse
            Dim wrRequest As WebRequest = HttpWebRequest.Create(strURL)

            auditlog.Text = "resetting serverWillClientSequence..." & Environment.NewLine

            wrResponse = wrRequest.GetResponse()

            Using sr As New StreamReader(wrResponse.GetResponseStream())
                strOutput = sr.ReadToEnd()
                ' Close and clean up the StreamReader
                sr.Close()
            End Using

            auditlog.Text = strOutput

            '  MsgBox(strOutput)
            If strOutput = "Error" Then
                Return -1
            Else
                Return strOutput
            End If

        Catch ex As Exception

            MsgBox(ex.Message)

        End Try

        Return False   'default

    End Function

    Public Function PHPPost(ByVal url As String, ByVal method As String, ByVal data As String)
        Try

            If url = remoteSyncURL + "submitSQL-encrypted.php" Then url = remoteSyncURL + "submitSQL-encrypted.php"
            If url = remoteSyncURL + "submitSQL-GetSequence-encrypted.php" Then url = remoteSyncURL + "submitSQL-GetSequence-encrypted.php"
            If url = remoteSyncURL + "workout-stages.php" Then url = remoteSyncURL + "workout-stages-encrypted.php"
            If url = remoteSyncURL + "workout-linked-wills.php" Then url = remoteSyncURL + "workout-linked-wills-encrypted.php"


            System.Net.ServicePointManager.Expect100Continue = False
            Dim request As System.Net.WebRequest = System.Net.WebRequest.Create(url)
            request.Method = method
            Dim postData = data
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()
            Dim response As WebResponse = request.GetResponse()
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            reader.Close()
            dataStream.Close()
            response.Close()
            ' MsgBox(url)
            ' MsgBox(data)
            ' MsgBox(responseFromServer)
            Return (responseFromServer)
        Catch ex As Exception
            Dim error1 As String = ErrorToString()
            If error1 = "Invalid URI: The format of the URI could not be determined." Then
                MsgBox("ERROR! Must have HTTP:// before the URL.")
            Else
                MsgBox(error1)
            End If
            Return ("ERROR")
        End Try
    End Function

    Public Function Rot13(ByVal value As String) As String
        ' Could be stored as integers directly.
        Dim lowerA As Integer = Asc("a"c)
        Dim lowerZ As Integer = Asc("z"c)
        Dim lowerM As Integer = Asc("m"c)

        Dim upperA As Integer = Asc("A"c)
        Dim upperZ As Integer = Asc("Z"c)
        Dim upperM As Integer = Asc("M"c)

        ' Convert to character array.
        Dim array As Char() = value.ToCharArray

        ' Loop over string.
        Dim i As Integer
        For i = 0 To array.Length - 1

            ' Convert to integer.
            Dim number As Integer = Asc(array(i))

            ' Shift letters.
            If ((number >= lowerA) AndAlso (number <= lowerZ)) Then
                If (number > lowerM) Then
                    number -= 13
                Else
                    number += 13
                End If
            ElseIf ((number >= upperA) AndAlso (number <= upperZ)) Then
                If (number > upperM) Then
                    number -= 13
                Else
                    number += 13
                End If
            End If

            ' Convert to character.
            array(i) = Chr(number)
        Next i

        ' Return string.
        Return New String(array)
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        uploadSpyData("dAxffyR1Ge", 51, 51)
        MsgBox("done")
    End Sub
End Class