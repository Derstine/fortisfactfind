﻿Imports System.Drawing.Drawing2D
Imports System.IO

Public Class clientsignatures
    Private Sub factfind_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CenterForm(Me)

        spy("Client Signatures screen opened")

        Me.Refresh()
        'set some default variables, radio buttons and statuses
        setDefaults()

        'now populate screen with client data
        populateScreen()

        Me.PictureBox1.Refresh()
        Me.PictureBox2.Refresh()

        signature_client1.Reset()
        signature_client2.Reset()

    End Sub

    Function setDefaults()

        'need to clear some data as it looks like resetForm doesn't work in my test
        person1Title.Text = ""
        person1Forenames.Text = ""
        person1Surname.Text = ""

        person2Title.Text = ""
        person2Forenames.Text = ""
        person2Surname.Text = ""

        PictureBox1.Image = Nothing
        PictureBox2.Image = Nothing
        Return True

    End Function

    Function populateScreen()

        If clientID <> "" And clientID <> "0" Then
            'we have some data to popultate
            Dim result As String
            Dim results As Array
            Dim sql As String
            Dim typeOfClient As String = ""

            'mirror or single
            sql = "select typeOfWill from will_instruction_stage1_data where willClientSequence=" + clientID
            result = runSQLwithID(sql)
            If result = "single" Then typeOfClient = "single" Else typeOfClient = "joint"

            If typeOfClient = "single" Then GroupBox2.Visible = False Else GroupBox2.Visible = True
            'personal details
            sql = "select * from will_instruction_stage2_data where willClientSequence=" + clientID
            results = runSQLwithArray(sql)
            person1Title.Text = results(12)
            person1Surname.Text = results(13)
            person1Forenames.Text = results(14)

            person2Title.Text = results(23)
            person2Surname.Text = results(24)
            person2Forenames.Text = results(25)

            'as it's an existing client, we need to change the button text
            Button1.Text = "Save"

            GroupBox1.Text = person1Forenames.Text + " " + person1Surname.Text
            GroupBox2.Text = person2Forenames.Text + " " + person2Surname.Text

            client1Yes.Checked = False
            client1No.Checked = True

            client2No.Checked = True
            client2Yes.Checked = False


            'do we have an existing signature file?
            Dim filename As String
            filename = "\signature-" + Me.GroupBox1.Text.Replace(" ", "-") + "(" + clientID + ").jpg"
            filename = localFolder + filename
            If File.Exists(filename) Then
                ' MsgBox("we have signature for client 1")P
                Dim bmp1 As Bitmap = Bitmap.FromFile(filename)
                PictureBox1.Image = New Bitmap(bmp1)
                bmp1.Dispose()
                GC.Collect()
            End If

            filename = "\signature-" + Me.GroupBox2.Text.Replace(" ", "-") + "(" + clientID + ").jpg"
            filename = localFolder + filename
            If File.Exists(filename) Then
                '  MsgBox("we have signature for client 2")
                Dim bmp2 As Bitmap = Bitmap.FromFile(filename)
                PictureBox2.Image = New Bitmap(bmp2)
                bmp2.Dispose()
                GC.Collect()
            End If
        End If


        Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        spy("Client Signatures screen closed - cancel button pressed")
        Me.Close()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Dim result As String
        Dim sql As String = ""
        Dim localFolder As String
        Dim filename As String

        'save signatures
        Dim bmp1 As New Drawing.Bitmap(PictureBox1.Width, PictureBox1.Height)
        PictureBox1.DrawToBitmap(bmp1, PictureBox1.ClientRectangle)

        localFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        filename = "\signature-" + Me.GroupBox1.Text.Replace(" ", "-") + "(" + clientID + ").jpg"

        Dim bmpToSave1 = New Bitmap(bmp1)
        bmpToSave1.Save(localFolder + filename, System.Drawing.Imaging.ImageFormat.Jpeg)

        sql = "insert into electronicFiles (willClientSequence,title,filename,associateWith) values (" + clientID + ",'" + SqlSafe(Me.GroupBox1.Text) + " signature','" + localFolder + filename + "','Declaration')"
        runSQL(sql)

        If client1Yes.Checked = True Then
            setWillLookup("Client1CoolingOffWaived", "Y")
        Else
            setWillLookup("Client1CoolingOffWaived", "N")
        End If

        'is there are client 2??
        sql = "select typeOfWill from will_instruction_stage1_data where willClientSequence=" + clientID
        result = runSQLwithID(sql)
        If result = "mirror" Then
            Dim bmp2 As New Drawing.Bitmap(PictureBox2.Width, PictureBox2.Height)
            PictureBox2.DrawToBitmap(bmp2, PictureBox2.ClientRectangle)

            localFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
            filename = "\signature-" + Me.GroupBox2.Text.Replace(" ", "-") + "(" + clientID + ").jpg"
            bmp2.Save(localFolder + filename, System.Drawing.Imaging.ImageFormat.Jpeg)

            sql = "insert into electronicFiles (willClientSequence,title,filename,associateWith) values (" + clientID + ",'" + SqlSafe(Me.GroupBox2.Text) + " signature','" + localFolder + filename + "','Declaration')"
            runSQL(sql)

            If client2Yes.Checked = True Then
                setWillLookup("Client2CoolingOffWaived", "Y")
            Else
                setWillLookup("Client2CoolingOffWaived", "N")
            End If

        End If

        setWillLookup("signatureDate", Now.ToLongDateString)
        setWillLookup("signatureAdviser", getEPCName())

        spy("Client Signatures screen closed - signatures captured")

        Me.Close()

    End Sub


    Private lastPT_client1 As Point
    Private signature_client1 As New GraphicsPath

    Private lastPT_client2 As Point
    Private signature_client2 As New GraphicsPath



    Private Sub PictureBox1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseDown
        If Not IsNothing(signature_client1) Then
            If e.Button = Windows.Forms.MouseButtons.Left Then
                lastPT_client1 = New Point(e.X, e.Y)
            End If
        End If
    End Sub

    Private Sub PictureBox1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseMove
        If Not IsNothing(signature_client1) Then
            If e.Button = Windows.Forms.MouseButtons.Left Then
                Dim curPt As New Point(e.X, e.Y)
                signature_client1.AddLine(lastPT_client1, curPt)
                lastPT_client1 = curPt
                PictureBox1.Refresh()
            End If
        End If
    End Sub

    Private Sub PictureBox1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseUp
        If Not IsNothing(signature_client1) Then
            If e.Button = Windows.Forms.MouseButtons.Left Then
                signature_client1.StartFigure()
            End If
        End If
    End Sub

    Private Sub PictureBox1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox1.Paint
        If Not IsNothing(signature_client1) Then
            e.Graphics.DrawPath(Pens.Black, signature_client1)
        End If
    End Sub


    Private Sub PictureBox2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox2.MouseDown
        If Not IsNothing(signature_client2) Then
            If e.Button = Windows.Forms.MouseButtons.Left Then
                lastPT_client2 = New Point(e.X, e.Y)
            End If
        End If
    End Sub

    Private Sub PictureBox2_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox2.MouseMove
        If Not IsNothing(signature_client2) Then
            If e.Button = Windows.Forms.MouseButtons.Left Then
                Dim curPt As New Point(e.X, e.Y)
                signature_client2.AddLine(lastPT_client2, curPt)
                lastPT_client2 = curPt
                PictureBox2.Refresh()
            End If
        End If
    End Sub

    Private Sub PictureBox2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox2.MouseUp
        If Not IsNothing(signature_client2) Then
            If e.Button = Windows.Forms.MouseButtons.Left Then
                signature_client2.StartFigure()
            End If
        End If
    End Sub

    Private Sub PictureBox2_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox2.Paint
        If Not IsNothing(signature_client2) Then
            e.Graphics.DrawPath(Pens.Black, signature_client2)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class