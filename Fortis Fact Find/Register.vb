﻿Imports Autofac
Imports Autofac.Builder
Imports Fortis.BusinessComponent

Public Module Register
    Public Property Container As Autofac.IContainer
    Public Sub Ioc()
        Dim builder = New Autofac.ContainerBuilder()
        builder.RegisterType(Of Fortis.Datac.CurrentCases)().[As](Of ICurrentCases)()
        builder.RegisterType(Of Fortis.Datac.UserActivity)().[As](Of IUserActivity)()
        Container  = builder.Build(ContainerBuildOptions.None)
    End Sub
End Module