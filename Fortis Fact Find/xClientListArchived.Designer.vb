﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xClientListArchived
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumnClientName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnPostCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnMakeActive = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemButtonEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemButtonEdit1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemButtonEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(1106, 644)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumnClientName, Me.GridColumnAddress, Me.GridColumnPostCode, Me.GridColumnMakeActive})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumnClientName
        '
        Me.GridColumnClientName.Caption = "Client Name"
        Me.GridColumnClientName.Name = "GridColumnClientName"
        Me.GridColumnClientName.Visible = true
        Me.GridColumnClientName.VisibleIndex = 0
        Me.GridColumnClientName.Width = 258
        '
        'GridColumnAddress
        '
        Me.GridColumnAddress.Caption = "Address"
        Me.GridColumnAddress.Name = "GridColumnAddress"
        Me.GridColumnAddress.Visible = true
        Me.GridColumnAddress.VisibleIndex = 1
        Me.GridColumnAddress.Width = 258
        '
        'GridColumnPostCode
        '
        Me.GridColumnPostCode.Caption = "Post Code"
        Me.GridColumnPostCode.Name = "GridColumnPostCode"
        Me.GridColumnPostCode.Visible = true
        Me.GridColumnPostCode.VisibleIndex = 2
        Me.GridColumnPostCode.Width = 406
        '
        'GridColumnMakeActive
        '
        Me.GridColumnMakeActive.Caption = "Make Active"
        Me.GridColumnMakeActive.ColumnEdit = Me.RepositoryItemButtonEdit1
        Me.GridColumnMakeActive.Name = "GridColumnMakeActive"
        Me.GridColumnMakeActive.Visible = true
        Me.GridColumnMakeActive.VisibleIndex = 3
        Me.GridColumnMakeActive.Width = 112
        '
        'RepositoryItemButtonEdit1
        '
        Me.RepositoryItemButtonEdit1.AutoHeight = false
        Me.RepositoryItemButtonEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)})
        Me.RepositoryItemButtonEdit1.LookAndFeel.SkinName = "The Bezier"
        Me.RepositoryItemButtonEdit1.LookAndFeel.UseDefaultLookAndFeel = false
        Me.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1"
        Me.RepositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'xClientListArchived
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1106, 644)
        Me.Controls.Add(Me.GridControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "xClientListArchived"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Archived Cases"
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemButtonEdit1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumnClientName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnPostCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnMakeActive As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemButtonEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
End Class
