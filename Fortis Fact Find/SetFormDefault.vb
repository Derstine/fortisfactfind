﻿Imports ComponentFactory.Krypton.Toolkit
Imports DevExpress.XtraEditors


''' <summary>
''' Specs says that the size of each form should be 1250 x 700
''' </summary>
Public Module SetFormDefault
    Public Sub [Set](form As XtraForm)
        form.Size = New Size(1250, 700)
        form.StartPosition=FormStartPosition.CenterScreen
    End Sub
End Module