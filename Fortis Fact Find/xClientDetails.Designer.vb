﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xClientDetails
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xClientDetails))
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditSingle = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditDual = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEditEmail = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditHomeNumber = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditMobileNumber = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEditAddress = New DevExpress.XtraEditors.MemoEdit()
        Me.DateEditDob1 = New DevExpress.XtraEditors.DateEdit()
        Me.TextEditSurname1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditForenames1 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditMs1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMiss1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMr1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMrs1 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEditPosecode = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditMaritalStatus = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem1 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleSeparator1 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditEmail2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditHomeNumber2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditMobileNumber2 = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEditAddress2 = New DevExpress.XtraEditors.MemoEdit()
        Me.DateEditDob2 = New DevExpress.XtraEditors.DateEdit()
        Me.TextEditSurname2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditForenames2 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditMs2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMiss2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMrs2 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEditPosecode2 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonLookup2 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditMaritalStatus2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckEditMr2 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleLabelItem2 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleSeparator2 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonCancel = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.CheckEditSingle.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditDual.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl1.SuspendLayout
        CType(Me.TextEditEmail.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditHomeNumber.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditMobileNumber.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEditAddress.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEditDob1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEditDob1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditSurname1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditForenames1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMs1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMiss1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMr1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMrs1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditPosecode.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditMaritalStatus.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleSeparator1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl2.SuspendLayout
        CType(Me.TextEditEmail2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditHomeNumber2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditMobileNumber2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEditAddress2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEditDob2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEditDob2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditSurname2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditForenames2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMs2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMiss2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMrs2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditPosecode2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEditMaritalStatus2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEditMr2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleSeparator2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(31, 16)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(79, 16)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Type of Client"
        '
        'CheckEditSingle
        '
        Me.CheckEditSingle.Location = New System.Drawing.Point(138, 2)
        Me.CheckEditSingle.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.CheckEditSingle.Name = "CheckEditSingle"
        Me.CheckEditSingle.Properties.Caption = "Single"
        Me.CheckEditSingle.Size = New System.Drawing.Size(90, 44)
        Me.CheckEditSingle.TabIndex = 6
        '
        'CheckEditDual
        '
        Me.CheckEditDual.Location = New System.Drawing.Point(234, 2)
        Me.CheckEditDual.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.CheckEditDual.Name = "CheckEditDual"
        Me.CheckEditDual.Properties.Caption = "Dual"
        Me.CheckEditDual.Size = New System.Drawing.Size(121, 44)
        Me.CheckEditDual.TabIndex = 7
        '
        'TextEdit1
        '
        Me.TextEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.TextEdit1.Location = New System.Drawing.Point(1021, 5)
        Me.TextEdit1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(169, 38)
        Me.TextEdit1.TabIndex = 9
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.TextEditEmail)
        Me.LayoutControl1.Controls.Add(Me.TextEditHomeNumber)
        Me.LayoutControl1.Controls.Add(Me.TextEditMobileNumber)
        Me.LayoutControl1.Controls.Add(Me.MemoEditAddress)
        Me.LayoutControl1.Controls.Add(Me.DateEditDob1)
        Me.LayoutControl1.Controls.Add(Me.TextEditSurname1)
        Me.LayoutControl1.Controls.Add(Me.TextEditForenames1)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMs1)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMiss1)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMr1)
        Me.LayoutControl1.Controls.Add(Me.CheckEditMrs1)
        Me.LayoutControl1.Controls.Add(Me.TextEditPosecode)
        Me.LayoutControl1.Controls.Add(Me.SimpleButtonLookup1)
        Me.LayoutControl1.Controls.Add(Me.TextEditMaritalStatus)
        Me.LayoutControl1.Location = New System.Drawing.Point(19, 46)
        Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(523, 497)
        Me.LayoutControl1.TabIndex = 10
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'TextEditEmail
        '
        Me.TextEditEmail.Location = New System.Drawing.Point(93, 451)
        Me.TextEditEmail.Name = "TextEditEmail"
        Me.TextEditEmail.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit8.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditEmail.Size = New System.Drawing.Size(428, 44)
        Me.TextEditEmail.StyleController = Me.LayoutControl1
        Me.TextEditEmail.TabIndex = 17
        '
        'TextEditHomeNumber
        '
        Me.TextEditHomeNumber.Location = New System.Drawing.Point(93, 403)
        Me.TextEditHomeNumber.Name = "TextEditHomeNumber"
        Me.TextEditHomeNumber.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit7.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditHomeNumber.Size = New System.Drawing.Size(427, 44)
        Me.TextEditHomeNumber.StyleController = Me.LayoutControl1
        Me.TextEditHomeNumber.TabIndex = 16
        '
        'TextEditMobileNumber
        '
        Me.TextEditMobileNumber.Location = New System.Drawing.Point(93, 355)
        Me.TextEditMobileNumber.Name = "TextEditMobileNumber"
        Me.TextEditMobileNumber.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit6.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditMobileNumber.Size = New System.Drawing.Size(427, 44)
        Me.TextEditMobileNumber.StyleController = Me.LayoutControl1
        Me.TextEditMobileNumber.TabIndex = 15
        '
        'MemoEditAddress
        '
        Me.MemoEditAddress.Location = New System.Drawing.Point(93, 306)
        Me.MemoEditAddress.Name = "MemoEditAddress"
        Me.MemoEditAddress.Size = New System.Drawing.Size(427, 45)
        Me.MemoEditAddress.StyleController = Me.LayoutControl1
        Me.MemoEditAddress.TabIndex = 13
        '
        'DateEditDob1
        '
        Me.DateEditDob1.EditValue = Nothing
        Me.DateEditDob1.Location = New System.Drawing.Point(93, 162)
        Me.DateEditDob1.Name = "DateEditDob1"
        Me.DateEditDob1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditDob1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditDob1.Properties.ContextImageOptions.Image = CType(resources.GetObject("DateEdit1.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.DateEditDob1.Size = New System.Drawing.Size(427, 44)
        Me.DateEditDob1.StyleController = Me.LayoutControl1
        Me.DateEditDob1.TabIndex = 10
        '
        'TextEditSurname1
        '
        Me.TextEditSurname1.Location = New System.Drawing.Point(93, 114)
        Me.TextEditSurname1.Name = "TextEditSurname1"
        Me.TextEditSurname1.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit3.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditSurname1.Size = New System.Drawing.Size(427, 44)
        Me.TextEditSurname1.StyleController = Me.LayoutControl1
        Me.TextEditSurname1.TabIndex = 9
        '
        'TextEditForenames1
        '
        Me.TextEditForenames1.Location = New System.Drawing.Point(93, 66)
        Me.TextEditForenames1.Name = "TextEditForenames1"
        Me.TextEditForenames1.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit2.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditForenames1.Size = New System.Drawing.Size(427, 44)
        Me.TextEditForenames1.StyleController = Me.LayoutControl1
        Me.TextEditForenames1.TabIndex = 8
        '
        'CheckEditMs1
        '
        Me.CheckEditMs1.Location = New System.Drawing.Point(433, 18)
        Me.CheckEditMs1.Name = "CheckEditMs1"
        Me.CheckEditMs1.Properties.Caption = "Ms."
        Me.CheckEditMs1.Size = New System.Drawing.Size(87, 44)
        Me.CheckEditMs1.StyleController = Me.LayoutControl1
        Me.CheckEditMs1.TabIndex = 7
        '
        'CheckEditMiss1
        '
        Me.CheckEditMiss1.Location = New System.Drawing.Point(335, 18)
        Me.CheckEditMiss1.Name = "CheckEditMiss1"
        Me.CheckEditMiss1.Properties.Caption = "Miss"
        Me.CheckEditMiss1.Size = New System.Drawing.Size(94, 44)
        Me.CheckEditMiss1.StyleController = Me.LayoutControl1
        Me.CheckEditMiss1.TabIndex = 6
        '
        'CheckEditMr1
        '
        Me.CheckEditMr1.Location = New System.Drawing.Point(110, 18)
        Me.CheckEditMr1.Name = "CheckEditMr1"
        Me.CheckEditMr1.Properties.Caption = "Mr."
        Me.CheckEditMr1.Size = New System.Drawing.Size(125, 44)
        Me.CheckEditMr1.StyleController = Me.LayoutControl1
        Me.CheckEditMr1.TabIndex = 5
        '
        'CheckEditMrs1
        '
        Me.CheckEditMrs1.Location = New System.Drawing.Point(239, 18)
        Me.CheckEditMrs1.Name = "CheckEditMrs1"
        Me.CheckEditMrs1.Properties.Caption = "Mrs."
        Me.CheckEditMrs1.Size = New System.Drawing.Size(92, 44)
        Me.CheckEditMrs1.StyleController = Me.LayoutControl1
        Me.CheckEditMrs1.TabIndex = 4
        '
        'TextEditPosecode
        '
        Me.TextEditPosecode.Location = New System.Drawing.Point(93, 210)
        Me.TextEditPosecode.Name = "TextEditPosecode"
        Me.TextEditPosecode.Properties.AutoHeight = false
        Me.TextEditPosecode.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit4.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditPosecode.Properties.Name = "TextEdit4"
        Me.TextEditPosecode.Size = New System.Drawing.Size(301, 44)
        Me.TextEditPosecode.StyleController = Me.LayoutControl1
        Me.TextEditPosecode.TabIndex = 11
        '
        'SimpleButtonLookup1
        '
        Me.SimpleButtonLookup1.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButtonLookup1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup1.Location = New System.Drawing.Point(398, 210)
        Me.SimpleButtonLookup1.Name = "SimpleButtonLookup1"
        Me.SimpleButtonLookup1.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButtonLookup1.StyleController = Me.LayoutControl1
        Me.SimpleButtonLookup1.TabIndex = 12
        Me.SimpleButtonLookup1.Text = "Lookup"
        '
        'TextEditMaritalStatus
        '
        Me.TextEditMaritalStatus.Location = New System.Drawing.Point(93, 258)
        Me.TextEditMaritalStatus.Name = "TextEditMaritalStatus"
        Me.TextEditMaritalStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TextEditMaritalStatus.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit5.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditMaritalStatus.Properties.Items.AddRange(New Object() {"Married", "Single", "Partnered", "Widowed", "Divorced"})
        Me.TextEditMaritalStatus.Size = New System.Drawing.Size(427, 44)
        Me.TextEditMaritalStatus.StyleController = Me.LayoutControl1
        Me.TextEditMaritalStatus.TabIndex = 14
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[False]
        Me.Root.GroupBordersVisible = false
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.SimpleLabelItem1, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.SimpleSeparator1})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(523, 497)
        Me.Root.TextVisible = false
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.CheckEditMrs1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(237, 16)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(96, 48)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = false
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.CheckEditMr1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 16)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(237, 48)
        Me.LayoutControlItem2.Text = "Title"
        Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(103, 16)
        Me.LayoutControlItem2.TextToControlDistance = 5
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.CheckEditMiss1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(333, 16)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(98, 48)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = false
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.CheckEditMs1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(431, 16)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(91, 48)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = false
        '
        'SimpleLabelItem1
        '
        Me.SimpleLabelItem1.AllowHotTrack = false
        Me.SimpleLabelItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.SimpleLabelItem1.AppearanceItemCaption.Options.UseFont = true
        Me.SimpleLabelItem1.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem1.MinSize = New System.Drawing.Size(45, 16)
        Me.SimpleLabelItem1.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem1.Size = New System.Drawing.Size(522, 16)
        Me.SimpleLabelItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.SimpleLabelItem1.Text = "Client 1"
        Me.SimpleLabelItem1.TextSize = New System.Drawing.Size(48, 16)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.TextEditForenames1
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 64)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(522, 48)
        Me.LayoutControlItem5.Text = "Forename(s)"
        Me.LayoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem5.TextToControlDistance = 5
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.TextEditSurname1
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 112)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(522, 48)
        Me.LayoutControlItem6.Text = "Surname"
        Me.LayoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem6.TextToControlDistance = 5
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.DateEditDob1
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 160)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(522, 48)
        Me.LayoutControlItem7.Text = "DOB"
        Me.LayoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem7.TextToControlDistance = 5
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.TextEditPosecode
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 208)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(396, 48)
        Me.LayoutControlItem8.Text = "Postcode"
        Me.LayoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(86, 13)
        Me.LayoutControlItem8.TextToControlDistance = 5
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.SimpleButtonLookup1
        Me.LayoutControlItem9.Location = New System.Drawing.Point(396, 208)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(126, 48)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = false
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem10.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem10.Control = Me.MemoEditAddress
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 304)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(522, 49)
        Me.LayoutControlItem10.Text = "Address"
        Me.LayoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem10.TextToControlDistance = 5
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.TextEditMaritalStatus
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 256)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(522, 48)
        Me.LayoutControlItem11.Text = "Marital Status"
        Me.LayoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem11.TextToControlDistance = 5
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.TextEditMobileNumber
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 353)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(522, 48)
        Me.LayoutControlItem12.Text = "Mobile Number"
        Me.LayoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem12.TextToControlDistance = 5
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.TextEditHomeNumber
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 401)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(522, 48)
        Me.LayoutControlItem13.Text = "Home Number"
        Me.LayoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem13.TextToControlDistance = 5
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.TextEditEmail
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 449)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(523, 48)
        Me.LayoutControlItem14.Text = "Email"
        Me.LayoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem14.TextToControlDistance = 5
        '
        'SimpleSeparator1
        '
        Me.SimpleSeparator1.AllowHotTrack = false
        Me.SimpleSeparator1.Location = New System.Drawing.Point(522, 0)
        Me.SimpleSeparator1.Name = "SimpleSeparator1"
        Me.SimpleSeparator1.Size = New System.Drawing.Size(1, 449)
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl2.Controls.Add(Me.TextEditEmail2)
        Me.LayoutControl2.Controls.Add(Me.TextEditHomeNumber2)
        Me.LayoutControl2.Controls.Add(Me.TextEditMobileNumber2)
        Me.LayoutControl2.Controls.Add(Me.MemoEditAddress2)
        Me.LayoutControl2.Controls.Add(Me.DateEditDob2)
        Me.LayoutControl2.Controls.Add(Me.TextEditSurname2)
        Me.LayoutControl2.Controls.Add(Me.TextEditForenames2)
        Me.LayoutControl2.Controls.Add(Me.CheckEditMs2)
        Me.LayoutControl2.Controls.Add(Me.CheckEditMiss2)
        Me.LayoutControl2.Controls.Add(Me.CheckEditMrs2)
        Me.LayoutControl2.Controls.Add(Me.TextEditPosecode2)
        Me.LayoutControl2.Controls.Add(Me.SimpleButtonLookup2)
        Me.LayoutControl2.Controls.Add(Me.TextEditMaritalStatus2)
        Me.LayoutControl2.Controls.Add(Me.CheckEditMr2)
        Me.LayoutControl2.Location = New System.Drawing.Point(574, 43)
        Me.LayoutControl2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(616, 498)
        Me.LayoutControl2.TabIndex = 15
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.ImageOptions.Image = CType(resources.GetObject("SimpleButton3.ImageOptions.Image"),System.Drawing.Image)
        Me.SimpleButton3.Location = New System.Drawing.Point(463, 210)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(150, 42)
        Me.SimpleButton3.StyleController = Me.LayoutControl2
        Me.SimpleButton3.TabIndex = 18
        Me.SimpleButton3.Text = "Copy Client 1"
        '
        'TextEditEmail2
        '
        Me.TextEditEmail2.Location = New System.Drawing.Point(93, 458)
        Me.TextEditEmail2.Name = "TextEditEmail2"
        Me.TextEditEmail2.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit9.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditEmail2.Size = New System.Drawing.Size(521, 44)
        Me.TextEditEmail2.StyleController = Me.LayoutControl2
        Me.TextEditEmail2.TabIndex = 17
        '
        'TextEditHomeNumber2
        '
        Me.TextEditHomeNumber2.Location = New System.Drawing.Point(93, 410)
        Me.TextEditHomeNumber2.Name = "TextEditHomeNumber2"
        Me.TextEditHomeNumber2.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit10.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditHomeNumber2.Size = New System.Drawing.Size(520, 44)
        Me.TextEditHomeNumber2.StyleController = Me.LayoutControl2
        Me.TextEditHomeNumber2.TabIndex = 16
        '
        'TextEditMobileNumber2
        '
        Me.TextEditMobileNumber2.Location = New System.Drawing.Point(93, 362)
        Me.TextEditMobileNumber2.Name = "TextEditMobileNumber2"
        Me.TextEditMobileNumber2.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit11.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditMobileNumber2.Size = New System.Drawing.Size(520, 44)
        Me.TextEditMobileNumber2.StyleController = Me.LayoutControl2
        Me.TextEditMobileNumber2.TabIndex = 15
        '
        'MemoEditAddress2
        '
        Me.MemoEditAddress2.Location = New System.Drawing.Point(93, 306)
        Me.MemoEditAddress2.Name = "MemoEditAddress2"
        Me.MemoEditAddress2.Size = New System.Drawing.Size(520, 52)
        Me.MemoEditAddress2.StyleController = Me.LayoutControl2
        Me.MemoEditAddress2.TabIndex = 13
        '
        'DateEditDob2
        '
        Me.DateEditDob2.EditValue = Nothing
        Me.DateEditDob2.Location = New System.Drawing.Point(93, 162)
        Me.DateEditDob2.Name = "DateEditDob2"
        Me.DateEditDob2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditDob2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditDob2.Properties.ContextImageOptions.Image = CType(resources.GetObject("DateEdit2.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.DateEditDob2.Size = New System.Drawing.Size(520, 44)
        Me.DateEditDob2.StyleController = Me.LayoutControl2
        Me.DateEditDob2.TabIndex = 10
        '
        'TextEditSurname2
        '
        Me.TextEditSurname2.Location = New System.Drawing.Point(93, 114)
        Me.TextEditSurname2.Name = "TextEditSurname2"
        Me.TextEditSurname2.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit12.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditSurname2.Size = New System.Drawing.Size(520, 44)
        Me.TextEditSurname2.StyleController = Me.LayoutControl2
        Me.TextEditSurname2.TabIndex = 9
        '
        'TextEditForenames2
        '
        Me.TextEditForenames2.Location = New System.Drawing.Point(93, 66)
        Me.TextEditForenames2.Name = "TextEditForenames2"
        Me.TextEditForenames2.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit13.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditForenames2.Size = New System.Drawing.Size(520, 44)
        Me.TextEditForenames2.StyleController = Me.LayoutControl2
        Me.TextEditForenames2.TabIndex = 8
        '
        'CheckEditMs2
        '
        Me.CheckEditMs2.Location = New System.Drawing.Point(515, 18)
        Me.CheckEditMs2.Name = "CheckEditMs2"
        Me.CheckEditMs2.Properties.Caption = "Ms."
        Me.CheckEditMs2.Size = New System.Drawing.Size(98, 44)
        Me.CheckEditMs2.StyleController = Me.LayoutControl2
        Me.CheckEditMs2.TabIndex = 7
        '
        'CheckEditMiss2
        '
        Me.CheckEditMiss2.Location = New System.Drawing.Point(406, 18)
        Me.CheckEditMiss2.Name = "CheckEditMiss2"
        Me.CheckEditMiss2.Properties.Caption = "Miss"
        Me.CheckEditMiss2.Size = New System.Drawing.Size(105, 44)
        Me.CheckEditMiss2.StyleController = Me.LayoutControl2
        Me.CheckEditMiss2.TabIndex = 6
        '
        'CheckEditMrs2
        '
        Me.CheckEditMrs2.Location = New System.Drawing.Point(299, 18)
        Me.CheckEditMrs2.Name = "CheckEditMrs2"
        Me.CheckEditMrs2.Properties.Caption = "Mrs."
        Me.CheckEditMrs2.Size = New System.Drawing.Size(103, 44)
        Me.CheckEditMrs2.StyleController = Me.LayoutControl2
        Me.CheckEditMrs2.TabIndex = 4
        '
        'TextEditPosecode2
        '
        Me.TextEditPosecode2.Location = New System.Drawing.Point(93, 210)
        Me.TextEditPosecode2.Name = "TextEditPosecode2"
        Me.TextEditPosecode2.Properties.AutoHeight = false
        Me.TextEditPosecode2.Properties.ContextImageOptions.Image = CType(resources.GetObject("TextEdit14.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditPosecode2.Properties.Name = "TextEdit4"
        Me.TextEditPosecode2.Size = New System.Drawing.Size(240, 44)
        Me.TextEditPosecode2.StyleController = Me.LayoutControl2
        Me.TextEditPosecode2.TabIndex = 11
        '
        'SimpleButtonLookup2
        '
        Me.SimpleButtonLookup2.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButtonLookup2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup2.Location = New System.Drawing.Point(337, 210)
        Me.SimpleButtonLookup2.Name = "SimpleButtonLookup2"
        Me.SimpleButtonLookup2.Size = New System.Drawing.Size(122, 42)
        Me.SimpleButtonLookup2.StyleController = Me.LayoutControl2
        Me.SimpleButtonLookup2.TabIndex = 12
        Me.SimpleButtonLookup2.Text = "Lookup"
        '
        'TextEditMaritalStatus2
        '
        Me.TextEditMaritalStatus2.Location = New System.Drawing.Point(93, 258)
        Me.TextEditMaritalStatus2.Name = "TextEditMaritalStatus2"
        Me.TextEditMaritalStatus2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TextEditMaritalStatus2.Properties.ContextImageOptions.Image = CType(resources.GetObject("ComboBoxEdit1.Properties.ContextImageOptions.Image"),System.Drawing.Image)
        Me.TextEditMaritalStatus2.Properties.Items.AddRange(New Object() {"Married", "Single", "Partnered", "Widowed", "Divorced"})
        Me.TextEditMaritalStatus2.Size = New System.Drawing.Size(520, 44)
        Me.TextEditMaritalStatus2.StyleController = Me.LayoutControl2
        Me.TextEditMaritalStatus2.TabIndex = 14
        '
        'CheckEditMr2
        '
        Me.CheckEditMr2.Location = New System.Drawing.Point(207, 18)
        Me.CheckEditMr2.Name = "CheckEditMr2"
        Me.CheckEditMr2.Properties.Caption = "Mr."
        Me.CheckEditMr2.Size = New System.Drawing.Size(88, 44)
        Me.CheckEditMr2.StyleController = Me.LayoutControl2
        Me.CheckEditMr2.TabIndex = 5
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[False]
        Me.LayoutControlGroup1.GroupBordersVisible = false
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.SimpleLabelItem2, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem27, Me.LayoutControlItem28, Me.SimpleSeparator2, Me.LayoutControlItem29})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(616, 498)
        Me.LayoutControlGroup1.TextVisible = false
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.CheckEditMrs2
        Me.LayoutControlItem15.Location = New System.Drawing.Point(297, 16)
        Me.LayoutControlItem15.Name = "LayoutControlItem1"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(107, 48)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = false
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.CheckEditMr2
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 16)
        Me.LayoutControlItem16.Name = "LayoutControlItem2"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(297, 48)
        Me.LayoutControlItem16.Text = "Title"
        Me.LayoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(200, 16)
        Me.LayoutControlItem16.TextToControlDistance = 5
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.CheckEditMiss2
        Me.LayoutControlItem17.Location = New System.Drawing.Point(404, 16)
        Me.LayoutControlItem17.Name = "LayoutControlItem3"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(109, 48)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = false
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.CheckEditMs2
        Me.LayoutControlItem18.Location = New System.Drawing.Point(513, 16)
        Me.LayoutControlItem18.Name = "LayoutControlItem4"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(102, 48)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = false
        '
        'SimpleLabelItem2
        '
        Me.SimpleLabelItem2.AllowHotTrack = false
        Me.SimpleLabelItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.SimpleLabelItem2.AppearanceItemCaption.Options.UseFont = true
        Me.SimpleLabelItem2.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem2.MinSize = New System.Drawing.Size(45, 16)
        Me.SimpleLabelItem2.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem2.Size = New System.Drawing.Size(615, 16)
        Me.SimpleLabelItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.SimpleLabelItem2.Text = "Client 2"
        Me.SimpleLabelItem2.TextSize = New System.Drawing.Size(48, 16)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.TextEditForenames2
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 64)
        Me.LayoutControlItem19.Name = "LayoutControlItem5"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(615, 48)
        Me.LayoutControlItem19.Text = "Forename(s)"
        Me.LayoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem19.TextToControlDistance = 5
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TextEditSurname2
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 112)
        Me.LayoutControlItem20.Name = "LayoutControlItem6"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(615, 48)
        Me.LayoutControlItem20.Text = "Surname"
        Me.LayoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem20.TextToControlDistance = 5
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.DateEditDob2
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 160)
        Me.LayoutControlItem21.Name = "LayoutControlItem7"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(615, 48)
        Me.LayoutControlItem21.Text = "DOB"
        Me.LayoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem21.TextToControlDistance = 5
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.TextEditPosecode2
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 208)
        Me.LayoutControlItem22.Name = "LayoutControlItem8"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(335, 48)
        Me.LayoutControlItem22.Text = "Postcode"
        Me.LayoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(86, 13)
        Me.LayoutControlItem22.TextToControlDistance = 5
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.SimpleButtonLookup2
        Me.LayoutControlItem23.Location = New System.Drawing.Point(335, 208)
        Me.LayoutControlItem23.Name = "LayoutControlItem9"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(126, 48)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = false
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem24.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem24.Control = Me.MemoEditAddress2
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 304)
        Me.LayoutControlItem24.Name = "LayoutControlItem10"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(615, 56)
        Me.LayoutControlItem24.Text = "Address"
        Me.LayoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem24.TextToControlDistance = 5
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.TextEditMaritalStatus2
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 256)
        Me.LayoutControlItem25.Name = "LayoutControlItem11"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(615, 48)
        Me.LayoutControlItem25.Text = "Marital Status"
        Me.LayoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem25.TextToControlDistance = 5
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.TextEditMobileNumber2
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 360)
        Me.LayoutControlItem26.Name = "LayoutControlItem12"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(615, 48)
        Me.LayoutControlItem26.Text = "Mobile Number"
        Me.LayoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem26.TextToControlDistance = 5
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.TextEditHomeNumber2
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 408)
        Me.LayoutControlItem27.Name = "LayoutControlItem13"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(615, 48)
        Me.LayoutControlItem27.Text = "Home Number"
        Me.LayoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem27.TextToControlDistance = 5
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.TextEditEmail2
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 456)
        Me.LayoutControlItem28.Name = "LayoutControlItem14"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(616, 42)
        Me.LayoutControlItem28.Text = "Email"
        Me.LayoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(86, 16)
        Me.LayoutControlItem28.TextToControlDistance = 5
        '
        'SimpleSeparator2
        '
        Me.SimpleSeparator2.AllowHotTrack = false
        Me.SimpleSeparator2.Location = New System.Drawing.Point(615, 0)
        Me.SimpleSeparator2.Name = "SimpleSeparator1"
        Me.SimpleSeparator2.Size = New System.Drawing.Size(1, 456)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.SimpleButton3
        Me.LayoutControlItem29.Location = New System.Drawing.Point(461, 208)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(154, 48)
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = false
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Location = New System.Drawing.Point(109, 562)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(213, 48)
        Me.SimpleButtonSave.TabIndex = 16
        Me.SimpleButtonSave.Text = "Save"
        '
        'SimpleButtonCancel
        '
        Me.SimpleButtonCancel.Location = New System.Drawing.Point(328, 562)
        Me.SimpleButtonCancel.Name = "SimpleButtonCancel"
        Me.SimpleButtonCancel.Size = New System.Drawing.Size(211, 48)
        Me.SimpleButtonCancel.TabIndex = 17
        Me.SimpleButtonCancel.Text = "Close"
        '
        'xClientDetails
        '
        Me.Appearance.Options.UseFont = true
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1248, 622)
        Me.Controls.Add(Me.SimpleButtonCancel)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.LayoutControl2)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.CheckEditDual)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.CheckEditSingle)
        Me.Controls.Add(Me.LabelControl2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "xClientDetails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Client Details"
        CType(Me.CheckEditSingle.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditDual.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl1.ResumeLayout(false)
        CType(Me.TextEditEmail.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditHomeNumber.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditMobileNumber.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEditAddress.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEditDob1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEditDob1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditSurname1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditForenames1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMs1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMiss1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMr1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMrs1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditPosecode.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditMaritalStatus.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleSeparator1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl2.ResumeLayout(false)
        CType(Me.TextEditEmail2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditHomeNumber2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditMobileNumber2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEditAddress2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEditDob2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEditDob2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditSurname2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditForenames2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMs2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMiss2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMrs2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditPosecode2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEditMaritalStatus2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEditMr2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem27,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem28,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleSeparator2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem29,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditDual As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSingle As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEditMr1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMrs1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEditMs1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMiss1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DateEditDob1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEditSurname1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditForenames1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleLabelItem1 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents MemoEditAddress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEditPosecode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEditEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditHomeNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextEditMaritalStatus As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleSeparator1 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEditEmail2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditHomeNumber2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditMobileNumber2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemoEditAddress2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents DateEditDob2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEditSurname2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditForenames2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditMs2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMiss2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMrs2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEditPosecode2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonLookup2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditMaritalStatus2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents CheckEditMr2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleLabelItem2 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleSeparator2 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonCancel As DevExpress.XtraEditors.SimpleButton
End Class
