﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class clientsignatures
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(clientsignatures))
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.client1No = New System.Windows.Forms.RadioButton()
        Me.client1Yes = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.person1Title = New System.Windows.Forms.TextBox()
        Me.person1Surname = New System.Windows.Forms.TextBox()
        Me.person1Forenames = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.client2No = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.client2Yes = New System.Windows.Forms.RadioButton()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.person2Title = New System.Windows.Forms.TextBox()
        Me.person2Surname = New System.Windows.Forms.TextBox()
        Me.person2Forenames = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupBox2.SuspendLayout
        CType(Me.PictureBox2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(1130, 614)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(92, 35)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = false
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 29)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Declaration"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(176,Byte),Integer), CType(CType(210,Byte),Integer))
        Me.Button1.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(1032, 614)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 35)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = false
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.client1No)
        Me.GroupBox1.Controls.Add(Me.client1Yes)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.person1Title)
        Me.GroupBox1.Controls.Add(Me.person1Surname)
        Me.GroupBox1.Controls.Add(Me.person1Forenames)
        Me.GroupBox1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(14, 373)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(580, 235)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Client 1"
        '
        'client1No
        '
        Me.client1No.AutoSize = true
        Me.client1No.Location = New System.Drawing.Point(393, 25)
        Me.client1No.Name = "client1No"
        Me.client1No.Size = New System.Drawing.Size(41, 22)
        Me.client1No.TabIndex = 9
        Me.client1No.TabStop = true
        Me.client1No.Text = "No"
        Me.client1No.UseVisualStyleBackColor = true
        '
        'client1Yes
        '
        Me.client1Yes.AutoSize = true
        Me.client1Yes.Location = New System.Drawing.Point(337, 24)
        Me.client1Yes.Name = "client1Yes"
        Me.client1Yes.Size = New System.Drawing.Size(45, 22)
        Me.client1Yes.TabIndex = 8
        Me.client1Yes.TabStop = true
        Me.client1Yes.Text = "Yes"
        Me.client1Yes.UseVisualStyleBackColor = true
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(6, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(317, 18)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Do you want to waive your 14-day cooling off period?"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Location = New System.Drawing.Point(6, 46)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(555, 148)
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = false
        '
        'person1Title
        '
        Me.person1Title.Enabled = false
        Me.person1Title.Location = New System.Drawing.Point(6, 200)
        Me.person1Title.Name = "person1Title"
        Me.person1Title.Size = New System.Drawing.Size(51, 23)
        Me.person1Title.TabIndex = 3
        '
        'person1Surname
        '
        Me.person1Surname.Enabled = false
        Me.person1Surname.Location = New System.Drawing.Point(337, 200)
        Me.person1Surname.Name = "person1Surname"
        Me.person1Surname.Size = New System.Drawing.Size(198, 23)
        Me.person1Surname.TabIndex = 2
        '
        'person1Forenames
        '
        Me.person1Forenames.Enabled = false
        Me.person1Forenames.Location = New System.Drawing.Point(63, 200)
        Me.person1Forenames.Name = "person1Forenames"
        Me.person1Forenames.Size = New System.Drawing.Size(268, 23)
        Me.person1Forenames.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.client2No)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.client2Yes)
        Me.GroupBox2.Controls.Add(Me.PictureBox2)
        Me.GroupBox2.Controls.Add(Me.person2Title)
        Me.GroupBox2.Controls.Add(Me.person2Surname)
        Me.GroupBox2.Controls.Add(Me.person2Forenames)
        Me.GroupBox2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(630, 373)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(580, 235)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Client 2"
        '
        'client2No
        '
        Me.client2No.AutoSize = true
        Me.client2No.Location = New System.Drawing.Point(393, 26)
        Me.client2No.Name = "client2No"
        Me.client2No.Size = New System.Drawing.Size(41, 22)
        Me.client2No.TabIndex = 11
        Me.client2No.TabStop = true
        Me.client2No.Text = "No"
        Me.client2No.UseVisualStyleBackColor = true
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(6, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(317, 18)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Do you want to waive your 14-day cooling off period?"
        '
        'client2Yes
        '
        Me.client2Yes.AutoSize = true
        Me.client2Yes.Location = New System.Drawing.Point(337, 25)
        Me.client2Yes.Name = "client2Yes"
        Me.client2Yes.Size = New System.Drawing.Size(45, 22)
        Me.client2Yes.TabIndex = 10
        Me.client2Yes.TabStop = true
        Me.client2Yes.Text = "Yes"
        Me.client2Yes.UseVisualStyleBackColor = true
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.Location = New System.Drawing.Point(6, 46)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(555, 148)
        Me.PictureBox2.TabIndex = 5
        Me.PictureBox2.TabStop = false
        '
        'person2Title
        '
        Me.person2Title.Enabled = false
        Me.person2Title.Location = New System.Drawing.Point(6, 200)
        Me.person2Title.Name = "person2Title"
        Me.person2Title.Size = New System.Drawing.Size(51, 23)
        Me.person2Title.TabIndex = 4
        '
        'person2Surname
        '
        Me.person2Surname.Enabled = false
        Me.person2Surname.Location = New System.Drawing.Point(337, 200)
        Me.person2Surname.Name = "person2Surname"
        Me.person2Surname.Size = New System.Drawing.Size(198, 23)
        Me.person2Surname.TabIndex = 2
        '
        'person2Forenames
        '
        Me.person2Forenames.Enabled = false
        Me.person2Forenames.Location = New System.Drawing.Point(63, 200)
        Me.person2Forenames.Name = "person2Forenames"
        Me.person2Forenames.Size = New System.Drawing.Size(268, 23)
        Me.person2Forenames.TabIndex = 1
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TextBox1.Location = New System.Drawing.Point(14, 51)
        Me.TextBox1.Multiline = true
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(1195, 185)
        Me.TextBox1.TabIndex = 23
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label4.Location = New System.Drawing.Point(18, 336)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(859, 25)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "I have read (or have had read to me) and understand the Data Protection Notice."
        '
        'clientsignatures
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(221,Byte),Integer), CType(CType(244,Byte),Integer), CType(CType(250,Byte),Integer))
        Me.ClientSize = New System.Drawing.Size(1234, 661)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.MaximizeBox = false
        Me.Name = "clientsignatures"
        Me.Text = "Client Signatures"
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        CType(Me.PictureBox2,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents Button2 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents person1Surname As TextBox
    Friend WithEvents person1Forenames As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents person2Surname As TextBox
    Friend WithEvents person2Forenames As TextBox
    Friend WithEvents person1Title As TextBox
    Friend WithEvents person2Title As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label3 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents client1No As RadioButton
    Friend WithEvents client1Yes As RadioButton
    Friend WithEvents client2No As RadioButton
    Friend WithEvents client2Yes As RadioButton
End Class
