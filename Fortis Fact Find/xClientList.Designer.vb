﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xClientList
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xClientList))
        Dim EditorButtonImageOptions1 As DevExpress.XtraEditors.Controls.EditorButtonImageOptions = New DevExpress.XtraEditors.Controls.EditorButtonImageOptions()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject4 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim EditorButtonImageOptions2 As DevExpress.XtraEditors.Controls.EditorButtonImageOptions = New DevExpress.XtraEditors.Controls.EditorButtonImageOptions()
        Dim SerializableAppearanceObject5 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject6 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject7 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject8 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim EditorButtonImageOptions3 As DevExpress.XtraEditors.Controls.EditorButtonImageOptions = New DevExpress.XtraEditors.Controls.EditorButtonImageOptions()
        Dim SerializableAppearanceObject9 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject10 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject11 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject12 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButtonNewClient = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonArchivedCases = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumnAppointment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnClientName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumnPostCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemButtonEditEdit = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.RepositoryItemButtonEditDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.RepositoryItemButtonEditArchive = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton7 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton8 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelControl1.SuspendLayout
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemButtonEditEdit,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemButtonEditDelete,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.RepositoryItemButtonEditArchive,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.SimpleButtonNewClient)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonArchivedCases)
        Me.PanelControl1.Controls.Add(Me.SimpleButton2)
        Me.PanelControl1.Controls.Add(Me.SimpleButton1)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1262, 82)
        Me.PanelControl1.TabIndex = 0
        '
        'SimpleButtonNewClient
        '
        Me.SimpleButtonNewClient.AllowFocus = false
        Me.SimpleButtonNewClient.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButtonNewClient.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButtonNewClient.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonNewClient.Location = New System.Drawing.Point(384, 22)
        Me.SimpleButtonNewClient.Name = "SimpleButtonNewClient"
        Me.SimpleButtonNewClient.Size = New System.Drawing.Size(181, 40)
        Me.SimpleButtonNewClient.TabIndex = 4
        Me.SimpleButtonNewClient.Text = "Add New Client"
        '
        'SimpleButtonArchivedCases
        '
        Me.SimpleButtonArchivedCases.AllowFocus = false
        Me.SimpleButtonArchivedCases.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButtonArchivedCases.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButtonArchivedCases.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonArchivedCases.Location = New System.Drawing.Point(1026, 22)
        Me.SimpleButtonArchivedCases.Name = "SimpleButtonArchivedCases"
        Me.SimpleButtonArchivedCases.Size = New System.Drawing.Size(208, 40)
        Me.SimpleButtonArchivedCases.TabIndex = 3
        Me.SimpleButtonArchivedCases.Text = "Show Archived Cases"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.AllowFocus = false
        Me.SimpleButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButton2.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton2.Location = New System.Drawing.Point(564, 22)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(216, 40)
        Me.SimpleButton2.TabIndex = 2
        Me.SimpleButton2.Text = "Check For New Cases"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.AllowFocus = false
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton1.Location = New System.Drawing.Point(779, 22)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(248, 40)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Show Old Submitted Cases"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Arial", 18!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.Options.UseFont = true
        Me.LabelControl1.Location = New System.Drawing.Point(29, 24)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(162, 29)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Current Cases"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(13, 130)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemButtonEditEdit, Me.RepositoryItemButtonEditDelete, Me.RepositoryItemButtonEditArchive})
        Me.GridControl1.Size = New System.Drawing.Size(1237, 412)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumnAppointment, Me.GridColumnClientName, Me.GridColumnAddress, Me.GridColumnPostCode})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = false
        Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumnAppointment
        '
        Me.GridColumnAppointment.Caption = "Appointment"
        Me.GridColumnAppointment.Name = "GridColumnAppointment"
        Me.GridColumnAppointment.Visible = true
        Me.GridColumnAppointment.VisibleIndex = 0
        Me.GridColumnAppointment.Width = 230
        '
        'GridColumnClientName
        '
        Me.GridColumnClientName.Caption = "Client Name"
        Me.GridColumnClientName.Name = "GridColumnClientName"
        Me.GridColumnClientName.Visible = true
        Me.GridColumnClientName.VisibleIndex = 1
        Me.GridColumnClientName.Width = 331
        '
        'GridColumnAddress
        '
        Me.GridColumnAddress.Caption = "Address"
        Me.GridColumnAddress.Name = "GridColumnAddress"
        Me.GridColumnAddress.Visible = true
        Me.GridColumnAddress.VisibleIndex = 2
        Me.GridColumnAddress.Width = 399
        '
        'GridColumnPostCode
        '
        Me.GridColumnPostCode.Caption = "Post Code"
        Me.GridColumnPostCode.Name = "GridColumnPostCode"
        Me.GridColumnPostCode.Visible = true
        Me.GridColumnPostCode.VisibleIndex = 3
        Me.GridColumnPostCode.Width = 242
        '
        'RepositoryItemButtonEditEdit
        '
        Me.RepositoryItemButtonEditEdit.AutoHeight = false
        EditorButtonImageOptions1.SvgImage = CType(resources.GetObject("EditorButtonImageOptions1.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.RepositoryItemButtonEditEdit.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, EditorButtonImageOptions1, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, SerializableAppearanceObject2, SerializableAppearanceObject3, SerializableAppearanceObject4, "", Nothing, Nothing, DevExpress.Utils.ToolTipAnchor.[Default])})
        Me.RepositoryItemButtonEditEdit.LookAndFeel.SkinName = "The Bezier"
        Me.RepositoryItemButtonEditEdit.LookAndFeel.UseDefaultLookAndFeel = false
        Me.RepositoryItemButtonEditEdit.Name = "RepositoryItemButtonEditEdit"
        Me.RepositoryItemButtonEditEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'RepositoryItemButtonEditDelete
        '
        Me.RepositoryItemButtonEditDelete.AutoHeight = false
        EditorButtonImageOptions2.SvgImage = CType(resources.GetObject("EditorButtonImageOptions2.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.RepositoryItemButtonEditDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, EditorButtonImageOptions2, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject5, SerializableAppearanceObject6, SerializableAppearanceObject7, SerializableAppearanceObject8, "", Nothing, Nothing, DevExpress.Utils.ToolTipAnchor.[Default])})
        Me.RepositoryItemButtonEditDelete.LookAndFeel.SkinName = "The Bezier"
        Me.RepositoryItemButtonEditDelete.LookAndFeel.UseDefaultLookAndFeel = false
        Me.RepositoryItemButtonEditDelete.Name = "RepositoryItemButtonEditDelete"
        Me.RepositoryItemButtonEditDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'RepositoryItemButtonEditArchive
        '
        Me.RepositoryItemButtonEditArchive.AutoHeight = false
        EditorButtonImageOptions3.SvgImage = CType(resources.GetObject("EditorButtonImageOptions3.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.RepositoryItemButtonEditArchive.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, EditorButtonImageOptions3, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject9, SerializableAppearanceObject10, SerializableAppearanceObject11, SerializableAppearanceObject12, "", Nothing, Nothing, DevExpress.Utils.ToolTipAnchor.[Default])})
        Me.RepositoryItemButtonEditArchive.LookAndFeel.SkinName = "The Bezier"
        Me.RepositoryItemButtonEditArchive.LookAndFeel.UseDefaultLookAndFeel = false
        Me.RepositoryItemButtonEditArchive.Name = "RepositoryItemButtonEditArchive"
        Me.RepositoryItemButtonEditArchive.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'SimpleButton5
        '
        Me.SimpleButton5.AllowFocus = false
        Me.SimpleButton5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButton5.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton5.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton5.Location = New System.Drawing.Point(735, 88)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(117, 36)
        Me.SimpleButton5.TabIndex = 2
        Me.SimpleButton5.Text = "Edit"
        '
        'SimpleButton6
        '
        Me.SimpleButton6.AllowFocus = false
        Me.SimpleButton6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButton6.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton6.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton6.Location = New System.Drawing.Point(851, 88)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(117, 36)
        Me.SimpleButton6.TabIndex = 3
        Me.SimpleButton6.Text = "Delete"
        '
        'SimpleButton7
        '
        Me.SimpleButton7.AllowFocus = false
        Me.SimpleButton7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButton7.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton7.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton7.Location = New System.Drawing.Point(967, 88)
        Me.SimpleButton7.Name = "SimpleButton7"
        Me.SimpleButton7.Size = New System.Drawing.Size(127, 36)
        Me.SimpleButton7.TabIndex = 4
        Me.SimpleButton7.Text = "Archive"
        '
        'SimpleButton8
        '
        Me.SimpleButton8.AllowFocus = false
        Me.SimpleButton8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.SimpleButton8.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton8.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton8.Location = New System.Drawing.Point(1093, 88)
        Me.SimpleButton8.Name = "SimpleButton8"
        Me.SimpleButton8.Size = New System.Drawing.Size(157, 36)
        Me.SimpleButton8.TabIndex = 5
        Me.SimpleButton8.Text = "Synchronize"
        '
        'xClientList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1262, 554)
        Me.Controls.Add(Me.SimpleButton8)
        Me.Controls.Add(Me.SimpleButton7)
        Me.Controls.Add(Me.SimpleButton6)
        Me.Controls.Add(Me.SimpleButton5)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "xClientList"
        Me.Text = "Current Cases"
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl1.ResumeLayout(false)
        Me.PanelControl1.PerformLayout
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemButtonEditEdit,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemButtonEditDelete,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.RepositoryItemButtonEditArchive,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonNewClient As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonArchivedCases As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumnAppointment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnClientName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumnPostCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemButtonEditEdit As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents RepositoryItemButtonEditDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents RepositoryItemButtonEditArchive As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton8 As DevExpress.XtraEditors.SimpleButton
End Class
