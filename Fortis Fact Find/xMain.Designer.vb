﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xMain
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TileItemElement16 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xMain))
        Dim TileItemElement17 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement18 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement19 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement20 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Me.TileControl1 = New DevExpress.XtraEditors.TileControl()
        Me.TileGroup5 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItemCurrentCases = New DevExpress.XtraEditors.TileItem()
        Me.TileItemSettings = New DevExpress.XtraEditors.TileItem()
        Me.TileItemSynchronize = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup7 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItemTechSupport = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup3 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItemQuit = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup2 = New DevExpress.XtraEditors.TileGroup()
        Me.FileSystemWatcher1 = New System.IO.FileSystemWatcher()
        Me.SplashScreenManager1 = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, GetType(Global.Fortis_Fact_Find.WaitForm1), true, true)
        CType(Me.FileSystemWatcher1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'TileControl1
        '
        Me.TileControl1.AppearanceText.ForeColor = System.Drawing.Color.White
        Me.TileControl1.AppearanceText.Options.UseForeColor = true
        Me.TileControl1.AppearanceText.Options.UseTextOptions = true
        Me.TileControl1.AppearanceText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.TileControl1.BackColor = System.Drawing.Color.DimGray
        Me.TileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TileControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileControl1.Groups.Add(Me.TileGroup5)
        Me.TileControl1.Groups.Add(Me.TileGroup7)
        Me.TileControl1.Groups.Add(Me.TileGroup3)
        Me.TileControl1.Location = New System.Drawing.Point(0, 0)
        Me.TileControl1.MaxId = 7
        Me.TileControl1.Name = "TileControl1"
        Me.TileControl1.ShowText = true
        Me.TileControl1.Size = New System.Drawing.Size(1153, 528)
        Me.TileControl1.TabIndex = 0
        Me.TileControl1.Text = "Fortis Law Fact Find"
        '
        'TileGroup5
        '
        Me.TileGroup5.Items.Add(Me.TileItemCurrentCases)
        Me.TileGroup5.Items.Add(Me.TileItemSettings)
        Me.TileGroup5.Items.Add(Me.TileItemSynchronize)
        Me.TileGroup5.Name = "TileGroup5"
        '
        'TileItemCurrentCases
        '
        Me.TileItemCurrentCases.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(CType(CType(48,Byte),Integer), CType(CType(51,Byte),Integer), CType(CType(57,Byte),Integer))
        Me.TileItemCurrentCases.AppearanceItem.Normal.Options.UseBackColor = true
        Me.TileItemCurrentCases.BorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never
        TileItemElement16.ImageOptions.Image = CType(resources.GetObject("resource.Image"),System.Drawing.Image)
        TileItemElement16.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Right
        TileItemElement16.Text = "Current Cases"
        Me.TileItemCurrentCases.Elements.Add(TileItemElement16)
        Me.TileItemCurrentCases.Id = 2
        Me.TileItemCurrentCases.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemCurrentCases.Name = "TileItemCurrentCases"
        '
        'TileItemSettings
        '
        Me.TileItemSettings.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(CType(CType(48,Byte),Integer), CType(CType(51,Byte),Integer), CType(CType(57,Byte),Integer))
        Me.TileItemSettings.AppearanceItem.Normal.Options.UseBackColor = true
        Me.TileItemSettings.BorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never
        TileItemElement17.ImageOptions.Image = CType(resources.GetObject("resource.Image1"),System.Drawing.Image)
        TileItemElement17.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement17.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Bottom
        TileItemElement17.Text = "Settings"
        Me.TileItemSettings.Elements.Add(TileItemElement17)
        Me.TileItemSettings.Id = 4
        Me.TileItemSettings.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItemSettings.Name = "TileItemSettings"
        '
        'TileItemSynchronize
        '
        Me.TileItemSynchronize.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(CType(CType(48,Byte),Integer), CType(CType(51,Byte),Integer), CType(CType(57,Byte),Integer))
        Me.TileItemSynchronize.AppearanceItem.Normal.Options.UseBackColor = true
        Me.TileItemSynchronize.BorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never
        TileItemElement18.ImageOptions.Image = CType(resources.GetObject("resource.Image2"),System.Drawing.Image)
        TileItemElement18.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement18.Text = "Synchronize"
        Me.TileItemSynchronize.Elements.Add(TileItemElement18)
        Me.TileItemSynchronize.Id = 3
        Me.TileItemSynchronize.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItemSynchronize.Name = "TileItemSynchronize"
        '
        'TileGroup7
        '
        Me.TileGroup7.Items.Add(Me.TileItemTechSupport)
        Me.TileGroup7.Name = "TileGroup7"
        '
        'TileItemTechSupport
        '
        Me.TileItemTechSupport.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(CType(CType(48,Byte),Integer), CType(CType(51,Byte),Integer), CType(CType(57,Byte),Integer))
        Me.TileItemTechSupport.AppearanceItem.Normal.BorderColor = System.Drawing.Color.White
        Me.TileItemTechSupport.AppearanceItem.Normal.Options.UseBackColor = true
        Me.TileItemTechSupport.AppearanceItem.Normal.Options.UseBorderColor = true
        Me.TileItemTechSupport.BorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never
        TileItemElement19.Appearance.Normal.BackColor = System.Drawing.Color.FromArgb(CType(CType(48,Byte),Integer), CType(CType(51,Byte),Integer), CType(CType(57,Byte),Integer))
        TileItemElement19.Appearance.Normal.Options.UseBackColor = true
        TileItemElement19.ImageOptions.Image = CType(resources.GetObject("resource.Image3"),System.Drawing.Image)
        TileItemElement19.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left
        TileItemElement19.Text = "Technical Support"
        Me.TileItemTechSupport.Elements.Add(TileItemElement19)
        Me.TileItemTechSupport.Id = 5
        Me.TileItemTechSupport.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItemTechSupport.Name = "TileItemTechSupport"
        '
        'TileGroup3
        '
        Me.TileGroup3.Items.Add(Me.TileItemQuit)
        Me.TileGroup3.Name = "TileGroup3"
        '
        'TileItemQuit
        '
        Me.TileItemQuit.AppearanceItem.Normal.BackColor = System.Drawing.Color.FromArgb(CType(CType(48,Byte),Integer), CType(CType(51,Byte),Integer), CType(CType(57,Byte),Integer))
        Me.TileItemQuit.AppearanceItem.Normal.Options.UseBackColor = true
        Me.TileItemQuit.BorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never
        TileItemElement20.ImageOptions.Image = CType(resources.GetObject("resource.Image4"),System.Drawing.Image)
        TileItemElement20.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter
        TileItemElement20.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top
        TileItemElement20.Text = "Quit"
        Me.TileItemQuit.Elements.Add(TileItemElement20)
        Me.TileItemQuit.Id = 6
        Me.TileItemQuit.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItemQuit.Name = "TileItemQuit"
        '
        'TileGroup2
        '
        Me.TileGroup2.Name = "TileGroup2"
        '
        'FileSystemWatcher1
        '
        Me.FileSystemWatcher1.EnableRaisingEvents = true
        Me.FileSystemWatcher1.SynchronizingObject = Me
        '
        'SplashScreenManager1
        '
        Me.SplashScreenManager1.ClosingDelay = 500
        '
        'xMain
        '
        Me.Appearance.Options.UseFont = true
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1153, 528)
        Me.Controls.Add(Me.TileControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "xMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Fortis Law Fact Find v1.0.0.a"
        CType(Me.FileSystemWatcher1,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents TileControl1 As DevExpress.XtraEditors.TileControl
    Friend WithEvents TileGroup5 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItemCurrentCases As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItemSettings As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItemSynchronize As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup7 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItemTechSupport As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItemQuit As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup2 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileGroup3 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents FileSystemWatcher1 As IO.FileSystemWatcher
    Friend WithEvents SplashScreenManager1 As DevExpress.XtraSplashScreen.SplashScreenManager
End Class
