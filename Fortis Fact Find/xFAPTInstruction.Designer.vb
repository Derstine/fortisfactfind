﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class xFAPTInstruction
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(xFAPTInstruction))
        Dim CustomHeaderButtonImageOptions7 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject7 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions8 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject8 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions11 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject11 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions12 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject12 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions9 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject9 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim CustomHeaderButtonImageOptions10 As DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions = New DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions()
        Dim SerializableAppearanceObject10 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.NavigationPane1 = New DevExpress.XtraBars.Navigation.NavigationPane()
        Me.NavigationPageSettlorDetails = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.NavigationPageTrusteeInformation = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.NavigationPageBeneficiaries = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.SimpleButtonLookup = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit7 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPageTrustFund = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl6 = New DevExpress.XtraGrid.GridControl()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl7 = New DevExpress.XtraGrid.GridControl()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.NavigationPagePurposeofFund = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit8 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit11 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit12 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit13 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit14 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit15 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit16 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit17 = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.SimpleLabelItem1 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.NavigationPageConsultantCompliance = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.NavigationPageAttendanceNotes = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit3 = New DevExpress.XtraEditors.MemoEdit()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPane1.SuspendLayout
        Me.NavigationPageSettlorDetails.SuspendLayout
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl1.SuspendLayout
        CType(Me.GroupControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl2.SuspendLayout
        CType(Me.LayoutControl3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl3.SuspendLayout
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GroupControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupControl1.SuspendLayout
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl2.SuspendLayout
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.DateEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageTrusteeInformation.SuspendLayout
        CType(Me.LayoutControl4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl4.SuspendLayout
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit4.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageBeneficiaries.SuspendLayout
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit7.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit6.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl2,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView2,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageTrustFund.SuspendLayout
        CType(Me.XtraTabControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabControl1.SuspendLayout
        Me.XtraTabPage1.SuspendLayout
        CType(Me.GridControl3,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView3,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage2.SuspendLayout
        CType(Me.GridControl6,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView6,System.ComponentModel.ISupportInitialize).BeginInit
        Me.XtraTabPage3.SuspendLayout
        CType(Me.GridControl7,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView7,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPagePurposeofFund.SuspendLayout
        CType(Me.LayoutControl5,System.ComponentModel.ISupportInitialize).BeginInit
        Me.LayoutControl5.SuspendLayout
        CType(Me.CheckEdit8.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit10.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit13.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit14.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit15.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit16.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.CheckEdit17.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlGroup4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageConsultantCompliance.SuspendLayout
        CType(Me.GridControl4,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView4,System.ComponentModel.ISupportInitialize).BeginInit
        Me.NavigationPageAttendanceNotes.SuspendLayout
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelControl1.SuspendLayout
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridControl5,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.GridView5,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'NavigationPane1
        '
        Me.NavigationPane1.Controls.Add(Me.NavigationPageSettlorDetails)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageTrusteeInformation)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageBeneficiaries)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageTrustFund)
        Me.NavigationPane1.Controls.Add(Me.NavigationPagePurposeofFund)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageConsultantCompliance)
        Me.NavigationPane1.Controls.Add(Me.NavigationPageAttendanceNotes)
        Me.NavigationPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPane1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText
        Me.NavigationPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPageSettlorDetails, Me.NavigationPageTrusteeInformation, Me.NavigationPageBeneficiaries, Me.NavigationPageTrustFund, Me.NavigationPagePurposeofFund, Me.NavigationPageConsultantCompliance, Me.NavigationPageAttendanceNotes})
        Me.NavigationPane1.RegularSize = New System.Drawing.Size(1240, 662)
        Me.NavigationPane1.SelectedPage = Me.NavigationPageTrusteeInformation
        Me.NavigationPane1.Size = New System.Drawing.Size(1240, 662)
        Me.NavigationPane1.TabIndex = 0
        Me.NavigationPane1.Text = "NavigationPane1"
        '
        'NavigationPageSettlorDetails
        '
        Me.NavigationPageSettlorDetails.Caption = "Settlor Details"
        Me.NavigationPageSettlorDetails.Controls.Add(Me.LayoutControl1)
        Me.NavigationPageSettlorDetails.ImageOptions.Image = CType(resources.GetObject("NavigationPageSettlorDetails.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageSettlorDetails.Name = "NavigationPageSettlorDetails"
        Me.NavigationPageSettlorDetails.Size = New System.Drawing.Size(1033, 602)
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.GroupControl2)
        Me.LayoutControl1.Controls.Add(Me.GroupControl1)
        Me.LayoutControl1.Location = New System.Drawing.Point(17, 20)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(1002, 571)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.LayoutControl3)
        Me.GroupControl2.Location = New System.Drawing.Point(522, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(468, 547)
        Me.GroupControl2.TabIndex = 0
        Me.GroupControl2.Text = "Settlor 2"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.TextEdit3)
        Me.LayoutControl3.Controls.Add(Me.DateEdit2)
        Me.LayoutControl3.Controls.Add(Me.MemoEdit2)
        Me.LayoutControl3.Controls.Add(Me.TextEdit4)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup2
        Me.LayoutControl3.Size = New System.Drawing.Size(464, 524)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(117, 40)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Size = New System.Drawing.Size(335, 38)
        Me.TextEdit3.StyleController = Me.LayoutControl3
        Me.TextEdit3.TabIndex = 4
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(117, 82)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(335, 38)
        Me.DateEdit2.StyleController = Me.LayoutControl3
        Me.DateEdit2.TabIndex = 5
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(117, 124)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Size = New System.Drawing.Size(335, 192)
        Me.MemoEdit2.StyleController = Me.LayoutControl3
        Me.MemoEdit2.TabIndex = 6
        '
        'TextEdit4
        '
        Me.TextEdit4.Location = New System.Drawing.Point(120, 320)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Size = New System.Drawing.Size(332, 38)
        Me.TextEdit4.StyleController = Me.LayoutControl3
        Me.TextEdit4.TabIndex = 7
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = false
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem5, Me.EmptySpaceItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.EmptySpaceItem7})
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(464, 524)
        Me.LayoutControlGroup2.TextVisible = false
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = false
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 350)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(444, 154)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = false
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(444, 28)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.TextEdit3
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 28)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(444, 42)
        Me.LayoutControlItem7.Text = "Name"
        Me.LayoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem7.TextToControlDistance = 5
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.DateEdit2
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 70)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(444, 42)
        Me.LayoutControlItem8.Text = "Date of Birth"
        Me.LayoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem8.TextToControlDistance = 5
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem9.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem9.Control = Me.MemoEdit2
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 112)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(444, 196)
        Me.LayoutControlItem9.Text = "Address"
        Me.LayoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem9.TextToControlDistance = 5
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.TextEdit4
        Me.LayoutControlItem10.Location = New System.Drawing.Point(108, 308)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(336, 42)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = false
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = false
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(0, 308)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(108, 42)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LayoutControl2)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(439, 547)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Settlor 1"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.TextEdit1)
        Me.LayoutControl2.Controls.Add(Me.DateEdit1)
        Me.LayoutControl2.Controls.Add(Me.MemoEdit1)
        Me.LayoutControl2.Controls.Add(Me.TextEdit2)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(2, 21)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(435, 524)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(117, 40)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(306, 38)
        Me.TextEdit1.StyleController = Me.LayoutControl2
        Me.TextEdit1.TabIndex = 4
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(117, 82)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(306, 38)
        Me.DateEdit1.StyleController = Me.LayoutControl2
        Me.DateEdit1.TabIndex = 5
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(117, 124)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(306, 192)
        Me.MemoEdit1.StyleController = Me.LayoutControl2
        Me.MemoEdit1.TabIndex = 6
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(119, 320)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(304, 38)
        Me.TextEdit2.StyleController = Me.LayoutControl2
        Me.TextEdit2.TabIndex = 7
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = false
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.EmptySpaceItem4})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(435, 524)
        Me.LayoutControlGroup1.TextVisible = false
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = false
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 350)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(415, 154)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = false
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(415, 28)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.TextEdit1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 28)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(415, 42)
        Me.LayoutControlItem3.Text = "Name"
        Me.LayoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem3.TextToControlDistance = 5
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.DateEdit1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 70)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(415, 42)
        Me.LayoutControlItem4.Text = "Date of Birth"
        Me.LayoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem4.TextToControlDistance = 5
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true
        Me.LayoutControlItem5.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.LayoutControlItem5.Control = Me.MemoEdit1
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 112)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(415, 196)
        Me.LayoutControlItem5.Text = "Address"
        Me.LayoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(100, 20)
        Me.LayoutControlItem5.TextToControlDistance = 5
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.TextEdit2
        Me.LayoutControlItem6.Location = New System.Drawing.Point(107, 308)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(308, 42)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = false
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = false
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 308)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(107, 42)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = false
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.EmptySpaceItem1})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1002, 571)
        Me.Root.TextVisible = false
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GroupControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(443, 551)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = false
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.GroupControl2
        Me.LayoutControlItem2.Location = New System.Drawing.Point(510, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(472, 551)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = false
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = false
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(443, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(67, 551)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'NavigationPageTrusteeInformation
        '
        Me.NavigationPageTrusteeInformation.Caption = "Trustee Information"
        Me.NavigationPageTrusteeInformation.Controls.Add(Me.LayoutControl4)
        CustomHeaderButtonImageOptions7.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions7.Image"),System.Drawing.Image)
        CustomHeaderButtonImageOptions8.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions8.Image"),System.Drawing.Image)
        Me.NavigationPageTrusteeInformation.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add a Trustee", true, CustomHeaderButtonImageOptions7, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject7, Nothing, -1), New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Children", true, CustomHeaderButtonImageOptions8, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject8, Nothing, -1)})
        Me.NavigationPageTrusteeInformation.ImageOptions.Image = CType(resources.GetObject("NavigationPageTrusteeInformation.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageTrusteeInformation.Name = "NavigationPageTrusteeInformation"
        Me.NavigationPageTrusteeInformation.Size = New System.Drawing.Size(1033, 597)
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.GridControl1)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit1)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit2)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit3)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit4)
        Me.LayoutControl4.Controls.Add(Me.CheckEdit5)
        Me.LayoutControl4.Location = New System.Drawing.Point(14, 10)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup3
        Me.LayoutControl4.Size = New System.Drawing.Size(1010, 577)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(12, 60)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(986, 505)
        Me.GridControl1.TabIndex = 9
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = false
        Me.GridView1.OptionsView.ShowIndicator = false
        Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Trustee Name"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = true
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 379
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Relationship to Client 1"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = true
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 259
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Relationship to Client 2"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = true
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 258
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Action"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.FixedWidth = true
        Me.GridColumn4.Visible = true
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 100
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(166, 12)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "NONE"
        Me.CheckEdit1.Size = New System.Drawing.Size(94, 44)
        Me.CheckEdit1.StyleController = Me.LayoutControl4
        Me.CheckEdit1.TabIndex = 4
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(264, 12)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Caption = "SETTLOR(S) ONLY"
        Me.CheckEdit2.Size = New System.Drawing.Size(169, 44)
        Me.CheckEdit2.StyleController = Me.LayoutControl4
        Me.CheckEdit2.TabIndex = 5
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(437, 12)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Caption = "OUR COMPANY"
        Me.CheckEdit3.Size = New System.Drawing.Size(151, 44)
        Me.CheckEdit3.StyleController = Me.LayoutControl4
        Me.CheckEdit3.TabIndex = 6
        '
        'CheckEdit4
        '
        Me.CheckEdit4.Location = New System.Drawing.Point(592, 12)
        Me.CheckEdit4.Name = "CheckEdit4"
        Me.CheckEdit4.Properties.Caption = "SETTLOR(S) WITH OTHERS"
        Me.CheckEdit4.Size = New System.Drawing.Size(215, 44)
        Me.CheckEdit4.StyleController = Me.LayoutControl4
        Me.CheckEdit4.TabIndex = 7
        '
        'CheckEdit5
        '
        Me.CheckEdit5.Location = New System.Drawing.Point(811, 12)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Caption = "NAMED INDIVIDUALS"
        Me.CheckEdit5.Size = New System.Drawing.Size(187, 44)
        Me.CheckEdit5.StyleController = Me.LayoutControl4
        Me.CheckEdit5.TabIndex = 8
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = false
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16})
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(1010, 577)
        Me.LayoutControlGroup3.TextVisible = false
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.CheckEdit1
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(252, 48)
        Me.LayoutControlItem11.Text = "Type of Trustees Required"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(151, 16)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.CheckEdit2
        Me.LayoutControlItem12.Location = New System.Drawing.Point(252, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(173, 48)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = false
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.CheckEdit3
        Me.LayoutControlItem13.Location = New System.Drawing.Point(425, 0)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(155, 48)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = false
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.CheckEdit4
        Me.LayoutControlItem14.Location = New System.Drawing.Point(580, 0)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(219, 48)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = false
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.CheckEdit5
        Me.LayoutControlItem15.Location = New System.Drawing.Point(799, 0)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(191, 48)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = false
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.GridControl1
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(990, 509)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = false
        '
        'NavigationPageBeneficiaries
        '
        Me.NavigationPageBeneficiaries.Caption = "Primary Distribution Beneficiaries"
        Me.NavigationPageBeneficiaries.Controls.Add(Me.SimpleButtonLookup)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.TextEdit7)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.TextEdit6)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.TextEdit5)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.CheckEdit7)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.LabelControl3)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.CheckEdit6)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.LabelControl2)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.LabelControl1)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.GridControl2)
        Me.NavigationPageBeneficiaries.Controls.Add(Me.NavigationPage1)
        CustomHeaderButtonImageOptions11.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions11.Image"),System.Drawing.Image)
        CustomHeaderButtonImageOptions12.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions12.Image"),System.Drawing.Image)
        Me.NavigationPageBeneficiaries.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Primary Beneficiary", true, CustomHeaderButtonImageOptions11, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject11, Nothing, -1), New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Children", true, CustomHeaderButtonImageOptions12, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject12, Nothing, -1)})
        Me.NavigationPageBeneficiaries.ImageOptions.Image = CType(resources.GetObject("NavigationPageBeneficiaries.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageBeneficiaries.Name = "NavigationPageBeneficiaries"
        Me.NavigationPageBeneficiaries.PageText = "Beneficiaries"
        Me.NavigationPageBeneficiaries.Size = New System.Drawing.Size(1033, 597)
        '
        'SimpleButtonLookup
        '
        Me.SimpleButtonLookup.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton1.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButtonLookup.Location = New System.Drawing.Point(875, 431)
        Me.SimpleButtonLookup.Name = "SimpleButtonLookup"
        Me.SimpleButtonLookup.Size = New System.Drawing.Size(140, 38)
        Me.SimpleButtonLookup.TabIndex = 6
        Me.SimpleButtonLookup.Text = "Lookup"
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(663, 475)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Size = New System.Drawing.Size(206, 38)
        Me.TextEdit7.TabIndex = 5
        '
        'TextEdit6
        '
        Me.TextEdit6.Location = New System.Drawing.Point(663, 431)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Size = New System.Drawing.Size(206, 38)
        Me.TextEdit6.TabIndex = 5
        '
        'TextEdit5
        '
        Me.TextEdit5.Location = New System.Drawing.Point(26, 431)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Size = New System.Drawing.Size(470, 38)
        Me.TextEdit5.TabIndex = 5
        '
        'CheckEdit7
        '
        Me.CheckEdit7.Location = New System.Drawing.Point(645, 377)
        Me.CheckEdit7.Name = "CheckEdit7"
        Me.CheckEdit7.Properties.Caption = "NO"
        Me.CheckEdit7.Size = New System.Drawing.Size(91, 44)
        Me.CheckEdit7.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl3.Appearance.Options.UseFont = true
        Me.LabelControl3.Location = New System.Drawing.Point(559, 486)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(100, 16)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Charity Number"
        '
        'CheckEdit6
        '
        Me.CheckEdit6.Location = New System.Drawing.Point(548, 377)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Caption = "YES"
        Me.CheckEdit6.Size = New System.Drawing.Size(91, 44)
        Me.CheckEdit6.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl2.Appearance.Options.UseFont = true
        Me.LabelControl2.Location = New System.Drawing.Point(559, 442)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 16)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Charity Name"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Arial", 10!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.Options.UseFont = true
        Me.LabelControl1.Location = New System.Drawing.Point(25, 391)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(471, 16)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Does the Settlor(s) wish to state a specific charity as an ultimate backstop?"
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(25, 13)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(990, 358)
        Me.GridControl2.TabIndex = 2
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = false
        Me.GridView2.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Beneficiary Name"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = true
        Me.GridColumn5.VisibleIndex = 0
        Me.GridColumn5.Width = 326
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Relationship to Client 1"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = true
        Me.GridColumn6.VisibleIndex = 1
        Me.GridColumn6.Width = 247
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Relationship to Client 2"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = true
        Me.GridColumn7.VisibleIndex = 2
        Me.GridColumn7.Width = 229
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Lapse/Issue"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = true
        Me.GridColumn8.VisibleIndex = 3
        Me.GridColumn8.Width = 118
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Action"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.OptionsColumn.FixedWidth = true
        Me.GridColumn9.Visible = true
        Me.GridColumn9.VisibleIndex = 4
        Me.GridColumn9.Width = 100
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Caption = "Beneficiaries"
        CustomHeaderButtonImageOptions9.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions9.Image"),System.Drawing.Image)
        CustomHeaderButtonImageOptions10.Image = CType(resources.GetObject("CustomHeaderButtonImageOptions10.Image"),System.Drawing.Image)
        Me.NavigationPage1.CustomHeaderButtons.AddRange(New DevExpress.XtraBars.Docking2010.IButton() {New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Primary Beneficiary", true, CustomHeaderButtonImageOptions9, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject9, Nothing, -1), New DevExpress.XtraBars.Docking.CustomHeaderButton("Add Children", true, CustomHeaderButtonImageOptions10, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, Nothing, true, false, true, SerializableAppearanceObject10, Nothing, -1)})
        Me.NavigationPage1.ImageOptions.Image = CType(resources.GetObject("NavigationPage1.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.PageText = ""
        Me.NavigationPage1.Size = New System.Drawing.Size(1041, 587)
        '
        'NavigationPageTrustFund
        '
        Me.NavigationPageTrustFund.Caption = "Define the Trust Fund"
        Me.NavigationPageTrustFund.Controls.Add(Me.XtraTabControl1)
        Me.NavigationPageTrustFund.ImageOptions.Image = CType(resources.GetObject("NavigationPageTrustFund.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageTrustFund.Name = "NavigationPageTrustFund"
        Me.NavigationPageTrustFund.PageText = "Trust Fund"
        Me.NavigationPageTrustFund.Size = New System.Drawing.Size(1033, 602)
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(1033, 602)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.SimpleButton2)
        Me.XtraTabPage1.Controls.Add(Me.GridControl3)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1031, 566)
        Me.XtraTabPage1.Text = "Property"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton2.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton2.Location = New System.Drawing.Point(17, 16)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(180, 33)
        Me.SimpleButton2.TabIndex = 1
        Me.SimpleButton2.Text = "Add Property"
        '
        'GridControl3
        '
        Me.GridControl3.Location = New System.Drawing.Point(17, 55)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(1001, 496)
        Me.GridControl3.TabIndex = 0
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.GridColumn15})
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowGroupPanel = false
        Me.GridView3.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Asset Type"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = true
        Me.GridColumn10.VisibleIndex = 0
        Me.GridColumn10.Width = 169
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Address"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = true
        Me.GridColumn11.VisibleIndex = 1
        Me.GridColumn11.Width = 169
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Value"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = true
        Me.GridColumn12.VisibleIndex = 2
        Me.GridColumn12.Width = 169
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Mortgage"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = true
        Me.GridColumn13.VisibleIndex = 3
        Me.GridColumn13.Width = 169
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Include?"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = true
        Me.GridColumn14.VisibleIndex = 4
        Me.GridColumn14.Width = 169
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Action"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.OptionsColumn.FixedWidth = true
        Me.GridColumn15.Visible = true
        Me.GridColumn15.VisibleIndex = 5
        Me.GridColumn15.Width = 100
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.GridControl6)
        Me.XtraTabPage2.Controls.Add(Me.SimpleButton3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1031, 566)
        Me.XtraTabPage2.Text = "Specific Items"
        '
        'GridControl6
        '
        Me.GridControl6.Location = New System.Drawing.Point(17, 55)
        Me.GridControl6.MainView = Me.GridView6
        Me.GridControl6.Name = "GridControl6"
        Me.GridControl6.Size = New System.Drawing.Size(1001, 496)
        Me.GridControl6.TabIndex = 3
        Me.GridControl6.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView6})
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn22, Me.GridColumn23, Me.GridColumn24, Me.GridColumn26, Me.GridColumn27})
        Me.GridView6.GridControl = Me.GridControl6
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsView.ShowGroupPanel = false
        Me.GridView6.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn22
        '
        Me.GridColumn22.Caption = "Asset Type"
        Me.GridColumn22.Name = "GridColumn22"
        Me.GridColumn22.Visible = true
        Me.GridColumn22.VisibleIndex = 0
        Me.GridColumn22.Width = 169
        '
        'GridColumn23
        '
        Me.GridColumn23.Caption = "Description"
        Me.GridColumn23.Name = "GridColumn23"
        Me.GridColumn23.Visible = true
        Me.GridColumn23.VisibleIndex = 1
        Me.GridColumn23.Width = 169
        '
        'GridColumn24
        '
        Me.GridColumn24.Caption = "Value"
        Me.GridColumn24.Name = "GridColumn24"
        Me.GridColumn24.Visible = true
        Me.GridColumn24.VisibleIndex = 2
        Me.GridColumn24.Width = 169
        '
        'GridColumn26
        '
        Me.GridColumn26.Caption = "Include?"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = true
        Me.GridColumn26.VisibleIndex = 3
        Me.GridColumn26.Width = 169
        '
        'GridColumn27
        '
        Me.GridColumn27.Caption = "Action"
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.OptionsColumn.FixedWidth = true
        Me.GridColumn27.Visible = true
        Me.GridColumn27.VisibleIndex = 4
        Me.GridColumn27.Width = 100
        '
        'SimpleButton3
        '
        Me.SimpleButton3.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton3.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton3.Location = New System.Drawing.Point(17, 16)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(180, 33)
        Me.SimpleButton3.TabIndex = 2
        Me.SimpleButton3.Text = "Add Item"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.GridControl7)
        Me.XtraTabPage3.Controls.Add(Me.SimpleButton4)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(1031, 566)
        Me.XtraTabPage3.Text = "Cash"
        '
        'GridControl7
        '
        Me.GridControl7.Location = New System.Drawing.Point(17, 55)
        Me.GridControl7.MainView = Me.GridView7
        Me.GridControl7.Name = "GridControl7"
        Me.GridControl7.Size = New System.Drawing.Size(1001, 496)
        Me.GridControl7.TabIndex = 4
        Me.GridControl7.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView7})
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn28, Me.GridColumn29, Me.GridColumn30, Me.GridColumn32, Me.GridColumn33})
        Me.GridView7.GridControl = Me.GridControl7
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsView.ShowGroupPanel = false
        Me.GridView7.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn28
        '
        Me.GridColumn28.Caption = "Asset Type"
        Me.GridColumn28.Name = "GridColumn28"
        Me.GridColumn28.Visible = true
        Me.GridColumn28.VisibleIndex = 0
        Me.GridColumn28.Width = 175
        '
        'GridColumn29
        '
        Me.GridColumn29.Caption = "Description"
        Me.GridColumn29.Name = "GridColumn29"
        Me.GridColumn29.Visible = true
        Me.GridColumn29.VisibleIndex = 1
        Me.GridColumn29.Width = 342
        '
        'GridColumn30
        '
        Me.GridColumn30.Caption = "Value"
        Me.GridColumn30.Name = "GridColumn30"
        Me.GridColumn30.Visible = true
        Me.GridColumn30.VisibleIndex = 2
        Me.GridColumn30.Width = 245
        '
        'GridColumn32
        '
        Me.GridColumn32.Caption = "Include?"
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.Visible = true
        Me.GridColumn32.VisibleIndex = 3
        Me.GridColumn32.Width = 121
        '
        'GridColumn33
        '
        Me.GridColumn33.Caption = "Action"
        Me.GridColumn33.Name = "GridColumn33"
        Me.GridColumn33.OptionsColumn.FixedWidth = true
        Me.GridColumn33.Visible = true
        Me.GridColumn33.VisibleIndex = 4
        Me.GridColumn33.Width = 100
        '
        'SimpleButton4
        '
        Me.SimpleButton4.ImageOptions.SvgImage = CType(resources.GetObject("SimpleButton4.ImageOptions.SvgImage"),DevExpress.Utils.Svg.SvgImage)
        Me.SimpleButton4.Location = New System.Drawing.Point(17, 16)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(180, 33)
        Me.SimpleButton4.TabIndex = 3
        Me.SimpleButton4.Text = "Add Cash"
        '
        'NavigationPagePurposeofFund
        '
        Me.NavigationPagePurposeofFund.Caption = "Purpose of Fund"
        Me.NavigationPagePurposeofFund.Controls.Add(Me.LayoutControl5)
        Me.NavigationPagePurposeofFund.ImageOptions.Image = CType(resources.GetObject("NavigationPagePurposeofFund.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPagePurposeofFund.Name = "NavigationPagePurposeofFund"
        Me.NavigationPagePurposeofFund.Size = New System.Drawing.Size(1033, 602)
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.CheckEdit8)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit9)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit10)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit11)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit12)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit13)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit14)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit15)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit16)
        Me.LayoutControl5.Controls.Add(Me.CheckEdit17)
        Me.LayoutControl5.Location = New System.Drawing.Point(12, 16)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup4
        Me.LayoutControl5.Size = New System.Drawing.Size(1038, 602)
        Me.LayoutControl5.TabIndex = 0
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'CheckEdit8
        '
        Me.CheckEdit8.Location = New System.Drawing.Point(12, 35)
        Me.CheckEdit8.Name = "CheckEdit8"
        Me.CheckEdit8.Properties.Caption = "My/our wishes to be specifically set out during our/my life for the benefit of th"& _ 
    "e family"
        Me.CheckEdit8.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit8.StyleController = Me.LayoutControl5
        Me.CheckEdit8.TabIndex = 4
        '
        'CheckEdit9
        '
        Me.CheckEdit9.Location = New System.Drawing.Point(12, 83)
        Me.CheckEdit9.Name = "CheckEdit9"
        Me.CheckEdit9.Properties.Caption = "I/We would like the Trustees to make sensible decisions for the welfare of the se"& _ 
    "ttlor(s)"
        Me.CheckEdit9.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit9.StyleController = Me.LayoutControl5
        Me.CheckEdit9.TabIndex = 5
        '
        'CheckEdit10
        '
        Me.CheckEdit10.Location = New System.Drawing.Point(12, 131)
        Me.CheckEdit10.Name = "CheckEdit10"
        Me.CheckEdit10.Properties.Caption = "To allow the Trustees to assist in taking proper and responsible steps if the set"& _ 
    "tlor(s) are incapacitated"
        Me.CheckEdit10.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit10.StyleController = Me.LayoutControl5
        Me.CheckEdit10.TabIndex = 6
        '
        'CheckEdit11
        '
        Me.CheckEdit11.Location = New System.Drawing.Point(12, 179)
        Me.CheckEdit11.Name = "CheckEdit11"
        Me.CheckEdit11.Properties.Caption = "To allow the Trustees to manage the property if the Settlor(s) are incapacitated"
        Me.CheckEdit11.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit11.StyleController = Me.LayoutControl5
        Me.CheckEdit11.TabIndex = 7
        '
        'CheckEdit12
        '
        Me.CheckEdit12.Location = New System.Drawing.Point(12, 227)
        Me.CheckEdit12.Name = "CheckEdit12"
        Me.CheckEdit12.Properties.Caption = "To prevent the assets of the Trust falling under the Court of Protection"
        Me.CheckEdit12.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit12.StyleController = Me.LayoutControl5
        Me.CheckEdit12.TabIndex = 8
        '
        'CheckEdit13
        '
        Me.CheckEdit13.Location = New System.Drawing.Point(12, 275)
        Me.CheckEdit13.Name = "CheckEdit13"
        Me.CheckEdit13.Properties.Caption = "To give some protection against sideways dis-inheritance"
        Me.CheckEdit13.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit13.StyleController = Me.LayoutControl5
        Me.CheckEdit13.TabIndex = 9
        '
        'CheckEdit14
        '
        Me.CheckEdit14.Location = New System.Drawing.Point(12, 323)
        Me.CheckEdit14.Name = "CheckEdit14"
        Me.CheckEdit14.Properties.Caption = "To allow the Trustees to have flexibility to loan money from the Trust"
        Me.CheckEdit14.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit14.StyleController = Me.LayoutControl5
        Me.CheckEdit14.TabIndex = 10
        '
        'CheckEdit15
        '
        Me.CheckEdit15.Location = New System.Drawing.Point(12, 371)
        Me.CheckEdit15.Name = "CheckEdit15"
        Me.CheckEdit15.Properties.Caption = "To allow an income to be taken from the funds if the property is sold"
        Me.CheckEdit15.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit15.StyleController = Me.LayoutControl5
        Me.CheckEdit15.TabIndex = 11
        '
        'CheckEdit16
        '
        Me.CheckEdit16.Location = New System.Drawing.Point(12, 419)
        Me.CheckEdit16.Name = "CheckEdit16"
        Me.CheckEdit16.Properties.Caption = "To reduce probate costs upon my/our death"
        Me.CheckEdit16.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit16.StyleController = Me.LayoutControl5
        Me.CheckEdit16.TabIndex = 12
        '
        'CheckEdit17
        '
        Me.CheckEdit17.Location = New System.Drawing.Point(12, 467)
        Me.CheckEdit17.Name = "CheckEdit17"
        Me.CheckEdit17.Properties.Caption = "To prevent your asset from being accessed by creditors"
        Me.CheckEdit17.Size = New System.Drawing.Size(1014, 44)
        Me.CheckEdit17.StyleController = Me.LayoutControl5
        Me.CheckEdit17.TabIndex = 13
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = false
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem8, Me.SimpleLabelItem1, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26})
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(1038, 602)
        Me.LayoutControlGroup4.TextVisible = false
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = false
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(0, 503)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(1018, 79)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'SimpleLabelItem1
        '
        Me.SimpleLabelItem1.AllowHotTrack = false
        Me.SimpleLabelItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold)
        Me.SimpleLabelItem1.AppearanceItemCaption.Options.UseFont = true
        Me.SimpleLabelItem1.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem1.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem1.Size = New System.Drawing.Size(1018, 23)
        Me.SimpleLabelItem1.Text = "Tick which statements are applicable"
        Me.SimpleLabelItem1.TextSize = New System.Drawing.Size(280, 19)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.CheckEdit8
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 23)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = false
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.CheckEdit9
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 71)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = false
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.CheckEdit10
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 119)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = false
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.CheckEdit11
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 167)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = false
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.CheckEdit12
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 215)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = false
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.CheckEdit13
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 263)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = false
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.CheckEdit14
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 311)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = false
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.CheckEdit15
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 359)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = false
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.CheckEdit16
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 407)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = false
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.CheckEdit17
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 455)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(1018, 48)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = false
        '
        'NavigationPageConsultantCompliance
        '
        Me.NavigationPageConsultantCompliance.Caption = "Consultant Compliance"
        Me.NavigationPageConsultantCompliance.Controls.Add(Me.GridControl4)
        Me.NavigationPageConsultantCompliance.ImageOptions.Image = CType(resources.GetObject("NavigationPageConsultantCompliance.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageConsultantCompliance.Name = "NavigationPageConsultantCompliance"
        Me.NavigationPageConsultantCompliance.Size = New System.Drawing.Size(1033, 602)
        '
        'GridControl4
        '
        Me.GridControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl4.Location = New System.Drawing.Point(0, 0)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(1033, 602)
        Me.GridControl4.TabIndex = 0
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn16, Me.GridColumn17, Me.GridColumn18})
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Question"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = true
        Me.GridColumn16.VisibleIndex = 0
        Me.GridColumn16.Width = 533
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "Answer"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = true
        Me.GridColumn17.VisibleIndex = 1
        Me.GridColumn17.Width = 302
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "Action"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = true
        Me.GridColumn18.VisibleIndex = 2
        Me.GridColumn18.Width = 185
        '
        'NavigationPageAttendanceNotes
        '
        Me.NavigationPageAttendanceNotes.Caption = "Attendance Notes"
        Me.NavigationPageAttendanceNotes.Controls.Add(Me.PanelControl1)
        Me.NavigationPageAttendanceNotes.Controls.Add(Me.GridControl5)
        Me.NavigationPageAttendanceNotes.ImageOptions.Image = CType(resources.GetObject("NavigationPageAttendanceNotes.ImageOptions.Image"),System.Drawing.Image)
        Me.NavigationPageAttendanceNotes.Name = "NavigationPageAttendanceNotes"
        Me.NavigationPageAttendanceNotes.Size = New System.Drawing.Size(1033, 602)
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.MemoEdit3)
        Me.PanelControl1.Location = New System.Drawing.Point(653, 21)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(356, 541)
        Me.PanelControl1.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.BackColor = System.Drawing.Color.DimGray
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.LabelControl4.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl4.Appearance.Options.UseBackColor = true
        Me.LabelControl4.Appearance.Options.UseFont = true
        Me.LabelControl4.Appearance.Options.UseForeColor = true
        Me.LabelControl4.Appearance.Options.UseTextOptions = true
        Me.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl4.Location = New System.Drawing.Point(2, 2)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(352, 38)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Supplementary Information"
        '
        'MemoEdit3
        '
        Me.MemoEdit3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit3.Location = New System.Drawing.Point(2, 2)
        Me.MemoEdit3.Name = "MemoEdit3"
        Me.MemoEdit3.Size = New System.Drawing.Size(352, 537)
        Me.MemoEdit3.TabIndex = 3
        '
        'GridControl5
        '
        Me.GridControl5.Location = New System.Drawing.Point(21, 21)
        Me.GridControl5.MainView = Me.GridView5
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(600, 541)
        Me.GridControl5.TabIndex = 1
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn19, Me.GridColumn20, Me.GridColumn21})
        Me.GridView5.GridControl = Me.GridControl5
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsView.ShowGroupPanel = false
        Me.GridView5.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView5.OptionsView.ShowViewCaption = true
        Me.GridView5.ViewCaption = "Attendance Notes"
        '
        'GridColumn19
        '
        Me.GridColumn19.Caption = "Question"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = true
        Me.GridColumn19.VisibleIndex = 0
        Me.GridColumn19.Width = 533
        '
        'GridColumn20
        '
        Me.GridColumn20.Caption = "Answer"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = true
        Me.GridColumn20.VisibleIndex = 1
        Me.GridColumn20.Width = 302
        '
        'GridColumn21
        '
        Me.GridColumn21.Caption = "Action"
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Visible = true
        Me.GridColumn21.VisibleIndex = 2
        Me.GridColumn21.Width = 185
        '
        'xFAPTInstruction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1240, 662)
        Me.Controls.Add(Me.NavigationPane1)
        Me.Name = "xFAPTInstruction"
        Me.Text = "FAPT Instruction"
        CType(Me.NavigationPane1,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPane1.ResumeLayout(false)
        Me.NavigationPageSettlorDetails.ResumeLayout(false)
        CType(Me.LayoutControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl1.ResumeLayout(false)
        CType(Me.GroupControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl2.ResumeLayout(false)
        CType(Me.LayoutControl3,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl3.ResumeLayout(false)
        CType(Me.TextEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit2.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem9,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem10,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GroupControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl1.ResumeLayout(false)
        CType(Me.LayoutControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl2.ResumeLayout(false)
        CType(Me.TextEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit1.Properties.CalendarTimeProperties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.DateEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.MemoEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Root,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem1,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageTrusteeInformation.ResumeLayout(false)
        CType(Me.LayoutControl4,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl4.ResumeLayout(false)
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit1.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit2.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit4.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem11,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem12,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem13,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem14,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem15,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem16,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageBeneficiaries.ResumeLayout(false)
        Me.NavigationPageBeneficiaries.PerformLayout
        CType(Me.TextEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.TextEdit5.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit7.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit6.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView2,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageTrustFund.ResumeLayout(false)
        CType(Me.XtraTabControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl1.ResumeLayout(false)
        Me.XtraTabPage1.ResumeLayout(false)
        CType(Me.GridControl3,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView3,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage2.ResumeLayout(false)
        CType(Me.GridControl6,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView6,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabPage3.ResumeLayout(false)
        CType(Me.GridControl7,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView7,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPagePurposeofFund.ResumeLayout(false)
        CType(Me.LayoutControl5,System.ComponentModel.ISupportInitialize).EndInit
        Me.LayoutControl5.ResumeLayout(false)
        CType(Me.CheckEdit8.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit9.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit10.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit11.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit12.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit13.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit14.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit15.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit16.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.CheckEdit17.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlGroup4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.EmptySpaceItem8,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SimpleLabelItem1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem17,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem18,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem19,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem20,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem21,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem22,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem23,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem24,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem25,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.LayoutControlItem26,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageConsultantCompliance.ResumeLayout(false)
        CType(Me.GridControl4,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView4,System.ComponentModel.ISupportInitialize).EndInit
        Me.NavigationPageAttendanceNotes.ResumeLayout(false)
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl1.ResumeLayout(false)
        CType(Me.MemoEdit3.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridControl5,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView5,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents NavigationPane1 As DevExpress.XtraBars.Navigation.NavigationPane
    Friend WithEvents NavigationPageSettlorDetails As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageTrusteeInformation As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageBeneficiaries As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageTrustFund As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPagePurposeofFund As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageConsultantCompliance As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPageAttendanceNotes As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButtonLookup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleLabelItem1 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit11 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit12 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit13 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit14 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit15 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit16 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CheckEdit17 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MemoEdit3 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl6 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridControl7 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
End Class
