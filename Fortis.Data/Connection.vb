﻿Imports System.Data.OleDb
Imports System.IO

Public Class Connection
    Public ReadOnly Property BuildConnectionString As String
        Get
            Dim builder As OleDbConnectionStringBuilder = New OleDbConnectionStringBuilder() With
                    {
                    .ConnectionString = String.Concat("Data Source=", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "fortis.accdb"))
                    }
            builder.Add("Provider", "Microsoft.ACE.OLEDB.12.0")
            Return builder.ToString()
        End Get
    End Property

    Public Sub New()
        MyBase.New()
    End Sub
End Class