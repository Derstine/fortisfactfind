﻿Imports System.Data.OleDb
Imports Dapper
Imports Fortis.BusinessComponent
Imports Fortis.Dto

Public Class DataTest: Implements  ITest

    Public Function GetString() As IEnumerable(Of String) Implements ITest.GetString
      Dim sql="select client from clause_library"
        Using con As IDbConnection = New OleDbConnection(New Connection().BuildConnectionString)
            Return ( con.Query(Of String)(sql))
        End Using
    End Function

  
End Class
  Public Class CurrentCases : Implements ICurrentCases

    ''' When doing multiple joins like this,
    ''' access requires you to add parentheses in the FROM clause
    ''' https://stackoverflow.com/questions/20929332/multiple-inner-join-sql-access

    Public Async Function GetCurrentCases() As Task(Of List(Of CurrentCasesDto)) Implements ICurrentCases.GetCurrentCases
       Dim sql = "SELECT
                            will_instruction_stage2_data.willClientSequence AS Appointment,
                            person1Forenames,
                            person1Surname,
                            person1Address,
                            person1Postcode,
                            person2Forenames,
                            person2Surname,
                            client_appointment.appointmentStart,
                            status
                    FROM    ((will_instruction_stage2_data
                        INNER JOIN
                            will_instruction_stage0_data
                                ON will_instruction_stage2_data.willClientSequence = will_instruction_stage0_data.sequence)
                        LEFT JOIN
                            client_appointment
                                ON will_instruction_stage2_data.willClientSequence = client_appointment.sequence)
                    WHERE
                            will_instruction_stage0_data.status = 'offline'
                            OR will_instruction_stage0_data.status = 'synchronise'
                    ORDER BY
                            will_instruction_stage2_data.person1Surname,
                            will_instruction_stage2_data.person1Forenames ASC;"

        Using con As IDbConnection = New OleDbConnection(New Connection().BuildConnectionString)
            Return (Await con.QueryAsync(Of CurrentCasesDto)(sql)).ToList()
        End Using
    End Function
End Class